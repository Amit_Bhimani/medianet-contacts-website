## MediaPeople

This contains the MediaPeople website.

## Submodules

There are two submodules used in this website.

1. The `MediaPeople.DataCnotracts` submodule is shared between the web and the [WCF](http://git.dev.aap/medianet/MediaPeopleWCF) project.
2. The `AAP.CSharpExtensionMethods` is an extension method library that is used.
