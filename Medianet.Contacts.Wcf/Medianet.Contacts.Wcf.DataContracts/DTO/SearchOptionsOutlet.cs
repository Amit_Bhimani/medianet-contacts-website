﻿using System;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract(Name="SearchOptionsOutlet")]
    public class SearchOptionsOutlet : SearchOptions
    {
        [DataMember]
        public string OutletID { get; set; }

        // Used for private contacts to determine what the outlet points to.
        [DataMember]
        public RecordType OutletType { get; set; }

        public SearchOptionsOutlet()
            : base() {
            Type = SearchType.Outlet;
            Context = SearchContext.Outlet;
            OutletID = string.Empty;
            OutletType = RecordType.Unknown;
        }

        public override bool HasSearchCriteria{
            get {
                return (!string.IsNullOrWhiteSpace(OutletID) &&
                        OutletType != RecordType.Unknown);
            }
        }

        public override bool SearchWithDB {
            get {
                return false;
            }
        }
    }
}