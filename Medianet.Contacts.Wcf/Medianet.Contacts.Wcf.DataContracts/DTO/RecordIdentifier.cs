﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    /// <summary>
    /// Business object that gets cached.
    /// </summary>
    [Serializable]
    [DataContract(Name="RecordIdentifier")]
    [KnownType(typeof(RecordType))]
    [KnownType(typeof(SearchResult))]
    public class RecordIdentifier : IEquatable<RecordIdentifier>, ICloneable
    {          
        public const string PREFIX_AAP_OUTLET = "AAPO";     
        public const string PREFIX_AAP_CONTACT = "AAPC";

        [DataMember]
        public string ContactIds { get; set; }

        public string ContactId {
            get {
                return IsContact ? ContactIds : "0";
            }
            set {
                ContactIds = value;
            }
        }

        [DataMember]
        public string OutletID { get; set; }

        // Used to work out what the record is pointing to.
        [DataMember]
        public RecordType Type { get; set; }

        /// <summary>
        /// If this record is an Outlet record then it returns the RecordType. If it is a Contact record
        /// it returns the RecordType of the outlet associated with the contact. Otherwise it returns Unknown;
        /// </summary>
        /// <returns></returns>        
        public RecordType OutletType {
            get {
                if (IsOutlet)
                    return Type;
                else if (Type == RecordType.MediaContact || Type == RecordType.OmaContactAtMediaOutlet)
                    return RecordType.MediaOutlet;
                else if (Type == RecordType.PrnContact || Type == RecordType.OmaContactAtPrnOutlet)
                    return RecordType.PrnOutlet;
                else if (Type == RecordType.OmaContactAtOmaOutlet)
                    return RecordType.OmaOutlet;
                else
                    return RecordType.Unknown;
            }
        }

        /// <summary>
        /// If this record is a Contact record then it returns the RecordType. If it is an Outlet record
        /// containing 1 single contact it returns the RecordType of the contact. Otherwise it returns Unknown;
        /// </summary>
        /// <returns></returns>
        public RecordType ContactType {
            get {
                if (IsContact)
                    return Type;
                else if (!string.IsNullOrEmpty(ContactIds) && !ContactIds.Contains("#") && ContactIds != "0") {
                    if (Type == RecordType.MediaOutlet) return RecordType.MediaContact;
                    else if (Type == RecordType.PrnOutlet) return RecordType.PrnContact;
                    else if (Type == RecordType.OmaOutlet) return RecordType.OmaContactAtOmaOutlet;
                }

                return RecordType.Unknown;
            }
        }

        #region Constructors
        
        public RecordIdentifier() {
            // Default values.
            Type = RecordType.OmaContactNoOutlet;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="OutletId">The outlet's ID</param>
        /// <param name="ContactId">The contact's ID. This defaults to 0 if this is an outlet contact.</param>
        /// <param name="recType">Value indicating what type of record this is.</param>
        /// <param name="outletRecType">Value indicating what type of record the outlet is.</param>
        public RecordIdentifier(string outletId, string contactId, RecordType recType) {
            this.OutletID = string.IsNullOrEmpty(outletId) ? null : outletId;
            this.ContactIds = string.IsNullOrEmpty(contactId) ? null : contactId; 
            this.Type = recType;
        }

        /// <summary>
        /// Constructor. Used when the RecordType provided is a string instead of an enum.
        /// </summary>
        /// <param name="OutletId">The outlet's ID</param>
        /// <param name="ContactId">The contact's ID. This defaults to 0 if this is an outlet contact.</param>
        /// <param name="recType">A string indicating what type of record this is.</param>
        /// <param name="outletRecType">A string indicating what type of record the outlet is.</param>
        public RecordIdentifier(string outletId, string contactId, string recType) {
            this.OutletID = string.IsNullOrEmpty(outletId) ? null : outletId;
            this.ContactIds = string.IsNullOrEmpty(contactId) ? null : contactId; 
            this.Type = (RecordType)Enum.Parse(typeof(RecordType), recType);
        }

        #endregion

        /// <summary>
        /// Determine if two record identifiers are the same.
        /// </summary>
        /// <param name="other">The <c>RecordIdentifer</c> to be compared to.</param>
        /// <returns>True if equal, False otherwise.</returns>
        public bool Equals(RecordIdentifier other) {
            if (other == null)
                return false;

            return Type == other.Type &&
                   OutletID == other.OutletID &&
                   ContactId == other.ContactId;
        }

        /// <summary>
        /// Override the base class <c>Object.Equals(Object)</c>.
        /// </summary>
        /// <param name="obj">The object to be compared to.</param>
        /// <returns>True if equal, False otherwise.</returns>
        public override bool Equals(object obj) {
            if (obj == null)
                return false;

            RecordIdentifier other = (RecordIdentifier)obj;

            return this.Equals(other);
        }

        /// <summary>
        /// Generate a unique hash based on relevant properties.
        /// </summary>
        /// <remarks>
        /// The prime numbers 17 and 23 are chosen to prevent collisions.
        /// This implementation is the same as what is used in the .NET compiler
        /// but we do it manually so we don't have the overhead of boxing value types.
        /// 
        /// See http://stackoverflow.com/a/263416/140578 for more info.
        /// </remarks>
        /// <returns>A integer hash representing the <c>RecordIdentifier</c> object.</returns>
        public override int GetHashCode() {
            int hash = 17;

            hash *= 23 + this.Type.GetHashCode();
            hash *= 23 + (this.ContactId != null ? this.ContactId.GetHashCode() : 0);
            hash *= 23 + (this.OutletID != null ? this.OutletID.GetHashCode() : 0);

            return hash;
        }

        /// <summary>
        /// Returns a client side representation of a record identifier.
        /// </summary>
        /// <returns></returns>
        public string ToClientString() {
            StringBuilder sb = new StringBuilder();
            sb.Append(OutletID);
            sb.Append("#");
            sb.Append(ContactId);
            sb.Append("#");
            sb.Append(Convert.ToInt32(Type));

            return sb.ToString();
        }

        public override string ToString() {
            string result = string.Empty;
            int value;

            switch (Type) {
                // Format: <AAPOutletId>#<AAPContactId>#<PrivateOutletId>#<PrivateContactId>#<PRNOutletId>#<PRNContactId>

                case RecordType.MediaOutlet:
                    if (OutletID.StartsWith(PREFIX_AAP_OUTLET))
                        result = string.Format("{0}#####", OutletID);
                    break;
                case RecordType.MediaContact:
                    if (OutletID.StartsWith(PREFIX_AAP_OUTLET) && ContactId.StartsWith(PREFIX_AAP_CONTACT))
                        result = string.Format("{0}#{1}####", OutletID, ContactId);
                    break;
                case RecordType.PrnOutlet:
                    if (int.TryParse(OutletID, out value))
                        result = string.Format("####{0}#", OutletID);
                    break;
                case RecordType.PrnContact:
                    if (int.TryParse(OutletID, out value) && int.TryParse(ContactId, out value))
                        result = string.Format("####{0}#{1}", OutletID, ContactId);
                    break;
                case RecordType.OmaOutlet:
                    if (int.TryParse(OutletID, out value))
                        result = string.Format("##{0}###", OutletID);
                    break;
                default:
                    if (IsPrivateContact) {
                        if (int.TryParse(ContactId, out value)) {
                            if (Type == RecordType.OmaContactAtMediaOutlet && OutletID.StartsWith(PREFIX_AAP_OUTLET))
                                result = string.Format("{0}###{1}##", OutletID, ContactId);
                            else if (Type == RecordType.OmaContactAtPrnOutlet && int.TryParse(OutletID, out value))
                                result = string.Format("###{0}#{1}#", ContactId, OutletID);
                            else if (Type == RecordType.OmaContactAtOmaOutlet && int.TryParse(OutletID, out value))
                                result = string.Format("##{0}#{1}##", OutletID, ContactId);
                            else
                                result = string.Format("###{0}##", ContactId);
                        }
                    }
                    break;
            }

            return result;
        }

        #region Derived Outlet and Contact ID's

        public string MediaOutletId {
            get {
                if (Type == RecordType.MediaOutlet || Type == RecordType.MediaContact)
                    return OutletID;
                else
                    return null;
            }
        }

        public string MediaContactId {
            get {
                if (Type == RecordType.MediaContact)
                    return ContactId;
                else
                    return null;
            }
        }

        public int? PrnOutletId {
            get {
                if (Type == RecordType.PrnOutlet || Type == RecordType.PrnContact)
                    return Convert.ToInt32(OutletID);
                else
                    return null;
            }
        }

        public int? PrnContactId {
            get {
                if (Type == RecordType.PrnContact)
                    return Convert.ToInt32(ContactId);
                else
                    return null;
            }
        }

        public int? OmaOutletId {
            get {
                if (Type == RecordType.OmaOutlet || Type == RecordType.OmaContactAtOmaOutlet)
                    return Convert.ToInt32(OutletID);
                else
                    return null;
            }
        }

        public int? OmaContactId {
            get {
                if (IsPrivateContact)
                    return Convert.ToInt32(ContactId);
                else
                    return null;
            }
        }

        #endregion

        #region Conditional checks

        /// <summary>
        /// Returns true if the record is referring to an Outlet.
        /// </summary>
        public bool IsOutlet {
            get {
                return (Type == RecordType.MediaOutlet ||
                        Type == RecordType.PrnOutlet ||
                        Type == RecordType.OmaOutlet);
            }
        }

        /// <summary>
        /// Returns true if the record is referring to a Contact.
        /// </summary>
        public bool IsContact {
            get {
                return (Type == RecordType.MediaContact ||
                        Type == RecordType.PrnContact ||
                        IsPrivateContact);
            }
        }

        /// <summary>
        /// Returns true if the record is referring to a Private Outlet or Contact.
        /// </summary>
        public bool IsPrivate {
            get {
                return (Type == RecordType.OmaOutlet || IsPrivateContact);
            }
        }

        /// <summary>
        /// Returns true if the record is referring to a Private Outlet.
        /// </summary>
        public bool IsPrivateOutlet {
            get {
                return (Type == RecordType.OmaOutlet);
            }
        }

        /// <summary>
        /// Returns true if the record is referring to a Private Contact.
        /// </summary>
        public bool IsPrivateContact {
            get {
                return (Type == RecordType.OmaContactNoOutlet ||
                        Type == RecordType.OmaContactAtMediaOutlet ||
                        Type == RecordType.OmaContactAtPrnOutlet ||
                        Type == RecordType.OmaContactAtOmaOutlet);
            }
        }

        /// <summary>
        /// Returns true if the record is referring to a Contact or
        /// it is referring to an Outlet with a single Contact.
        /// </summary>
        public bool HasContact {
            get {
                return (IsContact ||
                        (!string.IsNullOrEmpty(ContactIds) && !ContactIds.Contains("#") && ContactIds != "0"));
            }
        }

        /// <summary>
        /// Returns true if the record is referring to a Private Outlet or
        /// it is referring to a Private Contact at a Private Outlet.
        /// </summary>
        public bool HasPrivateOutlet {
            get {
                return (IsPrivateOutlet || Type == RecordType.OmaContactAtOmaOutlet);
            }
        }

        /// <summary>
        /// Returns true if the record is referring to a Private Contact or
        /// it is referring to an Outlet with a single Private Contact.
        /// </summary>
        public bool HasPrivateContact {
            get {
                return (IsPrivateContact ||
                        (Type == RecordType.OmaOutlet && !ContactIds.Contains("#") && ContactId != "0"));
            }
        }

        #endregion

        #region ICloneable Members

        public virtual object Clone() {
            return new RecordIdentifier() {
                ContactIds = this.ContactIds,
                OutletID = this.OutletID,
                Type = this.Type
            };
        }

        #endregion
    }
}