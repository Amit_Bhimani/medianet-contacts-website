﻿using System;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(RecordType))]
    public class SearchResult : RecordIdentifier, ICloneable
    {
        [DataMember]
        public string Id { get; set; }  // prn_contact, Contactid mapped from solrcontact

        [DataMember]
        public string OutletName { get; set; }
        [DataMember]
        public string ContactDisplay { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string MobileNumber { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string Twitter { get; set; }
        [DataMember]
        public string LogoFilename { get; set; }
        [DataMember]
        public DateTime? ContactOooStartDate { get; set; }
        [DataMember]
        public DateTime? ContactOooReturnDate { get; set; }
        [DataMember]
        public string RoleDisplay { get; set; }
        [DataMember]
        public string RoleName { get; set; }
        [DataMember]
        public string SubjectDisplay { get; set; }
        [DataMember]
        public string SubjectName { get; set; }
        [DataMember]
        public string ProductTypeName { get; set; }
        [DataMember]
        public string CityName { get; set; }
        [DataMember]
        public string StateName { get; set; }
        [DataMember]
        public int ServiceId { get; set; }
        [DataMember]
        public string ServiceName { get; set; }
        [DataMember]
        public string ContactPriorityNotice { get; set; }
        [DataMember]
        public string OutletPriorityNotice { get; set; }
        [DataMember]
        public string DistributionType { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public int? ModifiedUserId { get; set; }
        [DataMember]
        public string ModifiedUser { get; set; }
        [DataMember]
        public ContactStatus Status { get; set; }
        [DataMember]
        public double? Score { get; set; }

        /// <summary>
        /// True if record is pinned.
        /// </summary>
        [DataMember]
        public bool Pinned { get; set; }

        /// <summary>
        /// True if record belongs to current search. This will be true
        /// only if the item is also pinned.
        /// </summary>
        [DataMember]
        public bool BelongsToCurrentSearch { get; set; }

        /// <summary>
        /// True if the record is checked. Defaults to true.
        /// </summary>
        [DataMember]
        public bool Selected { get; set; }

        /// <summary>
        /// this valus is true is the outlet is generic
        /// </summary>
        [DataMember]
        public bool IsGeneric { get; set; }

        [DataMember]
        public int? ContactMediaInfluencerScore { get; set; }

        public SearchResult() {
            Selected = true;
            BelongsToCurrentSearch = true;
            IsGeneric = false;
        }

        #region ICloneable Members

        public override object Clone() {
            var super = (RecordIdentifier)base.Clone();

            return new SearchResult {
                Id = this.Id,
                Type = super.Type,
                ContactId = super.ContactIds,
                OutletID = super.OutletID,
                OutletName = this.OutletName,
                ContactDisplay = this.ContactDisplay,
                FullName = this.FullName,
                JobTitle = this.JobTitle,
                PhoneNumber = this.PhoneNumber,
                MobileNumber = this.MobileNumber,
                EmailAddress = this.EmailAddress,
                Twitter = this.Twitter,
                LogoFilename = this.LogoFilename,
                ContactOooStartDate = this.ContactOooStartDate,
                ContactOooReturnDate = this.ContactOooReturnDate,
                RoleDisplay = this.RoleDisplay,
                RoleName = this.RoleName,
                SubjectDisplay = this.SubjectDisplay,
                SubjectName = this.SubjectName,
                ProductTypeName = this.ProductTypeName,
                CityName = this.CityName,
                StateName = this.StateName,
                ServiceId = this.ServiceId,
                ServiceName = this.ServiceName,
                DistributionType = this.DistributionType,
                CreatedDate = this.CreatedDate,
                ModifiedDate = this.ModifiedDate,
                ModifiedUserId = this.ModifiedUserId,
                ModifiedUser = this.ModifiedUser,
                Status = this.Status,
                Pinned = this.Pinned,
                BelongsToCurrentSearch = this.BelongsToCurrentSearch,
                Selected = this.Selected,
                IsGeneric = this.IsGeneric,
                ContactMediaInfluencerScore = this.ContactMediaInfluencerScore,
                ContactPriorityNotice = this.ContactPriorityNotice,
                OutletPriorityNotice = this.OutletPriorityNotice,
                Score = this.Score
            };
        }

        #endregion
    }
}
