﻿using Medianet.Contacts.CSharp.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Web;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [DataContract(Name = "Search")]
    [KnownType(typeof(SearchType))]
    //[KnownType(typeof(GroupBase))]
    public class SavedSearch
    {
        public const string CONTEXT_MODE_PEOPLE = "people";
        public const string CONTEXT_MODE_OUTLET = "outlet";
        public const string CONTEXT_MODE_BOTH = "both";

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Query { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        //[DataMember]
        //public GroupBase Group { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public bool Visibility { get; set; }
        [DataMember]
        public string Context { get; set; }
        [DataMember]
        public string Criteria { get; set; }
        [DataMember]
        public SearchType Type { get; set; }

        /// <summary>
        /// Converts a saved search to a SearchOptions object.
        /// </summary>
        public SearchOptions ToSearchOptions() {
            SearchOptions sOptions;
            NameValueCollection values = HttpUtility.ParseQueryString(this.Criteria);

            // Checking both AdvancedSearch and QuickSearch might seem redundant
            // but it was necessary to make it backwards compatible.
            if (values["QuickSearch"] == "true" && values["AdvancedSearch"] != "true") {
                SearchOptionsQuick so = new SearchOptionsQuick();
                so.Type = SearchType.Quick;

                if (values["SearchAustralia"] == null && values["SearchNewZealand"] == null && values["SearchRestOfWorld"] == null) {
                    // For old saved searches that don't have these values select all countries.
                    so.SearchAustralia = true;
                    so.SearchNewZealand = true;
                    so.SearchAllCountries = true;
                }
                else {
                    so.SearchAustralia = values["SearchAustralia"].ParseBool(so.SearchAustralia);
                    so.SearchNewZealand = values["SearchNewZealand"].ParseBool(so.SearchNewZealand);
                    so.SearchAllCountries = values["SearchRestOfWorld"].ParseBool(so.SearchAllCountries);
                }

                so.LocationList = SearchOptionsQuick.ParseLocation(values["LocationRegionWiseContact"]);
                so.PositionList = ParseIntCSV(values["RoleContact"]);
                so.MediaTypeList = ParseIntCSV(values["MediaContact"]);
                so.SubjectList = ParseIntCSV(values["SubjectContact"]);
                so.SubjectGroupList = ParseIntCSV(values["SubjectGroupContact"]);

                so.SearchText = values["ContactContact"] ?? string.Empty;

                sOptions = so;
            }
            else if (values["OutletSearch"] == "true") {
                SearchOptionsOutlet so = new SearchOptionsOutlet();
                string outletType;

                so.Type = SearchType.Outlet;
                so.OutletID = values["OutletId"] ?? string.Empty;
                outletType = values["OutletType"] ?? string.Empty;

                if (string.IsNullOrEmpty(outletType)) {
                    // Old Outlet searches don't have the OutletType so guess.
                    int id;

                    if (int.TryParse(so.OutletID, out id))
                        so.OutletType = RecordType.OmaOutlet;
                    else
                        so.OutletType = RecordType.MediaOutlet;
                }
                else
                    so.OutletType = (RecordType)Enum.Parse(typeof(RecordType), outletType);

                sOptions = so;
            }
            else {
                SearchOptionsAdvanced so = new SearchOptionsAdvanced();
                so.Type = SearchType.Advanced;

                // People region.
                so.FirstName = values["ContactFirstName"] ?? string.Empty;
                so.LastName = values["ContactLastName"] ?? string.Empty;
                so.FullName = values["ContactFullName"] ?? string.Empty;
                so.JobTitle = values["JobTitle"] ?? string.Empty;

                so.PositionList = ParseIntCSV(values["RoleContact"]);
                so.ContactSubjectList = ParseIntCSV(values["SubjectContact"]);
                so.ContactSubjectGroupList = ParseIntCSV(values["SubjectGroupContact"]);

                so.PrimaryNewsContactOnly = values["PrimaryNewsContactOnly"].ParseBool(so.PrimaryNewsContactOnly);

                // Contact Geographic region.
                so.ContactContinentIDList = ParseIntCSV(values["ContinentGeoGraphicContact"]);
                so.ContactCountryIDList = ParseIntCSV(values["CountryGeographicContact"]);
                so.ContactStateIDList = ParseIntCSV(values["StateGeographicContact"]);
                so.ContactCityIDList = ParseIntCSV(values["CityGeographicContact"]);
                so.ContactPostCodeList = ParseStringCSV(values["PostCodeGeographicContact"]);

                // Outlet region.
                so.OutletName = values["OutletOutlet"] ?? string.Empty;
                so.MediaTypeList = ParseIntCSV(values["MediaOutlet"]);
                so.OutletSubjectList = ParseIntCSV(values["SubjectOutlet"]);
                so.OutletSubjectGroupList = ParseIntCSV(values["SubjectGroupOutlet"]);
                so.OutletFrequencyList = ParseIntCSV(values["FrequencyOutlet"]);

                int retVal;
                so.OutletCirculationMin = string.IsNullOrWhiteSpace(values["CirculationMinOutlet"]) &&
                    int.TryParse(values["CirculationMinOutlet"], out retVal) ?
                    retVal : so.OutletCirculationMin;

                so.OutletCirculationMax = string.IsNullOrWhiteSpace(values["CirculationMaxOutlet"]) &&
                    int.TryParse(values["CirculationMaxOutlet"], out retVal) ?
                    retVal : so.OutletCirculationMax; ;

                so.OutletLanguageList = ParseIntCSV(values["LanguageOutlet"]);
                so.OutletStationFrequencyList = ParseStringCSV(values["StationFrequencyOutlet"]);

                // Outlet Geographic region.
                so.OutletContinentIDList = ParseIntCSV(values["ContinentGeoGraphic"]);
                so.OutletCountryIDList = ParseIntCSV(values["CountryGeographic"]);
                so.OutletStateIDList = ParseIntCSV(values["StateGeographic"]);
                so.OutletCityIDList = ParseIntCSV(values["CityGeographicOutlet"]);

                so.OutletPostCodeList = ParseStringCSV(values["PostCodeGeographicOutlet"]);
                so.IncludeMetroOutlets = values["IncludeMetroOutlets"].ParseBool(so.IncludeMetroOutlets);
                so.IncludeRegionalOutlets = values["IncludeRegionalOutlets"].ParseBool(so.IncludeRegionalOutlets);
                so.IncludeSuburbanOutlets = values["IncludeSuburbanOutlets"].ParseBool(so.IncludeSuburbanOutlets);

                // Notes region.
                so.SystemNotes = values["SystemNotes"] ?? string.Empty;
                so.MyNotes = values["MyNotes"] ?? string.Empty;

                // Communication Preferences region.
                so.PrefersEmail = values["EmailChecked"].ParseBool(so.PrefersEmail);
                so.PrefersFax = values["FaxChecked"].ParseBool(so.PrefersFax);
                so.PrefersMail = values["MailChecked"].ParseBool(so.PrefersMail);
                so.PrefersPhone = values["PrefersPhoneChecked"].ParseBool(so.PrefersPhone);
                so.PrefersMobile = values["PrefersMobileChecked"].ParseBool(so.PrefersMobile);
                so.PrefersTwitter = values["PrefersTwitterChecked"].ParseBool(so.PrefersTwitter);
                so.PrefersLinkedIn = values["PrefersLinkedInChecked"].ParseBool(so.PrefersLinkedIn);
                so.PrefersFacebook = values["PrefersFacebookChecked"].ParseBool(so.PrefersFacebook);
                so.PrefersInstagram = values["PrefersInstagramChecked"].ParseBool(so.PrefersInstagram);
                so.ShowOnlyWithEmail = values["EmailAddressChecked"].ParseBool(so.ShowOnlyWithEmail);
                so.ShowOnlyWithFax = values["FaxNumberChecked"].ParseBool(so.ShowOnlyWithFax);
                so.ShowOnlyWithPostalAddress = values["PostalAddressChecked"].ParseBool(so.ShowOnlyWithPostalAddress);

                // Show only results with section.
                so.ShowOnlyWithTwitter = values["TwitterChecked"].ParseBool(so.ShowOnlyWithTwitter);
                so.ShowOnlyWithLinkedIn = values["LinkedinChecked"].ParseBool(so.ShowOnlyWithLinkedIn);
                so.ShowOnlyWithFacebook = values["FacebookChecked"].ParseBool(so.ShowOnlyWithFacebook);
                so.ShowOnlyWithInstagram = values["InstagramChecked"].ParseBool(so.ShowOnlyWithInstagram);
                so.ShowOnlyWithYouTube = values["YouTubeChecked"].ParseBool(so.ShowOnlyWithYouTube);
                so.ShowOnlyWithSnapchat = values["SnapchatChecked"].ParseBool(so.ShowOnlyWithSnapchat);

                // Lists section.
                so.ListList = ParseIntCSV(values["WithinListId"]);

                // Exclusion section.
                if (values["MediaDirectoryRecords"].ParseBool(false))
                    so.RecordsToShow = SearchOptionsAdvanced.SHOW_RECORDS_MEDIA_DIR;
                else if (values["PrivateRecords"].ParseBool(false))
                    so.RecordsToShow = SearchOptionsAdvanced.SHOW_RECORDS_PRIVATE;
                else
                    so.RecordsToShow = SearchOptionsAdvanced.SHOW_RECORDS_ALL;

                so.ExcludeKeywords = values["ExcludeKeyword"] ?? string.Empty;
                so.ExcludeOPS = values["ExcludeOPS"].ParseBool(so.ExcludeOPS);
                so.ExcludeFinanceInst = values["ExcludeFinance"].ParseBool(so.ExcludeFinanceInst);
                so.ExcludePoliticians = values["ExcludePoliticians"].ParseBool(so.ExcludePoliticians);
                so.ExcludeOutOfOffice = values["ExcludeOutOfOffice"].ParseBool(so.ExcludeOutOfOffice);

                sOptions = so;
            }

            // The context on the database is either o (outlet), b (both) or p (people).
            if (sOptions.Type == SearchType.Advanced)
                sOptions.Context = SearchContext.Both;
            else {
                if (CONTEXT_MODE_OUTLET.StartsWith(this.Context))
                    sOptions.Context = SearchContext.Outlet;
                else if (CONTEXT_MODE_BOTH.StartsWith(this.Context))
                    sOptions.Context = SearchContext.Both;
                else
                    sOptions.Context = SearchContext.People;
            }
            return sOptions;
        }

        /// <summary>
        /// Parses a CSV of numbers into a List of ints.
        /// </summary>
        /// <param name="input">The string to parse.</param>
        /// <returns>A List of ints.</returns>
        private List<int> ParseIntCSV(string input) {
            return input.ParseCSV<int>(x => int.Parse(x),
                x => {
                    int y;
                    return int.TryParse(x, out y);
                });
        }

        /// <summary>
        /// Parses the a string CSV to a List of strings.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <returns>A List of strings.</returns>
        private List<string> ParseStringCSV(string input) {
            return input.ParseCSV<string>(x => x);
        }
    }
}


