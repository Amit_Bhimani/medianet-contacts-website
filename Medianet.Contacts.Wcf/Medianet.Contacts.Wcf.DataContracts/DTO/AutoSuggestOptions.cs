﻿using System;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract]
    public class AutoSuggestOptions
    {
        [DataMember]
        public SearchContext Context { get; set; }

        [DataMember]
        public string SearchText { get; set; }

        [DataMember]
        public bool IsAdminSearch { get; set; }

        [DataMember]
        public bool SearchAustralia { get; set; }

        [DataMember]
        public bool SearchNewZealand { get; set; }

        [DataMember]
        public bool SearchAllCountries { get; set; }

        public AutoSuggestOptions() { }

        public AutoSuggestOptions(SearchContext context,
                                string searchText,
                                bool isAdminSearch,
                                bool searchAustralia,
                                bool searchNewZealand,
                                bool searchAllCountries) {
            Context = context;
            SearchText = searchText;
            IsAdminSearch = isAdminSearch;
            SearchAustralia = searchAustralia;
            SearchNewZealand = searchNewZealand;
            SearchAllCountries = searchAllCountries;
        }

        #region Public Methods

        public bool SearchWithDB() {
            return IsAdminSearch;
        }

        #endregion
    }
}
