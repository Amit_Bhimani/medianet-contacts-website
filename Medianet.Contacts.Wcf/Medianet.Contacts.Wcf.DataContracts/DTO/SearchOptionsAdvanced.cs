﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract(Name = "SearchOptionsAdvanced")]
    public class SearchOptionsAdvanced : SearchOptions
    {
        [DataMember]
        public static readonly string SHOW_RECORDS_ALL = "A";
        [DataMember]
        public static readonly string SHOW_RECORDS_MEDIA_DIR = "M";
        [DataMember]
        public static readonly string SHOW_RECORDS_PRIVATE = "P";

        [DataMember]
        public string SearchText { get; set; }

        // People section.
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public List<int> PositionList { get; set; }
        [DataMember]
        public List<int> ContactSubjectList { get; set; }
        [DataMember]
        public List<int> ContactSubjectGroupList { get; set; }
        [DataMember]
        public bool PrimaryNewsContactOnly { get; set; }

        [DataMember]
        public string ContactInfluencerScore { get; set; }

        // Contact Geographic section.
        [DataMember]
        public List<int> ContactContinentIDList { get; set; }
        [DataMember]
        public List<int> ContactCountryIDList { get; set; }
        [DataMember]
        public List<int> ContactStateIDList { get; set; }
        [DataMember]
        public List<int> ContactCityIDList { get; set; }
        [DataMember]
        public List<string> ContactPostCodeList { get; set; }

        // Outlet section.
        [DataMember]
        public string OutletName { get; set; }
        [DataMember]
        public List<int> MediaTypeList { get; set; }
        [DataMember]
        public List<int> OutletSubjectList { get; set; }
        [DataMember]
        public List<int> OutletSubjectGroupList { get; set; }
        [DataMember]
        public List<int> OutletFrequencyList { get; set; }
        [DataMember]
        public int OutletCirculationMin { get; set; }
        [DataMember]
        public int OutletCirculationMax { get; set; }
        [DataMember]
        public List<int> OutletLanguageList { get; set; }
        [DataMember]
        public List<string> OutletStationFrequencyList { get; set; }


        // Outlet Geographic section.
        [DataMember]
        public List<int> OutletContinentIDList { get; set; }
        [DataMember]
        public List<int> OutletCountryIDList { get; set; }
        [DataMember]
        public List<int> OutletStateIDList { get; set; }
        [DataMember]
        public List<int> OutletCityIDList { get; set; }
        [DataMember]
        public List<string> OutletPostCodeList { get; set; }
        [DataMember]
        public bool IncludeMetroOutlets { get; set; }
        [DataMember]
        public bool IncludeRegionalOutlets { get; set; }
        [DataMember]
        public bool IncludeSuburbanOutlets { get; set; }

        // Notes section.
        [DataMember]
        public string SystemNotes { get; set; }
        [DataMember]
        public string MyNotes { get; set; }

        // Communication Preferences section.
        [DataMember]
        public bool PrefersEmail { get; set; }
        [DataMember]
        public bool PrefersFax { get; set; }  // Kept for backward compability saved searches.
        [DataMember]
        public bool PrefersMail { get; set; }

        [DataMember]
        public bool PrefersPhone { get; set; }
        [DataMember]
        public bool PrefersMobile { get; set; }

        [DataMember]
        public bool PrefersTwitter { get; set; }
        [DataMember]
        public bool PrefersLinkedIn { get; set; }
        [DataMember]
        public bool PrefersFacebook { get; set; }
        [DataMember]
        public bool PrefersInstagram { get; set; }

        // Show only results with section.
        [DataMember]
        public bool ShowOnlyWithTwitter { get; set; }
        [DataMember]
        public bool ShowOnlyWithLinkedIn { get; set; }
        [DataMember]
        public bool ShowOnlyWithFacebook { get; set; }

        [DataMember]
        public bool ShowOnlyWithInstagram { get; set; }
        [DataMember]
        public bool ShowOnlyWithYouTube { get; set; }
        [DataMember]
        public bool ShowOnlyWithSnapchat { get; set; }
        
        [DataMember]
        public bool ShowOnlyWithEmail { get; set; }
        [DataMember]
        public bool ShowOnlyWithFax { get; set; }
        [DataMember]
        public bool ShowOnlyWithPostalAddress { get; set; }
        // Lists section.
        [DataMember]
        public List<int> ListList { get; set; }

        // Tasks section.
        [DataMember]
        public string TaskKeyword { get; set; }
        [DataMember]
        public List<int> TaskGroups { get; set; }
        [DataMember]
        public List<int> TaskCategories { get; set; }
        [DataMember]
        public TaskStatus? TaskStatus { get; set; }
        [DataMember]
        public bool ExcludeCompletedTasks { get; set; }


        // Exclusion section.
        [DataMember]
        public string RecordsToShow { get; set; }

        public bool ShowMediaDirRecords
        {
            get
            {
                return (RecordsToShow == SHOW_RECORDS_ALL ||
                        RecordsToShow == SHOW_RECORDS_MEDIA_DIR);
            }
        }

        public bool ShowPrivateRecords
        {
            get
            {
                return (RecordsToShow == SHOW_RECORDS_ALL ||
                        RecordsToShow == SHOW_RECORDS_PRIVATE);
            }
        }

        [DataMember]
        public string ExcludeKeywords { get; set; }
        [DataMember]
        public bool ExcludeOPS { get; set; }
        [DataMember]
        public bool ExcludeFinanceInst { get; set; }
        [DataMember]
        public bool ExcludePoliticians { get; set; }
        [DataMember]
        public bool ExcludeOutOfOffice { get; set; }

        public SearchOptionsAdvanced()
            : base()
        {
            Type = SearchType.Advanced;

            // People section.
            FirstName = string.Empty;
            LastName = string.Empty;
            FullName = string.Empty;
            JobTitle = string.Empty;
            PositionList = null;
            ContactSubjectList = null;
            ContactSubjectGroupList = null;
            PrimaryNewsContactOnly = false;

            // Contact Geographic section.
            ContactContinentIDList = null;
            ContactCountryIDList = null;
            ContactStateIDList = null;
            ContactCityIDList = null;
            ContactPostCodeList = null;

            // Outlet section.
            OutletName = string.Empty;
            MediaTypeList = null;
            OutletSubjectList = null;
            OutletSubjectGroupList = null;
            OutletFrequencyList = null;
            OutletCirculationMin = 0;
            OutletCirculationMax = 0;
            OutletLanguageList = null;
            OutletStationFrequencyList = null;

            // Outlet Geographic section.
            OutletContinentIDList = null;
            OutletCountryIDList = null;
            OutletStateIDList = null;
            OutletCityIDList = null;
            OutletPostCodeList = null;
            IncludeMetroOutlets = true;
            IncludeRegionalOutlets = true;
            IncludeSuburbanOutlets = true;

            // Notes section.
            SystemNotes = string.Empty;
            MyNotes = string.Empty;

            // Communication Preferences section.
            //PrefersEmail = true;
            //PrefersFax = true;
            //PrefersMail = true;
            //PrefersTwitter = true;
            //PrefersLinkedIn = true;
            //PrefersFacebook = true;
            //PrefersInstagram = true;
            //PrefersSkype = true;

            // Show only results with section.
            ShowOnlyWithTwitter = false;
            ShowOnlyWithLinkedIn = false;
            ShowOnlyWithFacebook = false;

            ShowOnlyWithInstagram = false;
            ShowOnlyWithYouTube = false;
            ShowOnlyWithSnapchat = false;
            
            ShowOnlyWithEmail = false;
            ShowOnlyWithFax = false;
            ShowOnlyWithPostalAddress = false;

            // Lists section.
            ListList = null;

            // Exclusion section.
            RecordsToShow = SHOW_RECORDS_ALL;
            ExcludeKeywords = string.Empty;
            ExcludeOPS = false;
            ExcludeFinanceInst = true;
            ExcludePoliticians = true;
            ExcludeOutOfOffice = false;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has search criteria.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has search criteria; otherwise, <c>false</c>.
        /// </value>
        public override bool HasSearchCriteria
        {
            get
            {
                return ((!string.IsNullOrWhiteSpace(SearchText)) ||
                        (!string.IsNullOrWhiteSpace(FirstName)) ||
                        (!string.IsNullOrWhiteSpace(LastName)) ||
                        (!string.IsNullOrWhiteSpace(FullName)) ||
                        (!string.IsNullOrWhiteSpace(JobTitle)) ||
                        (PositionList != null && PositionList.Count > 0) ||
                        (ContactSubjectList != null && ContactSubjectList.Count > 0) ||
                        //(PrimaryNewsContactOnly == true) ||
                        (ContactSubjectGroupList != null && ContactSubjectGroupList.Count > 0) ||
                        (ContactContinentIDList != null && ContactContinentIDList.Count > 0) ||
                        (ContactCountryIDList != null && ContactCountryIDList.Count > 0) ||
                        (ContactStateIDList != null && ContactStateIDList.Count > 0) ||
                        (ContactCityIDList != null && ContactCityIDList.Count > 0) ||
                        (ContactPostCodeList != null && ContactPostCodeList.Count > 0) ||
                        (!string.IsNullOrWhiteSpace(ContactInfluencerScore)) ||
                        (!string.IsNullOrWhiteSpace(OutletName)) ||
                        (MediaTypeList != null && MediaTypeList.Count > 0) ||
                        (OutletSubjectList != null && OutletSubjectList.Count > 0) ||
                        (OutletSubjectGroupList != null && OutletSubjectGroupList.Count > 0) ||
                        (OutletFrequencyList != null && OutletFrequencyList.Count > 0) ||
                        (OutletCirculationMin > 0) ||
                        (OutletCirculationMax > 0) ||
                        (OutletLanguageList != null && OutletLanguageList.Count > 0) ||
                        (OutletStationFrequencyList != null && OutletStationFrequencyList.Count > 0) ||
                        (!string.IsNullOrWhiteSpace(SystemNotes)) ||
                        (!string.IsNullOrWhiteSpace(MyNotes)) ||
                        (OutletContinentIDList != null && OutletContinentIDList.Count > 0) ||
                        (OutletCountryIDList != null && OutletCountryIDList.Count > 0) ||
                        (OutletStateIDList != null && OutletStateIDList.Count > 0) ||
                        (OutletCityIDList != null && OutletCityIDList.Count > 0) ||
                        (OutletPostCodeList != null && OutletPostCodeList.Count > 0) ||
                        //(IncludeMetroOutlets == false) ||
                        //(IncludeRegionalOutlets == false) ||
                        //(IncludeSuburbanOutlets == false) ||
                        (PrefersEmail == true) ||
                        //(PrefersFax == false) ||
                        (PrefersMail == true) ||
                        (PrefersPhone == true) ||
                        (PrefersMobile == true) ||
                        (PrefersTwitter == true) ||
                        (PrefersLinkedIn == true) ||
                        (PrefersFacebook == true) ||
                        (PrefersInstagram == true) ||
                        (ShowOnlyWithTwitter == true) ||
                        (ShowOnlyWithLinkedIn == true) ||
                        (ShowOnlyWithFacebook == true) ||
                        (ShowOnlyWithInstagram == true) ||
                        (ShowOnlyWithYouTube == true) ||
                        (ShowOnlyWithSnapchat == true) ||
                        (ListList != null && ListList.Count > 0) ||
                        (RecordsToShow != SHOW_RECORDS_ALL)
                        //(!string.IsNullOrWhiteSpace(ExcludeKeywords)) ||
                        //(ExcludeOPS == true) ||
                        //(ExcludeFinanceInst == true) ||
                        //(ExcludePoliticians == true)
                    );
            }
        }

        /// <summary>
        /// Determines whether [has task or list critiera].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [has task or list critiera]; otherwise, <c>false</c>.
        /// </returns>
        public override bool SearchWithDB
        {
            get
            {
                return false;
            }
        }
    }
}