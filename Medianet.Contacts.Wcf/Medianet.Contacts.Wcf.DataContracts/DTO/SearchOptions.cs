﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract(Name="SearchOptions")]
    [KnownType(typeof(SearchOptionsAdvanced))]
    [KnownType(typeof(SearchOptionsOutlet))]
    [KnownType(typeof(SearchOptionsQuick))]
    [KnownType(typeof(SearchOptionsServiceList))]
    [KnownType(typeof(SearchOptionsQuickAdmin))]
    public abstract class SearchOptions
    {
        [DataMember]
        public string SearchName { get; set; }

        [DataMember]
        public SearchType Type { get; set; }

        [DataMember]
        public SearchContext Context { get; set; }

        public string ContextDescription {
            get { return Enum.GetName(typeof(SearchContext), Context).ToLower(); }
        }

        // The context text stored in the db is the first char of the context description (in lower case).
        public string ContextDBCode {
            get { return Enum.GetName(typeof(SearchContext), Context).Substring(0, 1).ToLower(); }
        }

        public SearchOptions() {
            Type = SearchType.Quick;
            Context = SearchContext.People;
        }

        #region Public Methods
                
        public abstract bool HasSearchCriteria { get; }

        public abstract bool SearchWithDB { get; }

        public static List<int> ParseIntCSV(string pValue) {
            List<int> result = new List<int>();

            if (!string.IsNullOrEmpty(pValue)) {
                string[] idList = pValue.Split(',');
                int id;

                foreach (string s in idList) {
                    if (int.TryParse(s, out id))
                        result.Add(id);
                }
            }

            return result;
        }

        public static List<int> ParsePrefixedIntCSV(string pValue, string pPrefix) {
            List<int> result = new List<int>();

            if (!string.IsNullOrEmpty(pValue)) {
                string[] idList = pValue.Split(',');
                int id;

                foreach (string s in idList) {
                    if (s.StartsWith(pPrefix)) {

                        if (int.TryParse(s.Substring(pPrefix.Length), out id))
                            result.Add(id);
                    }
                }
            }

            return result;
        }

        public static List<string> ParseStringCSV(string pValue) {
            List<string> result = new List<string>();

            if (!string.IsNullOrEmpty(pValue)) {
                string[] strList = pValue.Split(',');

                foreach (string s in strList) {
                    if (!string.IsNullOrEmpty(s))
                        result.Add(s.Trim());
                }
            }

            return result;
        }

        public static string ParseText(string pValue) {
            if (pValue == null)
                return string.Empty;
            else
                return pValue;
        }

        public static int ParseInt(string pValue, int pDefault) {
            int result;

            if (pValue == null || !int.TryParse(pValue, out result))
                return pDefault;
            else
                return result;
        }

        public static bool ParseBool(string pValue, bool pDefault) {
            if (pValue == null)
                return pDefault;
            else
                return (pValue.ToLower() == "true");
        }

        public static string GetIntCSV(List<int> pList) {
            string result = string.Empty;

            if (pList != null) {
                for (int i = 0; i < pList.Count; i++) {
                    if (i != 0)
                        result += ",";

                    result += pList[i].ToString();
                }
            }

            return result;
        }

        public static string GetStringCSV(List<string> pList) {
            string result = string.Empty;

            if (pList != null) {
                for (int i = 0; i < pList.Count; i++) {
                    if (!string.IsNullOrEmpty(pList[i])) {
                        if (result.Length > 0)
                            result += ",";

                        result += pList[i];
                    }

                }
            }

            return result;
        }

        #endregion
    }
}