﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract(Name="SearchOptionsQuickAdmin")]
    public class SearchOptionsQuickAdmin : SearchOptions
    {
        [DataMember]
        public List<int> PositionList { get; set; }
        [DataMember]
        public List<int> MediaTypeList { get; set; }
        [DataMember]
        public List<int> SubjectList { get; set; }
        [DataMember]
        public List<int> SubjectGroupList { get; set; }
        [DataMember]
        public string SearchText { get; set; }
        [DataMember]
        public bool ShouldSearchNotes { get; set; }
        [DataMember]
        public ContactStatus? Status { get; set; }
        [DataMember]
        public int CountryId { get; set; }
        [DataMember]
        public int StateId { get; set; }
        [DataMember]
        public int CityId { get; set; }
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string ContactId { get; set; }

        // The Status text stored in the db is the first char of the Status description (in upper case).
        public string StatusDBCode {
            get { return Status.HasValue ? Enum.GetName(typeof(ContactStatus), Status).Substring(0, 1).ToUpper() : string.Empty; }
        }

        public SearchOptionsQuickAdmin()
            : base() {
                Type = SearchType.QuickAdmin;

            PositionList = null;
            MediaTypeList = null;
            SubjectList = null;
            SubjectGroupList = null;
            SearchText = string.Empty;
            ShouldSearchNotes = false;
            Status = ContactStatus.Active;
            ContactId = string.Empty;
            OutletId = string.Empty;
            CountryId = 0;
            StateId = 0;
            CityId = 0;
            }

        #region Public Methods

        public override bool HasSearchCriteria {
            get {
                return ((PositionList != null && PositionList.Count > 0) ||
                        (MediaTypeList != null && MediaTypeList.Count > 0) ||
                        (SubjectList != null && SubjectList.Count > 0) ||
                        (SubjectGroupList != null && SubjectGroupList.Count > 0) ||
                        !string.IsNullOrWhiteSpace(SearchText) ||
                        Status == ContactStatus.Unassigned);
            }
        }

        public override bool SearchWithDB {
            get {
                return true;
            }
        }

        #endregion
    }

}