﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [DataContract]
    public enum SortColumn
    {
        [EnumMember]
        [DbString("")]
        Default = 0,
        [EnumMember]
        [DbString("outletname")]
        OutletName = 1,
        [EnumMember]
        [DbString("fullname")]
        ContactName = 2,
        [EnumMember]
        [DbString("jobtitle")]
        JobTitle = 3,
        [EnumMember]
        [DbString("subjectname")]
        Subject = 4,
        [EnumMember]
        [DbString("producttypename")]
        MediaType = 5,
        [EnumMember]
        [DbString("cityname")]
        Location = 6,
        [EnumMember]
        [DbString("servicename")]
        ServiceName = 7,
        [EnumMember]
        [DbString("updateddate")]
        UpdatedDate = 8,
        [EnumMember]
        [DbString("updateduser")]
        UpdatedUser = 9,
        [EnumMember]
        [DbString("disttype")]
        DistributionType = 10,
        [EnumMember]
        [DbString("ContactInfluencerScore")]
        ContactInfluencerScore = 11,
        [EnumMember]
        [DbString("solrscore")]
        SolrScore = 12
    }

    [DataContract]
    public enum ContactSortColumn
    {
        [EnumMember]
        [DbString("firstname")]
        FirstName = 1,
        [EnumMember]
        [DbString("lastname")]
        LastName = 2,
        [EnumMember]
        [DbString("jobtitle")]
        JobTitle = 3,
        [EnumMember]
        [DbString("email")]
        Email = 4,
        [EnumMember]
        [DbString("phone")]
        Phone = 5,
    }

    [DataContract]    
    public enum SortDirection
    {
        [EnumMember]
        [DbString("asc")]
        ASC = 1,
        [EnumMember]
        [DbString("desc")]
        DESC = 2
    }

    [DataContract]
    public enum RecordType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        MediaOutlet = 1,
        [EnumMember]
        MediaContact = 2,
        [EnumMember]
        PrnOutlet = 3,
        [EnumMember]
        PrnContact = 4,
        [EnumMember]
        OmaOutlet = 5,
        [EnumMember]
        OmaContactNoOutlet = 6,
        [EnumMember]
        OmaContactAtMediaOutlet = 7,
        [EnumMember]
        OmaContactAtPrnOutlet = 8,
        [EnumMember]
        OmaContactAtOmaOutlet = 9
    }

    [DataContract]
    public enum SearchType
    {
        [EnumMember]
        Quick = 0,
        [EnumMember]
        Advanced = 1,
        [EnumMember]
        Outlet = 2,
        [EnumMember]
        ServiceList = 3,
        [EnumMember]
        QuickAdmin = 4
    };

    [DataContract]
    public enum SearchContext
    {
        [EnumMember]
        People = 0,
        [EnumMember]
        Outlet = 1,
        [EnumMember]
        Both = 2
    };

    [DataContract]
    public enum ContactStatus
    {
        [EnumMember]
        Active = 0,
        [EnumMember]
        Unassigned = 1,
        [EnumMember]
        Deleted = 2
    };

    /// <summary>
    /// Represents the three group types.
    /// </summary>
    [DataContract]
    public enum GroupType : int
    {
        [EnumMember]
        Search = 0,
        [EnumMember]
        List = 1,
        [EnumMember]
        Task = 2
    }

    //[DataContract]
    //public enum ShowResultType
    //{
    //    [EnumMember]
    //    [Description("All Records")]
    //    A = 0,
    //    [EnumMember]
    //    [Description("Media Directory Records")]
    //    M = 1,
    //    [EnumMember]
    //    [Description("Private Records")]
    //    P = 2
    //};

    /// <summary>
    /// Represents the different delete actions availabel from
    /// the grid.
    /// </summary>
    public enum DeleteAction
    {
        DeleteOne,
        DeleteSelected
    }

    /// <summary>
    /// Represents the different pinning actions available from
    /// the search grid.
    /// </summary>
    public enum PinAction
    {
        PinOne,
        UnpinOne,
        PinAll,
        UnpinAll,
        Clear
    }

    /// <summary>
    /// Represents the four states the checkboxes can be.
    /// </summary>
    public enum CheckboxState
    {
        All = 1,
        None = 2,
        PartAll = 3,
        PartNone = 4
    }

    /// <summary>
    /// Represents what is being added to a list or task.
    /// </summary>
    public enum PopupType : int
    {
        Search = 0,
        List = 1,
        Contact = 2
    }

    /// <summary>
    /// Represents the three roles.
    /// </summary>
    public enum MemberRole : int
    {
        SuperAdmin = 0,
        Admin = 1,
        User = 2
    }

    public enum Status : int
    {
        Inactive = 0,
        Active = 1,
        Trashed = 2
    }

    /// <summary>
    /// Represents the four states a task can be.
    /// </summary>
    [DataContract]
    public enum TaskStatus : int
    {
        [EnumMember]
        [Description("Not Started")]
        Not_Started = 0,
        [EnumMember]
        [Description("In Progress")]
        In_Progress = 1,
        [EnumMember]
        [Description("On Hold")]
        On_Hold = 2,
        [EnumMember]
        [Description("Completed")]
        Completed = 3
    }

    /// <summary>
    /// Represents the 4 different notification types for tasks.
    /// </summary>
    public enum NotificationType : int
    {
        [Description("Email")]
        Email = 0,
        [Description("SMS")]
        SMS = 1,
        [Description("Email and SMS")]
        Both = 2,
        [Description("Unknown")]
        Unknown = -1
    }

    public enum LocationType : int
    {
        Regional = 631,
        Metro = 630,
        Suburban = 632
    }

    /// <summary>
    /// Represents each of the reminder types for tasks.
    /// </summary>
    public enum ReminderType : int
    {
        FifteenMinutes = 0,
        ThirtyMinutes = 1,
        OneHour = 2,
        TwoHours = 3,
        FourHours = 4,
        OneDay = 5,
        TwoDays = 6,
        ThreeDays = 7,
        FiveDays = 8,
        OneWeek = 9,
        TwoWeeks = 10,
        ThreeWeeks = 11,
        OneMonth = 12
    }

    [DataContract]
    public enum PreferredDeliveryMethod : int
    {
        [CharValue('E'), Description("Email")]
        Email = 0,
        [CharValue('F'), Description("Fax")]
        Fax = 1,
        [CharValue('B'), Description("Facebook")]
        Facebook = 2,
        [CharValue('P'), Description("Phone")]
        Phone = 5,
        [CharValue('O'), Description("Phone")]
        Mobile = 6,
        [CharValue('T'), Description("Twitter")]
        Twitter = 7,
        [CharValue('S'), Description("Skype")]
        Skype = 8,
        [CharValue('I'), Description("Instagram")]
        Instagram = 9,
        [CharValue('L'), Description("LinkedIn")]
        LinkedIn = 4,
        [CharValue('M'), Description("Mail")]
        Mail = 3
    }

    /// <summary>
    /// Helper class for working with 'extended' enums using <see cref="CharValueAttribute"/> attributes.
    /// </summary>
    public class CharEnum
    {
        #region Instance implementation

        private Type _enumType;
        private static Hashtable _charValues = new Hashtable();

        /// <summary>
        /// Creates a new <see cref="CharEnum"/> instance.
        /// </summary>
        /// <param name="enumType">Enum type.</param>
        public CharEnum(Type enumType) {
            if (!enumType.IsEnum)
                throw new ArgumentException(String.Format("Supplied type must be an Enum.  Type was {0}", enumType.ToString()));

            _enumType = enumType;
        }

        /// <summary>
        /// Gets the char value associated with the given enum value.
        /// </summary>
        /// <param name="valueName">Name of the enum value.</param>
        /// <returns>Char Value</returns>
        public Char GetCharValue(char valueName) {
            Enum enumType;
            char CharValue = ' ';
            try {
                enumType = (Enum)Enum.Parse(_enumType, valueName.ToString());
                CharValue = GetCharValue(enumType);
            }
            catch (Exception) { }

            return CharValue;
        }

        /// <summary>
        /// Gets the char values associated with the enum.
        /// </summary>
        /// <returns>char value array</returns>
        public Array GetCharValues() {
            ArrayList values = new ArrayList();
            //Look for our char value associated with fields in this enum
            foreach (FieldInfo fi in _enumType.GetFields()) {
                //Check for our custom attribute
                CharValueAttribute[] attrs = fi.GetCustomAttributes(typeof(CharValueAttribute), false) as CharValueAttribute[];
                if (attrs.Length > 0)
                    values.Add(attrs[0].Value);

            }

            return values.ToArray();
        }

        /// <summary>
        /// Gets the values as a 'bindable' list datasource.
        /// </summary>
        /// <returns>IList for data binding</returns>
        public IList GetListValues() {
            Type underlyingType = Enum.GetUnderlyingType(_enumType);
            ArrayList values = new ArrayList();
            //Look for our char value associated with fields in this enum
            foreach (FieldInfo fi in _enumType.GetFields()) {
                //Check for our custom attribute
                CharValueAttribute[] attrs = fi.GetCustomAttributes(typeof(CharValueAttribute), false) as CharValueAttribute[];
                if (attrs.Length > 0)
                    values.Add(new DictionaryEntry(Convert.ChangeType(Enum.Parse(_enumType, fi.Name), underlyingType), attrs[0].Value));

            }

            return values;

        }

        /// <summary>
        /// Return the existence of the given Char value within the enum.
        /// </summary>
        /// <param name="CharValue">Char value.</param>
        /// <returns>Existence of the Char value</returns>
        public bool IsCharDefined(char CharValue) {
            return Parse(_enumType, CharValue) != null;
        }

        /// <summary>
        /// Return the existence of the given Char value within the enum.
        /// </summary>
        /// <param name="CharValue">Char value.</param>
        /// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied Char value</param>
        /// <returns>Existence of the char value</returns>
        public bool IsCharDefined(char charValue, bool ignoreCase) {
            return Parse(_enumType, charValue, ignoreCase) != null;
        }

        /// <summary>
        /// Gets the underlying enum type for this instance.
        /// </summary>
        /// <value></value>
        public Type EnumType {
            get { return _enumType; }
        }

        #endregion

        #region Static implementation

        /// <summary>
        /// Gets a char value for a particular enum value.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <returns>Char Value associated via a <see cref="CharValueAttribute"/> attribute, or null if not found.</returns>
        public static char GetCharValue(Enum value) {
            char output = ' ';
            Type type = value.GetType();

            if (_charValues.ContainsKey(value))
                output = (_charValues[value] as CharValueAttribute).Value;
            else {
                //Look for our 'CharValueAttribute' in the field's custom attributes
                FieldInfo fi = type.GetField(value.ToString());
                CharValueAttribute[] attrs = fi.GetCustomAttributes(typeof(CharValueAttribute), false) as CharValueAttribute[];
                if (attrs.Length > 0) {
                    _charValues.Add(value, attrs[0]);
                    output = attrs[0].Value;
                }

            }
            return output;

        }

        /// <summary>
        /// Parses the supplied enum and char value to find an associated enum value (case sensitive).
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="charValue">Char value.</param>
        /// <returns>Enum value associated with the char value, or null if not found.</returns>
        public static object Parse(Type type, char charValue) {
            return Parse(type, charValue, false);
        }

        /// <summary>
        /// Parses the supplied enum and char value to find an associated enum value.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="charValue">Char value.</param>
        /// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied char value</param>
        /// <returns>Enum value associated with the char value, or null if not found.</returns>
        public static object Parse(Type type, char charValue, bool ignoreCase) {
            object output = null;
            char enumCharValue = ' ';

            if (!type.IsEnum)
                throw new ArgumentException(String.Format("Supplied type must be an Enum.  Type was {0}", type.ToString()));

            //Look for our char value associated with fields in this enum
            foreach (FieldInfo fi in type.GetFields()) {
                //Check for our custom attribute
                CharValueAttribute[] attrs = fi.GetCustomAttributes(typeof(CharValueAttribute), false) as CharValueAttribute[];
                if (attrs.Length > 0)
                    enumCharValue = attrs[0].Value;

                //Check for equality then select actual enum value.
                if (string.Compare(enumCharValue.ToString(), charValue.ToString(), ignoreCase) == 0) {
                    output = Enum.Parse(type, fi.Name);
                    break;
                }
            }

            return output;
        }

        /// <summary>
        /// Return the existence of the given char value within the enum.
        /// </summary>
        /// <param name="charValue">Char value.</param>
        /// <param name="enumType">Type of enum</param>
        /// <returns>Existence of the char value</returns>
        public static bool IsCharDefined(Type enumType, char charValue) {
            return Parse(enumType, charValue) != null;
        }

        /// <summary>
        /// Return the existence of the given char value within the enum.
        /// </summary>
        /// <param name="charValue">Char value.</param>
        /// <param name="enumType">Type of enum</param>
        /// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied char value</param>
        /// <returns>Existence of the char value</returns>
        public static bool IsCharDefined(Type enumType, char charValue, bool ignoreCase) {
            return Parse(enumType, charValue, ignoreCase) != null;
        }

        #endregion
    }

    [DataContract]
    public enum TaskSortColumn
    {
        [EnumMember]
        [DbString("")]
        Default = 0,
        [EnumMember]
        [DbString("taskname")]
        TaskName = 1,
        [EnumMember]
        [DbString("statusid")]
        Status = 2,
        [EnumMember]
        [DbString("duedate")]
        Due = 3
    }

    [DataContract]
    public enum AllListsSortColumn
    {
        [EnumMember]
        [DbString("")]
        Default = 0,
        [EnumMember]
        [DbString("ListName")]
        ListName = 1,
        [EnumMember]
        [DbString("UserFullName")]
        UserFullName = 2,
        [EnumMember]
        [DbString("CreatedDate")]
        CreatedDate = 3
    }

    [DataContract]
    public enum AllSavedSearchesSortColumn
    {
        [EnumMember]
        [DbString("")]
        Default = 0,
        [EnumMember]
        [DbString("SearchName")]
        SearchName = 1,
        [EnumMember]
        [DbString("UserName")]
        UserName = 2,
        [EnumMember]
        [DbString("CreatedDate")]
        CreatedDate = 3
    }

    [DataContract]
    public enum Countries
    {
        [EnumMember]
        [Description("Australia")]
        Australia = 0,
        [EnumMember]
        [Description("New Zealand")]
        NewZealand = 1,
        [EnumMember]
        [Description("All Countries")]
        AllCountries = 2,
    }

    [DataContract]
    public enum Prefixes
    {
        [EnumMember]
        [Description("Mr")]
        Mr,
        [EnumMember]
        [Description("Ms")]
        Ms,
        [EnumMember]
        [Description("Mrs")]
        Mrs,
        [EnumMember]
        [Description("Miss")]
        Miss
    }

    public enum ProductTypeCategory
    {
        SatelliteBroadcast,
        Print,
        Website,
        PhotoDesk,
        Podcast,
        Government,
        Radio,
        Television,
        MediaOwner,
        App
    }

    /// <summary>
    /// Simple attribute class for storing Char Values
    /// </summary>
    public class CharValueAttribute : Attribute
    {
        public char Value { get; set; }

        /// <summary>
        /// Creates a new <see cref="CharValueAttribute"/> instance.
        /// </summary>
        /// <param name="value">The value.</param>
        public CharValueAttribute(char value) {
            Value = value;
        }
    }

    class DbStringAttribute : System.Attribute
    {
        #region Properties

        /// <summary>
        /// Holds the stringvalue for a value in an enum.
        /// </summary>
        public string DbStringValue { get; protected set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor used to init a StringValue Attribute
        /// </summary>
        /// <param name="value"></param>
        public DbStringAttribute(string value) {
            this.DbStringValue = value;
        }

        #endregion

    }
}
