﻿using System;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract(Name="SearchOptionsServiceList")]
    public class SearchOptionsServiceList : SearchOptions
    {
        [DataMember]
        public int? CategoryId { get; set; }
        [DataMember]
        public bool ShowFax { get; set; }
        [DataMember]
        public bool ShowEmail { get; set; }
        [DataMember]
        public int? ServiceListId { get; set; }
        [DataMember]
        public string SearchText { get; set; }

        public SearchOptionsServiceList()
            : base() {
            Type = SearchType.ServiceList;
            Context = SearchContext.Both;

            ShowFax = true;
            ShowEmail = true;
            SearchText = string.Empty;
        }

        #region Public Methods

        public override bool HasSearchCriteria {
            get {
                if (ShowFax || ShowEmail) {
                    if (CategoryId.HasValue && CategoryId.Value > 0)
                        return true;

                    if (!string.IsNullOrWhiteSpace(SearchText))
                        return true;

                    if (ServiceListId.HasValue && ServiceListId.Value > 0)
                        return true;
                }

                return false;
            }
        }

        public override bool SearchWithDB {
            get {
                return true;
            }
        }

        #endregion
    }

}