﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract(Name="SearchOptionsQuick")]
    public class SearchOptionsQuick : SearchOptions
    {
        [DataMember]
        public bool SearchAustralia { get; set; }
        [DataMember]
        public bool SearchNewZealand { get; set; }
        [DataMember]
        public bool SearchAllCountries { get; set; }

        [DataMember]
        public List<Location> LocationList { get; set; }
        [DataMember]
        public List<int> PositionList { get; set; }
        [DataMember]
        public List<int> MediaTypeList { get; set; }
        [DataMember]
        public List<int> SubjectList { get; set; }
        [DataMember]
        public List<int> SubjectGroupList { get; set; }
        [DataMember]
        public string SearchText { get; set; }
        [DataMember]
        public string SelectedId { get; set; }
        [DataMember]
        public RecordType SelectedRecordType { get; set; }

        public SearchOptionsQuick()
            : base() {
            Type = SearchType.Quick;

            SearchAustralia = true;
            SearchNewZealand = false;
            SearchAllCountries = false;

            LocationList = null;
            PositionList = null;
            MediaTypeList = null;
            SubjectList = null;
            SubjectGroupList = null;
            SearchText = string.Empty;
            SelectedId = null;
            SelectedRecordType = RecordType.Unknown;
        }

        #region Public Methods

        public override bool HasSearchCriteria {
            get {
                if (SearchAustralia || SearchNewZealand || SearchAllCountries)
                    return ((LocationList != null && LocationList.Count > 0) ||
                            (PositionList != null && PositionList.Count > 0) ||
                            (MediaTypeList != null && MediaTypeList.Count > 0) ||
                            (SubjectList != null && SubjectList.Count > 0) ||
                            (SubjectGroupList != null && SubjectGroupList.Count > 0) ||
                            !string.IsNullOrWhiteSpace(SearchText));
                else
                    return false;
            }
        }

        public override bool SearchWithDB {
            get {
                return false;
            }
        }

        public static List<Location> ParseLocation(string pValue) {
            return ParseLocation(pValue, '#');
        }

        public static List<Location> ParseLocation(string pValue, char separator) {
            List<Location> result = new List<Location>();

            if (!string.IsNullOrEmpty(pValue)) {
                string[] locList = pValue.Split(separator);
                Location loc;
                string state;
                int pos;

                // The location criteria is stored something like - ACT/Suburban#NSW/Metro#NSW/Regional#
                foreach (string s in locList) {
                    pos = s.IndexOf('/');

                    // Only process if we found a /. Sometimes states are found by themselves
                    // but it is redundant because you then get the state with all regions.
                    if (pos >= 0 && s.Length > pos) {
                        state = s.Substring(0, pos).ToUpper();

                        // Look for this state in the list. If it's not there add it.
                        loc = FindOrAddLocationToList(result, state);

                        // Add the region to the state.
                        AddFocusIDToLocation(loc, s.Substring(pos + 1));
                    }
                }
            }
            return result;
        }

        public static string GetLocation(List<Location> pLocList) {
            string result = string.Empty;

            if (pLocList != null) {
                for (int i = 0; i < pLocList.Count; i++) {
                    if (pLocList[i].RegionalFocusList != null && pLocList[i].RegionalFocusList.Count > 0) {
                        foreach (string focus in pLocList[i].RegionalFocusList)
                            result += pLocList[i].State + "/" + focus + "#";
                    }
                }
            }
            return result;
        }

        #endregion

        #region Private Methods

        private static Location FindOrAddLocationToList(List<Location> pLocList, string pState) {
            List<Location> locList = (from l in pLocList where l.State == pState select l).ToList();
            Location loc;

            if (locList == null || locList.Count < 1) {
                // We didn't find it so create a new Location.
                loc = new Location();
                loc.State = pState;
                loc.RegionalFocusList = new List<string>();

                // Add it to the list.
                pLocList.Add(loc);
            }
            else {
                // We found it already so return it.
                loc = locList[0];
            }
            return loc;
        }

        private static void AddFocusIDToLocation(Location pLoc, string pFocusName) {
            // If the regional focus name doesn't already exist then add it.
            if (!pLoc.RegionalFocusList.Any(c => c == pFocusName))
                pLoc.RegionalFocusList.Add(pFocusName);
        }

        #endregion
    }

}