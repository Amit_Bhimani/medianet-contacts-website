﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    public static class EnumHelper
    {
        /// <summary>
        /// Get the int value repesentation of an emum.
        /// </summary>
        /// <param name="enumValue">The input enum.</param>
        /// <returns>The integer representation of the enum.</returns>
        public static int ToInt(this Enum enumValue) {
            return Convert.ToInt32(enumValue);
        }

        /// <summary>
        /// Get the int value representation as a string.
        /// </summary>
        /// <param name="enumValue">The input enum.</param>
        /// <returns>The string representation of the int value.</returns>
        public static string ToIntString(this Enum enumValue) {
            return enumValue.ToInt().ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Returns the corresponding contact type that can belong to this outlet type.
        /// </summary>
        /// <param name="outletType">The type of the outlet.</param>
        /// <returns>The type of the contacts that can belong to this outlet.</returns>
        public static RecordType ContactRecordType(this RecordType outletType) {
            if (outletType == RecordType.MediaOutlet)
                return RecordType.MediaContact;

            if (outletType == RecordType.PrnOutlet)
                return RecordType.PrnContact;

            if (outletType == RecordType.OmaOutlet)
                return RecordType.OmaContactAtOmaOutlet;
            
            throw new InvalidEnumArgumentException("No contact type to return for given outlet type " +
                                                   outletType.ToString() + ".");
        }


        /// <summary>
        /// Retrieve the description on the enum, e.g.
        /// [Description("Bright Pink")]
        /// BrightPink = 2,
        /// Then when you pass in the enum, it will retrieve the description
        /// </summary>
        /// <param name="en">The Enumeration</param>
        /// <returns>A string representing the friendly name</returns>
        public static string GetDescription(Enum en) {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0) {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0) {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }

        public static string GetDbStringValue(this Enum value) {
            // Get the type
            Type type = value.GetType();

            // Get fieldinfo for this type
            FieldInfo fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            DbStringAttribute[] attribs = fieldInfo.GetCustomAttributes(
                typeof(DbStringAttribute), false) as DbStringAttribute[];

            // Return the first if there was a match.
            return attribs.Length > 0 ? attribs[0].DbStringValue : null;
        }

        public static bool IsNullableEnum(this Type t) {
            Type u = Nullable.GetUnderlyingType(t);
            return (u != null) && u.IsEnum;
        }

        public static ContactStatus ContactStatusToEnum(string value) {
            if (value != null) {
                if (value.Equals("D"))
                    return ContactStatus.Deleted;
                else if (value.Equals("U"))
                    return ContactStatus.Unassigned;
            }

            return ContactStatus.Active;
        }
    }
}
