﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [DataContract(Name="ServiceFault", Namespace = "http://MedianetContacts/services")]
    public class ServiceFault
    {
        private string message;
        private IDictionary<string, string> data;
        private Guid id;

        public ServiceFault() {
        }

        public ServiceFault(string message) {
            MessageText = message;
        }

        [DataMember]
        public string MessageText {
            get { return message; }
            set { message = value; }
        }

        [DataMember]
        public IDictionary<string, string> Data {
            get { return data; }
            set { data = value; }

        }
        [DataMember]
        public Guid Id {
            get { return id; }
            set { id = value; }
        }
    }
}
