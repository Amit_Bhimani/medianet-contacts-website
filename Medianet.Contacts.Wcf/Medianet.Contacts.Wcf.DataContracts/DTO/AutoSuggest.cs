﻿using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [DataContract]
    public class AutoSuggest
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public RecordType RecordType { get; set; }
        [DataMember]
        public string Id { get; set; }

        public int Sort { get; set; }
    }
}