﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Medianet.Contacts.Wcf.DataContracts.DTO
{
    [Serializable]
    [DataContract]
    public class Location
    {
        [DataMember]
        public string State { get; set; }
        [DataMember]        
        public List<string> RegionalFocusList { get; set; }

        public Location() {
            State = string.Empty;
            RegionalFocusList = null;
        }

        public override string ToString() {
            string result = State;
            string focus;

            // Convert to STATE:FocusId#FocusId#FocusId
            if (RegionalFocusList != null && RegionalFocusList.Count > 0) {
                result += ":";

                for (int i = 0; i < RegionalFocusList.Count; i++) {
                    if (i != 0)
                        result += "#";

                    focus = RegionalFocusList[i].ToLower();

                    switch (focus) {
                        case "metro":
                            result += "629#630"; // International#National
                            break;
                        case "suburban":
                            result += "632#633"; // Local#Community
                            break;
                        case "regional":
                            result += "631"; // Regional
                            break;
                    }
                }
            }
            return result;
        }
    }
}