﻿using System;
using System.Text.RegularExpressions;
using System.Threading;
using Medianet.Contacts.Wcf.Service.Common;
using NLog;

namespace Medianet.Contacts.Wcf.ConsoleIndexer
{
    public class Program
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public static void Main(string[] args) {
            while (true) {
                Console.WriteLine("Please select one of the following options: ");
                Console.WriteLine("[1] Rebuild full index.");
                Console.WriteLine("[2] Exit.");

                while (Console.KeyAvailable == false)
                    Thread.Sleep(250);

                var key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.D1 || key.Key == ConsoleKey.NumPad1)
                    Index();
                else if (key.Key == ConsoleKey.D2 || key.Key == ConsoleKey.NumPad2) {
                    Console.WriteLine("Bye.");
                    break;
                }
                else {
                    Console.WriteLine("Please enter a valid option.");
                    Console.WriteLine();
                }
            }
        }

        private static void Index() {
            AutoMapperConfigurator.Configure();

            var take = 0;

            var tasks = ReadNumber("Please enter the number of tasks to use.");
            if (tasks > 0) take = ReadNumber("Please enter the number of records to retrieve from the database at a time.");

            if (tasks > 0 && take > 0) {
                try {
                    var manager = new SolrIndexerManager(take, tasks);
                    manager.Rebuild();
                }
                catch (Exception e) {
                    _logger.DebugException("Exception was raised.", e);
                }
            }
        }

        /// <summary>
        /// Reads numeric input from the command line.
        /// </summary>
        /// <returns>A number read from the command line. If -1 is returned, exit has been entered.</returns>
        private static int ReadNumber(string message = "Please enter the number of worker threads you would like to use and then press return.") {
            var regex = new Regex(@"^\d+");
            var result = -1;

            Console.WriteLine(message);
            Console.WriteLine("To return to the main menu enter X.");

            while (true) {
                var input = Console.ReadLine();
                if (input != null && regex.IsMatch(input)) {
                    result = int.Parse(input);

                    if (result > 0)
                        break;

                    Console.WriteLine("Please enter a value greater than 0.");
                }
                else if (input == "x" || input == "X")
                    break;
                else
                    Console.WriteLine("Please enter a numeric value. To return to the main menu enter X.");
            }
            return result;
        }
    }
}
