﻿# Medianet.Contacts.ConsoleIndexer

A .NET Console application to one-off index Medianet Contacts in to Solr.

It simply,  

1. Deletes all documents in Solr
2. Then, inserts all documents in Solr

## Documentation

### Basic usage

A typical run of Console Indexer might involve the following steps:

1. Turn off ["SolrUpdate" key](http://git.dev.aap/medianet/Medianet.Contacts.WCF/blob/master/Medianet.Contacts.ServiceHost/app.config#L160) and ["UseIndex" key](http://git.dev.aap/medianet/Medianet.Contacts.WCF/blob/master/Medianet.Contacts.ServiceHost/app.config#L155) values in the [Medianet Contacts WCF] (http://git.dev.aap/medianet/Medianet.Contacts.WCF/blob/master/Medianet.Contacts.ServiceHost/app.config) config file. *This does not require restart*
2. Truncate all de-normalized tables- ```dbo.contact and dbo.prn_contact``` on the Medianet database
3. Run all the stored procedures to de-normalize data on the Medianet Database. *Order of stored procedure is irrelevant*
 * ```exec denormalize_aap_contacts.sql```  
 ```exec denormalize_aap_outlets.sql```  
 ```exec denormalize_private_contacts.sql```  
 ```exec denormalize_private_outlets.sql```  
 ```exec denormalize_prn_contacts.sql```  	 
 ```exec denormalize_prn_outlets.sql```  
  
4. Update ```is_dangling = false and is_dirty = false``` for all the rows in the de-normalized tables. Although there shouldn't be any row with is_dirty = true if you have followed the instruction correctly, this is just to emphasize that Console Indexer ignores these values. It will simply insert all the rows in tables as new documents in Solr
5. Running the Console Indexer application:
 1. Type in [1] to select "Rebuild full index". *Please note that you do not need to press return to feed in the value for __this step only__*
 2. Choose the numbers of tasks as 5 (*in TPL*) 
 3. Choose row size as 2000 (*skip size in EF*)
6. Should be good to test that all worked well by checking [Solr admin web page statistics](http://solr1:8080/solr/admin/stats.jsp) for the correct number of indexed documents 

Full indexing should normally take 15-20 minutes to run.