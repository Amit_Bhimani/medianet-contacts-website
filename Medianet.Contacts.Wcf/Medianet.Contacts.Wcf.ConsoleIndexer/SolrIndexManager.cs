﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Solr;
using Medianet.Contacts.Wcf.Service.Solr.Tasks;
using SolrNet;

namespace Medianet.Contacts.Wcf.ConsoleIndexer
{
    public class SolrIndexerManager : SolrBase
    {
        // Default number of records to take from the database.
        private readonly int _take = 10000;

        // Default number of actions to spawn.
        private readonly int _workers = 5;

        private readonly BlockingCollection<ITask> _queue;
        private readonly Task[] _tasks;

        /// <summary>
        /// Constructor for the indexer.
        /// </summary>
        /// <param name="take">The number of records to read from the database at a time.</param>
        /// <param name="workers">The number of workers to create.</param>
        public SolrIndexerManager(int take, int workers) {
            _take = take;
            _workers = workers;
            _tasks = new Task[_workers];
            _queue = new BlockingCollection<ITask>();
        }

        /// <summary>
        /// Recreates the index from scratch.
        /// </summary>
        public void Rebuild() {
            var sw = Stopwatch.StartNew();

            Console.WriteLine("Building index...");
            
            // Start tasks.
            for (int i = 0; i < _workers; i++)
                _tasks[i] = Task.Factory.StartNew(Execute);

            // Delete the old index first.
            Solr.Delete(SolrQuery.All);

            // Read data from the DB and add to the queue.
            int skip = 0;
            int take = _take;
            bool finished = false;

            while (!finished) {
                using (var context = new MedianetContactsContext()) {
                    var partition = context.Contacts.OrderBy(x => x.Id).Skip(skip).Take(take).ToList();
                    _queue.Add(new AddTask(partition));

                    skip += take;
                    finished = partition.Count == 0;
                }
            }

            // Block the main thread until all workers have finished then commit the changes.
            _queue.CompleteAdding();
            Task.WaitAll(_tasks.Where(x => x != null && !x.IsCompleted).ToArray());

            // Commit and optimize changes.
            OptimizeWithExpunge();
            
            Console.WriteLine("Building the full index took {0} minutes.", (int)sw.Elapsed.TotalMinutes);
        }

        /// <summary>
        /// POST the next available document to Solr.
        /// </summary>
        private void Execute() {
            foreach (var doc in _queue.GetConsumingEnumerable())
                doc.Execute();
        }
    }
}