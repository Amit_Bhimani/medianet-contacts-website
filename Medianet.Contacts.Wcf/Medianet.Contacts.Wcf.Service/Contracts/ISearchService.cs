﻿using System.Collections.Generic;
using System.ServiceModel;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Wcf.Service.Contracts
{
    [ServiceContract(Namespace = "http://MedianetContacts/services")]
    public interface ISearchService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<SearchResult> Search(
            out int totalResults,
            SearchOptions so,
            SortColumn sortCol,
            SortDirection sortDir,
            bool newSearch,
            int userid,
            int? startPos = null,
            int? count = null,
            List<RecordIdentifier> pinned = null,
            List<RecordIdentifier> deleted = null,
            List<RecordIdentifier> selected = null
        );

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<SearchResult> SearchWithExpandedOutlets(
            out int totalResults,
            SearchOptions searchOptions,
            SortColumn sortColumn,
            SortDirection sortDirection,
            int userid,
            int? startPos = null,
            int? count = null,
            List<RecordIdentifier> pinned = null,
            List<RecordIdentifier> deleted = null,
            List<RecordIdentifier> unselected = null
        );

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<SearchResult> FetchRange(
            out int totalResults,
            List<SearchResult> results,
            int startPos,
            int count,
            List<RecordIdentifier> pinned,
            List<RecordIdentifier> deleted,
            List<RecordIdentifier> unselected,
            int userid
        );

        [OperationContract]
        List<AutoSuggest> AutoSuggest(
            SearchContext context,
            string searchText,
            bool searchAustralia,
            bool searchNewZealand,
            bool searchAllCountries,
            int maxResults,
            int userid
        );

        [OperationContract]
        List<AutoSuggest> AutoSuggestComplex(
            AutoSuggestOptions options,
            int maxResults,
            int userid
        );

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<SearchResult> SearchEmployees(
            out int totalResults,
            SearchOptions searchOptions,
            int userid,
            int startPos,
            int maxResults
        );


    }
}
