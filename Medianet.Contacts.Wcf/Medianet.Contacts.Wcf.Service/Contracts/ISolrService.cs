﻿using System.ServiceModel;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Wcf.Service.Contracts
{
    [ServiceContract(Namespace = "http://MedianetContacts/services")]
    public interface ISolrService
    {
        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        bool UpdateContact(RecordIdentifier record);
    }
}
