﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using ComLib.Caching;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Wcf.Service.Caching
{
    public class SearchDataCache
    {
        public const string CacheKeySearchPrefix = "Search_";

        static public void Insert(string key, List<SearchResult> results, SearchOptions so, SortColumn sortCol, SortDirection sortDir) {
            SearchCache cacheResult = new SearchCache();
            string cacheKey = GetCacheKey(key);

            cacheResult.SearchOptionsHash = HashSearchOptions(so);
            cacheResult.Results = results;
            cacheResult.SortCol = sortCol;
            cacheResult.SortDir = sortDir;

            Cacher.Insert(cacheKey, cacheResult);
        }

        static public SearchCache Get(string key, SearchOptions so) {
            string cacheKey = GetCacheKey(key);
            SearchCache cacheResult;

            // Get the search results and hash value from the cache for this users last search.
            cacheResult = (SearchCache)Cacher.Get(cacheKey);

            // If it's not the same hash value as the current search then we ned to perform the search again.
            if (cacheResult != null && cacheResult.SearchOptionsHash == HashSearchOptions(so))
                return cacheResult;
            else
                return null;
        }

        static public void Remove(string key) {
            Cacher.Remove(GetCacheKey(key));
        }

        #region Private methods

        static private string GetCacheKey(string key) {
            return CacheKeySearchPrefix + key;
        }

        static private string HashSearchOptions(SearchOptions so) {
            StringBuilder sBuilder = new StringBuilder();
            string input;
            MD5 md5Hash = MD5.Create();
            byte[] data;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            input = serializer.Serialize(so);
            data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            for (int i = 0; i < data.Length; i++) {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        #endregion
    }
}