﻿using System;
using System.Globalization;
using System.Linq;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Contracts;
using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using NLog;

namespace Medianet.Contacts.Wcf.Service.Solr
{
    public class SolrService : ISolrService
    {
        private readonly Indexer _solrIndexer;
        private readonly Logger _logger;

        public SolrService() {
            // The windows service host normally does this but we do it here when running in debug mode.
            AutoMapperConfigurator.Configure();

            _solrIndexer = ServiceLocator.Resolve<Indexer>();
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Updates a single contact in Solr.
        /// </summary>
        /// <param name="record">The record to update.</param>
        /// <returns>True on success, false otherwise.</returns>
        public bool UpdateContact(RecordIdentifier record) {
            try {
                ContactModel contact;
                using (var context = new MedianetContactsContext()) {
                    contact = (from x in context.Contacts
                               where
                                   (record.ContactId == null || record.ContactId == "0"
                                        ? x.ContactId == null
                                        : x.ContactId == record.ContactId.Trim()) &&
                                   (record.OutletID == null
                                        ? x.OutletId == null
                                        : x.OutletId == record.OutletID.Trim()) &&
                                   x.RecordType == (byte) record.Type
                               select x).SingleOrDefault();
                }

                if (contact == null) {
                    _logger.Debug(
                        "Tried to update private contact {0}#{1}#{2} but record was not present in denormalised table.",
                        record.OutletID, record.ContactId, record.Type.ToIntString());
                }
                else {
                    if (contact.IsDangling) {
                        _logger.Debug("Deleting private contact {0}#{1}#{2} from index.", record.OutletID,
                                      record.ContactId, record.Type.ToIntString());
                        _solrIndexer.Solr.Delete(contact.Id.ToString(CultureInfo.InvariantCulture));
                        _solrIndexer.SoftCommit();
                    }
                    else if (contact.IsDirty) {
                        var doc = Mapper.Map<ContactModel, SolrContact>(contact);

                        if (doc != null) {
                            _logger.Debug("Updating private contact {0}#{1}#{2} in index.", record.OutletID,
                                          record.ContactId, record.Type.ToIntString());

                            _solrIndexer.Update(doc);
                            _solrIndexer.SoftCommit();
                        }
                        else {
                            _logger.Debug(
                                "Tried to update private contact {0}#{1}#{2} but record was not present in the Solr index.",
                                record.OutletID,
                                record.ContactId, record.Type.ToIntString());
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex) {
                _logger.ErrorException("Exception was raised.", ex);
                return false;
            }
        }
    }
}
