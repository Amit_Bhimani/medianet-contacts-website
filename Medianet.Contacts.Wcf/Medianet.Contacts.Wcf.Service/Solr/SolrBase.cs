﻿using System;
using System.Collections.Generic;
using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using SolrNet;

namespace Medianet.Contacts.Wcf.Service.Solr
{
    public class SolrBase
    {
        private ISolrOperations<SolrContact> _solr = null;
        public ISolrOperations<SolrContact> Solr {
            get { return _solr ?? (_solr = ServiceLocator.Resolve<ISolrOperations<SolrContact>>()); }
        }

        /// <summary>
        /// Commit changes to Solr now.
        /// </summary>
        public void Commit() {
            Solr.Commit();
        }

        public void SoftCommit()
        {
            Func<string, string, KeyValuePair<string, string>> kv = (k, v) => new KeyValuePair<string, string>(k, v);
            var connection = ServiceLocator.Resolve<ISolrConnection>();
            connection.Get("/update", new[] {
                kv("commit", "true"),
                kv("softCommit", "true")
            });
        }

        /// <summary>
        /// Optimize the Solr index.
        /// </summary>
        public void Optimize() {
            Solr.Optimize();
        }

        /// <summary>
        /// Commit and optimize the index.
        /// </summary>
        public static void OptimizeWithExpunge() {
            Func<string, string, KeyValuePair<string, string>> kv = (k, v) => new KeyValuePair<string, string>(k, v); 
            var connection = ServiceLocator.Resolve<ISolrConnection>();
            connection.Get("/update", new[] {
                kv("commit", "true"),
                kv("optimize", "true"),
                kv("expungeDeletes", "true")
            });
        }
    }
}