﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using SolrNet;
using SolrNet.Impl;
using SolrNet.Mapping;

namespace Medianet.Contacts.Wcf.Service.Solr
{
    public class SolrConfigurator
    {
        public static IMappingManager GetManager() {
            var mgr = new MappingManager();

            // Meta.
            var pk = GetProperty<SolrContact>(c => c.Id);
            mgr.Add(pk, Fields.ID);
            mgr.SetUniqueKey(pk);

            mgr.Add(GetProperty<SolrContact>(c => c.CreatedDate), Fields.CREATED_DATE);
            mgr.Add(GetProperty<SolrContact>(c => c.LastModified), Fields.LAST_MODIFIED);
            mgr.Add(GetProperty<SolrContact>(c => c.IsDangling), Fields.IS_DANGLING);
            mgr.Add(GetProperty<SolrContact>(c => c.IsDirty), Fields.IS_DIRTY);
            mgr.Add(GetProperty<SolrContact>(c => c.IsOutlet), Fields.IS_OUTLET);
            mgr.Add(GetProperty<SolrContact>(c => c.IsAap), Fields.IS_AAP);
            mgr.Add(GetProperty<SolrContact>(c => c.IsPrivate), Fields.IS_PRIVATE);

            // Contact
            mgr.Add(GetProperty<SolrContact>(c => c.RecordType), Fields.RECORD_TYPE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactId), Fields.CONTACT_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.FirstName), Fields.FIRST_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.LastName), Fields.LAST_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.MiddleName), Fields.MIDDLE_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.FullName), Fields.FULL_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.JobTitle), Fields.JOB_TITLE);
            mgr.Add(GetProperty<SolrContact>(c => c.PositionIds), Fields.POSITION_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.IsPrimaryContact), Fields.IS_PRIMARY_CONTACT);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactPreferredDeliveryMethod), Fields.CONTACT_PREFERRED_DELIVERY_METHOD);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactSubjectIds), Fields.CONTACT_SUBJECT_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactSubjectGroupIds), Fields.CONTACT_SUBJECT_GROUP_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactSubjectNames), Fields.CONTACT_SUBJECT_NAMES);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactSystemNotes), Fields.CONTACT_SYSTEM_NOTES);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactTwitter), Fields.CONTACT_TWITTER);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactLinkedIn), Fields.CONTACT_LINKEDIN);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactFacebook), Fields.CONTACT_FACEBOOK);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactInstagram), Fields.CONTACT_INSTAGRAM);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactYouTube), Fields.CONTACT_YOUTUBE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactSnapchat), Fields.CONTACT_SNAPCHAT);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactSkype), Fields.CONTACT_SKYPE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactBio), Fields.CONTACT_BIO);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactHasEmail), Fields.CONTACT_HAS_EMAIL);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactHasFax), Fields.CONTACT_HAS_FAX);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactHasPostal), Fields.CONTACT_HAS_POSTAL);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactCityName), Fields.CONTACT_CITY_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactContinentId), Fields.CONTACT_CONTINENT_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactCountryId), Fields.CONTACT_COUNTRY_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactStateId), Fields.CONTACT_STATE_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactStateName), Fields.CONTACT_STATE_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactCityId), Fields.CONTACT_CITY_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactPostcode), Fields.CONTACT_POSTCODE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactBugBears), Fields.CONTACT_BUG_BEARS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactAlsoKnownAs), Fields.CONTACT_ALSO_KNOWN_AS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactPressReleaseInterests), Fields.CONTACT_PRESS_RELEASE_INTERESTS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactPersonalInterests), Fields.CONTACT_PERSONAL_INTERESTS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactAppearsIn), Fields.CONTACT_APPEARS_IN);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactMediaInfluencerScore), Fields.CONTACT_MEDIA_INFLUENCER_SCORE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactCurrentStatus), Fields.CONTACT_CURRENT_STATUS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactBasedInLocation), Fields.CONTACT_BASED_IN_LOCATION);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactAlternativeEmailAddress), Fields.CONTACT_ALTERNATIVE_EMAIL_ADDRESS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactWebsite), Fields.CONTACT_WEBSITE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactOooStartDate), Fields.CONTACT_OOO_START_DATE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactOooReturnDate), Fields.CONTACT_OOO_RETURN_DATE);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactPhoneNumber), Fields.CONTACT_PHONE_NUMBER);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactMobileNumber), Fields.CONTACT_MOBILE_NUMBER);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactEmailAddress), Fields.CONTACT_EMAIL_ADDRESS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactPriorityNotice), Fields.CONTACT_PRIORITY_NOTICE);

            // Outlet
            mgr.Add(GetProperty<SolrContact>(c => c.OutletId), Fields.OUTLET_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactIds), Fields.CONTACT_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.ContactNames), Fields.CONTACT_NAMES);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletName), Fields.OUTLET_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPreferredDeliveryMethod), Fields.OUTLET_PREFERRED_DELIVERY_METHOD);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletSubjectIds), Fields.OUTLET_SUBJECT_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletSubjectGroupIds), Fields.OUTLET_SUBJECT_GROUP_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletSubjectNames), Fields.OUTLET_SUBJECT_NAMES);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletSystemNotes), Fields.OUTLET_SYSTEM_NOTES);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletTwitter), Fields.OUTLET_TWITTER);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletLinkedIn), Fields.OUTLET_LINKEDIN);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletFacebook), Fields.OUTLET_FACEBOOK);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletInstagram), Fields.OUTLET_INSTAGRAM);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletYouTube), Fields.OUTLET_YOUTUBE);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletSnapchat), Fields.OUTLET_SNAPCHAT);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletSkype), Fields.OUTLET_SKYPE);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletBio), Fields.OUTLET_BIO);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletHasEmail), Fields.OUTLET_HAS_EMAIL);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletHasFax), Fields.OUTLET_HAS_FAX);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletHasPostal), Fields.OUTLET_HAS_POSTAL);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletIsOffice), Fields.OUTLET_IS_OFFICE);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletIsFinancial), Fields.OUTLET_IS_FINANCIAL);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletIsPolitician), Fields.OUTLET_IS_POLITICIAN);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletContinentId), Fields.OUTLET_CONTINENT_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletCountryId), Fields.OUTLET_COUNTRY_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletStateId), Fields.OUTLET_STATE_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletStateName), Fields.OUTLET_STATE_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletCityName), Fields.OUTLET_CITY_NAME);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletCityId), Fields.OUTLET_CITY_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPostcode), Fields.OUTLET_POSTCODE);

            mgr.Add(GetProperty<SolrContact>(c => c.OutletPostalCityId), Fields.OUTLET_POSTAL_CITY_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPostalCountryId), Fields.OUTLET_POSTAL_COUNTRY_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPostalContinentId), Fields.OUTLET_POSTAL_CONTINENT_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletSubscriptionOnlyPublication), Fields.OUTLET_SUBSCRIPTION_ONLY_PUBLICATION);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletAlsoKnownAs), Fields.OUTLET_ALSO_KNOWN_AS);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletAdditionalWebsite), Fields.OUTLET_ADDITIONAL_WEBSITE);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPublishedOn), Fields.OUTLET_PUBLISHED_ON);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPressReleaseInterests), Fields.OUTLET_PRESS_RELEASE_INTERESTS);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletRegionsCovered), Fields.OUTLET_REGIONS_COVERED);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPhoneNumber), Fields.OUTLET_PHONE_NUMBER);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletEmailAddress), Fields.OUTLET_EMAIL_ADDRESS);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletWebsite), Fields.OUTLET_WEBSITE);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletLogoFilename), Fields.OUTLET_LOGO_FILENAME);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletIsGeneric), Fields.OUTLET_IS_GENERIC);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletPriorityNotice), Fields.OUTLET_PRIORITY_NOTICE);
            
            // Common
            mgr.Add(GetProperty<SolrContact>(c => c.MediaTypeId), Fields.MEDIA_TYPE_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.MediaType), Fields.MEDIA_TYPE);
            mgr.Add(GetProperty<SolrContact>(c => c.FrequencyId), Fields.FREQUENCY_ID);
            mgr.Add(GetProperty<SolrContact>(c => c.Circulation), Fields.CIRCULATION);
            mgr.Add(GetProperty<SolrContact>(c => c.LanguageIds), Fields.LANGUAGE_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.LanguageNames), Fields.LANGUAGE_NAMES);
            mgr.Add(GetProperty<SolrContact>(c => c.StationFrequency), Fields.STATION_FREQUENCY);
            mgr.Add(GetProperty<SolrContact>(c => c.NewsFocusIds), Fields.NEWS_FOCUS_IDS);
            mgr.Add(GetProperty<SolrContact>(c => c.OwnerDebtorNumber), Fields.OWNER_DEBTOR_NUMBER);
            mgr.Add(GetProperty<SolrContact>(c => c.OutletOrFullName), Fields.TEXT_OUTLET_OR_FULLNAME);
            mgr.Add(GetProperty<SolrContact>(c => c.TextFullSearch), Fields.TEXT_FULL_SEARCH);

            // Sorting.
            mgr.Add(GetProperty<SolrContact>(c => c.Location), Fields.LOCATION);
            mgr.Add(GetProperty<SolrContact>(c => c.Subjects), Fields.SUBJECTS);
            mgr.Add(GetProperty<SolrContact>(c => c.Score), Fields.RANK_SCORE);

            return mgr;
        }

        /// <summary>
        /// Compile time safe method to retrieve property info from a class.
        /// </summary>
        /// <typeparam name="TEntity">The type.</typeparam>
        /// <param name="expression">The lambda specifying the property.</param>
        /// <returns>A property info object representing the property specified in the lambda.</returns>
        public static PropertyInfo GetProperty<TEntity>(Expression<Func<TEntity, object>> expression) {
            MemberExpression memberExpression = null;
            if (expression.Body.NodeType == ExpressionType.Convert) {
                var body = (UnaryExpression)expression.Body;
                memberExpression = body.Operand as MemberExpression;
            }
            else if (expression.Body.NodeType == ExpressionType.MemberAccess) {
                memberExpression = expression.Body as MemberExpression;
            }

            if (memberExpression == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    expression.ToString()));

            return memberExpression.Member as PropertyInfo;
        }

        /// <summary>
        /// Get's the Solr connection.
        /// </summary>
        /// <returns>The SolrNet connection object.</returns>
        public static ISolrConnection GetSolrConnection() {
            var url = Config.SolrUrl();
            
            var conn = new SolrConnection(url);
            return new SolrConnection(url);
        }
    }
}