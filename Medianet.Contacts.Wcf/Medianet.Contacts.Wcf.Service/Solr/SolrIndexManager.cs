﻿using System;
using System.CodeDom;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Solr.Tasks;
using NLog;

namespace Medianet.Contacts.Wcf.Service.Solr
{
    public class SolrIndexManager
    {
        private readonly int _take;
        private readonly int _workers;
        private Timer _timer;

        private static object _padlock;
        private int _danglingCount;
        private int _dirtyCount;

        private BlockingCollection<ITask> _queue;
        private readonly Task[] _tasks;
        private readonly Stopwatch _stopWatch;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private StringBuilder _errors;

        /// <summary>
        /// Constructs a manager to manage the threads for updating the index.
        /// </summary>
        /// <param name="workers">The number of threads to use in total.</param>
        /// <param name="take">The number of rows to read from the database at a time.</param>
        public SolrIndexManager(int workers, int take) {
            _take = take;
            _workers = workers;
            _tasks = new Task[_workers];
            _padlock = new object();
            _stopWatch = new Stopwatch();
            _queue = new BlockingCollection<ITask>();
            _errors = new StringBuilder();
        }

        /// <summary>
        /// Generate tasks.
        /// </summary>
        /// <remarks>
        /// Use a mutex here to prevent more than one thread to enter
        /// this method. This can happen when not all records have been read and the timer
        /// starts a new thread.
        /// </remarks>
        private void GenerateTasks(object state) {
            // Block any other threads from entering.
            if (Monitor.TryEnter(_padlock)) {
                try {
                    // Process only when we're between the hours in the config.
                    var now = DateTime.Now.TimeOfDay;
                    if (now > Config.SolrProcessFrom() && now < Config.SolrProcessTo()) {
                        _stopWatch.Reset();
                        _stopWatch.Start();

                        // Clear previous Errors
                        _errors.Clear();
                        // Clear update table.
                        MedianetContactsContext.TruncateIndexContacts();

                        try
                        {
                            // Denormalise aap contacts and outlets.
                            using (var context = new MedianetContactsContext())
                            {
                                var dirtyContacts = context.MediaContacts.Where(m => m.IsDirty == true).ToList();
                                foreach (var mediaContactModel in dirtyContacts)
                                {
                                    try
                                    {
                                        context.Database.ExecuteSqlCommand(
                                            $"exec denormalize_aap_contacts {mediaContactModel.Id}");
                                        mediaContactModel.IsDirty = false;
                                        context.Entry(mediaContactModel).State = EntityState.Modified;
                                        context.SaveChanges();
                                    }
                                    catch (Exception contactEx)
                                    {
                                        _errors.AppendLine($"Failed to denormalize aap contact {mediaContactModel.Id}. Exception {contactEx}");
                                    }
                                }
                                var dirtyOutlets = context.MediaOutlets.Where(m => m.IsDirty == true).ToList();
                                foreach (var mediaOutletModel in dirtyOutlets)
                                {
                                    try
                                    {
                                        context.Database.ExecuteSqlCommand(
                                            $"exec denormalize_aap_outlet {mediaOutletModel.Id}");
                                        mediaOutletModel.IsDirty = false;
                                        context.Entry(mediaOutletModel).State = EntityState.Modified;
                                        context.SaveChanges();
                                    }
                                    catch (Exception outletException)
                                    {
                                        _errors.AppendLine($"Failed to denormalize aap outlet {mediaOutletModel.Id}. Exception {outletException}");
                                    }
                                }
                            }
                        }
                        catch (Exception ex1)
                        {
                            _logger.ErrorException("Exception was raised trying to denormalize aap contacts and outlets.", ex1);
                            _errors.AppendLine($"Failed to Denormalise Contacts or Outlets. Exception : {ex1}");
                            // We need to continue to update any denormalised data to solr... Hence this catch..
                        }

                        // Start workers.
                        StartConsumers();

                        int skip = 0;
                        int take = _take;
                        bool finished = false;

                        _logger.Debug("Generating delete tasks.");
                        while (!finished) {
                            using (var context = new MedianetContactsContext()) {
                                var partition = context.Contacts
                                    .Where(x => x.IsDangling)
                                    .OrderBy(x => x.Id).Skip(skip).Take(take).ToList();

                                if (partition.Count > 0) {
                                    _queue.Add(new DeleteTask(partition));
                                    _danglingCount += partition.Count;
                                }

                                skip += partition.Count;
                                finished = partition.Count == 0;
                            }
                        }

                        // Block until consumers are done.
                        WaitConsumers();
                        MedianetContactsContext.DeleteDangling();
                        _logger.Debug("Finished delete tasks. There were {0} dangling.", _danglingCount);

                        // Restart consumers.
                        StartConsumers();

                        finished = false;
                        skip = 0;

                        _logger.Debug("Generating update tasks.");
                        while (!finished) {
                            using (var context = new MedianetContactsContext()) {
                                var partition = context.Contacts
                                    .Where(x => x.IsDirty && !x.IsDangling)
                                    .OrderBy(x => x.Id).Skip(skip).Take(take).ToList();

                                if (partition.Count > 0) {
                                    _queue.Add(new UpdateTask(partition));
                                    _dirtyCount += partition.Count;
                                }

                                skip += partition.Count;
                                finished = partition.Count == 0;
                            }
                        }

                        // Block until consumers are done.
                        WaitConsumers();
                        MedianetContactsContext.UpdateDirty();

                        // Commit and optimize changes.
                        SolrBase.OptimizeWithExpunge();
                        _stopWatch.Stop();

                        _logger.Debug("Finished update tasks. There were {0} dirty.", _dirtyCount);

                        if (_dirtyCount > 0 || _danglingCount > 0)
                            _logger.Info("Updating index took {0} minutes.",
                                         (int)_stopWatch.Elapsed.TotalMinutes);
                        else
                            _logger.Debug("Nothing to update in index.");

                        _dirtyCount = 0;
                        _danglingCount = 0;
                    }
                    else
                        _logger.Debug("Not in time slot allowed for processing. Skipping generate tasks.");
                }
                catch (EntityCommandExecutionException e1) {
                    if (e1.InnerException != null && e1.InnerException is SqlException)
                    {
                        if (((SqlException) e1.InnerException).Number == -2)
                            _logger.Debug("SQL server timed out. Timer will restart.");
                        else
                        {
                            _logger.ErrorException("Exception was raised trying to generate tasks.", e1);
                            _errors.AppendLine($"Exception was raised trying to generate tasks e1. : {e1}");
                        }
                    }
                    else
                    {
                        _logger.ErrorException("Exception was raised trying to generate tasks.", e1);
                        _errors.AppendLine($"Exception was raised trying to generate tasks e1. : {e1}");
                    }
                }
                catch (Exception e2) {
                    _logger.ErrorException("Exception was raised trying to generate tasks.", e2);
                    _errors.AppendLine($"Exception was raised trying to generate tasks e2. : {e2}");
                }
                finally {

                }
                if (_errors.Length > 0)
                {
                    // Send Error email 
                    SendErrorEmail();
                }
                Monitor.Exit(_padlock);
                
            }
        }

        /// <summary>
        /// Checks to see if the manager is currently running.
        /// </summary>
        /// <returns>True if running false otherwise.</returns>
        public bool IsRunning() {
            return _timer != null;
        }

        /// <summary>
        /// Start updating the Solr index periodically.
        /// </summary>
        public void Start() {
            _logger.Info("Starting index update manager.");
            try {
                _timer = new Timer(GenerateTasks, null, TimeSpan.FromMinutes(Config.SolrUpdateInterval()),
                                   TimeSpan.FromMinutes(Config.SolrUpdateInterval()));
            }
            catch (Exception e) {
                _logger.ErrorException("Exception was raised trying to start timer.", e);
            }
            finally {
                _logger.Info("Index update manager started.");
            }
        }

        /// <summary>
        /// Stop all threads and timers.
        /// </summary>
        public void Stop() {
            try {
                _logger.Info("Stopping index update manager.");

                if (_timer != null) {
                    _timer.Dispose();
                    _timer = null;
                }

                if (!_queue.IsAddingCompleted) _queue.CompleteAdding();
                Task.WaitAll(_tasks.Where(x => x != null && !x.IsCompleted && !x.IsCanceled && !x.IsFaulted).ToArray());

                // Dispose and recreate the queue.
                if (_queue != null) _queue.Dispose();
                _queue = new BlockingCollection<ITask>();
            }
            catch (AggregateException errors) {
                errors.Handle(e => e is TaskCanceledException);
            }
            catch (Exception e2) {
                _logger.ErrorException("Exception was raised trying to stop the index manager.", e2);
            }
            finally {
                SolrBase.OptimizeWithExpunge();
                _logger.Info("Index update manager stopped.");
            }
        }

        /// <summary>
        /// Execute a task.
        /// </summary>
        private void Execute() {
            try {
                foreach (var doc in _queue.GetConsumingEnumerable()) {
                    doc.Execute();

                    using (var context = new MedianetContactsContext()) {
                        if (doc is UpdateTask) {
                            var update = doc.Contacts.Select(x => new IndexUpdateModel {
                                ContactId = x.Id,
                                LastModified = x.LastModified
                            }).ToList();

                            update.ForEach(c => context.IndexUpdateContacts.Add(c));
                            context.SaveChanges();

                        }
                        else if (doc is DeleteTask) {
                            var update = doc.Contacts.Select(x => new IndexDeleteModel {
                                ContactId = x.Id,
                                LastModified = x.LastModified
                            }).ToList();

                            update.ForEach(c => context.IndexDeleteContacts.Add(c));
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e) {
                _logger.ErrorException("Exception was raised executing a task.", e);
            }
        }

        /// <summary>
        /// Used to observe exceptions thrown within tasks.
        /// </summary>
        /// <param name="t">The task to observe.</param>
        private void Continue(Task t) {
            if (t.Exception == null) return;
            foreach (var e in t.Exception.Flatten().InnerExceptions)
            {
                _logger.ErrorException("Unhandled exception was raised in a task.", e);
                _errors.AppendLine($"Unhandled exception was raised in a task. : {e}");
            }
        }

        /// <summary>
        /// Start consumers.
        /// </summary>
        private void StartConsumers() {
            // Start required tasks.
            for (var i = 0; i < _workers; i++)
                _tasks[i] = Task.Factory
                    .StartNew(Execute)
                    .ContinueWith(Continue, TaskContinuationOptions.OnlyOnFaulted);
        }

        /// <summary>
        /// Block caller until consumers are completed. Once they are complete create a
        /// new consumer queue.
        /// </summary>
        private void WaitConsumers() {
            try {
                // Complete adding so consumers will end once their task is completed.
                _queue.CompleteAdding();

                // Wait on all tasks.
                Task.WaitAll(_tasks.Where(x => x != null && !x.IsCanceled && !x.IsCompleted && !x.IsFaulted).ToArray());
            }
            catch (AggregateException errors) {
                errors.Handle(e => e is TaskCanceledException);
            }
            catch (ObjectDisposedException) { }
            catch (Exception e) {
                _logger.ErrorException("Exception was raised.", e);
                _errors.AppendLine($"Exception was raised in WaitConsumers : {e}");
            }

            // Dispose and recreate the queue.
            if (_queue != null) _queue.Dispose();
            _queue = new BlockingCollection<ITask>();
        }

        private void SendErrorEmail()
        {
            if (_errors.Length > 0)
            {
                try
                {
                    var solrurl = ConfigurationManager.AppSettings["SolrUrl"];
                    _errors.Insert(0, $"The following errors occured when indexing data to Solr \r\n solr : {solrurl} running on {Environment.MachineName} \r\n");
                    var subject = "Error  syncing data to Solr ";
                    EmailHelper.SendEmail(ConfigurationManager.AppSettings["MailToAddresses"], subject,_errors.ToString(),false);

                }
                catch (Exception ex)
                {
                    _logger.ErrorException($"Failed to send error email , Content {_errors.ToString()}", ex);
                }
            }
        }

    }
}
