﻿using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using NLog;

namespace Medianet.Contacts.Wcf.Service.Solr.Tasks
{
    public class DeleteTask : ITask
    {
        private readonly ICollection<ContactModel> _contacts;

        public ICollection<ContactModel> Contacts {
            get { return _contacts; }
        }

        private readonly Logger _logger;
        private readonly Indexer _indexer;

        /// <summary>
        /// Create's a task to delete the collection of _contacts to the Solr index.
        /// </summary>
        /// <param name="contacts">The collection of _contacts to delete from the index.</param>
        public DeleteTask(ICollection<ContactModel> contacts) {
            _contacts = contacts;
            _indexer = ServiceLocator.Resolve<Indexer>();
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Create's a task to delete a single contact from the Solr index.
        /// </summary>
        /// <param name="contact">The single contact to delete from the index.</param>
        public DeleteTask(ContactModel contact) : this(new List<ContactModel> { contact }) { }

        /// <summary>
        /// Execute the task.
        /// </summary>
        public void Execute() {
            if (_contacts.IsNullOrEmpty()) return;

            var docs = _contacts.Select(Mapper.Map<ContactModel, SolrContact>).ToList();
            _logger.Debug("Deleting {0} documents.", docs.Count);
            _indexer.Delete(docs.ToArray());
        }
    }
}
