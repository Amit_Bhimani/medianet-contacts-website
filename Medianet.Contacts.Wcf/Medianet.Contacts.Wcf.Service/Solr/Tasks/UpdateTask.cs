﻿using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using NLog;

namespace Medianet.Contacts.Wcf.Service.Solr.Tasks
{
    public class UpdateTask : ITask
    {
        private readonly ICollection<ContactModel> _contacts;

        public ICollection<ContactModel> Contacts {
            get { return _contacts; }
        }

        private readonly Logger _logger;
        private readonly Indexer _indexer;

        /// <summary>
        /// Create's a task to add the collection of contacts to the Solr index.
        /// </summary>
        /// <param name="contacts">The collection of contacts to add to the index.</param>
        public UpdateTask(ICollection<ContactModel> contacts) {
            _contacts = contacts;
            _indexer = ServiceLocator.Resolve<Indexer>();
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Create's a task to add a single contact to the Solr index.
        /// </summary>
        /// <param name="contact">The single contact to add to the index.</param>
        public UpdateTask(ContactModel contact) : this(new List<ContactModel> { contact }) { }

        /// <summary>
        /// Execute the task.
        /// </summary>
        public void Execute() {
            if (_contacts.IsNullOrEmpty()) return;

            var docs = _contacts.Select(Mapper.Map<ContactModel, SolrContact>).ToList();
            _logger.Debug("Updating {0} documents.", docs.Count);
            _indexer.Update(docs.ToArray());
        }
    }
}
