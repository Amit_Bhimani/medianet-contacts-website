﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.Model;

namespace Medianet.Contacts.Wcf.Service.Solr.Tasks
{
    public interface ITask
    {
        ICollection<ContactModel> Contacts { get; }
        void Execute();
    }
}
