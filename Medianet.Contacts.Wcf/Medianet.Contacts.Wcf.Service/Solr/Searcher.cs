﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AAP.CSharp.Parsers;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using NLog;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.DSL;

namespace Medianet.Contacts.Wcf.Service.Solr
{
    public class Searcher : SolrBase
    {
        // Query container.
        private readonly QueryOptions _options;

        // All filter queries for the current search.
        private readonly List<ISolrQuery> _filters;

        // All queries for the current search.
        private readonly List<ISolrQuery> _queries;

        // Consolidated query.
        private ISolrQuery _query;
        private string _debtorNo;

        /// <summary>
        /// Used by <see cref="SolrBooleanExpressionParser"/> to log info and errors.
        /// </summary>
        private Logger _logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        public Searcher()
        {
            _filters = new List<ISolrQuery>();
            _queries = new List<ISolrQuery>();
            _options = new QueryOptions() { Rows = 5000, Fields = { "*", "score" } };
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Search for a single contact.
        /// </summary>
        /// <param name="record">The record to search.</param>
        /// <returns>A single instance of a SolrContact.</returns>
        public SolrContact Search(RecordIdentifier record)
        {
            ISolrQuery query;
            if (record.IsOutlet)
            {
                query = Query.Field(Fields.OUTLET_ID).Is(record.OutletID.TrimIfNotNull()) &&
                        Query.Field(Fields.RECORD_TYPE).Is(record.Type.ToIntString());
            }
            else
            {
                var queries = new List<ISolrQuery>();
                if (!record.OutletID.IsNullOrWhitespace())
                    queries.Add(Query.Field(Fields.OUTLET_ID).Is(record.OutletID.TrimIfNotNull()));
                if (!record.ContactId.IsNullOrWhitespace())
                    queries.Add(Query.Field(Fields.CONTACT_ID).Is(record.ContactId.TrimIfNotNull()));

                Query.Field(Fields.RECORD_TYPE).Is(record.Type.ToIntString());
                query = new SolrMultipleCriteriaQuery(queries, "AND");
            }

            var result = Solr.Query(query);

            if (!result.IsNullOrEmpty())
                return result.First();

            return null;
        }

        /// <summary>
        /// Given a search options object, perform a search on the index.
        /// </summary>
        /// <param name="criteria">The search criteria.</param>
        /// <param name="debtorNo">The current's debtor id.</param>
        /// <returns>A list of search result objects.</returns>
        public List<SearchResult> Search(SearchOptions criteria, string debtorNo)
        {
            _debtorNo = debtorNo;

            if (criteria is SearchOptionsQuick)
                BuildQuickSearch(criteria as SearchOptionsQuick);
            else if (criteria is SearchOptionsAdvanced)
                BuildAdvancedSearch(criteria as SearchOptionsAdvanced);
            else if (criteria is SearchOptionsOutlet)
                return SearchOutlet(criteria as SearchOptionsOutlet);

            var results = Solr.Query(_query, _options);
            var mapped = ParallelMap<SolrContact, SearchResult>(results);
            return mapped.ToList();
        }

        /// <summary>
        /// Retrieve records by record identifiers.
        /// </summary>
        /// <param name="records">The list of records.</param>
        /// <param name="debtorNumber">The current user id.</param>
        /// <returns></returns>
        public List<SearchResult> Search(List<RecordIdentifier> records, string debtorNumber)
        {
            _debtorNo = debtorNumber;

            List<AbstractSolrQuery> queries = new List<AbstractSolrQuery>();

            foreach (var item in records)
            {
                if (!string.IsNullOrEmpty(item.ContactId) && item.ContactId != "0")
                    queries.Add(Query.Field(Fields.CONTACT_ID).Is(item.ContactId) &&
                                Query.Field(Fields.OUTLET_ID).Is(item.OutletID) &&
                                Query.Field(Fields.RECORD_TYPE).Is(item.Type.ToIntString()));
                else
                    queries.Add(Query.Field(Fields.OUTLET_ID).Is(item.OutletID) &&
                              Query.Field(Fields.RECORD_TYPE).Is(item.Type.ToIntString()));
            }


            var filter = new List<ISolrQuery> { BuildPermissionFilter() };
            var options = new QueryOptions { FilterQueries = filter };

            var results = new BlockingCollection<SolrContact>();
            queries.Partition(100).AsParallel().ForAll(x =>
            {
                var matches = Solr.Query(new SolrMultipleCriteriaQuery(x, "OR"), options);
                if (matches.Count > 0)
                    matches.AsParallel().ForEach(results.Add);
            });

            // Map results in parallel.
            var mapped = ParallelMap<SolrContact, SearchResult>(results);
            return mapped.ToList();
        }

        /// <summary>
        /// Returns results for the auto suggest.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="searchAustralia">if set to <c>true</c> [search australia].</param>
        /// <param name="searchNewZealand">if set to <c>true</c> [search new zealand].</param>
        /// <param name="searchAllCountries">if set to <c>true</c> [search all countries].</param>
        /// <param name="maxResults">The max results.</param>
        /// <param name="debtorNo">The user's debtor number.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<AutoSuggest> AutoSuggest(
            SearchContext context,
            string searchText,
            bool searchAustralia,
            bool searchNewZealand,
            bool searchAllCountries,
            int maxResults,
            string debtorNo
        )
        {
            _debtorNo = debtorNo;
            _filters.Add(BuildPermissionFilter());
            var contextFilter = BuildContextFilter(context);
            if (contextFilter != null)
            {
                _filters.Add(contextFilter);
            }
            _filters.Add(BuildDataModulesFilter(context));

            List<AutoSuggest> suggests = null;
            var textQuery = BuildAutoSuggestTextQuery(context, searchText.TrimIfNotNull());
            var regionQuery = BuildRegionQuery(context, searchAustralia, searchNewZealand, searchAllCountries);

            if (textQuery != null) _queries.Add(textQuery);
            if (regionQuery != null) _queries.Add(regionQuery);

            if (!_queries.IsNullOrEmpty())
            {
                _query = new SolrMultipleCriteriaQuery(_queries, "AND");
                _options.FilterQueries = _filters;
                _options.Fields.Add(Fields.IS_OUTLET);
                _options.Fields.Add(Fields.OUTLET_NAME);
                _options.Fields.Add(Fields.FULL_NAME);
                _options.Fields.Add(Fields.RECORD_TYPE);
                _options.Fields.Add(Fields.CONTACT_ID);
                _options.Fields.Add(Fields.OUTLET_ID);
                _options.Rows = maxResults * 2;


                var results = Solr.Query(_query, _options);

                // Remove duplicates.
                suggests = results
                    .Select(Mapper.Map<SolrContact, AutoSuggest>)
                    .Distinct(new KeyEqualityComparer<AutoSuggest>(x => x.Name + x.Id))
                    .Take(maxResults)
                    .ToList();

                // Create sort order.
                suggests.ForEach(x =>
                {
                    if (x.Name.Equals(searchText, StringComparison.OrdinalIgnoreCase))
                        x.Sort = 1;
                    else if (x.Name.StartsWith(searchText, StringComparison.OrdinalIgnoreCase))
                        x.Sort = 2;
                    else if (x.Name.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) > 0)
                        x.Sort = 3;
                    else
                        x.Sort = 4;
                });

                // Sort.
                suggests = suggests.OrderBy(x => x.Sort).ThenBy(x => x.Name).ToList();
            }
            return suggests;
        }

        /// <summary>
        /// Create a Solr query from an advanced search.
        /// </summary>
        /// <param name="options">The SearchOptionsAdvanced object.</param>
        public void BuildAdvancedSearch(SearchOptionsAdvanced options)
        {
            var context = options.Context;

            #region Filters

            // Context.
            // Show only AAP records.
            if (options.ShowMediaDirRecords && !options.ShowPrivateRecords)
            {
                if (context == SearchContext.Both)
                    _filters.Add(Query.Field(Fields.IS_AAP).Is(true));
                else if (context == SearchContext.People)
                    _filters.Add(Query.Field(Fields.RECORD_TYPE).Is(RecordType.MediaContact.ToIntString()));
                else if (context == SearchContext.Outlet)
                    _filters.Add(Query.Field(Fields.RECORD_TYPE).Is(RecordType.MediaOutlet.ToIntString()));
            }
            // Show only private records.
            else if (options.ShowPrivateRecords && !options.ShowMediaDirRecords)
            {
                if (context == SearchContext.People)
                    _filters.Add(new SolrQueryInList(Fields.RECORD_TYPE, new string[] {
                        RecordType.OmaContactAtMediaOutlet.ToIntString(),
                        RecordType.OmaContactAtOmaOutlet.ToIntString(),
                        RecordType.OmaContactAtPrnOutlet.ToIntString(),
                        RecordType.OmaContactNoOutlet.ToIntString()
                    }));
                else if (context == SearchContext.Outlet)
                    _filters.Add(Query.Field(Fields.RECORD_TYPE).Is(RecordType.OmaOutlet.ToIntString()));

                // Just filter out everything but private records.
                _filters.Add(Query.Field(Fields.OWNER_DEBTOR_NUMBER).Is(_debtorNo));

            }
            // All selected.
            else
            {
                if (context == SearchContext.People) _filters.Add(Query.Field(Fields.IS_OUTLET).Is(false));
                if (context == SearchContext.Outlet) _filters.Add(Query.Field(Fields.IS_OUTLET).Is(true));
                _filters.Add(BuildPermissionFilter());
            }

            // Data modules.
            _queries.Add(BuildDataModulesFilter(context));

            #endregion

            if (!options.SearchText.IsNullOrWhitespace())
            {
                var textQuery = BuildAllTextQuery(context, options.SearchText);
                if (textQuery != null) _queries.Add(textQuery);
            }

            #region People

            if (!options.FirstName.IsNullOrWhitespace()) _queries.Add(BuildTextQuery(options.FirstName, Fields.FIRST_NAME));
            if (!options.LastName.IsNullOrWhitespace()) _queries.Add(BuildTextQuery(options.LastName, Fields.LAST_NAME));
            if (!options.FullName.IsNullOrWhitespace()) _queries.Add(BuildTextQuery(options.FullName, Fields.FULL_NAME));
            if (!options.JobTitle.IsNullOrWhitespace()) _queries.Add(BuildTextQuery(options.JobTitle, Fields.JOB_TITLE));
            if (options.PrimaryNewsContactOnly) _queries.Add(Query.Field(Fields.IS_PRIMARY_CONTACT).Is(options.PrimaryNewsContactOnly));

            // Positions
            if (!options.PositionList.IsNullOrEmpty())
                _queries.Add(BuildIdQuery(Fields.POSITION_IDS, options.PositionList));

            // Media Influencer Score
            if (!options.ContactInfluencerScore.IsNullOrEmpty() && options.ContactInfluencerScore.StartsWith(">"))
            {
                var score = Convert.ToInt32(options.ContactInfluencerScore.Replace(">", "").Trim());
                _queries.Add(new SolrQueryByRange<int?>(Fields.CONTACT_MEDIA_INFLUENCER_SCORE,
                    score + 1,
                    null));
            }
            #endregion

            #region Outlet

            if (!options.OutletName.IsNullOrWhitespace()) _queries.Add(BuildBooleanTextQuery(options.OutletName, Fields.OUTLET_NAME));

            if (!options.MediaTypeList.IsNullOrEmpty())
                _queries.Add(new SolrQueryInList(Fields.MEDIA_TYPE_ID, options.MediaTypeList.ConvertAll(((x => x.ToString())))));

            if (!options.OutletFrequencyList.IsNullOrEmpty())
                _queries.Add(new SolrQueryInList(Fields.FREQUENCY_ID, options.OutletFrequencyList.ConvertAll(((x => x.ToString())))));

            // Circulation
            if (options.OutletCirculationMin != 0 || options.OutletCirculationMax != 0)
                _queries.Add(new SolrQueryByRange<int?>(Fields.CIRCULATION,
                    options.OutletCirculationMin > 0 ? options.OutletCirculationMin as int? : null,
                    options.OutletCirculationMax > 0 ? options.OutletCirculationMax as int? : null));

            // Language
            if (!options.OutletLanguageList.IsNullOrEmpty())
                _queries.Add(new SolrQueryInList(Fields.LANGUAGE_IDS, options.OutletLanguageList.ConvertAll(((x => x.ToString())))));

            // Station Frequency
            if (!options.OutletStationFrequencyList.IsNullOrEmpty())
                _queries.Add(new SolrQueryInList(Fields.STATION_FREQUENCY, options.OutletStationFrequencyList.ConvertAll(((x => x.ToString())))));
            // Regions
            if ((options.IncludeMetroOutlets & options.IncludeSuburbanOutlets & options.IncludeRegionalOutlets) !=
                (options.IncludeMetroOutlets | options.IncludeSuburbanOutlets | options.IncludeRegionalOutlets))
            {
                var region = new List<ISolrQuery>();
                if (options.IncludeMetroOutlets)
                {
                    region.Add(Query.Field(Fields.NEWS_FOCUS_IDS).Is("629"));
                    region.Add(Query.Field(Fields.NEWS_FOCUS_IDS).Is("630"));
                }
                if (options.IncludeSuburbanOutlets)
                {
                    region.Add(Query.Field(Fields.NEWS_FOCUS_IDS).Is("632"));
                    region.Add(Query.Field(Fields.NEWS_FOCUS_IDS).Is("633"));
                }
                if (options.IncludeRegionalOutlets)
                    region.Add(Query.Field(Fields.NEWS_FOCUS_IDS).Is("631"));
                if (!region.IsNullOrEmpty()) _queries.Add(new SolrMultipleCriteriaQuery(region, "OR"));
            }

            #endregion

            #region Common

            // Subjects
            var subjects = BuildAdvancedSubjectQuery(options, context);
            if (subjects != null) _queries.Add(subjects);

            // Geographic
            var geography = BuildGeographyQuery(options, context);
            if (geography != null) _queries.Add(geography);

            #endregion

            #region Inclusions & Exclusions

            // Prefers

            var prefers = new List<ISolrQuery>();
            if (context == SearchContext.People || context == SearchContext.Both)
            {
                if (options.PrefersEmail)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("E"));
                if (options.PrefersFax)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("F"));
                if (options.PrefersMail)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("M"));
                if (options.PrefersPhone)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("P"));
                if (options.PrefersMobile)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("O"));
                if (options.PrefersTwitter)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("T"));
                if (options.PrefersLinkedIn)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("L"));
                if (options.PrefersFacebook)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("B"));
                if (options.PrefersInstagram)
                    prefers.Add(Query.Field(Fields.CONTACT_PREFERRED_DELIVERY_METHOD).Is("I"));
            }
            if (context == SearchContext.Outlet || context == SearchContext.Both)
            {
                if (options.PrefersEmail)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("E"));
                if (options.PrefersFax)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("F"));
                if (options.PrefersMail)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("M"));
                if (options.PrefersPhone)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("P"));
                if (options.PrefersMobile)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("O"));
                if (options.PrefersTwitter)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("T"));
                if (options.PrefersLinkedIn)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("L"));
                if (options.PrefersFacebook)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("B"));
                if (options.PrefersInstagram)
                    prefers.Add(Query.Field(Fields.OUTLET_PREFERRED_DELIVERY_METHOD).Is("I"));
               }

            if (prefers.Count > 0)
            {
                _queries.Add(new SolrMultipleCriteriaQuery(prefers, "OR"));
            }

            // Social
            if (context == SearchContext.People)
            {
                var people = BuildSocialPeopleQuery(options);
                if (people != null) _queries.Add(people);
            }
            else if (context == SearchContext.Outlet)
            {
                var outlet = BuildSocialOutletQuery(options);
                if (outlet != null) _queries.Add(outlet);
            }
            else if (context == SearchContext.Both)
            {
                var social = new List<ISolrQuery>();
                var people = BuildSocialPeopleQuery(options);
                var outlet = BuildSocialOutletQuery(options);

                if (people != null) social.Add(people);
                if (outlet != null) social.Add(outlet);
                if (!social.IsNullOrEmpty()) _queries.Add(new SolrMultipleCriteriaQuery(social, "OR"));
            }

            // Exclusion
            if (options.ExcludeFinanceInst)
                _queries.Add(Query.Field(Fields.OUTLET_IS_FINANCIAL).Is(!options.ExcludeFinanceInst));
            if (options.ExcludeOPS)
                _queries.Add(Query.Field(Fields.OUTLET_IS_OFFICE).Is(!options.ExcludeOPS));
            if (options.ExcludePoliticians)
                _queries.Add(Query.Field(Fields.OUTLET_IS_POLITICIAN).Is(!options.ExcludePoliticians));

            if (options.ExcludeOutOfOffice)
            {
                var oooQuery = new List<ISolrQuery>();
                var todaysDateTime = DateTime.Now;
                var todaysDate = new DateTime(todaysDateTime.Year, todaysDateTime.Month, todaysDateTime.Day);

                oooQuery.Add(new SolrQueryByRange<DateTime?>(Fields.CONTACT_OOO_START_DATE, null, todaysDate));
                oooQuery.Add(new SolrQueryByRange<DateTime?>(Fields.CONTACT_OOO_RETURN_DATE, todaysDate.AddDays(1), null));

                _queries.Add(new SolrMultipleCriteriaQuery(oooQuery, "AND").Not());
            }

            // Exclude words.
            if (!options.ExcludeKeywords.IsNullOrWhitespace())
            {
                var excludeKeywords = new List<ISolrQuery>();

                excludeKeywords.AddRange(options.ExcludeKeywords.Split(' ')
                                             .Where(s => !s.IsNullOrWhitespace())
                                             .Select(s => Query.Field(Fields.OUTLET_NAME).Is(s).Not()));

                if (context == SearchContext.People)
                {
                    excludeKeywords.AddRange(options.ExcludeKeywords.Split(' ')
                                                 .Where(s => !s.IsNullOrWhitespace())
                                                 .Select(s => Query.Field(Fields.FULL_NAME).Is(s).Not()));

                    excludeKeywords.AddRange(options.ExcludeKeywords.Split(' ')
                             .Where(s => !s.IsNullOrWhitespace())
                             .Select(s => Query.Field(Fields.JOB_TITLE).Is(s).Not()));
                }
                if (!excludeKeywords.IsNullOrEmpty()) _queries.AddRange(excludeKeywords);
            }

            // System notes.
            if (!options.SystemNotes.IsNullOrWhitespace())
            {
                var notesQuery = BuildSystemNotesQuery(context, options.SystemNotes);

                if (notesQuery != null)
                    _queries.Add(notesQuery);
            }

            #endregion

            _query = _queries.Count == 0 ? SolrQuery.All : new SolrMultipleCriteriaQuery(_queries, "AND");
            _options.FilterQueries = _filters;
        }


        /// <summary>
        /// Search Solr full Name field with RecordType 2 ( media contact) for auto suggest.
        /// </summary>
        /// <param name="searchOptions"></param>
        /// <param name="startPos"></param>
        /// <param name="maxResults"></param>
        /// <param name="totalResults"></param>
        /// <returns></returns>
        public List<SearchResult> SearchEmployee(SearchOptions searchOptions, int startPos, int maxResults, out int totalResults)
        {
            var textNameQuery = BuildTextQuery(searchOptions.SearchName, Fields.FULL_NAME);
            var recordType = Query.Field(Fields.RECORD_TYPE).Is(RecordType.MediaContact.ToIntString());

            var q = new SolrMultipleCriteriaQuery(new[] {
                textNameQuery,
                recordType
            }, "AND");

            _options.Rows = maxResults;
            _options.Start = startPos;
            var results = Solr.Query(q, _options);

            totalResults = results.NumFound;
            // Map results in parallel.
            var mapped = ParallelMap<SolrContact, SearchResult>(results);
            return mapped.ToList();

        }

        /// <summary>
        /// Create a Solr query from a quick search.
        /// </summary>
        /// <param name="quick">The SearchOptionsQuick object.</param>
        /// <returns>A instance of a SolrQueryContainer..</returns>
        private void BuildQuickSearch(SearchOptionsQuick quick)
        {
            #region Filters

            // Context
            var contextFilter = BuildContextFilter(quick.Context);
            if (contextFilter != null) _filters.Add(contextFilter);

            // Only show private contacts that belong to this company.
            _filters.Add(BuildPermissionFilter());

            // Only show contacts that customer has access to.
            _filters.Add(BuildDataModulesFilter(quick.Context));

            #endregion

            #region Queries

            // Subjects
            var subjects = BuildQuickSubjectQuery(quick);
            if (subjects != null) _queries.Add(subjects);

            // Position
            if (quick.Context != SearchContext.Outlet && !quick.PositionList.IsNullOrEmpty())
                _queries.Add(BuildIdQuery(Fields.POSITION_IDS, quick.PositionList));

            // Media type.
            if (!quick.MediaTypeList.IsNullOrEmpty())
                _queries.Add(BuildIdQuery(Fields.MEDIA_TYPE_ID, quick.MediaTypeList));

            // Region
            var region = BuildRegionQuery(quick.Context, quick.SearchAustralia, quick.SearchNewZealand,
                                          quick.SearchAllCountries);
            if (region != null) _queries.Add(region);

            // Location
            if (!quick.LocationList.IsNullOrEmpty())
                _queries.Add(BuildLocationQuery(quick));

            // Search text.
            if (!string.IsNullOrWhiteSpace(quick.SelectedId) && quick.SelectedRecordType != RecordType.Unknown)
            {
                var idQuery = BuildContactOrOutletIdQuery(quick.Context, quick.SelectedId, quick.SelectedRecordType);
                if (idQuery != null) _queries.Add(idQuery);
            }
            else
            {
                var textQuery = BuildAllTextQuery(quick.Context, quick.SearchText);
                if (textQuery != null) _queries.Add(textQuery);
            }

            #endregion

            _query = _queries.IsNullOrEmpty()
                ? SolrQuery.All
                : new SolrMultipleCriteriaQuery(_queries, "AND");

            _options.FilterQueries = _filters;
        }

        /// <summary>
        /// Searches an outlet.
        /// </summary>
        /// <param name="options">The SearchOptionsOutlet object.</param>
        /// <returns>A list of SearchResults.</returns>
        private List<SearchResult> SearchOutlet(SearchOptionsOutlet options)
        {
            List<SearchResult> results = null;

            // Let's get the outlet itself first.
            var query = Query.Field(Fields.OUTLET_ID).Is(options.OutletID) &&
                        Query.Field(Fields.RECORD_TYPE).Is((int)options.OutletType);

            var outlet = Solr.Query(query).FirstOrDefault();


            if (outlet != null && !outlet.ContactIds.IsNullOrEmpty())
            {
                var qOptions = new QueryOptions
                {
                    FilterQueries = new List<ISolrQuery> {
                        BuildPermissionFilter(),
                        BuildDataModulesFilter(SearchContext.People)
                    },
                    Rows = Constants.MaxRows
                };

                var queries =
                    outlet.ContactIds
                        .Select(x => new RecordIdentifier(options.OutletID, x, options.OutletType.ContactRecordType()))
                        .Select(r => Query.Field(Fields.CONTACT_ID).Is(r.ContactId) &&
                                     Query.Field(Fields.OUTLET_ID).Is(r.OutletID) &&
                                     Query.Field(Fields.RECORD_TYPE).Is(r.Type.ToIntString()))
                        .ToList();

                var matches = new BlockingCollection<SolrContact>();
                queries.Partition(100).AsParallel().ForAll(x =>
                {
                    var match =
                        Solr.Query(new SolrMultipleCriteriaQuery(x, "OR"), qOptions);
                    if (match.Count > 0)
                        match.AsParallel().ForEach(matches.Add);
                });

                results = ParallelMap<SolrContact, SearchResult>(matches).ToList();
            }
            return results.IsNullOrEmpty() ? new List<SearchResult>() : results;
        }

        /// <summary>
        /// Build region query.
        /// </summary>
        /// <returns>A IEnumerable of ISolrQuery objects.</returns>
        private ISolrQuery BuildRegionQuery(SearchContext context, bool au, bool nz, bool all)
        {
            var region = new List<ISolrQuery>();

            if (!all)
            {
                if (au) region.Add(Query.Field(Fields.OUTLET_COUNTRY_ID).Is(Constants.AustraliaCode));
                if (nz) region.Add(Query.Field(Fields.OUTLET_COUNTRY_ID).Is(Constants.NewZealandCode));
            }
            if (!region.IsNullOrEmpty())
            {
                region.Add(Query.Field(Fields.OUTLET_IS_GENERIC).Is(true));
                region.Add(Query.Field(Fields.IS_PRIVATE).Is(true));
                return new SolrMultipleCriteriaQuery(region, "OR");
            }
            return null;
        }

        /// <summary>
        /// Build a query to search by Outlet / Contact Id.
        /// </summary>
        /// <returns>An ISolrQuery object.</returns>
        private ISolrQuery BuildContactOrOutletIdQuery(SearchContext context, string selectedId, RecordType selectedRecordType)
        {
            if (selectedRecordType == RecordType.MediaOutlet || selectedRecordType == RecordType.PrnOutlet || selectedRecordType == RecordType.OmaOutlet)
            {
                if (context == SearchContext.Outlet)
                    return Query.Field(Fields.OUTLET_ID).Is(selectedId);
                else
                {
                    var queries = new List<ISolrQuery>();

                    // Boost results with a null contact id so outlets show up on top
                    queries.Add(new SolrMultipleCriteriaQuery(new[] {
                        Query.Field(Fields.OUTLET_ID).Is(selectedId),
                        Query.Field(Fields.CONTACT_ID).Is("*").Not()
                    }, "AND").Boost(20));

                    // Now add a plain outlet id search for the contacts of this outlet
                    queries.Add(Query.Field(Fields.OUTLET_ID).Is(selectedId));

                    return new SolrMultipleCriteriaQuery(queries, "OR");
                }
            }
            else
                return Query.Field(Fields.CONTACT_ID).Is(selectedId);
        }

        /// <summary>
        /// Filter for data module access.
        /// </summary>
        /// <param name="context">The current search context.</param>
        /// <returns>The filter query.</returns>
        private ISolrQuery BuildDataModulesFilter(SearchContext context)
        {
            ISolrQuery query = null;

            // Configure the data module permisions first.
            using (var ctx = new MedianetContactsContext())
            {
                var customer = ctx.Customers
                    .Include(d => d.DataModules.Select(c => c.Continent))
                    .SingleOrDefault(c => c.DebtorNumber == _debtorNo);

                if (customer != null)
                {
                    List<DataModuleModel> modules = customer.DataModules.ToList();
                    var continents = modules.Select(m => m.Continent.Id.ToString(CultureInfo.InvariantCulture)).ToList();

                    // Add to filters.
                    if (!continents.IsNullOrEmpty())
                    {
                        if (context == SearchContext.Both)
                        {
                            query = new SolrMultipleCriteriaQuery(new ISolrQuery[] {
                                new SolrQueryInList(
                                    Fields.CONTACT_CONTINENT_ID, continents),
                                new SolrQueryInList(
                                    Fields.OUTLET_CONTINENT_ID, continents)
                            }, "OR");
                        }
                        if (context == SearchContext.People)
                            query = new SolrQueryInList(Fields.CONTACT_CONTINENT_ID, continents);
                        if (context == SearchContext.Outlet)
                            query = new SolrQueryInList(Fields.OUTLET_CONTINENT_ID, continents);
                    }
                    else
                    {
                        query = Query.Field(Fields.OUTLET_CONTINENT_ID).Is("30") ||
                                Query.Field(Fields.CONTACT_CONTINENT_ID).Is("30");
                    }
                }
                return new SolrMultipleCriteriaQuery(new List<ISolrQuery> {
                    query,
                    Query.Field(Fields.IS_PRIVATE).Is(true)
                }, "OR");
            }
        }

        /// <summary>
        /// Builds the query for locations.
        /// </summary>
        /// <param name="quick">The SearchOptions object.</param>
        /// <returns>A ISolrQuery for querying on location.</returns>
        private ISolrQuery BuildLocationQuery(SearchOptionsQuick quick)
        {
            var location = new List<ISolrQuery>();

            foreach (var l in quick.LocationList)
            {
                var focuses = new List<string>();
                var loc = l.ToString();
                var focus = loc.Split(':');

                // State.
                var state = Query.Field(Fields.OUTLET_STATE_NAME).Is(focus[0]);

                // News focuses.
                var focusIds = focus[1].Split('#');
                focuses.AddRange(focusIds);

                location.Add(state && new SolrQueryInList(Fields.NEWS_FOCUS_IDS, focuses));
            }
            return new SolrMultipleCriteriaQuery(location, "OR");
        }

        /// <summary>
        /// Utilizes <see cref="SolrBooleanExpressionParser"/> for parsing the boolean expression <see cref="expression"/> and generates Solr queries to search based on the SearchContext <see cref="context"/>. 
        /// </summary>
        /// <param name="context"><see cref="SearchContext"/> to generate the correct SolrQuery</param>
        /// <param name="expression">A boolean expression to search for in <see cref="Fields.CONTACT_SYSTEM_NOTES"/> and <see cref="Fields.OUTLET_SYSTEM_NOTES"/>.</param>
        /// <returns>returns ISolrQuery for finding <see cref="Fields.CONTACT_SYSTEM_NOTES"/> and <see cref="Fields.OUTLET_SYSTEM_NOTES"/> based on the SearchContext <see cref="context"/>.</returns>
        private ISolrQuery BuildSystemNotesQuery(SearchContext context, string expression)
        {
            ISolrQuery outletQuery = null;
            ISolrQuery contactQuery = null;
            ISolrQuery solrQuery = null;

            if (context == SearchContext.Both || context == SearchContext.Outlet) outletQuery = BuildBooleanTextQuery(expression, Fields.OUTLET_SYSTEM_NOTES, true);

            if (context == SearchContext.Both || context == SearchContext.People) contactQuery = BuildBooleanTextQuery(expression, Fields.CONTACT_SYSTEM_NOTES, true);

            if (outletQuery != null) outletQuery = new SolrMultipleCriteriaQuery(new[] { outletQuery, Query.Field(Fields.IS_OUTLET).Is(true) }, "AND");

            if (contactQuery != null) contactQuery = new SolrMultipleCriteriaQuery(new[] { contactQuery, Query.Field(Fields.IS_OUTLET).Is(false) }, "AND");

            solrQuery = (outletQuery != null && contactQuery != null)
                ? new SolrMultipleCriteriaQuery(new[] { outletQuery, contactQuery }, "OR")
                : (outletQuery ?? contactQuery)
            ;

            return solrQuery;
        }

        /// <summary>
        /// Builds the text query. Query will vary depending on the number of
        /// words in the text input.
        /// </summary>
        /// <param name="text">The text input.</param>
        /// <param name="field">The field that is being searched.</param>
        /// <returns>A ISolrQuery object for querying the field.</returns>
        private AbstractSolrQuery BuildTextQuery(string text, string field)
        {
            AbstractSolrQuery query = null;

            if (!string.IsNullOrWhiteSpace(text))
            {
                var split = Regex.Split(text, @"\s+");

                if (split.Count() == 1)
                    query = BuildSingleWordTextQuery(text, field) as AbstractSolrQuery;
                else
                {
                    query =
                        new SolrMultipleCriteriaQuery(
                            split.Select(s => Query.Field(field).Is(s) || Query.Field(field).Is(s + "*")), "AND");
                }
            }

            return query;
        }

        /// <summary>
        /// Constructs a text query for a single word.
        /// </summary>
        /// <param name="field">The field the query is searching.</param>
        /// <param name="text">The text to search.</param>
        /// <returns>A AbstractSolrQuery that searches the given field.</returns>
        private AbstractSolrQuery BuildSingleWordTextQuery(string text, string field)
        {
            AbstractSolrQuery query = null;
            if (!text.IsNullOrWhitespace())
            {
                query = new SolrMultipleCriteriaQuery(new[] {
                    Query.Field(field).Is(text + "*"),
                    Query.Field(field).Is(text),
                    Query.Field(field).Is(text + @"~0.8")
                }, "OR");
            }
            return query;
        }

        /// <summary>
        /// Builds the text query. Query will vary depending on the number of
        /// words in the text input.
        /// </summary>
        /// <param name="text">The text input.</param>
        /// <param name="field">The field that is being searched.</param>
        /// <param name="implyQuotes">Use true to find exact matches. Defaults to false.</param>
        /// <returns>A ISolrQuery object for querying the field.</returns>
        private ISolrQuery BuildBooleanTextQuery(string text, string field, bool implyQuotes = false, double? boostValue = null)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                try
                {
                    var parser = new SolrBooleanExpressionParser(field, text, _logger, implyQuotes, boostValue);
                    return parser.Parse();

                }
                catch (Exception ex)
                {
                    return BuildTextQuery(text, field);
                }
            }

            return null;
        }

        /// <summary>
        /// Builds a text query for auto suggestion.
        /// </summary>
        /// <param name="context">The search context.</param>
        /// <param name="searchText">The text to search.</param>
        /// <returns>The built ISolrQuery object.</returns>
        private ISolrQuery BuildAutoSuggestTextQuery(SearchContext context, string searchText)
        {
            ISolrQuery textQuery = null;

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                if (context == SearchContext.People)
                    textQuery = BuildTextQuery(searchText, Fields.FULL_NAME);
                else if (context == SearchContext.Outlet)
                    textQuery = BuildTextQuery(searchText, Fields.OUTLET_NAME);
                else
                    textQuery = BuildTextQuery(searchText, Fields.TEXT_OUTLET_OR_FULLNAME);
            }
            return textQuery;
        }

        /// <summary>
        /// Builds a text query for searching all fields
        /// </summary>
        /// <param name="context">The search context.</param>
        /// <param name="searchText">The text to search.</param>
        /// <returns>The built ISolrQuery object.</returns>
        private ISolrQuery BuildAllTextQuery(SearchContext context, string searchText)
        {
            if (string.IsNullOrWhiteSpace(searchText))
                return null;

            var searchTextQuery = new List<ISolrQuery>();

            if (context == SearchContext.Outlet || context == SearchContext.Both)
            {
                // Exact outlet search first. Limit by record type in the first one to exclude contact records.
                searchTextQuery.Add(new SolrMultipleCriteriaQuery(new ISolrQuery[] {
                    BuildBooleanTextQuery(searchText, Fields.OUTLET_NAME, true),
                    new SolrQueryInList(Fields.RECORD_TYPE, ((int)RecordType.MediaOutlet).ToString(), ((int)RecordType.PrnOutlet).ToString(), ((int)RecordType.OmaOutlet).ToString()) }, "AND").Boost(15));
            }

            // Now do a wildcard outlet name search. This could return contacts too.
            searchTextQuery.Add(BuildBooleanTextQuery(searchText, Fields.OUTLET_NAME, false, 12));

            if (context == SearchContext.People || context == SearchContext.Both)
            {
                // Exact contact name search followed by wildcard contact name search, then job title
                searchTextQuery.Add(BuildBooleanTextQuery(searchText, Fields.FULL_NAME, true, 9));
                searchTextQuery.Add(BuildBooleanTextQuery(searchText, Fields.FULL_NAME, false, 6));
                searchTextQuery.Add(BuildBooleanTextQuery(searchText, Fields.JOB_TITLE, true, 3));
            }

            // Now search the generic field with all searchable fields combined.
            searchTextQuery.Add(BuildBooleanTextQuery(searchText, Fields.TEXT_FULL_SEARCH, false));

            return new SolrMultipleCriteriaQuery(searchTextQuery, "OR");
        }

        /// <summary>
        /// Creates the filter query for the context.
        /// </summary>
        /// <param name="context">The search context.</param>
        /// <returns>The filter query.</returns>
        private ISolrQuery BuildContextFilter(SearchContext context)
        {
            if (context == SearchContext.People)
                return Query.Field(Fields.IS_OUTLET).Is(false);
            if (context == SearchContext.Outlet)
                return Query.Field(Fields.IS_OUTLET).Is(true);
            return null;
        }

        /// <summary>
        /// Creates a filter on contacts that are private to this customer and anything else.
        /// </summary>
        /// <returns>The ISolrQuery object.</returns>
        private ISolrQuery BuildPermissionFilter()
        {
            return Query.Field(Fields.OWNER_DEBTOR_NUMBER).Is(_debtorNo) ||
                   Query.Field(Fields.IS_PRIVATE).Is(false);
        }

        /// <summary>
        /// Constructs a query based on the IDs and field given.
        /// </summary>
        /// <param name="ids">The list of ids.</param>
        /// <param name="field">The field to search on.</param>
        /// <returns>The ISolrQuery object.</returns>
        private ISolrQuery BuildIdQuery<T>(string field, List<T> ids)
        {
            ISolrQuery result = null;

            if (!ids.IsNullOrEmpty())
                result = new SolrQueryInList(field, ids.ConvertAll(x => x.ToString()));

            return result;
        }

        /// <summary>
        /// Builds a query for searching the social fields of only contacts.
        /// </summary>
        /// <param name="options">The search options object.</param>
        /// <returns>The ISolrQuery object.</returns>
        private ISolrQuery BuildSocialPeopleQuery(SearchOptionsAdvanced options)
        {
            var social = new List<ISolrQuery>();

            if (options.ShowOnlyWithTwitter)
                social.Add(new SolrHasValueQuery(Fields.CONTACT_TWITTER));
            if (options.ShowOnlyWithEmail)
                social.Add(Query.Field(Fields.CONTACT_HAS_EMAIL).Is(options.ShowOnlyWithEmail));
            if (options.ShowOnlyWithLinkedIn)
                social.Add(new SolrHasValueQuery(Fields.CONTACT_LINKEDIN));
            if (options.ShowOnlyWithFax)
                social.Add(Query.Field(Fields.CONTACT_HAS_FAX).Is(options.ShowOnlyWithFax));
            if (options.ShowOnlyWithFacebook)
                social.Add(new SolrHasValueQuery(Fields.CONTACT_FACEBOOK));
            if (options.ShowOnlyWithPostalAddress)
                social.Add(Query.Field(Fields.CONTACT_HAS_POSTAL).Is(options.ShowOnlyWithPostalAddress));

            if (options.ShowOnlyWithInstagram)
                social.Add(new SolrHasValueQuery(Fields.CONTACT_INSTAGRAM));
            if (options.ShowOnlyWithYouTube)
                social.Add(new SolrHasValueQuery(Fields.CONTACT_YOUTUBE));
            if (options.ShowOnlyWithSnapchat)
                social.Add(new SolrHasValueQuery(Fields.CONTACT_SNAPCHAT));

            return !social.IsNullOrEmpty() ? new SolrMultipleCriteriaQuery(social, "AND") : null;
        }

        /// <summary>
        /// Builds a query for searching the social fields of only outlets.
        /// </summary>
        /// <param name="options">The search options object.</param>
        /// <returns>The ISolrQuery object.</returns>
        private ISolrQuery BuildSocialOutletQuery(SearchOptionsAdvanced options)
        {
            var social = new List<ISolrQuery>();
            if (options.ShowOnlyWithTwitter)
                social.Add(new SolrHasValueQuery(Fields.OUTLET_TWITTER));
            if (options.ShowOnlyWithEmail)
                social.Add(Query.Field(Fields.OUTLET_HAS_EMAIL).Is(options.ShowOnlyWithEmail));
            if (options.ShowOnlyWithLinkedIn)
                social.Add(new SolrHasValueQuery(Fields.OUTLET_LINKEDIN));
            if (options.ShowOnlyWithFax)
                social.Add(Query.Field(Fields.OUTLET_HAS_FAX).Is(options.ShowOnlyWithFax));
            if (options.ShowOnlyWithFacebook)
                social.Add(new SolrHasValueQuery(Fields.OUTLET_FACEBOOK));
            if (options.ShowOnlyWithPostalAddress)
                social.Add(Query.Field(Fields.OUTLET_HAS_POSTAL).Is(options.ShowOnlyWithPostalAddress));


            if (options.ShowOnlyWithInstagram)
                social.Add(new SolrHasValueQuery(Fields.OUTLET_INSTAGRAM));
            if (options.ShowOnlyWithYouTube)
                social.Add(new SolrHasValueQuery(Fields.OUTLET_YOUTUBE));
            if (options.ShowOnlyWithSnapchat)
                social.Add(new SolrHasValueQuery(Fields.OUTLET_SNAPCHAT));
           
            return !social.IsNullOrEmpty() ? new SolrMultipleCriteriaQuery(social, "AND") : null;
        }

        private ISolrQuery BuildGeographyPeopleQuery(SearchOptionsAdvanced options)
        {
            var geography = new List<ISolrQuery>();

            if (!options.ContactContinentIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.CONTACT_CONTINENT_ID, options.ContactContinentIDList));

            if (!options.ContactCountryIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.CONTACT_COUNTRY_ID, options.ContactCountryIDList));

            if (!options.ContactStateIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.CONTACT_STATE_ID, options.ContactStateIDList));

            if (!options.ContactCityIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.CONTACT_CITY_ID, options.ContactCityIDList));

            if (!options.ContactPostCodeList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.CONTACT_POSTCODE, options.ContactPostCodeList));

            return geography.IsNullOrEmpty() ? null : new SolrMultipleCriteriaQuery(geography, "AND");
        }

        private ISolrQuery BuildGeographyOutletQuery(SearchOptionsAdvanced options)
        {
            var geography = new List<ISolrQuery>();

            if (!options.OutletContinentIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.OUTLET_CONTINENT_ID, options.OutletContinentIDList));

            if (!options.OutletCountryIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.OUTLET_COUNTRY_ID, options.OutletCountryIDList));

            if (!options.OutletStateIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.OUTLET_STATE_ID, options.OutletStateIDList));

            if (!options.OutletCityIDList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.OUTLET_CITY_ID, options.OutletCityIDList));

            if (!options.OutletPostCodeList.IsNullOrEmpty())
                geography.Add(BuildIdQuery(Fields.OUTLET_POSTCODE, options.OutletPostCodeList));

            return geography.IsNullOrEmpty() ? null : new SolrMultipleCriteriaQuery(geography, "AND");
        }

        private ISolrQuery BuildGeographyQuery(SearchOptionsAdvanced options, SearchContext context)
        {
            var geography = new List<ISolrQuery>();

            if (context == SearchContext.People || context == SearchContext.Both)
            {
                var people = BuildGeographyPeopleQuery(options);
                if (people != null) geography.Add(people);

                var outlet = BuildGeographyOutletQuery(options);
                if (outlet != null) geography.Add(outlet);
            }
            else if (context == SearchContext.Outlet)
            {
                var outlet = BuildGeographyOutletQuery(options);
                if (outlet != null) geography.Add(outlet);
            }

            if (geography.Count == 1)
                return geography.First();
            else
                return geography.IsNullOrEmpty() ? null : new SolrMultipleCriteriaQuery(geography, "AND");
        }

        /// <summary>
        /// Builds the query to serach subjects for advanced search.
        /// </summary>
        private ISolrQuery BuildAdvancedSubjectQuery(SearchOptionsAdvanced options, SearchContext context)
        {
            var subjects = new List<ISolrQuery>();

            if (context == SearchContext.People || context == SearchContext.Both)
            {
                if (!options.ContactSubjectList.IsNullOrEmpty())
                    subjects.Add(BuildIdQuery(Fields.CONTACT_SUBJECT_IDS, options.ContactSubjectList));
                if (!options.ContactSubjectGroupList.IsNullOrEmpty())
                    subjects.Add(BuildIdQuery(Fields.CONTACT_SUBJECT_GROUP_IDS, options.ContactSubjectGroupList));

                if (!options.OutletSubjectList.IsNullOrEmpty())
                    subjects.Add(BuildIdQuery(Fields.OUTLET_SUBJECT_IDS, options.OutletSubjectList));
                if (!options.OutletSubjectGroupList.IsNullOrEmpty())
                    subjects.Add(BuildIdQuery(Fields.OUTLET_SUBJECT_GROUP_IDS, options.OutletSubjectGroupList));
            }
            else if (context == SearchContext.Outlet)
            {
                if (!options.OutletSubjectList.IsNullOrEmpty())
                    subjects.Add(BuildIdQuery(Fields.OUTLET_SUBJECT_IDS, options.OutletSubjectList));
                if (!options.OutletSubjectGroupList.IsNullOrEmpty())
                    subjects.Add(BuildIdQuery(Fields.OUTLET_SUBJECT_GROUP_IDS, options.OutletSubjectGroupList));
            }
            return subjects.IsNullOrEmpty() ? null : new SolrMultipleCriteriaQuery(subjects, "AND");
        }

        /// <summary>
        /// Build query to search subjects for quick search.
        /// </summary>
        /// <param name="quick">The search options object.</param>
        /// <returns>The ISolrQuery object.</returns>
        private ISolrQuery BuildQuickSubjectQuery(SearchOptionsQuick quick)
        {
            var subjects = new List<ISolrQuery>();

            if (!quick.SubjectList.IsNullOrEmpty())
            {
                if (quick.Context == SearchContext.People)
                    subjects.Add(BuildIdQuery(Fields.CONTACT_SUBJECT_IDS, quick.SubjectList));
                else if (quick.Context == SearchContext.Outlet)
                    subjects.Add(BuildIdQuery(Fields.OUTLET_SUBJECT_IDS, quick.SubjectList));
                else if (quick.Context == SearchContext.Both)
                {
                    subjects.Add((AbstractSolrQuery)BuildIdQuery(Fields.CONTACT_SUBJECT_IDS, quick.SubjectList)
                        && Query.Field(Fields.IS_OUTLET).Is(false));
                    subjects.Add((AbstractSolrQuery)BuildIdQuery(Fields.OUTLET_SUBJECT_IDS, quick.SubjectList)
                        && Query.Field(Fields.IS_OUTLET).Is(true));
                }
            }

            if (!quick.SubjectGroupList.IsNullOrEmpty())
            {
                if (quick.Context == SearchContext.People)
                    subjects.Add(BuildIdQuery(Fields.CONTACT_SUBJECT_GROUP_IDS, quick.SubjectGroupList));
                else if (quick.Context == SearchContext.Outlet)
                    subjects.Add(BuildIdQuery(Fields.OUTLET_SUBJECT_GROUP_IDS, quick.SubjectGroupList));
                else if (quick.Context == SearchContext.Both)
                {
                    subjects.Add((AbstractSolrQuery)BuildIdQuery(Fields.CONTACT_SUBJECT_GROUP_IDS, quick.SubjectGroupList)
                        && Query.Field(Fields.IS_OUTLET).Is(false));
                    subjects.Add((AbstractSolrQuery)BuildIdQuery(Fields.OUTLET_SUBJECT_GROUP_IDS, quick.SubjectGroupList)
                        && Query.Field(Fields.IS_OUTLET).Is(true));
                }
            }

            return subjects.IsNullOrEmpty() ? null : new SolrMultipleCriteriaQuery(subjects, "OR");
        }

        /// <summary>
        /// Maps a collection of one type to another in parallel.
        /// </summary>
        /// <typeparam name="TSource">The type of the original.</typeparam>
        /// <typeparam name="TDestination">The type to be mapped to.</typeparam>
        /// <param name="toMap">The collection of items to map.</param>
        /// <returns>An IEnumerable of mapped objects.</returns>
        private IEnumerable<TDestination> ParallelMap<TSource, TDestination>(IEnumerable<TSource> toMap)
        {
            if (toMap.IsNullOrEmpty())
                return new List<TDestination>();

            var mapped = new BlockingCollection<TDestination>();
            toMap.AsParallel().ForAll(x => mapped.Add(Mapper.Map<TSource, TDestination>(x)));

            return mapped;
        }
    }
}
