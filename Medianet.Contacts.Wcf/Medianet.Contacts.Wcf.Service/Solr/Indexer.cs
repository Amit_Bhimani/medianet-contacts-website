﻿using Medianet.Contacts.Wcf.Service.Solr.Documents;
using NLog;

namespace Medianet.Contacts.Wcf.Service.Solr
{
    public class Indexer : SolrBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Updates one or more contacts.
        /// </summary>
        /// <param name="contact"> The contacts to update. </param>
        public void Update(params SolrContact[] contact) {
            if (contact != null)
                Solr.Add(contact);
        }

        /// <summary>
        /// Deletes one or more contacts.
        /// </summary>
        /// <param name="contact"> The contacts to delete. </param>
        public void Delete(params SolrContact[] contact) {
            if (contact != null)
                Solr.Delete(contact);
        }
    }
}