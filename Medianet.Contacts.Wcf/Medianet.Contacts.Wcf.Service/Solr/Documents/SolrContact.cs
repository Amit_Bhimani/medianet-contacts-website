﻿using System;
using System.Collections.Generic;

namespace Medianet.Contacts.Wcf.Service.Solr.Documents
{
    public class SolrContact
    {
        // Meta info.
        public string Id { get; set; }
        public bool IsDangling { get; set; }
        public bool IsDirty { get; set; }
        public bool IsAap { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsOutlet { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime LastModified { get; set; }

        // Contact.
        public string ContactId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string JobTitle { get; set; }
        public ICollection<int> PositionIds { get; set; }
        public bool? IsPrimaryContact { get; set; }
        public string ContactPreferredDeliveryMethod { get; set; }
        public ICollection<int> ContactSubjectIds { get; set; }
        public ICollection<int> ContactSubjectGroupIds { get; set; }
        public ICollection<string> ContactSubjectNames { get; set; }
        public string ContactSystemNotes { get; set; }
        public string ContactTwitter { get; set; }
        public string ContactLinkedIn { get; set; }
        public string ContactFacebook { get; set; }
        public string ContactInstagram { get; set; }
        public string ContactYouTube { get; set; }
        public string ContactSnapchat { get; set; }
        public string ContactSkype { get; set; }
        public string ContactBio { get; set; }
        public bool? ContactHasEmail { get; set; }
        public bool? ContactHasFax { get; set; }
        public bool? ContactHasPostal { get; set; }
        public string ContactCityName { get; set; }
        public decimal? ContactContinentId { get; set; }
        public decimal? ContactCountryId { get; set; }
        public decimal? ContactStateId { get; set; }
        public string ContactStateName { get; set; }
        public decimal? ContactCityId { get; set; }
        public string ContactPostcode { get; set; }

        public string ContactBugBears { get; set; }
        public string ContactAlsoKnownAs { get; set; }
        public string ContactPressReleaseInterests { get; set; }
        public string ContactPersonalInterests { get; set; }
        public string ContactAppearsIn { get; set; }
        public int? ContactMediaInfluencerScore { get; set; }
        public string ContactCurrentStatus { get; set; }
        public string ContactBasedInLocation { get; set; }
        public string ContactAlternativeEmailAddress { get; set; }
        public string ContactWebsite { get; set; }
        //public bool IsContactOutOfOffice { get; set; }
        public DateTime? ContactOooStartDate { get; set; }
        public DateTime? ContactOooReturnDate { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactMobileNumber { get; set; }
        public string ContactEmailAddress { get; set; }
        public string ContactPriorityNotice { get; set; }


        // Outlet
        public string OutletId { get; set; }
        public ICollection<string> ContactIds { get; set; }
        public ICollection<string> ContactNames { get; set; }
        public string OutletName { get; set; }
        public string OutletPreferredDeliveryMethod { get; set; }
        public ICollection<int> OutletSubjectIds { get; set; }
        public ICollection<int> OutletSubjectGroupIds { get; set; }
        public ICollection<string> OutletSubjectNames { get; set; }
        public string OutletSystemNotes { get; set; }
        public string OutletTwitter { get; set; }
        public string OutletLinkedIn { get; set; }
        public string OutletFacebook { get; set; }
        public string OutletInstagram { get; set; }
        public string OutletYouTube { get; set; }
        public string OutletSnapchat { get; set; }
        public string OutletSkype { get; set; }
        public string OutletBio { get; set; }
        public bool? OutletHasEmail { get; set; }
        public bool? OutletHasFax { get; set; }
        public bool? OutletHasPostal { get; set; }
        public bool? OutletIsOffice { get; set; }
        public bool? OutletIsFinancial { get; set; }
        public bool? OutletIsPolitician { get; set; }
        public string OutletCityName { get; set; }
        public decimal? OutletContinentId { get; set; }
        public decimal? OutletCountryId { get; set; }
        public decimal? OutletStateId { get; set; }
        public string OutletStateName { get; set; }
        public decimal? OutletCityId { get; set; }
        public string OutletPostcode { get; set; }
        public decimal? OutletPostalCityId { get; set; }
        public decimal? OutletPostalCountryId { get; set; }
        public decimal? OutletPostalContinentId { get; set; }
        public bool? OutletSubscriptionOnlyPublication { get; set; }
        public string OutletAlsoKnownAs { get; set; }
        public string OutletAdditionalWebsite { get; set; }
        public string OutletPublishedOn { get; set; }
        public string OutletPressReleaseInterests { get; set; }
        public string OutletRegionsCovered { get; set; }
        public string OutletPhoneNumber { get; set; }
        public string OutletEmailAddress { get; set; }
        public string OutletWebsite { get; set; }
        public string OutletLogoFilename { get; set; }
        public bool? OutletIsGeneric { get; set; }
        public string OutletPriorityNotice { get; set; }

        // Common
        public int RecordType { get; set; }
        public int MediaTypeId { get; set; }
        public string MediaType { get; set; }
        public decimal? FrequencyId { get; set; }
        public decimal? Circulation { get; set; }
        public ICollection<int> LanguageIds { get; set; }
        public ICollection<string> LanguageNames { get; set; }
        public string StationFrequency { get; set; }
        public ICollection<int> NewsFocusIds { get; set; }
        public string OwnerDebtorNumber { get; set; }
        public string OutletOrFullName { get; set; }
        public string TextFullSearch { get; set; }

        // Sorting.
        public string Location { get; set; }
        public string Subjects { get; set; }
        public int DataModuleId { get; set; }
        public double? Score { get; set; }
    }
}

