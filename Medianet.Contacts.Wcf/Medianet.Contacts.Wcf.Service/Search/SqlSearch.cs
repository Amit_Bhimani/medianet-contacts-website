﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Caching;
using Medianet.Contacts.Wcf.Service.Common;

namespace Medianet.Contacts.Wcf.Service.Search
{
    public class SqlSearch : SearchBase
    {
        /// <summary>
        /// Searches the specified <c>SearchOptions</c> object.
        /// </summary>
        /// <param name="totalResults"> </param>
        /// <param name="so">The <c>SearchOptions</c> object.</param>
        /// <param name="sortCol">The sort column.</param>
        /// <param name="sortDir">The sort direction.</param>
        /// <param name="startPos">The start position to return results from.</param>
        /// <param name="count">The number of results to return.</param>
        /// <param name="pinned">The pinned items.</param>
        /// <param name="deleted">The deleted items.</param>
        /// <param name="unselected">The selected items.</param>
        /// <param name="newSearch">Perform a new search if <c>true</c>.</param>
        /// <param name="userid">The current user's id.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<SearchResult> Search(
            out int totalResults,
            SearchOptions so,
            SortColumn sortCol,
            SortDirection sortDir,
            bool newSearch,
            int userid,
            int? startPos = null,
            int? count = null,
            List<RecordIdentifier> pinned = null,
            List<RecordIdentifier> deleted = null,
            List<RecordIdentifier> unselected = null
        ) {
            List<SearchResult> results = null;
            SortColumn oldSortCol = SortColumn.SolrScore;
            SortDirection oldSortDir = SortDirection.ASC;

            // If it isn't a new search then look for the results in the cache.
            if (!newSearch) {
                SearchCache cacheResult = SearchDataCache.Get(userid.ToString(CultureInfo.InvariantCulture), so);
                if (cacheResult != null) {
                    results = cacheResult.Results.Select(item => (SearchResult)item.Clone()).ToList();

                    oldSortCol = cacheResult.SortCol;
                    oldSortDir = cacheResult.SortDir;
                }
            }

            if (results == null) {
                using (var context = new MedianetContactsSPEntities()) {
                    // We don't have the results in the cache so perform the search.
                    if (so.Type == SearchType.Outlet) {
                        // Perform an outlet search (contacts belonging to an outlet).
                        var options = so as SearchOptionsOutlet;

                        if (options.OutletType == RecordType.MediaOutlet)
                            results = context.GetContactsByMediaOutletId(options.OutletID)
                                .Select(Mapper.Map<SearchResultModel, SearchResult>)
                                .ToList();
                        else if (options.OutletType == RecordType.PrnOutlet)
                            results = context.GetContactsByPrnOutletId(Convert.ToInt32(options.OutletID), userid)
                                .Select(Mapper.Map<SearchResultModel, SearchResult>)
                                .ToList();
                        else if (options.OutletType == RecordType.OmaOutlet)
                            results = context.GetContactsByOmaOutletId(Convert.ToInt32(options.OutletID), userid)
                                .Select(Mapper.Map<SearchResultModel, SearchResult>)
                                .ToList();
                    }
                    else if (so.Type == SearchType.Quick) {
                        // Perform a quick search.
                        var options = so as SearchOptionsQuick;

                        if (options.Context == SearchContext.People) {
                            results = context.GetContactsByQuickSearchContactCriteria(
                                options.SearchText,
                                options.PositionList.Join(","),
                                options.SubjectList.Join(","),
                                options.SubjectGroupList.Join(","),
                                options.MediaTypeList.Join(","),
                                options.LocationList.Join(","),
                                options.SearchAustralia,
                                options.SearchNewZealand,
                                options.SearchAllCountries,
                                userid,
                                Constants.MaxRows)
                                .Select(Mapper.Map<SearchResultModel, SearchResult>)
                                .ToList();
                        }
                        else if (options.Context == SearchContext.Outlet) {
                            results = context.GetContactsByQuickSearchOutletCriteria(
                                options.SearchText,
                                options.PositionList.Join(","),
                                options.SubjectList.Join(","),
                                options.SubjectGroupList.Join(","),
                                options.MediaTypeList.Join(","),
                                options.LocationList.Join(","),
                                options.SearchAustralia,
                                options.SearchNewZealand,
                                options.SearchAllCountries,
                                userid,
                                Constants.MaxRows)
                                .Select(Mapper.Map<SearchResultModel, SearchResult>)
                                .ToList(); ;
                        }
                        else if (options.Context == SearchContext.Both) {
                            results = context.GetContactsByQuickSearchCriteria(
                                options.SearchText,
                                options.PositionList.Join(","),
                                options.SubjectList.Join(","),
                                options.SubjectGroupList.Join(","),
                                options.MediaTypeList.Join(","),
                                options.LocationList.Join(","),
                                options.SearchAustralia,
                                options.SearchNewZealand,
                                options.SearchAllCountries,
                                userid,
                                Constants.MaxRows)
                                .Select(Mapper.Map<SearchResultModel, SearchResult>)
                                .ToList();
                        }
                    }
                    else if (so.Type == SearchType.ServiceList) {
                        var options = so as SearchOptionsServiceList;
                        var result = new List<ServiceListSearchResultModel>();

                        result = context.GetByServiceSearchCriteria(
                            options.SearchText,
                            options.ServiceListId,
                            options.CategoryId,
                            options.ShowFax,
                            options.ShowEmail,
                            Constants.MaxRows).ToList();

                        results = Mapper.Map<List<ServiceListSearchResultModel>, List<SearchResult>>(result);
                    }
                    else if (so.Type == SearchType.QuickAdmin) {
                        var options = so as SearchOptionsQuickAdmin;

                        results = context.GetByAdminQuickSearchCriteria(
                            options.ContextDBCode,
                            options.SearchText,
                            options.ShouldSearchNotes,
                            options.PositionList.Join(","),
                            options.SubjectList.Join(","),
                            options.SubjectGroupList.Join(","),
                            options.MediaTypeList.Join(","),
                            options.StatusDBCode,
                            options.OutletId,
                            options.ContactId,
                            options.CountryId,
                            options.StateId,
                            options.CityId,
                            userid,
                            Constants.MaxRows)
                            .Select(Mapper.Map<AdminQuickSearchResultModel, SearchResult>)
                            .ToList();
                    }
                    else {
                        // Perform an advanced search.
                        var options = so as SearchOptionsAdvanced;
                        results = context.GetContactsByAdvancedSearchCriteria(
                            options.ContextDBCode,
                            options.FirstName,
                            options.LastName,
                            options.JobTitle,
                            options.PositionList.Join(","),
                            options.ContactSubjectList.Join(","),
                            options.ContactSubjectGroupList.Join(","),
                            options.PrimaryNewsContactOnly,
                            options.ContactContinentIDList.Join(","),
                            options.ContactCountryIDList.Join(","),
                            options.ContactStateIDList.Join(","),
                            options.ContactCityIDList.Join(","),
                            options.ContactPostCodeList
                                .Where(x => !x.IsNullOrWhitespace())
                                .Select(x => "'" + x + "'")
                                .ToList()
                                .Join(","),
                            ConvertFieldSearchString(options.OutletName),
                            options.MediaTypeList.Join(","),
                            options.OutletSubjectList.Join(","),
                            options.OutletSubjectGroupList.Join(","),
                            options.OutletFrequencyList.Join(","),
                            options.OutletCirculationMin,
                            options.OutletCirculationMax,
                            options.OutletLanguageList.Join(","),
                            options.OutletStationFrequencyList
                                .Where(x => !x.IsNullOrWhitespace())
                                .Select(x => "'" + x + "'")
                                .ToList()
                                .Join(","),
                            options.OutletContinentIDList.Join(","),
                            options.OutletCountryIDList.Join(","),
                            options.OutletStateIDList.Join(","),
                            options.OutletCityIDList.Join(","),
                            options.OutletPostCodeList
                                .Where(x => !x.IsNullOrWhitespace())
                                .Select(x => "'" + x + "'")
                                .ToList()
                                .Join(","),
                            ConvertNewsFocusToString(options.IncludeMetroOutlets, options.IncludeRegionalOutlets, options.IncludeSuburbanOutlets),
                            options.ShowOnlyWithTwitter,
                            options.ShowOnlyWithLinkedIn,
                            options.ShowOnlyWithFacebook,
                            options.ShowOnlyWithEmail,
                            options.ShowOnlyWithFax,
                            options.ShowOnlyWithPostalAddress,
                            GetPreferredDeliveryMethod(options.PrefersEmail, options.PrefersFax, options.PrefersMail),
                            ConvertNotesSearchString(options.SystemNotes),
                            ConvertNotesSearchString(options.MyNotes),
                            options.ShowMediaDirRecords,
                            options.ShowPrivateRecords,
                            options.ExcludeKeywords,
                            options.ExcludeOPS,
                            options.ExcludeFinanceInst,
                            options.ExcludePoliticians,
                            options.ListList.Join(","),
                            options.TaskKeyword,
                            options.TaskGroups.Join(","),
                            options.TaskCategories.Join(","),
                            options.TaskStatus.HasValue ? (int?)options.TaskStatus.Value : null,
                            options.ExcludeCompletedTasks,
                            userid,
                            Constants.MaxRows,
                            options.ExcludeOutOfOffice)
                            .Select(Mapper.Map<SearchResultModel, SearchResult>)
                            .ToList();
                    }
                }
            }

            // Add it to the cache by making a deep clone.
            // If the data isn't sorted the way we want then sort it.
            if (sortCol != oldSortCol || sortDir != oldSortDir || newSearch)
                SortResults(results, sortCol, sortDir, so);

            // Add it to the cache by making a deep clone.
            SearchDataCache.Insert(userid.ToString(CultureInfo.InvariantCulture),
                                   results.Select(item => (SearchResult) item.Clone()).ToList(), so, sortCol, sortDir);

            return FetchRange(out totalResults, results, startPos.HasValue ? startPos.Value : 0,
                count.HasValue ? count.Value : int.MaxValue, pinned, deleted, unselected, userid); 
        }

        /// <summary>
        /// Searches the with expanded outlets.
        /// </summary>
        /// <param name="totalResults">The total results.</param>
        /// <param name="searchOptions">The search options.</param>
        /// <param name="sortColumn">The sort column.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <param name="startPos">The start position.</param>
        /// <param name="count">Number of records to return.</param>
        /// <param name="pinned">The pinned items.</param>
        /// <param name="deleted">The deleted items.</param>
        /// <param name="unselected">The unselected items.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<SearchResult> SearchWithExpandedOutlets(
            out int totalResults,
            SearchOptions searchOptions,
            SortColumn sortColumn,
            SortDirection sortDirection,
            int userid,
            int? startPos = null,
            int? count = null,
            List<RecordIdentifier> pinned = null,
            List<RecordIdentifier> deleted = null,
            List<RecordIdentifier> unselected = null
        ) {
            var contacts = new List<RecordIdentifier>();

            // Get the search results.
            var results = Search(out totalResults, searchOptions, sortColumn, sortDirection, false, userid);
            if (deleted != null) results.RemoveAll(deleted.Contains);

            foreach (SearchResult r in results) {
                if (!r.IsContact) {
                    if (!string.IsNullOrWhiteSpace(r.ContactIds)) {
                        string[] contactIds = r.ContactIds.Split('#');
                        RecordType rType;

                        if (r.IsContact) rType = r.Type;
                        else if (r.Type == RecordType.MediaOutlet) rType = RecordType.MediaContact;
                        else if (r.Type == RecordType.PrnOutlet) rType = RecordType.PrnContact;
                        else if (r.Type == RecordType.OmaOutlet) rType = RecordType.OmaContactAtOmaOutlet;
                        else rType = RecordType.Unknown;

                        contacts.AddRange(contactIds.Select(contactId => new RecordIdentifier(r.OutletID, contactId, rType)));
                    }
                    else
                        contacts.Add( new RecordIdentifier(r.OutletID, r.ContactIds, r.Type)); // This outlet has no contacts. Keep the outlet.
                }
                else
                    contacts.Add( new RecordIdentifier(r.OutletID, r.ContactId, r.Type));  // It's already a contact.
            }         

            results = FetchRecordIdentifiers(contacts, sortColumn, sortDirection, userid, searchOptions);

            return FetchRange(out totalResults, results, startPos.HasValue ? startPos.Value : 0,
                count.HasValue ? count.Value : int.MaxValue, pinned, deleted, unselected, userid); 
        }

        /// <summary>
        /// Fetches the range.
        /// </summary>
        /// <param name="totalResults">Total number of results.</param>
        /// <param name="results">The results.</param>
        /// <param name="startPos">The start pos.</param>
        /// <param name="count">The count.</param>
        /// <param name="pinned">The pinned.</param>
        /// <param name="deleted">The deleted.</param>
        /// <param name="unselected">The unselected.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>The range out of the given results.</returns>
        public List<SearchResult> FetchRange(
            out int totalResults,
            List<SearchResult> results,
            int startPos,
            int count,
            List<RecordIdentifier> pinned,
            List<RecordIdentifier> deleted,
            List<RecordIdentifier> unselected, int userid
        ) {
            // Remove deleted items.
            if (deleted != null)
                results.RemoveAll(deleted.Contains);

            // Remove pinned items in the search and append any remaining.
            totalResults = ConfigurePinnedRecords(results, pinned, (results.Count - startPos < count), userid);
            int actualCount = Math.Min(count, results.Count - startPos);

            // Deselect items.
            if (unselected != null)
                results.Where(unselected.Contains)
                    .ForEach(r => r.Selected = false);

            if (startPos < results.Count && actualCount > 0)
                return results.GetRange(startPos, actualCount);

            return new List<SearchResult>();
        }

        /// <summary>
        /// Returns results for the auto suggest.
        /// </summary>
        /// <param name="options">The <c>AutoSuggestOptions</c> object.</param>
        /// <param name="maxResults">The max results.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<AutoSuggest> AutoSuggest(
            AutoSuggestOptions options,
            int maxResults,
            int userid
        ) {
            List<AutoSuggest> suggests;

            using (var ctx = new MedianetContactsSPEntities()) {
                suggests = ctx.GetAutoSuggests(
                    options.Context.ToString().Substring(0).ToLower(),
                    options.SearchText,
                    options.IsAdminSearch,
                    options.SearchAustralia,
                    options.SearchNewZealand,
                    options.SearchAllCountries,
                    userid,
                    maxResults)
                    .Select(Mapper.Map<AutoSuggestModel, AutoSuggest>)
                    .ToList();
            }
            return suggests;
        }

        #region Private Methods

        /// <summary>
        /// Gets the preferred delivery method.
        /// </summary>
        /// <param name="pPrefersEmail">if set to <c>true</c> [p prefers email].</param>
        /// <param name="pPrefersFax">if set to <c>true</c> [p prefers fax].</param>
        /// <param name="pPrefersMail">if set to <c>true</c> [p prefers mail].</param>
        /// <returns>String representation of the delivery method.</returns>
        private string GetPreferredDeliveryMethod(bool pPrefersEmail, bool pPrefersFax, bool pPrefersMail) {
            string result = string.Empty;

            if (pPrefersEmail)
                result += "'e'";

            if (pPrefersFax) {
                if (result.Length > 0)
                    result += ",'f'";
                else
                    result += "'f'";
            }

            if (pPrefersMail) {
                if (result.Length > 0)
                    result += ",'m'";
                else
                    result += "'m'";
            }

            return result;
        }

        /// <summary>
        /// Converts the news focus to a string.
        /// </summary>
        /// <param name="metro">Metro flag.</param>
        /// <param name="regional">Regional flag..</param>
        /// <param name="suburban">Suburban flag.</param>
        /// <returns>String representation of the news focus.</returns>
        private string ConvertNewsFocusToString(bool metro, bool regional, bool suburban) {
            string result = string.Empty;

            // If everything is checked we want everything, so return an empty string.
            // It saves the database having to filter by another criteria.
            if (!metro || !regional || !suburban) {
                if (metro)
                    result += "629,630"; // International,National
                if (regional)
                    result += (result.Length > 0 ? "," : "") + "631"; // Regional
                if (suburban)
                    result += (result.Length > 0 ? "," : "") + "632,633"; // Local,Community
            }
            return result;
        }

        /// <summary>
        /// Sanitizes the notes search string.
        /// </summary>
        /// <param name="pInputString">The p input string.</param>
        /// <returns>Search query for the notes section.</returns>
        private static string ConvertNotesSearchString(string pInputString) {
            if (string.IsNullOrWhiteSpace(pInputString))
                return string.Empty;

            if (!(pInputString.Contains("AND") || pInputString.Contains("OR") ||
                pInputString.Contains("NOT") || pInputString.Contains('(') ||
                pInputString.Contains(')') || pInputString.Contains('\"'))) {
                if (pInputString.Length > 0)
                    return "\"" + pInputString + "*\"";
                return pInputString;
            }

            // Get rid of the quotes (if any)
            pInputString = pInputString.Replace("\"", "");

            // Split on white space and brackets, returning the brackets
            var regex = new Regex(@"\s+|([\)])|([\(])", RegexOptions.None);
            var splitinput = regex.Split(pInputString);
            var phraseString = "";
            var outputString = "";

            for (int i = 0; i < splitinput.Length; i++) {
                string tmpStr = splitinput[i].Trim();

                if (tmpStr == string.Empty)
                    continue;

                if ((tmpStr == "AND") || (tmpStr == "OR") ||
                    (tmpStr == "NOT") || (tmpStr == "(") ||
                    (tmpStr == ")")) {
                    if (phraseString != string.Empty) {
                        // Quote and wildcard the string
                        if (outputString != string.Empty)
                            outputString += " " + '\"' + phraseString + "*\"";
                        else
                            outputString = '\"' + phraseString + "*\"";

                        phraseString = "";
                    }

                    if (outputString != string.Empty)
                        outputString += " " + tmpStr;
                    else
                        outputString = tmpStr;
                }
                else {
                    if (phraseString != string.Empty)
                        phraseString += " " + tmpStr.Replace("\'", "\'\'");
                    else
                        phraseString = tmpStr.Replace("\'", "\'\'"); ;
                }

                if (((i + 1) < splitinput.Length) && ((splitinput[i + 1] == "AND") ||
                    (splitinput[i + 1] == "OR") || (splitinput[i + 1] == "NOT") ||
                    (splitinput[i + 1] == "(") || (splitinput[i + 1] == ")"))) {
                    if (phraseString != string.Empty) {
                        // Quote and wildcard the string
                        if (outputString != string.Empty)
                            outputString += " " + '\"' + phraseString + "*\"";
                        else
                            outputString = '\"' + phraseString + "*\"";

                        phraseString = "";
                    }
                }
            }

            // Do the last entry
            if (phraseString != string.Empty) {
                // Quote and wildcard the string
                if (outputString != string.Empty)
                    outputString += " " + '\"' + phraseString + "*\"";
                else
                    outputString = '\"' + phraseString + "*\"";
            }

            // Clean brackets
            outputString = outputString.Replace("( ", "(").Replace(" )", ")");

            return outputString;
        }

        /// <summary>
        /// Converts a search string with booleans (AND, OR and NOT), quotes and brackets in to a SQL stored procedure happy format.
        /// </summary>
        /// <param name="pInputString">Input search string.</param>
        /// <returns>Cleaned input search string suitable to be passed in to a SQL stored procedure.</returns>
        private static string ConvertFieldSearchString(string pInputString) {
            if (string.IsNullOrWhiteSpace(pInputString))
                return "";

            if (!(pInputString.Contains("AND") || pInputString.Contains("OR") || pInputString.Contains("NOT") ||
                  pInputString.Contains('\"'))) {
                return pInputString.Replace("'", "''");
            }

            // Get rid of the quotes (if any)
            pInputString = pInputString.Replace("\"", "");

            // Split on white space and brackets, returning the brackets
            var regex = new Regex(@"\s+|([\)])|([\(])", RegexOptions.None);
            var splitinput = regex.Split(pInputString);
            var phraseString = "";
            var outputString = "";

            for (int i = 0; i < splitinput.Length; i++) {
                var tmpStr = splitinput[i].Trim();

                if (tmpStr == string.Empty)
                    continue;

                if ((tmpStr == "AND") || (tmpStr == "OR") || (tmpStr == "NOT") || (tmpStr == "(") || (tmpStr == ")")) {
                    if (phraseString != string.Empty) {
                        if (outputString != string.Empty)
                            outputString += " <fieldname> like '%" + phraseString + "%'";
                        else
                            outputString = "<fieldname> like '%" + phraseString + "%'";

                        phraseString = "";
                    }

                    if (outputString != string.Empty)
                        outputString += " " + tmpStr;
                    else
                        outputString = tmpStr;
                }
                else {
                    if (phraseString != string.Empty)
                        phraseString += " " + tmpStr.Replace("\'", "\'\'");
                    else
                        phraseString = tmpStr.Replace("\'", "\'\'");
                }

                if (((i + 1) < splitinput.Length) &&
                    ((splitinput[i + 1] == "AND") || (splitinput[i + 1] == "OR") ||
                     (splitinput[i + 1] == "NOT") || (splitinput[i + 1] == "(") ||
                     (splitinput[i + 1] == ")"))) {
                    if (phraseString != string.Empty) {
                        if (outputString != string.Empty)
                            outputString += " <fieldname> like '%" + phraseString + "%'";
                        else
                            outputString = "<fieldname> like '%" + phraseString + "%'";

                        phraseString = "";
                    }
                }
            }

            // Do the last entry
            if (phraseString != string.Empty) {
                if (outputString != string.Empty)
                    outputString += " <fieldname> like '%" + phraseString + "%'";
                else
                    outputString = "<fieldname> like '%" + phraseString + "%'";
            }

            // Clean brackets
            outputString = outputString.Replace("( ", "(").Replace(" )", ")");

            return outputString;
        }

        #endregion
    }
}
