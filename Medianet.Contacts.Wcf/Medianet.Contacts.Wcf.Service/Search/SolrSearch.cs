﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Caching;
using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.Solr;
using NLog;
using SolrNet.Exceptions;
using Medianet.Contacts.Wcf.Service.Common;
using System.Configuration;

namespace Medianet.Contacts.Wcf.Service.Search
{
    public class SolrSearch : SearchBase
    {
        private readonly Searcher _searcher;
        private readonly SqlSearch _sqlSearch;
        private readonly Logger _logger;
        private static Dictionary<string, DateTime> LastEmailDates = new Dictionary<string, DateTime>();
        private const string ErrorEmailSubject = "MedianetContactsWCFService SolrSearch Exception";
        private const string ErrorEmailContent = "An unhandled exception has occurred on the SolrSearch class - {0} method \r\nError details {1}";

        public SolrSearch() {
            _searcher = ServiceLocator.Resolve<Searcher>();
            _sqlSearch = ServiceLocator.Resolve<SqlSearch>();
            _logger = LogManager.GetCurrentClassLogger();

            if (LastEmailDates.Count == 0)
            {
                LastEmailDates.Add("Search", DateTime.MinValue);
                LastEmailDates.Add("SearchWithExpandedOutlets", DateTime.MinValue);
                LastEmailDates.Add("AutoSuggest", DateTime.MinValue);
                LastEmailDates.Add("FetchRecordIdentifiers", DateTime.MinValue);
            }
        }
        
        /// <summary>
        /// Searches the specified <c>SearchOptions</c> object.
        /// </summary>
        /// <param name="totalResults">The total results returned.</param>
        /// <param name="so">The <c>SearchOptions</c> object.</param>
        /// <param name="sortCol">The sort column.</param>
        /// <param name="sortDir">The sort direction.</param>
        /// <param name="startPos">The start position to return results from.</param>
        /// <param name="count">The number of results to return.</param>
        /// <param name="pinned">The pinned items.</param>
        /// <param name="deleted">The deleted items.</param>
        /// <param name="unselected">The selected items.</param>
        /// <param name="newSearch">Perform a new search if <c>true</c>.</param>
        /// <param name="userid">The current user's id.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<SearchResult> Search(
             out int totalResults,
             SearchOptions so,
             SortColumn sortCol,
             SortDirection sortDir,
             bool newSearch,
             int userid,
             int? startPos = null,
             int? count = null,
             List<RecordIdentifier> pinned = null,
             List<RecordIdentifier> deleted = null,
             List<RecordIdentifier> unselected = null
        ) {
            List<SearchResult> results = null;
            SortColumn oldSortCol = SortColumn.SolrScore;
            SortDirection oldSortDir = SortDirection.ASC;
            try {
                // If it isn't a new search then look for the results in the cache.
                if (!newSearch) {
                    var cacheResult = SearchDataCache.Get(userid.ToString(CultureInfo.InvariantCulture), so);
                    if (cacheResult != null) {
                        results = cacheResult.Results.Select(item => (SearchResult)item.Clone()).ToList();
                        oldSortCol = cacheResult.SortCol;
                        oldSortDir = cacheResult.SortDir;
                    }
                }

                if (results == null) {
                    results = so.SearchWithDB
                                  ? _sqlSearch.Search(out totalResults, so, sortCol, sortDir, newSearch, userid)
                                  : _searcher.Search(so, GetDebtorNumber(userid));
                }
            }
            catch (Exception e) {
                _logger.ErrorException("Search failed." + (so.SearchWithDB ? "" : "Falling back to SQL search."), e);
                _logger.Debug("SearchOptions: " + so.ToString());

                SendEmailForException("Search", e);

                // If we searched on Solr and failed then try SQL.
                if (!so.SearchWithDB) {
                    try {
                        results = _sqlSearch.Search(out totalResults, so, sortCol, sortDir, newSearch, userid);
                    }
                    catch (Exception ex) {
                        _logger.ErrorException("Search failed with both, Solr and SQL. Log and pretend no results were found.", ex);
                        results = new List<SearchResult>();
                    }
                }
            }

            ConfigurePinnedRecords(results, pinned, true, userid);

            // Add it to the cache by making a deep clone.
            // If the data isn't sorted the way we want then sort it.
            if (newSearch || (sortCol != oldSortCol || sortDir != oldSortDir))
                results = SortResults(results, sortCol, sortDir, so);

            // Add it to the cache by making a deep clone.
            SearchDataCache.Insert(userid.ToString(CultureInfo.InvariantCulture), results.Select(item => (SearchResult)item.Clone()).ToList(),
                                   so, sortCol, sortDir);

            return FetchRange(out totalResults, results, startPos.HasValue ? startPos.Value : 0,
                              count.HasValue ? count.Value : int.MaxValue, pinned, deleted, unselected, userid);
        }

        /// <summary>
        /// Get's all the outlets in a search and expands them to contacts.
        /// </summary>
        /// <param name="totalResults">The total results returned.</param>
        /// <param name="searchOptions">The search options.</param>
        /// <param name="sortColumn">The sort column.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <param name="startPos">The start position to return results from.</param>
        /// <param name="count">The number of results to return.</param>
        /// <param name="pinned">The pinned items.</param>
        /// <param name="deleted">The deleted items.</param>
        /// <param name="unselected">The unselected items.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<SearchResult> SearchWithExpandedOutlets(
            out int totalResults,
            SearchOptions searchOptions,
            SortColumn sortColumn,
            SortDirection sortDirection,
            int userid,
            int? startPos = null,
            int? count = null,
            List<RecordIdentifier> pinned = null,
            List<RecordIdentifier> deleted = null,
            List<RecordIdentifier> unselected = null
        ) {
            List<SearchResult> results;
            var contacts = new List<RecordIdentifier>();
            var expanded = new List<SearchResult>();

            try {
                results = Search(out totalResults, searchOptions, sortColumn, sortDirection, false, userid);
                if (!deleted.IsNullOrEmpty()) results.RemoveAll(r => deleted.Contains(r));
            }
            catch (Exception e) {
                _logger.ErrorException("SearchWithExpandedOutlets in Solr failed. Falling back to SQL search.", e);

                SendEmailForException("SearchWithExpandedOutlets", e);

                results = _sqlSearch.Search(out totalResults, searchOptions, sortColumn, sortDirection, false, userid,
                                            startPos, count, pinned, deleted, unselected);
            }

            foreach (var r in results) {
                if (!r.IsContact) {
                    if (!string.IsNullOrWhiteSpace(r.ContactIds)) {
                        var contactIds = r.ContactIds.Split('#');
                        RecordType rType;

                        if (r.IsContact) rType = r.Type;
                        else if (r.Type == RecordType.MediaOutlet) rType = RecordType.MediaContact;
                        else if (r.Type == RecordType.PrnOutlet) rType = RecordType.PrnContact;
                        else if (r.Type == RecordType.OmaOutlet) rType = RecordType.OmaContactAtOmaOutlet;
                        else rType = RecordType.Unknown;

                        contacts.AddRange(
                            contactIds.Select(contactId => new RecordIdentifier(r.OutletID, contactId, rType)));
                    }
                    else
                        expanded.Add(r); // This outlet has no contacts. Keep the outlet.
                }
                else
                    expanded.Add(r); // It's already a contact.
            }
            // only add what not in expanded list
            var expandedoutlets = FetchRecordIdentifiers(contacts, sortColumn, sortDirection, userid, null);
            var extraItems = expandedoutlets.Where(p => expanded.All(p2 => p2.Id != p.Id)).ToList();

            expanded.AddRange(extraItems);
            expanded = SortResults(expanded, sortColumn, sortDirection, searchOptions);
            return FetchRange(out totalResults, expanded, startPos.HasValue ? startPos.Value : 0,
                              count.HasValue ? count.Value : int.MaxValue, pinned, deleted, unselected, userid);
        }

        /// <summary>
        /// Fetches the range.
        /// </summary>
        /// <param name="totalResults"> </param>
        /// <param name="results">The results.</param>
        /// <param name="startPos">The start pos.</param>
        /// <param name="count">The count.</param>
        /// <param name="pinned">The pinned.</param>
        /// <param name="deleted">The deleted.</param>
        /// <param name="unselected">The unselected.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>The range out of the given results.</returns>
        public List<SearchResult> FetchRange(
            out int totalResults,
            List<SearchResult> results,
            int startPos, int count,
            List<RecordIdentifier> pinned,
            List<RecordIdentifier> deleted,
            List<RecordIdentifier> unselected,
            int userid
        ) {
            // Remove deleted items.
            if (deleted != null)
                results.RemoveAll(deleted.Contains);

            // Remove pinned items in the search and append any remaining.
            totalResults = ConfigurePinnedRecords(results, pinned, (results.Count - startPos < count), userid);
            var actualCount = Math.Min(count, results.Count - startPos);

            // Deselect items.
            if (unselected != null)
                results.Where(unselected.Contains)
                    .ForEach(r => r.Selected = false);

            if (startPos < results.Count && actualCount > 0)
                return results.GetRange(startPos, actualCount);

            return new List<SearchResult>();
        }

        /// <summary>
        /// Returns results for the auto suggest.
        /// </summary>
        /// <param name="options">The <c>AutoSuggestOptions</c> object.</param>
        /// <param name="maxResults">The max results.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<AutoSuggest> AutoSuggest(
            AutoSuggestOptions options,
            int maxResults,
            int userid
        ) {
            var debtorNumber = GetDebtorNumber(userid);
            List<AutoSuggest> result = null;

            try {
                if (options.SearchWithDB())
                    result = _sqlSearch.AutoSuggest(options, maxResults, userid);
                else
                    result = _searcher.AutoSuggest(options.Context, options.SearchText, options.SearchAustralia, options.SearchNewZealand,
                                          options.SearchAllCountries, maxResults, debtorNumber);
            }
            catch (InvalidFieldException) { }
            catch (Exception e) {
                _logger.ErrorException("AutoSuggest in Solr failed. Falling back to SQL search.", e);

                SendEmailForException("AutoSuggest", e);

                result = _sqlSearch.AutoSuggest(options, maxResults, userid);
            }
            return result;
        }

        /// <summary>
        /// Search Solr full Name field with RecordType 2 ( media contact) for auto suggest.
        /// </summary>
        /// <param name="totalResults"></param>
        /// <param name="searchOptions"></param>
        /// <param name="startPos"></param>
        /// <param name="maxResults"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public List<SearchResult> SearchEmployees(out int totalResults, SearchOptions searchOptions, int startPos, int maxResults, int userid)
        {
            var results = _searcher.SearchEmployee(searchOptions, startPos, maxResults, out totalResults);
            return results;
        }

        /// <summary>
        /// Converts a list of <c>RecordIdentifier</c>s into a list of <c>SearchResult</c>.
        /// </summary>
        /// <param name="ri">The list of RecordIdentifiers.</param>
        /// <param name="sortCol">The column to sort on.</param>
        /// <param name="sortDir">The direction of the sorting.</param>
        /// <param name="userid">The current user's id.</param>
        /// <returns>
        /// A <c>List&lt;SearchResult&gt;</c>s of matching results.
        /// </returns>
        protected override List<SearchResult> FetchRecordIdentifiers(
            List<RecordIdentifier> ri,
            SortColumn sortCol,
            SortDirection sortDir,
            int userid,
            SearchOptions so
        ) {
            try {
                var results = _searcher.Search(ri, GetDebtorNumber(userid));
                if (!results.IsNullOrEmpty()) SortResults(results, sortCol, sortDir, so);
                return results;
            }
            catch (Exception e) {
                _logger.ErrorException("FetchRecordIdentifiers in Solr failed. Falling back to SQL search.", e);

                SendEmailForException("FetchRecordIdentifiers", e);

                return base.FetchRecordIdentifiers(ri, sortCol, sortDir, userid, null);
            }
        }

        /// <summary>
        /// Get's a debtor number for the given user id.
        /// </summary>
        /// <param name="userid">The user id.</param>
        /// <returns>The debtor number for the user id.</returns>
        private string GetDebtorNumber(int userid) {
            string debtorNumber;

            using (var context = new MedianetContactsContext()) {
                var user = context.Users.SingleOrDefault(u => u.Id == userid);
                debtorNumber = user.DebtorNumber;
            }
            return debtorNumber;
        }

        private void SendEmailForException(string type, Exception e)
        {
            DateTime lastEmaildate = LastEmailDates[type];

            if (DateTime.Now >= lastEmaildate.AddMinutes(1))
            {
                EmailHelper.SendEmail(ConfigurationManager.AppSettings["MailToAddresses"],
                    ErrorEmailSubject,
                    string.Format(ErrorEmailContent, type, e.ToString()),
                    false);

                LastEmailDates[type] = DateTime.Now;
            }
        }
    }
}
