﻿using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;
using System;

namespace Medianet.Contacts.Wcf.Service.Search
{
    public class SearchBase
    {
        /// <summary>
        /// Add pinned records to the bottom of a result if not already in the result.
        /// </summary>
        /// <param name="results">The results that the pinned records will be added to.</param>
        /// <param name="pinned">The pinned records to add.</param>
        /// <param name="append"> </param>
        /// <param name="userid"> </param>
        protected virtual int ConfigurePinnedRecords(
            List<SearchResult> results,
            List<RecordIdentifier> pinned, bool append, int userid
        )
        {
            if (pinned != null && results != null)
            {
                // Create a copy of the pinned items so we don't modify the session variable.
                var newPinned = new List<RecordIdentifier>(pinned);
                int pinnedNotAppended = 0;

                results.ForEach(r => r.Pinned = false);

                if (newPinned.Count > 0)
                {
                    // If any pinned records are already in the search results, remove them from
                    // the pinned results and set them to pinned in the search results.
                    results.Where(r => newPinned.Contains(r))
                        .ForEach(r => r.Pinned = true);
                    newPinned = newPinned.Except(results).ToList();

                    // Only fetch and add the pinned items if we don't have 
                    // enough to fill the current page.
                    if (append && newPinned.Count > 0)
                    {
                        // Fetch the pinned records, set them all to pinned and
                        // add all the pinned records to the end.
                        var pinnedResults = FetchRecordIdentifiers(newPinned, SortColumn.OutletName, SortDirection.ASC,
                                                                   userid, null);
                        pinnedResults.ForEach(p =>
                        {
                            p.Pinned = true;
                            p.BelongsToCurrentSearch = false;
                        });
                        results.AddRange(pinnedResults);
                    }
                    else // Record how many we didn't add.
                        pinnedNotAppended = newPinned.Count;
                }
                return results.Count + pinnedNotAppended;
            }
            return results.IsNullOrEmpty() ? 0 : results.Count;
        }

        /// <summary>
        /// Converts a list of <c>RecordIdentifier</c>s into a list of <c>SearchResult</c>.
        /// </summary>
        /// <param name="ri">The list of RecordIdentifiers.</param>
        /// <param name="sortCol">The column to sort on.</param>
        /// <param name="sortDir">The direction of the sorting.</param>
        /// <param name="userid">The current user's id.</param>
        /// <returns>
        /// A <c>List&lt;SearchResult&gt;</c>s of matching results.
        /// </returns>
        protected virtual List<SearchResult> FetchRecordIdentifiers(
            List<RecordIdentifier> ri,
            SortColumn sortCol,
            SortDirection sortDir,
            int userid,
            SearchOptions so
        )
        {
            List<SearchResult> results = null;
            using (var context = new MedianetContactsSPEntities())
            {
                results = context.GetContactsByIds(ri.Join(","), sortCol.ToString(), sortDir.ToString(), userid)
                    .Select(x => Mapper.Map<SearchResultModel, SearchResult>(x))
                    .ToList();
            }

            return results;
        }

        protected List<SearchResult> SortResults(
            List<SearchResult> results,
            SortColumn sortCol,
            SortDirection sortDir,
            SearchOptions options
        )
        {
            switch (sortCol)
            {
                case SortColumn.ContactName:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.ContactDisplay).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.ContactDisplay).ToList();
                    break;
                case SortColumn.JobTitle:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.JobTitle).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.JobTitle).ToList();
                    break;
                case SortColumn.Subject:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.SubjectDisplay).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.SubjectDisplay).ToList();
                    break;
                case SortColumn.MediaType:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.ProductTypeName).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.ProductTypeName).ToList();
                    break;
                case SortColumn.Location:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.CityName).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.CityName).ToList();
                    break;
                case SortColumn.ServiceName:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.ServiceName).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.ServiceName).ToList();
                    break;
                case SortColumn.UpdatedDate:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.ModifiedDate).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.ModifiedDate).ToList();
                    break;
                case SortColumn.UpdatedUser:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.ModifiedUser).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.ModifiedUser).ToList();
                    break;
                case SortColumn.DistributionType:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.DistributionType).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.DistributionType).ToList();
                    break;
                case SortColumn.ContactInfluencerScore:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.ContactMediaInfluencerScore).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.ContactMediaInfluencerScore).ToList();
                    break;
                case SortColumn.OutletName:
                    if (sortDir == SortDirection.ASC)
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.OutletName).ThenBy(m => m.ContactDisplay).ToList();
                    else
                        results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenByDescending(m => m.OutletName).ThenByDescending(m => m.ContactDisplay).ToList();
                    break;
                case SortColumn.SolrScore:
                case SortColumn.Default:
                    //if (sortDir == SortDirection.ASC)
                    //    results = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch).ThenBy(m => m.Score??0).ThenBy(m => m.OutletName).ToList();
                    //else
                    var sorted = results.OrderByDescending(m => m.Pinned && !m.BelongsToCurrentSearch);

                    if (options != null)
                    {
                        if (options.Type == SearchType.Quick || options.Type == SearchType.Advanced)
                        {
                            string searchText;
                            var searchContext = options.Context;

                            if (options.Type == SearchType.Quick)
                                searchText = (options as SearchOptionsQuick).SearchText;
                            else
                                searchText = (options as SearchOptionsAdvanced).SearchText;

                            if (!string.IsNullOrWhiteSpace(searchText))
                            {
                                if (searchContext == SearchContext.Outlet || searchContext == SearchContext.Both)
                                {
                                    // If the search text matches the outlet name then sort matching outlets by outlet name alphabetically
                                    // followed by matching contact records by outlet name alphabetically
                                    sorted = sorted.ThenBy(m => m.OutletName == null ? "zzzz" : (m.OutletName.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) >= 0 ? (m.IsOutlet ? "_" + m.OutletName : m.OutletName) : "zzzz"));
                                }
                                if (searchContext == SearchContext.People || searchContext == SearchContext.Both)
                                {
                                    // If the search text matches the contact name then sort matching contacts by contact name alphabetically
                                    sorted = sorted.ThenBy(m => m.ContactDisplay == null ? "zzzz" : (m.ContactDisplay.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) >= 0 ? m.ContactDisplay : "zzzz"));
                                }
                            }
                        }
                    }

                    results = sorted.ThenBy(m => m.IsOutlet ? 1 : 2).ThenBy(m => m.OutletName).ThenBy(m => m.ContactDisplay).ToList();
                    break;
            }

            return results;
        }
    }
}
