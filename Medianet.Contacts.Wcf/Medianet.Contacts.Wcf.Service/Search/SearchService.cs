﻿using System.Collections.Generic;
using System.ServiceModel;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Contracts;
using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.WCF;

namespace Medianet.Contacts.Wcf.Service.Search
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehavior(typeof(ServiceErrorHandler))]
    public class SearchService : ISearchService
    {
        private readonly SolrSearch _solrSearcher;
        private readonly SqlSearch _sqlSearcher;

        public SearchService() {
            // The windows service host normally does this but we do it here when running in debug mode.
            AutoMapperConfigurator.Configure();

            _solrSearcher = ServiceLocator.Resolve<SolrSearch>();
            _sqlSearcher = ServiceLocator.Resolve<SqlSearch>();
        }

        /// <summary>
        /// Searches the specified <c>SearchOptions</c> object.
        /// </summary>
        /// <param name="totalResults"> </param>
        /// <param name="so">The <c>SearchOptions</c> object.</param>
        /// <param name="sortCol">The sort column.</param>
        /// <param name="sortDir">The sort direction.</param>
        /// <param name="startPos">The start position to return results from.</param>
        /// <param name="count">The number of results to return.</param>
        /// <param name="pinned">The pinned items.</param>
        /// <param name="deleted">The deleted items.</param>
        /// <param name="unselected">The selected items.</param>
        /// <param name="newSearch">Perform a new search if <c>true</c>.</param>
        /// <param name="userid">The current user's id.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<SearchResult> Search(
            out int totalResults,
            SearchOptions so,
            SortColumn sortCol,
            SortDirection sortDir,
            bool newSearch,
            int userid,
            int? startPos = null,
            int? count = null,
            List<RecordIdentifier> pinned = null,
            List<RecordIdentifier> deleted = null,
            List<RecordIdentifier> unselected = null
        ) {
            if (Config.ShouldUseSolr())
                return _solrSearcher.Search(out totalResults, so, sortCol, sortDir, newSearch, userid, startPos, count, pinned,
                                  deleted, unselected);

            return _sqlSearcher.Search(out totalResults, so, sortCol, sortDir, newSearch, userid, startPos, count, pinned,
                                       deleted, unselected);
        }

        /// <summary>
        /// Searches the with expanded outlets.
        /// </summary>
        /// <param name="totalResults"> </param>
        /// <param name="searchOptions">The search options.</param>
        /// <param name="sortColumn">The sort column.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <param name="startPos"> </param>
        /// <param name="count"> </param>
        /// <param name="pinned">The pinned items.</param>
        /// <param name="deleted">The deleted items.</param>
        /// <param name="unselected">The unselected items.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<SearchResult> SearchWithExpandedOutlets(
            out int totalResults,
            SearchOptions searchOptions,
            SortColumn sortColumn,
            SortDirection sortDirection,
            int userid,
            int? startPos = null,
            int? count = null,
            List<RecordIdentifier> pinned = null,
            List<RecordIdentifier> deleted = null,
            List<RecordIdentifier> unselected = null
        ) {
            if (Config.ShouldUseSolr())
                return _solrSearcher.SearchWithExpandedOutlets(out totalResults, searchOptions, sortColumn,
                                                               sortDirection, userid, startPos, count, pinned, deleted,
                                                               unselected);
            
            return _sqlSearcher.SearchWithExpandedOutlets(out totalResults, searchOptions, sortColumn,
                                                          sortDirection, userid, startPos, count, pinned, deleted,
                                                          unselected);
        }

        /// <summary>
        /// Fetches the range.
        /// </summary>
        /// <param name="totalResults"> </param>
        /// <param name="results">The results.</param>
        /// <param name="startPos">The start pos.</param>
        /// <param name="count">The count.</param>
        /// <param name="pinned">The pinned.</param>
        /// <param name="deleted">The deleted.</param>
        /// <param name="unselected">The unselected.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>The range out of the given results.</returns>
        public List<SearchResult> FetchRange(
            out int totalResults,
            List<SearchResult> results,
            int startPos,
            int count,
            List<RecordIdentifier> pinned,
            List<RecordIdentifier> deleted,
            List<RecordIdentifier> unselected, int userid
        ) {
            if (Config.ShouldUseSolr())
                return _solrSearcher.FetchRange(out totalResults, results, startPos, count, pinned, deleted, unselected,
                                                userid);
            
            return _sqlSearcher.FetchRange(out totalResults, results, startPos, count, pinned, deleted, unselected,
                                           userid);
        }

        /// <summary>
        /// Returns results for the auto suggest.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="searchAustralia">if set to <c>true</c> [search australia].</param>
        /// <param name="searchNewZealand">if set to <c>true</c> [search new zealand].</param>
        /// <param name="searchAllCountries">if set to <c>true</c> [search all countries].</param>
        /// <param name="maxResults">The max results.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<AutoSuggest> AutoSuggest(
            SearchContext context,
            string searchText,
            bool searchAustralia,
            bool searchNewZealand,
            bool searchAllCountries,
            int maxResults,
            int userid
        ) {
            var options = new AutoSuggestOptions(context, searchText, false, searchAustralia, searchNewZealand, searchAllCountries);

            if (Config.ShouldUseSolr())
                return _solrSearcher.AutoSuggest(options, maxResults, userid);

            return _sqlSearcher.AutoSuggest(options, maxResults, userid);
        }

        /// <summary>
        /// Returns results for the auto suggest.
        /// </summary>
        /// <param name="options">The <c>AutoSuggestOptions</c> object.</param>
        /// <param name="maxResults">The max results.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>SearchResult</c> objects.</returns>
        public List<AutoSuggest> AutoSuggestComplex(
            AutoSuggestOptions options,
            int maxResults,
            int userid
        ) {
            if (Config.ShouldUseSolr())
                return _solrSearcher.AutoSuggest(options, maxResults, userid);

            return _sqlSearcher.AutoSuggest(options, maxResults, userid);
        }

        public List<SearchResult> SearchEmployees(out int totalResults, SearchOptions searchOptions, int userid, int startPos, int maxResults)
        {
            // This routin only searches solr.
            return _solrSearcher.SearchEmployees(out totalResults, searchOptions, startPos, maxResults, userid);
        }
    }
}
