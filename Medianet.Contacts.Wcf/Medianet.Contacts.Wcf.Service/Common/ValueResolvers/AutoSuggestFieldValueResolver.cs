﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class AutoSuggestFieldValueResolver : ValueResolver<ContactModel, string>
    {
        protected override string ResolveCore(ContactModel source)
        {
            string result = string.Empty;
            switch ((RecordType)source.RecordType)
            {
                case RecordType.MediaOutlet:
                case RecordType.PrnOutlet:
                case RecordType.OmaOutlet:
                    result = source.OutletName;
                    break;
                case RecordType.MediaContact:
                case RecordType.PrnContact:
                case RecordType.OmaContactNoOutlet:
                case RecordType.OmaContactAtMediaOutlet:
                case RecordType.OmaContactAtPrnOutlet:
                case RecordType.OmaContactAtOmaOutlet:
                    result += !string.IsNullOrWhiteSpace(source.FirstName) ? source.FirstName : string.Empty;
                    result += !string.IsNullOrWhiteSpace(source.MiddleName) ? " " + source.MiddleName : string.Empty;
                    result += !string.IsNullOrWhiteSpace(source.LastName) ? " " + source.LastName : string.Empty;
                    break;
            }
            return result;

        }
    }
}
