﻿using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Model;
using System.Collections.Generic;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class ContactModelTextFullSearchResolver : ValueResolver<ContactModel, string>
    {
        protected override string ResolveCore(ContactModel source) {
            string result = string.Empty;

            if (source != null) {
                List<string> fieldsList = new List<string>() { source.OutletName };

                if (source.IsOutlet)
                {
                    fieldsList.Add(source.OutletSubjectNames);
                    fieldsList.Add(source.OutletSystemNotes);
                    fieldsList.Add(source.OutletTwitter);
                    fieldsList.Add(source.OutletBio);
                    fieldsList.Add(source.OutletAlsoKnownAs);
                    fieldsList.Add(source.OutletAdditionalWebsite);
                    fieldsList.Add(source.OutletPublishedOn);
                    fieldsList.Add(source.OutletPressReleaseInterests);
                    fieldsList.Add(source.OutletRegionsCovered);
                }
                else {
                    fieldsList.Add(source.FirstName);
                    fieldsList.Add(source.MiddleName);
                    fieldsList.Add(source.LastName);
                    fieldsList.Add(source.JobTitle);
                    fieldsList.Add(source.ContactSubjectNames);
                    fieldsList.Add(source.ContactSystemNotes);
                    fieldsList.Add(source.ContactTwitter);
                    fieldsList.Add(source.ContactBio);
                    fieldsList.Add(source.ContactBugBears);
                    fieldsList.Add(source.ContactAlsoKnownAs);
                    fieldsList.Add(source.ContactPressReleaseInterests);
                    fieldsList.Add(source.ContactPersonalInterests);
                    fieldsList.Add(source.ContactAppearsIn);
                    fieldsList.Add(source.ContactCurrentStatus);
                    fieldsList.Add(source.ContactBasedInLocation);
                    fieldsList.Add(source.ContactWebsite);
                    fieldsList.Add(source.ContactAuthorOf);
                }

                result = string.Join<string>(" ", fieldsList.Where(s => !string.IsNullOrEmpty(s)));
            }

            return result.TrimIfNotNull();
        }
    }
}