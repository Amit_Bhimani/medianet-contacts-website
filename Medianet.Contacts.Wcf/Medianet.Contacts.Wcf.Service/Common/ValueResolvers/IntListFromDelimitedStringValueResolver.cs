﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class IntListFromDelimitedStringValueResolver : ValueResolver<string, ICollection<int>>
    {
        protected override ICollection<int> ResolveCore(string source) {
            ICollection<int> result = null;

            if (!string.IsNullOrWhiteSpace(source))
                result = source.Split('#').Select(x => {
                    int val;
                    return int.TryParse(x, out val) ? val : 0;
                }).ToList();

            return result;
        }
    }
}