﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Medianet.Contacts.Wcf.Model;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class NaStripperValueResolver : ValueResolver<string, string>
    {

        protected override string ResolveCore(string source)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(source))
            {
                if (source.Trim().ToLower() != "na" && source.Trim().ToLower() != "n/a")
                {
                    result = source.Trim();
                }
            }
            return result;
        }
    }
}
