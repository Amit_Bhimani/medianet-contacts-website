﻿using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class ContactHashListDisplayValueResolver : ValueResolver<string, string>
    {
        private const char Separator = '#';

        protected override string ResolveCore(string source) {
            string display = source ?? string.Empty;
            
            if(!string.IsNullOrWhiteSpace(display)) {
                display = string.Format("{0:d} {1}", source.Split(Separator).Length.ToString() , (source.Contains(Separator) ? "Contacts" : "Contact"));
            }

            return display.TrimIfNotNull();
        }
    }
}