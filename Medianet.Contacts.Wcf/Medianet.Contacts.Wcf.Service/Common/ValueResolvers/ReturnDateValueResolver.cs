﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Medianet.Contacts.Wcf.Model;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class ReturnDateValueResolver : ValueResolver<ContactModel, DateTime?>
    {
        protected override DateTime? ResolveCore(ContactModel source)
        {
            // Only contacts have a return date
            if (source.IsOutlet)
                return null;

            // If there's no start date then in doesn't make sense to have a return date
            if (!source.ContactOooStartDate.HasValue)
                return null;

            // If there's a start date and return date then keep the original return date
            if (source.ContactOooReturnDate.HasValue)
                return source.ContactOooReturnDate;

            // We don't know the return date, so just set it to far in the future
            // to make the solr query easier
            return DateTime.Now.AddYears(10);
        }
    }
}
