﻿using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Service.Solr.Documents;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class SolrContactIdValueResolver : ValueResolver<SolrContact, string>
    {
        protected override string ResolveCore(SolrContact source) {
            if (source.IsOutlet) {
                return !source.ContactIds.IsNullOrEmpty() ? source.ContactIds.ToList().Join("#") : string.Empty;
            }
            return source.ContactId;
        }
    }
}
