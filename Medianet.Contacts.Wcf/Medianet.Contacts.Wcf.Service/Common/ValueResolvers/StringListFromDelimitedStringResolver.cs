﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class StringListFromHashDelimitedStringResolver : ValueResolver<string, ICollection<string>>
    {
        protected override ICollection<string> ResolveCore(string source) {
            ICollection<string> result = null;

            if (!string.IsNullOrWhiteSpace(source))
                result = source.Split('#').ToList();

            return result;
        }
    }
}