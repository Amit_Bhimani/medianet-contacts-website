﻿using System.Linq;
using AutoMapper;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class SubjectDisplayValueResolver : ValueResolver<string, string>
    {
        private const char SEPARATOR = '#';

        protected override string ResolveCore(string source) {
            string display = source ?? string.Empty;

            if (!string.IsNullOrWhiteSpace(display) && source.Contains(SEPARATOR)) {
                display = string.Format("{0:d} {1}", source.Split(SEPARATOR).Length.ToString(), "Subjects");
            }

            return display;
        }
    }
}