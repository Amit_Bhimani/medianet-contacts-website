﻿using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Service.Solr.Documents;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class SolrContactSubjectNameValueResolver : ValueResolver<SolrContact, string>
    {
        protected override string ResolveCore(SolrContact source) {
            var result = string.Empty;
            if (!source.IsOutlet) {
                if (!source.ContactSubjectNames.IsNullOrEmpty())
                    result = source.ContactSubjectNames.ToList().Join(", ");
            }
            else if (source.OutletSubjectNames != null && source.OutletSubjectNames.Count > 0)
                result = source.OutletSubjectNames.ToList().Join(", ");

            return result;
        }
    }
}