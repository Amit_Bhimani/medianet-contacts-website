﻿using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Service.Solr.Documents;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class SolrContactFullNameValueResolver : ValueResolver<SolrContact, string>
    {
        protected override string ResolveCore(SolrContact source) {
            string result = string.Empty;
            if (source != null) {
                if (source.IsOutlet) {
                    if (!source.ContactNames.IsNullOrEmpty())
                        result = source.ContactNames.ToList().Join(", ");
                }
                else {
                    result += !source.FirstName.IsNullOrWhitespace() ? source.FirstName : string.Empty;
                    result += !source.MiddleName.IsNullOrWhitespace() ? " " + source.MiddleName : string.Empty;
                    result += !source.LastName.IsNullOrWhitespace() ? " " + source.LastName : string.Empty;
                }
            }
            return result.TrimIfNotNull();
        }
    }
}