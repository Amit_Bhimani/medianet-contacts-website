﻿using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class CommaDelimitedStringFromHashDelimitedStringResolver : ValueResolver<string, string>
    {
        protected override string ResolveCore(string source) {
            var result = string.Empty;

            if (!source.IsNullOrWhitespace()) {
                result = source.Contains("#") ? source.Split('#').Join(", ") : source;
            }
            return result;
        }
    }
}