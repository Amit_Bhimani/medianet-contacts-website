﻿using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class SearchResultModelContactFullNameValueResolver : ValueResolver<string, string>
    {
        protected override string ResolveCore(string source) {
            var display = string.Empty;

            if (!source.IsNullOrWhitespace()) {
                display = source.Contains("#") ? source.Split('#').Join(", ") : source;
            }
            return display;
        }
    }
}
