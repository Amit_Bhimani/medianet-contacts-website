﻿using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Service.Solr.Documents;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class SolrContactDisplayValueResolver : ValueResolver<SolrContact, string>
    {
        protected override string ResolveCore(SolrContact source) {
            string display = string.Empty;
            if (source.IsOutlet) {
                if (source.ContactNames != null && source.ContactNames.Count > 0) {
                    if (source.ContactNames.Count == 1)
                        display = source.ContactNames.Count + " Contact";
                    else
                        display = source.ContactNames.Count + " Contacts";
                }
            }
            else {
                display += !source.FirstName.IsNullOrWhitespace() ? source.FirstName : string.Empty;
                display += !source.MiddleName.IsNullOrWhitespace() ? " " + source.MiddleName : string.Empty;
                display += !source.LastName.IsNullOrWhitespace() ? " " + source.LastName : string.Empty;
            }
            return display.TrimIfNotNull();
        }
    }
}