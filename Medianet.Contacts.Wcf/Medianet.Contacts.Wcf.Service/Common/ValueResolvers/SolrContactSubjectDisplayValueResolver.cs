﻿using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.Service.Solr.Documents;

namespace Medianet.Contacts.Wcf.Service.Common.ValueResolvers
{
    public class SolrContactSubjectDisplayValueResolver : ValueResolver<SolrContact, string>
    {
        protected override string ResolveCore(SolrContact source) {
            var result = string.Empty;
            if (!source.IsOutlet) {
                if (source.ContactSubjectNames != null) {
                    if (source.ContactSubjectNames.Count > 1)
                        result = source.ContactSubjectNames.Count + " Subjects";
                    else
                        result = source.ContactSubjectNames.First();
                }
            }
            else {
                if (source.OutletSubjectNames != null) {
                    if (source.OutletSubjectNames.Count > 1)
                        result = source.OutletSubjectNames.Count + " Subjects";
                    else
                        result = source.OutletSubjectNames.First();
                }
            }
            return result.TrimIfNotNull();
        }
    }
}