﻿
using System;
using System.Configuration;

namespace Medianet.Contacts.Wcf.Service.Common
{
    public static class Config
    {
        //private static DateTime? _lastRead;

        public static int SolrUpdateInterval() {
            RefreshAppSettings();
            var interval = ConfigurationManager.AppSettings["SolrUpdateInterval"];
            int val;
            int.TryParse(interval, out val);
            return val == 0 ? 15 : val;
        }

        public static TimeSpan SolrProcessFrom() {
            RefreshAppSettings();
            var time = ConfigurationManager.AppSettings["SolrProcessFrom"];
            int val;
            int.TryParse(time, out val);

            if (val == 0) val = 2;

            return new TimeSpan(val, 0, 0);
        }

        public static TimeSpan SolrProcessTo() {
            RefreshAppSettings();
            var time = ConfigurationManager.AppSettings["SolrProcessTo"];
            int val;
            int.TryParse(time, out val);

            if (val == 0) val = 22;

            return new TimeSpan(val, 0, 0);
        }

        public static bool SolrUpdate() {
            RefreshAppSettings();
            var interval = ConfigurationManager.AppSettings["SolrUpdate"];
            bool val;
            bool.TryParse(interval, out val);
            return val;
        }

        public static bool ShouldUseCache() { 
            RefreshAppSettings();
            var cache = ConfigurationManager.AppSettings["UseCache"];

            bool result;
            bool.TryParse(cache, out result);

            return result;
        }

        public static bool ShouldUseSolr() {
            RefreshAppSettings();
            var index = ConfigurationManager.AppSettings["UseIndex"];

            bool result;
            bool.TryParse(index, out result);

            return result;
        }

        public static string SolrUrl() {
            RefreshAppSettings();
            return ConfigurationManager.AppSettings["SolrUrl"] ?? "http://localhost:8080/solr";
        }

        public static void RefreshAppSettings() {
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
