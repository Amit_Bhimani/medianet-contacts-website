﻿using System.Web.UI.WebControls;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Medianet.Contacts.Wcf.Service.Solr.Documents;

namespace Medianet.Contacts.Wcf.Service.Common
{
    public class AutoMapperConfigurator
    {
        public static bool IsInitialized = false;

        public static void Configure() {
            if (!IsInitialized) {
                Mapper.Initialize(x => {
                    // Model -> T
                    x.AddProfile<ModelToSolrDocMappingProfile>();
                    x.AddProfile<ModelToDTOMappingProfile>();

                    // Solr -> T
                    x.AddProfile<SolrDocToDTOMappingProfile>();
                });

                IsInitialized = true;
            }
        }

        /// <summary>
        /// Mapping business model objects to our DTOs.
        /// </summary>
        private sealed class ModelToDTOMappingProfile : Profile
        {
            protected override void Configure() {
                Mapper.CreateMap<SearchResultModel, SearchResult>()
                    .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.RecordType))
                    .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId.TrimIfNotNull()))
                    .ForMember(dest => dest.OutletID, opt => opt.MapFrom(src => src.OutletId.TrimIfNotNull()))
                    .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName.TrimIfNotNull()))
                    .ForMember(dest => dest.FullName, opt => opt.ResolveUsing<CommaDelimitedStringFromHashDelimitedStringResolver>().FromMember(src => src.FullName.TrimIfNotNull()))
                    .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle.TrimIfNotNull()))
                    .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber.TrimIfNotNull()))
                    .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src.MobileNumber.TrimIfNotNull()))
                    .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.EmailAddress.TrimIfNotNull()))
                    .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter.TrimIfNotNull()))
                    .ForMember(dest => dest.LogoFilename, opt => opt.MapFrom(src => src.LogoFilename.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactOooStartDate, opt => opt.MapFrom(src => src.ContactOooStartDate))
                    .ForMember(dest => dest.ContactOooReturnDate, opt => opt.MapFrom(src => src.ContactOooReturnDate))
                    .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.RoleName.TrimIfNotNull()))
                    .ForMember(dest => dest.SubjectName, opt => opt.ResolveUsing<CommaDelimitedStringFromHashDelimitedStringResolver>().FromMember(src => src.SubjectName))
                    .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.ProductTypeName.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactDisplay, opt => opt.ResolveUsing<ContactHashListDisplayValueResolver>().FromMember(src => src.FullName))
                    .ForMember(dest => dest.SubjectDisplay, opt => opt.ResolveUsing<SubjectHashListDisplayValueResolver>().FromMember(src => src.SubjectName))
                    .ForMember(dest => dest.CityName, opt => opt.MapFrom(src => src.CityName.TrimIfNotNull()))
                    .ForMember(dest => dest.StateName, opt => opt.MapFrom(src => src.StateName.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactIds, opt=>opt.MapFrom(src => src.ContactId.IsNullOrWhitespace() ? "" : src.ContactId.Trim()))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

                Mapper.CreateMap<ServiceListSearchResultModel, SearchResult>()
                    .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.RecordType))
                    .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactID.TrimIfNotNull()))
                    .ForMember(dest => dest.OutletID, opt => opt.MapFrom(src => src.OutletId.TrimIfNotNull()))
                    .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName.TrimIfNotNull()))
                    .ForMember(dest => dest.FullName, opt => opt.ResolveUsing<CommaDelimitedStringFromHashDelimitedStringResolver>().FromMember(src => src.Fullname.TrimIfNotNull()))
                    .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle.TrimIfNotNull()))
                    .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Rolename.TrimIfNotNull()))
                    .ForMember(dest => dest.SubjectName, opt => opt.ResolveUsing<CommaDelimitedStringFromHashDelimitedStringResolver>().FromMember(src => src.Subjectname))
                    .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.ProductTypeName.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactDisplay, opt => opt.ResolveUsing<ContactHashListDisplayValueResolver>().FromMember(src => src.Fullname))
                    .ForMember(dest => dest.SubjectDisplay, opt => opt.ResolveUsing<SubjectHashListDisplayValueResolver>().FromMember(src => src.Subjectname))
                    .ForMember(dest => dest.CityName, opt => opt.MapFrom(src => src.CityName.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactIds, opt => opt.MapFrom(src => src.ContactID.IsNullOrWhitespace() ? "" : src.ContactID.Trim()))
                    .ForMember(dest => dest.ServiceId, opt => opt.MapFrom(src => src.ServiceId))
                    .ForMember(dest => dest.ServiceName, opt => opt.MapFrom(src => src.ServiceName.TrimIfNotNull()))
                    .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => src.DistributionType.TrimIfNotNull()))
                    .ForMember(dest => dest.ModifiedDate, opt => opt.MapFrom(src => src.ModifiedDate))
                    .ForMember(dest => dest.ModifiedUserId, opt => opt.MapFrom(src => src.ModifiedUserId))
                    .ForMember(dest => dest.ModifiedUser, opt => opt.MapFrom(src => src.ModifiedUser.TrimIfNotNull()))
                    .ForMember(dest => dest.Status, opt => opt.MapFrom(src => EnumHelper.ContactStatusToEnum(src.Status)));

                Mapper.CreateMap<AdminQuickSearchResultModel, SearchResult>()
                    .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.RecordType))
                    .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactID.TrimIfNotNull()))
                    .ForMember(dest => dest.OutletID, opt => opt.MapFrom(src => src.OutletId.TrimIfNotNull()))
                    .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName.TrimIfNotNull()))
                    .ForMember(dest => dest.FullName, opt => opt.ResolveUsing<CommaDelimitedStringFromHashDelimitedStringResolver>().FromMember(src => src.Fullname.TrimIfNotNull()))
                    .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle.TrimIfNotNull()))
                    .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Rolename.TrimIfNotNull()))
                    .ForMember(dest => dest.SubjectName, opt => opt.ResolveUsing<CommaDelimitedStringFromHashDelimitedStringResolver>().FromMember(src => src.Subjectname))
                    .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.ProductTypeName.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactDisplay, opt => opt.ResolveUsing<ContactHashListDisplayValueResolver>().FromMember(src => src.Fullname))
                    .ForMember(dest => dest.SubjectDisplay, opt => opt.ResolveUsing<SubjectHashListDisplayValueResolver>().FromMember(src => src.Subjectname))
                    .ForMember(dest => dest.CityName, opt => opt.MapFrom(src => src.CityName.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactIds, opt => opt.MapFrom(src => src.ContactID.IsNullOrWhitespace() ? "" : src.ContactID.Trim()))
                    .ForMember(dest => dest.ServiceId, opt => opt.MapFrom(src => src.ServiceId))
                    .ForMember(dest => dest.ServiceName, opt => opt.MapFrom(src => src.ServiceName.TrimIfNotNull()))
                    .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => src.DistributionType.TrimIfNotNull()))
                    .ForMember(dest => dest.ModifiedDate, opt => opt.MapFrom(src => src.ModifiedDate))
                    .ForMember(dest => dest.ModifiedUserId, opt => opt.MapFrom(src => src.ModifiedUserId))
                    .ForMember(dest => dest.ModifiedUser, opt => opt.MapFrom(src => src.ModifiedUser.TrimIfNotNull()))
                    .ForMember(dest => dest.Status, opt => opt.MapFrom(src => EnumHelper.ContactStatusToEnum(src.Status)));

                Mapper.CreateMap<SavedSearchModel, SavedSearch>()
                   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.SearchId))
                   .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.SearchName))
                   .ForMember(dest => dest.Query, opt => opt.MapFrom(src => src.SearchQuery))
                   .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                   .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                   .ForMember(dest => dest.ModifiedDate, opt => opt.MapFrom(src => src.ModifiedDate))
                   .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                   .ForMember(dest => dest.Visibility, opt => opt.MapFrom(src => src.Visibility))
                   .ForMember(dest => dest.Context, opt => opt.MapFrom(src => src.ContextText))
                   .ForMember(dest => dest.Criteria, opt => opt.MapFrom(src => src.SearchCriteria))
                   .ForMember(dest => dest.Type, opt => opt.Ignore());

                Mapper.CreateMap<AutoSuggestModel, AutoSuggest>()
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.RecordName.TrimIfNotNull()))
                    .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src => src.RecordType))
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => (src.RecordType == (int)RecordType.MediaOutlet || src.RecordType == (int)RecordType.PrnOutlet) ? src.OutletId : src.ContactId));
            }
        }

        /// <summary>
        /// Mapping Solr documents to DTOs.
        /// </summary>
        private sealed class SolrDocToDTOMappingProfile : Profile
        {
            protected override void Configure() {
                Mapper.CreateMap<SolrContact, SearchResult>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.ContactIds, opt => opt.Ignore())
                    .ForMember(dest => dest.ContactId, opt => opt.ResolveUsing<SolrContactIdValueResolver>().FromMember(src => src))
                    .ForMember(dest => dest.ContactType, opt => opt.MapFrom(src => (RecordType)src.RecordType))
                    .ForMember(dest => dest.Type, opt => opt.MapFrom(src => (RecordType)src.RecordType))
                    .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
                    .ForMember(dest => dest.PhoneNumber , opt => opt.MapFrom(src => src.IsOutlet ? src.OutletPhoneNumber : src.ContactPhoneNumber))
                    .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src.ContactMobileNumber))
                    .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.IsOutlet ? src.OutletEmailAddress : src.ContactEmailAddress))
                    .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.ContactTwitter))
                    .ForMember(dest => dest.LogoFilename, opt => opt.MapFrom(src => src.OutletLogoFilename))
                    .ForMember(dest => dest.ContactOooStartDate, opt => opt.MapFrom(src => src.ContactOooStartDate))
                    .ForMember(dest => dest.ContactOooReturnDate, opt => opt.MapFrom(src => src.ContactOooReturnDate))
                    .ForMember(dest => dest.OutletID, opt => opt.MapFrom(src => src.OutletId))
                    .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName))
                    .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.MediaType))
                    .ForMember(dest => dest.CityName, opt => opt.MapFrom(src => src.IsOutlet ? src.OutletCityName.Trim() : src.ContactCityName.Trim()))
                    .ForMember(dest => dest.StateName, opt => opt.MapFrom(src => src.IsOutlet ? src.OutletStateName : src.ContactStateName))
                    .ForMember(dest => dest.ContactDisplay, opt => opt.ResolveUsing<SolrContactDisplayValueResolver>().FromMember(src => src))
                    .ForMember(dest => dest.SubjectDisplay, opt => opt.ResolveUsing<SolrContactSubjectDisplayValueResolver>().FromMember(src => src))
                    .ForMember(dest => dest.SubjectName, opt => opt.ResolveUsing<SolrContactSubjectNameValueResolver>().FromMember(src => src))
                    .ForMember(dest => dest.FullName, opt => opt.ResolveUsing<SolrContactFullNameValueResolver>().FromMember(src => src))
                    .ForMember(dest => dest.IsGeneric, opt => opt.MapFrom(src => src.OutletIsGeneric))
                    .ForMember(dest => dest.ContactPriorityNotice, opt => opt.MapFrom(src => src.ContactPriorityNotice))
                    .ForMember(dest => dest.OutletPriorityNotice, opt => opt.MapFrom(src => src.OutletPriorityNotice));

                Mapper.CreateMap<SolrContact, AutoSuggest>()
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.IsOutlet ? src.OutletName : src.FullName))
                    .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src => src.RecordType))
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.IsOutlet ? src.OutletId : src.ContactId));
            }
        }

        /// <summary>
        /// Mapping business model objects to Solr documents.
        /// </summary>
        private sealed class ModelToSolrDocMappingProfile : Profile
        {
            protected override void Configure() {
                Mapper.CreateMap<ContactModel, SolrContact>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.IsDangling, opt => opt.MapFrom(src => src.IsDangling))
                    .ForMember(dest => dest.IsDirty, opt => opt.MapFrom(src => src.IsDirty))
                    .ForMember(dest => dest.IsAap, opt => opt.MapFrom(src => src.IsAap))
                    .ForMember(dest => dest.IsOutlet, opt => opt.MapFrom(src => src.IsOutlet))
                    .ForMember(dest => dest.IsPrivate, opt => opt.MapFrom(src => src.IsPrivate))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                    .ForMember(dest => dest.LastModified, opt => opt.MapFrom(src => src.LastModified))

                    // Contact
                    .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId.TrimIfNotNull()))
                    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName.TrimIfNotNull()))
                    .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName.TrimIfNotNull()))
                    .ForMember(dest => dest.FullName, opt => opt.ResolveUsing<ContactModelFullNameResolver>().FromMember(src => src))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                    .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
                    .ForMember(dest => dest.PositionIds, opt => opt.ResolveUsing<IntListFromDelimitedStringValueResolver>().FromMember(src => src.PositionIds))
                    .ForMember(dest => dest.IsPrimaryContact, opt => opt.MapFrom(src => src.IsPrimaryContact))
                    .ForMember(dest => dest.ContactPreferredDeliveryMethod, opt => opt.MapFrom(src => src.ContactPreferredDeliveryMethod))
                    .ForMember(dest => dest.ContactSubjectIds, opt => opt.ResolveUsing<IntListFromDelimitedStringValueResolver>().FromMember(src => src.ContactSubjectIds))
                    .ForMember(dest => dest.ContactSubjectGroupIds, opt => opt.ResolveUsing<IntListFromDelimitedStringValueResolver>().FromMember(src => src.ContactSubjectGroupIds))
                    .ForMember(dest => dest.ContactSubjectNames, opt => opt.ResolveUsing<StringListFromHashDelimitedStringResolver>().FromMember(src => src.ContactSubjectNames))
                    .ForMember(dest => dest.ContactSystemNotes, opt => opt.MapFrom(src => src.ContactSystemNotes))
                    .ForMember(dest => dest.ContactTwitter, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.ContactTwitter))
                    .ForMember(dest => dest.ContactLinkedIn, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.ContactLinkedIn))
                    .ForMember(dest => dest.ContactFacebook, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.ContactFacebook))
                    .ForMember(dest => dest.ContactInstagram, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.ContactInstagram))
                    .ForMember(dest => dest.ContactYouTube, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.ContactYouTube))
                    .ForMember(dest => dest.ContactSnapchat, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.ContactSnapchat))
                    .ForMember(dest => dest.ContactSkype, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.ContactSkype))
                    .ForMember(dest => dest.ContactBio, opt => opt.MapFrom(src => src.ContactBio))
                    .ForMember(dest => dest.ContactHasEmail, opt => opt.MapFrom(src => src.ContactHasEmail))
                    .ForMember(dest => dest.ContactHasFax, opt => opt.MapFrom(src => src.ContactHasFax))
                    .ForMember(dest => dest.ContactHasPostal, opt => opt.MapFrom(src => src.ContactHasPostal))
                    .ForMember(dest => dest.ContactCityName, opt => opt.MapFrom(src => src.ContactCityName))
                    .ForMember(dest => dest.ContactContinentId, opt => opt.MapFrom(src => src.ContactContinentId))
                    .ForMember(dest => dest.ContactCountryId, opt => opt.MapFrom(src => src.ContactCountryId))
                    .ForMember(dest => dest.ContactStateId, opt => opt.MapFrom(src => src.ContactStateId))
                    .ForMember(dest => dest.ContactStateName, opt => opt.MapFrom(src => src.ContactStateName))
                    .ForMember(dest => dest.ContactPostcode, opt => opt.MapFrom(src => src.ContactPostcode))
                    .ForMember(dest => dest.ContactBugBears, opt => opt.MapFrom(src => src.ContactBugBears))
                    .ForMember(dest => dest.ContactAlsoKnownAs, opt => opt.MapFrom(src => src.ContactAlsoKnownAs))
                    .ForMember(dest => dest.ContactPressReleaseInterests, opt => opt.MapFrom(src => src.ContactPressReleaseInterests))
                    .ForMember(dest => dest.ContactPersonalInterests, opt => opt.MapFrom(src => src.ContactPersonalInterests))
                    .ForMember(dest => dest.ContactAppearsIn, opt => opt.MapFrom(src => src.ContactAppearsIn))
                    .ForMember(dest => dest.ContactMediaInfluencerScore, opt => opt.MapFrom(src => src.ContactMediaInfluencerScore))
                    .ForMember(dest => dest.ContactCurrentStatus, opt => opt.MapFrom(src => src.ContactCurrentStatus))
                    .ForMember(dest => dest.ContactBasedInLocation, opt => opt.MapFrom(src => src.ContactBasedInLocation))
                    .ForMember(dest => dest.ContactAlternativeEmailAddress, opt => opt.MapFrom(src => src.ContactAlternativeEmailAddress))
                    .ForMember(dest => dest.ContactWebsite, opt => opt.MapFrom(src => src.ContactWebsite))
                    //.ForMember(dest => dest.IsContactOutOfOffice, opt => opt.MapFrom(src => src.IsContactOutOfOffice))
                    .ForMember(dest => dest.ContactOooStartDate, opt => opt.MapFrom(src => src.ContactOooStartDate))
                    .ForMember(dest => dest.ContactOooReturnDate, opt => opt.ResolveUsing<ReturnDateValueResolver>().FromMember(src => src))
                    .ForMember(dest => dest.ContactPhoneNumber, opt => opt.MapFrom(src => src.ContactPhoneNumber))
                    .ForMember(dest => dest.ContactMobileNumber, opt => opt.MapFrom(src => src.ContactMobileNumber))
                    .ForMember(dest => dest.ContactEmailAddress, opt => opt.MapFrom(src => src.ContactEmailAddress))
                    .ForMember(dest => dest.ContactPriorityNotice, opt => opt.MapFrom(src => src.ContactPriorityNotice))

                    // Outlet
                    .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId.TrimIfNotNull()))
                    .ForMember(dest => dest.ContactIds, opt => opt.ResolveUsing<StringListFromHashDelimitedStringResolver>().FromMember(src => src.ContactIds))
                    .ForMember(dest => dest.ContactNames, opt => opt.ResolveUsing<StringListFromHashDelimitedStringResolver>().FromMember(src => src.ContactNames))
                    .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName.TrimIfNotNull()))
                    .ForMember(dest => dest.OutletPreferredDeliveryMethod, opt => opt.MapFrom(src => src.OutletPreferredDeliveryMethod))
                    .ForMember(dest => dest.OutletSubjectIds, opt => opt.ResolveUsing<IntListFromDelimitedStringValueResolver>().FromMember(src => src.OutletSubjectIds))
                    .ForMember(dest => dest.OutletSubjectGroupIds, opt => opt.ResolveUsing<IntListFromDelimitedStringValueResolver>().FromMember(src => src.OutletSubjectGroupIds))
                    .ForMember(dest => dest.OutletSubjectNames, opt => opt.ResolveUsing<StringListFromHashDelimitedStringResolver>().FromMember(src => src.OutletSubjectNames))
                    .ForMember(dest => dest.OutletSystemNotes, opt => opt.MapFrom(src => src.OutletSystemNotes))
                    .ForMember(dest => dest.OutletTwitter, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.OutletTwitter))
                    .ForMember(dest => dest.OutletLinkedIn, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.OutletLinkedIn))
                    .ForMember(dest => dest.OutletFacebook, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.OutletFacebook))
                    .ForMember(dest => dest.OutletInstagram, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.OutletInstagram))
                    .ForMember(dest => dest.OutletYouTube, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.OutletYouTube))
                    .ForMember(dest => dest.OutletSnapchat, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.OutletSnapchat))
                    .ForMember(dest => dest.OutletSkype, opt => opt.ResolveUsing<NaStripperValueResolver>().FromMember(src => src.OutletSkype))
                    .ForMember(dest => dest.OutletBio, opt => opt.MapFrom(src => src.OutletBio))
                    .ForMember(dest => dest.OutletHasEmail, opt => opt.MapFrom(src => src.OutletHasEmail))
                    .ForMember(dest => dest.OutletHasFax, opt => opt.MapFrom(src => src.OutletHasFax))
                    .ForMember(dest => dest.OutletHasPostal, opt => opt.MapFrom(src => src.OutletHasPostal))
                    .ForMember(dest => dest.OutletIsOffice, opt => opt.MapFrom(src => src.OutletIsOffice))
                    .ForMember(dest => dest.OutletIsFinancial, opt => opt.MapFrom(src => src.OutletIsFinancial))
                    .ForMember(dest => dest.OutletIsPolitician, opt => opt.MapFrom(src => src.OutletIsPolitician))
                    .ForMember(dest => dest.OutletCityName, opt => opt.MapFrom(src => src.OutletCityName))
                    .ForMember(dest => dest.OutletContinentId, opt => opt.MapFrom(src => src.OutletContinentId))
                    .ForMember(dest => dest.OutletCountryId, opt => opt.MapFrom(src => src.OutletCountryId))
                    .ForMember(dest => dest.OutletStateId, opt => opt.MapFrom(src => src.OutletStateId))
                    .ForMember(dest => dest.OutletCityId, opt => opt.MapFrom(src => src.OutletCityId))
                    .ForMember(dest => dest.OutletPostcode, opt => opt.MapFrom(src => src.OutletPostcode))
                    .ForMember(dest => dest.OutletPostalCityId, opt => opt.MapFrom(src => src.OutletPostalCityId))
                    .ForMember(dest => dest.OutletPostalCountryId, opt => opt.MapFrom(src => src.OutletPostalContinentId))
                    .ForMember(dest => dest.OutletPostalContinentId, opt => opt.MapFrom(src => src.OutletPostalContinentId))
                    .ForMember(dest => dest.OutletSubscriptionOnlyPublication, opt => opt.MapFrom(src => src.OutletSubscriptionOnlyPublication))
                    .ForMember(dest => dest.OutletAlsoKnownAs, opt => opt.MapFrom(src => src.OutletAlsoKnownAs))
                    .ForMember(dest => dest.OutletAdditionalWebsite, opt => opt.MapFrom(src => src.OutletAdditionalWebsite))
                    .ForMember(dest => dest.OutletPublishedOn, opt => opt.MapFrom(src => src.OutletPublishedOn))
                    .ForMember(dest => dest.OutletPressReleaseInterests, opt => opt.MapFrom(src => src.OutletPressReleaseInterests))
                    .ForMember(dest => dest.OutletRegionsCovered, opt => opt.MapFrom(src => src.OutletRegionsCovered))
                    .ForMember(dest => dest.OutletPhoneNumber, opt => opt.MapFrom(src => src.OutletPhoneNumber))
                    .ForMember(dest => dest.OutletEmailAddress, opt => opt.MapFrom(src => src.OutletEmailAddress))
                    .ForMember(dest => dest.OutletRegionsCovered, opt => opt.MapFrom(src => src.OutletRegionsCovered))
                    .ForMember(dest => dest.OutletWebsite, opt => opt.MapFrom(src => src.OutletWebsite))
                    .ForMember(dest => dest.OutletLogoFilename, opt => opt.MapFrom(src => src.OutletLogoFilename))
                    .ForMember(dest => dest.OutletPriorityNotice, opt => opt.MapFrom(src => src.OutletPriorityNotice))

                    // Common
                    .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src => (RecordType)src.RecordType))
                    .ForMember(dest => dest.MediaTypeId, opt => opt.MapFrom(src => src.MediaTypeId))
                    .ForMember(dest => dest.MediaType, opt => opt.MapFrom(src => src.MediaType))
                    .ForMember(dest => dest.FrequencyId, opt => opt.MapFrom(src => src.FrequencyId))
                    .ForMember(dest => dest.Circulation, opt => opt.MapFrom(src => src.Circulation))
                    .ForMember(dest => dest.LanguageIds, opt => opt.ResolveUsing<IntListFromDelimitedStringValueResolver>().FromMember(src => src.LanguageIds))
                    .ForMember(dest => dest.LanguageNames, opt => opt.ResolveUsing<StringListFromHashDelimitedStringResolver>().FromMember(src => src.LanguageNames))
                    .ForMember(dest => dest.StationFrequency, opt => opt.MapFrom(src => src.StationFrequency))
                    .ForMember(dest => dest.NewsFocusIds, opt => opt.ResolveUsing<IntListFromDelimitedStringValueResolver>().FromMember(src => src.NewsFocusIds))
                    .ForMember(dest => dest.OwnerDebtorNumber, opt => opt.MapFrom(src => src.OwnerDebtorNumber))
                    .ForMember(dest => dest.OutletOrFullName, opt => opt.ResolveUsing<AutoSuggestFieldValueResolver>().FromMember(src => src))
                    .ForMember(dest => dest.TextFullSearch, opt => opt.ResolveUsing<ContactModelTextFullSearchResolver>().FromMember(src => src))

                    // Sorting
                    .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => !string.IsNullOrWhiteSpace(src.ContactSubjectNames) ? src.ContactSubjectNames : src.OutletSubjectNames))
                    .ForMember(dest => dest.Location, opt => opt.MapFrom(src => !string.IsNullOrWhiteSpace(src.ContactCityName) ? src.ContactCityName : src.OutletCityName));
            }
        }
    }
}