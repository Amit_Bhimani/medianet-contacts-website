﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace Medianet.Contacts.Wcf.Service.Common
{
    public class EmailHelper
    {

        public static void SendEmail(string toAddress, string subject, string content, bool isHtml = false)
        {
            using (var client = new SmtpClient(ConfigurationManager.AppSettings["MailServer"]))
            {
                var message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["MailFromAddress"], ConfigurationManager.AppSettings["MailFromName"]);
                string[] addresses = toAddress.Split(new Char[] { ';' });
                foreach (var a in addresses)
                {
                    message.To.Add(new MailAddress(a));
                }
                message.Subject = subject;
                message.IsBodyHtml = isHtml;
                message.Body = content;
                client.Send(message);
            }

        }
    }
}
