﻿using System.Configuration;

namespace Medianet.Contacts.Wcf.Service.Common
{
    public static class Constants
    {
        public static readonly string GenericServiceErrorMessage =
            "An error has occured. If this continues, please contact Client    Services on 1300 616 813 or at " +
            ConfigurationManager.AppSettings["MailDefaultAddress"] + ".";
        
        // Should be a URL or the course of action that the client should take.
        public const string GenericServiceErrorFaultAction = "information unavailable.";

        public const string AustraliaCode = "210";
        public const string NewZealandCode = "211";

        public static int? MaxRows = 5000;
    }
}
