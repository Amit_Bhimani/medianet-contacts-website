﻿using Medianet.Contacts.Wcf.Service.Ninject.Modules;
using Ninject;
using Ninject.Modules;

namespace Medianet.Contacts.Wcf.Service.Ninject
{
    public static class ServiceLocator
    {
        private static IKernel _kernel;
        private static IKernel Kernel {
            get {
                if (_kernel == null)
                    _kernel = CreateKernel();
                return _kernel;
            }
            set {
                _kernel = value;
            }
        }

        /// <summary>
        /// Gets an instance of the type provided.
        /// </summary>
        /// <typeparam name="T">The type to retrieve an instance for.</typeparam>
        /// <returns>A concrete implementation for the type provided.</returns>
        public static T Resolve<T>() {
            return Kernel.Get<T>();
        }

        /// <summary>
        /// Creates the kernel.
        /// </summary>
        /// <returns>A standard kernel with the appropriate modules loaded.</returns>
        private static IKernel CreateKernel() {
            return new StandardKernel(new INinjectModule[] {
                // Load modules.
                new SolrModule(),
                new SolrNetModule(),
                new SearchModule()
            });
        }
    }
}
