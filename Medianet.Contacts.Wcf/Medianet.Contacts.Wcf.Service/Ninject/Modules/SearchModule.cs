﻿using Medianet.Contacts.Wcf.Service.Search;
using Ninject.Modules;

namespace Medianet.Contacts.Wcf.Service.Ninject.Modules
{
    public class SearchModule : NinjectModule
    {
        public override void Load() {
            Bind<SolrSearch>().ToSelf();
            Bind<SqlSearch>().ToSelf();
        }
    }
}
