﻿using Medianet.Contacts.Wcf.Service.Solr;
using Ninject.Modules;
using SolrNet;
using SolrNet.Impl;
using SolrNet.Impl.DocumentPropertyVisitors;
using SolrNet.Impl.FacetQuerySerializers;
using SolrNet.Impl.FieldParsers;
using SolrNet.Impl.FieldSerializers;
using SolrNet.Impl.QuerySerializers;
using SolrNet.Impl.ResponseParsers;
using SolrNet.Mapping.Validation;
using SolrNet.Mapping.Validation.Rules;
using SolrNet.Schema;

namespace Medianet.Contacts.Wcf.Service.Ninject.Modules
{
    public class SolrNetModule : NinjectModule
    {
        public override void Load() {
            Bind<IReadOnlyMappingManager>().ToConstant(SolrConfigurator.GetManager());
            Bind<ISolrConnection>().ToMethod(x => SolrConfigurator.GetSolrConnection());

            Bind<ISolrDocumentPropertyVisitor>().To<DefaultDocumentVisitor>();
            Bind<ISolrFieldParser>().To<DefaultFieldParser>();
            Bind(typeof(ISolrDocumentActivator<>)).To(typeof(SolrDocumentActivator<>));
            Bind(typeof(ISolrDocumentResponseParser<>)).To(typeof(SolrDocumentResponseParser<>));
            Bind<ISolrFieldSerializer>().To<DefaultFieldSerializer>();
            Bind<ISolrQuerySerializer>().To<DefaultQuerySerializer>();
            Bind<ISolrFacetQuerySerializer>().To<DefaultFacetQuerySerializer>();
            foreach (var p in new[] {
                typeof(ResultsResponseParser<>),
                typeof(HeaderResponseParser<>),
                typeof(FacetsResponseParser<>),
                typeof(HighlightingResponseParser<>),
                typeof(MoreLikeThisResponseParser<>),
                typeof(SpellCheckResponseParser<>),
                typeof(StatsResponseParser<>),
                typeof(CollapseResponseParser<>),
            })
                Bind(typeof(ISolrResponseParser<>)).To(p);
            Bind<ISolrHeaderResponseParser>().To<HeaderResponseParser<string>>();
            foreach (var p in new[] {
                typeof(MappedPropertiesIsInSolrSchemaRule),
                typeof(RequiredFieldsAreMappedRule),
                typeof(UniqueKeyMatchesMappingRule),
            })
                Bind<IValidationRule>().To(p);

            Bind(typeof(ISolrQueryResultParser<>)).To(typeof(SolrQueryResultParser<>));
            Bind(typeof(ISolrQueryExecuter<>)).To(typeof(SolrQueryExecuter<>));
            Bind(typeof(ISolrDocumentSerializer<>)).To(typeof(SolrDocumentSerializer<>));
            Bind(typeof(ISolrBasicOperations<>)).To(typeof(SolrBasicServer<>));
            Bind(typeof(ISolrBasicReadOnlyOperations<>)).To(typeof(SolrBasicServer<>));
            Bind(typeof(ISolrOperations<>)).To(typeof(SolrServer<>));
            Bind(typeof(ISolrReadOnlyOperations<>)).To(typeof(SolrServer<>));
            Bind<ISolrSchemaParser>().To<SolrSchemaParser>();
            Bind<IMappingValidator>().To<MappingValidator>();
        }
    }
}
