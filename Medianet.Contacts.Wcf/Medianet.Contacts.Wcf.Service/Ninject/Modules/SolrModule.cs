﻿using Medianet.Contacts.Wcf.Service.Solr;
using Ninject.Modules;

namespace Medianet.Contacts.Wcf.Service.Ninject.Modules
{
    public class SolrModule : NinjectModule
    {
        public override void Load() {
            Bind<Searcher>().ToSelf();
            Bind<Indexer>().ToSelf();
        }
    }
}
