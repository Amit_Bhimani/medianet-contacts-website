﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Medianet.Contacts.Wcf.Service.WCF
{
    // This attribute can be used to install a custom error handler for a service
    public sealed class ErrorBehaviorAttribute : Attribute, IServiceBehavior
    {
        private readonly Type _errorHandlerType;

        public ErrorBehaviorAttribute(Type errorHandlerType) {
            _errorHandlerType = errorHandlerType;
        }

        public Type ErrorHandlerType {
            get { return _errorHandlerType; }
        }

        void IServiceBehavior.Validate(ServiceDescription description, ServiceHostBase serviceHostBase) {}

        void IServiceBehavior.AddBindingParameters(ServiceDescription description, ServiceHostBase serviceHostBase,
                                                   System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints,
                                                   BindingParameterCollection parameters) {}

        void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription description, ServiceHostBase serviceHostBase) {
            IErrorHandler errorHandler;

            try {
                errorHandler = (IErrorHandler) Activator.CreateInstance(_errorHandlerType);
            }
            catch (MissingMethodException e) {
                throw new ArgumentException(
                    "The errorHandlerType specified in the ErrorBehaviorAttribute constructor must have a public empty constructor.",
                    e);
            }
            catch (InvalidCastException e) {
                throw new ArgumentException(
                    "The errorHandlerType specified in the ErrorBehaviorAttribute constructor must implement System.ServiceModel.Dispatcher.IErrorHandler.",
                    e);
            }

            foreach (
                var channelDispatcher in
                    serviceHostBase.ChannelDispatchers.Select(
                        channelDispatcherBase => channelDispatcherBase as ChannelDispatcher)) {
                channelDispatcher.ErrorHandlers.Add(errorHandler);
            }
        }
    }
}
