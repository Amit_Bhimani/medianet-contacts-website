﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Service.Common;
using NLog;

namespace Medianet.Contacts.Wcf.Service.WCF
{
    public class ServiceErrorHandler : IErrorHandler
    {        
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault) {
            if (error is FaultException) return;
            var faultDetail = new ServiceFault
                              {Id = Guid.NewGuid(), MessageText = Constants.GenericServiceErrorMessage};
            var fex = new FaultException<ServiceFault>(faultDetail, "Unhandled Application Fault",
                                                       new FaultCode("ApplicationFault"));

            var msgFault = fex.CreateMessageFault();
            fault = Message.CreateMessage(version, msgFault, Constants.GenericServiceErrorFaultAction);
        }

        public bool HandleError(Exception error) {
            Logger.LogException(LogLevel.Error, "Exception handled in " + typeof (ServiceErrorHandler).Name, error);
            return true;
        }
    }
}
