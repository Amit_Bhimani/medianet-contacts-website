﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Search;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Integration
{
    [TestClass]
    public class SearchTests : SolrTestBase
    {
        private static List<SubjectModel> _subjects;
        private static List<RoleModel> _positions;
        private static List<ProductModel> _products;
        private static List<SubjectGroupModel> _subjectGroups;

        private static List<SearchResult> _difference;
        private static List<SearchResult> _solr;
        private static List<SearchResult> _sql;

        [ClassInitialize]
        public static void SolrNetTestInitialise(TestContext testContext) {
            AutoMapperConfigurator.Configure();

            using (var context = new MedianetContactsContext()) {
                _subjects = context.Subjects.As<IEnumerable<SubjectModel>>().ToList();
                _positions = context.Roles.As<IEnumerable<RoleModel>>().ToList();
                _products = context.Products.As<IEnumerable<ProductModel>>().ToList();
                _subjectGroups = context.SubjectGroups.As<IEnumerable<SubjectGroupModel>>().ToList();
            }
        }

        [TestInitialize]
        public void InitialiseDifference() {
            if (_difference == null) _difference = new List<SearchResult>();
            _difference.Clear();
        }

        [TestCleanup]
        public void PrintDifference() {
            if (_solr.Count > _sql.Count) {
                _difference = _solr.Except(_sql).ToList();
                Console.WriteLine("Solr found {0} more records.", _difference.Count);
            }
            else if (_solr.Count < _sql.Count) {
                _difference = _sql.Except(_solr).ToList();
                Console.WriteLine("SQL found {0} more records.", _difference.Count);
            }

            _difference.ForEach(x => Console.WriteLine(x.ToClientString()));
        }

        [TestMethod]
        public void Subject_People_Australia_Quick_Search_Test() {
            foreach (var subject in _subjects) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.People,
                    SubjectList = new List<int> { subject.Id }
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }

        [TestMethod]
        public void Subject_Outlet_Australia_Quick_Search_Test() {
            foreach (var subject in _subjects) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.Outlet,
                    SubjectList = new List<int> {subject.Id}
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }

        [TestMethod]
        public void Subject_Group_People_Quick_Search_Test() {
            foreach (var subjectGroup in _subjectGroups) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.People,
                    SubjectGroupList = new List<int> { subjectGroup.Id }
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }

        [TestMethod]
        public void Subject_Group_Outlet_Quick_Search_Test() {
            foreach (var subjectGroup in _subjectGroups) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.Outlet,
                    SubjectGroupList = new List<int> { subjectGroup.Id }
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }

        [TestMethod]
        public void Product_People_Quick_Search_Test() {
            foreach (var product in _products) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.People,
                    MediaTypeList = new List<int> { product.Id }
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }

        [TestMethod]
        public void Product_Outlet_Quick_Search_Test() {
            foreach (var product in _products) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.Outlet,
                    MediaTypeList = new List<int> { product.Id }
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }

        [TestMethod]
        public void Position_People_Quick_Search_Test() {
            foreach (var position in _positions) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.People,
                    PositionList = new List<int> { position.Id }
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }

        [TestMethod]
        public void Position_Outlet_Quick_Search_Test() {
            foreach (var position in _positions) {
                var indexSearcher = new SolrSearch();
                var sqlSearcher = new SqlSearch();

                var option = new SearchOptionsQuick {
                    Context = SearchContext.Outlet,
                    PositionList = new List<int> { position.Id }
                };

                int indexTotal;
                int sqlTotal;

                _solr = indexSearcher.Search(out indexTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);
                _sql = sqlSearcher.Search(out sqlTotal, option, SortColumn.Default, SortDirection.ASC, true, UserId);

                indexTotal.Should().BeGreaterOrEqualTo(sqlTotal);
            }
        }
    }
}
