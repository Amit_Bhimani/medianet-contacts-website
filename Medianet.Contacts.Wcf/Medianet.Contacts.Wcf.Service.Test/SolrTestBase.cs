﻿using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolrNet;

namespace Medianet.Contacts.Wcf.Service.Test
{
    [TestClass]
    public class SolrTestBase
    {
        public const int UserId = 28923;

        private static ISolrOperations<SolrContact> _solr;
        public static ISolrOperations<SolrContact> Solr {
            get {
                if (_solr == null)
                    _solr = ServiceLocator.Resolve<ISolrOperations<SolrContact>>();
                return _solr;
            }
        }
    }
}
