﻿using System.Linq;
using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Ninject;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolrNet;
using SolrNet.Mapping.Validation;

namespace Medianet.Contacts.Wcf.Service.Test.Unit
{
    /// <summary>
    /// Unit test SolrNet things.
    /// </summary>
    [TestClass]
    public class SolrNetTest : SolrTestBase
    {
        [ClassInitialize]
        public static void SolrNetTestInitialise(TestContext testContext) {
            AutoMapperConfigurator.Configure();
        }

        [TestMethod]
        public void Should_Have_No_Errors() {
            var misses = Solr.EnumerateValidationResults().ToList();
            var errors = misses.OfType<ValidationError>();

            errors.Count().Should().Be(0);
        }

        [TestMethod]
        public void Should_Have_No_Warnings() {
            var misses = Solr.EnumerateValidationResults().ToList();
            var warnings = misses.OfType<ValidationWarning>();

            warnings.Count().Should().Be(0);
        }

        [TestMethod]
        public void Should_Be_Able_To_Ping_Solr_Server() {
            var solr = ServiceLocator.Resolve<ISolrOperations<SolrContact>>();
            solr.Ping();
        }
    }
}