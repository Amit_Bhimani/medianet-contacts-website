﻿using FluentAssertions;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SearchResultModelSubjectDisplayTest : SubjectDisplayValueResolver
    {
        public string TestResolveCore(SearchResultModel input) {
            return ResolveCore(input.SubjectName);
        }

        [TestMethod]
        public void Should_Resolve_Corrent_Contact_Display() {
            var model = new SearchResultModel {
                SubjectName = "Business"
            };

            ResolveCore(model.SubjectName).Should().Be("Business");
        }

        [TestMethod]
        public void Should_Resolve_Correct_Outlet_Display() {
            var model = new SearchResultModel {
                SubjectName = "Business#Entertainment (General)#Gossip#National news"
            };

            ResolveCore(model.SubjectName).Should().Be("4 Subjects");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_Or_Empty_Fullname() {
            var model = new SearchResultModel {
                SubjectName = null
            };

            var model2 = new SearchResultModel {
                SubjectName = string.Empty
            };

            ResolveCore(model.SubjectName).Should().Be(string.Empty);
            ResolveCore(model2.SubjectName).Should().Be(string.Empty);
        }
    }
}
