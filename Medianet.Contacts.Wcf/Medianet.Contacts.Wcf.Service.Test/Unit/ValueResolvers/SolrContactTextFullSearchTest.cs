﻿using System.Collections.Generic;
using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Contacts.Wcf.Model;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SolrContactTextFullSearchTest : ContactModelTextFullSearchResolver
    {
        public string TestResolveCore(ContactModel input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Contact_Details_And_Outlet_Name_Only() {
            var contact = new ContactModel
            {
                RecordType = (byte)Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.MediaContact,
                IsOutlet = false,
                OutletName = "Sydney Morning Herald",
                FirstName = "Jose",
                MiddleName = null,
                LastName = "Carlos",
                OutletBio = "Bio",
                OutletSystemNotes = "System Notes"
            };

            ResolveCore(contact).Should().Be("Sydney Morning Herald Jose Carlos");
        }

        [TestMethod]
        public void Should_Resolve_Outlet_Details_Only() {
            var contact = new ContactModel
            {
                RecordType = (byte)Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.MediaOutlet,
                IsOutlet = true,
                OutletName = "Sydney Morning Herald",
                FirstName = "Jose",
                MiddleName = null,
                LastName = "Carlos",
                OutletBio = "Bio",
                OutletSystemNotes = "System Notes"
            };

            ResolveCore(contact).Should().Be("Sydney Morning Herald System Notes Bio");
        }
    }
}
