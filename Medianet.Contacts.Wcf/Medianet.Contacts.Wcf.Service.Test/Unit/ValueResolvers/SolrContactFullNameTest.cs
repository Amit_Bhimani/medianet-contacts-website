﻿using System.Collections.Generic;
using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SolrContactFullNameTest : SolrContactFullNameValueResolver
    {
        public string TestResolveCore(SolrContact input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Corrent_Contact_FullName() {
            var contact = new SolrContact {
                FirstName = "Jose",
                MiddleName = null,
                LastName = "Carlos"
            };

            ResolveCore(contact).Should().Be("Jose Carlos");
        }

        [TestMethod]
        public void Should_Resolve_Correct_Outlet_FullName() {
            var contact = new SolrContact {
                ContactNames = new List<string> {
                    "Cathy Morris",
                    "Chris Pavlich",
                    "Craig Herbert",
                    "Matt Sun",
                    "Tim Elbra",
                    "Vanessa Stubbs"
                },
                IsOutlet = true
            };

            ResolveCore(contact).Should().Be("Cathy Morris, Chris Pavlich, Craig Herbert, Matt Sun, Tim Elbra, Vanessa Stubbs");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_Or_Empty_ContactNames() {
            var contact = new SolrContact {
                IsOutlet = true,
                ContactNames = null
            };

            ResolveCore(contact).Should().Be(string.Empty);
        }
    }
}
