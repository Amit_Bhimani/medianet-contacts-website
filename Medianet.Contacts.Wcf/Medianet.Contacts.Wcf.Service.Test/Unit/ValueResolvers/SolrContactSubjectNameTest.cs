﻿using System.Collections.Generic;
using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SolrContactSubjectNameTest : SolrContactSubjectNameValueResolver
    {
        public string TestResolveCore(SolrContact input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Corrent_Contact_Subject_Name() {
            var contact = new SolrContact {
                ContactSubjectNames = new List<string> {
                    "Business",
                    "Entertainment (General)",
                    "Gossip",
                    "National news"
                },
                IsOutlet = false
            };
            ResolveCore(contact).Should().Be("Business, Entertainment (General), Gossip, National news");
        }

        [TestMethod]
        public void Should_Resolve_Corrent_Outlet_Subject_Name() {
            var contact = new SolrContact {
                OutletSubjectNames = new List<string> {
                    "Business",
                    "Entertainment (General)",
                    "Gossip",
                    "National news"
                },
                IsOutlet = true
            };
            ResolveCore(contact).Should().Be("Business, Entertainment (General), Gossip, National news");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_Or_Empty_Subject_Names() {
            var contact1 = new SolrContact {
                ContactSubjectNames = null,
                IsOutlet = true
            };

            var contact2 = new SolrContact {
                OutletSubjectNames = null,
                IsOutlet = false
            };
            ResolveCore(contact1).Should().Be(string.Empty);
            ResolveCore(contact2).Should().Be(string.Empty);
        }
    }
}
