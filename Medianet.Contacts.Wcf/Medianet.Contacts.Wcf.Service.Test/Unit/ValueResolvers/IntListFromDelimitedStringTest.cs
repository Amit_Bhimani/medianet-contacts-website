﻿using System.Collections.Generic;
using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class IntListFromDelimitedStringTest : IntListFromDelimitedStringValueResolver
    {
        public ICollection<int> TestResolveCore(string input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Int_List_From_String() {
            var input = "1#2#3#4";
            TestResolveCore(input).Should().BeEquivalentTo(new List<int> { 1, 2, 3, 4 });
        }

        [TestMethod]
        public void Should_Resolve_Empty_List_Given_Null_Or_Empty_String() {
            TestResolveCore(null).Should().BeNull();
            TestResolveCore("").Should().BeNull();
        }
    }
}
