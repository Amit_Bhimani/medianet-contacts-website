﻿using System.Collections.Generic;
using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SolrContactIdTest : SolrContactIdValueResolver
    {
        public string TestResolveCore(SolrContact input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Correct_Contact_Display() {
            var model = new SolrContact {
                ContactId = "27538881",
                IsOutlet = false
            };
            ResolveCore(model).Should().Be("27538881");
        }

        [TestMethod]
        public void Should_Resolve_Correct_Outlet_Display() {
            var model = new SolrContact {
                ContactIds = new List<string> {
                    "3067",
                    "3068",
                    "3069",
                    "3070",
                    "3071",
                    "3072",
                    "3073",
                    "3074",
                    "3075",
                    "3076"
                },
                IsOutlet = true
            };
            ResolveCore(model).Should().Be("3067#3068#3069#3070#3071#3072#3073#3074#3075#3076");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_ContactNames_And_Outlet() {
            var model = new SolrContact {
                ContactNames = null,
                IsOutlet = true
            };

            ResolveCore(model).Should().Be("");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_ContactIds_And_Outlet() {
            var model = new SolrContact {
                ContactIds = null,
                IsOutlet = true
            };
            ResolveCore(model).Should().Be("");
        }
    }
}
