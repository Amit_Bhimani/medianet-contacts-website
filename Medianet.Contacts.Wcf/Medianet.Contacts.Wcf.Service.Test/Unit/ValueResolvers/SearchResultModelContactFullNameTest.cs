﻿using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SearchResultModelContactFullNameTest : SearchResultModelContactFullNameValueResolver
    {
        public string TestResolveCore(string input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Corrent_Contact_FullName() {
            var names = "Matt Sun";

            ResolveCore(names).Should().Be("Matt Sun");
        }

        [TestMethod]
        public void Should_Resolve_Correct_Outlet_FullName() {
            var names = "Cathy Morris#Chris Pavlich#Craig Herbert#Matt Sun#Tim Elbra#Vanessa Stubbs";

            ResolveCore(names).Should().Be("Cathy Morris, Chris Pavlich, Craig Herbert, Matt Sun, Tim Elbra, Vanessa Stubbs");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_Or_Empty_ContactNames() {
            string names = null;

            ResolveCore(names).Should().Be(string.Empty);
        }
    }
}
