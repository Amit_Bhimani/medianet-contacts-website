﻿using FluentAssertions;
using Medianet.Contacts.Wcf.Model;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SearchResultModelContactDisplayTest : ContactHashListDisplayValueResolver
    {
        public string TestResolveCore(SearchResultModel input) {
            return ResolveCore(input.FullName);
        }

        [TestMethod]
        public void Should_Resolve_Corrent_Contact_Display() {
            var model = new SearchResultModel {
                FullName = "Matt Sun"
            };

            ResolveCore(model.FullName).Should().Be("Matt Sun");
        }

        [TestMethod]
        public void Should_Resolve_Correct_Outlet_Display() {
            var model = new SearchResultModel {
                FullName = "Cathy Morris#Chris Pavlich#Craig Herbert#Matt Sun#Tim Elbra#Vanessa Stubbs"
            };

            ResolveCore(model.FullName).Should().Be("6 Contacts");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_Or_Empty_Fullname() {
            var model = new SearchResultModel {
                FullName = null
            };

            var model2 = new SearchResultModel {
                FullName = string.Empty
            };

            ResolveCore(model.FullName).Should().Be(string.Empty);
            ResolveCore(model2.FullName).Should().Be(string.Empty);
        }
    }
}
