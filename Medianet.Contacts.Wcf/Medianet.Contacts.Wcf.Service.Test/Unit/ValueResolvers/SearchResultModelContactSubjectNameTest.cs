﻿using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SearchResultModelContactSubjectNameTest : CommaDelimitedStringFromHashDelimitedStringResolver
    {
        public string TestResolveCore(string input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Corrent_Contact_FullName() {
            var subjects = "Business";

            ResolveCore(subjects).Should().Be("Business");
        }

        [TestMethod]
        public void Should_Resolve_Correct_Outlet_FullName() {
            var subjects = "Business#Entertainment (General)#Gossip#National news";

            ResolveCore(subjects).Should().Be("Business, Entertainment (General), Gossip, National news");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_Or_Empty_ContactNames() {
            string subjects = null;

            ResolveCore(subjects).Should().Be(string.Empty);
        }
    }
}
