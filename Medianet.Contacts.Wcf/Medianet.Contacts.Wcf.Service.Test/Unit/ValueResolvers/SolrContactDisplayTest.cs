﻿using System.Collections.Generic;
using FluentAssertions;
using Medianet.Contacts.Wcf.Service.Common.ValueResolvers;
using Medianet.Contacts.Wcf.Service.Solr.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test.Unit.ValueResolvers
{
    [TestClass]
    public class SolrContactDisplayTest : SolrContactDisplayValueResolver
    {
        public string TestResolveCore(SolrContact input) {
            return ResolveCore(input);
        }

        [TestMethod]
        public void Should_Resolve_Correct_Contact_Display() {
            var model = new SolrContact {
                FirstName = "Matt",
                LastName = "Sun",
                IsOutlet = false
            };
            ResolveCore(model).Should().Be("Matt Sun");
        }

        [TestMethod]
        public void Should_Resolve_Correct_Outlet_Display() {
            var model = new SolrContact {
                ContactNames = new List<string> {
                    "Cathy Morris",
                    "Chris Pavlich",
                    "Craig Herbert",
                    "Matt Sun",
                    "Tim Elbra",
                    "Vanessa Stubbs"
                },
                IsOutlet = true
            };

            ResolveCore(model).Should().Be("6 Contacts");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_ContactNames_And_Outlet() {
            var model = new SolrContact {
                ContactNames = null,
                IsOutlet = true
            };

            ResolveCore(model).Should().Be("");
        }

        [TestMethod]
        public void Should_Resolve_Empty_When_Null_Names_And_Contact() {
            var model = new SolrContact {
                FirstName = null,
                MiddleName = "",
                LastName = null,
                IsOutlet = false
            };

            ResolveCore(model).Should().Be("");
        }
    }
}
