﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Search;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Contacts.Wcf.Service.Test
{
    
    
    /// <summary>
    ///This is a test class for SqlSearchTest and is intended
    ///to contain all SqlSearchTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SqlSearchTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            AutoMapperConfigurator.Configure();
        }
        
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Search
        ///</summary>
        [TestMethod()]
        public void SearchTest_ServiceListSearch_Works() {
            SqlSearch target = new SqlSearch();
            int totalResults = 0;
            var so = new SearchOptionsServiceList();
            List<SearchResult> actual;

            so.Type = SearchType.ServiceList;
            so.SearchText = "major metr";
            so.ShowFax = true;
            so.ShowEmail = true;

            actual = target.Search(out totalResults, so, SortColumn.OutletName, SortDirection.ASC, true, 5, 1, 20, null, null, null);
            Assert.IsTrue(actual != null && actual.Count > 0 && totalResults >= actual.Count);
        }

        [TestMethod()]
        public void SearchTest_QuickAdminSearch_Works() {
            SqlSearch target = new SqlSearch();
            int totalResults = 0;
            var so = new SearchOptionsQuickAdmin();
            List<SearchResult> actual;

            so.Type = SearchType.QuickAdmin;
            so.Status = ContactStatus.Active;
            so.SearchText = "david";

            actual = target.Search(out totalResults, so, SortColumn.OutletName, SortDirection.ASC, true, 5, 1, 20, null, null, null);
            Assert.IsTrue(actual != null && actual.Count > 0 && totalResults >= actual.Count);
        }

        [TestMethod()]
        public void SearchTest_Autosuggest_Works() {
            SqlSearch target = new SqlSearch();
            var options = new AutoSuggestOptions();
            List<AutoSuggest> actual;

            options.Context = SearchContext.People;
            options.SearchText = "david";
            options.IsAdminSearch = true;

            actual = target.AutoSuggest(options, 20, 5);
            Assert.IsTrue(actual != null && actual.Count > 0);
        }

        [TestMethod()]
        public void SearchTestAdvancedAdminSearchWorks()
        {
            SqlSearch target = new SqlSearch();
            int totalResults = 0;
            var so = new SearchOptionsAdvanced();
            List<SearchResult> actual;

            so.Type = SearchType.Advanced;
            List<int> lstGroup = new List<int>();
            lstGroup.Add(8149);
            so.TaskGroups = lstGroup;

            List<int> lstCat = new List<int>();
            lstCat.Add(6);
            lstCat.Add(3);
            lstCat.Add(9);
            lstCat.Add(11);
            so.TaskCategories = lstCat;

            so.ContactPostCodeList = new List<string>();
            so.OutletStationFrequencyList = new List<string>();
            so.OutletPostCodeList = new List<string>();

            actual = target.Search(out totalResults, so, SortColumn.OutletName, SortDirection.ASC, true, 34548, 0, 200, null, null, null);
            Assert.IsTrue(actual != null && actual.Count > 0 && totalResults >= actual.Count);
        }
    }
}
