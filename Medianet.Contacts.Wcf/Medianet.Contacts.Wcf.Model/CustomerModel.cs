﻿using System;
using System.Collections.Generic;

namespace Medianet.Contacts.Wcf.Model
{
    public class CustomerModel
    {
        public string DebtorNumber { get; set; }
        public string CustomerName { get; set; }
        public string CompanyLogonName { get; set; }
        public string FilerCode { get; set; }
        public string Pin { get; set; }
        public string SalesEmail { get; set; }
        public int? DefaultCoverPageId { get; set; }
        public char Status { get; set; }
        public string Comment { get; set; }
        public Int16 NoOfLicenses { get; set; }
        public string MasterUserID { get; set; }
        public string MasterPassword { get; set; }
        public char ReportMethod { get; set; }
        public char ResultsType { get; set; }
        public bool SendResultsData { get; set; }
        public char OptOutMethod { get; set; }
        public bool OptoutFax { get; set; }
        public bool OptoutEmail { get; set; }
        public bool OptoutSMS { get; set; }
        public string OriginatorEmail { get; set; }
        public string FreecallNo { get; set; }
        public string Timezone { get; set; }
        public DateTime CreatedTime { get; set; }
        public int CreatedUserId { get; set; }
        public DateTime ModifiedTime { get; set; }
        public int ModifiedUserId { get; set; }
        public char? MediaDirectorySystem { get; set; }

        public ICollection<UserModel> Users { get; set; }
        public ICollection<DataModuleModel> DataModules { get; set; }
    }
}
