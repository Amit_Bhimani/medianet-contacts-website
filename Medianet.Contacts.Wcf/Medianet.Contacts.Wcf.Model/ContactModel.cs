﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Wcf.Model
{
    public class ContactModel
    {
        // Meta info.
        public int Id { get; set; }
        public bool IsDangling { get; set; }
        public bool IsDirty { get; set; }

        public bool IsAap { get; set; }
        public bool IsOutlet { get; set; }
        public bool IsPrivate { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime LastModified { get; set; }

        // Contact.
        public string ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string JobTitle { get; set; }
        public string PositionIds { get; set; }
        public bool? IsPrimaryContact { get; set; }
        public string ContactPreferredDeliveryMethod { get; set; }
        public string ContactSubjectIds { get; set; }
        public string ContactSubjectNames { get; set; }
        public string ContactSubjectGroupIds { get; set; }
        public string ContactSystemNotes { get; set; }
        public string ContactTwitter { get; set; }
        public string ContactLinkedIn { get; set; }
        public string ContactFacebook { get; set; }
        public string ContactInstagram { get; set; }
        public string ContactYouTube { get; set; }
        public string ContactSnapchat { get; set; }
        public string ContactSkype { get; set; }
        public string ContactBio { get; set; }
        public bool? ContactHasEmail { get; set; }
        public bool? ContactHasFax { get; set; }
        public bool? ContactHasPostal { get; set; }
        public string ContactCityName { get; set; }
        public decimal? ContactContinentId { get; set; }
        public decimal? ContactCountryId { get; set; }
        public decimal? ContactStateId { get; set; }
        public string ContactStateName { get; set; }
        public decimal? ContactCityId { get; set; }
        public string ContactPostcode { get; set; }

        public string ContactBugBears { get; set; }
        public string ContactAlsoKnownAs { get; set; }
        public string ContactPressReleaseInterests { get; set; }
        public string ContactPersonalInterests { get; set; }
        public string ContactAppearsIn { get; set; }
        public int? ContactMediaInfluencerScore { get; set; }
        public string ContactCurrentStatus { get; set; }
        public string ContactBasedInLocation { get; set; }
        public string ContactAlternativeEmailAddress { get; set; }
        public string ContactWebsite { get; set; }
        //public bool IsContactOutOfOffice { get; set; }
        public DateTime? ContactOooStartDate { get; set; }
        public DateTime? ContactOooReturnDate { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactMobileNumber { get; set; }
        public string ContactEmailAddress { get; set; }
        public string ContactPriorityNotice { get; set; }
        public string ContactAuthorOf { get; set; }

        // Outlet.
        public string OutletId { get; set; }
        public string ContactIds { get; set; }
        public string ContactNames { get; set; }
        public string OutletName { get; set; }
        public string OutletPreferredDeliveryMethod { get; set; }
        public string OutletSubjectIds { get; set; }
        public string OutletSubjectNames { get; set; }
        public string OutletSubjectGroupIds { get; set; }
        public string OutletSystemNotes { get; set; }
        public string OutletTwitter { get; set; }
        public string OutletLinkedIn { get; set; }
        public string OutletFacebook { get; set; }
        public string OutletInstagram { get; set; }
        public string OutletYouTube { get; set; }
        public string OutletSnapchat { get; set; }
        public string OutletSkype { get; set; }
        public string OutletBio { get; set; }
        public bool? OutletHasEmail { get; set; }
        public bool? OutletHasFax { get; set; }
        public bool? OutletHasPostal { get; set; }
        public bool? OutletIsOffice { get; set; }
        public bool? OutletIsFinancial { get; set; }
        public bool? OutletIsPolitician { get; set; }
        public string OutletCityName { get; set; }
        public decimal? OutletContinentId { get; set; }
        public decimal? OutletCountryId { get; set; }
        public decimal? OutletStateId { get; set; }
        public string OutletStateName { get; set; }
        public decimal? OutletCityId { get; set; }
        public string OutletPostcode { get; set; }

        public decimal? OutletPostalCityId { get; set; }
        public decimal? OutletPostalCountryId { get; set; }
        public decimal? OutletPostalContinentId { get; set; }
        public bool? OutletSubscriptionOnlyPublication { get; set; }
        public string OutletAlsoKnownAs { get; set; }
        public string OutletAdditionalWebsite { get; set; }
        public string OutletPublishedOn { get; set; }
        public string OutletPressReleaseInterests { get; set; }
        public string OutletRegionsCovered { get; set; }
        public string OutletPhoneNumber { get; set; }
        public string OutletEmailAddress { get; set; }
        public string OutletWebsite { get; set; }
        public string OutletLogoFilename { get; set; }
        public bool? OutletIsGeneric { get; set; }
        public string OutletPriorityNotice { get; set; }

        public string OwnerDebtorNumber { get; set; }

        // Common.
        public byte RecordType { get; set; }
        public string MediaTypeId { get; set; }
        public string MediaType { get; set; }
        public decimal? FrequencyId { get; set; }
        public decimal? Circulation { get; set; }
        public string LanguageIds { get; set; }
        public string LanguageNames { get; set; }
        public string StationFrequency { get; set; }
        public string NewsFocusIds { get; set; }

        public bool IsPrivateOrAAP() {
            return (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.MediaContact ||
                   (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.MediaOutlet ||
                   (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.OmaOutlet ||
                   (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.OmaContactAtMediaOutlet ||
                   (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.OmaContactAtOmaOutlet ||
                   (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.OmaContactAtPrnOutlet ||
                   (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.OmaContactNoOutlet;
        }

        public bool IsPrn() {
            return (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.PrnContact ||
                   (RecordType) RecordType == Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.PrnContact;
        }
    }
}
