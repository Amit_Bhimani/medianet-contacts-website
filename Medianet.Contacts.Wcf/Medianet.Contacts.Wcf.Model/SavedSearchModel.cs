﻿using System;

namespace Medianet.Contacts.Wcf.Model
{
    public class SavedSearchModel
    {
        public int SearchId { get; set; }
        public string SearchName { get; set; }
        public string SearchQuery { get; set; }
        public string SearchCriteria { get; set; }
        public Byte Status { get; set; }
        public int GroupId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int UserId { get; set; }
        public bool Visibility { get; set; }
        public string ContextText { get; set; }
    }
}
