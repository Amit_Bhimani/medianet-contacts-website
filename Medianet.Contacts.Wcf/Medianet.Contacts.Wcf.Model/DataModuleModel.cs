﻿namespace Medianet.Contacts.Wcf.Model
{
    public class DataModuleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ContinentId { get; set; }
        public ContinentModel Continent { get; set; }
    }
}