﻿using System;

namespace Medianet.Contacts.Wcf.Model
{
    public class IndexDeleteModel
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public DateTime LastModified { get; set; }
    }
}
