﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Contacts.Wcf.Model
{
    public class MediaOutletModel
    {
        public string Id { get; set; }
        public bool? IsDirty { get; set; }

    }
}
