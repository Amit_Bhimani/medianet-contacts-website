﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;

namespace Medianet.Contacts.Wcf.Model
{
    public class MedianetContactsContext : DbContext
    {
        public DbSet<ContactModel> Contacts { get; set; }
        public DbSet<AapContactModel> AapContacts { get; set; }
        public DbSet<PrnContactModel> PrnContacts { get; set; }
        public DbSet<CustomerModel> Customers { get; set; }
        public DbSet<SavedSearchModel> SavedSearches { get; set; }
        public DbSet<UserModel> Users { get; set; }
        public DbSet<ContinentModel> Continents { get; set; }
        public DbSet<DataModuleModel> DataModules { get; set; }
        public DbSet<SubjectModel> Subjects { get; set; }
        public DbSet<SubjectGroupModel> SubjectGroups { get; set; }
        public DbSet<RoleModel> Roles { get; set; }
        public DbSet<ProductModel> Products { get; set; }
        public DbSet<IndexUpdateModel> IndexUpdateContacts { get; set; }
        public DbSet<IndexDeleteModel> IndexDeleteContacts { get; set; }
        public DbSet<MediaContactModel> MediaContacts { get; set; }

        public DbSet<MediaOutletModel> MediaOutlets { get; set; }

        public MedianetContactsContext() {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 300;
        }

        protected override void OnModelCreating(DbModelBuilder builder) {

            #region Main Denormalized View

            // Contacts.
            builder.Entity<ContactModel>().ToTable("vw_contact");
            builder.Entity<ContactModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("id");
            builder.Entity<ContactModel>().Property(p => p.RecordType).HasColumnName("record_type");
            builder.Entity<ContactModel>().Property(p => p.ContactId).HasColumnName("contact_id");
            builder.Entity<ContactModel>().Property(p => p.FirstName).HasColumnName("first_name");
            builder.Entity<ContactModel>().Property(p => p.MiddleName).HasColumnName("middle_name");
            builder.Entity<ContactModel>().Property(p => p.LastName).HasColumnName("last_name");
            builder.Entity<ContactModel>().Property(p => p.JobTitle).HasColumnName("job_title");
            builder.Entity<ContactModel>().Property(p => p.PositionIds).HasColumnName("position_ids");
            builder.Entity<ContactModel>().Property(p => p.IsPrimaryContact).HasColumnName("is_primary_contact");
            builder.Entity<ContactModel>().Property(p => p.ContactPreferredDeliveryMethod).HasColumnName("contact_preferred_delivery_method");
            builder.Entity<ContactModel>().Property(p => p.ContactSubjectIds).HasColumnName("contact_subject_ids");
            builder.Entity<ContactModel>().Property(p => p.ContactSubjectNames).HasColumnName("contact_subject_names");
            builder.Entity<ContactModel>().Property(p => p.ContactSubjectGroupIds).HasColumnName("contact_subject_group_ids");
            builder.Entity<ContactModel>().Property(p => p.ContactSystemNotes).HasColumnName("contact_system_notes");
            builder.Entity<ContactModel>().Property(p => p.ContactTwitter).HasColumnName("contact_twitter");
            builder.Entity<ContactModel>().Property(p => p.ContactLinkedIn).HasColumnName("contact_linkedIn");
            builder.Entity<ContactModel>().Property(p => p.ContactFacebook).HasColumnName("contact_facebook");
            builder.Entity<ContactModel>().Property(p => p.ContactInstagram).HasColumnName("contact_instagram");
            builder.Entity<ContactModel>().Property(p => p.ContactYouTube).HasColumnName("contact_youtube");
            builder.Entity<ContactModel>().Property(p => p.ContactSnapchat).HasColumnName("contact_snapchat");
            builder.Entity<ContactModel>().Property(p => p.ContactSkype).HasColumnName("contact_skype");
            builder.Entity<ContactModel>().Property(p => p.ContactBio).HasColumnName("contact_bio");
            builder.Entity<ContactModel>().Property(p => p.ContactHasEmail).HasColumnName("contact_has_email");
            builder.Entity<ContactModel>().Property(p => p.ContactHasFax).HasColumnName("contact_has_fax");
            builder.Entity<ContactModel>().Property(p => p.ContactHasPostal).HasColumnName("contact_has_postal");
            builder.Entity<ContactModel>().Property(p => p.ContactCityName).HasColumnName("contact_city_name");
            builder.Entity<ContactModel>().Property(p => p.ContactContinentId).HasColumnName("contact_continent_id");
            builder.Entity<ContactModel>().Property(p => p.ContactCountryId).HasColumnName("contact_country_id");
            builder.Entity<ContactModel>().Property(p => p.ContactStateId).HasColumnName("contact_state_id");
            builder.Entity<ContactModel>().Property(p => p.ContactStateName).HasColumnName("contact_state_name");
            builder.Entity<ContactModel>().Property(p => p.ContactCityId).HasColumnName("contact_city_id");
            builder.Entity<ContactModel>().Property(p => p.ContactPostcode).HasColumnName("contact_postcode");
            builder.Entity<ContactModel>().Property(p => p.ContactBugBears).HasColumnName("contact_bug_bears");
            builder.Entity<ContactModel>().Property(p => p.ContactAlsoKnownAs).HasColumnName("contact_also_known_as");
            builder.Entity<ContactModel>().Property(p => p.ContactPressReleaseInterests).HasColumnName("contact_press_release_interests");
            builder.Entity<ContactModel>().Property(p => p.ContactPersonalInterests).HasColumnName("contact_personal_interests");
            builder.Entity<ContactModel>().Property(p => p.ContactAppearsIn).HasColumnName("contact_appears_in");
            builder.Entity<ContactModel>().Property(p => p.ContactMediaInfluencerScore).HasColumnName("contact_media_influencer_score");
            builder.Entity<ContactModel>().Property(p => p.ContactCurrentStatus).HasColumnName("contact_current_status");
            builder.Entity<ContactModel>().Property(p => p.ContactBasedInLocation).HasColumnName("contact_based_in_location");
            builder.Entity<ContactModel>().Property(p => p.ContactAlternativeEmailAddress).HasColumnName("contact_alternative_email_address");
            builder.Entity<ContactModel>().Property(p => p.ContactWebsite).HasColumnName("contact_website");
            //builder.Entity<ContactModel>().Property(p => p.IsContactOutOfOffice).HasColumnName("is_contact_out_of_office");
            builder.Entity<ContactModel>().Property(p => p.ContactOooStartDate).HasColumnName("contact_ooo_start_date");
            builder.Entity<ContactModel>().Property(p => p.ContactOooReturnDate).HasColumnName("contact_ooo_return_date");
            builder.Entity<ContactModel>().Property(p => p.ContactPhoneNumber).HasColumnName("contact_phone_number");
            builder.Entity<ContactModel>().Property(p => p.ContactMobileNumber).HasColumnName("contact_mobile_number");
            builder.Entity<ContactModel>().Property(p => p.ContactEmailAddress).HasColumnName("contact_email_address");
            builder.Entity<ContactModel>().Property(p => p.ContactPriorityNotice).HasColumnName("contact_priority_notice");
            builder.Entity<ContactModel>().Property(p => p.ContactAuthorOf).HasColumnName("contact_author_of");
            builder.Entity<ContactModel>().Property(p => p.OutletId).HasColumnName("outlet_id");
            builder.Entity<ContactModel>().Property(p => p.ContactIds).HasColumnName("contact_ids");
            builder.Entity<ContactModel>().Property(p => p.ContactNames).HasColumnName("contact_names");
            builder.Entity<ContactModel>().Property(p => p.OutletName).HasColumnName("outlet_name");
            builder.Entity<ContactModel>().Property(p => p.OutletPreferredDeliveryMethod).HasColumnName("outlet_preferred_delivery_method");
            builder.Entity<ContactModel>().Property(p => p.OutletSubjectIds).HasColumnName("outlet_subject_ids");
            builder.Entity<ContactModel>().Property(p => p.OutletSubjectNames).HasColumnName("outlet_subject_names");
            builder.Entity<ContactModel>().Property(p => p.OutletSubjectGroupIds).HasColumnName("outlet_subject_group_ids");
            builder.Entity<ContactModel>().Property(p => p.MediaTypeId).HasColumnName("media_type_id");
            builder.Entity<ContactModel>().Property(p => p.MediaType).HasColumnName("media_type");
            builder.Entity<ContactModel>().Property(p => p.FrequencyId).HasColumnName("frequency_id");
            builder.Entity<ContactModel>().Property(p => p.Circulation).HasColumnName("circulation");
            builder.Entity<ContactModel>().Property(p => p.LanguageIds).HasColumnName("language_ids");
            builder.Entity<ContactModel>().Property(p => p.LanguageNames).HasColumnName("language_names");
            builder.Entity<ContactModel>().Property(p => p.StationFrequency).HasColumnName("station_frequency");
            builder.Entity<ContactModel>().Property(p => p.NewsFocusIds).HasColumnName("news_focus_ids");
            builder.Entity<ContactModel>().Property(p => p.OutletSystemNotes).HasColumnName("outlet_system_notes");
            builder.Entity<ContactModel>().Property(p => p.OutletTwitter).HasColumnName("outlet_twitter");
            builder.Entity<ContactModel>().Property(p => p.OutletLinkedIn).HasColumnName("outlet_linkedIn");
            builder.Entity<ContactModel>().Property(p => p.OutletFacebook).HasColumnName("outlet_facebook");
            builder.Entity<ContactModel>().Property(p => p.OutletInstagram).HasColumnName("outlet_instagram");
            builder.Entity<ContactModel>().Property(p => p.OutletYouTube).HasColumnName("outlet_youtube");
            builder.Entity<ContactModel>().Property(p => p.OutletSnapchat).HasColumnName("outlet_snapchat");
            builder.Entity<ContactModel>().Property(p => p.OutletSkype).HasColumnName("outlet_skype");
            builder.Entity<ContactModel>().Property(p => p.OutletBio).HasColumnName("outlet_bio");
            builder.Entity<ContactModel>().Property(p => p.OutletHasEmail).HasColumnName("outlet_has_email");
            builder.Entity<ContactModel>().Property(p => p.OutletHasFax).HasColumnName("outlet_has_fax");
            builder.Entity<ContactModel>().Property(p => p.OutletHasPostal).HasColumnName("outlet_has_postal");
            builder.Entity<ContactModel>().Property(p => p.OutletIsOffice).HasColumnName("outlet_is_office");
            builder.Entity<ContactModel>().Property(p => p.OutletIsFinancial).HasColumnName("outlet_is_financial");
            builder.Entity<ContactModel>().Property(p => p.OutletIsPolitician).HasColumnName("outlet_is_politician");
            builder.Entity<ContactModel>().Property(p => p.OutletCityName).HasColumnName("outlet_city_name");
            builder.Entity<ContactModel>().Property(p => p.OutletContinentId).HasColumnName("outlet_continent_id");
            builder.Entity<ContactModel>().Property(p => p.OutletCountryId).HasColumnName("outlet_country_id");
            builder.Entity<ContactModel>().Property(p => p.OutletStateId).HasColumnName("outlet_state_id");
            builder.Entity<ContactModel>().Property(p => p.OutletStateName).HasColumnName("outlet_state_name");
            builder.Entity<ContactModel>().Property(p => p.OutletCityId).HasColumnName("outlet_city_id");
            builder.Entity<ContactModel>().Property(p => p.OutletPostcode).HasColumnName("outlet_postcode");
            builder.Entity<ContactModel>().Property(p => p.OutletPostalCityId).HasColumnName("outlet_postal_city_id");
            builder.Entity<ContactModel>().Property(p => p.OutletPostalCountryId).HasColumnName("outlet_postal_country_id");
            builder.Entity<ContactModel>().Property(p => p.OutletPostalContinentId).HasColumnName("outlet_postal_continent_id");
            builder.Entity<ContactModel>().Property(p => p.OutletSubscriptionOnlyPublication).HasColumnName("outlet_subscription_only_publication");
            builder.Entity<ContactModel>().Property(p => p.OutletAlsoKnownAs).HasColumnName("outlet_also_known_as");
            builder.Entity<ContactModel>().Property(p => p.OutletAdditionalWebsite).HasColumnName("outlet_additional_website");
            builder.Entity<ContactModel>().Property(p => p.OutletPublishedOn).HasColumnName("outlet_published_on");
            builder.Entity<ContactModel>().Property(p => p.OutletPressReleaseInterests).HasColumnName("outlet_press_release_interests");
            builder.Entity<ContactModel>().Property(p => p.OutletRegionsCovered).HasColumnName("outlet_regions_covered");
            builder.Entity<ContactModel>().Property(p => p.OutletPhoneNumber).HasColumnName("outlet_phone_number");
            builder.Entity<ContactModel>().Property(p => p.OutletEmailAddress).HasColumnName("outlet_email_address");
            builder.Entity<ContactModel>().Property(p => p.OutletWebsite).HasColumnName("outlet_website");
            builder.Entity<ContactModel>().Property(p => p.OutletLogoFilename).HasColumnName("outlet_logo_filename");
            builder.Entity<ContactModel>().Property(p => p.OutletIsGeneric).HasColumnName("outlet_is_generic");
            builder.Entity<ContactModel>().Property(p => p.OutletPriorityNotice).HasColumnName("outlet_priority_notice");
            builder.Entity<ContactModel>().Property(p => p.OwnerDebtorNumber).HasColumnName("owner_debtor_number");
            builder.Entity<ContactModel>().Property(p => p.IsDangling).HasColumnName("is_dangling");
            builder.Entity<ContactModel>().Property(p => p.IsAap).HasColumnName("is_aap");
            builder.Entity<ContactModel>().Property(p => p.IsOutlet).HasColumnName("is_outlet");
            builder.Entity<ContactModel>().Property(p => p.IsPrivate).HasColumnName("is_private");
            builder.Entity<ContactModel>().Property(p => p.IsDirty).HasColumnName("is_dirty");
            builder.Entity<ContactModel>().Property(p => p.CreatedDate).HasColumnName("created_date");
            builder.Entity<ContactModel>().Property(p => p.LastModified).HasColumnName("last_modified");

            #endregion

            #region AAP Denormalized Table

            builder.Entity<AapContactModel>().ToTable("contact");
            builder.Entity<AapContactModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("id");
            builder.Entity<AapContactModel>().Property(p => p.RecordType).HasColumnName("record_type");
            builder.Entity<AapContactModel>().Property(p => p.ContactId).HasColumnName("contact_id");
            builder.Entity<AapContactModel>().Property(p => p.FirstName).HasColumnName("first_name");
            builder.Entity<AapContactModel>().Property(p => p.MiddleName).HasColumnName("middle_name");
            builder.Entity<AapContactModel>().Property(p => p.LastName).HasColumnName("last_name");
            builder.Entity<AapContactModel>().Property(p => p.JobTitle).HasColumnName("job_title");
            builder.Entity<AapContactModel>().Property(p => p.PositionIds).HasColumnName("position_ids");
            builder.Entity<AapContactModel>().Property(p => p.IsPrimaryContact).HasColumnName("is_primary_contact");
            builder.Entity<AapContactModel>().Property(p => p.ContactPreferredDeliveryMethod).HasColumnName("contact_preferred_delivery_method");
            builder.Entity<AapContactModel>().Property(p => p.ContactSubjectIds).HasColumnName("contact_subject_ids");
            builder.Entity<AapContactModel>().Property(p => p.ContactSubjectNames).HasColumnName("contact_subject_names");
            builder.Entity<AapContactModel>().Property(p => p.ContactSubjectGroupIds).HasColumnName("contact_subject_group_ids");
            builder.Entity<AapContactModel>().Property(p => p.ContactSystemNotes).HasColumnName("contact_system_notes");
            builder.Entity<AapContactModel>().Property(p => p.ContactTwitter).HasColumnName("contact_twitter");
            builder.Entity<AapContactModel>().Property(p => p.ContactLinkedIn).HasColumnName("contact_linkedIn");
            builder.Entity<AapContactModel>().Property(p => p.ContactFacebook).HasColumnName("contact_facebook");
            builder.Entity<AapContactModel>().Property(p => p.ContactInstagram).HasColumnName("contact_instagram");
            builder.Entity<AapContactModel>().Property(p => p.ContactYouTube).HasColumnName("contact_youtube");
            builder.Entity<AapContactModel>().Property(p => p.ContactSnapchat).HasColumnName("contact_snapchat");
            builder.Entity<AapContactModel>().Property(p => p.ContactSkype).HasColumnName("contact_skype");
            builder.Entity<AapContactModel>().Property(p => p.ContactBio).HasColumnName("contact_bio");
            builder.Entity<AapContactModel>().Property(p => p.ContactHasEmail).HasColumnName("contact_has_email");
            builder.Entity<AapContactModel>().Property(p => p.ContactHasFax).HasColumnName("contact_has_fax");
            builder.Entity<AapContactModel>().Property(p => p.ContactHasPostal).HasColumnName("contact_has_postal");
            builder.Entity<AapContactModel>().Property(p => p.ContactCityName).HasColumnName("contact_city_name");
            builder.Entity<AapContactModel>().Property(p => p.ContactContinentId).HasColumnName("contact_continent_id");
            builder.Entity<AapContactModel>().Property(p => p.ContactCountryId).HasColumnName("contact_country_id");
            builder.Entity<AapContactModel>().Property(p => p.ContactStateId).HasColumnName("contact_state_id");
            builder.Entity<AapContactModel>().Property(p => p.ContactStateName).HasColumnName("contact_state_name");
            builder.Entity<AapContactModel>().Property(p => p.ContactCityId).HasColumnName("contact_city_id");
            builder.Entity<AapContactModel>().Property(p => p.ContactPostcode).HasColumnName("contact_postcode");
            builder.Entity<AapContactModel>().Property(p => p.ContactBugBears).HasColumnName("contact_bug_bears");
            builder.Entity<AapContactModel>().Property(p => p.ContactAlsoKnownAs).HasColumnName("contact_also_known_as");
            builder.Entity<AapContactModel>().Property(p => p.ContactPressReleaseInterests).HasColumnName("contact_press_release_interests");
            builder.Entity<AapContactModel>().Property(p => p.ContactPersonalInterests).HasColumnName("contact_personal_interests");
            builder.Entity<AapContactModel>().Property(p => p.ContactAppearsIn).HasColumnName("contact_appears_in");
            builder.Entity<AapContactModel>().Property(p => p.ContactMediaInfluencerScore).HasColumnName("contact_media_influencer_score");
            builder.Entity<AapContactModel>().Property(p => p.ContactCurrentStatus).HasColumnName("contact_current_status");
            builder.Entity<AapContactModel>().Property(p => p.ContactBasedInLocation).HasColumnName("contact_based_in_location");
            builder.Entity<AapContactModel>().Property(p => p.ContactAlternativeEmailAddress).HasColumnName("contact_alternative_email_address");
            builder.Entity<AapContactModel>().Property(p => p.ContactWebsite).HasColumnName("contact_website");
            //builder.Entity<AapContactModel>().Property(p => p.IsContactOutOfOffice).HasColumnName("is_contact_out_of_office");
            builder.Entity<AapContactModel>().Property(p => p.ContactOooStartDate).HasColumnName("contact_ooo_start_date");
            builder.Entity<AapContactModel>().Property(p => p.ContactOooReturnDate).HasColumnName("contact_ooo_return_date");
            builder.Entity<AapContactModel>().Property(p => p.ContactPhoneNumber).HasColumnName("contact_phone_number");
            builder.Entity<AapContactModel>().Property(p => p.ContactMobileNumber).HasColumnName("contact_mobile_number");
            builder.Entity<AapContactModel>().Property(p => p.ContactEmailAddress).HasColumnName("contact_email_address");
            builder.Entity<AapContactModel>().Property(p => p.OutletId).HasColumnName("outlet_id");
            builder.Entity<AapContactModel>().Property(p => p.ContactIds).HasColumnName("contact_ids");
            builder.Entity<AapContactModel>().Property(p => p.ContactNames).HasColumnName("contact_names");
            builder.Entity<AapContactModel>().Property(p => p.OutletName).HasColumnName("outlet_name");
            builder.Entity<AapContactModel>().Property(p => p.OutletPreferredDeliveryMethod).HasColumnName("outlet_preferred_delivery_method");
            builder.Entity<AapContactModel>().Property(p => p.OutletSubjectIds).HasColumnName("outlet_subject_ids");
            builder.Entity<AapContactModel>().Property(p => p.OutletSubjectNames).HasColumnName("outlet_subject_names");
            builder.Entity<AapContactModel>().Property(p => p.OutletSubjectGroupIds).HasColumnName("outlet_subject_group_ids");
            builder.Entity<AapContactModel>().Property(p => p.MediaTypeId).HasColumnName("media_type_id");
            builder.Entity<AapContactModel>().Property(p => p.MediaType).HasColumnName("media_type");
            builder.Entity<AapContactModel>().Property(p => p.FrequencyId).HasColumnName("frequency_id");
            builder.Entity<AapContactModel>().Property(p => p.Circulation).HasColumnName("circulation");
            builder.Entity<AapContactModel>().Property(p => p.LanguageIds).HasColumnName("language_ids");
            builder.Entity<AapContactModel>().Property(p => p.LanguageNames).HasColumnName("language_names");
            builder.Entity<AapContactModel>().Property(p => p.StationFrequency).HasColumnName("station_frequency");
            builder.Entity<AapContactModel>().Property(p => p.NewsFocusIds).HasColumnName("news_focus_ids");
            builder.Entity<AapContactModel>().Property(p => p.OutletSystemNotes).HasColumnName("outlet_system_notes");
            builder.Entity<AapContactModel>().Property(p => p.OutletTwitter).HasColumnName("outlet_twitter");
            builder.Entity<AapContactModel>().Property(p => p.OutletLinkedIn).HasColumnName("outlet_linkedIn");
            builder.Entity<AapContactModel>().Property(p => p.OutletFacebook).HasColumnName("outlet_facebook");
            builder.Entity<AapContactModel>().Property(p => p.OutletInstagram).HasColumnName("outlet_instagram");
            builder.Entity<AapContactModel>().Property(p => p.OutletYouTube).HasColumnName("outlet_youtube");
            builder.Entity<AapContactModel>().Property(p => p.OutletSnapchat).HasColumnName("outlet_snapchat");
            builder.Entity<AapContactModel>().Property(p => p.OutletSkype).HasColumnName("outlet_skype");
            builder.Entity<AapContactModel>().Property(p => p.OutletBio).HasColumnName("outlet_bio");
            builder.Entity<AapContactModel>().Property(p => p.OutletHasEmail).HasColumnName("outlet_has_email");
            builder.Entity<AapContactModel>().Property(p => p.OutletHasFax).HasColumnName("outlet_has_fax");
            builder.Entity<AapContactModel>().Property(p => p.OutletHasPostal).HasColumnName("outlet_has_postal");
            builder.Entity<AapContactModel>().Property(p => p.OutletIsOffice).HasColumnName("outlet_is_office");
            builder.Entity<AapContactModel>().Property(p => p.OutletIsFinancial).HasColumnName("outlet_is_financial");
            builder.Entity<AapContactModel>().Property(p => p.OutletIsPolitician).HasColumnName("outlet_is_politician");
            builder.Entity<AapContactModel>().Property(p => p.OutletCityName).HasColumnName("outlet_city_name");
            builder.Entity<AapContactModel>().Property(p => p.OutletContinentId).HasColumnName("outlet_continent_id");
            builder.Entity<AapContactModel>().Property(p => p.OutletCountryId).HasColumnName("outlet_country_id");
            builder.Entity<AapContactModel>().Property(p => p.OutletStateId).HasColumnName("outlet_state_id");
            builder.Entity<AapContactModel>().Property(p => p.OutletStateName).HasColumnName("outlet_state_name");
            builder.Entity<AapContactModel>().Property(p => p.OutletCityId).HasColumnName("outlet_city_id");
            builder.Entity<AapContactModel>().Property(p => p.OutletPostcode).HasColumnName("outlet_postcode");
            builder.Entity<AapContactModel>().Property(p => p.OutletPostalCityId).HasColumnName("outlet_postal_city_id");
            builder.Entity<AapContactModel>().Property(p => p.OutletPostalCountryId).HasColumnName("outlet_postal_country_id");
            builder.Entity<AapContactModel>().Property(p => p.OutletPostalContinentId).HasColumnName("outlet_postal_continent_id");
            builder.Entity<AapContactModel>().Property(p => p.OutletSubscriptionOnlyPublication).HasColumnName("outlet_subscription_only_publication");
            builder.Entity<AapContactModel>().Property(p => p.OutletAlsoKnownAs).HasColumnName("outlet_also_known_as");
            builder.Entity<AapContactModel>().Property(p => p.OutletAdditionalWebsite).HasColumnName("outlet_additional_website");
            builder.Entity<AapContactModel>().Property(p => p.OutletPublishedOn).HasColumnName("outlet_published_on");
            builder.Entity<AapContactModel>().Property(p => p.OutletPressReleaseInterests).HasColumnName("outlet_press_release_interests");
            builder.Entity<AapContactModel>().Property(p => p.OutletRegionsCovered).HasColumnName("outlet_regions_covered");
            builder.Entity<AapContactModel>().Property(p => p.OutletPhoneNumber).HasColumnName("outlet_phone_number");
            builder.Entity<AapContactModel>().Property(p => p.OutletEmailAddress).HasColumnName("outlet_email_address");
            builder.Entity<AapContactModel>().Property(p => p.OutletWebsite).HasColumnName("outlet_website");
            builder.Entity<AapContactModel>().Property(p => p.OutletLogoFilename).HasColumnName("outlet_logo_filename");
            builder.Entity<AapContactModel>().Property(p => p.OutletIsGeneric).HasColumnName("outlet_is_generic");
            builder.Entity<AapContactModel>().Property(p => p.OwnerDebtorNumber).HasColumnName("owner_debtor_number");
            builder.Entity<AapContactModel>().Property(p => p.IsDangling).HasColumnName("is_dangling");
            builder.Entity<AapContactModel>().Property(p => p.IsDirty).HasColumnName("is_dirty");
            builder.Entity<AapContactModel>().Property(p => p.CreatedDate).HasColumnName("created_date");
            builder.Entity<AapContactModel>().Property(p => p.LastModified).HasColumnName("last_modified");

            #endregion

            #region PRN Denormalized Table

            builder.Entity<PrnContactModel>().ToTable("prn_contact");
            builder.Entity<PrnContactModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("id");
            builder.Entity<PrnContactModel>().Property(p => p.RecordType).HasColumnName("record_type");
            builder.Entity<PrnContactModel>().Property(p => p.ContactId).HasColumnName("contact_id");
            builder.Entity<PrnContactModel>().Property(p => p.FirstName).HasColumnName("first_name");
            builder.Entity<PrnContactModel>().Property(p => p.MiddleName).HasColumnName("middle_name");
            builder.Entity<PrnContactModel>().Property(p => p.LastName).HasColumnName("last_name");
            builder.Entity<PrnContactModel>().Property(p => p.JobTitle).HasColumnName("job_title");
            builder.Entity<PrnContactModel>().Property(p => p.PositionIds).HasColumnName("position_ids");
            builder.Entity<PrnContactModel>().Property(p => p.IsPrimaryContact).HasColumnName("is_primary_contact");
            builder.Entity<PrnContactModel>().Property(p => p.ContactPreferredDeliveryMethod).HasColumnName("contact_preferred_delivery_method");
            builder.Entity<PrnContactModel>().Property(p => p.ContactSubjectIds).HasColumnName("contact_subject_ids");
            builder.Entity<PrnContactModel>().Property(p => p.ContactSubjectNames).HasColumnName("contact_subject_names");
            builder.Entity<PrnContactModel>().Property(p => p.ContactSubjectGroupIds).HasColumnName("contact_subject_group_ids");
            builder.Entity<PrnContactModel>().Property(p => p.ContactSystemNotes).HasColumnName("contact_system_notes");
            builder.Entity<PrnContactModel>().Property(p => p.ContactTwitter).HasColumnName("contact_twitter");
            builder.Entity<PrnContactModel>().Property(p => p.ContactLinkedIn).HasColumnName("contact_linkedIn");
            builder.Entity<PrnContactModel>().Property(p => p.ContactFacebook).HasColumnName("contact_facebook");
            builder.Entity<PrnContactModel>().Property(p => p.ContactInstagram).HasColumnName("contact_instagram");
            builder.Entity<PrnContactModel>().Property(p => p.ContactYouTube).HasColumnName("contact_youtube");
            builder.Entity<PrnContactModel>().Property(p => p.ContactSnapchat).HasColumnName("contact_snapchat");
            builder.Entity<PrnContactModel>().Property(p => p.ContactSkype).HasColumnName("contact_skype");
            builder.Entity<PrnContactModel>().Property(p => p.ContactBio).HasColumnName("contact_bio");
            builder.Entity<PrnContactModel>().Property(p => p.ContactHasEmail).HasColumnName("contact_has_email");
            builder.Entity<PrnContactModel>().Property(p => p.ContactHasFax).HasColumnName("contact_has_fax");
            builder.Entity<PrnContactModel>().Property(p => p.ContactHasPostal).HasColumnName("contact_has_postal");
            builder.Entity<PrnContactModel>().Property(p => p.ContactCityName).HasColumnName("contact_city_name");
            builder.Entity<PrnContactModel>().Property(p => p.ContactContinentId).HasColumnName("contact_continent_id");
            builder.Entity<PrnContactModel>().Property(p => p.ContactCountryId).HasColumnName("contact_country_id");
            builder.Entity<PrnContactModel>().Property(p => p.ContactStateId).HasColumnName("contact_state_id");
            builder.Entity<PrnContactModel>().Property(p => p.ContactStateName).HasColumnName("contact_state_name");
            builder.Entity<PrnContactModel>().Property(p => p.ContactCityId).HasColumnName("contact_city_id");
            builder.Entity<PrnContactModel>().Property(p => p.ContactPostcode).HasColumnName("contact_postcode");
            builder.Entity<PrnContactModel>().Property(p => p.ContactBugBears).HasColumnName("contact_bug_bears");
            builder.Entity<PrnContactModel>().Property(p => p.ContactAlsoKnownAs).HasColumnName("contact_also_known_as");
            builder.Entity<PrnContactModel>().Property(p => p.ContactPressReleaseInterests).HasColumnName("contact_press_release_interests");
            builder.Entity<PrnContactModel>().Property(p => p.ContactPersonalInterests).HasColumnName("contact_personal_interests");
            builder.Entity<PrnContactModel>().Property(p => p.ContactAppearsIn).HasColumnName("contact_appears_in");
            builder.Entity<PrnContactModel>().Property(p => p.ContactMediaInfluencerScore).HasColumnName("contact_media_influencer_score");
            builder.Entity<PrnContactModel>().Property(p => p.ContactCurrentStatus).HasColumnName("contact_current_status");
            builder.Entity<PrnContactModel>().Property(p => p.ContactBasedInLocation).HasColumnName("contact_based_in_location");
            builder.Entity<PrnContactModel>().Property(p => p.ContactAlternativeEmailAddress).HasColumnName("contact_alternative_email_address");
            builder.Entity<PrnContactModel>().Property(p => p.ContactWebsite).HasColumnName("contact_website");
            builder.Entity<PrnContactModel>().Property(p => p.OutletId).HasColumnName("outlet_id");
            builder.Entity<PrnContactModel>().Property(p => p.ContactIds).HasColumnName("contact_ids");
            builder.Entity<PrnContactModel>().Property(p => p.ContactNames).HasColumnName("contact_names");
            builder.Entity<PrnContactModel>().Property(p => p.OutletName).HasColumnName("outlet_name");
            builder.Entity<PrnContactModel>().Property(p => p.OutletPreferredDeliveryMethod).HasColumnName("outlet_preferred_delivery_method");
            builder.Entity<PrnContactModel>().Property(p => p.OutletSubjectIds).HasColumnName("outlet_subject_ids");
            builder.Entity<PrnContactModel>().Property(p => p.OutletSubjectNames).HasColumnName("outlet_subject_names");
            builder.Entity<PrnContactModel>().Property(p => p.OutletSubjectGroupIds).HasColumnName("outlet_subject_group_ids");
            builder.Entity<PrnContactModel>().Property(p => p.MediaTypeId).HasColumnName("media_type_id");
            builder.Entity<PrnContactModel>().Property(p => p.MediaType).HasColumnName("media_type");
            builder.Entity<PrnContactModel>().Property(p => p.FrequencyId).HasColumnName("frequency_id");
            builder.Entity<PrnContactModel>().Property(p => p.Circulation).HasColumnName("circulation");
            builder.Entity<PrnContactModel>().Property(p => p.LanguageIds).HasColumnName("language_ids");
            builder.Entity<PrnContactModel>().Property(p => p.LanguageNames).HasColumnName("language_names");
            builder.Entity<PrnContactModel>().Property(p => p.StationFrequency).HasColumnName("station_frequency");
            builder.Entity<PrnContactModel>().Property(p => p.NewsFocusIds).HasColumnName("news_focus_ids");
            builder.Entity<PrnContactModel>().Property(p => p.OutletSystemNotes).HasColumnName("outlet_system_notes");
            builder.Entity<PrnContactModel>().Property(p => p.OutletTwitter).HasColumnName("outlet_twitter");
            builder.Entity<PrnContactModel>().Property(p => p.OutletLinkedIn).HasColumnName("outlet_linkedIn");
            builder.Entity<PrnContactModel>().Property(p => p.OutletFacebook).HasColumnName("outlet_facebook");
            builder.Entity<PrnContactModel>().Property(p => p.OutletInstagram).HasColumnName("outlet_instagram");
            builder.Entity<PrnContactModel>().Property(p => p.OutletYouTube).HasColumnName("outlet_youtube");
            builder.Entity<PrnContactModel>().Property(p => p.OutletSnapchat).HasColumnName("outlet_snapchat");
            builder.Entity<PrnContactModel>().Property(p => p.OutletSkype).HasColumnName("outlet_skype");
            builder.Entity<PrnContactModel>().Property(p => p.OutletBio).HasColumnName("outlet_bio");
            builder.Entity<PrnContactModel>().Property(p => p.OutletHasEmail).HasColumnName("outlet_has_email");
            builder.Entity<PrnContactModel>().Property(p => p.OutletHasFax).HasColumnName("outlet_has_fax");
            builder.Entity<PrnContactModel>().Property(p => p.OutletHasPostal).HasColumnName("outlet_has_postal");
            builder.Entity<PrnContactModel>().Property(p => p.OutletIsOffice).HasColumnName("outlet_is_office");
            builder.Entity<PrnContactModel>().Property(p => p.OutletIsFinancial).HasColumnName("outlet_is_financial");
            builder.Entity<PrnContactModel>().Property(p => p.OutletIsPolitician).HasColumnName("outlet_is_politician");
            builder.Entity<PrnContactModel>().Property(p => p.OutletCityName).HasColumnName("outlet_city_name");
            builder.Entity<PrnContactModel>().Property(p => p.OutletContinentId).HasColumnName("outlet_continent_id");
            builder.Entity<PrnContactModel>().Property(p => p.OutletCountryId).HasColumnName("outlet_country_id");
            builder.Entity<PrnContactModel>().Property(p => p.OutletStateId).HasColumnName("outlet_state_id");
            builder.Entity<PrnContactModel>().Property(p => p.OutletStateName).HasColumnName("outlet_state_name");
            builder.Entity<PrnContactModel>().Property(p => p.OutletCityId).HasColumnName("outlet_city_id");
            builder.Entity<PrnContactModel>().Property(p => p.OutletPostcode).HasColumnName("outlet_postcode");
            builder.Entity<PrnContactModel>().Property(p => p.OutletPostalCityId).HasColumnName("outlet_postal_city_id");
            builder.Entity<PrnContactModel>().Property(p => p.OutletPostalCountryId).HasColumnName("outlet_postal_country_id");
            builder.Entity<PrnContactModel>().Property(p => p.OutletPostalContinentId).HasColumnName("outlet_postal_continent_id");
            builder.Entity<PrnContactModel>().Property(p => p.OutletSubscriptionOnlyPublication).HasColumnName("outlet_subscription_only_publication");
            builder.Entity<PrnContactModel>().Property(p => p.OutletAlsoKnownAs).HasColumnName("outlet_also_known_as");
            builder.Entity<PrnContactModel>().Property(p => p.OutletAdditionalWebsite).HasColumnName("outlet_additional_website");
            builder.Entity<PrnContactModel>().Property(p => p.OutletPublishedOn).HasColumnName("outlet_published_on");
            builder.Entity<PrnContactModel>().Property(p => p.OutletPressReleaseInterests).HasColumnName("outlet_press_release_interests");
            builder.Entity<PrnContactModel>().Property(p => p.OutletRegionsCovered).HasColumnName("outlet_regions_covered");
            builder.Entity<PrnContactModel>().Property(p => p.OutletIsGeneric).HasColumnName("outlet_is_generic");
            builder.Entity<PrnContactModel>().Property(p => p.OwnerDebtorNumber).HasColumnName("owner_debtor_number");
            builder.Entity<PrnContactModel>().Property(p => p.IsDangling).HasColumnName("is_dangling");
            builder.Entity<PrnContactModel>().Property(p => p.IsDirty).HasColumnName("is_dirty");
            builder.Entity<PrnContactModel>().Property(p => p.LastModified).HasColumnName("last_modified");

            #endregion

            // Saved Search.
            builder.Entity<SavedSearchModel>().ToTable("SavedSearchMaster");
            builder.Entity<SavedSearchModel>().HasKey(c => new { c.SearchId });

            // Continent.
            builder.Entity<ContinentModel>().ToTable("vw_prn_continent");
            builder.Entity<ContinentModel>().HasKey(c => new {c.Id}).Property(p => p.Id).HasColumnName("MediaAtlasContinentId");
            builder.Entity<ContinentModel>().Property(p => p.Name).HasColumnName("ContinentName");

            // Data Modules.
            builder.Entity<DataModuleModel>().ToTable("mn_data_modules");
            builder.Entity<DataModuleModel>().HasKey(c => new {c.Id});
            builder.Entity<DataModuleModel>().Property(p => p.Name).HasColumnName("DataModuleName");
            builder.Entity<DataModuleModel>().Property(p => p.ContinentId).HasColumnName("MediaAtlasContinentId");
            builder.Entity<DataModuleModel>()
                .HasRequired(c => c.Continent)
                .WithMany()
                .HasForeignKey(k => k.ContinentId);

            // Customers.
            builder.Entity<CustomerModel>().ToTable("mn_customer");
            builder.Entity<CustomerModel>().HasKey(c => new {c.DebtorNumber});
            builder.Entity<CustomerModel>()
                .HasMany(d => d.DataModules)
                .WithMany()
                .Map(m => {
                    m.MapLeftKey("DebtorNumber");
                    m.MapRightKey("DataModuleID");
                    m.ToTable("mn_data_modules_xref");
                });

            // Users.
            builder.Entity<UserModel>().ToTable("mn_user");
            builder.Entity<UserModel>().HasKey(c => new { c.Id });

            // Subjects
            builder.Entity<SubjectModel>().ToTable("mn_subject");
            builder.Entity<SubjectModel>().HasKey(c => new {c.Id}).Property(p => p.Id).HasColumnName("SubjectCodeId");
            builder.Entity<SubjectModel>().Property(p => p.Name).HasColumnName("SubjectName");
            builder.Entity<SubjectModel>().Property(p => p.Hidden).HasColumnName("Hidden");

            // Subject groups.
            builder.Entity<SubjectGroupModel>().ToTable("mn_subjectgroup");
            builder.Entity<SubjectGroupModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("SubjectGroupId");
            builder.Entity<SubjectGroupModel>().Property(p => p.Name).HasColumnName("SubjectGroupName");
            builder.Entity<SubjectGroupModel>()
                .HasMany(s => s.Subjects)
                .WithMany(g => g.Groups)
                .Map(x => {
                    x.MapLeftKey("SubjectGroupId");
                    x.MapRightKey("SubjectCodeId");
                    x.ToTable("mn_subjectgroup_xref");
                });

            // Products
            builder.Entity<ProductModel>().ToTable("mn_product_type");
            builder.Entity<ProductModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("ProductTypeId");
            builder.Entity<ProductModel>().Property(p => p.Name).HasColumnName("ProductTypeName");

            // Roles
            builder.Entity<RoleModel>().ToTable("mn_contact_role");
            builder.Entity<RoleModel>().HasKey(c => new {c.Id}).Property(p => p.Id).HasColumnName("RoleId");
            builder.Entity<RoleModel>().Property(p => p.Name).HasColumnName("RoleName");

            // Index update.
            builder.Entity<IndexUpdateModel>().ToTable("contact_index_update");
            builder.Entity<IndexUpdateModel>().HasKey(c => new {c.Id}).Property(p => p.Id).HasColumnName("id");
            builder.Entity<IndexUpdateModel>().Property(p => p.ContactId).HasColumnName("contact_id");
            builder.Entity<IndexUpdateModel>().Property(p => p.LastModified).HasColumnName("last_modified");

            // Index delete.
            builder.Entity<IndexDeleteModel>().ToTable("contact_index_delete");
            builder.Entity<IndexDeleteModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("id");
            builder.Entity<IndexDeleteModel>().Property(p => p.ContactId).HasColumnName("contact_id");
            builder.Entity<IndexDeleteModel>().Property(p => p.LastModified).HasColumnName("last_modified");

            // Media Contact
            builder.Entity<MediaContactModel>().ToTable("mn_media_contact");
            builder.Entity<MediaContactModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("ContactId");
            builder.Entity<MediaContactModel>().Property(p => p.IsDirty).HasColumnName("IsDirty");


            // Media Outlet
            builder.Entity<MediaOutletModel>().ToTable("mn_media_outlet");
            builder.Entity<MediaOutletModel>().HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("OutletId");
            builder.Entity<MediaOutletModel>().Property(p => p.IsDirty).HasColumnName("IsDirty");
        }

        /// <summary>
        /// Truncate the temporary update table.
        /// </summary>
        public static void TruncateIndexContacts() {
            using (var context = new MedianetContactsContext()) {
                var adapter = context as IObjectContextAdapter;
                var objectContext = adapter.ObjectContext;
                objectContext.ExecuteStoreCommand("truncate table contact_index_update");
                objectContext.ExecuteStoreCommand("truncate table contact_index_delete");
                objectContext.SaveChanges();
            }
        }

        /// <summary>
        /// Updates denormalised both tables.
        /// </summary>
        public static void UpdateDirty() {
            int skip = 0;
            int take = 2000;
            var finished = false;

            while (!finished) {
                using (var context = new MedianetContactsContext()) {
                    var partition = context.IndexUpdateContacts.OrderBy(c => c.Id).Skip(skip).Take(take);

                    (from c in context.AapContacts
                     join u in partition
                         on c.Id equals u.ContactId
                     where c.LastModified == u.LastModified
                     select c).ForEach(x => {
                                           x.IsDirty = false;
                                           x.IsDangling = false;
                                       });

                    (from c in context.PrnContacts
                     join u in partition
                         on c.Id equals u.ContactId
                     where c.LastModified == u.LastModified
                     select c).ForEach(x => {
                                           x.IsDirty = false;
                                           x.IsDangling = false;
                                       });

                    context.SaveChanges();
                    finished = !partition.Any();
                    skip += take;
                }
            }
        }

        /// <summary>
        /// Delete dangling records from denormalized tables.
        /// </summary>
        public static void DeleteDangling() {
            int skip = 0;
            int take = 2000;
            var finished = false;

            while (!finished) {
                using (var context = new MedianetContactsContext()) {
                    var partition = context.IndexDeleteContacts.OrderBy(c => c.Id).Skip(skip).Take(take);
                    var dirtyAap = (from c in context.AapContacts
                                    join u in partition
                                        on c.Id equals u.ContactId
                                    where c.LastModified == u.LastModified
                                    select c).ToList();
                    dirtyAap.ForEach(c => context.AapContacts.Remove(c));

                    var dirtyPrn = (from c in context.PrnContacts
                                    join u in partition
                                        on c.Id equals u.ContactId
                                    where c.LastModified == u.LastModified
                                    select c).ToList();
                    dirtyPrn.ForEach(c => context.PrnContacts.Remove(c));

                    context.SaveChanges();
                    finished = !partition.Any();
                    skip += take;
                }
            }
        }

        /// <summary>
        /// Updates contacts in the database.
        /// </summary>
        /// <param name="contact">The contact to update.</param>
        public static void UpdateContacts(ContactModel contact) {
            if (contact == null) return;

            using (var context = new MedianetContactsContext()) {
                if (contact.IsAap || contact.IsPrivate) {
                    var update =
                        context.AapContacts.SingleOrDefault(
                            x => x.Id == contact.Id && x.LastModified == contact.LastModified);
                    if (update != null) {
                        update.IsDirty = false;
                        update.IsDangling = false;
                    }
                }
                else {
                    var update =
                        context.PrnContacts.SingleOrDefault(
                            x => x.Id == contact.Id && x.LastModified == contact.LastModified);
                    if (update != null) {
                        update.IsDirty = false;
                        update.IsDangling = false;
                    }
                }
                context.SaveChanges();
            }
        }


        /// <summary>
        /// Delete the contacts from the database.
        /// </summary>
        /// <param name="contact">The contact to delete.</param>
        public static void DeleteContacts(ContactModel contact) {
            if (contact == null) return;

            using (var context = new MedianetContactsContext()) {
                if (contact.IsAap || contact.IsPrivate) {
                    var update =
                        context.AapContacts.SingleOrDefault(
                            x => x.Id == contact.Id && x.LastModified == contact.LastModified);
                    if (update != null)
                        context.AapContacts.Remove(update);
                }
                else {
                    var update =
                        context.PrnContacts.SingleOrDefault(
                            x => x.Id == contact.Id && x.LastModified == contact.LastModified);
                    if (update != null)
                        context.PrnContacts.Remove(update);
                }
                context.SaveChanges();
            }
        }
    }
}
