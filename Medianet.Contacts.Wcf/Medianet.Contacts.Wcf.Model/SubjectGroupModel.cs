﻿using System.Collections.Generic;

namespace Medianet.Contacts.Wcf.Model
{
    public class SubjectGroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<SubjectModel> Subjects { get; set; }
    }
}
