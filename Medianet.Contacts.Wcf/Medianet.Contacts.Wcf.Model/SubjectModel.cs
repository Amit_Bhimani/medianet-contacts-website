﻿using System.Collections.Generic;

namespace Medianet.Contacts.Wcf.Model
{
    public class SubjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Hidden { get; set; }
        public ICollection<SubjectGroupModel> Groups { get; set; }
    }
}
