﻿using System;

namespace Medianet.Contacts.Wcf.Model
{
    /// <summary>
    /// Entity representing table mn_user.
    /// </summary>
    public class UserModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string LogonName { get; set; }
        public string Password { get; set; }
        public string DebtorNumber { get; set; }
        public string OutletId { get; set; }
        public string ShortName { get; set; }
        public char Status { get; set; }
        public string IVRPin { get; set; }
        public string DefaultBillingRef { get; set; }
        public char DefaultJobPriority { get; set; }
        public string EmailAddress { get; set; }
        public string Telephone { get; set; }
        public string FaxNumber { get; set; }
        public char ReportMethod { get; set; }
        public char ResultType { get; set; }
        public bool DeduplicateJobs { get; set; }
        public char MNUserAccessRights { get; set; }
        public bool MediaNetWebAccess { get; set; }
        public bool MessageConnectWebAccess { get; set; }
        public bool MNJWebAccess { get; set; }
        public bool AdminSiteWebAccess { get; set; }
        public bool FaxAccess { get; set; }
        public bool EmailAccess { get; set; }
        public bool SMSAccess { get; set; }
        public bool VoiceAccess { get; set; }
        public bool MailMergeAccess { get; set; }
        public bool AdminAccess { get; set; }
        public bool OMAAccess { get; set; }
        public char OMAAccessRights { get; set; }
        public bool OMAReportAccess { get; set; }
        public bool ReleaseWatchAccess { get; set; }
        public DateTime? LastLogonTime { get; set; }
        public Int16 LogonFailureCount { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ModifiedUserId { get; set; }

        public CustomerModel Customer { get; set; }
    }
}
