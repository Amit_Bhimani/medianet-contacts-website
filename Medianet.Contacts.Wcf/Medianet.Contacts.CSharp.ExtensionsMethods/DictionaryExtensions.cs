﻿using System;
using System.Collections.Generic;

namespace Medianet.Contacts.CSharp.ExtensionMethods
{
    /// <summary>
    /// Represents a collection of useful extenions methods.
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Translate a dictionary into a string for display.
        /// </summary>
        public static string PrettyPrint<K, V>(this IDictionary<K, V> dict) {
            if (dict == null)
                return "";
            string dictStr = "[";
            ICollection<K> keys = dict.Keys;
            int i = 0;
            foreach (K key in keys) {
                dictStr += key.ToString() + "=" + dict[key].ToString();
                if (i++ < keys.Count - 1) {
                    dictStr += ", ";
                }
            }
            return dictStr + "]";
        }

        /// <summary>
        /// Merges two dictionaries.
        /// </summary>
        /// <remarks>
        /// Values in original dictionary will be replaced with values from additional dictionary with same keys.
        /// </remarks>
        public static void Merge<TKey, TValue>(this IDictionary<TKey, TValue> original, IDictionary<TKey, TValue> additional) {
            if (original == null) throw new ArgumentNullException("original");
            if (additional == null) throw new ArgumentNullException("additional");

            foreach (var pair in additional)
                original[pair.Key] = pair.Value;
        }
    }
}
