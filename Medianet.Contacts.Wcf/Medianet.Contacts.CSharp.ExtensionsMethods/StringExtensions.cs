﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace Medianet.Contacts.CSharp.ExtensionMethods
{
    public static class StringExtensions
    {
        /// <summary>
        /// Reverse the string.
        /// </summary>
        /// <param name="s">Input string</param>
        public static string Reverse(this string input) {
            char[] chars = input.ToCharArray();
            Array.Reverse(chars);
            return new String(chars);
        }

        /// <summary>
        /// Remove white space, but not new lines.
        /// Useful when parsing user input such as phone numbers, prices.
        /// eg int.Parse("1 000 000".RemoveSpaces() ...
        /// </summary>
        /// <param name="s">Input string</param>
        public static string RemoveSpaces(this string s) {
            return s.Replace(" ", "");
        }

        /// <summary>
        /// true, if the string can be parse as Double respective Int32
        /// Spaces are not considred.
        /// </summary>
        /// <param name="s">Input string</param>
        /// <param name="floatpoint">true, if Double is considered, otherwhise Int32 is considered.</param>
        /// <returns>true, if the string contains only digits or float-point</returns>
        public static bool IsNumber(this string s, bool floatpoint) {
            int i;
            double d;
            string withoutWhiteSpace = s.RemoveSpaces();
            if (floatpoint)
                return double.TryParse(withoutWhiteSpace, NumberStyles.Any,
                    Thread.CurrentThread.CurrentUICulture, out d);
            else
                return int.TryParse(withoutWhiteSpace, out i);
        }

        /// <summary>
        /// Remove accents from strings.
        /// </summary>
        /// <example>
        ///  input:  "Příliš žluťoučký kůň úpěl ďábelské ódy."
        ///  result: "Prilis zlutoucky kun upel dabelske ody."
        /// </example>
        /// <param name="s">Input string.</param>
        /// <remarks>
        /// Found at http://stackoverflow.com/questions/249087/how-do-i-remove-diacritics-accents-from-a-string-in-net
        /// </remarks>
        /// <returns>string without accents.</returns>
        public static string RemoveDiacritics(this string s) {
            string stFormD = s.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            for (int ich = 0; ich < stFormD.Length; ich++) {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != UnicodeCategory.NonSpacingMark) {
                    sb.Append(stFormD[ich]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        /// <summary>
        /// Determines whether the input string is null or empty.
        /// </summary>
        /// <param name="s">The input string.</param>
        /// <returns>
        ///   <c>true</c> if the string is null or empty, otherwise <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty(this string s) {
            return string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Determines whether the input string is null or just whitespace.
        /// </summary>
        /// <param name="s">The input string.</param>
        /// <returns>
        ///   <c>true</c> if the string is null or whitespace, otherwise <c>false</c>.
        /// </returns>
        public static bool IsNullOrWhitespace(this string s) {
            return string.IsNullOrWhiteSpace(s);
        }

        /// <summary>
        /// Converts a CSV to a generic List using the given converter.
        /// </summary>
        /// <typeparam name="T">The type of the elements to parse.</typeparam>
        /// <param name="input">The input string.</param>
        /// <param name="converter">The converter lambda.</param>
        /// <param name="predicate">A predicate to filter out elements that cannot be parsed.</param>
        /// <returns>
        /// A generic List containing the parsed elements.
        /// </returns>
        public static List<T> ParseCSV<T>(this string input, Func<string, T> converter, Func<string, bool> predicate = null) {
            if (string.IsNullOrWhiteSpace(input))
                return new List<T>();

            var split = input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (predicate != null)
                return split.Where(x => predicate(x))
                     .ToList().Select(x => converter(x)).ToList<T>();
            else
                return split.ToList().Select(x => converter(x)).ToList<T>();
        }

        /// <summary>
        /// Converts a CSV that has prefixes on each element to a generic List using the 
        /// given converter.
        /// </summary>
        /// <typeparam name="T">The type of the elements to parse.</typeparam>
        /// <param name="input">The input string.</param>
        /// <param name="prefix">The prefix on each element.</param>
        /// <param name="converter">The converter lambda.</param>
        /// <returns>A generic List containing the parsed elements.</returns>
        public static List<T> ParsePrefixedCSV<T>(this string input, string prefix, Func<string, T> converter) {
            if (string.IsNullOrEmpty(input))
                return new List<T>();

            if (string.IsNullOrWhiteSpace(prefix))
                return input.ParseCSV<T>(converter);
            
            string[] idList = input.Split(',');

            return input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .ToList()
                .Where(s => !s.IsNullOrWhitespace() && s.StartsWith(prefix))
                .Select(s => converter(s.Substring(prefix.Length)))
                .ToList<T>();
        }

        /// <summary>
        /// Parses a text representation of a boolean.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <param name="_default">The default value to use if input is null or whitespace.</param>
        /// <returns>
        /// The parsed bool.
        /// </returns>
        public static bool ParseBool(this string input, bool _default) {
            if (string.IsNullOrWhiteSpace(input))
                return _default;
            else
                return (input.ToLower() == "true");
        }

        /// <summary>
        /// Parses a string. If the string is null returns the empty string.
        /// </summary>
        /// <param name="pValue">The input string.</param>
        /// <returns></returns>
        public static string Clean(this string input) {
            if (input == null)
                return string.Empty;
            else
                return input;
        }

        /// <summary>
        /// Trim's a string if it isn't null or empty.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <returns>The trimmed string.</returns>
        public static string TrimIfNotNull(this string input) {
            if (input.IsNullOrWhitespace())
                return input;
            return input.Trim();
        }
    }
}
