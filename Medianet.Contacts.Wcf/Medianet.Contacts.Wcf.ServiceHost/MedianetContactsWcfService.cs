﻿using System;
using System.ServiceProcess;
using System.Threading;
using Medianet.Contacts.Wcf.Service.Common;
using Medianet.Contacts.Wcf.Service.Search;
using Medianet.Contacts.Wcf.Service.Solr;
using Ninject.Extensions.Wcf;
using NLog;

namespace Medianet.Contacts.Wcf.ServiceHost
{
    public partial class MedianetContactsWcfService : ServiceBase
    {
        private readonly System.ServiceModel.ServiceHost _searchServiceHost;
        private readonly System.ServiceModel.ServiceHost _solrServiceHost;
        private readonly SolrIndexManager _indexManager;
        private readonly Logger _logger;
        private readonly object _padlock;
        private Timer _timer;
        private bool _solrUpdate;

        public MedianetContactsWcfService(NinjectServiceHost<SearchService> searchServiceHost,
                                  NinjectServiceHost<SolrService> solrServiceHost, SolrIndexManager indexManager) {
            InitializeComponent();
            _searchServiceHost = searchServiceHost;
            _solrServiceHost = solrServiceHost;
            _indexManager = indexManager;
            _logger = LogManager.GetCurrentClassLogger();
            _solrUpdate = Config.SolrUpdate();
            _padlock = new object();
        }
        
        protected override void OnStart(string[] args) {
            try {
                // Start the WCF host.
                _logger.Info("Starting service host.");
                _searchServiceHost.Open();
                _solrServiceHost.Open();

                // Start the index manager.
                if (_solrUpdate) _indexManager.Start();
                _timer = new Timer(CheckSolrIndexManager, null, TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(10));

                _logger.Info("Service host started.");
            }
            catch (Exception ex) {
                _logger.ErrorException("Failed to start the service.", ex);
                Stop();
            }
        }

        protected override void OnStop() {
            try {
                if (_timer != null) _timer.Dispose();
                if (Config.SolrUpdate() && _indexManager != null) _indexManager.Stop();

                _logger.Info("Stopping service host.");
                 _searchServiceHost.Close();
                _logger.Info("Service host stopped.");
            }
            catch (Exception ex) {
                _logger.ErrorException("Failed to stop the service normally.", ex);
            }
        }

        /// <summary>
        /// Periodically checks to see if the index manager should be running or not.
        /// </summary>
        private void CheckSolrIndexManager(object state) {
            if (Monitor.TryEnter(_padlock)) {
                try {
                    var solrUpdateTmp = Config.SolrUpdate();
                    if (_solrUpdate != solrUpdateTmp) {
                        _logger.Info("SolrUpdate setting has changed...");
                        _solrUpdate = solrUpdateTmp;

                        if (_solrUpdate) {
                            if (_indexManager != null && !_indexManager.IsRunning())
                                _indexManager.Start();
                        }
                        else {
                            if (_indexManager != null && _indexManager.IsRunning())
                                _indexManager.Stop();
                        }
                    }
                }
                catch (Exception e) {
                    _logger.ErrorException("Exception raised in settings timer delegate.", e);
                }
                finally {
                    Monitor.Exit(_padlock);
                }
            }
        }
    }
}
