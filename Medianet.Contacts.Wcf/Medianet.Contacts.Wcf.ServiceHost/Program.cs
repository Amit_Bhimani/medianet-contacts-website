﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.ServiceProcess;
using Medianet.Contacts.Wcf.Service.Common;
using Ninject;
using NLog;

namespace Medianet.Contacts.Wcf.ServiceHost
{
    static class Program
    {
        private static Logger _logger;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args) {
            _logger = LogManager.GetCurrentClassLogger();
            _logger.Debug("Started main method.");

            // At least log unhandled exceptions.
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += UnhandledExceptionHandler;

            AutoMapperConfigurator.Configure();
            var kernel = new StandardKernel(new MediaContactsServiceModule());

            try {
                if (args.Length == 1) {
                    var installer = kernel.Get<MedianetContactsWcfServiceInstaller>();

                    switch (args[0].ToLower()) {
                        case "-install":
                            installer.InstallService();
                            break;
                        case "-uninstall":
                            installer.StopService();
                            installer.UninstallService();
                            break;
                    }
                }
                else {
                    // Start the service.
                    ServiceBase.Run(kernel.Get<MedianetContactsWcfService>());
                }
            }
            catch (Exception ex) {
                _logger.ErrorException("Exception was raised.", ex);
            }

            _logger.Debug("Finished main method.");
        }

        /// <summary>
        /// Method to handle unhandled exceptions.
        /// </summary>
        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args) {
            if (args != null && args.ExceptionObject != null) {
                try
                {
                    var e = args.ExceptionObject as Exception;
                    _logger.ErrorException("An unhandled exception was raised at the top level.", e);

                    // Send an email.
                    var subject = "MedianetContactsServiceHost Crash Report";
                    var content = $"An unhandled exception has occurred on the MediaContactsServiceHost service \r\n" +
                                  $" Service was running on machine :  {Environment.MachineName} \r \n" +
                                  $" Error details {e} ";

                    EmailHelper.SendEmail(ConfigurationManager.AppSettings["MailToAddresses"], subject, content, false);

                }
                catch (Exception ex)
                {
                    _logger.ErrorException("Failed to send email.", ex);
                }
            }
        }
    }
}
