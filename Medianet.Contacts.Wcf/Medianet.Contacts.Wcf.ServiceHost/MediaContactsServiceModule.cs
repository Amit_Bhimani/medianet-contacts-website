﻿using System.Configuration;
using Medianet.Contacts.Wcf.Service.Solr;
using Ninject.Modules;

namespace Medianet.Contacts.Wcf.ServiceHost
{
    public class MediaContactsServiceModule : NinjectModule
    {
        private readonly string _serviceName;
        private readonly string _serviceDescription;
        private readonly int _maxThreads = 5;
        private readonly int _maxRows = 2000;

        public MediaContactsServiceModule() {
            // Read config file.
            _serviceName = ConfigurationManager.AppSettings["ServiceName"] ?? "MedianetContacts Service Host";
            _serviceDescription = ConfigurationManager.AppSettings["ServiceDescription"] ?? "MedianetContacts Service Host";

            int maxThreads;
            if (int.TryParse(ConfigurationManager.AppSettings["MaxThreads"], out maxThreads))
                _maxThreads = maxThreads;

            int maxRows;
            if (int.TryParse(ConfigurationManager.AppSettings["MaxRows"], out maxRows))
                _maxRows = maxRows;
        }

        public override void Load() {
            Bind<MedianetContactsWcfService>().ToSelf();
            Bind<MedianetContactsWcfServiceInstaller>()
                .ToSelf()
                .WithConstructorArgument("serviceName", _serviceName)
                .WithConstructorArgument("serviceDescription", _serviceDescription);

            Bind<SolrIndexManager>()
                .ToSelf()
                .WithConstructorArgument("workers", _maxThreads)
                .WithConstructorArgument("take", _maxRows);
        }
    }
}