﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Medianet.Contacts.Wcf.ServiceHost
{
    [RunInstaller(true)]
    public partial class MedianetContactsWcfServiceInstaller : System.Configuration.Install.Installer
    {
        private readonly ServiceInstaller _serviceInstaller;
        private readonly ServiceProcessInstaller _processInstaller;

        private readonly string _serviceName;
        private readonly string _serviceDescription;

        public MedianetContactsWcfServiceInstaller(string serviceName, string serviceDescription) {
            _serviceDescription = serviceDescription;
            _serviceName = serviceName;
            InitializeComponent();

            try {
                _processInstaller = new ServiceProcessInstaller();
                _serviceInstaller = new ServiceInstaller();

                // Service will run under system account
                _processInstaller.Account = ServiceAccount.LocalSystem;

                // Service will have Start Type of Manual
                _serviceInstaller.StartType = ServiceStartMode.Manual;

                _serviceInstaller.ServiceName = serviceName;
                _serviceInstaller.Description = serviceDescription;

                Installers.Add(_serviceInstaller);
                Installers.Add(_processInstaller);
            }
            catch { }
        }

        public bool IsInstalled() {
            using (var controller = new ServiceController(_serviceName)) {
                try {
                    var status = controller.Status;
                }
                catch {
                    return false;
                }
            }
            return true;
        }

        public bool IsRunning() {
            using (var controller = new ServiceController(_serviceName)) {
                if (!IsInstalled())
                    return false;
                return controller.Status == ServiceControllerStatus.Running;
            }
        }

        public MedianetContactsWcfServiceInstaller GetInstaller() {
            var installer = new MedianetContactsWcfServiceInstaller(_serviceName, _serviceDescription);
            return installer;
        }

        public void UninstallService() {
            if (!IsInstalled())
                return;

            try {
                var ti = new TransactedInstaller();
                MedianetContactsWcfServiceInstaller installer = GetInstaller();
                ti.Installers.Add(installer);

                var ctx = new InstallContext();
                ctx.Parameters.Add("assemblypath", System.Reflection.Assembly.GetExecutingAssembly().Location);
                ti.Context = ctx;

                var ht = new Hashtable();
                ti.Uninstall(ht);
            }
            catch { }
        }

        public void StopService() {
            if (IsInstalled())
                return;

            using (var controller = new ServiceController(_serviceName)) {
                try {
                    if (controller.Status != ServiceControllerStatus.Stopped) {
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(10));
                    }
                }
                catch { }
            }
        }

        public void InstallService() {
            if (IsInstalled())
                return;

            try {
                var ti = new TransactedInstaller();
                var installer = GetInstaller();
                ti.Installers.Add(installer);

                var ctx = new InstallContext();
                ctx.Parameters.Add("assemblypath", System.Reflection.Assembly.GetExecutingAssembly().Location);
                ti.Context = ctx;

                var ht = new Hashtable();
                ti.Install(ht);
            }
            catch { }
        }
    }
}