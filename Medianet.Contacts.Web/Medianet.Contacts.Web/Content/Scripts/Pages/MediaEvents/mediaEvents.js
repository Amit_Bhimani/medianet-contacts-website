﻿MediaPeople.MediaEvents = (function () {
    return {
        InitDailyDiary: function () {
            $("#lnkDailyDiary").click(function () {
                $("#lnkDailyDiary").addClass("selected_outlet1");
                $("#lnkMediaEventsAgenda").removeClass("selected_record1");
                $("#contentDailyDiary").show();
                $("#contentMediaEventsAgenda").hide();
            });
        },
        InitMediaEventsAgenda: function () {
            $("#lnkMediaEventsAgenda").click(function () {
                $("#lnkMediaEventsAgenda").addClass("selected_record1");
                $("#lnkDailyDiary").removeClass("selected_outlet1");
                $("#contentMediaEventsAgenda").show();
                $("#contentDailyDiary").hide();
            });
        },
        Init: function () {
            MediaPeople.MediaEvents.InitDailyDiary();
            MediaPeople.MediaEvents.InitMediaEventsAgenda();
        }
    }
})();

$(document).ready(function () {
    MediaPeople.MediaEvents.Init();
});
