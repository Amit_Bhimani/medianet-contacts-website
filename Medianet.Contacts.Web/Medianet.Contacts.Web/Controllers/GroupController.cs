﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.Services;
using System.Net;
using Medianet.Contacts.Web.ViewModels.AllLists;
using Medianet.Contacts.Web.Services.Search;
using System;

namespace Medianet.Contacts.Web.Controllers
{
    public class GroupController : BaseController
    {
        private IGroupService _groupService;
        private IGroupListService _groupListService;
        private ISavedSearchService _savedSearchService;
        public GroupController() {
            _groupService = GroupService.CreateGroupService();
            _savedSearchService = SavedSearchService.CreateSavedSearchService();
            _groupListService = GroupListService.CreateGroupListService();
        }

        protected override void Dispose(bool disposing) {
            if (_groupService != null) _groupService.Dispose();
            if (_groupListService != null) _groupListService.Dispose();
            if (_savedSearchService != null) _savedSearchService.Dispose();
            base.Dispose(disposing);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult Rename(int id, string name)
        {
            return Json(new { result = _groupService.RenameGroup(id, name) });
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Tasks()
        {
            return PartialView("_GroupListPartial", GetGroups(GroupType.Task));
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Searches()
        {
            return PartialView("_GroupListPartial", GetGroups(GroupType.Search));
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Lists()
        {
            return PartialView("_GroupListPartial", GetGroups(GroupType.List));
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult RenameGroupModal(int groupId)
        {
            var results = _groupService.GetGroupById(groupId, CurrentUser.UserID);

            return PartialView("_RenameGroup",
                new RenameGroupViewModel() { GroupId = results.Id, GroupName = results.Name });
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult RenameGroup(RenameGroupViewModel model)
        {
            _groupService.RenameGroup(model.GroupId, model.NewGroupName);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult DeleteGroupList(int groupId)
        {
            _groupListService.DeleteGroupList(groupId, CurrentUser.UserID);

             return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult DeleteGroupSearch(int groupId)
        {
            _savedSearchService.DeleteGroupSearch(groupId, CurrentUser.UserID);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        
        private List<GroupViewModel> GetGroups(GroupType type) {
            var groups = _groupService.GetGroupsByType(type, CurrentUser.UserID, CurrentUser.SessionKey);

            if (groups != null && groups.Count == 0)
                HttpContext.Response.StatusCode = 404;

            return groups;
        }
    }
}
