﻿using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Services.Dashboard;
using Medianet.Contacts.Web.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Medianet.Contacts.Web.GeneralService;
using Medianet.Contacts.Web.ViewModels;

namespace Medianet.Contacts.Web.Controllers
{
    public class DashboardController : BaseController
    {
        #region Field

        private readonly IDashboardService _dashboardService;

        #endregion

        public DashboardController() : base()
        {
            _dashboardService = DashboardService.CreateDashboardService();
        }

        #region MediaMovements

        [HandleAjaxError, HandleAjaxAuthorization]
        [OutputCache(Location = OutputCacheLocation.Server, VaryByParam = "none", Duration = 300)] // Cache for 5 minutes
        public PartialViewResult GetMediaMovements()
        {
            var mediaMovementsList = _dashboardService.GetMediaMovements(false, 1, 3, SessionManager.Instance.CurrentUser);

            return PartialView("~/Views/Dashboard/_MediaMovementsPartial.cshtml", mediaMovementsList);
        }

        #endregion

        #region Recently added

        [HandleAjaxError, HandleAjaxAuthorization]
        [OutputCache(Location = OutputCacheLocation.Server, VaryByParam = "none", Duration = 300)] // Cache for 5 minutes
        public PartialViewResult GetRecentlyAdded()
        {
            var recentList = _dashboardService.GetRecentlyAdded();

            return PartialView("_RecentlyAddedPartial", recentList);
        }

        #endregion

        #region Recently Updated
        [HandleAjaxError, HandleAjaxAuthorization]
        [OutputCache(Location = OutputCacheLocation.Server, VaryByParam = "none", Duration = 300)] // Cache for 5 minutes
        public PartialViewResult GetRecentlyUpdated()
        {
            var recentList = _dashboardService.GetRecentlyUpdated(3, SessionManager.Instance.CurrentUser);

            return PartialView("_RecentlyUpdatedPartial", recentList);
        }
        #endregion

        #region Recent Lists

        [HandleAjaxError, HandleAjaxAuthorization]
        public PartialViewResult GetRecentlyUpdatedLists()
        {
            var recentLists = _dashboardService.GetRecentlyUpdatedLists(SessionManager.Instance.CurrentUser);

            return PartialView("_RecentlyUpdatedListsPartial", recentLists);
        }

        #endregion

        #region Media Statistics

        [HandleAjaxError]
        [OutputCache(Location = OutputCacheLocation.Server, VaryByParam = "none", Duration = 1800)] // Cache for 30 minutes
        public PartialViewResult GetMediaStatistics()
        {
            MediaStatistics ms = null;

            try
            {
                ms = _dashboardService.GetMediaStatistics();
            }
            catch (Exception e)
            {
                BasePage.LogException(e, this.GetType().FullName, "GetMediaStatistics", null);
            }

            return PartialView("_MediaStatisticsPartial", ms);
        }

        #endregion

        #region Request Research

        [HttpPost]
        [HandleAjaxError, HandleAuthentication]
        public ActionResult RequestResearch(string details)
        {
            if (string.IsNullOrWhiteSpace(details))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Email.SendAdminResearchRequestEmail(SessionManager.Instance.CurrentUser, details);
                Email.SendClientResearchRequestEmail(SessionManager.Instance.CurrentUser);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(DashboardController).FullName, "Submit[POST]", null);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "An error has occured. Unable to send research request.");
            }
        }

        #endregion
    }
}
