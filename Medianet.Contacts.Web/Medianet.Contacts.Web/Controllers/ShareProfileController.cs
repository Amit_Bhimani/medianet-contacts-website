﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Helpers;
using Medianet.Contacts.Web.Services.Contact;
using Medianet.Contacts.Web.Services.Outlet;
using Medianet.Contacts.Web.Services.ShareProfile;
using Medianet.Contacts.Web.Services.Social;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.Outlet;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Controllers
{
    public class ShareProfileController : BaseController
    {
        private readonly IShareProfileService _shareProfileService;

        public ShareProfileController() : base()
        {
            _shareProfileService = ShareProfileService.CreateShareProfileService();
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Index(string outletId, string contactId, RecordType recordType)
        {
            ShareProfileViewModel model = new ShareProfileViewModel
            {
                ContactId = contactId,
                OutletId = outletId,
                RecordType = recordType,
                IsBodyHtml = true
            };

            ViewBag.Users = _shareProfileService.GetUsers(CurrentUser);

            return PartialView("OutletAndContact/_ShareProfilePartial", model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Index(ShareProfileViewModel model)
        {
            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(400, ModelErrorMessages);

            bool isOutlet = string.IsNullOrEmpty(model.ContactId);
            var data = new ContactOutletBaseViewModel();
            string name = string.Empty;

            if (isOutlet)
            {
                data = OutletService.CreateOutletService().GetOutlet(model.OutletId, model.RecordType, CurrentUser);
                name = ((OutletViewModel)data).Name;
            }
            else
            {
                var contactModel = ContactService.CreateContactService().GetContact(model.ContactId, model.OutletId, model.RecordType, CurrentUser, false);
                data = contactModel;
                name = contactModel.Name.FirstName + " " + contactModel.Name.LastName;
            }

            string html = GetHtml(data, isOutlet);

            Email.SendShareProfileEmail(html, model.EmailAddress, name, model.IsBodyHtml, isOutlet);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private string GetHtml(ContactOutletBaseViewModel data, bool isOutlet)
        {
            string HTMLContent = "";
            ViewData.Model = data;

            if (data.SocialDetails != null)
            {
                if (data.SocialDetails.Twitter.HasValue())
                {
                    var twitterService = TwitterService.CreateTwitterService();

                    try
                    {
                        ViewBag.TwitterInfo = Task.Run(() => twitterService.GetUserTwitterInfo(data.SocialDetails.Twitter)).Result;
                    }
                    catch (Exception ex) { }

                    try
                    {
                        ViewBag.Tweets = Task.Run(() => twitterService.GetUserTweets(data.SocialDetails.Twitter)).Result;
                    }
                    catch (Exception ex) { }
                }

                if (!string.IsNullOrEmpty(UriHelper.AbsolutePath(data.SocialDetails.Youtube)))
                {
                    try
                    {
                        ViewBag.YoutubeInfo = Task.Run(() => YouTubeService.CreateYouTubeService().GetData(data.SocialDetails.Youtube)).Result;
                    }
                    catch (Exception ex) { }
                }
            }
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext,
                    $"~/Views/{(isOutlet ? "Outlet" : "Contact")}/ShareProfile/Index.cshtml",
                    "~/Views/Shared/_LayoutShareProfile.cshtml");

                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                HTMLContent += sw.GetStringBuilder().ToString();
            }

            return HTMLContent.Replace(@"\", "/");
        }
    }
}
