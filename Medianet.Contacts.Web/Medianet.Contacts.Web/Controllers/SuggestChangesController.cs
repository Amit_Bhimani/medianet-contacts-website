﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Services.Contact;
using Medianet.Contacts.Web.Services.Outlet;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.Outlet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Controllers
{
    public class SuggestChangesController : BaseController
    {
        #region Field

        private readonly IOutletService _outletService;
        private readonly IContactService _contactService;


        #endregion

        public SuggestChangesController() : base()
        {
            _outletService = OutletService.CreateOutletService();
            _contactService = ContactService.CreateContactService();
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Index(string outletId, string contactId, string recordType, string message)
        {
            SuggestedChangesViewModel model = new SuggestedChangesViewModel
            {
                OutletId = outletId,
                RecordType = (RecordType)Enum.Parse(typeof(RecordType), recordType),
                Message = message
            };

            if (!string.IsNullOrEmpty(contactId))
                model.ContactId = contactId;

            return PartialView("OutletAndContact/_SuggestionPartial", model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Index(SuggestedChangesViewModel model)
        {
            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(400, ModelErrorMessages);

            string contactName = string.Empty,
                    outletName = string.Empty;

            if (!string.IsNullOrEmpty(model.ContactId))
            {
                DataTable dtOutlet = _contactService.GetAllOutletContactHomePageDal(model.OutletId, model.ContactId);

                if (dtOutlet == null || dtOutlet.Rows.Count == 0)
                    return new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed);

                contactName = Convert.ToString(dtOutlet.Rows[0]["Name"]);
                outletName = Convert.ToString(dtOutlet.Rows[0]["OutletName"]);
            }
            else
            {
                OutletViewModel ot = _outletService.GetOutlet(model.OutletId, model.RecordType, CurrentUser);

                if (ot == null)
                    return new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed);

                outletName = ot.Name;
            }
            
            var oh = new OutletDetailsHistory
            {
                FirstName = CurrentUser.FullName,
                ContactName = contactName,
                OutletName = outletName,
                Email = CurrentUser.EmailAddress,
                UpdateRequest = model.Message.Trim(),
                LogonName = CurrentUser.LogonName,
                CompanyName = CurrentUser.CompanyName,
            };

            Email.SendAdminEmail(oh);
            Email.SendCustomerEmail(oh);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

    }
}
