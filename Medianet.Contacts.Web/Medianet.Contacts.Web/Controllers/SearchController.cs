﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.Services.List;
using Medianet.Contacts.Web.Services.Search;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.Grid;
using Medianet.Contacts.Web.ViewModels.List;
using Medianet.Contacts.Web.ViewModels.Search;
using Ninject;
using Location = Medianet.Contacts.Wcf.DataContracts.DTO.Location;
using LocationModel = Medianet.Contacts.Web.Common.Model.Location;
using Medianet.Contacts.Web.Services;
using Medianet.Contacts.Web.ViewModels.AllSavedSearches;
using System.Text.RegularExpressions;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Services.Outlet;
using Medianet.Contacts.Web.Helpers;

namespace Medianet.Contacts.Web.Controllers
{
    public class SearchController : BaseController
    {
        #region Field

        public static readonly string Search_ErrorMessage = "An error has occured. Unable to perform search.";
        public static readonly string Search_InvalidSavedSearch = "The saved search either does not exist or you are not authorised to view it.";

        public static readonly int MaxAutosuggest = 100;

        private readonly IOutletService _outletService;

        // Services.
        private ISavedSearchService _savedSearchService;
        private IGroupService _groupService;
        private IListService _listService;
        private Medianet.Contacts.Web.Services.Search.SearchService _searchService;

        #endregion

        [Inject]
        public SearchController()
            : base()
        {
            _savedSearchService = SavedSearchService.CreateSavedSearchService();
            _groupService = GroupService.CreateGroupService();
            _listService = ListService.CreateListService();
            _searchService = Services.Search.SearchService.CreateSearchService();
            _outletService = OutletService.CreateOutletService();
        }

        protected override void Dispose(bool disposing)
        {
            if (_savedSearchService != null) _savedSearchService.Dispose();
            if (_groupService != null) _groupService.Dispose();
            base.Dispose(disposing);
        }

        #region Actions

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError, RequiresSession, SavesSession]
        public ActionResult Rebind(PaginationSearchViewModel model, bool? isSearchPage)
        {
            isSearchPage = (isSearchPage ?? true) && !new List<string> { "/privatecontact", "/privateoutlet" }.Contains(Request.UrlReferrer.AbsolutePath.ToString().ToLower());
            ViewBag.IsSearchPage = isSearchPage;
            model.UseCookieForPageSize = isSearchPage.Value;

            List<SearchResult> results = GetCurrentSearchResults(model.Expand, model.SortColumn, model.SortDirection, !isSearchPage.Value);

            // Calculate pagination.
            model.CalculatePagination();

            // Set the unselected items.
            SetUnselectedItems(model, results);

            // Get the current page.
            model.Results = GetCurrentPagedSearch(model, results);

            var current = results.Except(DeletedItems);
            if (UnselectedItems.Count() > 0)
            {
                if (model.CheckboxState == CheckboxState.PartNone)
                {
                    var SelectedItems = current.Except(UnselectedItems).ToList();

                    model.CurrentPageOnly = model.Results
                        .Except(DeletedItems)
                        .Intersect(SelectedItems).Count() == SelectedItems.Count;
                }
                else if (model.CheckboxState == CheckboxState.None)
                {
                    model.CurrentPageOnly = UnselectedItems.Count == current.Count();
                }
                else
                {
                    model.CurrentPageOnly = model.Results
                        .Except(DeletedItems)
                        .Intersect(UnselectedItems).Count() == UnselectedItems.Count();
                }
            }
            else
                model.CurrentPageOnly = true;

            // This prevents the form data from persisting.
            ModelState.Clear();

            var s = new SearchViewModel
            {
                Pagination = model,
                Context = model.Expand == "all" || SearchOptions == null ? SearchContext.People : SearchOptions.Context,
                AdvancedSearch = SearchOptions != null && SearchOptions is SearchOptionsAdvanced ? new AdvancedSearchViewModel() : null
            };

            s.Pagination.ResultCount = results.Count;

            return PartialView("_PagePartial", s);
        }

        [HttpGet]
        [SavesSession]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(SearchViewModel s, string id)
        {
            bool showGrid = false;
            int _id = 0;

            GetPinnedItems();

            if (!string.IsNullOrWhiteSpace(Request.Url.Query) || !string.IsNullOrEmpty(id))
            {
                showGrid = true;

                if (!string.IsNullOrEmpty(id))
                {
                    try
                    {
                        if (int.TryParse(id, out _id) && _id > 0)
                        {
                            // We were passed a saved search id so fetch its details.
                            SearchOptions = _savedSearchService.GetSavedSearchOptions(_id, CurrentUser.SessionKey);

                            if (SearchOptions.Type == SearchType.Quick)
                            {
                                SearchOptionsQuick soq = (SearchOptionsQuick)SearchOptions;

                                s.SearchText = soq.SearchText;
                                s.SelectedId = soq.SelectedId;
                                s.SelectedRecordType = (int)soq.SelectedRecordType;
                                s.CountryID = (int)(soq.SearchAustralia ? Countries.Australia : soq.SearchNewZealand ? Countries.NewZealand : Countries.AllCountries);
                            }

                            s.Context = SearchOptions.Context;

                            s.Name = SearchOptions.SearchName;
                            s.SearchId = _id;
                        }
                        else if (Regex.IsMatch(id, @"^RS\d+"))
                        {
                            int recentSearchId = int.Parse(Regex.Match(id, @"\d+").Value);

                            SearchLogViewModel search = _searchService.GetRecentSearchById(recentSearchId, CurrentUser.SessionKey);

                            SearchOptions = GetSavedSearchOptions(search, s);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Either the saved search doesnt exist or the user doesn't have access.
                        BasePage.LogException(ex, this.GetType().FullName, "Index", new NameValueCollection { { "id", _id.ToString() } });
                        ViewData["ErrorMessage"] = Search_InvalidSavedSearch;
                        return View(new SearchViewModel() { Pagination = new PaginationSearchViewModel() { Results = new List<SearchResult>() } });
                    }
                }
                else
                {
                    // Create a search object based on the SearchViewModel.
                    SearchOptions = (SearchOptionsQuick)GetSearchOptionsFromViewModel(s);
                }
            }
            else
            {
                if (SearchOptions == null)
                {
                    s.CountryID = CurrentUser.DefaultSearchCountry;
                }
            }

            // If we got search options then perform a search.
            if (SearchOptions != null && SearchOptions.HasSearchCriteria && !(SearchOptions is SearchOptionsAdvanced))
            {
                // This is a new search so clear the deleted and checkbox items of the previous search.
                ClearDeletedItems();
                ClearUnselectedItems();

                using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
                {
                    s.Pagination.Results = searchServiceProxy.Search(SearchOptions, s.Pagination.SortColumn, s.Pagination.SortDirection, true, CurrentUser.UserID, 0, s.Pagination.PageSize,
                        PinnedItems, DeletedItems, UnselectedItems);

                    s.Pagination.ResultCount = searchServiceProxy.TotalResults();
                }

                // Calculate pagination.
                s.Pagination.CalculatePagination();

                // Check if all items in current search are pinned.
                s.Pagination.Pinning.AllPinned = PinnedItems.Count == s.Pagination.ResultCount;

                // Set current page to true for first load.
                s.Pagination.CurrentPageOnly = true;

                // Log search
                _searchService.LogSearch(new SearchLogViewModel()
                {
                    UserId = CurrentUser.UserID,
                    DebtorNumber = CurrentUser.DebtorNumber,
                    SearchType = (int)SearchOptions.Type,
                    SearchCriteria = SearchBAL.GetSearchCriteria(SearchOptions),
                    CreatedDate = DateTime.Now,
                    SavedSearchId = _id,
                    System = MediaContactsService.SystemType.Contacts,
                    SearchContext = SearchOptions.ContextDBCode,
                    ResultsCount = s.Pagination.ResultCount
                }, CurrentUser.SessionKey);
            }
            else if (showGrid)
            {
                s.Pagination.Results = new List<SearchResult>();
            }

            s.HasViewedTermsAndConditions = CurrentUser.HasViewedTermsAndConditions;

            InitDropdowns(SearchOptions);
            return View(s);
        }

        private SearchOptions GetSavedSearchOptions(SearchLogViewModel search, SearchViewModel s)
        {
            SearchOptions so = _savedSearchService.GetRecentSearchOptions(search, CurrentUser.SessionKey);

            if (so.Type == SearchType.Quick)
            {
                SearchOptionsQuick soq = (SearchOptionsQuick)so;

                s.SearchText = StringHelper.CleanQuery(soq.SearchText);
                s.SelectedId = soq.SelectedId;
                s.SelectedRecordType = (int)soq.SelectedRecordType;
                s.CountryID = (int)(soq.SearchAustralia ? Countries.Australia : soq.SearchNewZealand ? Countries.NewZealand : Countries.AllCountries);
            }

            s.Context = so.Context;

            return so;
        }

        [RequiresSession, SavesSession]
        [HandleAuthentication, HandleHtmlError]
        public ActionResult ExpandAll(SearchViewModel s, bool Advanced = false)
        {
            // Only if it's an expand from quick search do we use the view model.
            if (Advanced)
            {
                AdvancedSearchViewModel a = Mapper.Map<SearchOptionsAdvanced, AdvancedSearchViewModel>(SearchOptions as SearchOptionsAdvanced);
                a.ShowResults = true;
                PopulateAdvancedSearchDatasources(a);
                s.AdvancedSearch = a;
            }
            else
            {
                //SearchOptions = GetSearchOptionsFromViewModel(s);
                InitDropdowns(SearchOptions);
            }

            using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
            {
                s.Pagination.Results = searchServiceProxy.SearchWithExpandedOutlets(SearchOptions, SortColumn.OutletName, SortDirection.ASC, CurrentUser.UserID, 0, s.Pagination.PageSize, PinnedItems, DeletedItems, UnselectedItems);

                s.Pagination.ResultCount = searchServiceProxy.TotalResults();
            }

            s.Context = SearchContext.People;
            ViewBag.Expand = "all";

            // Calculate pagination.
            s.Pagination.CalculatePagination();

            return View(Advanced ? "Advanced" : "Index", s);
        }

        [RequiresSession, SavesSession]
        [HandleAuthentication, HandleHtmlError]
        public ActionResult Expand(string outletid, int type, bool Advanced)
        {
            return View(Advanced ? "Advanced" : "Index", GetExpandResult(outletid, type, Advanced));
        }

        [NonAction]
        public SearchViewModel GetExpandResult(string outletId, int type, bool advanced)
        {
            SearchOptionsOutlet so = new SearchOptionsOutlet
            {
                OutletID = outletId,
                OutletType = (RecordType)type
            };

            SearchViewModel searchModel = new SearchViewModel();

            if (Request != null && Request.UrlReferrer != null && Request.UrlReferrer.AbsolutePath.StartsWith("/outlet/"))
            {
                searchModel.NavigateBack = true;
            }

            using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
            {
                searchModel.Pagination.Results = searchServiceProxy.Search(so, SortColumn.OutletName, SortDirection.ASC, true, CurrentUser.UserID, 0, searchModel.Pagination.PageSizeDefault,
                    PinnedItems, DeletedItems, UnselectedItems);

                searchModel.Pagination.ResultCount = searchServiceProxy.TotalResults();
            }

            // Only if it's an expand from quick search do we use the view model.
            if (advanced)
            {
                AdvancedSearchViewModel a = Mapper.Map<SearchOptionsAdvanced, AdvancedSearchViewModel>(SearchOptions as SearchOptionsAdvanced);
                a.ShowResults = true;
                PopulateAdvancedSearchDatasources(a);

                searchModel.AdvancedSearch = a;
            }
            else
            {
                InitDropdowns(so);
            }

            // Calculate pagination.
            searchModel.Pagination.CalculatePagination();

            searchModel.Pagination.Expand = ViewBag.Expand = string.Format("{0}#{1}", ((int)so.OutletType).ToString(), so.OutletID);

            SearchOptions = so;

            return searchModel;
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Save()
        {
            SavedSearchViewModel s = new SavedSearchViewModel
            {
                Groups = new SelectList(_groupService.GetGroupsByType(GroupType.Search, CurrentUser.UserID, CurrentUser.SessionKey), "Id", "Name")
            };

            return PartialView("_SaveSearchPartial", s);
        }

        [HttpPost, HandleAjaxAuthorization, RequiresSession, HandleAjaxError]
        public ActionResult Save(SavedSearchViewModel s)
        {
            s.Groups = new SelectList(_groupService.GetGroupsByType(GroupType.Search, CurrentUser.UserID, CurrentUser.SessionKey), "Id", "Name");

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(s.Group.Name))
                {
                    // New group.
                    s.Group.UserId = CurrentUser.UserID;
                    s.Group.Type = GroupType.Search;
                    s.Group.Id = _groupService.CreateGroup(s.Group, CurrentUser.SessionKey);
                }
                s.SearchId = _savedSearchService.Create(s, SearchOptions, CurrentUser.UserID);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ModelErrorMessages);
        }

        // GET: /Search/AutoSuggest/
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult AutoSuggest(string term, string context, int country)
        {
            List<AutoSuggest> results = null;

            SearchContext sc = (SearchContext)Enum.Parse(typeof(SearchContext), context);

            if (!string.IsNullOrWhiteSpace(term))
            {

                using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
                {
                    results = searchServiceProxy.AutoSuggest(sc, term.TrimStart(),
                        country == (int)Countries.Australia,
                        country == (int)Countries.NewZealand,
                        country == (int)Countries.AllCountries,
                        MaxAutosuggest,
                        CurrentUser.UserID);
                }
            }

            return this.Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        [RequiresSession]
        public PartialViewResult QuickSearchPanel(bool redirectToSearchPage)
        {
            SearchViewModel model = redirectToSearchPage == false && SearchOptions is SearchOptionsQuick ?
                Mapper.Map<SearchOptionsQuick, SearchViewModel>((SearchOptionsQuick)SearchOptions) : null;

            if (model == null)
                SearchOptions = (SearchOptionsQuick)GetSearchOptionsFromViewModel(new SearchViewModel());

            InitDropdowns(SearchOptions);

            return PartialView("~/Views/Search/_QuickSearchPanelPartial.cshtml", model ?? new SearchViewModel());
        }

        [HandleAjaxError, HandleAjaxAuthorization]
        public PartialViewResult GetRecentSearchList()
        {
            List<SearchLogViewModel> list = _searchService.GetRecentSearchList(6, CurrentUser.SessionKey);

            using (DBClass db = new DBClass())
            {
                SubjectBAL sb = new SubjectBAL(db);
                GeographicBAL gb = new GeographicBAL(db);
                GeneralBAL generalBAL = new GeneralBAL(db);
                MediaTypeBAL MTB = new MediaTypeBAL(db);
                PositionBAL PB = new PositionBAL(db);

                List<string> criteria = new List<string>();

                foreach (var item in list)
                {
                    criteria = new List<string>();

                    SearchOptions so = GetSavedSearchOptions(item, new SearchViewModel());

                    if (item.SearchType == (int)SearchType.Quick)
                    {
                        SearchOptionsQuick soq = (SearchOptionsQuick)so;

                        criteria.Add(soq.SearchText);

                        if (soq.SubjectList != null && soq.SubjectList.Count > 0)
                            criteria.Add(string.Join(", ", (from sub in sb.GetSubjects().Where(subject => soq.SubjectList.Contains(subject.Id)) select sub.Name).ToList()));

                        if (soq.SearchNewZealand)
                            criteria.Add(EnumHelper.GetDescription(Countries.NewZealand));
                        else if (soq.SearchAllCountries)
                            criteria.Add(EnumHelper.GetDescription(Countries.AllCountries));

                        if (soq.LocationList != null && soq.LocationList.Count > 0)
                            criteria.Add(string.Join(", ", InitLocationList(soq.LocationList).Select(m => m.Id)));

                        if (soq.MediaTypeList != null && soq.MediaTypeList.Count > 0)
                            criteria.Add(string.Join(", ", (from mt in MTB.GetMediaTypes().Where(mtype => soq.MediaTypeList.Contains(mtype.Id)) select mt.Name).ToList()));

                        if (soq.PositionList != null && soq.PositionList.Count > 0)
                            criteria.Add(string.Join(", ", (from pos in PB.GetPositions().Where(pos => soq.PositionList.Contains(pos.Id)) select pos.Name).ToList()));
                    }
                    if (item.SearchType == (int)SearchType.Outlet)
                    {
                        SearchOptionsOutlet soq = (SearchOptionsOutlet)so;

                        var outletViewModel = soq.OutletType == RecordType.MediaOutlet ? _outletService.GetOutletPreview(soq.OutletID, soq.OutletType, CurrentUser) :
                                _outletService.GetOutlet(soq.OutletID, soq.OutletType, CurrentUser);

                        item.OutletId = soq.OutletID;
                        item.OutletType = (int)soq.OutletType;

                        criteria.Add(outletViewModel.Name);

                    }
                    else if (item.SearchType == (int)SearchType.Advanced)
                    {
                        SearchOptionsAdvanced soq = (SearchOptionsAdvanced)so;

                        criteria.Add(soq.SearchText);
                        criteria.Add(soq.FirstName);
                        criteria.Add(soq.LastName);
                        criteria.Add(soq.FullName);
                        criteria.Add(soq.JobTitle);

                        if (soq.PositionList != null && soq.PositionList.Count > 0)
                            criteria.Add(string.Join(", ", (from pos in PB.GetPositions().Where(pos => soq.PositionList.Contains(pos.Id)) select pos.Name).ToList()));

                        if (soq.ContactSubjectList != null && soq.ContactSubjectList.Count > 0)
                            criteria.Add(string.Join(", ", (from sub in sb.GetSubjects().Where(subject => soq.ContactSubjectList.Contains(subject.Id)) select sub.Name).ToList()));

                        if (soq.ContactContinentIDList != null && soq.ContactContinentIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetContinents().Where(m => soq.ContactContinentIDList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.ContactCountryIDList != null && soq.ContactCountryIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetCountries().Where(m => soq.ContactCountryIDList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.ContactStateIDList != null && soq.ContactStateIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetStates().Where(m => soq.ContactStateIDList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.ContactCityIDList != null && soq.ContactCityIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetCities().Where(m => soq.ContactCityIDList.Contains(m.Id)) select model.Name).ToList()));

                        criteria.Add(string.Join(", ", soq.ContactPostCodeList));

                        criteria.Add(soq.ContactInfluencerScore);

                        if (soq.PrimaryNewsContactOnly)
                            criteria.Add("Primary contacts only");

                        criteria.Add(soq.OutletName);

                        if (soq.MediaTypeList != null && soq.MediaTypeList.Count > 0)
                            criteria.Add(string.Join(", ", (from mt in MTB.GetMediaTypes().Where(mtype => soq.MediaTypeList.Contains(mtype.Id)) select mt.Name).ToList()));

                        if (soq.OutletSubjectList != null && soq.OutletSubjectList.Count > 0)
                            criteria.Add(string.Join(", ", (from sub in sb.GetSubjects().Where(subject => soq.OutletSubjectList.Contains(subject.Id)) select sub.Name).ToList()));


                        if (soq.OutletFrequencyList != null && soq.OutletFrequencyList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in generalBAL.GetFrequencies().Where(m => soq.OutletFrequencyList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.OutletCirculationMin > 0)
                            criteria.Add(soq.OutletCirculationMin.ToString());

                        if (soq.OutletCirculationMax > 0)
                            criteria.Add(soq.OutletCirculationMax.ToString());

                        if (soq.OutletLanguageList != null && soq.OutletLanguageList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in generalBAL.GetLanguages().Where(m => soq.OutletLanguageList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.OutletContinentIDList != null && soq.OutletContinentIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetContinents().Where(m => soq.OutletContinentIDList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.OutletCountryIDList != null && soq.OutletCountryIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetCountries().Where(m => soq.OutletCountryIDList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.OutletStateIDList != null && soq.OutletStateIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetStates().Where(m => soq.OutletStateIDList.Contains(m.Id)) select model.Name).ToList()));

                        if (soq.OutletCityIDList != null && soq.OutletCityIDList.Count > 0)
                            criteria.Add(string.Join(", ", (from model in gb.GetCities().Where(m => soq.OutletCityIDList.Contains(m.Id)) select model.Name).ToList()));

                        criteria.Add(string.Join(", ", soq.OutletPostCodeList));

                        if (soq.IncludeMetroOutlets)
                            criteria.Add("Metro");

                        if (soq.IncludeRegionalOutlets)
                            criteria.Add("Regional");

                        if (soq.IncludeSuburbanOutlets)
                            criteria.Add("Suburban");

                        if (soq.RecordsToShow == ShowResultType.M.ToString())
                            criteria.Add(EnumHelper.GetDescription(ShowResultType.M));
                        else if (soq.RecordsToShow == ShowResultType.P.ToString())
                            criteria.Add(EnumHelper.GetDescription(ShowResultType.P));

                        if (soq.ShowOnlyWithTwitter)
                            criteria.Add("Twitter");

                        if (soq.ShowOnlyWithLinkedIn)
                            criteria.Add("LinkedIn");

                        if (soq.ShowOnlyWithFacebook)
                            criteria.Add("Facebook");

                        if (soq.ShowOnlyWithInstagram)
                            criteria.Add("Instagram");

                        if (soq.ShowOnlyWithYouTube)
                            criteria.Add("YouTube");

                        if (soq.ShowOnlyWithSnapchat)
                            criteria.Add("Snapchat");

                        if (soq.ShowOnlyWithEmail)
                            criteria.Add("Email");

                        if (soq.ShowOnlyWithFax)
                            criteria.Add("Fax");

                        if (soq.ShowOnlyWithPostalAddress)
                            criteria.Add("Postal address");

                        criteria.Add(soq.ExcludeKeywords);

                        if (soq.ExcludeOPS)
                            criteria.Add("Exclude offices, programmes, supplements");

                        if (!soq.ExcludeFinanceInst)
                            criteria.Add("Include financial institutes");

                        if (!soq.ExcludePoliticians)
                            criteria.Add("Include politicians");
                        if (soq.ExcludeOutOfOffice)
                            criteria.Add("Exclude Out of Office");

                        // do prefers
                        var prefers = "";
                        if (soq.PrefersEmail)
                        {
                            prefers = string.Join(",", prefers, "Email");
                        }
                        if (soq.PrefersMail)
                        {
                            prefers = string.Join(",", prefers, "Mail");
                        }
                        if (soq.PrefersPhone)
                        {
                            prefers = string.Join(",", prefers, "Phone");
                        }
                        if (soq.PrefersMobile)
                        {
                            prefers = string.Join(",", prefers, "Mobile");
                        }
                        if (soq.PrefersTwitter)
                        {
                            prefers = string.Join(",", prefers, "Twitter");
                        }
                        if (soq.PrefersLinkedIn)
                        {
                            prefers = string.Join(",", prefers, "LinkedIn");
                        }
                        if (soq.PrefersFacebook)
                        {
                            prefers = string.Join(",", prefers, "Facebook");
                        }
                        if (soq.PrefersInstagram)
                        {
                            prefers = string.Join(",", prefers, "Instagram");
                        }
                        if (prefers.Length > 1)
                        {
                            prefers = "Prefers:" + prefers.Substring(1);
                            criteria.Add(prefers);
                        }
                    }

                    item.SearchCriteria = string.Join(", ", criteria.FindAll(m => !string.IsNullOrEmpty(m.Trim())));
                }
            }

            return PartialView("~/Views/Search/_RecentSearchesPartial.cshtml", list);
        }

        #region Advanced Search

        /// <summary>
        /// Populates the advanced search datasources.
        /// </summary>
        /// <param name="model">The model.</param>
        private void PopulateAdvancedSearchDatasources(AdvancedSearchViewModel model)
        {
            using (DBClass db = new DBClass())
            {
                GeographicBAL gb = new GeographicBAL(db);
                PositionBAL pb = new PositionBAL(db);
                GeneralBAL gen = new GeneralBAL(db);
                SubjectBAL sb = new SubjectBAL(db);
                MediaTypeBAL mtb = new MediaTypeBAL(db);
                IGroupService groupSvc = GroupService.CreateGroupService();
                IListService listSvc = ListService.CreateListService();

                model.MediaTypes = mtb.GetMediaTypes();
                model.Subjects = sb.GetSubjects();
                model.SubjectGroups = sb.GetSubjectGroups();
                model.Positions = pb.GetPositions().ToList();
                model.Frequencies = gen.GetFrequencies();
                model.Languages = gen.GetLanguages();
                model.Continents = gb.GetContinents().Where(con => CurrentUser.DataModules.Contains(con.DataModuleId)).ToList();

                // Pre-fetch all the lists for this user.
                List<ListViewModel> lists = listSvc.GetLists(CurrentUser.UserID, CurrentUser.SessionKey);
                model.Lists = new List<ListBase>();
                model.Lists.AddRange(lists.Select(lst => new List { Id = lst.ListId, Name = lst.Name }));
            }
        }

        [HttpGet]
        [HandleAuthentication, HandleHtmlError]
        [RequiresSession, SavesSession]
        public ActionResult Advanced(bool Resume = false, bool r = false, string id = "")
        {
            AdvancedSearchViewModel model = null;
            SearchViewModel s = new SearchViewModel();
            int _id = 0;
            int.TryParse(id, out _id);
            string viewName = "Advanced"; 

            // We're refining a search.
            if (r && SearchOptions != null && SearchOptions is SearchOptionsAdvanced)
            {
                // We're refining a saved search.
                if (_id > 0)
                    SearchOptions = _savedSearchService.GetSavedSearchOptions(_id, CurrentUser.SessionKey);

                model = Mapper.Map<SearchOptionsAdvanced, AdvancedSearchViewModel>(SearchOptions as SearchOptionsAdvanced);
                s.AdvancedSearch = model;
                PopulateAdvancedSearchDatasources(model);
            }
            // Loading a saved search and not refining.
            else if (!string.IsNullOrEmpty(id) && !r)
            {
                if (!Resume)
                {
                    ClearDeletedItems();
                    ClearUnselectedItems();
                }

                if (Regex.IsMatch(id, @"^RS\d+"))
                {
                    int recentSearchId = int.Parse(Regex.Match(id, @"\d+").Value);

                    SearchLogViewModel search = _searchService.GetRecentSearchById(recentSearchId, CurrentUser.SessionKey);

                    SearchOptions = GetSavedSearchOptions(search, s);
                }
                else
                {
                    SearchOptions = _savedSearchService.GetSavedSearchOptions(_id, CurrentUser.SessionKey);
                    s.SearchId = _id;
                }

                PerformAdvancedSearch(s, true);

                // Set current page to true for first load.
                s.Pagination.CurrentPageOnly = true;

                // Log Search
                _searchService.LogSearch(new SearchLogViewModel()
                {
                    UserId = CurrentUser.UserID,
                    DebtorNumber = CurrentUser.DebtorNumber,
                    SearchType = (int)SearchOptions.Type,
                    SearchCriteria = SearchBAL.GetSearchCriteria(SearchOptions),
                    CreatedDate = DateTime.Now,
                    SavedSearchId = _id,
                    System = MediaContactsService.SystemType.Contacts,
                    SearchContext = SearchOptions.ContextDBCode,
                    ResultsCount = s.Pagination.ResultCount
                }, CurrentUser.SessionKey);

                viewName = "AdvancedResult";
            }
            else
            {
                // Resuming a search. This is when you click back on a contact or outlet page.
                if (Resume)
                    PerformAdvancedSearch(s, false);
                // New search.
                else
                {
                    model = new AdvancedSearchViewModel();
                    s.AdvancedSearch = model;
                    PopulateAdvancedSearchDatasources(model);
                }
            }

            return View(viewName, s);
        }

        [HttpPost]
        [RequiresSession, SavesSession]
        [HandleAuthentication, HandleHtmlError]
        public ActionResult Advanced(AdvancedSearchViewModel model)
        {
            GetAdvanceSearchResult(model);

            return RedirectToAction("AdvancedResult");
        }

        [HttpGet]
        [HandleAuthentication, HandleHtmlError]
        [RequiresSession]
        public ActionResult AdvancedResult()
        {
            AdvancedSearchViewModel model = null;
            SearchViewModel s = new SearchViewModel();
         
            // We're refining a search.
            if (SearchOptions != null && SearchOptions is SearchOptionsAdvanced)
            {
                model = Mapper.Map<SearchOptionsAdvanced, AdvancedSearchViewModel>(SearchOptions as SearchOptionsAdvanced);
                s.AdvancedSearch = model;
                PopulateAdvancedSearchDatasources(model);
            }

            // If we got search options then perform a search.
            if (SearchOptions != null && SearchOptions.HasSearchCriteria && SearchOptions is SearchOptionsAdvanced)
            {
                using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
                {
                    s.Pagination.Results = searchServiceProxy.Search(SearchOptions, s.Pagination.SortColumn, s.Pagination.SortDirection, false, CurrentUser.UserID, 0, s.Pagination.PageSize,
                        PinnedItems, DeletedItems, UnselectedItems);

                    s.Pagination.ResultCount = searchServiceProxy.TotalResults();
                }

                // Calculate pagination.
                s.Pagination.CalculatePagination();
            }

            return View(s);
        }

        [NonAction]
        public SearchViewModel GetAdvanceSearchResult(AdvancedSearchViewModel model, int? pageSize = null, bool IsSearchPage = true)
        {
            model.SearchText = StringHelper.CleanQuery(model.SearchText);

            SearchViewModel s = new SearchViewModel();
            SearchOptionsAdvanced so = Mapper.Map<AdvancedSearchViewModel, SearchOptionsAdvanced>(model);
            so.Context = SearchContext.Both;
            model.Lists = _listService.GetListsByIds(so.ListList.JoinDelimited(",", x => x.ToString()));

            s.AdvancedSearch = model;
            SearchOptions = so;
            s.Pagination.UseCookieForPageSize = IsSearchPage;

            if (so != null && so.HasSearchCriteria)
            {
                if (IsSearchPage)
                {
                    // This is a new search so clear the deleted and checkbox items of the previous search.
                    ClearDeletedItems();
                    ClearUnselectedItems();
                }

                using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
                {
                    s.Pagination.Results = searchServiceProxy.Search(so, s.Pagination.SortColumn, s.Pagination.SortDirection, true, CurrentUser.UserID, 0,
                       pageSize.HasValue ? pageSize : s.Pagination.PageSize, PinnedItems, DeletedItems, UnselectedItems);
                    s.Pagination.ResultCount = searchServiceProxy.TotalResults();
                    s.Pagination.CalculatePagination();

                }

                if (IsSearchPage)
                    // log search
                    _searchService.LogSearch(new SearchLogViewModel()
                    {
                        UserId = CurrentUser.UserID,
                        DebtorNumber = CurrentUser.DebtorNumber,
                        SearchType = (int)SearchOptions.Type,
                        SearchCriteria = SearchBAL.GetSearchCriteria(SearchOptions),
                        CreatedDate = DateTime.Now,
                        System = MediaContactsService.SystemType.Contacts,
                        SearchContext = SearchOptions.ContextDBCode,
                        ResultsCount = s.Pagination.ResultCount
                    }, CurrentUser.SessionKey);
            }
            else
                s.Pagination.Results = new List<SearchResult>();

            s.AdvancedSearch.ShowResults = true;
            s.Context = s.AdvancedSearch.GetContext();

            // Set current page to true for first load.
            s.Pagination.CurrentPageOnly = true;

            return s;
        }

        [OutputCache(Duration = 0)]
        public PartialViewResult RenderListContactReplacements()
        {
            List<ListContactReplacementViewModel> ll = null;

            try
            {
                ll = _listService.GetLivingListContactReplacements(CurrentUser.SessionKey, CurrentUser.UserID);
            }
            catch (Exception e)
            {
                BasePage.LogException(e, this.GetType().FullName, "RenderListContactReplacements", null);
            }

            return PartialView("_LivingListUpdates", ll);
        }

        #endregion

        #endregion

        #region Deleting

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError, RequiresSession, SavesSession]
        public JsonResult Delete(DeleteViewModel model)
        {
            List<SearchResult> results = null;

            results = GetCurrentSearchResults(model.Expand, SortColumn.Default, SortDirection.ASC);

            if (model.Type == DeleteAction.DeleteSelected)
            {
                DeletedItems = DeletedItems
                    .Union(GetSelectedRecords(model, results))
                    .ToList();
            }
            else if (model.Type == DeleteAction.DeleteOne)
                DeletedItems = DeletedItems.Union(CSVPsToRecordIdentifiers(model.CheckedItems)).ToList();

            UnselectedItems = UnselectedItems.Except(DeletedItems).ToList();
            PinnedItems = PinnedItems.Except(DeletedItems).ToList();

            return Json(new { DeletedCount = results.Count - UnselectedItems.Count });
        }

        #endregion

        #region Pinninng

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError, RequiresSession, SavesSession]
        public JsonResult Pin(PinningViewModel model)
        {
            List<RecordIdentifier> records = null;
            List<SearchResult> results = GetCurrentSearchResults(model.Expand, SortColumn.Default, SortDirection.ASC);

            if (!string.IsNullOrWhiteSpace(model.Items))
                records = CSVPsToRecordIdentifiers(model.Items);

            if (model.Type == PinAction.PinOne)
            {
                PinnedItems = PinnedItems.Union(records).ToList();
                // Check if all elements in the current search are pinned.
                model.AllPinned = PinnedItems.Intersect(results).Count() == results.Count();
            }
            else if (model.Type == PinAction.UnpinOne)
                PinnedItems = PinnedItems.Except(records).ToList();
            else if (model.Type == PinAction.PinAll)
            {
                PinnedItems = PinnedItems.Union(results).ToList();
                model.AllPinned = true;
            }
            else if (model.Type == PinAction.UnpinAll)
                PinnedItems = PinnedItems.Except(results).ToList();
            else if (model.Type == PinAction.Clear)
                ClearPinnedItems();

            return Json(model);
        }

        #endregion

        #region "Other Content"

        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult RenderRecentSavedSearches()
        {
            List<SavedSearchViewModel> savedSearches = _savedSearchService.GetRecentSavedSearches(CurrentUser.UserID, CurrentUser.SessionKey);
            return PartialView("_RecentSavedSearches", savedSearches);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Performs an advanced search.
        /// </summary>
        /// <param name="model">The search view model.</param>
        /// <param name="newSearch">True if performing a new search, false to use the cache.</param>
        private void PerformAdvancedSearch(SearchViewModel model, bool newSearch = true)
        {
            if (SearchOptions != null && SearchOptions.HasSearchCriteria)
            {
                model.Name = SearchOptions.SearchName;
                model.Context = SearchOptions.Context;
                using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
                {
                    model.Pagination.Results = searchServiceProxy.Search(SearchOptions, SortColumn.Default, SortDirection.ASC, newSearch, CurrentUser.UserID, 0, model.Pagination.PageSize,
                    PinnedItems, DeletedItems, UnselectedItems);
                    model.Pagination.ResultCount = searchServiceProxy.TotalResults();
                }
                model.Pagination.CalculatePagination();
            }
            else
                model.Pagination.Results = new List<SearchResult>();

            model.AdvancedSearch = Mapper.Map<SearchOptionsAdvanced, AdvancedSearchViewModel>(SearchOptions as SearchOptionsAdvanced);
            model.AdvancedSearch.ShowResults = true;
        }

        /// <summary>
        /// Gets the current paged search.
        /// </summary>
        /// <param name="p">The pagination view model.</param>
        /// <returns>List of search results.</returns>
        private List<SearchResult> GetCurrentPagedSearch(PaginationSearchViewModel p, List<SearchResult> results)
        {
            using (SearchServiceProxy searchServiceProxy = new SearchServiceProxy())
            {
                results = searchServiceProxy.FetchRange(results, (p.Page - 1) * p.PageSize, p.PageSize, PinnedItems,
                    DeletedItems, UnselectedItems, CurrentUser.UserID);
            }
            return results;
        }

        /// <summary>
        /// Sets the unselected items.
        /// </summary>
        /// <param name="p">The pagination view model.</param>
        private void SetUnselectedItems(PaginationSearchViewModel p, List<SearchResult> results)
        {
            // If all are unselected, just do a cache search and set them all to be unselected.
            if (p.CheckboxState == CheckboxState.None)
            {
                ClearUnselectedItems();
                UnselectedItems = UnselectedItems.Union(results).ToList();
            }
            else if (p.CheckboxState == CheckboxState.All)
                ClearUnselectedItems();
            else
            {
                // If only this page, clear unselected items.
                if (p.CurrentPageOnly)
                    ClearUnselectedItems();
                else
                    results = new List<SearchResult>();

                // If all was selected and some have been unchecked, simply add items to list.
                if (p.CheckboxState == CheckboxState.PartAll)
                {
                    if (!string.IsNullOrWhiteSpace(p.UncheckedItems))
                        UnselectedItems = UnselectedItems.Union(CSVPsToRecordIdentifiers(p.UncheckedItems)).ToList();
                }
                else if (p.CheckboxState == CheckboxState.PartNone)
                {
                    // Add any new unchecked items.
                    if (!string.IsNullOrWhiteSpace(p.UncheckedItems))
                        UnselectedItems = UnselectedItems.Union(CSVPsToRecordIdentifiers(p.UncheckedItems)).ToList();

                    // Remove any items that are now checked.
                    if (!string.IsNullOrWhiteSpace(p.CheckedItems))
                    {
                        UnselectedItems = UnselectedItems.Except(CSVPsToRecordIdentifiers(p.CheckedItems)).ToList();
                        results = results.Where(r => !CSVPsToRecordIdentifiers(p.CheckedItems).Contains(r)).ToList();
                    }
                    UnselectedItems = results.Union(UnselectedItems).ToList();
                }
            }
            UnselectedItems = UnselectedItems.Except(DeletedItems).ToList();
        }

        //private void InitSave(SavedSearchViewModel s) {
        //    ViewData["NewListId"] = s.SearchId;

        //}

        /// <summary>
        /// Add the quick search filter options to the ViewBag to configure the dropdowns.
        /// </summary>
        /// <param name="so">A SearchoptionsQuick object populated with the selected filter options.</param>
        /// <param name="db">An existing database connection.</param>
        private void InitDropdowns(SearchOptions so)
        {
            using (DBClass db = new DBClass())
            {
                SubjectBAL sb = new SubjectBAL(db);
                List<SubjectGroup> subGroups = sb.GetSubjectGroups();

                ViewBag.SubjectGroups = subGroups.Where(g => g.Subjects.Count > 0);

                if (so != null && so.Type == SearchType.Quick)
                {
                    SearchOptionsQuick soq = (SearchOptionsQuick)so;

                    if (soq.SubjectList != null && soq.SubjectList.Count > 0)
                    {
                        ViewBag._subjects = (from sub in sb.GetSubjects().Where(subject => soq.SubjectList.Contains(subject.Id)) select sub).ToList();
                    }
                    if (soq.SubjectGroupList != null && soq.SubjectGroupList.Count > 0)
                    {
                        // _subjects and _subjectGroups get merged together so cast this to a Subject type as well.
                        ViewBag._subjectGroups = (from sg in subGroups.Where(subGroup => soq.SubjectGroupList.Contains(subGroup.Id)) select new Subject() { Id = sg.Id, Name = sg.Name }).ToList();
                    }
                    if (soq.LocationList != null && soq.LocationList.Count > 0)
                    {
                        ViewBag._locations = InitLocationList(soq.LocationList);
                    }
                    if (soq.MediaTypeList != null && soq.MediaTypeList.Count > 0)
                    {
                        MediaTypeBAL MTB = new MediaTypeBAL(db);
                        ViewBag._mediaTypes = (from mt in MTB.GetMediaTypes().Where(mtype => soq.MediaTypeList.Contains(mtype.Id)) select mt).ToList();
                    }
                    if (soq.PositionList != null && soq.PositionList.Count > 0)
                    {
                        PositionBAL PB = new PositionBAL(db);
                        ViewBag._positions = (from pos in PB.GetPositions().Where(pos => soq.PositionList.Contains(pos.Id)) select pos).ToList();
                    }
                }
            }
        }

        /// <summary>
        /// Returns a SearchOptionQuick object based on the view model.
        /// </summary>
        /// <param name="s">The view model.</param>
        /// <returns>A SearchOptionQuick object,</returns>
        public SearchOptionsQuick GetSearchOptionsFromViewModel(SearchViewModel s)
        {
            SearchOptionsQuick soq = new SearchOptionsQuick();

            soq.Context = s.Context;

            soq.SearchAustralia = s.Country == Countries.Australia;
            soq.SearchNewZealand = s.Country == Countries.NewZealand;
            soq.SearchAllCountries = s.Country == Countries.AllCountries;
            //soq.SearchAllCountries = s.SearchAllCountries; //Sahar Commented this line because it was duplicated

            // Location is only valid when searching Australian data only.
            if (soq.SearchAustralia && !soq.SearchNewZealand && !soq.SearchAllCountries)
                soq.LocationList = SearchOptionsQuick.ParseLocation(s.Locations, ',');

            soq.MediaTypeList = string.IsNullOrWhiteSpace(s.MediaTypes) ? new List<int>() : s.MediaTypes.ParseIntCSV();

            // Position is not valid when searching Outlet only.
            if (soq.Context != SearchContext.Outlet)
                soq.PositionList = string.IsNullOrWhiteSpace(s.Positions) ? new List<int>() : s.Positions.ParseIntCSV();

            soq.SubjectList = string.IsNullOrWhiteSpace(s.Subjects) ? new List<int>() : s.Subjects.ParseIntCSV();
            soq.SubjectGroupList = string.IsNullOrWhiteSpace(s.Subjects) ? new List<int>() : s.Subjects.ParsePrefixedIntCSV("g");

            soq.SubjectList = string.IsNullOrWhiteSpace(s.Subjects) ? new List<int>() : s.Subjects.ParseIntCSV();
            soq.SearchText = StringHelper.CleanQuery(s.SearchText);

            if (!string.IsNullOrWhiteSpace(s.SelectedId))
            {
                soq.SelectedId = s.SelectedId;
                soq.SelectedRecordType = (RecordType)s.SelectedRecordType;
            }

            return soq;
        }

        /// <summary>
        /// Returns a NameValueCollection containing relevant items in the SearchViewModel.
        /// </summary>
        /// <param name="s">The SearchViewModel to get values from.</param>
        /// <param name="more">Another NameValueCollection to append to the end of the one created.</param>
        /// <returns>A NameValueCollection.</returns>
        private NameValueCollection SearchViewModelToCollection(SearchViewModel s, NameValueCollection more)
        {
            NameValueCollection col = new NameValueCollection();

            try
            {
                if (s != null)
                {
                    //col.Add("SearchId", s.SearchId.ToString());
                    //col.Add("Name", s.Name);
                    //col.Add("Criteria", s.Criteria);
                    //col.Add("Status", s.Status.ToString());
                    //col.Add("IsPrivate", s.IsPrivate.ToString());

                    //if (s.Group != null) {
                    //    col.Add("Group.Id", s.Group.Id.ToString());
                    //    col.Add("Group.Name", s.Group.Name);
                    //}

                    col.Add("Context", s.Context.ToString());
                    col.Add("SearchAustralia", (s.Country == Countries.Australia).ToString());
                    col.Add("SearchNewZealand", (s.Country == Countries.NewZealand).ToString());
                    col.Add("SearchRestOfWorld", (s.Country == Countries.AllCountries).ToString());
                    col.Add("Locations", s.Locations);
                    col.Add("Positions", s.Positions);
                    col.Add("MediaTypes", s.MediaTypes);
                    col.Add("Subjects", s.Subjects);
                    col.Add("SearchText", s.SearchText);
                }

                col.Add(more);
            }
            catch (Exception) { }

            return col;
        }

        private List<LocationModel> InitLocationList(List<Medianet.Contacts.Wcf.DataContracts.DTO.Location> locList)
        {
            List<LocationModel> result = new List<LocationModel>();

            foreach (Location l in locList)
            {
                if (l.RegionalFocusList != null)
                {
                    foreach (string rf in l.RegionalFocusList)
                    {
                        LocationModel loc = new LocationModel();
                        loc.Name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(rf);
                        loc.Id = l.State + '/' + loc.Name;
                        loc.ParentId = l.State;
                        result.Add(loc);
                    }

                    //check we got all the kids for this parent? The number of kids should be == 3 (regional, suburban and metro)
                    if (l.RegionalFocusList.Count() == 3)
                    {
                        //add node for parent;
                        result.Add(new LocationModel { Name = l.State, Id = l.State, ParentId = null });
                    }
                }
                else
                {
                    LocationModel loc = new LocationModel();
                    loc.Name = l.State;
                    loc.Id = loc.Name;
                    loc.ParentId = null;
                    result.Add(loc);
                }
            }

            return result;
        }

        #endregion
    }
}
