﻿using System;
using System.Configuration;
using System.Data;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.Filters;

namespace Medianet.Contacts.Web.Controllers
{
    public class ContactUsController : BaseController
    {
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index()
        {
            return View(new ContactUs { Submitted = false });
        }

        [HttpPost]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(ContactUs contactUs)
        {
            if (ModelState.IsValid)
                Submit(ref contactUs);

            return View(contactUs);
        }

        private void Submit(ref ContactUs contactUs)
        {
            try
            {
                contactUs.Submitted = false;

                using (DBClass db = new DBClass())
                {
                    GeneralContactUs newContact = new GeneralContactUs
                    {
                        CreatedDate = DateTime.Now,
                        Status = 1,
                        Subject = Server.HtmlEncode(contactUs.Subject),
                        Comment = Server.HtmlEncode(contactUs.Comment),
                        IpAddress = Request.ServerVariables["REMOTE_ADDR"],
                        PhoneNo = Server.HtmlEncode(ConfigurationManager.AppSettings["ClientServicePhoneNo"]),
                        FirstName = Server.HtmlEncode(CurrentUser.FullName),
                        Email = Server.HtmlEncode(CurrentUser.EmailAddress),
                        ClientServiceEmail = Server.HtmlEncode(ConfigurationManager.AppSettings["ClientServiceEmail"]),
                        LogonName = CurrentUser.FullName,
                        CompanyName = CurrentUser.CompanyName ?? string.Empty
                    };

                    string fullpath = Request.ServerVariables["HTTP_REFERER"];

                    if (!string.IsNullOrEmpty(fullpath))
                        newContact.Referer = fullpath;

                    new GeneralContactUsBAL(db).Insert(newContact);

                    new EmailContactUs().mail_individual(newContact);
                    contactUs.Submitted = true;
                }
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(ContactUsController).FullName, "Submit[POST]", null);
                ModelState.AddModelError(string.Empty, "An error has occured. Unable to send contact information.");
            }
        }

    }
}
