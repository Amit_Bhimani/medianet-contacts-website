﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.Services.Document;

namespace Medianet.Contacts.Web.Controllers
{
    public class DocumentController : BaseController
    {
        #region Fields

        IDocumentService _documentService;

        #endregion

        #region Constructors

        public DocumentController()
        {
            _documentService = DocumentService.CreateDocumentService();
        }

        #endregion

        #region Action methods

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Index(DocumentViewModel model)
        {
            InitRequest();
            
            if (model.DocumentId > 0)
                model = _documentService.GetById(model.DocumentId, CurrentUser);

            ModelState.Clear();
            
            return PartialView("~/Views/Document/_UploadFormPartial.cshtml", model);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult GetDocuments(DocumentViewModel model)
        {
            var documents = _documentService.GetDocuments(model.OutletId, model.ContactId, model.RecordType, CurrentUser);

            return PartialView("~/Views/Document/_DocumentGridPartial.cshtml", documents);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult Upload(DocumentViewModel model)
        {
            InitRequest();

            if (!ModelState.IsValid)
                return Json(new { ErrorMessage = ModelErrorMessages, Success = false });

            if (model.DocumentId > 0 && !_documentService.IsAuthorized(model.IsPrivate, model.UploadedByUserId, CurrentUser))
                return Json(new { Success = false, ErrorMessage = "Not authorized to update document" });

            _documentService.Save(model, CurrentUser);

            return Json(new { Success = true });
        }

        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Download(int documentId)
        {
            try
            {
                InitRequest();

                DocumentViewModel document = _documentService.GetDocumentFile(documentId, CurrentUser);

                return File(document.DocumentContent, MimeTypes.GetContentType(document.DocumentType), $"{document.Name}{document.DocumentType}");
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, status = 400 });
            }
        }

        [HttpDelete]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult Delete(int id)
        {
            InitRequest();

            DocumentViewModel model = _documentService.GetById(id, CurrentUser);

            if (!_documentService.IsAuthorized(model.IsPrivate, model.UploadedByUserId, CurrentUser))
                return Json(new { Success = false, ErrorMessage = "Not authorized to update document" });

            _documentService.Delete(id, CurrentUser);

            return Json(new { Success = true });
        }

        #endregion

        #region Private methods

        private void InitRequest()
        {
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;
        }

        #endregion
    }
}
