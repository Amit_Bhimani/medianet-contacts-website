﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using Medianet.Contacts.Reporting;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Helpers;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.Services.List;
using Medianet.Contacts.Web.Services.User;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.List;
using Medianet.Contacts.Web.MediaContactsService;

using DBClass = Medianet.Contacts.Web.Common.DBClass;
using Medianet.Contacts.Web.Services;
using System.Net;
using System.Configuration;
using Medianet.Contacts.Web.Common.BO;

//using PaginationViewModel = MediaPeople.ViewModel.PaginationViewModel;


namespace Medianet.Contacts.Web.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None)]
    public class ListController : BaseController
    {
        #region Field

        private IGroupService _groupService;
        private IListService _listService;
        private IUserService _userService;

        public static readonly string ErrorMessage =
            "An error has occured. Unable to create/edit list.";

        public static readonly string ExportWarningFormatString =
            $"Please note, exports are limited to {{0}} rows. If you have any enquiries, please contact the Medianet Team on {ConfigurationManager.AppSettings["ClientServicePhoneNo"]} or {ConfigurationManager.AppSettings["ClientServiceEmail"]}";

        #endregion

        public ListController() : base()
        {
            _groupService = GroupService.CreateGroupService();
            _listService = ListService.CreateListService();
            _userService = UserService.CreateUserService();
        }

        protected override void Dispose(bool disposing)
        {
            if (_groupService != null) _groupService.Dispose();
            if (_listService != null) _listService.Dispose();
            base.Dispose(disposing);
        }

        #region Display List

        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(int ListId = 0, int ListSetId = 0)
        {
            if (ListId > 0)
            {
                try
                {
                    //@ We are loading new list so we need to clean checked/unchecked records from the previous list.
                    ClearListItemsChecked();
                    ClearListItemsUnchecked();

                    var myListModel = new ListViewModel();

                    myListModel.Name = GetListName(ListId);
                    myListModel.ListResults = _listService.GetListItems(ListId, ListSetId, CurrentUser.UserID, CurrentUser.SessionKey);
                    myListModel.ListResults.Pagination.CalculatePagination();
                    myListModel.ListResults.Pagination.CurrentPageOnly = true;
                    myListModel.ListVersions = _listService.GetListVersions(ListId, ListSetId,
                                                                            CurrentUser.SessionKey);
                    myListModel.LivingList = GetLivingList(ListId, ListSetId);
                    myListModel.ListId = ListId;
                    myListModel.ListSet = myListModel.ListVersions.SelectedListSetId;
                    ViewBag.ListSetId = ListSetId;
                    ViewBag.CanExportContacts = CurrentUser.CanExportContacts;

                    return View("List", myListModel);
                }
                catch (FaultException<MediaContactsService.AccessDeniedFault>)
                {
                    ViewBag.Message = "You are not authorised to view the list";
                }
            }

            return View("List");
        }

        private List<ListContactReplacementViewModel> GetLivingList(int pListId, int pListSetId)
        {
            var myReplacements = _listService.GetLivingListContactReplacements(CurrentUser.SessionKey, CurrentUser.UserID, pListId, 100);
            return myReplacements;
        }

        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Restore(int listId = 0, int listSetId = 0)
        {
            _listService.RestoreListSet(listId, listSetId, CurrentUser.UserID, CurrentUser.SessionKey);
            return RedirectToAction("Index", new { ListId = listId });
        }

        #region List Results

        //private ListResultsViewModel GetListItems(int listId, int listSetId) {
        //    ListResultsViewModel listResult = _listService.GetListItems(listId, listSetId, CurrentUser.UserID);

        //    // Calculate pagination.
        //    listResult.Pagination.CalculatePagination();

        //    return listResult;
        //}

        #endregion

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult RebindLivingList(int listId, int listSetId)
        {
            List<ListContactReplacementViewModel> m = GetLivingList(listId, listSetId);
            return PartialView("_LivingListPartial", m);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult RebindListVersions(int listId, int listSetId)
        {
            ListVersionViewModel m = _listService.GetListVersions(listId, listSetId, CurrentUser.SessionKey);
            return PartialView("_ListVersionPartial", m);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Rebind(PaginationListViewModel model)
        {
            // Calculate pagination.
            model.CalculatePagination();

            // Get the current page.
            List<ListRecord> results = GetCurrentPagedSearch(model);
            model.Results = results;
            //@ ??? Set current page to true for first load.
            //@ model.CurrentPageOnly = true;

            if (model.ResultCount > ((model.Page - 1) * model.PageSize))
            {
                ; //@ ok
            }
            else
            {
                //@ may be we have deleted items and we need to go to the last page
                model.Page = 1 + (model.ResultCount / model.PageSize);
                results = GetCurrentPagedSearch(model);
                model.Results = results;
                model.CalculatePagination();
            }

            // This prevents the form data from persisting.
            ModelState.Clear();

            var s = new ListResultsViewModel
            {
                Pagination = model,
            };

            return PartialView("_ListPagePartial", s);
        }

        #endregion

        #region AJAX

        /// <summary>
        /// Used for AJAX requests. Gets lists by group ID.
        /// </summary>
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult GetLists(int id)
        {
            if (IsUserLoggedIn())
            {
                List<ListViewModel> lists = _listService.GetListsByGroupId(id, CurrentUser.UserID);
                ViewData["Lists"] = lists != null && lists.Count > 0 ? new SelectList(lists, "ListId", "Name") : null;
            }

            return PartialView("_ListPartial");
        }

        #endregion

        #region Add to List

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Create()
        {
            ListViewModel l = new ListViewModel();

            Init(l);
            
            return PartialView("_AddListPartial", l);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        [RequiresSession]
        public ActionResult Create(ListViewModel model)
        {
            List<RecordIdentifier> recordsToAdd = null;
            var records = new List<string>();

            // From a contact page.
            if (model.Type == PopupType.Contact)
                recordsToAdd = CSVPsToRecordIdentifiers(model.CheckedItems);
            else
                recordsToAdd = GetSelectedRecords(model,
                                                  GetCurrentSearchResults(model.Expand, Medianet.Contacts.Wcf.DataContracts.DTO.SortColumn.Default,
                                                                          Medianet.Contacts.Wcf.DataContracts.DTO.SortDirection.ASC));
            recordsToAdd.ForEach(delegate (RecordIdentifier record)
                {
                    records.Add(record.ToString());
                });

            if (!records.Any())
                return new HttpStatusCodeResult(400, "Please select one or more records to add to the list");

            if (records.Count > ListsAndTasksMaxRows)
                return new HttpStatusCodeResult(400,
                    String.Format("Too many results. The maximum number of records is {0}. Please refine your search.", ListsAndTasksMaxRows));

            // Existing list so can assume that group ID will be present.
            if (string.IsNullOrEmpty(model.Name) && model.ListId != 0)
            {
                int existingCount = _listService.CoutListRecords(model.ListId, CurrentUser.UserID);

                if ((records.Count + existingCount) > ListsAndTasksMaxRows)
                    return new HttpStatusCodeResult(400,
                        String.Format("Too many results. The maximum number of records is {0}. Please refine your search.", ListsAndTasksMaxRows));

                _listService.AddToList(model.ListId, CurrentUser.UserID, records, true, CurrentUser.SessionKey);

                return Json(new { Url = Url.Action("Index", "List", new { listId = model.ListId }) }, JsonRequestBehavior.AllowGet);
            }

            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed);

            // Existing group. Create a new list in this group.
            if (!string.IsNullOrEmpty(model.Group.Name))
            {
                var g = new GroupViewModel
                {
                    Name = model.Group.Name,
                    UserId = CurrentUser.UserID,
                    Type = GroupType.List
                };

                model.Group.Id = _groupService.CreateGroup(g, CurrentUser.SessionKey);
            }
            // Have a group ID now. Create list.
            model.ListId = _listService.CreateList(model, CurrentUser.UserID);
            _listService.AddToList(model.ListId, CurrentUser.UserID, records, false, CurrentUser.SessionKey);

            return Json(new { Url = Url.Action("Index", "List", new { listId = model.ListId }) }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Popups

        #region Reports

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult ChaseList()
        {
            if (!CurrentUser.CanExportContacts)
                throw new AccessViolationException("Access to chase lists denied.");

            int maxRows = MaxRowsToExport();
            if (maxRows > 0)
                ViewBag.ExportLimitWarning = string.Format(ExportWarningFormatString, maxRows.ToString("N0"));
            else
                ViewBag.ExportLimitWarning = "";

            return PartialView("_ListChasePartial");
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult MailingLabels()
        {
            return PartialView("_ListMailingLabelsPartial");
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult TelerikListReport(int listId, int listSetId, string exportType, int reportType, int checkboxState, string checkedItems, string uncheckedItems)
        {
            BaseReport report = null;
            switch (reportType)
            {
                case 0:
                    if (!CurrentUser.CanExportContacts)
                        throw new AccessViolationException("Access to chase lists denied.");

                    report = new ChaseList();
                    break;
                case 1:
                    report = new AveryL7159();
                    break;
                case 2:
                    report = new AveryDL30();
                    break;
                default:
                    throw new ArgumentException("Unknown reportType " + reportType.ToString());
            }

            var fileName = "ListReport_" + listId.ToString() + "." + exportType.ToLower();
            var contentType = MimeExtensionHelper.GetMimeType(fileName);
            var processor = new Telerik.Reporting.Processing.ReportProcessor();
            string[] myCheckedItems = GetItemsMergedDistinctChecked(checkedItems, uncheckedItems, ListCheckedItems);
            string[] myUncheckedItems = GetItemsMergedDistinctUnchecked(checkedItems, uncheckedItems, ListUncheckedItems);

            report.ReportParameters["list"].Value = listId;
            report.ReportParameters["Listset"].Value = listSetId;
            report.ReportParameters["UserID"].Value = CurrentUser.UserID;
            report.ReportParameters["CheckboxState"].Value = checkboxState;
            report.ReportParameters["CheckedItems"].Value = string.Join(",", myCheckedItems); // (checkedItems + checkboxState) will later [in GetMailingLabelData method] make all the data necessary
            report.ReportParameters["UncheckedItems"].Value = string.Join(",", myUncheckedItems); // uncheckedItems + checkboxState) will later [in GetMailingLabelData method] make all the data necessary
            report.ReportParameters["MaxRowsToDisplay"].Value = MaxRowsToExport();

            var result = processor.RenderReport(exportType, report, null);

            return File(result.DocumentBytes, contentType, fileName);
        }

        #endregion

        #region Merge Lists

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Merge(int id)
        {
            var model = new ListMergeViewModel();

            model.Name = GetListName(id);
            Init(model);
            model.ListId = id;
            model.ListsGrouped = _listService.GetListsByGroup(id, CurrentUser.UserID, new List<int>(), CurrentUser.SessionKey);

            return PartialView("_ListMergePartial", model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        [RequiresSession]
        public ActionResult Merge(ListMergeViewModel model)
        {
            Init(model);

            if (model.GroupState == GroupState.New && string.IsNullOrWhiteSpace(model.Group.Name))
            {
                ModelState.AddModelError("Group.Name", "Please either enter a new group name or select an existing one");
            }

            if (ModelState.IsValid)
            {
                // Create a new group if required.
                if (model.GroupState == GroupState.New)
                {
                    model.Group.UserId = CurrentUser.UserID;
                    model.Group.Type = GroupType.List;
                    model.Group.Id = _groupService.CreateGroup(model.Group, CurrentUser.SessionKey);
                }

                model.Name = model.NewListName;

                // Now create the new list and merge.
                int newListId = Insert(model, CurrentUser.UserID);

                // Add the current list to the merge list.
                model.ListsToMergeWith.Add(model.ListId);

                //@ Second - merge the selected lists
                _listService.MergeList(newListId, model.ListsToMergeWith, CurrentUser.UserID);

                model.NewListId = newListId;
            }
            else
            {
                model.ListsGrouped = _listService.GetListsByGroup(model.ListId, CurrentUser.UserID, new List<int>(), CurrentUser.SessionKey);
                HttpContext.Response.StatusCode = 400;
            }

            return PartialView("_ListMergePartial", model);
        }

        #endregion

        #region Rename List

        [HttpGet]
        [HandleAjaxError, HandleAjaxAuthorization]
        public ActionResult Edit(int listId)
        {
            ListViewModel model = _listService.GetListById(listId, CurrentUser.SessionKey);
            return PartialView("_EditListPartial", model);
        }

        [HttpPost]
        [HandleAjaxError, HandleAjaxAuthorization]
        public ActionResult Edit(ListViewModel m)
        {
            if (m != null && m.ListId != 0 && !string.IsNullOrWhiteSpace(m.Name))
            {
                _listService.UpdateList(m.ListId, m.Name, m.IsPrivate, CurrentUser.UserID);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return Json(new { Url = Url.Action("Index", "List", new { listId = m.ListId }) }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Living Lists

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult RejectReplacement(ListContactReplacementViewModel myListContactReplacement)
        {
            _listService.LivingListReject(CurrentUser.UserID, myListContactReplacement.RecordId, CurrentUser.SessionKey);

            return Json(new { });
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult AcceptReplacement(ListContactReplacementViewModel myListContactReplacement)
        {
            _listService.LivingListAccept(CurrentUser.UserID, myListContactReplacement.RecordId, CurrentUser.SessionKey);

            return Json(new { });
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult ChooseAnotherContact(long id)
        {
            var l = new LivingListChooseAnotherContactModel();

            l.RecordId = (int)id;
            if (CurrentUser.UserID != 0 && l.RecordId != 0)
            {
                l.Contacts = new SelectList(_listService.GetLivingListOutletContactsByRecordId(l.RecordId, 500, CurrentUser.SessionKey), "Key", "Value");
            }
            else
            {
                throw new Exception(CurrentUser.UserID == 0 ? "UserId is not provided." : l.RecordId == 0 ? "RecordId should be valid." : "");
            }

            return PartialView("_ListChooseAnotherContactPartial", l);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult ChooseAnotherContact(LivingListChooseAnotherContactModel l)
        {
            if (CurrentUser.UserID != 0 && l.RecordId != 0)
            {
                _listService.LivingListReplaceContact(CurrentUser.UserID, l.RecordId, l.ContactId, CurrentUser.SessionKey);
            }
            else
            {
                throw new Exception(CurrentUser.UserID == 0 ? "UserId is not provided." : l.RecordId == 0 ? "RecordId should be valid." : "");
            }

            return PartialView("_ListChooseAnotherContactPartial", l);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult AcceptAll(int listId, int listSetId)
        {
            _listService.LivingListAcceptRejectAll(CurrentUser.UserID, listId, CurrentUser.SessionKey);           

            //var m = GetLivingList(listId, listSetId);

            return Json(new {});
        }

        #endregion

        #region Duplicate List

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Duplicate(int id)
        {
            var model = new ListDuplicateViewModel();

            Init(model);
            model.ListId = id;

            return PartialView("_ListDuplicatePartial", model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Duplicate(ListDuplicateViewModel model)
        {
            Init(model);

            if (model.GroupState == GroupState.New && string.IsNullOrWhiteSpace(model.Group.Name))
            {
                ModelState.AddModelError("Group.Name", "Please either enter a new group name or select an existing one");
            }

            if (ModelState.IsValid)
            {
                // Create a new group if required.
                if (model.GroupState == GroupState.New)
                {
                    model.Group.UserId = CurrentUser.UserID;
                    model.Group.Type = GroupType.List;
                    model.Group.Id = _groupService.CreateGroup(model.Group, CurrentUser.SessionKey);
                }

                model.Name = model.NewListName;

                // Now create the new list and merge.
                int newListId = Insert(model, CurrentUser.UserID);

                _listService.MergeList(newListId, new List<int>() { model.ListId }, CurrentUser.UserID);

                model.NewListId = newListId;
            }
            else
            {
                HttpContext.Response.StatusCode = 400;
            }

            return PartialView("_ListDuplicatePartial", model);
        }

        public static int Insert(ListViewModel l, int pUserId)
        {
            int id = 0;
            var list = new List
            {
                BelongsToGroup = new GroupBase { Id = l.Group.Id },
                UserId = pUserId,
                Name = l.Name,
                IsPrivate = l.IsPrivate
            };

            using (var db = new DBClass())
            {
                var bal = new ListBAL(db);
                id = bal.Insert(list);
            }

            return id;
        }

        #endregion

        #region Delete Items

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult Delete(ListDeleteItemsViewModel model)
        {
            string[] myCheckedItems = GetItemsMergedDistinctChecked(model, ListCheckedItems);
            string[] myUncheckedItems = GetItemsMergedDistinctUnchecked(model, ListUncheckedItems);
            List<int> myItemsToDelete = null;

            if (model.Type == DeleteAction.DeleteOne)
            {
                myItemsToDelete = myCheckedItems.Select(c => int.Parse(myCheckedItems.First())).ToList();
            }
            else //@ DeleteAction.DeleteSelected
            {
                var m = _listService.GetListItemsAll(model.ListId, model.ListSetId, CurrentUser.UserID, CurrentUser.SessionKey);

                if (model.CheckboxState == CheckboxState.All)
                {
                    myCheckedItems = m.Pagination.Results.Select(r => r.ToClientString()).ToArray();
                }

                if (model.CheckboxState == CheckboxState.PartAll)
                {
                    myCheckedItems = m.Pagination.Results.Select(r => r.ToClientString()).Except(myUncheckedItems).ToArray();
                }

                myItemsToDelete = m.Pagination.Results.Where(r => myCheckedItems.Contains(r.ToClientString())).Select(s => s.RecordId).ToList();
            }

            _listService.DeleteItemsFromList(model.ListId, myItemsToDelete, CurrentUser.SessionKey);

            //@ Clean checked/unchecked records after deletion
            ClearListItemsUnchecked();
            ClearListItemsChecked();

            return Json(new { });
        }

        #endregion

        #region Export List

        /// <summary>
        /// Display the List Export popup.
        /// </summary>
        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Export(MediaContactsExportType exportType)
        {
            var exportView = new ListExportViewModel();

            exportView.ExportType = exportType;
            exportView.ExportProfiles = new SelectList(new List<SelectListItem>());
            //exportViews = GetAllExportProfiles(exportType);


            if (!CurrentUser.CanExportContacts)
                throw new AccessViolationException("Access to export denied.");

            int maxRows = MaxRowsToExport();
            if (maxRows > 0)
                ViewBag.ExportLimitWarning = string.Format(ExportWarningFormatString, maxRows.ToString("N0"));
            else
                ViewBag.ExportLimitWarning = "";

            return PartialView("_AddExportListPartial", exportView);
        }

        /// <summary>
        /// Gets a csv export of the list, returning only the columns requested.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Export(int listId, int taskId, string exportFields, int checkboxState, string checkedItems, string uncheckedItems, MediaContactsExportType exportType)
        {
            string csvContent = string.Empty;
            string fileName = "Export_List.csv";

            if (!CurrentUser.CanExportContacts)
                throw new AccessViolationException("Access to export denied.");

            if (exportType == MediaContactsExportType.Tasks)
            {
                fileName = "Export_Task.csv";
            }

            string contentType = MimeExtensionHelper.GetMimeType(fileName);
            int pos = 0, newPos = 0;

            if (IsUserLoggedIn())
            {
                string[] myCheckedItems = { };
                string[] myUncheckedItems = { };
                List<string> columnList = exportFields.ParseStringCSV();
                int originalRowCount;
                DataTable dtexport = new DataTable();

                if (exportType == MediaContactsExportType.Lists)
                {
                    dtexport = _listService.ExportList(listId, CurrentUser.UserID);
                    myCheckedItems = GetItemsMergedDistinctChecked(checkedItems, uncheckedItems, ListCheckedItems);
                    myUncheckedItems = GetItemsMergedDistinctUnchecked(checkedItems, uncheckedItems, ListUncheckedItems);
                }
                else
                {
                    myCheckedItems = GetItemsMergedDistinctChecked(checkedItems, uncheckedItems, TaskCheckedItems);
                    myUncheckedItems = GetItemsMergedDistinctUnchecked(checkedItems, uncheckedItems, TaskUncheckedItems);
                }

                // Remove the unchecked items.
                dtexport = RemoveUncheckedItems(dtexport, (CheckboxState)checkboxState, myCheckedItems.ToList(), myUncheckedItems.ToList());
                originalRowCount = dtexport.Rows.Count;

                // Limit to the max number of rows allowable.
                dtexport = LimitToMaxRows(dtexport);


                // Remove the columns not wanted.
                for (int i = dtexport.Columns.Count - 1; i >= 0; i--)
                {
                    if (!columnList.Contains(dtexport.Columns[i].ColumnName))
                        dtexport.Columns.RemoveAt(i);
                }

                // Re-order the columns into the order specified.
                for (int i = 0; i < columnList.Count; i++)
                {
                    pos = dtexport.Columns.IndexOf(columnList[i]);
                    if (pos >= 0)
                    {
                        dtexport.Columns[pos].SetOrdinal(newPos++);
                    }
                }

                csvContent = dtexport.ConvertToCSV();

                // If we truncated the number of rows let them know.
                if (originalRowCount > dtexport.Rows.Count)
                {
                    csvContent = csvContent +
                                 string.Format("\r\n\"Copyright © Medianet. {0}\"{1}",
                                               string.Format(ExportWarningFormatString,
                                                             dtexport.Rows.Count.ToString("N0")),
                                               new string(',', columnList.Count - 1));
                }

                //log the export
                using (var wc = new MediaContactsServiceProxy())
                {
                    var exportedFields = dtexport.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();

                    wc.LogExport(new MediaContactExportLog()
                    {
                        UserId = CurrentUser.UserID,
                        DebtorNumber = CurrentUser.DebtorNumber,
                        CreatedDate = DateTime.Now,
                        NoOfExportedRows = dtexport.Rows.Count,
                        ExportedFields = string.Join(",", exportFields),
                        ListId = exportType == MediaContactsService.MediaContactsExportType.Lists ? listId : 0,
                        TaskId = exportType == MediaContactsService.MediaContactsExportType.Tasks ? taskId : 0,
                    }, CurrentUser.SessionKey);
                }
            }
            var enc = Encoding.GetEncoding(1252);
            return File(Encoding.Convert(Encoding.Unicode, enc, Encoding.Unicode.GetBytes(csvContent)), contentType, fileName);
        }

        private DataTable RemoveUncheckedItems(DataTable dt, CheckboxState pCheckboxState, List<string> pCheckedItems, List<string> pUncheckedItems)
        {
            if (pCheckboxState > 0)
            {
                if (pCheckboxState == CheckboxState.All)
                {
                    //@ Do no filtering - all data is required
                }
                else if (pCheckboxState == CheckboxState.None)
                {
                    //@ Do nothing, return all data - this should be handled on front end by javascript
                }
                else if ((pCheckboxState == CheckboxState.PartNone) || (pCheckboxState == CheckboxState.PartAll))
                {
                    DataTable t2 = dt.Clone();

                    //@ Consider only CheckedItems
                    foreach (DataRow r in dt.Rows)
                    {
                        string myId = r["OutletId"].ToString().Trim() + "#" + ((r["ContactId"].ToString().Trim() == "") ? "0" : r["ContactId"].ToString().Trim()) + "#" + r["RecordType"].ToString().Trim();

                        //@ Some selected
                        if ((pCheckboxState == CheckboxState.PartNone) && (pCheckedItems.Contains(myId)))
                        {
                            t2.ImportRow(r);
                        }

                        //@ Some unselected
                        if ((pCheckboxState == CheckboxState.PartAll) && (!pUncheckedItems.Contains(myId)))
                        {
                            t2.ImportRow(r);
                        }
                    }

                    dt = t2;
                }
            }

            return dt;
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxError, HandleAjaxAuthorization]
        public ActionResult GetExportProfiles(MediaContactsExportType exportType)
        {
            List<ExportProfileViewModel> exportViews;

            if (IsUserLoggedIn())
                exportViews = GetAllExportProfiles(exportType);
            else
                exportViews = new List<ExportProfileViewModel>();

            return PartialView("_ExportProfilePartial", exportViews);
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult GetExportProfile(int id)
        {
            ExportProfileViewModel exportView = null;

            if (IsUserLoggedIn())
                exportView = _listService.GetExportProfile(id, CurrentUser.SessionKey);

            return this.Json(exportView, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult SaveExportProfile(int id, string name, bool isPrivate, string[] exportFields, MediaContactsExportType exportType)
        {
            //List<ExportProfileViewModel> exportViews;

            if (IsUserLoggedIn())
            {
                var exportView = new ExportProfileViewModel
                {
                    Id = id,
                    Name = name,
                    IsPrivate = isPrivate,
                    DebtorNumber = CurrentUser.DebtorNumber,
                    UserId = CurrentUser.UserID,
                    ExportFields = string.Join(",", exportFields),
                    ExportType = exportType
                };

                id = _listService.SaveExportProfile(exportView, CurrentUser.SessionKey);

                //// Fetch all the latest export profiles to display.
                //exportViews = _listService.GetExportProfiles(CurrentUser.DebtorNumber, CurrentUser.UserID, CurrentUser.SessionKey);

                //// Select the currently saved item.
                //var item = exportViews.Where(e => e.Id == id).FirstOrDefault();
                //if (item != null) item.IsSelected = true;
            }
            //else
            //exportViews = new List<ExportProfileViewModel>();

            //return PartialView("_ExportProfilePartial", exportViews);
            return this.Json(new { id = id }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult DeleteExportProfile(int id)
        {
            if (IsUserLoggedIn())
                _listService.DeleteExportProfile(id, CurrentUser.SessionKey);

            return this.Json(new { id = id }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Private Methods

        #region Populate Labels

        /// <summary>
        /// This Function populate the list name in the header label
        /// </summary>
        private string GetListName(int pListId = 0)
        {
            if (pListId == 0)
            {
                int.TryParse(Request.QueryString["listid"], out pListId);
            }

            if (pListId != 0)
            {
                var groupList = _listService.GetListById(pListId, CurrentUser.SessionKey);

                //@ is it reliable thing to do?
                Session["ListId"] = Convert.ToString(Request.QueryString["listid"]);

                return groupList.Name;
            }

            return string.Empty;
        }

        #endregion

        /// <summary>
        /// Gets the current paged search.
        /// </summary>
        /// <param name="p">The pagination view model.</param>
        /// <returns>List of search results.</returns>
        private List<ListRecord> GetCurrentPagedSearch(PaginationListViewModel p)
        {
            List<ListRecord> results = null;

            int myTotalRecords;
            results = _listService.GetByGroupListsListIdAndListSet(p.ListId, p.ListSet, CurrentUser.UserID, CurrentUser.SessionKey, out myTotalRecords, p.Page, p.PageSize, p.SortColumn, p.SortDirection);


            //@ Unselect all records if main checkbox is all or none
            if ((p.CheckboxState == CheckboxState.All) || (p.CheckboxState == CheckboxState.None))
            {
                ClearListItemsUnchecked();
                ClearListItemsChecked();
                p.CurrentPageOnly = true;
            }

            //@ Unselect all records if main checkbox is unchecked
            if (p.CheckboxState == CheckboxState.None)
            {
                results.ForEach(r => r.Selected = false);
            }

            //@ PartNone - some selected.
            if (p.CheckboxState == CheckboxState.PartNone)
            {
                if (p.CurrentPageOnly)
                    ListCheckedItems = "";
                //@ Join stored checked records and the once we just checked
                string[] myCheckedItems = GetItemsMergedDistinctChecked(p, ListCheckedItems);
                results.ForEach(r => r.Selected = myCheckedItems.Contains(r.ToClientString()));
                int myCheckedItemsOnThisPageOnlyCount = results.Count(r => r.Selected == true);
                p.CurrentPageOnly = (myCheckedItems.Length == myCheckedItemsOnThisPageOnlyCount);
                ListCheckedItems = string.Join(",", myCheckedItems);
            }

            //@ PartAll - some unselected.
            if (p.CheckboxState == CheckboxState.PartAll)
            {
                if (p.CurrentPageOnly)
                    ListUncheckedItems = "";
                //@ Join stored unchecked records and the once we just unchecked
                string[] myUncheckedItems = GetItemsMergedDistinctUnchecked(p, ListUncheckedItems);
                results.ForEach(r => r.Selected = !myUncheckedItems.Contains(r.ToClientString()));
                int myUncheckedItemsOnThisPageOnlyCount = results.Count(r => r.Selected == false);
                p.CurrentPageOnly = (myUncheckedItems.Length == myUncheckedItemsOnThisPageOnlyCount);
                ListUncheckedItems = string.Join(",", myUncheckedItems);
            }

            p.ResultCount = myTotalRecords;


            return results;
        }

        /// <summary>
        /// Get existing groups and store in ViewData.
        /// </summary>
        private void Init(ListViewModel l)
        {
            l.Groups = new SelectList(_groupService.GetGroupsByType(GroupType.List, CurrentUser.UserID, CurrentUser.SessionKey), "Id", "Name",
                                      l.Group);
            l.Lists = new SelectList(_listService.GetListsByGroupId(l.Group.Id, CurrentUser.UserID), "ListId", "Name",
                                     l.ListId);
        }

        /// <summary>
        /// Removes all the unchecked items from the session.
        /// </summary>
        protected void ClearListItemsUnchecked()
        {
            ListUncheckedItems = null;
        }

        /// <summary>
        /// Removes all the checked list items from the session.
        /// </summary>
        protected void ClearListItemsChecked()
        {
            ListCheckedItems = null;
        }

        private int MaxRowsToExport()
        {
            int maxRows = 0;

            if (CurrentUser.DebtorNumber != System.Configuration.ConfigurationManager.AppSettings["DebtorNumberAAP"])
                int.TryParse(System.Configuration.ConfigurationManager.AppSettings["ExportMaxRows"], out maxRows);

            return maxRows;
        }

        private DataTable LimitToMaxRows(DataTable table)
        {
            int maxRows = MaxRowsToExport();

            if (maxRows > 0 && maxRows < table.Rows.Count)
                table = table.AsEnumerable().Take(maxRows).CopyToDataTable();

            return table;
        }

        private List<ExportProfileViewModel> GetAllExportProfiles(MediaContactsExportType exportType)
        {
            return _listService.GetExportProfiles(CurrentUser.DebtorNumber, CurrentUser.UserID,
                exportType, CurrentUser.SessionKey).OrderBy(p => p.Name).ToList();
        }

        #endregion
    }
}
