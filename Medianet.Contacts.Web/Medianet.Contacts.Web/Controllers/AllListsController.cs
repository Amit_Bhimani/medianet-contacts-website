﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Medianet.Contacts.Web.Services;
using Medianet.Contacts.Web.ViewModels.AllLists;
using Medianet.Contacts.Web.Filters;
using System.Web.UI;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using System.Linq;
using Medianet.Contacts.Web.Services.List;
using System.Net;

namespace Medianet.Contacts.Web.Controllers
{
    public class AllListsController : BaseController
    {
        #region Field

        private IGroupListService _groupListService;
        private IListService _listService;

        #endregion

        public AllListsController() : base()
        {
            _groupListService = GroupListService.CreateGroupListService();
            _listService = ListService.CreateListService();
        }

        protected override void Dispose(bool disposing)
        {
            _groupListService?.Dispose();
            _listService?.Dispose();
            base.Dispose(disposing);
        }

        #region Display List

        // GET: /AllLists/
        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index()
        {
            List<ListsGroupedViewModel> listGrouped = _groupListService.GetGroupedListsDebtorUserId(CurrentUser.UserID, CurrentUser.DebtorNumber);
            return View(listGrouped);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult RebindAll()
        {
            List<ListsGroupedViewModel> listGrouped = _groupListService.GetGroupedListsDebtorUserId(CurrentUser.UserID, CurrentUser.DebtorNumber);
            return PartialView("_AllGroupedListsPartial", listGrouped);
        }

        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult RebindGroup(RebindGroupViewModel model)
        {
            List<ListViewModel> listGrouped = _groupListService.GetGroupedListsDebtorUserIdGroupId(model.GroupId,
                CurrentUser.UserID, CurrentUser.DebtorNumber);

            ListsGroupedViewModel orderedGroup = new ListsGroupedViewModel()
            {
                GroupId = model.GroupId
            };

            if (listGrouped != null)
            {
                switch (model.SortColumn)
                {
                    case AllListsSortColumn.ListName:
                        orderedGroup.Lists = model.SortDirection == SortDirection.ASC ? listGrouped.OrderBy(o => o.ListName).ToList() : listGrouped.OrderByDescending(o => o.ListName).ToList();
                        break;
                    case AllListsSortColumn.UserFullName:
                        orderedGroup.Lists = model.SortDirection == SortDirection.ASC ? listGrouped.OrderBy(o => o.UserFullName).ToList() : listGrouped.OrderByDescending(o => o.UserFullName).ToList();
                        break;
                    case AllListsSortColumn.CreatedDate:
                        orderedGroup.Lists = model.SortDirection == SortDirection.ASC ? listGrouped.OrderBy(o => o.CreatedDate).ToList() : listGrouped.OrderByDescending(o => o.CreatedDate).ToList();
                        break;
                    case AllListsSortColumn.Default:
                        orderedGroup.Lists = model.SortDirection == SortDirection.ASC ? listGrouped.OrderBy(o => o.ListName).ToList() : listGrouped.OrderByDescending(o => o.ListName).ToList();
                        break;

                }
            }
            return PartialView("_ListsPartial", orderedGroup);

        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult DeleteList(int listId) {

            _groupListService.DeleteOnListIdAndUserId(CurrentUser.UserID, listId);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion
    }
}
