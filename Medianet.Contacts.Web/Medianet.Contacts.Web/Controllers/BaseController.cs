﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.ViewModels.Grid;

namespace Medianet.Contacts.Web.Controllers
{
    public class BaseController : Controller
    {
        #region Properties

        private const int DEFAULT_MAX_ROWS = 5000;

        /// <summary>
        /// Checked items on the list.
        /// </summary>
        public string ListCheckedItems
        {
            get { return Session[SessionManager.SessionListItemsChecked] == null ? "" : Session[SessionManager.SessionListItemsChecked].ToString(); }
            set { Session[SessionManager.SessionListItemsChecked] = value; }
        }

        /// <summary>
        /// Checked items on the task.
        /// </summary>
        public string TaskCheckedItems
        {
            get { return Session[SessionManager.SessionTaskItemsChecked] == null ? "" : Session[SessionManager.SessionTaskItemsChecked].ToString(); }
            set { Session[SessionManager.SessionTaskItemsChecked] = value; }
        }

        public string[] GetItemsMergedDistinctChecked(GridViewModelBase pModel, string pCheckedItemsSession)
        {
            string[] myCheckedItems = GetItemsMergedDistinct(pModel.CheckedItems, pCheckedItemsSession);
            if (pModel.UncheckedItems != null)
                myCheckedItems = myCheckedItems.Except(pModel.UncheckedItems.Split(',')).ToArray();
            return myCheckedItems;
        }

        public string[] GetItemsMergedDistinctChecked(string pCheckedItems, string pUncheckedItems, string pCheckedItemsSession)
        {
            string[] myCheckedItems = GetItemsMergedDistinct(pCheckedItems, pCheckedItemsSession);
            if (pUncheckedItems != null)
                myCheckedItems = myCheckedItems.Except(pUncheckedItems.Split(',')).ToArray();
            return myCheckedItems;
        }

        public string[] GetItemsMergedDistinctUnchecked(GridViewModelBase pModel, string pUncheckedItemsSession)
        {
            string[] myUncheckedItems = GetItemsMergedDistinct(pModel.UncheckedItems, pUncheckedItemsSession);
            if (pModel.CheckedItems != null)
                myUncheckedItems = myUncheckedItems.Except(pModel.CheckedItems.Split(',')).ToArray();
            return myUncheckedItems;
        }

        public string[] GetItemsMergedDistinctUnchecked(string pCheckedItems, string pUncheckedItems, string pUncheckedItemsSession)
        {
            string[] myUncheckedItems = GetItemsMergedDistinct(pUncheckedItems, pUncheckedItemsSession);
            if (pCheckedItems != null)
                myUncheckedItems = myUncheckedItems.Except(pCheckedItems.Split(',')).ToArray();
            return myUncheckedItems;
        }

        private static string[] GetItemsMergedDistinct(string pPart1, string pPart2)
        {
            string[] myItemsPart1 = (pPart1 ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] myItemsPart2 = (pPart2 ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] result = myItemsPart1.Concat(myItemsPart2).ToArray();
            return result.Distinct().ToArray();
        }

        /// <summary>
        /// Unchecked items on the list.
        /// </summary>
        public string ListUncheckedItems
        {
            get { return Session[SessionManager.SessionListItemsUnchecked] == null ? "" : Session[SessionManager.SessionListItemsUnchecked].ToString(); }
            set { Session[SessionManager.SessionListItemsUnchecked] = value; }
        }

        /// <summary>
        /// Unchecked items on the task.
        /// </summary>
        public string TaskUncheckedItems
        {
            get { return Session[SessionManager.SessionTaskItemsUnchecked] == null ? "" : Session[SessionManager.SessionTaskItemsUnchecked].ToString(); }
            set { Session[SessionManager.SessionTaskItemsUnchecked] = value; }
        }

        /// <summary>
        /// The pinned items.
        /// </summary>
        public List<RecordIdentifier> PinnedItems { get; set; }

        /// <summary>
        /// The deleted items.
        /// </summary>
        public List<RecordIdentifier> DeletedItems { get; set; }

        /// <summary>
        /// The checked or unchecked items.
        /// </summary>
        public List<RecordIdentifier> UnselectedItems { get; set; }

        /// <summary>
        /// The current search options.
        /// </summary>
        public SearchOptions SearchOptions { get; set; }

        /// <summary>
        /// Gets the current logged in user.
        /// </summary>
        /// <returns><c>UserMaster</c> object if there is a user logged in, null otherwise.</returns>
        public User CurrentUser
        {
            get
            {
                if (IsUserLoggedIn())
                {
                    return SessionManager.Instance.CurrentUser;
                }
                return null;
            }
        }

        protected int ListsAndTasksMaxRows
        {
            get
            {
                int maxRows;

                if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings["ListsAndTasksMaxRows"], out maxRows))
                    maxRows = DEFAULT_MAX_ROWS;

                return maxRows;
            }
        }

        protected string ModelErrorMessages
        {
            get
            {
                return string.Join(", ", ViewData.ModelState.Values.SelectMany(m => m.Errors).Select(m => m.ErrorMessage));
            }
        }
        
        #endregion

        #region Actions

        [HttpGet]
        public RedirectToRouteResult Logout()
        {
            throw new NotImplementedException();
        }

        public RedirectToRouteResult RedirectSessionExpired()
        {
            Session["Message"] = SessionManager.SessionExpiredMessage;
            return RedirectToAction("Index", "Public");
        }

        [HttpGet]
        public virtual ActionResult Error()
        {
            ViewBag.Message = $"An error has occured. If this continues, please contact Client Services on {ConfigurationManager.AppSettings["ClientServicePhoneNo"]} or at {ConfigurationManager.AppSettings["ClientServiceEmail"]}.";
            return View();
        }

        #endregion

        public BaseController()
        {
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #region Methods

        /// <summary>
        /// Gets the pinned items from the session.
        /// </summary>
        public virtual void GetPinnedItems()
        {
            if (Session[SessionManager.SessionSearchPinnedItems] == null)
                PinnedItems = new List<RecordIdentifier>();
            else
                PinnedItems = Session[SessionManager.SessionSearchPinnedItems] as List<RecordIdentifier>;
        }

        /// <summary>
        /// Get all items that are stored in the session. Do not call this function directly;
        /// instead just attribute the action with RequiresSession attribute.
        /// </summary>
        public virtual void GetSessionItems()
        {
            GetPinnedItems();

            if (Session[SessionManager.SessionSearchDeletedItems] == null)
                DeletedItems = new List<RecordIdentifier>();
            else
                DeletedItems = Session[SessionManager.SessionSearchDeletedItems] as List<RecordIdentifier>;

            if (Session[SessionManager.SessionSearchUnselectedItems] == null)
                UnselectedItems = new List<RecordIdentifier>();
            else
                UnselectedItems = Session[SessionManager.SessionSearchUnselectedItems] as List<RecordIdentifier>;

            if (Session[SessionManager.SessionSearchOptions] != null)
            {
                SearchOptions = Session[SessionManager.SessionSearchOptions] as SearchOptions;
            }
        }

        /// <summary>
        /// Checks to see if the current user is logged in.
        /// </summary>
        /// <returns><c>true</c> if the user is logged in, <c>false</c> otherwise.</returns>
        public bool IsUserLoggedIn()
        {
            return SessionManager.Instance.IsUserLoggedIn();
        }

        /// <summary>
        /// Saves the current state of grid to the session.
        /// </summary>
        public virtual void SaveSessionItems()
        {
            Session[SessionManager.SessionSearchPinnedItems] = PinnedItems;
            Session[SessionManager.SessionSearchDeletedItems] = DeletedItems;
            Session[SessionManager.SessionSearchUnselectedItems] = UnselectedItems;
            Session[SessionManager.SessionSearchOptions] = SearchOptions;
        }

        /// <summary>
        /// Removes all the pinned items from the session.
        /// </summary>
        protected void ClearPinnedItems()
        {
            Session[SessionManager.SessionSearchPinnedItems] = null;
            PinnedItems = new List<RecordIdentifier>();
        }

        /// <summary>
        /// Removes all the deleted items from the session.
        /// </summary>
        protected void ClearDeletedItems()
        {
            Session[SessionManager.SessionSearchDeletedItems] = null;
            DeletedItems = new List<RecordIdentifier>();
        }

        /// <summary>
        /// Removes all checked items from the session.
        /// </summary>
        protected void ClearUnselectedItems()
        {
            Session[SessionManager.SessionSearchUnselectedItems] = null;
            UnselectedItems = new List<RecordIdentifier>();
        }

        /// <summary>
        /// Converts a CVSP to a <c>List&lt;RecordIdentifier&gt;</c>.
        /// </summary>
        /// <param name="s">The CSVP.</param>
        /// <returns>A <c>List&lt;RecordIdentifier&gt;</c>.</returns>
        protected List<RecordIdentifier> CSVPsToRecordIdentifiers(string s)
        {
            List<RecordIdentifier> items = new List<RecordIdentifier>();

            if (!string.IsNullOrWhiteSpace(s))
            {
                string[] records = s.Split(',');

                foreach (string r in records)
                {
                    string[] record = r.Split('#');
                    RecordIdentifier ri = new RecordIdentifier(record[0], record[1], record[2]);
                    items.Add(ri);
                }
            }
            return items;
        }

        /// <summary>
        /// Converts a CVSP to a <c>List&lt;RecordIdentifier&gt;</c>.
        /// </summary>
        /// <param name="s">The CSVP.</param>
        /// <returns>A <c>List&lt;RecordIdentifier&gt;</c>.</returns>
        protected List<int> CSVPsToListOfContactId(string s)
        {
            var items = new List<int>();

            if (!string.IsNullOrWhiteSpace(s))
            {
                string[] records = s.Split(',');

                foreach (string r in records)
                {
                    string[] record = r.Split('#');
                    int myContactId = Convert.ToInt32(record[1]);
                    items.Add(myContactId);
                }
            }
            return items;
        }

        /// <summary>
        /// When a popup posts back to the server we need to update the checked items
        /// and get the items required for the popup to proceed.
        /// </summary>
        /// <param name="m">The popup view model.</param>
        /// <param name="results">The current search results.</param>
        /// <returns>A List of <c>RecordIdentifier</c> objects for the popup.</returns>
        public virtual List<RecordIdentifier> GetSelectedRecords(GridViewModelBase model, List<SearchResult> results)
        {
            GetSessionItems();
            var r = results.Except(DeletedItems).Union(PinnedItems).ToList();
            List<RecordIdentifier> recordsToAdd = null;

            // All checkbox are checked or unchecked. Empty session items.
            if (model.CheckboxState == CheckboxState.All)
            {
                recordsToAdd = r.Union(PinnedItems).ToList<RecordIdentifier>();
                ClearUnselectedItems();
            }
            else
            {
                // If only this page, clear unselected items.
                if (model.CurrentPageOnly)
                {
                    ClearUnselectedItems();

                    if (model.CheckboxState == CheckboxState.PartAll)
                        UnselectedItems = CSVPsToRecordIdentifiers(model.UncheckedItems);
                    else if (model.CheckboxState == CheckboxState.PartNone)
                        UnselectedItems = r.Except(CSVPsToRecordIdentifiers(model.CheckedItems)).ToList();
                }
                else
                {
                    // Add any new unchecked items and remove any now checked items.
                    UnselectedItems = UnselectedItems
                            .Union(CSVPsToRecordIdentifiers(model.UncheckedItems))
                            .Except(CSVPsToRecordIdentifiers(model.CheckedItems)).ToList();
                }

                recordsToAdd = r.Except(UnselectedItems).ToList();
            }
            return recordsToAdd;
        }

        /// <summary>
        /// Returns all the results for the current search.
        /// </summary>
        /// <param name="expand">The expand property. Used to indicate if the current search is expanded or not.</param>
        /// <param name="sortCol">The sort col.</param>
        /// <param name="sortDir">The sort dir.</param>
        /// <returns>
        /// A list of <c>SearchResult</c> objects.
        /// </returns>
        public List<SearchResult> GetCurrentSearchResults(string expand, SortColumn sortCol, SortDirection sortDir, bool newSearch = false)
        {
            List<SearchResult> results = null;
            using (SearchServiceProxy SearchService = new SearchServiceProxy())
            {
                // Single expanded.
                if (!string.IsNullOrWhiteSpace(expand) && expand.Contains('#'))
                {
                    SearchOptionsOutlet so = new SearchOptionsOutlet();
                    int hashPos = expand.IndexOf('#');

                    so.OutletID = expand.Substring(hashPos + 1);
                    so.OutletType = (RecordType)int.Parse(expand.Substring(0, hashPos));

                    results = SearchService.Search(so, sortCol, sortDir, false, CurrentUser.UserID, null, null, PinnedItems, DeletedItems, null);
                }
                else
                {
                    if (SearchOptions != null)
                    {
                        // All expanded.
                        if (!string.IsNullOrWhiteSpace(expand) && expand.Equals("all"))
                            results = SearchService.SearchWithExpandedOutlets(SearchOptions, sortCol, sortDir, CurrentUser.UserID, null, null, PinnedItems, DeletedItems, null);
                        // Normal.
                        else
                            results = SearchService.Search(SearchOptions, sortCol, sortDir, newSearch, CurrentUser.UserID, null, null, PinnedItems, DeletedItems, null);
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Checks if current user is a trial user.
        /// </summary>
        /// <returns>Returns true if the current user is a trial user, otherwise false.</returns>
        public bool IsTrialUser()
        {
            return IsUserLoggedIn() ? ((User)Session["Member"]).isTrial() : false;
        }

        public static string FormatTextAsHtml(string content, string emptyMessage = "")
        {
            var newContent = emptyMessage;

            if (!string.IsNullOrWhiteSpace(content))
            {
                var urlMatch = @"\b(?:(?:https?|ftp|file)://|www\.|ftp\.)[-A-Z0-9+&@#/%=~_|$?!:,.]*[A-Z0-9+&@#/%=~_|$]";
                var emailMatch = @"((?:mailto:)?[A-Z0-9._%+-]+@[A-Z0-9._%-]+\.[A-Z]{2,4})\b";

                newContent = HttpUtility.HtmlEncode(content).Replace(Environment.NewLine, @"<br/>");

                newContent = Regex.Replace(newContent, urlMatch, delegate (Match m)
                {
                    var url = m.ToString();
                    if (!url.StartsWith("http", StringComparison.CurrentCultureIgnoreCase))
                        url = "http://" + url;

                    return string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url, m.ToString());
                }, RegexOptions.IgnoreCase);

                newContent = Regex.Replace(newContent, emailMatch, delegate (Match m)
                {
                    var url = m.ToString();
                    if (!url.StartsWith("mailto", StringComparison.CurrentCultureIgnoreCase))
                        url = "mailto:" + url;

                    return string.Format("<a href=\"{0}\">{1}</a>", url, m.ToString());
                }, RegexOptions.IgnoreCase);
            }

            return newContent;
        }

        public ContentResult FormatOutletTextAsHtml(string content, string emptyMessage = "")
        {
            return Content(FormatTextAsHtml(content));
        }

        //public List<MyRecordTaskGroups> GetMyRecordTaskGroups(ICollection<TaskBase> BelongsToTasks)
        //{
        //    List<MyRecordTaskGroups> allCategories = null;
        //    var myRecordTaskGroup = new List<MyRecordTaskGroups>();

        //    if (BelongsToTasks != null && BelongsToTasks.Any())
        //    {
        //        allCategories = (from TaskBase tb in BelongsToTasks
        //                         where tb.TaskStatus != TaskStatus.Completed
        //                         group tb by tb.DueDate.Date.CompareTo(DateTime.Now.Date)
        //                             into g
        //                             select new MyRecordTaskGroups()
        //                             {
        //                                 Name = IntToCategory(g.Key),
        //                                 Tasks = g.ToList()
        //                             }).ToList();

        //        var completedTasks = new MyRecordTaskGroups() { Name = "Completed", Tasks = BelongsToTasks.Where(rc => rc.TaskStatus == TaskStatus.Completed).ToList() };

        //        if (completedTasks.Tasks.Count > 0)
        //            allCategories.Add(completedTasks);
        //        //myRecord = new MyRecordViewModel();
        //        myRecordTaskGroup = allCategories.OrderBy(n => n.Name).ToList();
        //    }

        //    return myRecordTaskGroup;
        //}

        private string IntToCategory(int ct)
        {
            return ct == 0 ? "Today" : ct > 0 ? "Upcoming" : "Overdue";
        }
        #endregion
    }
}
