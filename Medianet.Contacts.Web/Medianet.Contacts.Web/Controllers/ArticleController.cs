﻿using System;
using System.Linq;
using System.Web.Mvc;
using Medianet.Contacts.Web.NewsCentreArticle;
using Medianet.Contacts.Web.Filters;

namespace Medianet.Contacts.Web.Controllers
{
    public class ArticleController : Controller
    {
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(string articleId)
        {
            Article[] articles = PopulateArticle(articleId);

            return PartialView("_ArticlePopupPartial", articles.FirstOrDefault());
        }

        private Article[] PopulateArticle(string articleId)
        {
            NewsCentreArticle.Search ncSearch = new NewsCentreArticle.Search();
            ncSearch.Url = System.Configuration.ConfigurationManager.AppSettings["NewsCentreArticleFetchURL"];

            int custId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["NewsCentreCustomerID"]);
            ArticleResponse objArticle = ncSearch.GetArticleByID(custId, "", Convert.ToInt32(articleId));
            Article[] obj = objArticle.ArticleManager;

            return obj.Length > 0 ? obj : null;
        }
    }
}
