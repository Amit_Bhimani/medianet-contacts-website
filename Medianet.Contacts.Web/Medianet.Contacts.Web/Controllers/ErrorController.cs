﻿using System.Configuration;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult PageNotFound()
        {
            return View();
        }

        /// <summary>
        /// Page exception
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            ViewBag.Message = $"If this continues, please contact Client Services on {ConfigurationManager.AppSettings["ClientServicePhoneNo"]} or at {ConfigurationManager.AppSettings["ClientServiceEmail"]}.";

            return View("Error");
        }

        public ActionResult HttpError()
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            ViewBag.Message = $"If this continues, please contact Client Services on {ConfigurationManager.AppSettings["ClientServicePhoneNo"]} or at {ConfigurationManager.AppSettings["ClientServiceEmail"]}.";

            return PartialView("Error");
        }
    }

}
