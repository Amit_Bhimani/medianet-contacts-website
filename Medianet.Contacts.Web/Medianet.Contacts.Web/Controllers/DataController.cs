﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Filters;
using LocationModel = Medianet.Contacts.Web.Common.Model.Location;

namespace Medianet.Contacts.Web.Controllers
{
    public class DataController : BaseController
    {
        #region Properties
        private static readonly int DefaultCountryID = 210;
        public SubjectBAL SB { get; set; }
        public MediaTypeBAL MTB { get; set; }
        public PositionBAL PB { get; set; }
        public ElectorateBAL EB { get; set; }

        public const int CacheItemsDuration = 240;

        #endregion

#if DEBUG
        public ContentResult Abandon()
        {
            Session.Abandon();

            return Content("Abandoned...", "text/html", System.Text.Encoding.UTF8);

        }
#endif

        /// <summary>
        /// Returns JSON Object for QuickSearch Locations 
        /// </summary>
        /// <param name="q">Only Locations which contain q are returned</param>
        /// <param name="selected">A CSV of LocationsIds. These Locations are removed from the collection of Locations that is returned</param>
        /// <returns>A JSON array of Locations. Each Location is [Id:int, Name: string].</returns>
        [HandleAjaxError, HandleAjaxAuthorization]
        [OutputCache(Duration = CacheItemsDuration)]
        public JsonResult Locations(string q, string selected)
        {
            using (DBClass db = new DBClass())
            {
                EB = new ElectorateBAL(db);
                List<LocationModel> locations = EB.GetLocations();

                List<string> selectedL = string.IsNullOrWhiteSpace(selected) ? new List<string>() : selected.ParseStringCSV();

                locations.RemoveAll(mt => !string.IsNullOrWhiteSpace(q) ?
                    !mt.Name.ToLower().Contains(q.Trim().ToLower()) || selectedL.Contains(mt.Id) : selectedL.Contains(mt.Id));

                return this.Json(locations, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Returns JSON Object for QuickSearch Positions 
        /// </summary>
        /// <param name="q">Only Positions which contain q are returned</param>
        /// <param name="selected">A CSV of PositionIds. These Positions are removed from the collection of Positions that is returned</param>
        /// <returns>A JSON array of Positions. Each Position is [Id:int, Name: string].</returns>
        [HandleAjaxError, HandleAjaxAuthorization]
        [OutputCache(Duration = CacheItemsDuration)]
        public JsonResult Positions(string q, string selected)
        {
            using (DBClass db = new DBClass())
            {
                PB = new PositionBAL(db);
                List<Position> positions = PB.GetPositions();

                List<int> selectedp = string.IsNullOrWhiteSpace(selected) ? new List<int>() : selected.ParseIntCSV();

                positions.RemoveAll(mt => !string.IsNullOrWhiteSpace(q) ?
                    !mt.Name.ToLower().Contains(q.Trim().ToLower()) || selectedp.Contains(mt.Id) : selectedp.Contains(mt.Id));

                return Json(positions, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Returns JSON Object for QuickSearch MediaTypes 
        /// </summary>
        /// <param name="q">Only MediaTypes which contain q are returned</param>
        /// <param name="selected">A CSV of MediaTypeIds. These MediaTypes are removed from the collection of MediaTypes that is returned</param>
        /// <returns>A JSON array of MediaTypes. Each MediaType is [Id:int, Name: string].</returns>
        [HandleAjaxError, HandleAjaxAuthorization]
        [OutputCache(Duration = CacheItemsDuration)]
        public JsonResult MediaTypes(string q, string selected)
        {
            using (DBClass db = new DBClass())
            {
                MTB = new MediaTypeBAL(db);
                List<MediaType> mediaTypes = MTB.GetMediaTypes();
                List<int> selectedMt = string.IsNullOrWhiteSpace(selected) ? new List<int>() : selected.ParseIntCSV();

                mediaTypes.RemoveAll(mt => !string.IsNullOrWhiteSpace(q) ?
                    !mt.Name.ToLower().Contains(q.Trim().ToLower()) || selectedMt.Contains(mt.Id) : selectedMt.Contains(mt.Id));

                return Json(mediaTypes, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Returns JSON Object for QuickSearch Subjects 
        /// </summary>
        /// <param name="q">Only Subjects which contain q are returned</param>
        /// <param name="selectedSubjects">A CSV of SubjectIds. These Subjects are removed from the collection of Subjects that is returned</param>
        /// <param name="group">A CSV of SubjectsGroupIds. Subjects within these SubjectsGroups are removed from the collection of Subjects that is returned</param>
        /// <returns>A JSON array of Subjects. Each Subject is [Id:int, Name: string].</returns>
        [HandleAjaxError, HandleAjaxAuthorization]
        [OutputCache(Duration = CacheItemsDuration)]
        public JsonResult Subjects(string q, string selectedSubjects, string selectedGroups, string group)
        {
            using (DBClass db = new DBClass())
            {
                SB = new SubjectBAL(db);

                List<Subject> subjects = string.IsNullOrWhiteSpace(group)   //if a group is specified then only show subjects in this group
                    ? string.IsNullOrWhiteSpace(selectedGroups) ? SB.GetSubjects()  //else if a selectedGroups is specified then these subjects are to be filtered out
                    : (from sb1 in SB.GetSubjects()                             // return all subjects where subjects not in the selectedSubjects
                       where !(from sb2 in SB.GetSubjectsInGroups(selectedGroups.ParseStringCSV())
                               select sb2.Id
                          ).Contains(sb1.Id)
                       select sb1).ToList<Subject>()
                    : SB.GetSubjectInGroup(SB.GetSubjectGroups().Where(g => g.Id == int.Parse(group)).FirstOrDefault<SubjectGroup>()); // return subjects in the specified group (param)

                List<int> filterSubjects = string.IsNullOrWhiteSpace(selectedSubjects) ? new List<int>() : selectedSubjects.ParseIntCSV(); // selectedSubjects are to be filtered out

                if (string.IsNullOrWhiteSpace(q))
                    return this.Json((from s in subjects
                                      where !filterSubjects.Contains(s.Id)
                                      select s).ToList(), JsonRequestBehavior.AllowGet);

                return Json((from s in subjects
                             where s.Name.Trim().ToLower().Contains(q.Trim().ToLower()) && !filterSubjects.Contains(s.Id)
                             select s).ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Returns JSON Object for Countries in a given continent
        /// </summary>
        /// <param name="q">Is a valid id of a continent. Only countries in this continent are returned.</param>
        /// <param name="continents">A csv of continent Ids [int]. Only countries in these continents will be returned. Func can handle wrong Ids and NaN</param>
        /// <returns>A JSON array of Countries. Each Country is [Id: int, Name: string].</returns>
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult Countries(string q, string continents, string selected)
        {
            List<Country> countries = new List<Country>();

            using (DBClass db = new DBClass())
            {
                GeographicBAL gb = new GeographicBAL(db);

                q = string.IsNullOrWhiteSpace(q) ? string.Empty : q;

                List<int> inCon = string.IsNullOrWhiteSpace(continents)
                    ? new List<int>() : continents.ParseIntCSV();

                List<int> selectedCountries = string.IsNullOrWhiteSpace(selected) ? new List<int>() : selected.ParseIntCSV();

                List<Continent> inContinents =
                    inCon.Count > 0
                    ? gb.GetContinents().Where(con => inCon.Contains(con.Id) && CurrentUser.DataModules.Contains(con.DataModuleId)).ToList()
                    : gb.GetContinents().Where(con => CurrentUser.DataModules.Contains(con.DataModuleId)).ToList();

                foreach (Continent con in inContinents)
                {
                    List<Country> cts = gb.GetCountries(con.Id);

                    if (cts != null && cts.Count > 0)
                        countries.AddRange(cts);
                }

                countries.RemoveAll(c => selectedCountries.Contains(c.Id));
            }

            return Json((from c in countries
                         where c.Name.Trim().ToLower().Contains(q.Trim().ToLower())
                         select c).OrderBy(c => c.Name).ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Returns JSON Object for States in a given Country
        /// </summary>
        /// <param name="q">Is a valid id of a Country. Only States in this Country are returned.</param>
        /// <param name="countries">A csv of country Ids [int]. Only countries in these countries will be returned. Func can handle wrong Ids and NaN</param>
        /// <returns>A JSON array of States. Each State is [Id: int, Name: string].</returns>
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult States(string q, string countries, string selected)
        {
            List<State> states = new List<State>();

            using (DBClass db = new DBClass())
            {
                GeographicBAL gb = new GeographicBAL(db);
                q = string.IsNullOrWhiteSpace(q) ? string.Empty : q;
                List<int> inCountries = string.IsNullOrWhiteSpace(countries)
                    ? new List<int>() : countries.ParseIntCSV();

                List<int> selectedStates = string.IsNullOrWhiteSpace(selected) ? new List<int>() : selected.ParseIntCSV();

                //default to Australia if no countries passed in.
                if (inCountries.Count() == 0)
                    inCountries.Add(DefaultCountryID);
                else
                    inCountries = GetDistinctCountriesInModule(inCountries, gb);

                foreach (int c in inCountries)
                {
                    List<State> sts = gb.GetStates(c);

                    if (sts != null && sts.Count > 0)
                        states.AddRange(sts);
                }

                //order states by name. If the same state exists in 2 countries -> append the name of the country...
                List<string> duplicateStates = (from c in states
                                                group c by c.Name.ToLower().Trim() into stateGroup
                                                where stateGroup.Count() > 1
                                                select stateGroup.Key).ToList();

                if (duplicateStates.Count > 0)
                    foreach (State st in states)
                    {
                        if (duplicateStates.Contains(st.Name.ToLower().Trim()))
                        {
                            string countryName = null;
                            //append the country
                            try
                            {
                                countryName = gb.GetCountry(st.CountryId).Name;
                            }
                            catch (Exception) { }

                            if (!string.IsNullOrWhiteSpace(countryName)) st.Name = st.Name + ", " + countryName;
                        }
                    }

                states.RemoveAll(s => selectedStates.Contains(s.Id));
            }

            return Json((from c in states
                         where c.Name.Trim().ToLower().Contains(q.Trim().ToLower())
                         select c).OrderBy(s => s.Name).ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Returns JSON object for Cities in a given State
        /// </summary>
        /// <param name="q"></param>
        /// <param name="states">A csv of state Ids [int]. Only cities in these states will be returned. Func can handle wrong Ids and NaN</param>
        /// <param name="countries">A csv of country Ids[int]. Only cities in the countries that none of the states belong to will be returned. Func can handle wrong Ids and NaN. 
        ///<returns>A JSON array of Cities. Each State is [Id: int, Name: string].</returns>
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult Cities(string q, string states, string countries, string selected)
        {
            List<City> cities = new List<City>();

            using (DBClass db = new DBClass())
            {
                GeographicBAL gb = new GeographicBAL(db);
                q = string.IsNullOrWhiteSpace(q) ? string.Empty : q;
                List<int> inStates = string.IsNullOrWhiteSpace(states)
                    ? new List<int>() : states.ParseIntCSV();

                List<int> selectedCities = string.IsNullOrWhiteSpace(selected) ? new List<int>() : selected.ParseIntCSV();

                List<int> inCountries = string.IsNullOrWhiteSpace(countries)
                    ? new List<int>() : countries.ParseIntCSV();

                //default to Australia if no countries passed in.
                if (inCountries.Count() == 0)
                    inCountries.Add(DefaultCountryID);
                else
                    inCountries = GetDistinctCountriesInModule(inCountries, gb);

                //only find cities in the selected states.
                foreach (int inSt in inStates)
                {
                    List<City> cts = gb.GetCities(null, null, inSt, null);

                    if (cts != null && cts.Count > 0)
                        cities.AddRange(cts);
                }

                //find cities in the countries only if a state from the country was not passed in to the "states" parameter. This case will also cover
                //countries which do not have any state but we would like to show cities from that country.
                if (inCountries.Count > 0)
                {
                    List<City> citiesInCountries = new List<City>();
                    List<int> countriesForStates;
                    List<State> selectedStates = new List<State>();

                    // Get the distinct list of countries for the selected states.
                    foreach (int s in inStates)
                    {
                        State st = gb.GetState(s);

                        if (st != null && st.Id == s)
                            selectedStates.Add(st);
                    }
                    countriesForStates = selectedStates.Select(s => s.CountryId).Distinct().ToList();

                    foreach (int country in inCountries)
                    {
                        if (!countriesForStates.Contains(country))
                        {
                            List<City> cts = gb.GetCities(null, country, null, null);

                            if (cts != null && cts.Count > 0)
                            {
                                citiesInCountries.AddRange(cts);
                            }
                        }
                    }

                    //only add cities that are not already in the cities collection
                    List<City> moreCities = (from c in citiesInCountries
                                             where !(from ct in cities
                                                     select ct.Id).Contains(c.Id)
                                             select c).ToList();

                    cities.AddRange(moreCities);
                }

                //order cities by name. If the same city exists in 2 states -> append the name of the state and country...
                List<string> duplicateCities = (from c in cities
                                                group c by c.Name.ToLower().Trim() into stateGroup
                                                where stateGroup.Count() > 1
                                                select stateGroup.Key).ToList();

                if (duplicateCities.Count > 0)
                    foreach (City ct in cities)
                    {
                        if (duplicateCities.Contains(ct.Name.ToLower().Trim()))
                        {
                            string countryName = null;
                            //append the country
                            try
                            {
                                countryName = gb.GetStates(ct.CountryId.Value).Where(st => st.Id == ct.StateId.Value).FirstOrDefault().Name + ", " + gb.GetCountry(ct.CountryId.Value).Name;
                            }
                            catch (Exception) { }

                            if (!string.IsNullOrWhiteSpace(countryName)) ct.Name = ct.Name + ", " + countryName;
                        }
                    }

                cities.RemoveAll(c => selectedCities.Contains(c.Id));
            }

            // Return cities starting with the entered text first, and then cities containing the entered text.
            return Json((from c in cities
                         where c.Name.StartsWith(q.Trim(), StringComparison.CurrentCultureIgnoreCase)
                         select new
                         {
                             Id = c.Id,
                             Name = c.Name.Trim()
                         }).OrderBy(c => c.Name).Union((from c in cities
                                                        where c.Name.IndexOf(q.Trim(), StringComparison.CurrentCultureIgnoreCase) > 0
                                                        select new
                                                        {
                                                            Id = c.Id,
                                                            Name = c.Name.Trim()
                                                        }).OrderBy(c => c.Name)).ToList(), JsonRequestBehavior.AllowGet);
        }

        private List<int> GetDistinctCountriesInModule(List<int> countries, GeographicBAL gb)
        {
            List<int> toRemove = new List<int>();

            foreach (var ct in countries)
            {
                Country cnt = gb.GetCountry(ct);

                if (cnt == null || !CurrentUser.DataModules.Contains(cnt.DataModuleId))
                    toRemove.Add(ct);

            }

            countries.RemoveAll(icnt => toRemove.Contains(icnt));

            return countries.Distinct().ToList();
        }
    }
}
