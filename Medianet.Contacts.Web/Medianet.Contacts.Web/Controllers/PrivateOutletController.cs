﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Services.PrivateOutlet;
using Medianet.Contacts.Web.ViewModels.PrivateOutlet;
using Medianet.Contacts.Web.Services.BaseInfo;
using Medianet.Contacts.Web.Services.Geographic;
using System.Web;
using Medianet.Contacts.Web.Services.PrivateContact;
using System.Net;
using Medianet.Contacts.Web.ViewModels.Search;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels.Grid;

namespace Medianet.Contacts.Web.Controllers
{
    public class PrivateOutletController : BaseController
    {
        private readonly IPrivateOutletService _privateOutletService;
        private readonly IPrivateContactService _privateContactService;

        public PrivateOutletViewModel PrivateOutletViewModel { get; set; }

        public PrivateOutletController() : base()
        {
            _privateOutletService = PrivateOutletService.CreatePrivateOutletService();
            _privateContactService = PrivateContactService.CreatePrivateContactService();
        }

        [HttpGet]
        [RequiresSession, SavesSession]
        [HandleAuthentication, HandleHtmlError]
        public ActionResult Index()
        {
            return View(GetPrivateOutlet(false));
        }

        [HttpGet]
        [RequiresSession, SavesSession]
        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult GetOutlets(bool? loadLimitedRecords, string outletId)
        {
            SearchViewModel model = null;

            if (string.IsNullOrWhiteSpace(outletId))
                model = GetPrivateOutlet(loadLimitedRecords ?? true);
            else
            {
                SearchOptions = new SearchOptionsOutlet
                {
                    OutletID = outletId,
                    OutletType = RecordType.OmaOutlet
                };

                model = new SearchController().GetExpandResult(outletId, (int)RecordType.OmaOutlet, false);
            }

            return PartialView("_GridPartial", model);
        }

        [HttpGet]
        [HandleAuthentication, HandleHtmlError]
        public ActionResult Edit(string id)
        {
            ViewBag.ReferrerUrl = Request.UrlReferrer.ToString();

            var model = string.IsNullOrWhiteSpace(id) ? new PrivateOutletViewModel() : _privateOutletService.GetById(id, CurrentUser.DebtorNumber);

            if (model != null)
            {
                InitOutlet(model);

                InitDropdowns(model);
            }

            return View(model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Create(PrivateOutletViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UpdatedBy = CurrentUser.UserID;

                _privateOutletService.Save(model, CurrentUser.SessionKey);
            }
            else
                return new HttpStatusCodeResult(400, ModelErrorMessages);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult Delete(DeleteViewModel model)
        {
            string[] ids = model.CheckedItems.Split(',');

            bool result = !string.IsNullOrWhiteSpace(ids[1]) && ids[1] != "0" ?
                _privateContactService.Delete(ids[1], CurrentUser.DebtorNumber, CurrentUser.UserID) :
                _privateOutletService.Delete(ids[0], CurrentUser.DebtorNumber, CurrentUser.UserID, CurrentUser.SessionKey);

            if (!result)
                throw new HttpException();

            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult GetCountries(int? continentId)
        {
            var countries = continentId.HasValue ? GeographicService.CreateGeographicService().GetCountries(continentId.Value) :
                GeographicService.CreateGeographicService().GetCountries();

            return Json(countries, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult GetStates(int countryId)
        {
            List<State> states = GeographicService.CreateGeographicService().GetStates(countryId);

            return Json(states, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult GetCities(int? countryId, int? stateId)
        {
            List<City> cities = GeographicService.CreateGeographicService().GetCities(countryId, stateId);

            return Json(cities, JsonRequestBehavior.AllowGet);
        }

        #region Private methods

        private SearchViewModel GetPrivateOutlet(bool loadLimitedRecords)
        {
            ViewBag.LoadLimitedRecords = loadLimitedRecords;

            int? pageSize = loadLimitedRecords == true ? 6 : (int?)null;

            ViewModels.AdvancedSearchViewModel model = new ViewModels.AdvancedSearchViewModel { RecordsToShow = Common.BO.ShowResultType.P };

            SearchController searchController = new SearchController();

            SearchViewModel result = searchController.GetAdvanceSearchResult(model, pageSize, false);
            result.Pagination.UseCookieForPageSize = false;

            SearchOptions = new SearchOptionsAdvanced { RecordsToShow = Common.BO.ShowResultType.P.ToString(), Context = SearchContext.Both };

            return result;
        }

        private void InitOutlet(PrivateOutletViewModel model)
        {
            GeneralDataService generalDataservice = GeneralDataService.CreateSubjectService();
            var geographicService = GeographicService.CreateGeographicService();

            ViewBag.SubjectGroups = generalDataservice.GetSubjectGroups();
            ViewBag.WorkingLanguages = generalDataservice.GetWorkingLanguage();
            ViewBag.Frequency = generalDataservice.GetFrequency();
            ViewBag.MediaType = generalDataservice.GetMediaType();

            ViewBag.Continents = geographicService.GetContinents();
            ViewBag.ContinentId = null;
            ViewBag.Countries = geographicService.GetCountries();

            if (!model.MediaAtlasCountryId.HasValue)
                return;

            Country country = geographicService.GetCountry(model.MediaAtlasCountryId.Value);

            if (country == null || !country.ContinentId.HasValue)
                return;

            ViewBag.ContinentId = country.ContinentId.Value;

            ViewBag.Countries = geographicService.GetCountries(country.ContinentId.Value);

            ViewBag.States = geographicService.GetStates(model.MediaAtlasCountryId.Value);

            ViewBag.Cities = model.StateId.HasValue ?
                geographicService.GetCities(country.ContinentId.Value, model.MediaAtlasCountryId.Value, model.StateId.Value) :
                geographicService.GetCities(country.ContinentId.Value, model.MediaAtlasCountryId.Value);
        }

        private void InitDropdowns(PrivateOutletViewModel model)
        {
            List<SubjectGroup> subGroups = GeneralDataService.CreateSubjectService().GetSubjectGroups();

            ViewBag.SubjectGroups = subGroups.Where(g => g.Subjects.Count > 0);
        }

        #endregion
    }
}
