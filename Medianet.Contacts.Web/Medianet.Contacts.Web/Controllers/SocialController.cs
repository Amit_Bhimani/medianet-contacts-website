﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Contacts.Web.Services.Social;
using Medianet.Contacts.Web.ViewModels.Social;
using Medianet.Contacts.Web.Filters;
using System.Threading.Tasks;
using System.Web.Caching;
using System.Runtime.Caching;

namespace Medianet.Contacts.Web.Controllers
{
    public class SocialController : BaseController
    {
        private IKloutService _kloutService;
        private ITwitterService _twitterService;
        private IYouTubeService _youTubeService;
        private static readonly MemoryCache _cache = new MemoryCache("ActionResultCacheAttribute");
        private static string twitterCacheKey = "twit-prof";

        public SocialController() : base()
        {
            _kloutService = KloutService.CreateKloutService();
            _twitterService = TwitterService.CreateTwitterService();
            _youTubeService = YouTubeService.CreateYouTubeService();
        }

        //
        // GET: /Social/Klout?twitterHandle={twitterHandle}
        [HttpGet]
        public ActionResult Klout(string twitterHandle)
        {
            var model = new KloutViewModel();

            model = _kloutService.GetKloutScore(twitterHandle);

            return PartialView(model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public async Task<JsonResult> MultiUsersTwitterImages(string twitterHandles)
        {
            return Json(await GetMultiTwitterDetails(twitterHandles), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public async Task<JsonResult> UserTwitterImage(string twitterHandle)
        {
            return Json(await GetTwitterDetails(twitterHandle), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public async Task<PartialViewResult> UserTwitter(string twitterHandle)
        {
            return PartialView("Social/_TwitterPartial", await GetTwitterDetails(twitterHandle));
        }
        
        [HttpGet]
        public async Task<PartialViewResult> UserTweets(string twitterHandle)
        {
            return PartialView("Social/_TweetsPartial", await _twitterService.GetUserTweets(twitterHandle));
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult UserYouTube(string url)
        {
            YouTubeViewModel result = _youTubeService.GetData(url);

            return PartialView("Social/_YouTubePartial", result);
        }

        private async Task<List<TwitterUserViewModel>> GetMultiTwitterDetails(string twitterHandles)
        {
            var handles = HttpUtility.UrlDecode(twitterHandles ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var handlesNotCached = "";
            var twitterDetailsList = new List<TwitterUserViewModel>();

            foreach (var item in handles)
            {
                var key = $"{twitterCacheKey}-{item.ToLower()}";
                var cachedItem = _cache.Get(key);

                if (cachedItem == null)
                    handlesNotCached += item + ",";               
                else
                {
                    twitterDetailsList.Add((TwitterUserViewModel)cachedItem);
                }
            }

            if (!string.IsNullOrEmpty(handlesNotCached))
            {
                var models = await _twitterService.GetUsersTwitterInfo(handlesNotCached);

                foreach (var item in models)
                {
                    if (!string.IsNullOrEmpty(item.ProfileImage))
                    {
                        _cache.Add($"{twitterCacheKey}-{item.Handle.ToLower()}", item, DateTime.Now.AddSeconds(3600));
                        twitterDetailsList.Add(item);
                    }
                }
            }

            return twitterDetailsList;
        }

        private async Task<TwitterUserViewModel> GetTwitterDetails(string twitterHandle)
        {
            var twitterDetails = new TwitterUserViewModel();
            var cacheKey = $"{twitterCacheKey}-{twitterHandle.ToLower()}";
            var cachedItem = _cache.Get(cacheKey);

            if (cachedItem != null)
            {
               return  (TwitterUserViewModel)cachedItem;
            }
                
            twitterDetails = await _twitterService.GetUserTwitterInfo(twitterHandle);

            if (twitterDetails != null && !string.IsNullOrEmpty(twitterDetails.ProfileImage))
            {
                _cache.Add(cacheKey, twitterDetails, DateTime.Now.AddSeconds(3600));
            }               
          
            return twitterDetails;
        }
    }
}
