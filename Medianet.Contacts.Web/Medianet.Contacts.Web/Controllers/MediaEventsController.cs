﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Filters;
using Medianet.UmbracoContent;
using Medianet.UmbracoContent.Model;

namespace Medianet.Contacts.Web.Controllers
{
    public class MediaEventsController : BaseController
    {
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index()
        {
            return View();
        }

        [HandleHtmlError, HandleAuthentication]
        public PartialViewResult RenderDailyDiary()
        {
            try
            {
                string address = ConfigurationManager.AppSettings["MedianetPublicAPIAddress"];
                DiaryContent diaryDL = new DiaryContent(address);
                DailyDiary diary = diaryDL.GetDiary();
                return PartialView("_DailyDiaryPartial", Server.HtmlDecode(diary.DiaryContent));
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(MediaEventsController).Name, "PopulateDailyDiary", null);
                throw ex;
            }
        }

        [HandleHtmlError, HandleAuthentication]
        public PartialViewResult RenderMediaEventsAgenda()
        {
            try
            {
                string address = ConfigurationManager.AppSettings["MedianetPublicAPIAddress"];
                EventAgendaContent agendaDL = new EventAgendaContent(address);
                List<AgendaItem> agendaItems = agendaDL.GetEventsFor(DateTime.Now);
                return PartialView("_MediaEventsAgendaPartial", agendaItems);
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(MediaEventsController).Name, "PopulateMediaEventsAgenda", null);
                throw ex;
            }
        }
    }
}
