﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Services.PrivateContact;
using Medianet.Contacts.Web.ViewModels.PrivateContact;
using Medianet.Contacts.Web.Services.BaseInfo;
using Medianet.Contacts.Web.Services.Geographic;
using Medianet.Contacts.Web.Services.PrivateOutlet;
using Medianet.Contacts.Web.Services.Outlet;
using Medianet.Contacts.Web.ViewModels.Outlet;
using System.Net;
using Medianet.Contacts.Web.ViewModels.Search;

namespace Medianet.Contacts.Web.Controllers
{
    public class PrivateContactController : BaseController
    {
        private readonly IPrivateContactService _privateContactService;
        private readonly IPrivateOutletService _privateOutletService;
        private readonly IOutletService _OutletService;

        public PrivateContactViewModel PrivateContactViewModel { get; set; }

        public PrivateContactController() : base()
        {
            _privateContactService = PrivateContactService.CreatePrivateContactService();
            _privateOutletService = PrivateOutletService.CreatePrivateOutletService();
            _OutletService = OutletService.CreateOutletService();
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public JsonResult AutoSuggest(string term)
        {
            List<OutletSuggestViewModel> results = null;

            if (!string.IsNullOrWhiteSpace(term))
                results = _OutletService.GetOutletsSuggestions(term, CurrentUser.DebtorNumber);

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [HandleAuthentication, HandleHtmlError]
        public ActionResult Edit(string Id)
        {
            ViewBag.ReferrerUrl = Request.UrlReferrer.ToString();

            var model = string.IsNullOrWhiteSpace(Id) ? new PrivateContactViewModel() : _privateContactService.GetById(Id, CurrentUser.DebtorNumber);

            if (model != null)
            {
                InitContact(model);

                InitDropdowns(model);
            }

            return View(model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Create(PrivateContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UpdatedBy = CurrentUser.UserID;
                _privateContactService.Save(model);
            }
            else
                return new HttpStatusCodeResult(400, ModelErrorMessages);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #region Private methods

        private void InitContact(PrivateContactViewModel model)
        {
            GeneralDataService generalDataservice = GeneralDataService.CreateSubjectService();
            var geographicService = GeographicService.CreateGeographicService();

            ViewBag.SubjectGroups = generalDataservice.GetSubjectGroups();

            ViewBag.Roles = generalDataservice.GetRoles();
            ViewBag.Continents = geographicService.GetContinents();
            ViewBag.ContinentId = null;
            ViewBag.Countries = geographicService.GetCountries();

            ViewBag.Prefixes = MedianetHelpers.EnumToSelectList(typeof(Prefixes), model.Prefix);

            ViewBag.DeliverMethods = MedianetHelpers.EnumToSelectList(typeof(PreferredDeliveryMethod),
                model.ContactId > 0 ? model.PreferredDelivery.ToString() : string.Empty);

            if (!model.CountryId.HasValue)
                return;

            Country country = geographicService.GetCountry(model.CountryId.Value);

            if (country == null || !country.ContinentId.HasValue)
                return;

            ViewBag.ContinentId = country.ContinentId.Value;

            ViewBag.Countries = geographicService.GetCountries(country.ContinentId.Value);

            ViewBag.States = geographicService.GetStates(model.CountryId.Value);

            ViewBag.Cities = model.StateId.HasValue ?
                geographicService.GetCities(country.ContinentId.Value, model.CountryId.Value, model.StateId.Value) :
                geographicService.GetCities(country.ContinentId.Value, model.CountryId.Value);
        }

        private void InitDropdowns(PrivateContactViewModel model)
        {
            List<SubjectGroup> subGroups = GeneralDataService.CreateSubjectService().GetSubjectGroups();

            ViewBag.SubjectGroups = subGroups.Where(g => g.Subjects.Count > 0);
        }

        #endregion
    }
}
