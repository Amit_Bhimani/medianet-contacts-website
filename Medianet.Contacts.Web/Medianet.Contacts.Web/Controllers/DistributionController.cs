﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Services.Distribution;
using Medianet.Contacts.Web.Services.List;
using Medianet.Contacts.Web.ViewModels.Distribution;
using Medianet.Contacts.Web.ViewModels.List;
using System.Configuration;

namespace Medianet.Contacts.Web.Controllers
{
    public class DistributionController : BaseController
    {
        private IListService _listService;
        private IDistributionService _distributionService;
        private List<ListViewModel> _lists = null;

        public DistributionController() : base()
        {
            _listService = ListService.CreateListService();
            _distributionService = DistributionService.Create();
        }

        protected override void Dispose(bool disposing)
        {
            _listService?.Dispose();
            _distributionService?.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(int? listId)
        {
            var model = InitialiseModel();
            var selectedLists = new List<int>();

            if (listId.HasValue)
                selectedLists.Add(listId.Value);

            PopulateLists(selectedLists);

            return View(model);
        }

        [HttpPost]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(DistributionViewModel model)
        {
            ValidateDistribution(model);

            PopulateLists(model.SelectedListIds);

            if (ModelState.IsValid)
            {
                var releaseId = Submit(model);

                // Clear the form and return the release id of the release just created
                model = InitialiseModel();
                model.ReleaseId = releaseId;
                ViewBag._selectedLists = null;
                ModelState.Clear();

                return View(model);
            }

            return View(model);
        }

        private DistributionViewModel InitialiseModel()
        {
            var model = new DistributionViewModel();

            model.CompanyName = CurrentUser.CompanyName;
            model.DebtorNumber = CurrentUser.DebtorNumber;
            model.SenderName = CurrentUser.FullName;
            model.EmailAddress = CurrentUser.EmailAddress;
            model.SelectedLists = string.Empty;
            model.DistributionWire = false;
            model.WireAll = true;
            model.WireTas = true;
            model.WireVic = true;
            model.WireNsw = true;
            model.WireAct = true;
            model.WireSa = true;
            model.WireWa = true;
            model.WireQld = true;
            model.WireNt = true;
            model.OnHold = false;
            model.SpecialInstructions = string.Empty;
            model.ReleaseId = 0;

            return model;
        }

        private List<ListViewModel> Lists
        {
            get
            {
                if (_lists == null)
                    _lists = _listService.GetLists(CurrentUser.UserID, CurrentUser.SessionKey);

                return _lists;
            }
        }

        private void PopulateLists(List<int> selectedLists)
        {
            var baseLists = new List<ListBase>();
            var baseSelectedLists = new List<ListBase>();

            baseLists.AddRange(
                Lists.Select(lst => new List { Id = lst.ListId, Name = lst.Name }).OrderBy(l => l.Name).ToList());

            if (selectedLists != null)
            {
                foreach (var id in selectedLists)
                {
                    var selectedList = baseLists.FirstOrDefault(l => l.Id == id);
                    if (selectedList != null)
                        baseSelectedLists.Add(selectedList);
                }
            }

            ViewBag._lists = baseLists;
            ViewBag._selectedLists = baseSelectedLists;

        }

        private void ValidateDistribution(DistributionViewModel model)
        {
            if (model.Release == null || model.Release.ContentLength == 0)
            {
                ModelState.AddModelError("Release", "Please attach a release document.");
            }

            if (model.DistributionWire)
            {
                if (!model.WireAll && !model.WireTas && !model.WireVic && !model.WireNsw && !model.WireAct &&
                    !model.WireSa && !model.WireWa && !model.WireQld && !model.WireNt)
                {
                    ModelState.AddModelError("DistributionWire", "Please select at least one of the wire states.");
                }
            }

            if (string.IsNullOrEmpty(model.SelectedLists))
            {
                ModelState.AddModelError("SelectedLists", "Please select one or more lists to distribute to.");
            }

            model.SelectedListIds = string.IsNullOrWhiteSpace(model.SelectedLists) ? new List<int>() : model.SelectedLists.ParseIntCSV();
        }

        private int Submit(DistributionViewModel model)
        {
            int releaseId = 0;
            Stream headerStream = null;
            Stream emailCsvStream = null;
            Stream emailTextStream = null;

            try
            {
                DataSet dsEmail;

                // Get the details of the distribution.
                Distribute distribute = GetDistribution(model);

                // Create the Job on the database.
                releaseId = _distributionService.CreateJob(distribute);

                dsEmail = _distributionService.GetEmailFax(model.SelectedLists);

                // Create the Header file.
                headerStream = CreateHeaderFile(releaseId, model, distribute, dsEmail.Tables[0].Rows.Count);

                emailCsvStream = CreateCsvStream(dsEmail.Tables[0]);
                emailTextStream = CreateTextStream(dsEmail.Tables[0]);

                // Send all the content to CS staff in an email.
                EmailContactUs emailContact = new EmailContactUs();

                emailContact.SendEmail_Distribution(
                                releaseId.ToString(),
                                ConfigurationManager.AppSettings["ClientServiceEmail"],
                                ConfigurationManager.AppSettings["ClientServiceEmail"],
                                "Medianet Contacts Database Distribution",
                                headerStream, emailCsvStream, emailTextStream,
                                model.Release?.InputStream, model.Release?.FileName,
                                model.Attachment?.InputStream, model.Attachment?.FileName,
                                "Find attached distribution files.",
                                model.SenderName.Trim(), model.EmailAddress.Trim());
            }
            finally
            {
                headerStream?.Close();
                emailCsvStream?.Close();
                emailTextStream?.Close();
            }

            return releaseId;
        }

        /// <summary>
        /// Method for creating header file as attachment
        /// </summary>
        private Stream CreateHeaderFile(int jobId, DistributionViewModel model, Distribute dist, int emailCount)
        {
            var ms = new MemoryStream();
            string aapNewswire = "";
            string distributionList = "";

            // Check if they also want Wire.
            if (model.DistributionWire)
            {
                if (model.WireAll)
                    aapNewswire = AppendToString("ALL", aapNewswire);
                if (model.WireTas)
                    aapNewswire = AppendToString("TAS", aapNewswire);
                if (model.WireVic)
                    aapNewswire = AppendToString("VIC", aapNewswire);
                if (model.WireNsw)
                    aapNewswire = AppendToString("NSW", aapNewswire);
                if (model.WireAct)
                    aapNewswire = AppendToString("ACT", aapNewswire);
                if (model.WireSa)
                    aapNewswire = AppendToString("SA", aapNewswire);
                if (model.WireWa)
                    aapNewswire = AppendToString("WA", aapNewswire);
                if (model.WireQld)
                    aapNewswire = AppendToString("QLD", aapNewswire);
                if (model.WireNt)
                    aapNewswire = AppendToString("NT", aapNewswire);
            }

            foreach (int id in model.SelectedListIds)
            {
                var selectedList = Lists.FirstOrDefault(l => l.ListId == id);
                if (selectedList != null)
                    distributionList = AppendToString(selectedList.Name, distributionList);
            }

            using (StreamWriter sw = new StreamWriter(ms, Encoding.UTF8, 1024, true))
            {
                sw.WriteLine("Job No: " + jobId.ToString());
                sw.WriteLine("Organisation Name: " + model.CompanyName);
                sw.WriteLine("Customer ID Number: " + CurrentUser.DebtorNumber);
                sw.WriteLine("Sender Name: " + model.SenderName);
                sw.WriteLine("Sender Phone: " + model.WorkPhoneNumber);
                sw.WriteLine("Sender Mobile Phone / After Hours number: " + model.AfterHoursNumber);
                sw.WriteLine("Sender E-mail Address: " + model.EmailAddress);
                sw.WriteLine("Client Reference Number: " + model.CustomerReference);
                sw.WriteLine("Distribution List Name:" + distributionList);
                sw.WriteLine("Release File: " + model.Release.FileName);

                if (model.Attachment != null)
                    sw.WriteLine("Attachment File: " + model.Attachment.FileName);

                sw.WriteLine("Job Schedule: " + (model.OnHold && !string.IsNullOrWhiteSpace(model.HoldTime) ? "Hold" : "Immediate"));
                sw.WriteLine("Hold/Embargo Date and Time: " + (model.OnHold && !string.IsNullOrWhiteSpace(model.HoldTime) ? model.HoldTime : ""));
                sw.WriteLine("Total E-mail: " + emailCount);
                sw.WriteLine("Distribution Method: Email");
                sw.WriteLine("Newswires: " + aapNewswire);
                sw.WriteLine("Client Services Instructions: " + model.SpecialInstructions);
            }

            ms.Position = 0;
            return ms;
        }

        private Stream CreateCsvStream(DataTable dt)
        {
            var ms = new MemoryStream();

            using (var sw = new StreamWriter(ms, Encoding.UTF8, 1024, true))
            {
                // First we will write the headers.
                int iColCount = dt.Columns.Count;

                // Now write all the rows.
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < iColCount; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            sw.Write(dr[i].ToString());
                        }

                        if (i < iColCount - 1)
                        {
                            sw.Write(",");
                        }
                    }

                    sw.Write(sw.NewLine);
                }
            }

            ms.Position = 0;
            return ms;
        }

        private Stream CreateTextStream(DataTable dt)
        {
            var ms = new MemoryStream();

            using (var sw = new StreamWriter(ms, Encoding.UTF8, 1024, true))
            {
                System.Text.StringBuilder str = new System.Text.StringBuilder();

                foreach (DataRow row in dt.Rows)
                {
                    foreach (var item in row.ItemArray) // Loop over the items.
                    {
                        sw.WriteLine(item);
                    }

                    //put a gap between the records
                    if (row.ItemArray.Length > 1)
                        sw.WriteLine("");
                }
            }

            ms.Position = 0;
            return ms;
        }

        private Distribute GetDistribution(DistributionViewModel model)
        {
            Distribute distribute = new Distribute();
            distribute.DebtorNumber = CurrentUser.DebtorNumber;
            distribute.CompanyName = model.CompanyName ?? string.Empty;
            distribute.SubmittedUserId = CurrentUser.UserID;
            distribute.ReleaseDescription = "Medianet Contacts Database Distribution for " + model.CompanyName ?? string.Empty;
            distribute.WorkPhone = model.WorkPhoneNumber ?? string.Empty;
            distribute.Mobile = model.AfterHoursNumber ?? string.Empty;
            distribute.Email = model.EmailAddress ?? string.Empty;
            distribute.CustRef = model.CustomerReference ?? string.Empty;

            if (model.OnHold && !string.IsNullOrWhiteSpace(model.HoldTime))
            {
                distribute.HoldDateTime = DateTime.ParseExact(model.HoldTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                distribute.Hold = 1;
            }
            else
            {
                DateTime dt = new DateTime(1901, 1, 1);
                distribute.HoldDateTime = dt;
                distribute.Hold = 0;
            }

            distribute.Comment = model.SpecialInstructions ?? string.Empty;

            return distribute;
        }

        private string AppendToString(string value, string existing)
        {
            return string.IsNullOrEmpty(existing) ? value : $"{existing}, {value}";
        }
    }
}
