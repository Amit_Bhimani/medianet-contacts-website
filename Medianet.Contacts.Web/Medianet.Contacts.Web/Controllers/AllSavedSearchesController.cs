﻿using System.Collections.Generic;
using System.Web.Mvc;
using Medianet.Contacts.Web.Filters;
using System.Web.UI;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using System.Linq;
using Medianet.Contacts.Web.Services.Search;
using Medianet.Contacts.Web.ViewModels.AllSavedSearches;
using System.Net;
using System;

namespace Medianet.Contacts.Web.Controllers
{
    public class AllSavedSearchesController : BaseController
    {
        #region Field

        private ISavedSearchService _savedSearchService;
        #endregion

        public AllSavedSearchesController() : base()
        {
            _savedSearchService = SavedSearchService.CreateSavedSearchService();
        }

        protected override void Dispose(bool disposing)
        {
            if (_savedSearchService != null) _savedSearchService.Dispose();
            base.Dispose(disposing);
        }

        #region Display 

        // GET: /AllSavedSearches/
        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index()
        {
            List<SavedSearchGroupedViewModel> savedSearchesGrouped = _savedSearchService.GetGroupedSavedSearches(CurrentUser.UserID, CurrentUser.SessionKey);
            return View(savedSearchesGrouped);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public PartialViewResult RebindAll()
        {
            List<SavedSearchGroupedViewModel> savedSearchesGrouped = _savedSearchService.GetGroupedSavedSearches(CurrentUser.UserID, CurrentUser.SessionKey);
            return PartialView("_AllGroupedSavedSearchesPartial", savedSearchesGrouped);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult RebindGroup(RebindGroupViewModel model)
        {
            SavedSearchGroupedViewModel searchGrouped = _savedSearchService.GetGroupedSavedSearches(CurrentUser.UserID, CurrentUser.SessionKey)
                .Where(g => g.GroupId == model.GroupId).FirstOrDefault();

            var orderedGroup = new SavedSearchGroupedViewModel()
            {
                GroupId = model.GroupId
            };

            if (searchGrouped != null)
            {
                switch (model.SortColumn)
                {
                    case AllSavedSearchesSortColumn.SearchName:
                        orderedGroup.Lists = model.SortDirection == SortDirection.ASC ? searchGrouped.Lists
                            .OrderBy(o => o.Name).ToList() : searchGrouped.Lists.OrderByDescending(o => o.Name).ToList();
                        break;
                    case AllSavedSearchesSortColumn.UserName:
                        orderedGroup.Lists = model.SortDirection == SortDirection.ASC ? searchGrouped.Lists.OrderBy(o => o.UserName).ToList() : searchGrouped.Lists.OrderByDescending(o => o.UserName).ToList();
                        break;
                    case AllSavedSearchesSortColumn.CreatedDate:
                        orderedGroup.Lists = model.SortDirection == SortDirection.ASC ? searchGrouped.Lists.OrderBy(o => o.CreatedDate).ToList() : searchGrouped.Lists.OrderByDescending(o => o.CreatedDate).ToList();
                        break;
                }
            }

            return PartialView("_SavedSearchPartial", orderedGroup);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult DeleteSavedSearch(int searchId)
        {
            _savedSearchService.Delete(searchId);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Edit(int searchId)
        {
            SavedSearchViewModel model = _savedSearchService.GetSavedSearchById(searchId, CurrentUser.SessionKey);
            return PartialView("_EditSavedSearchPartial", model);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Edit(SavedSearchViewModel model)
        {
            if (model != null && model.SearchId != 0 && !string.IsNullOrWhiteSpace(model.Name))
            {
                _savedSearchService.Edit(model);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion
    }
}
