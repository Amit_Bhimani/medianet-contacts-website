﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.ViewModels.Account;
using Medianet.Contacts.Web.ViewModels.List;
using Medianet.Contacts.Web.ChargeBeeService;
using System.Linq;
using System.Web.UI;
using AutoMapper;
using Medianet.Contacts.Web.Common.Logging;
using Medianet.Contacts.Web.Filters;
using System.Net;

namespace Medianet.Contacts.Web.Controllers
{
    public class AccountController : Controller
    {
        public static Hashtable SessionErrorCodesToQueryString = ConfigurationManager.GetSection("SessionErrorCodesToQueryString") as Hashtable;

        public static string LoginFailedURL
        {
            get
            {
                return LoginPageURL + SessionErrorCodesToQueryString["LoginFailed"] as string ?? "";
            }
        }

        public static string InvalidSessionURL
        {
            get
            {
                return LoginPageURL + SessionErrorCodesToQueryString["InvalidSession"] as string ?? "";
            }
        }

        public static string InsufficientLogonDetailsURL
        {
            get
            {
                return LoginPageURL + SessionErrorCodesToQueryString["InsuffecientDetails"] as string ?? "";
            }
        }

        public static string LoginPageURL
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginPageUrl"];
            }
        }

        public ActionResult Logon()
        {
            LogOnEmailViewModel model = new LogOnEmailViewModel();

            return View(model);
        }

        [HandleHtmlError, HandleAuthentication]
        public ActionResult MyAccount()
        {
            var contracts = SessionManager.Instance.GetCustomerContracts().Where(a =>  !string.IsNullOrEmpty(a.Status) && a.Status.ToLower() == "active").ToList();
            var profile = SessionManager.Instance.GetSalesForceContact();
            var accViewModel = new MyAccountViewModel()
            {
                Profile = Mapper.Map<ChargeBeeService.SalesForceContact, ProfileViewModel>(profile),
                Contracts = Mapper.Map<List<ChargeBeeService.AAPContract>, List<AAPContractViewModel>>(contracts)
            };
            return View(accViewModel);
        }
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Edit()
        {
            var profile = SessionManager.Instance.GetContactProfile();
            EditProfileViewModel accProfileViewModel =  new EditProfileViewModel();
            accProfileViewModel.Id = profile.UserId.ToString();
            accProfileViewModel.FirstName = profile.FirstName;
            accProfileViewModel.LastName = profile.LastName;
            accProfileViewModel.Company = profile.CompanyName;
            accProfileViewModel.DebtorNumber = profile.DebtorNumber;
            accProfileViewModel.PositionTitle = profile.ContactPosition;
            accProfileViewModel.Phone = profile.TelephoneNumber;
            accProfileViewModel.BusinessType = profile.IndustryCode;
            accProfileViewModel.Email = profile.EmailAddress;
            accProfileViewModel.RelevantSubjects = profile.RelevantSubjects;
            accProfileViewModel.ABN = profile.ABN;
            accProfileViewModel.Industries = new List<SelectListItem>();
            using (var cs = new ChargeBeeServiceProxy())
            {
                IList<ChargeBeeService.IndustryCode> industryList = cs.GetIndustryCodes();

                foreach (ChargeBeeService.IndustryCode code in industryList)
                {
                    accProfileViewModel.Industries.Add(new SelectListItem() { Text = code.Description.Trim(), Value = code.Code.ToString() });
                }
            }
            return View(accProfileViewModel);
        }

        /// <summary>
        /// Logon only for debugging purposes.
        /// </summary>
        [HttpPost]
        [HandleHtmlError]
        public ActionResult Logon(LogOnEmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return LogOn(model, string.Empty);
                }
                catch(FaultException <InvalidLoginFault>)
                {
                    ModelState.AddModelError("", ConfigurationManager.AppSettings["GenericErrorMessage"]);
                    return View(model);
                }
                catch (FaultException<ChargeBeeService.SessionAlreadyExistsFault> ex)
                {
                    ViewData["returnUrl"] = "/";
                    return View("SessionAlreadyExists", ex.Detail.ActiveSession);
                }
                catch (Exception ex)
                {
                    // an error occurred
                    MPLogger.LogException(ex);
                    ModelState.AddModelError("", ex.Message);
                    ModelState.AddModelError("", ConfigurationManager.AppSettings["GenericErrorMessage"]);
                    return View(model);
                }
            }

            //insuffecient login informaion.
            return View(model);
        }
        /// <summary>
        /// Edit account
        /// </summary>
        /// <param name="submitModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitEdit(EditProfileViewModel submitModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (SessionManager.Instance.UpdateAccount(submitModel))
                    {
                        TempData["message"] = "Your profile information saved successfully.";
                        return RedirectToAction("MyAccount", "Account");
                    }
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View("Edit", submitModel);
        }
        /// <summary>
        /// Forgot password only for debugging purposes
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [HandleHtmlError]
        public ActionResult ForgotPassword(LogOnEmailViewModel model)
        {
            try
            {
                SessionManager.Instance.SendForgotPasswordEmail(model.EmailAddress);
            }
            catch (FaultException<InvalidLoginFault> ex)
            {
                //account doesn't exist
                ModelState.AddModelError("", ex.Message);
                return View("Logon", model);
            }
            catch (Exception)
            {
                //internal error
                ModelState.AddModelError("", ConfigurationManager.AppSettings["GenericErrorMessage"]);
                return View("Logon", model);
            }

            //password reset email sent
            ModelState.AddModelError("", "Password reset email sent");
            return View("Logon", model);
        }

        /// <summary>
        /// Logon using an email address and password as credentials.
        /// </summary>
        [HandleHtmlError]
        public ActionResult LogOnUsingEmailAddress(LogOnEmailViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return LogOn(model, returnUrl);
                }
                catch (FaultException<InvalidLoginFault>)
                {
                    return Redirect(LoginFailedURL);
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                    return Redirect(LoginFailedURL);
                }
            }

            //insuffecient login informaion.
            return Redirect(InsufficientLogonDetailsURL);
        }

        private ActionResult LogOn(LogOnEmailViewModel model, string returnUrl)
        {
            //Validate User here.
            try
            {
                if (SessionManager.Instance.LogOnUsingEmailAddress(model.EmailAddress, model.Password))
                {
                    return RedirectDefaultOrPath(returnUrl, SessionManager.Instance.CurrentUser.MustChangePassword);
                }
                else
                {
                    return Redirect(LoginFailedURL);
                }
            }
            catch (FaultException<ChargeBeeService.LogonLicensesExceededFault> lex)
            {
                var exceededModel = new LicensesExceededViewModel();
                exceededModel.LicenseCount = lex.Detail.LicenseCount;
                exceededModel.ActiveSessions = lex.Detail.ActiveSessions.ToList();

                return View("LicensesExceeded", exceededModel);
            }
            catch (FaultException<ChargeBeeService.SessionAlreadyExistsFault> ex)
            {
                ViewData["returnUrl"] = returnUrl;
                return View("SessionAlreadyExists", ex.Detail.ActiveSession);
            }
            catch (FaultException<ChargeBeeService.AgreeToTermsFault>)
            {
                return View("AgreeToTerms", new AgreeToTermsViewModel { EmailAddress = model.EmailAddress, ReturnUrl = returnUrl });
            }
        }

        /// <summary>
        /// Logon using a session for this system which is already created for us by someone else.
        /// </summary>
        [HandleHtmlError]
        public ActionResult LogOnUsingSession(string session, string returnUrl)
        {
            if (!string.IsNullOrWhiteSpace(session) && session != "0")
            {
                //Validate User here.
                try
                {
                    if (SessionManager.Instance.LogOnUsingSession(session))
                    {
                        return RedirectDefaultOrPath(returnUrl, SessionManager.Instance.CurrentUser.MustChangePassword);
                    }
                    else
                    {
                        return Redirect(LoginFailedURL);
                    }
                }
                catch (FaultException<ChargeBeeService.SessionAlreadyExistsFault> ex)
                {
                    ViewData["returnUrl"] = returnUrl;
                    return View("SessionAlreadyExists", ex.Detail.ActiveSession);
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                    return Redirect(LoginFailedURL);
                }
            }
            else
            {
                //insuffecient login informaion.
                return Redirect(InsufficientLogonDetailsURL);
            }
        }

        /// <summary>
        /// Logon using a session from another system as credentials.
        /// </summary>
        [HandleHtmlError]
        public ActionResult LogOnUsingOtherSession(string session, string returnUrl)
        {
            if (!string.IsNullOrWhiteSpace(session) && session != "0")
            {
                //Validate User here.
                try
                {
                    if (SessionManager.Instance.LogOnUsingOtherSession(session))
                    {
                        return RedirectDefaultOrPath(returnUrl, SessionManager.Instance.CurrentUser.MustChangePassword);
                    }
                    else
                    {
                        return Redirect(LoginFailedURL);
                    }
                }
                catch (FaultException<ChargeBeeService.LogonLicensesExceededFault> lex)
                {
                    var exceededModel = new LicensesExceededViewModel();
                    exceededModel.LicenseCount = lex.Detail.LicenseCount;
                    exceededModel.ActiveSessions = lex.Detail.ActiveSessions.ToList();

                    return View("LicensesExceeded", exceededModel);
                }
                catch (FaultException<ChargeBeeService.SessionAlreadyExistsFault> ex)
                {
                    ViewData["returnUrl"] = returnUrl;
                    return View("SessionAlreadyExists", ex.Detail.ActiveSession);
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                    return Redirect(LoginFailedURL);
                }
            }
            else
            {
                //insuffecient login informaion.
                return Redirect(InsufficientLogonDetailsURL);
            }
        }

        /// <summary>
        /// Logon using a session from the same system. The existing session is killed in the process.
        /// </summary>
        [HandleHtmlError]
        public ActionResult LogOnReplacingSession(string session, string returnUrl)
        {
            if (!string.IsNullOrWhiteSpace(session) && session != "0")
            {
                //Validate User here.
                try
                {
                    if (SessionManager.Instance.LogOnReplacingSession(session))
                    {
                        return RedirectDefaultOrPath(returnUrl, SessionManager.Instance.CurrentUser.MustChangePassword);
                    }
                    else
                    {
                        return Redirect(LoginFailedURL);
                    }
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                    return Redirect(LoginFailedURL);
                }
            }
            else
            {
                //insuffecient login informaion.
                return Redirect(InsufficientLogonDetailsURL);
            }
        }

        //
        // GET: /Account/LogOff
        [HandleHtmlError]
        public ActionResult LogOff()
        {
            SessionManager.Instance.LogOff();

            return Redirect(LoginPageURL);
        }

        /// <summary>
        /// Return a page to allow a User to change their password.
        /// </summary>
        /// <param name="SessionKey"></param>
        [HandleHtmlError ]
        public ActionResult ChangePassword(string SessionKey, string returnUrl)
        {
            if (string.IsNullOrWhiteSpace(SessionKey))
            {
                if (!SessionManager.Instance.IsUserLoggedIn())
                    return Redirect(InvalidSessionURL);

                return View(new ChangePasswordViewModel { SessionKey = SessionManager.Instance.CurrentUser.SessionKey, ReturnUrl = returnUrl });
            }

            return View(new ChangePasswordViewModel { SessionKey = SessionKey, ReturnUrl = returnUrl });
        }

        [HttpPost]
        [HandleHtmlError]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var cs = new ChargeBeeServiceProxy())
                {
                    try
                    {
                        DBSession dbSession = cs.Proxy.ChangePassword(model.NewPassword, model.SessionKey);

                        if (dbSession == null)
                            return Redirect(InvalidSessionURL);

                        // We may have been given a new session, so update it again.
                        SessionManager.Instance.InitialiseSessionChargeBee(dbSession);

                        // Redirect to the search page.
                        //return RedirectToAction("Index", "Search");
                        return RedirectDefaultOrPath(model.ReturnUrl, false);
                    }
                    catch (FaultException<ChargeBeeService.LogonLicensesExceededFault> lex)
                    {
                        var exceededModel = new LicensesExceededViewModel();
                        exceededModel.LicenseCount = lex.Detail.LicenseCount;
                        exceededModel.ActiveSessions = lex.Detail.ActiveSessions.ToList();

                        // Clear the session to stop the current temporary password change session from being picked up.
                        Session.Clear();

                        return View("LicensesExceeded", exceededModel);
                    }
                    catch (FaultException<ChargeBeeService.SessionAlreadyExistsFault> ex)
                    {
                        ViewData["returnUrl"] = model.ReturnUrl;
                        return View("SessionAlreadyExists", ex.Detail.ActiveSession);
                    }
                    catch (FaultException<InvalidSessionFault>)
                    {
                        return Redirect(InvalidSessionURL);
                    }
                    catch (FaultException<ChargeBeeService.AgreeToTermsFault> ex)
                    {
                        return View("AgreeToTerms", new AgreeToTermsViewModel { EmailAddress = ex.Detail.EmailAddress, ReturnUrl = model.ReturnUrl });
                    }
                    catch (Exception ex)
                    {
                        MPLogger.LogException(ex);
                        return Redirect(LoginFailedURL);
                    }
                }
            }
            else
                return View(model);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [HttpPost]
        public ActionResult AgreeToTerms(AgreeToTermsViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    SessionManager.Instance.AcknowledgeTermsReadAndLogin(model.EmailAddress, model.Password);
                    return RedirectDefaultOrPath(model.ReturnUrl, SessionManager.Instance.CurrentUser.MustChangePassword);
                }
                catch (FaultException<ChargeBeeService.LogonLicensesExceededFault> lex)
                {
                    var exceededModel = new LicensesExceededViewModel();
                    exceededModel.LicenseCount = lex.Detail.LicenseCount;
                    exceededModel.ActiveSessions = lex.Detail.ActiveSessions.ToList();

                    return View("LicensesExceeded", exceededModel);
                }
                catch (FaultException<ChargeBeeService.SessionAlreadyExistsFault> ex)
                {
                    ViewData["returnUrl"] = model.ReturnUrl;
                    return View("SessionAlreadyExists", ex.Detail.ActiveSession);
                }
                catch (FaultException<InvalidLoginFault> invalid)
                {
                    ModelState.AddModelError("Password", invalid.Message);
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                    ModelState.AddModelError("Password", ConfigurationManager.AppSettings["GenericErrorMessage"]);
                    return View(model);
                }
            }

            return View(model);
        }

        [HttpPost, HandleAjaxError]
        public void AcceptBrowserCompatibility()
        {
            Response.Cookies.Add(new HttpCookie("outdatedBrowser", "true"));
        }

        [HandleAuthentication, HandleHtmlError]
        public ActionResult Journalists()
        {
            if (SessionManager.Instance.CurrentUser.HasJournalistsWebAccess)
                return Redirect($"{ConfigurationManager.AppSettings["JournalistsSiteLogon"]}?session={SessionManager.Instance.CurrentUser.SessionKey}");

            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        [HandleAuthentication, HandleHtmlError]
        public ActionResult Distribution()
        {
            if (SessionManager.Instance.CurrentUser.HasDistributionWebAccess)
                return Redirect($"{ConfigurationManager.AppSettings["DistributionSiteLogon"]}?session={SessionManager.Instance.CurrentUser.SessionKey}");

            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult RedirectDefaultOrPath(string returnUrl, bool mustChangePassword = false)
        {
            if (mustChangePassword)
                return RedirectToAction("ChangePassword", "Account");
            else if (!string.IsNullOrWhiteSpace(returnUrl))
                return Redirect(HttpUtility.UrlDecode(returnUrl));
            else if(Request.UrlReferrer != null)
            {
                NameValueCollection col = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);

                if (col["redirect"] != null)
                {
                    MPLogger.LogMessage("Redirecting to " + col["redirect"].ToString());
                    return Redirect("~/" + HttpUtility.UrlDecode(col["redirect"].ToString()));
                }
            }

            // Redirect to the search page.
            return Redirect("~/");
        }
    }
}
