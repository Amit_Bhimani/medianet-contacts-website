﻿using System.Web.Mvc;
using System.Web.UI;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Services.Contact;
using Medianet.Contacts.Web.Services.Dashboard;
using Medianet.Contacts.Web.Services.Outlet;

namespace Medianet.Contacts.Web.Controllers
{
    public class MediaMovementsController : BaseController
    {
        #region Field

        private readonly IDashboardService _dashboardService;

        private readonly IOutletService _outletService;

        private readonly IContactService _contactService;

        #endregion

        public MediaMovementsController() : base()
        {
            _dashboardService = DashboardService.CreateDashboardService();
            _outletService = OutletService.CreateOutletService();
            _contactService = ContactService.CreateContactService();
        }

        #region MediaMovements

        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index()
        {
            var mediaMovementLists = _dashboardService.GetMediaMovements(false, 1, 50,
                SessionManager.Instance.CurrentUser);
            return View(mediaMovementLists);
        }

        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Outlet(string outletId)
        {
            var mediaMovementLists = _outletService.GetOutletMediaMovements(outletId, 1, 50,
                SessionManager.Instance.CurrentUser);

            return View("~/Views/MediaMovements/Index.cshtml",mediaMovementLists);
        }

        [HttpGet]
        [HandleHtmlError, HandleAuthentication]
        public ActionResult Contact(string contactId)
        {
            var mediaMovementLists = _contactService.GetContactMediaMovements(contactId, 1, 50,
                SessionManager.Instance.CurrentUser);

            return View("~/Views/MediaMovements/Index.cshtml", mediaMovementLists);
        }
        #endregion

    }
}
