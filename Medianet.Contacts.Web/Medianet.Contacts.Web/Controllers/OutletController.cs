﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.Services.Outlet;
using Medianet.Contacts.Web.ViewModels.Outlet;
using Medianet.Contacts.Web.ProxyWrappers;

namespace Medianet.Contacts.Web.Controllers
{
    public class OutletController : BaseController
    {
        #region Field

        private readonly IOutletService _outletService;

        public static string OutletNotFound { get { return "Either the Outlet does not exist or you do not have permissions to view the Outlet."; } }

        public OutletViewModel OutletViewModel { get; set; }

        #endregion

        public OutletController() : base()
        {
            _outletService = OutletService.CreateOutletService();
        }

        protected override void Dispose(bool disposing)
        {
            if (_outletService != null) _outletService.Dispose();
            base.Dispose(disposing);
        }

        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(string id, string recordType)
        {
            if (string.IsNullOrEmpty(id) && string.IsNullOrEmpty(recordType))
            {
                ViewBag.Message = OutletNotFound;
            }
            else
            {
                GetOutletDetails(id, recordType);

                if (OutletViewModel == null)
                    ViewBag.Message = OutletNotFound;
                else
                    LogOutletView(id, recordType);
            }

            return View(OutletViewModel);
        }
        
        [HandleAjaxError, HandleAjaxAuthorization]
        public PartialViewResult OutletPreview(string outletId, string recordType)
        {
            RecordType rt = (RecordType)Enum.Parse(typeof(RecordType), recordType);
            var outletViewModel = new OutletViewModel();

            if (rt == RecordType.MediaOutlet)
            {
                outletViewModel = _outletService.GetOutletPreview(outletId, (RecordType)Enum.Parse(typeof(RecordType), recordType), CurrentUser);
            }
            else {
                outletViewModel = _outletService.GetOutlet(outletId, (RecordType)Enum.Parse(typeof(RecordType), recordType), CurrentUser);
            }

            ViewBag.UserId = CurrentUser.UserID;
            return PartialView("_OutletPreviewPartial", outletViewModel);
        }

        [HandleAjaxError]
        [HandleAjaxAuthorization]
        public PartialViewResult GetOutletMediaMovements(string outletId)
        {
           var viewModel = _outletService.GetOutletMediaMovements(outletId, 1 , 5, CurrentUser);
           return PartialView("_OutletMediaMovementsPartial", viewModel);
        }

        [NonAction]
        public string GetOutletLogo(string logoFileName)
        {
            try
            {
                string path = _outletService.GetOutletLogo(logoFileName, CurrentUser);

                return path;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        #region Methods

        private OutletViewModel GetOutletDetails(string outletId, string recordType)
        {
            OutletViewModel = _outletService.GetOutlet(outletId, (RecordType)Enum.Parse(typeof(RecordType), recordType), CurrentUser);
            
            ViewBag.UserId = CurrentUser.UserID;
            return OutletViewModel;
        }

        private void LogOutletView(string outletId, string recordType)
        {
            var recordTypeEnum = (RecordType)Enum.Parse(typeof(RecordType), recordType);

            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                if (recordTypeEnum == RecordType.MediaOutlet)
                {
                    mediaContactsService.LogMediaOutletView(outletId, CurrentUser.UserID, CurrentUser.SessionKey);
                }
            }
        }

        #endregion
    }
}
