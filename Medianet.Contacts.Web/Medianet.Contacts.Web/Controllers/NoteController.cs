﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Medianet.Contacts.Web.Filters;
using System.Collections.Generic;
using System.Net;
using Medianet.Contacts.Web.Services.Note;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.List;
using Medianet.Contacts.Web.Services.List;
using Medianet.Contacts.Web.ViewModels.Note;
using System.Web.UI;

namespace Medianet.Contacts.Web.Controllers
{
    public class NoteController : BaseController
    {
        private readonly INoteService _noteService;

        public NoteController() : base()
        {
            _noteService = NoteService.CreateNoteService();
        }
        
        [HandleAjaxError, HandleAjaxAuthorization]
        public PartialViewResult PinnedNotes(string contactId, string outletId, string recordType)
        {
            var paginatesNotes = GetNotes(contactId, outletId, recordType, true);
            paginatesNotes.Pagination.CalculatePagination();
            paginatesNotes.Pagination.CurrentPageOnly = true;
            
            return PartialView("Note/_PinnedNotesPartial", paginatesNotes);
        }
        
        [HandleAjaxError, HandleAjaxAuthorization]
        public PartialViewResult UnPinnedNotes(string contactId, string outletId, string recordType)
        {
            var paginatesNotes = GetNotes(contactId, outletId, recordType, false);
            paginatesNotes.Pagination.CalculatePagination();
            paginatesNotes.Pagination.CurrentPageOnly = true;

            return PartialView("Note/_UnPinnedNotesPartial", paginatesNotes);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Rebind(string contactId, string outletId, string recordType, bool pinned, PaginationNoteViewModel model)
        {
            // Calculate pagination.
            model.CalculatePagination();

            // Get the current page.
             List<NoteViewModel> results = GetCurrentPagedNote(contactId, outletId, recordType, pinned, model.Page);
            model.Notes= results;

            if (model.ResultCount > ((model.Page - 1) * model.PageSize))
            {
                ; //@ ok
            }
            else
            {
                //@ may be we have deleted items and we need to go to the last page
                model.Page = 1 + (model.ResultCount / model.PageSize);
                results = GetCurrentPagedNote(contactId, outletId, recordType, pinned, model.Page);
                model.Notes = results;
                model.CalculatePagination();
            }

            // This prevents the form data from persisting.
            ModelState.Clear();

            var notes = new NoteResultsViewModel
            {
                Pagination = model,
            };
            
            return PartialView($"Note/{(pinned ? "_PinnedNotesPartial" : "_UnPinnedNotesPartial")}", notes);
        }

        private List<NoteViewModel> GetCurrentPagedNote(string contactId, string outletId, string recordType, bool pinned, int pageNumber)
        {
            var notes = GetNotes(contactId, outletId, recordType, pinned, pageNumber);

            return notes.Pagination.Notes;
        }

        [HandleAjaxError, HandleAjaxAuthorization]
        public ActionResult DeleteNote(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                _noteService.DeleteNote(Convert.ToInt32(id), CurrentUser.SessionKey);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        
        [HandleAjaxError, HandleAjaxAuthorization]
        public ActionResult PinUnpinNote(string id, string pinned)
        {
            if (!string.IsNullOrEmpty(id))
            {
                _noteService.PinUnpinnNote(Convert.ToInt32(id), Convert.ToBoolean(pinned), CurrentUser.SessionKey);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Create(string outletId, string contactId, string recordType)
        {
            var note = new NoteViewModel
            {
                OutletId = outletId,
                ContactId = contactId,
                UserId = CurrentUser.UserID,
                DebtorNumber = CurrentUser.DebtorNumber,
                RecordType = (RecordType)Enum.Parse(typeof(RecordType), recordType.ToString())
            };

            return PartialView("Note/_AddNotePartial", note);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult CreateBulk()
        {
            var note = new NoteViewModel
            {
                UserId = CurrentUser.UserID,
                DebtorNumber = CurrentUser.DebtorNumber
            };

            return PartialView("Note/_AddNotePartial", note);
        }

        [HttpGet]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult Update(string id, string recordType)
        {
            var note = _noteService.GetNote(Convert.ToInt32(id), recordType, CurrentUser.SessionKey);
            return PartialView("Note/_AddNotePartial", note);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult CreateUpdate(NoteViewModel note)
        {
            if (note.Id == 0)
            {
                _noteService.AddNote(note, CurrentUser.SessionKey);
            }
            else
            {
                _noteService.UpdateNote(note, CurrentUser.SessionKey);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        [HandleAjaxAuthorization, HandleAjaxError]
        public ActionResult CreateBulk(NoteViewModel note)
        {
            List<ListRecord> recordsToAdd = null;

            string[] myCheckedItems = GetItemsMergedDistinctChecked(note, ListCheckedItems);
            string[] myUncheckedItems = GetItemsMergedDistinctUnchecked(note, ListCheckedItems);

            ListResultsViewModel listItems;

            using (ListService myListService = ListService.CreateListService())
            {
                listItems = myListService.GetListItemsAll(note.ListId, note.ListSetId, CurrentUser.UserID, CurrentUser.SessionKey);
            }

            if (note.CheckboxState == CheckboxState.All)
            {
                myCheckedItems = listItems.Pagination.Results.Select(r => r.ToClientString()).ToArray();
            }
            else if (note.CheckboxState == CheckboxState.PartAll)
            {
                myCheckedItems =
                    listItems.Pagination.Results.Select(r => r.ToClientString()).Except(myUncheckedItems).ToArray();
            }

            recordsToAdd =
                new List<ListRecord>(
                    listItems.Pagination.Results.Where(r => myCheckedItems.Contains(r.ToClientString())));

            _noteService.AddNoteBulk(note, recordsToAdd, CurrentUser.SessionKey);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private NoteResultsViewModel GetNotes(string contactId, string outletId, string recordType, bool pinned,
                                             int pageNumber = 1, int pageSize = 0)
        {
            var paginatesNotes = new NoteResultsViewModel();
            var notes = new List<NoteViewModel>();
            if (pageSize == 0)
                pageSize = paginatesNotes.Pagination.PageSize;

            if (!string.IsNullOrEmpty(contactId) && !string.IsNullOrEmpty(outletId) && recordType != null)
            {
                notes = _noteService.GetContactNotes(
                    contactId, outletId, recordType, CurrentUser.UserID, CurrentUser.SessionKey).ToList();
            }
            else if (string.IsNullOrEmpty(contactId) && !string.IsNullOrEmpty(outletId) && recordType != null)
            {
                notes = _noteService.GetOutletNotes(outletId, recordType, CurrentUser.UserID, CurrentUser.SessionKey);
            }

            notes = notes.Where(n => n.Pinned == pinned).ToList().OrderByDescending(n => n.AAPNote).ThenByDescending(n => n.CreatedDate).ToList();

            paginatesNotes.Pagination.Notes = notes.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            paginatesNotes.Pagination.ResultCount = notes.Count;
            paginatesNotes.Pagination.PageSize = pageSize;
            return paginatesNotes;
        }
    }
}
