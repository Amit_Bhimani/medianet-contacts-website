﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Filters;
using Medianet.Contacts.Web.Services.Contact;
using Medianet.Contacts.Web.ViewModels.Contact;
using Medianet.Contacts.Web.NewsCentreArticle;
using System.Collections.Generic;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.ViewModels.Dashboard;

namespace Medianet.Contacts.Web.Controllers
{
    public class ContactController : BaseController
    {
        #region Field

        private readonly IContactService _contactService;

        public static string ContactNotFound { get { return "Either the Contact does not exist or you do not have permissions to view the Contact."; } }

        public ContactViewModel ContactViewModel { get; set; }

        #endregion

        public ContactController() : base()
        {
            _contactService = ContactService.CreateContactService();
        }

        protected override void Dispose(bool disposing)
        {
            if (_contactService != null) _contactService.Dispose();
            base.Dispose(disposing);
        }

        [HandleHtmlError, HandleAuthentication]
        public ActionResult Index(string contactId, string outletId, string recordType)
        {
            if (!string.IsNullOrEmpty(contactId) && !string.IsNullOrEmpty(recordType))
            {
                GetContactDetails(contactId, outletId, recordType);

                if (ContactViewModel == null)
                {
                    ViewBag.Message = ContactNotFound;
                    return View(ContactViewModel);
                }

                LogContactView(contactId, outletId, recordType);
            }
            else
            {
                ViewBag.Message = ContactNotFound;
            }

            return View(ContactViewModel);
        }
        
        [HandleAjaxError, HandleAjaxAuthorization]
        public ActionResult GetArticle(string articleId)
        {
            Article articles = PopulateArticle(articleId);

            return PartialView("_ArticlePopupPartial", articles);
        }

        [HttpPost]
        [HandleAjaxError, HandleAjaxAuthorization]
        public PartialViewResult MediaContactsAlsoViewed(string contactId, string outletId, string subjectCodeIds)
        {
            var list = new List<PeopleAlsoViewedViewModel>();

            if (CurrentUser.DataModules != null)
            {
                using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
                {
                    if (!string.IsNullOrEmpty(subjectCodeIds))
                    {
                        list = _contactService.GetContactViewSummary(contactId, outletId, subjectCodeIds, CurrentUser);
                        
                    }
                }
            }

            return PartialView("_PeopleAlsoViewedPartial", list.Any() ? list : null);
        }
        
        [HandleAjaxError, HandleAjaxAuthorization]
        public PartialViewResult ContactPreview(string contactId, string outletId, string recordType)
        {
            RecordType rt = (RecordType)Enum.Parse(typeof(RecordType), recordType);
            var contactViewModel = new ContactViewModel();

            if (rt == RecordType.MediaContact)
            {
                contactViewModel = _contactService.GetContactPreview(contactId, outletId, (RecordType)Enum.Parse(typeof(RecordType), recordType), CurrentUser);
            }
            else
            {
                contactViewModel = _contactService.GetContact(contactId, outletId, (RecordType)Enum.Parse(typeof(RecordType), recordType), CurrentUser, false);
            }
            return PartialView("_ContactPreviewPartial", contactViewModel);
        }

        [HandleAjaxError]
        [HandleAjaxAuthorization]
        public PartialViewResult GetContactMediaMovements(string contactId)
        {
            var viewModel = _contactService.GetContactMediaMovements(contactId, 1, 5, CurrentUser);

            return PartialView("_ContactMediaMovementsPartial", viewModel);
        }

        #region Methods

        private ContactViewModel GetContactDetails(string contactId, string outletId, string recordType)
        {
            ContactViewModel = _contactService.GetContact(contactId, outletId, (RecordType)Enum.Parse(typeof(RecordType), recordType), CurrentUser, true);

            ViewBag.UserId = CurrentUser.UserID;
            return ContactViewModel;
        }

        private Article PopulateArticle(string articleId)
        {
            NewsCentreArticle.Search ncSearch = new NewsCentreArticle.Search();
            ncSearch.Url = System.Configuration.ConfigurationManager.AppSettings["NewsCentreArticleFetchURL"];

            int custId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["NewsCentreCustomerID"]);
            ArticleResponse objArticle = ncSearch.GetArticleByID(custId, "", Convert.ToInt32(articleId));
            Article[] obj = objArticle.ArticleManager;

            if (obj != null && obj.Length > 0)
            {
                return obj.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        private void LogContactView(string contactId, string outletId, string recordType)
        {
            var recordTypeEnum = (RecordType)Enum.Parse(typeof(RecordType), recordType);

            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                if (recordTypeEnum == RecordType.MediaContact)
                {
                    mediaContactsService.LogMediaContactView(contactId, outletId, CurrentUser.UserID, CurrentUser.SessionKey);
                }
                else if (recordTypeEnum == RecordType.PrnContact)
                {
                    mediaContactsService.LogPrnContactView(contactId, outletId, CurrentUser.UserID, CurrentUser.SessionKey);
                }
            }
        }
        #endregion
    }
}
