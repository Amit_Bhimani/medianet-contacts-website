﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using Medianet.Contacts.Web.Common.BO;

namespace Medianet.Contacts.Web.Common
{
    /// <summary>
    /// Summary description for EmailContactUs
    /// </summary>
    public class EmailContactUs
    {
        String wwwroot = ConfigurationManager.AppSettings["RootUrl"];

        public bool mail_individual(GeneralContactUs contact)
        {
            string smtphost = ConfigurationManager.AppSettings["MailServer"];
            string MediaPeople = ConfigurationManager.AppSettings["ClientServiceEmail"];

            DateTime DS = DateTime.Now;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = smtphost;
            smtpClient.Port = 25;

            // This part goes to admin
            String mailcontent = get_mail_contents(contact, HttpContext.Current.Server.MapPath("~/Content/Emails/ContactUs.htm"));
            MailMessage Admin_mail_msg = new MailMessage();
            MailAddress fromAddress = new MailAddress(MediaPeople, "Medianet");
            MailAddress replyTo = new MailAddress(contact.Email, contact.Email);
            MailAddress toAddress = new MailAddress(MediaPeople, "Medianet");

            Admin_mail_msg.From = fromAddress;
            Admin_mail_msg.To.Add(toAddress);
            Admin_mail_msg.ReplyToList.Add(replyTo);

            Admin_mail_msg.Subject = ConfigurationManager.AppSettings["ContactUsCustomerSubject"] + " - " + DS.ToString("dd/MM/yyyy hh:mm:ss:tt");
            Admin_mail_msg.IsBodyHtml = true;
            Admin_mail_msg.Body = mailcontent;

            // This part goes to User
            String mailcontent_user = get_mail_contents(contact, HttpContext.Current.Server.MapPath("~/Content/Emails/ContactUs.htm"));
            MailMessage user_mail_msg = new MailMessage();
            MailAddress noreply_u = new MailAddress(MediaPeople, "Medianet");
            MailAddress fromAddress_u = new MailAddress(MediaPeople, "Medianet");
            MailAddress toAddress_u = new MailAddress(contact.Email, contact.FirstName.Replace("''", "'"));

            user_mail_msg.From = noreply_u;
            user_mail_msg.To.Add(toAddress_u);
            user_mail_msg.ReplyToList.Add(fromAddress_u);

            user_mail_msg.Subject = ConfigurationManager.AppSettings["ContactUsCustomerSubject"] + " - " + DS.ToString("dd/MM/yyyy hh:mm:ss:tt");
            user_mail_msg.IsBodyHtml = true;
            user_mail_msg.Body = mailcontent_user;

            try
            {
                smtpClient.Send(Admin_mail_msg);
                smtpClient.Send(user_mail_msg);

                return true;
            }
            catch (SmtpException smtpex)
            {
                throw smtpex;
            }
        }

        private String get_mail_contents(GeneralContactUs regobj, String path)
        {
            StreamReader obj = File.OpenText(path);
            string mailcontent = obj.ReadToEnd();
            HttpContext ctx = HttpContext.Current;
            string fname = ctx.Server.HtmlEncode(regobj.FirstName);
            string subject = ctx.Server.HtmlEncode(regobj.Subject);
            string comment = ctx.Server.HtmlEncode(regobj.Comment);
            string telephone = ctx.Server.HtmlEncode(regobj.PhoneNo);
            string email = ctx.Server.HtmlEncode(regobj.Email);
            string ipaddress = regobj.IpAddress;
            string referral = regobj.Referer;
            string CompanyName = ctx.Server.HtmlEncode(regobj.CompanyName);
            string UserLogoName = ctx.Server.HtmlEncode(regobj.LogonName);
            fname = fname.Replace("''", "'");
            subject = subject.Replace("''", "'");
            comment = comment.Replace("''", "'");
            mailcontent = Regex.Replace(mailcontent, "#fname#", fname);
            mailcontent = Regex.Replace(mailcontent, "#subject#", subject);
            mailcontent = Regex.Replace(mailcontent, "#comment#", comment);
            mailcontent = Regex.Replace(mailcontent, "#telephone#", telephone);
            mailcontent = Regex.Replace(mailcontent, "#email#", email);
            mailcontent = Regex.Replace(mailcontent, "#IpAddress#", ipaddress);
            mailcontent = Regex.Replace(mailcontent, "#referral#", referral);
            mailcontent = Regex.Replace(mailcontent, "#UserLogoName#", UserLogoName);
            mailcontent = Regex.Replace(mailcontent, "#CompanyName#", CompanyName);
            mailcontent = Regex.Replace(mailcontent, "#wwwroot#", wwwroot);
            mailcontent = Regex.Replace(mailcontent, "#ClientServicePhoneNo#", ConfigurationManager.AppSettings["ClientServicePhoneNo"]);
            mailcontent = Regex.Replace(mailcontent, "#ClientServiceEmail#", ConfigurationManager.AppSettings["ClientServiceEmail"]);
            obj.Close();
            return mailcontent;
        }

        public void SendEmail_Distribution(string filenamePrefix, string fromEmail, string toEmail, string subject, Stream headerStream,
            Stream emailCsvStream, Stream emailTextStream, Stream releaseStream,
            string releaseFileName, Stream attachmentStream, string attachmentFileName, string body, string sender, string senderEmail)
        {
            string smtphost = ConfigurationManager.AppSettings["MailServer"];
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = smtphost;
            smtpClient.Port = 25;

            // This part goes to admin
            using (var distributionMailMsg = new MailMessage())
            {
                MailAddress fromAddress = new MailAddress(fromEmail, sender);
                MailAddress toAddress = new MailAddress(toEmail, "Medianet");

                distributionMailMsg.From = fromAddress;
                distributionMailMsg.To.Add(toAddress);
                distributionMailMsg.ReplyToList.Add(new MailAddress(senderEmail, sender));
                distributionMailMsg.Subject = subject;
                distributionMailMsg.IsBodyHtml = true;
                distributionMailMsg.Body = body;

                Attachment release = new Attachment(releaseStream, releaseFileName);
                distributionMailMsg.Attachments.Add(release);

                if (attachmentStream != null)
                {
                    Attachment attachment = new Attachment(attachmentStream, attachmentFileName);
                    distributionMailMsg.Attachments.Add(attachment);
                }

                if (headerStream != null)
                {
                    Attachment header = new Attachment(headerStream, filenamePrefix + "_header.txt");
                    distributionMailMsg.Attachments.Add(header);
                }

                if (emailCsvStream != null)
                {
                    Attachment emailCsv = new Attachment(emailCsvStream, filenamePrefix + "_email.csv");
                    distributionMailMsg.Attachments.Add(emailCsv);
                }

                if (emailTextStream != null)
                {
                    Attachment emailTxt = new Attachment(emailTextStream, filenamePrefix + "_email.txt");
                    distributionMailMsg.Attachments.Add(emailTxt);
                }

                smtpClient.Send(distributionMailMsg);
            }
        }
    }
}
