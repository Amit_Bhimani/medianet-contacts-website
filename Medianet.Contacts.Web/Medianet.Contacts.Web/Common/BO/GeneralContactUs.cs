﻿using System;

namespace Medianet.Contacts.Web.Common.BO
{
    /// <summary>
    /// Summary description for GeneralContactUs
    /// </summary>
    public class GeneralContactUs
    {
        public GeneralContactUs()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private int _id = 0;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _FirstName = string.Empty;
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        private string _Email = string.Empty;
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
    
        private string _ClientServiceEmail = string.Empty;
        public string ClientServiceEmail
        {
            get { return _ClientServiceEmail; }
            set { _ClientServiceEmail = value; }
        }

        private string _PhoneNo = string.Empty;
        public string PhoneNo
        {
            get { return _PhoneNo; }
            set { _PhoneNo = value; }
        }
        private string _Subject = string.Empty;
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        private string _Comment = string.Empty;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private string _IpAddress = string.Empty;
        public string IpAddress
        {
            get { return _IpAddress; }
            set { _IpAddress = value; }
        }
    
        private string _Referer = string.Empty;
        public string Referer
        {
            get { return _Referer; }
            set { _Referer = value; }
        }

        private int status = 0;
        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        private DateTime _createdDate = DateTime.Now;
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private string _CompanyName = string.Empty;
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        private string _LogonName = string.Empty;
        public string LogonName
        {
            get { return _LogonName; }
            set { _LogonName = value; }
        }
    }
}