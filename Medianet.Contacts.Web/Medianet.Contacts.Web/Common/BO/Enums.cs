﻿using System.ComponentModel;

namespace Medianet.Contacts.Web.Common.BO
{
    //public enum SearchType { Quick = 0, Advanced = 1, Outlet = 2 };
    //public enum SearchContext { People = 0, Outlet = 1, Both = 2 };
    public enum ShowResultType
    {
        [Description("All Records")]
        A = 0,
        [Description("Only Medianet data records ")]
        M = 1,
        [Description("Only my private records")]
        P = 2
    };

    public enum GroupState
    {
        New = 0,
        Existing = 1
    }
}