﻿using System;

namespace Medianet.Contacts.Web.Common.BO
{
    /// <summary>
    /// Summary description for OutletDetailsHistory
    /// </summary>
    public class OutletDetailsHistory
    {
        public OutletDetailsHistory()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private string _OutletId = string.Empty;
        public string OutletId
        {
            get { return _OutletId; }
            set { _OutletId = value; }
        }

        private int _id = 0;
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _UserId = 0;
        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        private int _Visibility = 0;
        public int Visibility
        {
            get { return _Visibility; }
            set { _Visibility = value; }
        }

        private string _ContactId = string.Empty;
        public string ContactId
        {
            get { return _ContactId; }
            set { _ContactId = value; }
        }

        private string _ContactName = string.Empty;
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        private string _OutletName = string.Empty;
        public string OutletName
        {
            get { return _OutletName; }
            set { _OutletName = value; }
        }

        private DateTime _CreatedDate = DateTime.Now;
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private DateTime _ModifiedDate = DateTime.Now;
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }

        private string _Email = string.Empty;
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _Fname = string.Empty;
        public string FirstName
        {
            get { return _Fname; }
            set { _Fname = value; }
        }


        private string _OldOutletName = string.Empty;

        public string OldOutletName
        {
            get { return _OldOutletName; }
            set { _OldOutletName = value; }
        }

        private string _OldContactName = string.Empty;

        public string OldContactName
        {
            get { return _OldContactName; }
            set { _OldContactName = value; }
        }



        private string _UpdateRequest = string.Empty;
        public string UpdateRequest
        {
            get { return _UpdateRequest; }
            set { _UpdateRequest = value; }
        }

        private string _CompanyName = string.Empty;
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        private string _LogonName = string.Empty;
        public string LogonName
        {
            get { return _LogonName; }
            set { _LogonName = value; }
        }


    }
}