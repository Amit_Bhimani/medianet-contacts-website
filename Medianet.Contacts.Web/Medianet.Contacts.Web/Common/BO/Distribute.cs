﻿using System;

namespace Medianet.Contacts.Web.Common.BO
{
    /// <summary>
    /// Summary description for Distribute
    /// </summary>
    public class Distribute
    {
        public Distribute()
        {
            DebtorNumber = string.Empty;
            CompanyName = string.Empty;
            SubmittedUserId = 0;
            ReleaseDescription = string.Empty;
            WorkPhone = string.Empty;
            Email = string.Empty;
            Mobile = string.Empty;
            CustRef = string.Empty;
            Comments = string.Empty;
            Hold = 0;
            Comment = string.Empty;
        }

        public string DebtorNumber { get; set; }

        /// <summary>
        /// This field is used to set orignator name in mn_job table
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// This field is used to set Submitted Userid in mn_job table
        /// </summary>
        public int SubmittedUserId { get; set; }

        /// <summary>
        /// This field is used to set ReleaseDescription in mn_job table
        /// </summary>
        public string ReleaseDescription { get; set; }

        /// <summary>
        /// This field is used to set work phone in mn_job table
        /// </summary>
        public string WorkPhone { get; set; }

        /// <summary>
        /// This field is used to set Email Orignator in mn_job table
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// This field is used to set Mobile in mn_job table
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// This field is used to set customer Reference in mn_job table
        /// </summary>
        public string CustRef { get; set; }

        /// <summary>
        /// This will be used to set comments column in mn_job
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// This field is used to set hold field  in mn_job table
        /// </summary>
        public int Hold { get; set; }

        /// <summary>
        /// this will set the comment column in mn_job table
        /// </summary>
        public string Comment { get; set; }

        public DateTime HoldDateTime { get; set; }

        public bool AapNewsWires { get; set; }
    }
}