﻿using System;

namespace Medianet.Contacts.Web.Common.BO
{
    /// <summary>
    /// Summary description for GroupList
    /// </summary>
    public class GroupList
    {
        public GroupList()
        {
        }

        private int _grouplistid = 0;
        public int Grouplistid
        {
            get { return _grouplistid; }
            set { _grouplistid = value; }
        }

        private string _listname = string.Empty;
        public string Listname
        {
            get { return _listname; }
            set { _listname = value; }
        }

        private int status = 0;
        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        private DateTime _createddate = DateTime.Now;
        public DateTime Createddate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }

        private DateTime _updateddate = DateTime.Now;
        public DateTime Updateddate
        {
            get { return _updateddate; }
            set { _updateddate = value; }
        }
    
        private int _groupid = 0;
        public int Groupid
        {
            get { return _groupid; }
            set { _groupid = value; }
        }

        private int _visibility;
        public int Visibility
        {
            get { return _visibility; }
            set { _visibility = value; }
        }

        public bool IsPrivate
        {
            get
            {
                return (_visibility == 1);
            }
            set
            {
                _visibility = (value == true ? 1 : 0);
            }
        }

        private int _userId;
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
    }
}

