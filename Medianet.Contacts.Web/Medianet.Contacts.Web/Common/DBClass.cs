﻿using System;
using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Web.Common.Logging;

namespace Medianet.Contacts.Web.Common
{
    /// <summary>
    /// Summary description for DBClass
    /// </summary>
    public class DBClass : IDisposable
    {
        // private static bool isDeveloper = false;
        private WebDataClient.AAP.WebDataClient.MsgProtocol _WebDataClient = null;
        private string _DatabaseAlias;
        private string _error;
        private bool _inTran = false;
        public bool InTran { get { return _inTran; } set { _inTran = value; } }

        public DBClass()
        {
            _DatabaseAlias = System.Configuration.ConfigurationManager.AppSettings["WebDataServerAlias"];
        }

        public DBClass(string dbAlias)
        {
            _DatabaseAlias = dbAlias;
        }

        public void CloseDBConnection()
        {
            if (!(_WebDataClient == null))
            {
                if (InTran)
                {
                    if (!_WebDataClient.CommitTransaction(ref _error)) BasePage.LogException(new Exception(_error), typeof(DBClass).FullName, "CloseDBConnection", null);
                }

                if (_WebDataClient.IsConnected) _WebDataClient.Disconnect();
            }
        }

        /// <summary>
        /// This method fires Select Query or Procedure of select query.   
        /// </summary>
        /// <param name="query">This parameter takes the query or procedure name string</param>
        /// <param name="isStoredProcedure">This parameter checks whether it is query or procedure</param>
        /// <returns>@tbl</returns>
        public DataTable SelectQuery(string query, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                if (ds.Tables.Count == 0)
                    throw new Exception("No table returned.");

                return ds.Tables[0];
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }
        }

        public DataSet Select(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                return ds;
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// This query returns the single record
        /// </summary>
        /// <param name="query">String parameter takes query or stored procedures name.</param>
        /// <param name="sqlParams">Parameter for fetching single record</param>
        /// <param name="isStoredProcedure">Bool parameter checked whether it is procedure or query </param>
        /// <returns>@record</returns>
        public Object SelectScalarQuery(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return null;

                // Return the first column of the first row of the first table.
                return (object)ds.Tables[0].Rows[0][0];
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// This is method overriden method of SelectQuery where we can set the Sql parameters for where clause.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@tbl</returns>
        public DataTable SelectQuery(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                if (ds.Tables.Count == 0)
                    throw new Exception("No table returned.");

                return ds.Tables[0];
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }
        }

        public DataSet SelectQueryMultiple(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                return ds;
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to insert the data into database.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@result</returns>
        public int Insert(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return 0;

                // Return the first column of the first row of the first table.
                return int.Parse(ds.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }

        }

        /// <summary>
        /// This method is used to delete the data from database.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@result</returns>
        public int Delete(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return 0;

                // Return the first column of the first row of the first table.
                return int.Parse(ds.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to update the data from database.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@result</returns>
        public int Update(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                // Connect to the database if we haven't already.
                ConnectToDatabase();

                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return 0;

                // Return the first column of the first row of the first table.
                return int.Parse(ds.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex) {
                MPLogger.LogException(ex);
                throw ex;
            }
        }

        public bool BeginTransaction()
        {
            // Connect to the database if we haven't already.
            ConnectToDatabase();

            return _WebDataClient.BeginTransaction(ref _error, _DatabaseAlias);
        }

        public bool CommitTransaction()
        {
            return _WebDataClient.CommitTransaction(ref _error);
        }

        public bool RollBackTransaction()
        {
            return _WebDataClient.RollbackDatasetTransaction(ref _error);
        }

        private void ConnectToDatabase()
        {
            if (_WebDataClient == null || !(bool)_WebDataClient.IsConnected)
            {
                int maxWaitingTime;
                int remotePort;
                if (_WebDataClient == null)
                {
                    _WebDataClient = new WebDataClient.AAP.WebDataClient.MsgProtocol();
                }
                _WebDataClient.RemoteHost = System.Configuration.ConfigurationManager.AppSettings["WebDataServerHost"].ToString();
                int.TryParse(System.Configuration.ConfigurationManager.AppSettings["WebDataServerPort"], out remotePort);
                _WebDataClient.RemotePort = remotePort;

                if (int.TryParse(System.Configuration.ConfigurationManager.AppSettings["WebDataServerMaxWaitingTime"], out maxWaitingTime))
                {
                    _WebDataClient.MaxWaitingTime = maxWaitingTime;
                }
                _WebDataClient.Connect();
            }
        }

        #region Database format routines

        public static String FormatDatabaseString(Object pDBValue, String pDefault)
        {
            if (pDBValue == DBNull.Value)
                return pDefault;
            else
                return pDBValue.ToString();
        }

        //Public Shared Function FormatDatabaseChar(ByRef objDBValue As Object, ByVal cDefault As Char) As Char
        //    If objDBValue Is System.DBNull.Value Then
        //        FormatDatabaseChar = cDefault
        //    Else
        //        FormatDatabaseChar = CType(objDBValue, Char)
        //    End If
        //End Function

        public static int FormatDatabaseInt(Object pDBValue, int pDefault)
        {
            if (pDBValue == DBNull.Value)
                return pDefault;
            else
                return Convert.ToInt32(pDBValue);
        }

        public static bool FormatDatabaseBoolean(Object pDBValue, bool pDefault)
        {
            if (pDBValue == DBNull.Value)
                return pDefault;
            else
            {
                return Convert.ToBoolean(pDBValue);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            CloseDBConnection();
        }

        #endregion
    }
}




