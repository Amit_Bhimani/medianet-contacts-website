﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Medianet.Contacts.Web.Common.Model;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Medianet.Contacts.Web.Common
{
    public abstract class BasePage : System.Web.UI.Page
    {
        private static readonly int maxFormFieldLengthLogged = 200;

        private static readonly string _sessionListIdKey = "frmList_listId";
        public string SessionListIdKey { get { return _sessionListIdKey; } }

        private static readonly string _sessionListSetIdKey = "frmList_listSetId";
        public string SessionListSetIdKey { get { return _sessionListSetIdKey; } }

        public static string HomePage { get { return "/Search"; } }

        /// <summary>
        /// Gets the current logged in user.
        /// </summary>
        /// <returns><c>UserMaster</c> object if there is a user logged in, null otherwise.</returns>
        private User _currentUser = null;
        public User CurrentUser
        {
            get
            {
                if (IsUserLoggedIn())
                {
                    if (_currentUser == null)
                        _currentUser = (User)Session["Member"];
                }
                return _currentUser;
            }
            set
            {
                _currentUser = value;
            }
        }

        protected const string INTERNAL_ERROR = "Internal error processing request.";

        private DBClass _DBConnection;

        public DBClass DBConnection
        {
            get
            {
                return _DBConnection;
            }
        }

        public void InitDBConnection()
        {
            // Close the database connection. Otherwise WebDataServer keeps it open until it times out.
            if (!(_DBConnection == null))
                _DBConnection.CloseDBConnection();
            _DBConnection = new DBClass();
        }

        protected static string GetSessionDetails()
        {
            StringBuilder message = new StringBuilder("############# Session #############" + Environment.NewLine);
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Session != null)
                {
                    HttpContext state = HttpContext.Current;

                    var Session = state.Session;

                    if (Session != null)
                    {
                        message.Append("Session.Timeout       :  " + Convert.ToString(Session.Timeout) + Environment.NewLine);
                        message.Append("Session(\"SessionID\")  :" + (SessionManager.Instance.CurrentUser != null ? SessionManager.Instance.CurrentUser.SessionKey : "Null")  + Environment.NewLine);


                        if (Session.Keys != null)
                        {
                            message.Append("Session.Keys      : ");

                            foreach (string ky in Session.Keys)
                            {
                                message.Append(string.IsNullOrEmpty(ky) ? "Null" : ky + "; ");
                            }

                            message.Append(Environment.NewLine);
                        }
                        else
                            message.Append("Session.Keys      : nothing" + Environment.NewLine);
                    }
                    else
                        message.Append("Session is nothing" + Environment.NewLine);
                }
            }
            catch (Exception exc)
            {
                message.Append("An exception occurred while trying to execute function GetSessionDetails. Message: " + (string.IsNullOrEmpty(exc.Message) ? "Null" : exc.Message));
            }
            return message.ToString();
        }

        protected static string GetRequestDetails()
        {
            StringBuilder s = new StringBuilder("");
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    HttpContext state = HttpContext.Current;

                    var Request = state.Request;

                    var _with1 = Request.Browser;

                    s.Append("############# Browser Capabilities #############" + Environment.NewLine);
                    s.Append("Type = " + (string.IsNullOrEmpty(_with1.Type) ? "Null" : _with1.Type) + Environment.NewLine);
                    s.Append("Name = " + (string.IsNullOrEmpty(_with1.Browser) ? "Null" : _with1.Browser) + Environment.NewLine);
                    s.Append("Version = " + (string.IsNullOrEmpty(_with1.Version) ? "Null" : _with1.Version) + Environment.NewLine);
                    s.Append("Major Version = " + Convert.ToString(_with1.MajorVersion) + Environment.NewLine);
                    s.Append("Minor Version = " + Convert.ToString(_with1.MinorVersion) + Environment.NewLine);
                    s.Append("Platform = " + (string.IsNullOrEmpty(_with1.Platform) ? "Null" : _with1.Platform) + Environment.NewLine);
                    s.Append("Supports Cookies = " + Convert.ToString(_with1.Cookies) + Environment.NewLine);

                    s.Append("############# Request Details #############" + Environment.NewLine);
                    s.Append("URL = " + Convert.ToString(Request.Url == null ? "Null" : Request.Url.ToString()) + Environment.NewLine);
                    s.Append("HttpMethod = " + Request.HttpMethod.ToString() + Environment.NewLine);
                    s.Append("FilePath = " + Request.FilePath.ToString() + Environment.NewLine);
                    s.Append("UserHostAddress = " + Request.UserHostAddress + Environment.NewLine);
                    s.Append("RawUrl = " + Request.RawUrl + Environment.NewLine);

                }

            }
            catch (Exception ex)
            {
                s.Append("Error encountered while running GetRequestDetails. Error message: " +
                         (string.IsNullOrEmpty(ex.Message) ? "Null" : ex.Message));
            }

            return s.ToString();
        }

        protected static string GetFormDetails()
        {
            StringBuilder message = new StringBuilder("");

            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.RequestType.ToLower().Equals("post"))
                {
                    message.Append("############# Form Fields #############" + Environment.NewLine);
                    HttpContext state = HttpContext.Current;
                    var Request = state.Request;

                    foreach (string ky in Request.Form.Keys)
                    {
                        if (!string.IsNullOrEmpty(ky) && !ky.StartsWith("__"))
                            message.Append(string.Format("Key:  {0} \t Value: {1}", ky, (Request.Form[ky].ToString().Length > 0 ?
                                Request.Form[ky].ToString().Substring(0, Request.Form[ky].ToString().Length > maxFormFieldLengthLogged ? maxFormFieldLengthLogged : Request.Form[ky].ToString().Length) :
                                Request.Form[ky].ToString()) + Environment.NewLine));                  
                    }
                }
            }
            catch (Exception exc)
            {
                message.Append("An exception occurred while trying to execute function GetFormDetails. Message: " + (string.IsNullOrEmpty(exc.Message) ? "Null" : exc.Message));
            }
            return message.ToString();
        }

        private static string GetFormattedExceptionMessage(Exception Ex)
        {
            string message = "";

            try
            {
                if (Ex != null)
                {
                    message += "Message     : " + (string.IsNullOrEmpty(Ex.Message) ? "Null" : Ex.Message) + Environment.NewLine;

                    if (!string.IsNullOrEmpty(Ex.StackTrace))
                        message += "Stack Trace : " + Ex.StackTrace + Environment.NewLine;
                    if (Ex.InnerException != null && Ex.InnerException.Message != null)
                        message += "Inner exception message : " + Ex.InnerException.Message + Environment.NewLine;

                    Exception baseEx = Ex.GetBaseException();

                    if (baseEx != null)
                    {
                        message += "############# Base exception ############## : " + Environment.NewLine + "Message    : " +
                                   (string.IsNullOrEmpty(baseEx.Message) ? "Null" : baseEx.Message) + Environment.NewLine;
                    }
                }
                else
                {
                    message += "Exception passed in as nothing" + Environment.NewLine;
                }
            }
            catch (Exception exc)
            {
                message = "An exception occurred while trying to execute function GetFormattedExceptionMessage. Message: " +
                          (string.IsNullOrEmpty(exc.Message) ? "Null" : exc.Message);
            }
            return message;
        }

        public static void LogException(Exception ex, string PageName, string FunctionName, NameValueCollection Parameters)
        {
            LogEntry log = new LogEntry();
            StringBuilder pstr = new StringBuilder("");
            StringBuilder message = new StringBuilder(Environment.NewLine +
                                                      "################################# Error details #################################" + Environment.NewLine);

            try
            {
                message.Append(GetRequestDetails());
                message.Append(GetSessionDetails());
                message.Append(GetFormDetails());

                if (!string.IsNullOrEmpty(FunctionName))
                    message.Append("FunctionName     : " + Environment.NewLine + FunctionName + Environment.NewLine);

                if (Parameters != null)
                {
                    foreach (string k in Parameters.AllKeys)
                    {
                        pstr.Append(Convert.ToString(string.IsNullOrEmpty(k) ? "Null" : k) + " : " + Convert.ToString(string.IsNullOrEmpty(Parameters[k]) ? "Null" : Parameters[k]) + Environment.NewLine);
                    }

                    message.Append("############# Parameters #############" + Environment.NewLine + pstr.ToString());
                }

                message.Append("Page        : " + Convert.ToString(string.IsNullOrEmpty(PageName) ? "Null" : PageName) + Environment.NewLine);

                message.Append(GetFormattedExceptionMessage(ex));

                log.Message = message.ToString();
                log.Categories.Add("General");
                log.Severity = TraceEventType.Error;

                Logger.Write(log);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Checks if a user is logged in.
        /// </summary>
        /// <returns></returns>
        public bool IsUserLoggedIn()
        {
            return Session["Member"] != null;
        }

        /// <summary>
        /// Checks if current user is a trial user.
        /// </summary>
        /// <returns>Returns true if the current user is a trial user, otherwise false.</returns>
        public bool isTrialUser()
        {
            return IsUserLoggedIn() ? ((User)Session["Member"]).isTrial() : false;
        }

        public static string FormatTextAsHtml(string content, string emptyMessage = "") {
            var newContent = emptyMessage;

            if (!string.IsNullOrWhiteSpace(content)) {
                var urlMatch = @"\b(?:(?:https?|ftp|file)://|www\.|ftp\.)[-A-Z0-9+&@#/%=~_|$?!:,.]*[A-Z0-9+&@#/%=~_|$]";
                var emailMatch = @"((?:mailto:)?[A-Z0-9._%+-]+@[A-Z0-9._%-]+\.[A-Z]{2,4})\b";

                newContent = HttpUtility.HtmlEncode(content).Replace(Environment.NewLine, @"<br/>");

                newContent = Regex.Replace(newContent, urlMatch, delegate(Match m) {
                                                                                       var url = m.ToString();
                                                                                       if (!url.StartsWith("http", StringComparison.CurrentCultureIgnoreCase))
                                                                                           url = "http://" + url;

                                                                                       return string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url, m.ToString());
                }, RegexOptions.IgnoreCase);

                newContent = Regex.Replace(newContent, emailMatch, delegate(Match m) {
                                                                                         var url = m.ToString();
                                                                                         if (!url.StartsWith("mailto", StringComparison.CurrentCultureIgnoreCase))
                                                                                             url = "mailto:" + url;

                                                                                         return string.Format("<a href=\"{0}\">{1}</a>", url, m.ToString());
                }, RegexOptions.IgnoreCase);
            }

            return newContent;
        }
    }
}