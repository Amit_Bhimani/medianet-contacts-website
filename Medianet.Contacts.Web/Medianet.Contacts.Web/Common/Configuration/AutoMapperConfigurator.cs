﻿using AutoMapper;

namespace Medianet.Contacts.Web.Common.Configuration
{
    public class AutoMapperConfigurator
    {
        public static void Configure() {
            Mapper.Initialize(x => x.AddProfile<MediaPeopleAutoMapperProfile>());
        }
    }
}