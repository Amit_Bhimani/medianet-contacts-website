﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Common.ValueResolvers;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.Contact;
using Medianet.Contacts.Web.ViewModels.List;
using Medianet.Contacts.Web.ViewModels.Outlet;
using Medianet.Contacts.Web.ViewModels.PrivateContact;
using Medianet.Contacts.Web.ViewModels.PrivateOutlet;
using ReminderType = Medianet.Contacts.Web.Common.Model.ReminderType;
using Medianet.Contacts.Web.MediaContactsService;
using System.Globalization;
using Medianet.Contacts.Web.ViewModels.Account;
using Medianet.Contacts.Web.ViewModels.Note;

namespace Medianet.Contacts.Web.Common.Configuration
{
    public class MediaPeopleAutoMapperProfile : Profile
    {
        public const string VIEW_MODEL = "MediaPeopleAutoMapperProfile";

        public override string ProfileName
        {
            get { return VIEW_MODEL; }
        }

        protected override void Configure()
        {
            CreateMaps();
        }

        private static void CreateMaps()
        {
            Mapper.CreateMap<int, ReminderType>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src))
                .IgnoreAllNonExisting();


            Mapper.CreateMap<GroupViewModel, GroupBase>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<string, List<string>>().ConvertUsing(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList());

            int result;

            Mapper.CreateMap<AdvancedSearchViewModel, SearchOptionsAdvanced>()
                  .ForMember(dest => dest.PositionList, opt => opt.MapFrom(src => src.SelectedPositions))
                  .ForMember(dest => dest.ContactSubjectList, opt => opt.MapFrom(src => src.SelectedCSubjects))
                  .ForMember(dest => dest.ContactSubjectGroupList, opt => opt.MapFrom(src => src.SelectedCSubjectGroups))
                  .ForMember(dest => dest.ContactContinentIDList, opt => opt.MapFrom(src => src.SelectedCContinents))
                  .ForMember(dest => dest.ContactCountryIDList, opt => opt.MapFrom(src => src.SelectedCCountries))
                  .ForMember(dest => dest.ContactStateIDList, opt => opt.MapFrom(src => src.SelectedCStates))
                  .ForMember(dest => dest.ContactCityIDList, opt => opt.MapFrom(src => src.SelectedCCities))
                  .ForMember(dest => dest.ContactPostCodeList, opt => opt.MapFrom(src => string.IsNullOrWhiteSpace(src.SelectedCPostCodes) ? null : src.SelectedCPostCodes.ParseStringCSV()))
                  .ForMember(dest => dest.MediaTypeList, opt => opt.MapFrom(src => src.SelectedMediaTypes))
                  .ForMember(dest => dest.OutletSubjectList, opt => opt.MapFrom(src => src.SelectedOSubjects))
                  .ForMember(dest => dest.OutletSubjectGroupList, opt => opt.MapFrom(src => src.SelectedOSubjectGroups))
                  .ForMember(dest => dest.OutletFrequencyList, opt => opt.MapFrom(src => src.SelectedFrequencies))
                  .ForMember(dest => dest.OutletLanguageList, opt => opt.MapFrom(src => src.SelectedLanguages))
                  .ForMember(dest => dest.OutletStationFrequencyList, opt => opt.MapFrom(src => string.IsNullOrWhiteSpace(src.OutletStationFrequencyList) ? null : src.OutletStationFrequencyList.ParseStringCSV()))
                  .ForMember(dest => dest.OutletContinentIDList, opt => opt.MapFrom(src => src.SelectedOContinents))
                  .ForMember(dest => dest.OutletCountryIDList, opt => opt.MapFrom(src => src.SelectedOCountries))
                  .ForMember(dest => dest.OutletStateIDList, opt => opt.MapFrom(src => src.SelectedOStates))
                  .ForMember(dest => dest.OutletCityIDList, opt => opt.MapFrom(src => src.SelectedOCities))
                  .ForMember(dest => dest.OutletPostCodeList, opt => opt.MapFrom(src => string.IsNullOrWhiteSpace(src.SelectedOPostCodes) ? null : src.SelectedOPostCodes.ParseStringCSV()))
                  .ForMember(dest => dest.OutletCirculationMax, opt => opt.MapFrom(src => !string.IsNullOrWhiteSpace(src.OutletCirculationMax) && int.TryParse(src.OutletCirculationMax, out result) ? result : 0))
                  .ForMember(dest => dest.OutletCirculationMin, opt => opt.MapFrom(src => !string.IsNullOrWhiteSpace(src.OutletCirculationMin) && int.TryParse(src.OutletCirculationMin, out result) ? result : 0))
                  .ForMember(dest => dest.ListList, opt => opt.MapFrom(src => src.SelectedListIds))
                  .IgnoreAllNonExisting();

            // SearchOptionsAdvanced -> AdvancedSearchViewModel
            Mapper.CreateMap<SearchOptionsAdvanced, AdvancedSearchViewModel>()
                  .ForMember(dest => dest.SelectedPositions, opt => opt.MapFrom(src => src.PositionList))
                  .ForMember(dest => dest.SelectedCSubjects, opt => opt.MapFrom(src => src.ContactSubjectList))
                  .ForMember(dest => dest.SelectedCSubjectGroups, opt => opt.MapFrom(src => src.ContactSubjectGroupList))
                  .ForMember(dest => dest.SelectedCContinents, opt => opt.MapFrom(src => src.ContactContinentIDList))
                  .ForMember(dest => dest.SelectedCCountries, opt => opt.MapFrom(src => src.ContactCountryIDList))
                  .ForMember(dest => dest.SelectedCStates, opt => opt.MapFrom(src => src.ContactStateIDList))
                  .ForMember(dest => dest.SelectedCCities, opt => opt.MapFrom(src => src.ContactCityIDList))
                  .ForMember(dest => dest.SelectedCPostCodes, opt => opt.MapFrom(src => src.ContactPostCodeList == null || src.ContactPostCodeList.Count == 0 ? string.Empty : src.ContactPostCodeList.Join(", ")))
                  .ForMember(dest => dest.SelectedMediaTypes, opt => opt.MapFrom(src => src.MediaTypeList))
                  .ForMember(dest => dest.OutletCirculationMin, opt => opt.MapFrom(src => src.OutletCirculationMin < 1 ? "" : src.OutletCirculationMin.ToString()))
                  .ForMember(dest => dest.OutletCirculationMax, opt => opt.MapFrom(src => src.OutletCirculationMax < 1 ? "" : src.OutletCirculationMax.ToString()))
                  .ForMember(dest => dest.SelectedOSubjects, opt => opt.MapFrom(src => src.OutletSubjectList))
                  .ForMember(dest => dest.SelectedOSubjectGroups, opt => opt.MapFrom(src => src.OutletSubjectGroupList))
                  .ForMember(dest => dest.SelectedFrequencies, opt => opt.MapFrom(src => src.OutletFrequencyList))
                  .ForMember(dest => dest.SelectedLanguages, opt => opt.MapFrom(src => src.OutletLanguageList))
                  .ForMember(dest => dest.OutletStationFrequencyList, opt => opt.MapFrom(src => src.OutletStationFrequencyList == null || src.OutletStationFrequencyList.Count == 0 ? string.Empty : src.OutletStationFrequencyList.Join(", ")))
                  .ForMember(dest => dest.SelectedOContinents, opt => opt.MapFrom(src => src.OutletContinentIDList))
                  .ForMember(dest => dest.SelectedOCountries, opt => opt.MapFrom(src => src.OutletCountryIDList))
                  .ForMember(dest => dest.SelectedOStates, opt => opt.MapFrom(src => src.OutletStateIDList))
                  .ForMember(dest => dest.SelectedOCities, opt => opt.MapFrom(src => src.OutletCityIDList))
                  .ForMember(dest => dest.SelectedOPostCodes, opt => opt.MapFrom(src => src.OutletPostCodeList == null || src.OutletPostCodeList.Count == 0 ? string.Empty : src.OutletPostCodeList.Join(", ")))
                  .ForMember(dest => dest.SelectedListIds, opt => opt.MapFrom(src => src.ListList))
                  .ForMember(dest => dest.Lists, opt => opt.ResolveUsing<ListResolver>().FromMember(src => src.ListList))
                  .IgnoreAllNonExisting();


            Mapper.CreateMap<SearchOptionsQuick, ViewModels.Search.SearchViewModel>()
                  .ForMember(dest => dest.Context, opt => opt.MapFrom(src => src.Context))
                  .ForMember(dest => dest.CountryID, opt => opt.MapFrom(src => (int)(src.SearchAustralia ? Countries.Australia : src.SearchNewZealand ? Countries.NewZealand : Countries.AllCountries)))
                  .ForMember(dest => dest.Locations, opt => opt.MapFrom(src => SearchOptionsQuick.GetLocation(src.LocationList)))
                  .ForMember(dest => dest.MediaTypes, opt => opt.MapFrom(src => src.MediaTypeList != null && src.MediaTypeList.Any() ? src.MediaTypeList.Join(",") : string.Empty))
                  .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.SearchName))
                  .ForMember(dest => dest.Positions, opt => opt.MapFrom(src => src.PositionList != null && src.PositionList.Any() ? src.PositionList.Join(",") : string.Empty))
                  .ForMember(dest => dest.SearchText, opt => opt.MapFrom(src => src.SearchText))
                  .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => src.SubjectList != null && src.SubjectList.Any() ? src.SubjectList.Join(",") : string.Empty))
               .IgnoreAllNonExisting();

            Mapper.CreateMap<OmaDocument, DocumentViewModel>()
               .ForMember(dest => dest.IsPrivate, opt => opt.MapFrom(src => src.IsPrivate))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.DocumentId, opt => opt.MapFrom(src => src.DocumentId))
               .ForMember(dest => dest.UploadedByUserId , opt => opt.MapFrom(src => src.UploadedByUserId))
               .ForMember(dest => dest.DocumentContent, opt => opt.MapFrom(src => src.DocumentContent))
               .ForMember(dest => dest.DocumentType, opt => opt.MapFrom(src => src.DocumentType))
               .IgnoreAllNonExisting();

            Mapper.CreateMap<Contact, ContactViewModel>().ForAllMembers(opt => opt.Ignore());
            Mapper.CreateMap<Contact, ContactViewModel>()
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
                .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.address))
                .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
                .ForMember(dest => dest.ProfileNotes, opt => opt.MapFrom(src => src.ProfileNotes))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Fax, opt => opt.MapFrom(src => src.Fax))
                .ForMember(dest => dest.Mobile, opt => opt.MapFrom(src => src.Mobile))
                .ForMember(dest => dest.AlternativeEmailAddress, opt => opt.MapFrom(src => src.AlternativeEmailAddress))
                .ForMember(dest => dest.AdditionalMobileNumber, opt => opt.MapFrom(src => src.AdditionalMobileNumber))
                .ForMember(dest => dest.AdditionalPhoneNumber, opt => opt.MapFrom(src => src.AdditionalPhoneNumber))
                .ForMember(dest => dest.OfficeHours, opt => opt.MapFrom(src => src.OfficeHours))
                .ForMember(dest => dest.SocialDetails, opt => opt.MapFrom(src => src.SocialDetails))
                .ForMember(dest => dest.DeliveryMethod, opt => opt.MapFrom(src => src.DeliveryMethod))
                .ForMember(dest => dest.PrimarySubject, opt => opt.MapFrom(src => src.PrimarySubject))
                .ForMember(dest => dest.PrimaryContact, opt => opt.MapFrom(src => src.PrimaryContact))
                .ForMember(dest => dest.Positions, opt => opt.MapFrom(src => src.Positions))
                .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => src.Subjects))
                .ForMember(dest => dest.BelongsToLists, opt => opt.MapFrom(src => src.BelongsToLists))
                .ForMember(dest => dest.Articles, opt => opt.MapFrom(src => src.Articles))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.AtOutlet, opt => opt.MapFrom(src => src.AtOutlet))
                .ForMember(dest => dest.Outlets, opt => opt.MapFrom(src => src.Outlets))
                .ForMember(dest => dest.Bio, opt => opt.MapFrom(src => src.Bio))
                .ForMember(dest => dest.MediaInfluencerScore, opt => opt.MapFrom(src => src.MediaInfluencerScore))
                .ForMember(dest => dest.PitchingProfileViewModel, opt => opt.MapFrom(src => src.PitchingProfile))
                .ForMember(dest => dest.BlogUrl, opt => opt.MapFrom(src => src.BlogUrl))
                .ForMember(dest => dest.WebUrl, opt => opt.MapFrom(src => src.WebUrl))
                .ForMember(dest => dest.CurrentlyOutOfOffice, opt => opt.MapFrom(src => src.CurrentlyOutOfOffice))
                .ForMember(dest => dest.OutOfOfficeStartDate, opt => opt.MapFrom(src => src.OutOfOfficeStartDate))
                .ForMember(dest => dest.OutOfOfficeReturnDate, opt => opt.MapFrom(src => src.OutOfOfficeReturnDate))
                .ForMember(dest => dest.WorkHistory, opt => opt.MapFrom(src => src.WorkHistory))
                .ForMember(dest => dest.PriorityNotice, opt => opt.MapFrom(src => src.PriorityNotice))
                .ForMember(dest => dest.InterviewLink, opt => opt.MapFrom(src => src.InterviewLink))
                .ForMember(dest => dest.InterviewPhotoLink, opt => opt.MapFrom(src => src.InterviewPhotoLink))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<WorkHistory, WorkHistoryViewModel>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<OutletDetails, OutletViewModel>().ForAllMembers(opt => opt.Ignore());
            Mapper.CreateMap<OutletDetails, OutletViewModel>()
              .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type))
              .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.address))
              .ForMember(dest => dest.PostalAddress, opt => opt.MapFrom(src => src.PostalAddress))
              .ForMember(dest => dest.Audience, opt => opt.MapFrom(src => src.Audience))
              .ForMember(dest => dest.Readership, opt => opt.MapFrom(src => src.Readership))
              .ForMember(dest => dest.Circulation, opt => opt.MapFrom(src => src.Circulation))
              .ForMember(dest => dest.DeliveryMethod, opt => opt.MapFrom(src => src.DeliveryMethod))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
              .ForMember(dest => dest.Fax, opt => opt.MapFrom(src => src.Fax))
              .ForMember(dest => dest.Bio, opt => opt.MapFrom(src => src.Bio))
              .ForMember(dest => dest.PublishedOn, opt => opt.MapFrom(src => src.PublishedOn))
              .ForMember(dest => dest.OfficeHours, opt => opt.MapFrom(src => src.OfficeHours))
              .ForMember(dest => dest.Deadlines, opt => opt.MapFrom(src => src.Deadlines))
              .ForMember(dest => dest.LogoFileName, opt => opt.MapFrom(src => src.LogoFileName))
              .ForMember(dest => dest.CopyPrice, opt => opt.MapFrom(src => src.CopyPrice))
              .ForMember(dest => dest.ParentOutlet, opt => opt.MapFrom(src => src.ParentOutlet))
              .ForMember(dest => dest.Frequency, opt => opt.MapFrom(src => src.Frequency))
              .ForMember(dest => dest.Languages, opt => opt.MapFrom(src => src.Languages))
              .ForMember(dest => dest.MediaType, opt => opt.MapFrom(src => src.MediaType))
              .ForMember(dest => dest.Mobile, opt => opt.MapFrom(src => src.Mobile))
              .ForMember(dest => dest.OutletDemographics, opt => opt.MapFrom(src => src.OutletDemographics))
              .ForMember(dest => dest.ParentOutlet, opt => opt.MapFrom(src => src.ParentOutlet))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
              .ForMember(dest => dest.ProfileNotes, opt => opt.MapFrom(src => src.ProfileNotes))
              .ForMember(dest => dest.SocialDetails, opt => opt.MapFrom(src => src.SocialDetails))
              .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => src.Subjects))
              .ForMember(dest => dest.BelongsToLists, opt => opt.MapFrom(src => src.BelongsToLists))
              .ForMember(dest => dest.Contacts, opt => opt.MapFrom(src => src.Contacts))
              .ForMember(dest => dest.MediaInfluencerScore, opt => opt.MapFrom(src => src.MediaInfluencerScore))
              .ForMember(dest => dest.WebUrl, opt => opt.MapFrom(src => src.WebUrl))
              .ForMember(dest => dest.YearEstablished, opt => opt.MapFrom(src => src.YearEstablished))
              .ForMember(dest => dest.NewsFocusName, opt => opt.MapFrom(src => src.NewsFocusName))
              .ForMember(dest => dest.CallLetters, opt => opt.MapFrom(src => src.CallLetters))
              .ForMember(dest => dest.TrackingReference, opt => opt.MapFrom(src => src.TrackingReference))
              .ForMember(dest => dest.NamePronunciation, opt => opt.MapFrom(src => src.NamePronunciation))
              .ForMember(dest => dest.SubscriptionOnlyPublication, opt => opt.MapFrom(src => src.SubscriptionOnlyPublication))
              .ForMember(dest => dest.AlsoKnownAs, opt => opt.MapFrom(src => src.AlsoKnownAs))
              .ForMember(dest => dest.AlternativeEmailAddress, opt => opt.MapFrom(src => src.AlternativeEmailAddress))
              .ForMember(dest => dest.AdditionalPhoneNumber, opt => opt.MapFrom(src => src.AdditionalPhoneNumber))
              .ForMember(dest => dest.AdditionalWebsite, opt => opt.MapFrom(src => src.AdditionalWebsite))
              .ForMember(dest => dest.BroadcastTime, opt => opt.MapFrom(src => src.BroadcastTime))
              .ForMember(dest => dest.PressReleaseInterests, opt => opt.MapFrom(src => src.PressReleaseInterests))
              .ForMember(dest => dest.RegionsCovered, opt => opt.MapFrom(src => src.RegionsCovered))
              .ForMember(dest => dest.PriorityNotice, opt => opt.MapFrom(src => src.PriorityNotice))
              .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
              .IgnoreAllNonExisting();

            Mapper.CreateMap<PrivateContactModel, PrivateContactViewModel>()
             .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
             .ForMember(dest => dest.Prefix, opt => opt.MapFrom(src => src.Prefix))
             .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
             .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
             .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
             .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
             .ForMember(dest => dest.AddressLine1, opt => opt.MapFrom(src => src.AddressLine1))
             .ForMember(dest => dest.AddressLine2, opt => opt.MapFrom(src => src.AddressLine2))
             .ForMember(dest => dest.CityId, opt => opt.MapFrom(src => src.CityId))
             .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
             .ForMember(dest => dest.StateId, opt => opt.MapFrom(src => src.StateId))
             .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId))
             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
             //.ForMember(dest => dest.FaxNumber, opt => opt.MapFrom(src => src.FaxNumber))
             .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src.MobileNumber))
             .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Notes))
             .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.EmailAddress))
             .ForMember(dest => dest.PreferredDelivery,  opt => opt.MapFrom(src => CharEnum.Parse(typeof(PreferredDeliveryMethod), src.PreferredDeliveryMethod.Value)))
             .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
             .ForMember(dest => dest.Facebook, opt => opt.MapFrom(src => src.Facebook))
             .ForMember(dest => dest.LinkedIn, opt => opt.MapFrom(src => src.LinkedIn))
             .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter))
             .ForMember(dest => dest.BlogUrl, opt => opt.MapFrom(src => src.BlogUrl))
             .ForMember(dest => dest.SubjectIdsCsv, opt => opt.MapFrom(src => src.SubjectIdsCsv))
             .ForMember(dest => dest.RoleIdsCsv, opt => opt.MapFrom(src => src.RoleIdsCsv))
             .ForMember(dest => dest.MediaOutletId, opt => opt.MapFrom(src => src.MediaOutletId))
             .ForMember(dest => dest.PrnOutletId, opt => opt.MapFrom(src => src.PrnOutletId))
             .ForMember(dest => dest.MediaOutletId, opt => opt.MapFrom(src => src.MediaOutletId))
             //.ForMember(dest => dest.MediaOutletName, opt => opt.MapFrom(src => src.MediaOutletName))
             .ForMember(dest => dest.PrivateOutletId, opt => opt.MapFrom(src => src.PrivateOutletId))
             //.ForMember(dest => dest.PrivateOutletName, opt => opt.MapFrom(src => src.PrivateOutletName))
             .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles))
             .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => src.Subjects))
             .IgnoreAllNonExisting();

            Mapper.CreateMap<PrivateContactViewModel, PrivateContactModel>()
             .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
             .ForMember(dest => dest.Prefix, opt => opt.MapFrom(src => src.Prefix))
             .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
             .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
             .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
             .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
             .ForMember(dest => dest.AddressLine1, opt => opt.MapFrom(src => src.AddressLine1))
             .ForMember(dest => dest.AddressLine2, opt => opt.MapFrom(src => src.AddressLine2))
             .ForMember(dest => dest.CityId, opt => opt.MapFrom(src => src.CityId))
             .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
             .ForMember(dest => dest.StateId, opt => opt.MapFrom(src => src.StateId))
             .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId))
             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
             //.ForMember(dest => dest.FaxNumber, opt => opt.MapFrom(src => src.FaxNumber))
             .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src.MobileNumber))
             .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Notes))
             .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.EmailAddress))
             .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => src.PreferredDeliveryMethod))
             .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
             .ForMember(dest => dest.Facebook, opt => opt.MapFrom(src => src.Facebook))
             .ForMember(dest => dest.LinkedIn, opt => opt.MapFrom(src => src.LinkedIn))
             .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter))
             .ForMember(dest => dest.BlogUrl, opt => opt.MapFrom(src => src.BlogUrl))
             .ForMember(dest => dest.SubjectIdsCsv, opt => opt.MapFrom(src => src.SubjectIdsCsv))
             .ForMember(dest => dest.RoleIdsCsv, opt => opt.MapFrom(src => src.RoleIdsCsv))
             .ForMember(dest => dest.MediaOutletId, opt => opt.MapFrom(src => src.MediaOutletId))
             .ForMember(dest => dest.PrnOutletId, opt => opt.MapFrom(src => src.PrnOutletId))
             .ForMember(dest => dest.MediaOutletId, opt => opt.MapFrom(src => src.MediaOutletId))
             //.ForMember(dest => dest.MediaOutletName, opt => opt.MapFrom(src => src.MediaOutletName))
             .ForMember(dest => dest.PrivateOutletId, opt => opt.MapFrom(src => src.PrivateOutletId))
             //.ForMember(dest => dest.PrivateOutletName, opt => opt.MapFrom(src => src.PrivateOutletName))
             .IgnoreAllNonExisting();

            Mapper.CreateMap<PrivateOutletModel, PrivateOutletViewModel>()
             .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
             .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
             .ForMember(dest => dest.Website, opt => opt.MapFrom(src => src.Website))
             .ForMember(dest => dest.AddressLine1, opt => opt.MapFrom(src => src.AddressLine1))
             .ForMember(dest => dest.AddressLine2, opt => opt.MapFrom(src => src.AddressLine2))
             .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
             .ForMember(dest => dest.Facebook, opt => opt.MapFrom(src => src.Facebook))
             .ForMember(dest => dest.LinkedIn, opt => opt.MapFrom(src => src.LinkedIn))
             .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter))
             .ForMember(dest => dest.BlogUrl, opt => opt.MapFrom(src => src.BlogUrl))
             .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
             .ForMember(dest => dest.FrequencyId, opt => opt.MapFrom(src => src.FrequencyId))
             .ForMember(dest => dest.MediaAtlasCountryId, opt => opt.MapFrom(src => src.MediaAtlasCountryId))
             .ForMember(dest => dest.CityId, opt => opt.MapFrom(src => src.CityId))
             .ForMember(dest => dest.StateId, opt => opt.MapFrom(src => src.StateId))
             .ForMember(dest => dest.SubjectIdsCsv, opt => opt.MapFrom(src => src.SubjectIdsCsv))
             .ForMember(dest => dest.ProductTypeId, opt => opt.MapFrom(src => src.ProductTypeId))
             .ForMember(dest => dest.WorkingLanguageIdsCsv, opt => opt.MapFrom(src => src.WorkingLanguageIdsCsv))
             .IgnoreAllNonExisting();

            Mapper.CreateMap<PrivateOutletViewModel, PrivateOutletModel>()
             .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
             .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
             .ForMember(dest => dest.Website, opt => opt.MapFrom(src => src.Website))
             .ForMember(dest => dest.AddressLine1, opt => opt.MapFrom(src => src.AddressLine1))
             .ForMember(dest => dest.AddressLine2, opt => opt.MapFrom(src => src.AddressLine2))
             .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
             .ForMember(dest => dest.Facebook, opt => opt.MapFrom(src => src.Facebook))
             .ForMember(dest => dest.LinkedIn, opt => opt.MapFrom(src => src.LinkedIn))
             .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter))
             .ForMember(dest => dest.BlogUrl, opt => opt.MapFrom(src => src.BlogUrl))
             .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
             .ForMember(dest => dest.FrequencyId, opt => opt.MapFrom(src => src.FrequencyId))
             .ForMember(dest => dest.MediaAtlasCountryId, opt => opt.MapFrom(src => src.MediaAtlasCountryId))
             .ForMember(dest => dest.CityId, opt => opt.MapFrom(src => src.CityId))
             .ForMember(dest => dest.StateId, opt => opt.MapFrom(src => src.StateId))
             .ForMember(dest => dest.SubjectIdsCsv, opt => opt.MapFrom(src => src.SubjectIdsCsv))
             .ForMember(dest => dest.ProductTypeId, opt => opt.MapFrom(src => src.ProductTypeId))
             .ForMember(dest => dest.WorkingLanguageIdsCsv, opt => opt.MapFrom(src => src.WorkingLanguageIdsCsv))
             .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaContactsService.MediaContactLivingListContactReplacement, ListContactReplacementViewModel>()
            .ForMember(dest => dest.RecordId, opt => opt.MapFrom(src => src.RecordId))
            .ForMember(dest => dest.ListId, opt => opt.MapFrom(src => src.ListId))
            .ForMember(dest => dest.ListSetId, opt => opt.MapFrom(src => src.ListSetId))
            .ForMember(dest => dest.ListName, opt => opt.MapFrom(src => src.ListName))
            .ForMember(dest => dest.MediaContactId, opt => opt.MapFrom(src => src.MediaContactId))
            .ForMember(dest => dest.PrnContactId, opt => opt.MapFrom(src => src.PrnContactId))
            .ForMember(dest => dest.OmaContactId, opt => opt.MapFrom(src => src.OmaContactId))
            .ForMember(dest => dest.ContactDeletedName, opt => opt.MapFrom(src => src.ContactDeletedName))
            .ForMember(dest => dest.OutletDeletedName, opt => opt.MapFrom(src => src.OutletDeletedName))
            .ForMember(dest => dest.OutletDeletedId, opt => opt.MapFrom(src => src.OutletDeletedId))
            .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src => src.RecordType))
            .ForMember(dest => dest.ContactReplacementId, opt => opt.MapFrom(src => src.ContactReplacementId))
            .ForMember(dest => dest.ContactReplacementName, opt => opt.MapFrom(src => src.ContactReplacementName))
            .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaContactsService.MediaContactListRecordDetails, ListRecord>()
                .ForMember(dest => dest.RecordId, opt => opt.MapFrom(src => src.RecordId))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.RecordType))
                .ForMember(dest => dest.OutletID, opt => opt.MapFrom(src => src.OutletId))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName))
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
                .ForMember(dest => dest.SubjectName, opt => opt.MapFrom(src => src.SubjectName))
                .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.ProductTypeName))
                .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.RoleName))
                .ForMember(dest => dest.CityName, opt => opt.MapFrom(src => src.CityName))
                .ForMember(dest => dest.IsEntityDeleted, opt => opt.MapFrom(src => src.IsEntityDeleted))
                .ForMember(dest => dest.ContactDisplay, opt => opt.MapFrom(src => src.ContactDisplay))
                .ForMember(dest => dest.RoleDisplay, opt => opt.MapFrom(src => src.RoleDisplay))
                .ForMember(dest => dest.SubjectDisplay, opt => opt.MapFrom(src => src.SubjectDisplay))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ViewModels.Search.SearchLogViewModel, MediaContactsService.MediaContactSearchLog>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.SearchCriteria, opt => opt.MapFrom(src => src.SearchCriteria))
                .ForMember(dest => dest.SearchType, opt => opt.MapFrom(src => src.SearchType))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.SavedSearchId, opt => opt.MapFrom(src => src.SavedSearchId))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => src.System))
                .ForMember(dest => dest.SearchContext, opt => opt.MapFrom(src => src.SearchContext))
                .ForMember(dest => dest.ResultsCount, opt => opt.MapFrom(src => src.ResultsCount))
                .IgnoreAllNonExisting();


            Mapper.CreateMap<MediaContactsService.MediaContactSearchLog, ViewModels.Search.SearchLogViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.SearchCriteria, opt => opt.MapFrom(src => src.SearchCriteria))
                .ForMember(dest => dest.SearchType, opt => opt.MapFrom(src => src.SearchType))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.SavedSearchId, opt => opt.MapFrom(src => src.SavedSearchId))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => src.System))
                .ForMember(dest => dest.SearchContext, opt => opt.MapFrom(src => src.SearchContext))
                .ForMember(dest => dest.ResultsCount, opt => opt.MapFrom(src => src.ResultsCount))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<TwitterUser, ViewModels.Social.TwitterUserViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.name))
                .ForMember(dest => dest.Handle, opt => opt.MapFrom(src => src.screen_name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.description))
                .ForMember(dest => dest.Followers, opt => opt.MapFrom(src => src.followers_count))
                .ForMember(dest => dest.Following, opt => opt.MapFrom(src => src.friends_count))
                .ForMember(dest => dest.Likes, opt => opt.MapFrom(src => src.favourites_count))
                .ForMember(dest => dest.Tweets, opt => opt.MapFrom(src => src.statuses_count))
                .ForMember(dest => dest.ProfileImage, opt => opt.MapFrom(src => src.profile_image_url_https))
                .ForMember(dest => dest.DefaultProfileImage, opt => opt.MapFrom(src => src.default_profile_image))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Tweet, ViewModels.Social.TweetViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.text))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => src.user))
                .ForMember(dest => dest.Retweet, opt => opt.MapFrom(src => src.retweeted_status))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => DateTime.ParseExact(src.created_at, "ddd MMM dd HH:mm:ss zzz yyyy", CultureInfo.InvariantCulture)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<statistics, ViewModels.Social.YouTubeViewModel>()
                .ForMember(dest => dest.SubscriberCount, opt => opt.MapFrom(src => src.subscriberCount))
                .ForMember(dest => dest.VideoCount, opt => opt.MapFrom(src => src.videoCount))
                .ForMember(dest => dest.ViewCount, opt => opt.MapFrom(src => src.viewCount))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaMovement, ViewModels.Dashboard.MediaMovementsViewModel>()
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
                .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
                .ForMember(dest => dest.TwitterHandle, opt => opt.MapFrom(src => src.Twitter))
                .ForMember(dest => dest.LogoFileName, opt => opt.MapFrom(src => src.LogoFileName))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.PitchingProfile, ViewModels.PitchingProfileViewModel>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<OutletBase, ViewModels.Outlet.OutletBaseViewModel>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<NoteViewModel, NotesBulk>()
                .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.IsPinned, opt => opt.MapFrom(src => src.Pinned))
                .ForMember(dest => dest.IsAapNote, opt => opt.MapFrom(src => src.AAPNote))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ListRecord, MediaRecordIdentifier>()
                .ForMember(dest => dest.PrnOutletId, opt => opt.MapFrom(src => src.PrnOutletId))
                .ForMember(dest => dest.PrnContactId, opt => opt.MapFrom(src => src.PrnContactId))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ChargeBeeService.AAPContract, AAPContractViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Product))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.ContractStartDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.ContractEndDate))
                .ForMember(dest => dest.ContractPrice, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.ContractTerm, opt =>  opt.MapFrom(src => src.ContractTerm))
                .IgnoreAllNonExisting();

            //Mapper.Map<SalesForceContact>, List < ProfileViewModel >> (profile),
            Mapper.CreateMap<ChargeBeeService.SalesForceContact, ProfileViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.AccountName))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber))
                .ForMember(dest => dest.PositionTitle, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src => src.RecordType))
                .ForMember(dest => dest.BusinessType, opt => opt.MapFrom(src => src.BusinessType))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.MainContact, opt => opt.MapFrom(src => src.MainContact))
                .ForMember(dest => dest.RelevantSubjects, opt => opt.MapFrom(src => src.RelevantSubjects.Join(",")))
                .ForMember(dest => dest.AccountOwnerName, opt => opt.MapFrom(src => src.AccountOwnerName))
                .ForMember(dest => dest.ABN, opt => opt.MapFrom(src => src.ABN))
                .IgnoreAllNonExisting();


            Mapper.CreateMap<MediaContactsService.MediaContactAlsoViewedDto, PeopleAlsoViewedViewModel>()
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
                .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId))
                .ForMember(dest => dest.Contact, opt => opt.MapFrom( src =>
                 new ContactName {FirstName = src.FirstName, LastName = src.LastName}))
                .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.IsGeneric, opt => opt.MapFrom(src => src.IsGeneric))
                .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src => src.RecordType))
                .ForMember(dest => dest.TwitterHandle, opt => opt.MapFrom(src => src.TwitterHandle));
        }
    }
}