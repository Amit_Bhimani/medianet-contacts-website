﻿using System.Web.Optimization;

namespace Medianet.Contacts.Web.Common.Configuration
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Bootstrap 4 js file & dependencies
            bundles.Add(new ScriptBundle("~/bundles/Scripts")
                                .Include("~/Scripts/jquery-3.0.0.js")
                                .Include("~/Scripts/umd/popper.js")
                                .Include("~/Scripts/jquery.unobtrusive-ajax.js")
                                .Include("~/Scripts/jquery.validate.js")
                                .Include("~/Scripts/jquery.validate.unobtrusive.js")
                                .Include("~/Scripts/toastr.js")
                                .Include("~/Scripts/lodash.js")
                                .Include("~/Scripts/bootstrap.js")); // Bootstrap should be last since it's min file ends in a comment, destroying everything after it

            bundles.Add(new ScriptBundle("~/bundles/global")
                                .Include("~/Scripts/fontawesome-all.js")
                                .Include("~/Scripts/Global/common.js")
                                .Include("~/Scripts/Plugins/jquery.tokeninput.js")
                                .Include("~/Scripts/Plugins/jquery.tristatecheck.js")
                                .Include("~/Scripts/bootstrap3-typeahead.js")
                                .Include("~/Scripts/spinner.js")
                                .Include("~/Scripts/Pages/common/socialmedia.js"));

            bundles.Add(new ScriptBundle("~/bundles/agreeterms")
                                .Include("~/Scripts/Pages/agree-terms.js"));

            bundles.Add(new ScriptBundle("~/bundles/quicksearch")
                                .Include("~/Scripts/Pages/quick-search.js")
                                .Include("~/Scripts/Pages/common/search-common.js")
                                .Include("~/Scripts/Pages/common/add-to-list.js")
                                .Include("~/Scripts/Pages/common/grid.js"));

            bundles.Add(new ScriptBundle("~/bundles/advancedsearch")
                                .Include("~/Scripts/Pages/common/search-common.js")
                                .Include("~/Scripts/Pages/common/add-to-list.js")
                                .Include("~/Scripts/Pages/common/grid.js")
                                .Include("~/Scripts/Pages/advanced-search.js"));

            bundles.Add(new ScriptBundle("~/bundles/allsavedsearches")
                                .Include("~/Scripts/Pages/all-saved-searches.js")
                                .Include("~/Scripts/Pages/common/group.js"));

            bundles.Add(new ScriptBundle("~/bundles/list")
                                .Include("~/Scripts/Pages/common/grid.js")
                                .Include("~/Scripts/Pages/common/export.js")
                                .Include("~/Scripts/Plugins/Sortable.js")
                                .Include("~/Scripts/Pages/list.js"));

            bundles.Add(new ScriptBundle("~/bundles/alllists")
                                .Include("~/Scripts/Pages/all-lists.js")
                                .Include("~/Scripts/Pages/common/group.js"));

            bundles.Add(new ScriptBundle("~/bundles/outletandcontact")
                                .Include("~/Scripts/Pages/common/outlet-contact.js")
                                .Include("~/Scripts/Pages/common/upload-document.js")
                                .Include("~/Scripts/pages/common/add-to-list.js")
                                .Include("~/Scripts/pages/common/suggest-change.js")
                                .Include("~/Scripts/pages/common/share-profile.js"));

            bundles.Add(new ScriptBundle("~/bundles/contact")
                                .Include("~/Scripts/Pages/contact.js"));

            bundles.Add(new ScriptBundle("~/bundles/outlet")
                                .Include("~/Scripts/pages/outlet.js"));


            bundles.Add(new ScriptBundle("~/bundles/distribution")
                                .Include("~/Scripts/Plugins/moment.js")
                                .Include("~/Scripts/Plugins/daterangepicker.js")
                                .Include("~/Scripts/Pages/distribution.js"));

            bundles.Add(new ScriptBundle("~/bundles/privatedata")
                                .Include("~/Scripts/Pages/common/grid.js")
                                .Include("~/Scripts/Pages/PrivateData.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/dashboard")
                                .Include("~/Scripts/Pages/dashboard.js"));

            bundles.Add(new ScriptBundle("~/bundles/myaccount")
                .Include("~/Scripts/Pages/my-account.js")
                .Include("~/Scripts/Pages/common/grid.js")
                .Include("~/Scripts/Pages/PrivateData.js"));
                         
            bundles.Add(new ScriptBundle("~/bundles/mediaMovements")
                        .Include("~/Scripts/Pages/media-movements.js"));

            //Design styles including custom bootstrap 4
            bundles.Add(new StyleBundle("~/Content/css/main")
                                .Include("~/Content/css/main.css")
                                .Include("~/Content/toastr.css"));

            //bundles.Add(new ScriptBundle("~/bundles/dashboard")
            //                    .Include("~/Scripts/Pages/dashboard.js"));

            bundles.Add(new StyleBundle("~/Content/distribution")
                                .Include("~/Content/css/daterangepicker.css"));

        }
    }
}