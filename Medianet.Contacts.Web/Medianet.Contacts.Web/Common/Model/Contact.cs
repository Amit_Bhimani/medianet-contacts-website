﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model.Interfaces;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.NewsCentreArticle;
using System;

namespace Medianet.Contacts.Web.Common.Model
{
    public class Contact : ContactBase
    {
        public Contact() { }

        public Address address { get; set; }
        //public string JobTitle { get; set; }
        public string ProfileNotes { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public string AlternativeEmailAddress { get; set; }
        public string AdditionalMobileNumber { get; set; }
        public string AdditionalPhoneNumber { get; set; }
        public string OfficeHours { get; set; }

        public string Bio { get; set; }
        public int? MediaInfluencerScore { get; set; }
        public string BlogUrl { get; set; }
        public string WebUrl { get; set; }
        public PitchingProfile PitchingProfile { get; set; }
        public PreferredDeliveryMethod? DeliveryMethod { get; set; }
        public Subject PrimarySubject { get; set; }
        public bool? PrimaryContact { get; set; }
        public bool CurrentlyOutOfOffice { get; set; }
        public DateTime? OutOfOfficeStartDate { get; set; }
        public DateTime? OutOfOfficeReturnDate { get; set; }
        public string PriorityNotice { get; set; }
        public string InterviewLink { get; set; }
        public string InterviewPhotoLink { get; set; }
        public ICollection<Position> Positions { get; set; }
        public ICollection<Subject> Subjects { get; set; }
        public ICollection<ListBase> BelongsToLists { get; set; }
        public PagedList<Article> Articles { get; set; }
        public ICollection<WorkHistory> WorkHistory { get; set; }

        public List<MediaContactsService.OmaDocument> Documents { get; set; }

        public int? UpdatedBy { get; set; }
        public OutletDetails AtOutlet { get; set; }
        public ICollection<OutletBase> Outlets { get; set; }
    }
}