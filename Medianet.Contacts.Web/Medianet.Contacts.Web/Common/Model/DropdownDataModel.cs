﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediaPeople.Common.Model
{
    [Serializable]
    public class DropdownDataModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}