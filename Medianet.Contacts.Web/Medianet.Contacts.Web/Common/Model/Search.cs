﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Model
{
    public class SavedSearch
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Query { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public GroupBase Group { get; set; }
        public int UserId { get; set; }
        public bool Visibility { get; set; }
        public string Context { get; set; }
        public string Criteria { get; set; }
        public int ResultsCount { get; set; }
        public SearchType Type { get; set; }
    }
}
