﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using MediaPeople.Helpers;

namespace MediaPeople.Common.Model
{
    public class MonitoringSearchModel : BaseModel
    {
        /// <summary>
        /// Custom validations for the search model.
        /// </summary>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            DateTime myDateFrom;
            DateTime myDateTo;
            Hashtable SearchConfig = ConfigurationManager.GetSection("MonitoringSearch") as Hashtable;
            int maxMonths = 3; // default it to 3 incase we dont have the web config setting for some reason.

            if (SearchConfig["MaxCampaignLength"] != null)
                maxMonths = int.Parse(SearchConfig["MaxCampaignLength"].ToString());

            if (!SearchCampaign && defaultCampaign.Name.SafeTrim() == "")
            {
                var prop = new[] { "defaultCampaign.Name" };
                yield return new ValidationResult("Campaign Name cannot be blank.", prop);
            }

            if (!SearchInternet && !SearchText && !SearchWire)
            {
                // the user must select atleast 1 media type.
                var prop = new[] { "SearchInternet", "SearchText", "SearchWire" };
                yield return new ValidationResult("Please select at least one Media Type. ", prop);
            }

            if (!DateTime.TryParse(PeriodFrom.ToShortDateString(), out myDateFrom))
            {
                // the From Date value must be a valid date.
                var prop = new[] { "PeriodFrom" };
                yield return new ValidationResult("Please select at Valid Date. ", prop);
            }
            else
            {
                if (!DateTime.TryParse(PeriodTo.ToShortDateString(), out myDateTo))
                {
                    // the From Date value must be a valid date.
                    var prop = new[] { "PeriodTo" };
                    yield return new ValidationResult("Please select at Valid Date. ", prop);
                }
                else
                {
                    if (myDateFrom > myDateTo)
                    {
                        // The from-date cannot be greater than the to-date
                        var prop = new[] { "PeriodFrom" };
                        yield return new ValidationResult("Period From date must be before the Period To date. ", prop);
                    }

                    if (myDateTo > DateTime.Now && SearchCampaign)
                    {
                        // you cannot select a date in the future if your doing a general search.
                        // future dates only allowed if the user is configuring a campaign.
                        var prop = new[] { "PeriodTo" };
                        yield return new ValidationResult("Period To date cannot appear in the future. Please enter a date on or before " + DateTime.Now.ToString("dd/MM/yyyy"), prop);
                    }

                    if (myDateFrom.AddMonths(maxMonths) < myDateTo)
                    {
                        var prop = new[] { "PeriodFrom" };
                        yield return new ValidationResult("Invalid date range. The maximum allowable campaign length is restricted to " + maxMonths.ToString() + " months.", prop);
                    }
                }
            }
        }

        [DisplayName("Search Text")]
        [Required(ErrorMessage = "Please enter a search keyword.", AllowEmptyStrings = false)]
        public string SearchKeywords { get; set; }          // The text being entered as search criteria.
        public bool SearchFullText { get; set; }            // Are we doing a full text search? True = Full text, False = Headlines only.
        [DisplayName("Internet")]
        public bool SearchInternet { get; set; }            // Are we searching internet content.
        [DisplayName("Press Release")]
        public bool SearchPressReleases { get; set; }       // Are we searching Medianet Press releases.
        [DisplayName("Text")]
        public bool SearchText { get; set; }
        [DisplayName("Wire")]
        public bool SearchWire { get; set; }                // Are we searching the Wire.
        [DisplayName("Media Sources")]
        public string Sources { get; set; }                 // Sources and owners selected for searching (blank = all)
        [DisplayName("Website Categories")]
        public string Owners { get; set; }                 // Owners selected for searching (blank = all)
        public bool SearchMostRecent { get; set; }          // If true, search order by most recent docs; false, order most relevant docs
        [DisplayName("From")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a From Date.")]
        public DateTime PeriodFrom { get; set; }            // From date or lower bound of the date search range.
        [DisplayName("To")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a To Date.")]
        public DateTime PeriodTo { get; set; }              // To Date or upper bound of the date search range.
        public int MonCampaignId { get; set; }

        public string UserMessage { get; set; }
        public bool HasError { get; set; }

        public bool SearchCampaign { get; set; }

        //below items are used in construction of the view only.
        public List<DropdownDataModel> OwnersList { get; set; }
        public List<DropdownDataModel> SourcesList { get; set; }
        public List<DropdownDataModel> DefaultSources { get; set; }
        public List<DropdownDataModel> DefaultOwners { get; set; }

        public List<CampaignModel> CampaignList { get; set; }
        public CampaignModel defaultCampaign { get; set; }

        public DateTime GetStartDate()
        {
            if (PeriodFrom == null || PeriodFrom.Year < 2012)
            {
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddDays(-7);
            }

            return new DateTime(PeriodFrom.Year, PeriodFrom.Month, PeriodFrom.Day, 0, 0, 0);
        }

        public DateTime GetEndDate()
        {
            if (PeriodTo == null || PeriodTo.Year < 2012)
            {
                return DateTime.Now;
            }

            DateTime ToDate = new DateTime(PeriodTo.Year, PeriodTo.Month, PeriodTo.Day, 23, 59, 59);

            return ToDate;
        }
        
    }
}