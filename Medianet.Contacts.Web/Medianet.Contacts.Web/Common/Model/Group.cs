﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.Common.Model
{
    public class Group : GroupBase
    {
        public int UserId { get; set; }
        public ICollection<ListBase> Lists { get; set; }
        public GroupBase ParentGroup { get; set; }
        public ICollection<GroupBase> ChildGroups { get; set; }
    }
} 