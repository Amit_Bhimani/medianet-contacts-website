﻿namespace Medianet.Contacts.Web.Common.Model
{
    public class Address
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public override string ToString() {
            return (AddressLine1 != null ? AddressLine1 : "") +
                (AddressLine2 != null ? ", " + AddressLine2 : "") +
                (City != null ? ", " + ", " + City : "") +
                (PostCode != null ? ", " + PostCode : "") +
                (State != null ? ", " + State : "") + (Country != null ? ", " + Country : "");
        }
    }
}