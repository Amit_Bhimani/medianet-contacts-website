﻿using System.Collections.Generic;
using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(SubjectGroupTypeConverter))]
    public class SubjectGroup: IModel
    {
        public List<Subject> Subjects { get; set; }

        public static explicit operator int(SubjectGroup sg) {
            return sg == null ? -1 : sg.Id;
        }

        public static explicit operator string(SubjectGroup sg) {
            return sg == null && sg != null ? string.Empty : sg.Name;
        }
    }
}