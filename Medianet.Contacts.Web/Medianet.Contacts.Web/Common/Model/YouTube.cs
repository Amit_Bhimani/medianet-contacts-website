﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class YouTube
    {
        public YouTube()
        {
            items = new List<item>();
        }

        public List<item> items { get; set; }

        public class item
        {
            public statistics statistics { get; set; }
        }

    }
    public class statistics
    {
        public int subscriberCount { get; set; }

        public int videoCount { get; set; }

        public long viewCount { get; set; }
    }

}