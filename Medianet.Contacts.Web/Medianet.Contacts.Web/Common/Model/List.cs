﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.Common.Model
{
    public class List : ListBase
    {
        public int VersionNumber { get; set; }
        public ICollection<ListRecord> Records { get; set; }
    }
}