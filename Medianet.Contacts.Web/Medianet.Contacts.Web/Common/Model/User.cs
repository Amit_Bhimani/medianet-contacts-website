﻿using System;
using System.Collections.Generic;

namespace Medianet.Contacts.Web.Common.Model
{
    [Serializable]
    public class User
    {
        public static readonly char TrialFlag = 'R';

        public int UserID { get; set; }
        public string FullName { get; set; }
        public string LogonName { get; set; }
        public string Password { get; set; }
        public string DebtorNumber { get; set; }
        public string OutletId { get; set; }
        public string EmailAddress { get; set; }
        public string FaxNumber { get; set; }
        public string CompanyName { get; set; }
        public char OMAAccessRights { get; set; }
        public bool OMAAccess { get; set; }
        public bool HasViewedTermsAndConditions { get; set; }
        public bool MustChangePassword { get; set; }
        public DateTime LastLogonTime { get; set; }
        public string SessionKey { get; set; }
        public bool HasMonitoringPermission { get; set; }
        public bool CanExportContacts { get; set; }
        public List<int> DataModules { get; set; }
        public DateTime LastValidatedTime { get; set; }
        public bool HasJournalistsWebAccess { get; set; }
        public bool HasDistributionWebAccess { get; set; }
        public int DefaultSearchCountry { get; set; }
        /// <summary>
        /// Checks to see if this user is a trial user or not.
        /// </summary>
        /// <returns>Returns true if the current user is a trial account.</returns>
        public bool isTrial() {
            return OMAAccessRights == User.TrialFlag;
        }

        public string OMAAccessRightsToString() {
            return OMAAccessRights == 'A' ? "[Administrator]" : 
                OMAAccessRights == 'S' ? "[Supervisor]" :
                OMAAccessRights == 'O' ? "[Operator]" :
                OMAAccessRights == 'T' ? "[Trainee]" :
                OMAAccessRights == 'Q' ? "[Query]" :
                OMAAccessRights == 'X' ? "[Executive]" :
                OMAAccessRights == User.TrialFlag ? "[Trial]" : "[Visitor]";
        }

        public override string ToString() {
            return string.Format("UserId: {0} \t FullName: {1} \t LogonName: {2} \t DebtorNumber: {3} \t OutletId: {4} \t SessionId: {5}.",
                UserID.ToString(), FullName == null ? string.Empty : FullName, LogonName == null ? string.Empty : LogonName,
                DebtorNumber == null ? string.Empty : DebtorNumber, OutletId == null ? string.Empty : OutletId,
                SessionKey);
        }
    }
}