﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    public class OutletDetails : OutletBase
    {
        public Address address { get; set; }

        public Address PostalAddress { get; set; }

        public string ProfileNotes { get; set; }
        public string Circulation { get; set; }
        public int? Audience { get; set; }

        public int? Readership { get; set; }

        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Bio { get; set; }
        public string PublishedOn { get; set; }
        public string OfficeHours { get; set; }
        public string Deadlines { get; set; }
        public string CopyPrice { get; set; }
        //public string LogoFileName { get; set; }
        public OutletDetails ParentOutlet { get; set; }
        public PreferredDeliveryMethod? DeliveryMethod { get; set; }
        public OutletFrequency Frequency { get; set; }
        public SocialInfo SocialDetails { get; set; }
        public string BlogUrl { get; set; }
        public string WebUrl { get; set; }
        public Demographics OutletDemographics { get; set; }
        public int? MediaInfluencerScore { get; set; }
        public ICollection<Language> Languages { get; set; }
        public ICollection<Subject> Subjects { get; set; }

        public ICollection<ListBase> BelongsToLists { get; set; }
        
        public List<MediaContactsService.OmaDocument> Documents { get; set; }

        public int? UpdatedBy { get; set; }

        public ICollection<ContactBase> Contacts { get; set; }

        //public bool IsGeneric { get; set; }

        public string YearEstablished { get; set; }

        public string NewsFocusName { get; set; }

        public string CallLetters { get; set; }

        public string TrackingReference { get; set; }

        public string NamePronunciation { get; set; }

        public bool SubscriptionOnlyPublication { get; set; }

        public string AlsoKnownAs { get; set; }

        public string AlternativeEmailAddress { get; set; }

        public string AdditionalPhoneNumber { get; set; }

        public string AdditionalWebsite { get; set; }

        public string BroadcastTime { get; set; }

        public string PressReleaseInterests { get; set; }

        public string RegionsCovered { get; set; }

        public string PriorityNotice { get; set; }

        public bool IsActive { get; set; }
    }
}