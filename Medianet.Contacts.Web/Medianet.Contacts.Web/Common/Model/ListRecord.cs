﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Model
{
    [Serializable]
    public class ListRecord : RecordIdentifier
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public string OutletName { get; set; }
        public string ContactDisplay { get; set; }
        public string FullName { get; set; }
        public string JobTitle { get; set; }
        public string RoleDisplay { get; set; }
        public string RoleName { get; set; }
        public string SubjectDisplay { get; set; }
        public string SubjectName { get; set; }
        public string ProductTypeName { get; set; }
        public string CityName { get; set; }
        public bool IsEntityDeleted { get; set; }
        public int? MediaInfluencerScore { get; set; }
        public bool IsGeneric { get; set; }

        /// <summary>
        /// True if the record is checked. Defaults to true.
        /// </summary>
        public bool Selected { get; set; }

        public ListRecord()
        {
            Selected = true;
        }

        public ListRecord(string outletId, string contactId, string recType) : base(outletId, contactId, recType)
        {
        }
    }
}