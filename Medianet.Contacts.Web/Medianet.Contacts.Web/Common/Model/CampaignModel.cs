﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MediaPeople.Common.Model
{
    [Serializable()]
    public class CampaignModel : BaseModel
    {
        public int Id { get; set; }
        [StringLength(200, ErrorMessage = "Project Name cannot be larger than 200 characters.")]
        [DisplayName("Project Name")]
        public string ProjectName { get; set; }
        [StringLength(200, ErrorMessage = "Campaign Name cannot be larger than 200 characters.")]
        [DisplayName("Campaign Name")]
        public string Name { get; set; }
        [DisplayName("Public Campaign")]
        public bool PublicCampaign { get; set; }
        public int UserId { get; set; }
        public int MonitoringCampaignId { get; set; }
        public int SearchId { get; set; }
        public bool Active { get; set; }
        public string DebtorNumber { get; set; }
        public int ArticleCount { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public bool HasAlert { get; set; }

    }
}