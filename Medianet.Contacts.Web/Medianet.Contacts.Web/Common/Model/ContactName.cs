﻿using System;

namespace Medianet.Contacts.Web.Common.Model
{
    public class ContactName : IComparable<ContactName>
    {
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string Initials
        {
            get
            {
                var result = string.Empty;
                result += (string.IsNullOrEmpty(this.FirstName) ? string.Empty : this.FirstName.Substring(0, 1));
                result += (string.IsNullOrEmpty(this.LastName) ? string.Empty : this.LastName.Substring(0, 1));

                return result.Trim().ToUpper();
            }
        }

        #region IComparable<TaskGroup> Members

        private string CompareToValue(ContactName cn) {
            return (!string.IsNullOrEmpty(cn.FirstName) ? cn.FirstName + " " : " ") + (!string.IsNullOrEmpty(cn.LastName) ? cn.LastName : "").ToLower();
        }

        public int CompareTo(ContactName other) {
            return CompareToValue(this).CompareTo(CompareToValue(other));
        }

        public override string ToString() {
            //this is used by website to display name on the webpages.
            return (!string.IsNullOrEmpty(this.FirstName) ? this.FirstName : "")
                                          + (!string.IsNullOrEmpty(this.MiddleName) ? " " + this.MiddleName : "")
                                          + (!string.IsNullOrEmpty(this.LastName) ? " " + this.LastName : "");
        }
        #endregion
    }
}