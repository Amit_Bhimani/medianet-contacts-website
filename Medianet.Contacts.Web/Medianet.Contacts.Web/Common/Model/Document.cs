﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.Common.Model
{
    public class Document : DocumentBase
    {
        public Document() { }
       
        public byte[] DocumentContent { get; set; }
        public string MimeType { get; set; }

        public ICollection<OutletContactBase> BelongsTo { get; set; }
    }
}