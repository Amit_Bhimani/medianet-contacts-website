﻿namespace Medianet.Contacts.Web.Common.Model
{
    public class OutletType
    {
        public OutletType() {
            Id = 0;
            Name = string.Empty;
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}