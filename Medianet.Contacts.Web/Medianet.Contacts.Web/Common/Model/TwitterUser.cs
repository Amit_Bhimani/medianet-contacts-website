﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class TwitterUser
    {
        public string id { get; set; }
        public string name { get; set; }
        public string screen_name { get; set; }
        public string location { get; set; }
        public string description { get; set; }
        public int followers_count { get; set; }
        public int friends_count { get; set; }
        public int favourites_count { get; set; }
        public int statuses_count { get; set; }
        public string profile_image_url_https { get; set; }
        public Boolean default_profile_image { get; set; }
        public TwitterUserStatus status { get; set; }
    }
}