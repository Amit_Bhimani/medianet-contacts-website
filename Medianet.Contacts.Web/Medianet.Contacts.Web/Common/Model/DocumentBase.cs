﻿using System;

namespace Medianet.Contacts.Web.Common.Model
{
    public class DocumentBase
    {
        public DocumentBase() {
            FullName = string.Empty;
        }

        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UploadedByUserId { get; set; }
        public bool? Status { get; set; }
        public bool IsPrivate { get; set; }
    }
}