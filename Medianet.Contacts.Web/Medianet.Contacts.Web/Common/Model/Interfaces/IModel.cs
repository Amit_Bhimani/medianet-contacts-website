﻿using System;

namespace Medianet.Contacts.Web.Common.Model.Interfaces
{
    [Serializable]
    public class IModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }
    }
}