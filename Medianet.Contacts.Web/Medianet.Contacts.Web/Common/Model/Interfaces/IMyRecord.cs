﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Model.Interfaces
{
    public interface IMyRecord
    {
        RecordType Type { get; set; }
        ICollection<ListBase> BelongsToLists { get; set; }
        ICollection<TaskBase> BelongsToTasks { get; set; }
        ICollection<DocumentBase> Documents { get; set; }
        string OutletId { get; set; }
        string ContactId { get; set; }
    }
}
