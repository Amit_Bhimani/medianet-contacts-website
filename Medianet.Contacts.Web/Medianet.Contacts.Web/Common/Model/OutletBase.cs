﻿namespace Medianet.Contacts.Web.Common.Model
{
    public class OutletBase : OutletContactBase
    {
        public string Name { get; set; }
        public string LogoFileName { get; set; }
        public bool IsGeneric { get; set; }
        public MediaType MediaType { get; set; }
    }
}