﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(OutletFrequencyTypeConverter))]
    public class OutletFrequency : IModel
    {
        public OutletFrequency() {
            Id = -1;
            Name = string.Empty;
        }
    }
}