﻿using System;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    public class ListBase : IModel
    {
        public ListBase() {
            Id = 0;
            Name = string.Empty;
            IsPrivate = false;
        }

        public int UserId { get; set; }
        public bool IsPrivate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Status { get; set; }

        public GroupBase BelongsToGroup { get; set; }
    }
}