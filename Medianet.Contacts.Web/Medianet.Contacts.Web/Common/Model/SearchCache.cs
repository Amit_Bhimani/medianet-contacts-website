﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Model
{
    public class SearchCache
    {
        public string SearchOptionsHash { get; set; }
        public List<SearchResult> Results { get; set; }
        public SortColumn SortCol { get; set; }
        public SortDirection SortDir { get; set; }
    }
}
