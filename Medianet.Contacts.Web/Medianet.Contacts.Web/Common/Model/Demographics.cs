﻿namespace Medianet.Contacts.Web.Common.Model
{
    public class Demographics
    {
        public string ABs { get; set; }
        public string GBs { get; set; }
        public string M16Plus { get; set; }
        public string F16Plus { get; set; }
        public string P16Plus { get; set; }
    }
}