﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(LanguageTypeConverter))]
    public class Language: IModel
    {
        public Language() {
            Name = string.Empty;
        }

        public bool? IsPreferred { get; set; }
    }
}