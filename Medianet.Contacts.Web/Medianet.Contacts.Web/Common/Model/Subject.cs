﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(SubjectTypeConverter))]
    public class Subject: IModel
    {
        public Subject() {
            Id = -1;
            Name = string.Empty;
        }
    }
}