﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [Serializable]
    public class GroupBase : IModel
    {
        public GroupBase() {
            Id = 0;
            Name = string.Empty;
            Status = true;
        }

        public bool Status { get; set; }
        public GroupType Type { get; set; }
    }
}