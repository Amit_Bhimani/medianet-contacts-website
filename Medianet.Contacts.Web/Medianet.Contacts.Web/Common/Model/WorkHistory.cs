﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class WorkHistory
    {
        public string ContactId { get; set; }
        public string JobTitle { get; set; }
        public int? StartYear { get; set; }
        public int? StartMonth { get; set; }
        public int? EndYear { get; set; }
        public int? EndMonth { get; set; }
        public string OutletId { get; set; }
        public string CompanyName { get; set; }
        public string LogoFileName { get; set; }
        public string Location { get; set; }
    }
}
