﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class Tweet
    {
        public string id { get; set; }

        public string created_at { get; set; }

        public string text { get; set; }

        public TwitterUser user { get; set; }

        public Tweet retweeted_status { get; set; }
    }
}