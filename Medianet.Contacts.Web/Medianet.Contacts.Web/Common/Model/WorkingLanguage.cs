﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(SubjectTypeConverter))]
    public class WorkingLanguage: IModel
    {
        public WorkingLanguage()
        {
            Id = -1;
            Name = string.Empty;
        }
    }
}