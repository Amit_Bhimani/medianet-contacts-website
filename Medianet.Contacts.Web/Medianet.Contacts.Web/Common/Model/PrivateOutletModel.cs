﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.Common.Model
{
    public class PrivateOutletModel
    {
        public string CompanyName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public string PostCode { get; set; }
        public int MediaAtlasCountryId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public int OutletTypeId { get; set; }
        public int ProductTypeId { get; set; }
        public int Circulation { get; set; }
        public char PreferredDeliveryMethod { get; set; }
        public string Website { get; set; }
        public string Ownership { get; set; }
        public string StationFrequency { get; set; }
        public int FrequencyId { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string BlogUrl { get; set; }
        public string Notes { get; set; }
        public int UpdatedBy { get; set; }
        public string SubjectIdsCsv { get; set; }
        public string WorkingLanguageIdsCsv { get; set; }
        public int OutletId { get; set; }

        private List<Subject> _subjects;
        public List<Subject> Subjects
        {
            get
            {
                if (_subjects == null)
                    _subjects = new List<Subject>();
                return _subjects;
            }
            set { _subjects = (List<Subject>)value; }
        }

        private List<WorkingLanguage> _workingLanguages;
        public List<WorkingLanguage> WorkingLanguages
        {
            get
            {
                if (_workingLanguages == null)
                    _workingLanguages = new List<WorkingLanguage>();
                return _workingLanguages;
            }
            set { _workingLanguages = (List<WorkingLanguage>)value; }
        }
    }
}