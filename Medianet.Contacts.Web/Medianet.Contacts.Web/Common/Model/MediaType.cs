﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(MediaTypeTypeConverter))]
    public class MediaType: IModel
    {
        public MediaType() {
            Id = 0;
            Name = string.Empty;
        }
    }
}