﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class TwitterUserStatus
    {
        public string text { get; set; }
        public int retweet_count { get; set; }
        public int favorite_count { get; set; }
    }
}