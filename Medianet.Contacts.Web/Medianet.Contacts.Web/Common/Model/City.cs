﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(CityTypeConverter))]
    public class City : IModel
    {
        public City()
        {
            Id = 0;
            Name = string.Empty;
            StateId = null;
            CountryId = null;
            ContinentId = null;
        }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public int? ContinentId { get; set; }
    }
}
