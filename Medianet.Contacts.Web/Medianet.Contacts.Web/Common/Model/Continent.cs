﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(ContinentTypeConverter))]
    public class Continent: IModel
    {
        public Continent()
        {
            Id = 0;
            Name = string.Empty;
            DataModuleId = 0;
        }

        public int DataModuleId { get; set; }
    }
}
