﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediaPeople.Common.Model
{
    public class BaseModel
    {

        /// <summary>
        /// 
        /// </summary>
        public User session { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool isRelease
        {
            get
            {
                #if (!DEBUG)
                    return true;
                #else
                return false;
                #endif
            }
        }
    }
}