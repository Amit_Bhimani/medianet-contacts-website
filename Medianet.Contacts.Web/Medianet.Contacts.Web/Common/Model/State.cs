﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(StateTypeConverter))]
    public class State : IModel
    {
        public State()
        {
            Id = 0;
            Name = string.Empty;
            CountryId = 0;
        }
        public int CountryId { get; set; }
    }
}
