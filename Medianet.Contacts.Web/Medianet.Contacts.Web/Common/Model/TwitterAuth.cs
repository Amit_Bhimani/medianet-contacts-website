﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class TwitterAuth
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}