﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(PositionTypeConverter))]
    public class Position: IModel
    {
        public Position() {
            Id = 0;
            Name = string.Empty;
        }
    }
}