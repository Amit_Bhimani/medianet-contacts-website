﻿using System;

namespace Medianet.Contacts.Web.Common.Model
{
    [Serializable]
    public class ReminderType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Minutes { get; set; }
    }
}