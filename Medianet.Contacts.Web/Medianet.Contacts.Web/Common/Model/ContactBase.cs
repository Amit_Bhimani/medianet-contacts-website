﻿namespace Medianet.Contacts.Web.Common.Model
{
    public class ContactBase : OutletContactBase
    {
        public ContactName Name { get; set; }
        public string JobTitle { get; set; }
        public SocialInfo SocialDetails { get; set; }
        //public string BlogUrl { get; set; }
        //public string WebUrl { get; set; }
        public bool IsPrimaryNewsContact { get; set; }
        public string CompanyName { get; set; }
    }
}