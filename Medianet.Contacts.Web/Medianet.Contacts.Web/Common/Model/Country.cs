﻿using System.ComponentModel;
using Medianet.Contacts.Web.Common.Converters;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Model
{
    [TypeConverter(typeof(CountryTypeConverter))]
    public class Country: IModel
    {
        public Country()
        {
            Id = 0;
            Name = string.Empty;
            ContinentId = null;
        }

        public int DataModuleId { get; set; }

        public int? ContinentId { get; set; }
    }
}
