﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Medianet.Contacts.CSharp.ExtensionMethods;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Model
{
    [Bind(Exclude = "Roles, Subjects")]
    public class PrivateContactModel
    {
        public int? ContactId { get; set; }
        public string Prefix { get; set; }
        [Required(ErrorMessage = "Required Field")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Required Field")]
        public string LastName { get; set; }
        [StringLength(50)]        
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "Required Field")]
        public string JobTitle { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int? CityId { get; set; }
        public string PostCode { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string MobileNumber { get; set; }        
        public string Notes { get; set; }
        [Required(ErrorMessage = "Required Field")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Required Field")]
        public char? PreferredDeliveryMethod { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string BlogUrl { get; set; }
        public int UpdatedBy { get; set; }
        [Required(ErrorMessage = "Required Field")]
        public string SubjectIdsCsv{get; set;}
        public string RoleIdsCsv { get; set; }
        public string MediaOutletId { get; set; }
        public int? PrnOutletId { get; set; }
        public int? PrivateOutletId { get; set; }
        public string OutletName { get; set; }
        private List<Position> _roles;
        public List<Position> Roles
        {
            get
            {
                if (_roles == null)
                    _roles = new List<Position>();
                return _roles;
            }
            set { _roles = (List<Position>)value; }
        }

        private List<Subject> _subjects;
        public List<Subject> Subjects
        {
            get
            {
                if (_subjects == null)
                    _subjects = new List<Subject>();
                return _subjects;
            }
            set { _subjects = (List<Subject>)value; }
        }

        public RecordIdentifier GetRecordIdentifier()
        {
            var record = new RecordIdentifier();

            if (!MediaOutletId.IsNullOrWhitespace())
            {
                record.Type = RecordType.OmaContactAtMediaOutlet;
                record.OutletID = MediaOutletId;
            }
            else if (PrnOutletId.HasValue)
            {
                record.Type = RecordType.OmaContactAtPrnOutlet;
                record.OutletID = PrnOutletId.Value.ToString();
            }
            else if (PrivateOutletId.HasValue)
            {
                record.Type = RecordType.OmaContactAtOmaOutlet;
                record.OutletID = PrivateOutletId.Value.ToString();
            }
            else
            {
                record.Type = RecordType.OmaContactNoOutlet;
            }

            record.ContactId = this.ContactId.ToString();
            return record;
        }
    }
}