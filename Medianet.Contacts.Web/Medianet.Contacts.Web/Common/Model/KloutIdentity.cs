﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class KloutIdentity
    {
        public string id { get; set; }
        public string network { get; set; }
    }
}