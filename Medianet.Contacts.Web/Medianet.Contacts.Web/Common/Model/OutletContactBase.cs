﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Model
{
    public class OutletContactBase
    {
        public RecordType Type { get; set; }
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}