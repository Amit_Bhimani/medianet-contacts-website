﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Common.Model
{
    public class Frequency
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}