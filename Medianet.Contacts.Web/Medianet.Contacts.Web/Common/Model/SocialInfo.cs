﻿namespace Medianet.Contacts.Web.Common.Model
{
    public class SocialInfo
    {
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string SnapChat { get; set; }
        public string Skype { get; set; }
        public string Name { get; set; }
    }
}