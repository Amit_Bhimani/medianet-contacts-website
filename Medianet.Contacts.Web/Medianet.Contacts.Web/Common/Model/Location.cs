﻿namespace Medianet.Contacts.Web.Common.Model
{
    public class Location
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string ParentId { get; set; }
    }
}