﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Optimization;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.Logging;
using Medianet.Contacts.Web.ProxyWrappers;
using User = Medianet.Contacts.Web.Common.Model.User;
using Medianet.Contacts.Web.ChargeBeeService;

namespace Medianet.Contacts.Web.Common
{
    /// <summary>
    /// This Class is responsible for manageing session for the current User.
    /// </summary>
    public sealed class SessionManager
    {
        #region Session Constants

        public static readonly string SessionExpiredMessage = "Your session has expired. Please login again.";
        public const string SessionMember = "Member";
        public const string SessionSearchOptions = "SearchOptions";
        public const string SessionSearchPinnedItems = "SearchPinnedItems";
        public const string SessionSearchDeletedItems = "SearchDeletedItems";
        public const string SessionSearchUnselectedItems = "SearchUnselectedItems";
        public const string SessionListItemsChecked = "ListItemsChecked";
        public const string SessionListItemsUnchecked = "ListItemsUnchecked";
        public const string SessionTaskItemsChecked = "TaskItemsChecked";
        public const string SessionTaskItemsUnchecked = "TaskItemsUnchecked";
        public const string SessionExpired = "Your session has expird. Please login again.";
        public const string SystemTypeMediaPeople = "P";

        #endregion

        public int ValidateSessionEvery
        {
            get
            {
                int validateEvery;

                if (!int.TryParse(ConfigurationManager.AppSettings["ValidateSessionFromDbEvery"], out validateEvery))
                    validateEvery = 3;

                return validateEvery;
            }
        }

        public System.Web.SessionState.HttpSessionState Session
        {
            get
            {
                HttpContext state = HttpContext.Current;
                return state.Session;
            }
        }

        /*Properties*/

        /// <summary>
        /// Gets the current logged in user.
        /// </summary>
        /// <returns><c>UserMaster</c> object if there is a user logged in, null otherwise.</returns>        
        public User CurrentUser
        {
            get { return Session[SessionMember] as User; }
        }

        /// <summary>
        /// Allocate ourselves. We have a private constructor, so no one else can.
        /// </summary>
        static readonly SessionManager _instance = new SessionManager();

        private bool HasValidDatabaseSession()
        {
            bool hasValidSession = true;

            if (CurrentUser != null)
            {
                //check if it been long enough since we last checked database connection
                TimeSpan span = DateTime.Now.Subtract(CurrentUser.LastValidatedTime);

                if (span.TotalMinutes > ValidateSessionEvery)
                {
                    //using (var cs = new CustomerServiceProxy())
                    //{
                    //    CustomerService.DBSession session = cs.ValidateSession(CurrentUser.SessionKey);
                    //    return InitialiseSession(session);
                    //}
                    using (var cs = new ChargeBeeServiceProxy())
                    {
                        ChargeBeeService.DBSession session = cs.ValidateSession(CurrentUser.SessionKey);
                        return InitialiseSessionChargeBee(session);
                    }
                }
            }
            else
                hasValidSession = false;

            return hasValidSession;
        }

        /// <summary>
        /// Access SiteStructure.Instance to get the singleton object.
        /// Then call methods on that instance.
        /// </summary>
        public static SessionManager Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// This is a private constructor, meaning no outsiders have access.
        /// </summary>
        private SessionManager()
        {
            // Initialize members, etc. here.
        }

        public bool IsUserLoggedIn()
        {
            return CurrentUser != null && HasValidDatabaseSession();
        }

        /// <summary>
        /// Call this method to log on using a session from another system.
        /// </summary>
        public bool LogOnUsingOtherSession(string sessionKey)
        {
            using (var cs = new ChargeBeeServiceProxy())
            {
                DBSession session = cs.LoginUsingOtherSession(sessionKey);
                return InitialiseSessionChargeBee(session);
            }
        }

        /// <summary>
        /// Call this method to validate a session created by someone else.
        /// </summary>
        public bool LogOnUsingSession(string sessionKey)
        {
            using (var cs = new ChargeBeeServiceProxy())
            {
                DBSession session = cs.ValidateSession(sessionKey);
                return InitialiseSessionChargeBee(session);
            }
        }

        /// <summary>
        /// Call this method to log on using email address and password.
        /// </summary>
        public bool LogOnUsingEmailAddress(string emailAddress, string password)
        {
            using (var cs = new ChargeBeeServiceProxy())
            {
                DBSession session = cs.LoginUsingEmailAddress(emailAddress, password);
                return InitialiseSessionChargeBee(session);
            }
        }

        /// <summary>
        /// Call this method to logon using a session from the same system. The existing session is killed in the process.
        /// </summary>
        public bool LogOnReplacingSession(string sessionKey)
        {
            using (var cs = new ChargeBeeServiceProxy())
            {
                DBSession session = cs.LoginReplacingSession(sessionKey);
                return InitialiseSessionChargeBee(session);
            }
        }

        public void LogOff()
        {
            if (CurrentUser != null)
            {
                try
                {
                    using (var cs = new ChargeBeeServiceProxy())
                    {
                        cs.Logout(CurrentUser.SessionKey.ToString());
                    }

                    // Cleanup any search results stored in the cache for this session.
                    SearchDataCache.Remove(CurrentUser.UserID.ToString());

                    Session.Clear(); //this will not trigger Session_End.
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                }
            }
        }

        public void SendForgotPasswordEmail(string emailAddress)
        {
            using (var cs = new CustomerServiceProxy())
            {
                cs.SendForgotPasswordEmail(emailAddress);
            }
        }

        public bool AcknowledgeTermsReadAndLogin(string emailAddress, string password)
        {
            using (var cs = new ChargeBeeServiceProxy())
            {
                DBSession session = cs.AcknowledgeTermsReadAndLogin(emailAddress, password);
                return InitialiseSessionChargeBee(session);
            }
        }

        public bool InitialiseSession(CustomerService.DBSession session)
        {
            if (session != null && session.User.HasContactsWebAccess)
            {
                User currentUser = new User();

                currentUser.UserID = session.User.Id;
                currentUser.FullName = (session.User.FirstName + " " + session.User.LastName).Trim();
                currentUser.LogonName = session.User.LogonName;
                currentUser.Password = string.Empty;
                currentUser.DebtorNumber = session.User.DebtorNumber;
                currentUser.OutletId = string.Empty;
                currentUser.EmailAddress = session.User.EmailAddress;
                currentUser.FaxNumber = session.User.FaxNumber;
                currentUser.CompanyName = session.Customer.Name;//Change Here
                currentUser.OMAAccessRights = (char) session.User.MessageConnectAccessRights;
                currentUser.OMAAccess = session.User.HasContactsWebAccess;
                currentUser.MustChangePassword = session.User.MustChangePassword;
                currentUser.LastLogonTime = session.LoginDate;
                currentUser.SessionKey = session.Key;
                currentUser.HasViewedTermsAndConditions = session.Customer.HasViewedTermsAndConditionsP;
                currentUser.HasJournalistsWebAccess = session.User.HasJournalistsWebAccess;
                currentUser.HasDistributionWebAccess = session.User.HasDistributeWebAccess;
                currentUser.CanExportContacts = session.Customer.CanExportContacts;
                currentUser.LastValidatedTime = session.LastAccessedDate;
                currentUser.DefaultSearchCountry = session.Customer.DefaultSearchCountry;

                // Get the data modules for the user.
                using (DBClass db = new DBClass())
                {
                    UserBAL uBAL = new UserBAL(db);
                    User tmpUser = uBAL.GetStrongUser(session.User.Id);
                    currentUser.DataModules = tmpUser.DataModules;
                }

                //Set session variables.                    
                Session[SessionMember] = currentUser;

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool InitialiseSessionChargeBee(ChargeBeeService.DBSession session)
        {
            if (session != null && session.User.HasContactsWebAccess)
            {
                User currentUser = new User();

                currentUser.UserID = session.User.Id;
                currentUser.FullName = (session.User.FirstName + " " + session.User.LastName).Trim();
                currentUser.LogonName = session.User.LogonName;
                currentUser.Password = string.Empty;
                currentUser.DebtorNumber = session.User.DebtorNumber;
                currentUser.OutletId = string.Empty;
                currentUser.EmailAddress = session.User.EmailAddress;
                currentUser.FaxNumber = session.User.FaxNumber;
                currentUser.CompanyName = session.Customer.Name;
                currentUser.OMAAccessRights = (char)session.User.MessageConnectAccessRights;
                currentUser.OMAAccess = session.User.HasContactsWebAccess;
                currentUser.MustChangePassword = session.User.MustChangePassword;
                currentUser.LastLogonTime = session.LoginDate;
                currentUser.SessionKey = session.Key;
                currentUser.HasViewedTermsAndConditions = session.Customer.HasViewedTermsAndConditionsP;
                currentUser.HasJournalistsWebAccess = session.User.HasJournalistsWebAccess;
                currentUser.HasDistributionWebAccess = session.User.HasDistributeWebAccess;
                currentUser.CanExportContacts = session.Customer.CanExportContacts;
                currentUser.LastValidatedTime = session.LastAccessedDate;
                currentUser.DefaultSearchCountry = session.Customer.DefaultSearchCountry;

                // Get the data modules for the user.
                using (DBClass db = new DBClass())
                {
                    UserBAL uBAL = new UserBAL(db);
                    User tmpUser = uBAL.GetStrongUser(session.User.Id);
                    currentUser.DataModules = tmpUser.DataModules;
                }

                //Set session variables.                    
                Session[SessionMember] = currentUser;

                return true;
            }
            else
            {
                return false;
            }
        }
        public List<AAPContract> GetCustomerContracts()
        {
            var debtorNumber = CurrentUser.DebtorNumber;
            using (var cs = new ChargeBeeServiceProxy())
            {
                return cs.GetCustomerContracts(debtorNumber);
            }
        }

        public SalesForceContact GetSalesForceContact()
        {
            var userId = CurrentUser.UserID;
            var sessionKey = CurrentUser.SessionKey;
            //var debtorNumber = CurrentUser.DebtorNumber;
            //var emailAddress = CurrentUser.EmailAddress;
            using (var cs = new ChargeBeeServiceProxy())
            {
                return cs.GetSalesForceContact(userId, sessionKey);
            }
        }

        public Account GetContactProfile()
        {
            var userId = CurrentUser.UserID;
            var sessionKey = CurrentUser.SessionKey;
            using (var cs = new ChargeBeeServiceProxy())
            {
                return cs.GetContactProfile(userId, sessionKey);
            }
        }

        public bool UpdateAccount(ViewModels.Account.EditProfileViewModel editProfileViewModel)
        {
            using (var cs = new ChargeBeeServiceProxy())
            {
                Account account = cs.Proxy.GetAccountFromChargeBee(CurrentUser.UserID, CurrentUser.SessionKey);
                account.ContactPosition = editProfileViewModel.PositionTitle;
                //account.EmailAddress = editProfileViewModel.Email;
                account.FirstName = editProfileViewModel.FirstName;
                account.LastName = editProfileViewModel.LastName;
                account.TelephoneNumber = editProfileViewModel.Phone;
                account.IndustryCode = editProfileViewModel.BusinessType;
                account.RelevantSubjects = editProfileViewModel.RelevantSubjects;
                try
                {
                    cs.Proxy.UpdateChargeBeeAccount(account, CurrentUser.SessionKey);
                    return true;
                }
                catch (Exception ex)
                {
                    MPLogger.LogException(ex);
                    throw ex;
                }
            }            
        }
      
    }
}
