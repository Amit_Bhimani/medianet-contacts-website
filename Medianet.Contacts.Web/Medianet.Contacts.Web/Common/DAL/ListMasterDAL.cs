﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class ListMasterDAL
    {
        private DBClass _DBConnection;

        public ListMasterDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        ///<summary>
        ///Returns all recent Updates 
        /// </summary>
        public DataTable GetLlistUpdates()
        {
            DataTable dt = new DataTable();

            try
            {
                string strQuery = "e_GetLlistUpdates";
                OleDbParameter[] parameters = { };
                dt = _DBConnection.SelectQuery(strQuery, parameters, true);
                return dt;
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///<summary>
        ///Code written to call stored procedure for export list
        ///</summary>
        public DataTable GetExportListAll(int ListId, int userID,int pMaxRows)
        {
            DataTable dt = new DataTable();

            try
            {
                string strQuery = "e_GetExportListAll";
                OleDbParameter[] param = { new OleDbParameter("@listid", ListId),
                    new OleDbParameter("@USER_ID", userID),
                    new OleDbParameter("@MaxRowcount", pMaxRows)
                };
                dt = _DBConnection.SelectQuery(strQuery, param, true);
                return dt;
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///<summary>
        ///Code written to call stored procedure for export list
        ///</summary>
        public DataTable CountListRecords(int ListId, int userID)
        {
            DataTable dt = new DataTable();

            try
            {
                string strQuery = "e_CountListRecords";
                OleDbParameter[] param = { new OleDbParameter("@listid", ListId),
                    new OleDbParameter("@USER_ID", userID)
                };
                dt = _DBConnection.SelectQuery(strQuery, param, true);
                return dt;
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
