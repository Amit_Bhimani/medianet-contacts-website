﻿using System.Data;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for ContactRoleDAL
    /// </summary>
    public class ContactRoleDAL
    {
        private DBClass _DBConnection;

        public ContactRoleDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }
        /// <summary>
        /// Retrive all Contact Role
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllContactRole()
        {
            DataTable dt = new DataTable();

            dt = _DBConnection.SelectQuery("usp_GetAllContactRole",true);
            return dt;             
        }
    }
}