﻿using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for OmaOutletDAL
    /// </summary>
    public class OmaOutletDAL
    {
        private DBClass _DBConnection;

        public OmaOutletDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetPrivateOutlet(int outletId, string ownedBy)
        {
            string query = "oma_outlet_get";

            OleDbParameter[] param = {
                new OleDbParameter("@OutletId", outletId),
                new OleDbParameter("@OwnedBy", ownedBy)
            };
            return _DBConnection.SelectQuery(query, param, true);
        }

        /// <summary>
        /// This method insert/update the private outlet details . 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdatePrivateOutlet(PrivateOutletModel model)
        {
            int outletId = 0;

            string query = "oma_outlet_update";

            OleDbParameter[] parameters = { new OleDbParameter("@CompanyName", model.CompanyName),
                new OleDbParameter("@AddressLine1", model.AddressLine1),
                new OleDbParameter("@AddressLine2", model.AddressLine2),
                new OleDbParameter("@CityId", model.CityId),
                new OleDbParameter("@PostCode ",model.PostCode),
                new OleDbParameter("@StateId", model.StateId),
                new OleDbParameter("@MediaAtlasCountryId", model.MediaAtlasCountryId),
                new OleDbParameter("@PhoneNumber", model.PhoneNumber),
                new OleDbParameter("@FaxNumber", model.FaxNumber),
                new OleDbParameter("@EmailAddress", model.Email),
                new OleDbParameter("@OutletTypeId", model.OutletTypeId),
                new OleDbParameter("@ProductTypeId", model.ProductTypeId),
                new OleDbParameter("@Circulation", model.Circulation),
                new OleDbParameter("@PreferredDeliveryMethod", model.PreferredDeliveryMethod),
                new OleDbParameter("@WebSite", model.Website),
                new OleDbParameter("@Ownership", model.Ownership),
                new OleDbParameter("@StationFrequency", model.StationFrequency),
                new OleDbParameter("@FrequencyId", model.FrequencyId),
                new OleDbParameter("@Facebook", model.Facebook),
                new OleDbParameter("@LinkedIn", model.LinkedIn),
                new OleDbParameter("@Twitter", model.Twitter),
                new OleDbParameter("@BlogURL", model.BlogUrl),
                new OleDbParameter("@Notes", model.Notes),
                new OleDbParameter("@UpdatedBy", model.UpdatedBy),
                new OleDbParameter("@SubjectCodeIdsCsv", model.SubjectIdsCsv),
                new OleDbParameter("@WorkingLanguageIdsCsv", model.WorkingLanguageIdsCsv),
                new OleDbParameter("@OutletId", model.OutletId) };

            outletId = _DBConnection.Insert(query, parameters, true);
            model.OutletId = outletId;

            return true;
        }

        public bool CreatePrivateOutlet(PrivateOutletModel model)
        {
            model.OutletId = 0;
            return UpdatePrivateOutlet(model);
        }

        public int DeletePrivateOutlet(int outletId, string ownedBy)
        {
            string query = "oma_outlet_delete";

            OleDbParameter[] param = { new OleDbParameter("@OutletId", outletId), 
                new OleDbParameter("@OwnedBy", ownedBy)};
            return _DBConnection.Delete(query, param, true);
        }
    }
}