﻿using System;
using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Web.Common.BO;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for DistributeDAL
    /// </summary>
    public class DistributeDAL
    {
        private DBClass _DBConnection;

        public DistributeDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        ///<summary>
        ///This method  retrive all the the table to diaplay in distribution page.   
        ///</summary>
        public DataSet GetDistribution(int UserID, string debtornumber)
        {
            DataSet ds = new DataSet();

            OleDbParameter[] parameters = { new OleDbParameter("@userId", UserID), new OleDbParameter("@debtornumber", debtornumber) };
            ds = _DBConnection.SelectQueryMultiple("e_get_distribution_details", parameters, true);
            return ds;
        }

        ///<summary>
        ///This method insert the details in mn_document table,mn_job table,mn_distributionlist table
        ///</summary>
        ///
        public int InsertDistributionList(Distribute distribute)
        {
            string query = "e_InsertDistributionList";
            OleDbParameter[] parameters = {
                    new OleDbParameter("@DebtorNumber", distribute.DebtorNumber),
                    new OleDbParameter("@Companyname", distribute.CompanyName),
                    new OleDbParameter("@UserId",distribute.SubmittedUserId),
                    new OleDbParameter("@ReleaseDescription", distribute.ReleaseDescription),
                    new OleDbParameter("@WorkPhone", distribute.WorkPhone),
                    new OleDbParameter("@Email",distribute.Email),
                    new OleDbParameter("@Mobile",distribute.Mobile),
                    new OleDbParameter("@customerref", distribute.CustRef),
                    new OleDbParameter("@distributionlist", string.Empty),
                    new OleDbParameter("@Hold", distribute.Hold),
                    new OleDbParameter("@comment", distribute.Comment),
                    new OleDbParameter("@holdDatetime", distribute.HoldDateTime)
                };

            int rowsAffected = _DBConnection.Insert(query, parameters, true);
            return rowsAffected;
        }

        ///<summary>
        ///This method  retrive all Email and Fax based on preferred Distribution Method .   
        ///</summary>
        public DataSet GetEmailFax(String ListIDCsv)
        {
            DataSet ds = new DataSet();

            OleDbParameter[] parameters = { new OleDbParameter("@listIdCsv", ListIDCsv) };
            ds = _DBConnection.SelectQueryMultiple("e_EmailDetails", parameters, true);
            return ds;
        }
    }
}