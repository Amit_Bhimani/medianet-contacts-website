﻿using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class GeographicDAL
    {
        private DBClass _DBConnection;

        public GeographicDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetContinents()
        {
            return _DBConnection.SelectQuery("Continent_GetAll", true);
        }

        public DataTable GetCountries()
        {
            return _DBConnection.SelectQuery("Country_GetAll", true);
        }

        public DataTable GetStates()
        {
            return _DBConnection.SelectQuery("State_GetAll", true);
        }

        public DataTable GetCities()
        {
            return _DBConnection.SelectQuery("City_GetAll", true);
        }

        public DataTable GetCity(int cityId) {
            OleDbParameter[] parameters = { new OleDbParameter("@CityId", cityId) };

            return _DBConnection.SelectQuery("City_Get", parameters, true);
        }

        public DataTable SearchCities(int? continentId, int? countryId, int? stateId, string name) {
            OleDbParameter[] parameters = {new OleDbParameter("@ContinentId", continentId.HasValue ? continentId.Value : 0),
                                new OleDbParameter("@CountryId", countryId.HasValue ? countryId.Value : 0),
                                new OleDbParameter("@StateId", stateId.HasValue ? stateId.Value : 0),
                                new OleDbParameter("@CityName", name != null ? name : "")};

            return _DBConnection.SelectQuery("City_Search", parameters, true);
        }
    }
}
