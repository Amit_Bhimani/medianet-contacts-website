﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class OutletContactMastetDAL
    {
        private DBClass _DBConnection;

        public OutletContactMastetDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        /// <summary>
        ///   Get Contact  Details By Contact ID 
        /// </summary>
        /// <param name="ContactId">Contact ID</param>
        /// <returns>Datatable</returns>
        public DataTable GetAllOutletContactHomePageDAL(string OutletId, string ContactId)
        {
            try
            {
                string query = "usp_OutletContact_HomePage_Detail";
                OleDbParameter[] parameters = {new OleDbParameter("@OutletId", OutletId), new OleDbParameter("@ContactId", ContactId)};
                return _DBConnection.SelectQuery(query, parameters, true);
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}