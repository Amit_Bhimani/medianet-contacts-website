﻿using System.Data;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for ElectorateDAL
    /// </summary>
    public class ElectorateDAL
    {
        private DBClass _DBConnection;

        public ElectorateDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetStateName()
        {
            string query = "e_GetAllStates";
            return _DBConnection.SelectQuery(query, true);
        }
    }
}