﻿using System.Data;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class PositionDAL
    {
        private DBClass _DBConnection;

        public PositionDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetPositions()
        {
            return _DBConnection.SelectQuery("e_mn_contact_role", true);
        }
    }
}