﻿using System;
using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class OutletDAL
    {
        private DBClass db;

        public OutletDAL(DBClass dbConn)
        {
            db = dbConn;
        }


        public DataSet GetOmaOutlet(int OutletID, User loggedinuser, short contactCount)
        {
            DataSet ds = new DataSet();

            OleDbParameter[] parameters = {
                                            new OleDbParameter("@OutletID", OutletID),
                                            new OleDbParameter("@UserID", loggedinuser.UserID.ToString()),
                                            new OleDbParameter("@ContactCount", contactCount)
                                     };
            ds = db.Select("OmaOutlet_Get", parameters, true);

            return ds;
        }

        public DataSet GetMediaOutlet(string OutletID, User loggedinuser, short contactCount)
        {
            DataSet ds = new DataSet();

            OleDbParameter[] parameters = {
                                            new OleDbParameter("@OutletID", OutletID),
                                            new OleDbParameter("@UserID", loggedinuser.UserID.ToString()),
                                            new OleDbParameter("@ContactCount", contactCount)
                                     };
            ds = db.Select("MediaOutlet_Get", parameters, true);

            return ds;
        }
        public DataSet GetMediaOutletPreview(string OutletID, User loggedinuser)
        {
            DataSet ds = new DataSet();

            OleDbParameter[] parameters = {
                                            new OleDbParameter("@OutletID", OutletID),
                                            new OleDbParameter("@UserID", loggedinuser.UserID.ToString())
                                     };
            ds = db.Select("MediaOutletPreview_Get", parameters, true);

            return ds;
        }

        public DataSet GetPrnOutlet(long OutletID, User loggedinuser, short contactCount)
        {
            DataSet ds = new DataSet();

            OleDbParameter[] parameters = {
                                            new OleDbParameter("@OutletID", OutletID),
                                            new OleDbParameter("@UserID", loggedinuser.UserID.ToString()),
                                            new OleDbParameter("@ContactCount", contactCount)
                                        };
            ds = db.Select("PrnOutlet_Get", parameters, true);

            return ds;
        }

        public DataSet GetRecentlyAdded()
        {
            DataSet ds = null;

            try
            {
                OleDbParameter[] parameters = { new OleDbParameter("@Total", null) };
                ds = db.Select("Outlet_RecentlyAdded_Get", parameters, true);
            }
            catch (Exception e)
            {
                BasePage.LogException(e, typeof(OutletDAL).FullName, "Outlet_RecentlyAdded_Get", null);
            }

            return ds;
        }
    }
}