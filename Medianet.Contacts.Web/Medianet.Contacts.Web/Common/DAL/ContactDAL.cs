﻿using System;
using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class ContactDAL
    {
        private DBClass db;

        public ContactDAL(DBClass dbConn)
        {
            db = dbConn;
        }

        public DataSet GetOmaContact(int ContactId, string OutletId, User loggedinuser)
        {
            DataSet ds = new DataSet();
            try
            {
                OleDbParameter[] parameters = {new OleDbParameter("@ContactID", ContactId),
                                          new OleDbParameter("@UserID", loggedinuser.UserID.ToString())
                                     };
                ds = db.Select("OmaContact_Get", parameters, true);
            }
            catch (Exception e)
            {
                BasePage.LogException(e,
                    typeof(ContactDAL).FullName, "GetMediaContact", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId.ToString()}, {"OutletId", OutletId}, {"LoggedInUser", loggedinuser.ToString()}
                    });
            }
            return ds;
        }

        public DataSet GetMediaContact(string ContactId, string OutletId, User loggedinuser)
        {
            DataSet ds = new DataSet();

            try
            {
                OleDbParameter[] parameters = {
                                                  new OleDbParameter("@ContactID", ContactId),
                                                  new OleDbParameter("@OutletId", OutletId),
                                                  new OleDbParameter("@UserID", loggedinuser.UserID.ToString())
                                     };
                ds = db.Select("MediaContact_Get", parameters, true);
            }
            catch (Exception e)
            {
                BasePage.LogException(e,
                    typeof(ContactDAL).FullName, "GetMediaContact", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId}, {"OutletId", OutletId}, {"LoggedInUser", loggedinuser.ToString()}
                    });
            }
            return ds;
        }
        public DataSet GetMediaContactPreview(string ContactId, string OutletId, User loggedinuser)
        {
            DataSet ds = new DataSet();

            try
            {
                OleDbParameter[] parameters = {
                                                  new OleDbParameter("@ContactID", ContactId),
                                                  new OleDbParameter("@OutletId", OutletId),
                                                  new OleDbParameter("@UserID", loggedinuser.UserID.ToString())
                                     };
                ds = db.Select("MediaContactPreview_Get", parameters, true);
            }
            catch (Exception e)
            {
                BasePage.LogException(e,
                    typeof(ContactDAL).FullName, "GetMediaContactPreview", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId}, {"OutletId", OutletId}, {"LoggedInUser", loggedinuser.ToString()}
                    });
            }
            return ds;
        }

        public DataSet GetPrnContact(long ContactId, long OutletId, User loggedinuser)
        {
            DataSet ds = new DataSet();

            try
            {
                OleDbParameter[] parameters = {
                                                  new OleDbParameter("@ContactID", ContactId),
                                                  new OleDbParameter("@OutletId", OutletId),
                                                  new OleDbParameter("@UserID", loggedinuser.UserID.ToString())
                                     };
                ds = db.Select("PrnContact_Get", parameters, true);
            }
            catch (Exception e)
            {
                BasePage.LogException(e,
                    typeof(ContactDAL).FullName, "GetPrnContact", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId.ToString()}, {"OutletId", OutletId.ToString()}, {"LoggedInUser", loggedinuser.ToString()}
                    });
            }
            return ds;
        }

        public DataSet GetRecentlyAdded()
        {
            DataSet ds = null;

            try
            {
                OleDbParameter[] parameters = { new OleDbParameter("@Total", null) };
                ds = db.Select("Contact_RecentlyAdded_Get", parameters, true);
            }
            catch (Exception e)
            {
                BasePage.LogException(e, typeof(ContactDAL).FullName, "Contact_RecentlyAdded_Get", null);
            }

            return ds;
        }
    }
}