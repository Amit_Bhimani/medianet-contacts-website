﻿using Medianet.Contacts.Web.Common;
using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class GroupDAL
    {
        private DBClass db;

        public GroupDAL(DBClass dbConn) {
            db = dbConn;
        }

        #region Read

        /// <summary>
        /// Get <c>Groups</c> by ID.
        /// </summary>
        /// <param name="groupId">The ID of the group to retrieve.</param>
        /// <param name="userId">The current user ID.</param>
        /// <returns></returns>
        public DataTable GetGroupById(int groupId, int userId) {
            DataTable dt = null;

            try {
                OleDbParameter[] parameters = { new OleDbParameter("@GroupId", groupId), new OleDbParameter("@UserID", userId) };
                dt = db.SelectQuery("e_Group_GroupId_Get", parameters, true);
            }
            catch (OleDbException ex_sql) {
                throw ex_sql;
            }
            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        /// <summary>
        /// Get <c>Groups</c> by IDs.
        /// </summary>
        /// <param name="groupIds">a CSV list of group Ids to retrieve.</param>
        /// <param name="userId">The current user ID.</param>
        /// <returns></returns>
        public DataTable GetGroupsByIds(string groupIds, int userId) {
            DataTable dt = null;

            try {
                OleDbParameter[] parameters = { new OleDbParameter("@GroupIds", groupIds), new OleDbParameter("@UserID", userId) };
                dt = db.SelectQuery("e_Group_GroupIds_Get", parameters, true);
            }
            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }


        /// <summary>
        /// Renames the given group.
        /// </summary>
        /// <param name="id">ID of the list group.</param>
        /// <param name="name">The new name.</param>
        /// <returns></returns>
        public int Rename(int id, string name)
        {
            try
            {
                string query = "e_GroupListRename";

                OleDbParameter[] parameters = { new OleDbParameter("@name", id),
                                            new OleDbParameter("@id", name) };

                return db.Update(query, parameters, true);
            }
            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }
            catch (IndexOutOfRangeException index_ex)
            {
                throw index_ex;
            }
            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}