﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for SavedSearchMasterDAL
    /// </summary>
    public class SavedSearchMasterDAL
    {
        private DBClass _DBConnection;

        public SavedSearchMasterDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetSearchList(int groupId, int UserId, string DebtorNumber)
        {
            try
            {
                OleDbParameter[] parameter = { new OleDbParameter("@groupId", groupId), new OleDbParameter("@UserId ", UserId), new OleDbParameter("@DebtorNumber", DebtorNumber) };
                return _DBConnection.SelectQuery("e_SavedSearchMaster_SearchName", parameter, true);
            }
            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (IndexOutOfRangeException index_ex)
            {
                throw index_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// It returns the GroupName and Saved search list by UserId
        /// </summary>  
        /// <param name="UserId">UserId of Logged In User</param>
        /// <returns>@DataTable</returns>
        public DataTable GetSavedSearches(int UserId)
        {
            try
            {
                OleDbParameter[] parameter = { new OleDbParameter("@UserId ", UserId) };
                return _DBConnection.SelectQuery("e_SavedSearchMasterByUserId", parameter, true);
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (IndexOutOfRangeException index_ex)
            {
                throw index_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        public int Delete(int SearchId) {
            try {
                OleDbParameter[] parameter = { new OleDbParameter("@SearchId ", SearchId) };
                return _DBConnection.Update("e_SavedSearchMasterDelete", parameter, true);
            }
            catch (OleDbException sql_ex) {
                throw sql_ex;
            }
            catch (IndexOutOfRangeException index_ex) {
                throw index_ex;
            }
            catch (NullReferenceException null_ex) {
                throw null_ex;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        /// <summary>
        /// Soft deletes the given group of saved searches.
        /// </summary>
        /// <param name="groupid">The group ID.</param>
        public int DeleteGroupSearch(int groupid, int userid)
        {
            try
            {
                string query = "e_GroupSavedSearchDelete";

                OleDbParameter[] parameters = { new OleDbParameter("@groupid", groupid),
                                            new OleDbParameter("@userid", userid) };

                return _DBConnection.Update(query, parameters, true);
            }
            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }
            catch (IndexOutOfRangeException index_ex)
            {
                throw index_ex;
            }
            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

