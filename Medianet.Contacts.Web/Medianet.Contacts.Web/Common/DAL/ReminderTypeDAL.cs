﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class ReminderTypeDAL
    {
        private DBClass _DBConnection;

        public ReminderTypeDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        /// <summary>
        ///   Get  Reminder Types
        /// </summary>
        /// <returns>Datatable</returns>
        public DataTable GetReminderTypes()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = _DBConnection.SelectQuery("e_GetReminderType", true);
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}