﻿using System.Data.OleDb;
using Medianet.Contacts.Web.Common.BO;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for GeneralContactUsBAL
    /// </summary>
    public class GeneralContactUsDAL
    {
        private DBClass _DBConnection;

        public GeneralContactUsDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public int Insert(GeneralContactUs newContact)
        {
            string query = "usp_mn_AddGeneralContact_Insert";
            // OleDbParameter id = new OleDbParameter();
            //id.ParameterName = "@Id";
            //id.Direction = ParameterDirection.Output;
            //id.DbType = DbType.Int32;

            OleDbParameter[] parameters = {new OleDbParameter("@Subject", newContact.Subject),
                new OleDbParameter("@Comment", newContact.Comment),
                new OleDbParameter("@CreatedDate", newContact.CreatedDate),
                new OleDbParameter("@Status", newContact.Status),
                new OleDbParameter("@FirstName", newContact.FirstName),
                new OleDbParameter("@Email", newContact.Email),
                new OleDbParameter("@ClientServiceEmail", newContact.ClientServiceEmail),
                new OleDbParameter("@PhoneNo", newContact.PhoneNo),
                new OleDbParameter("@IpAddress", newContact.IpAddress),
                new OleDbParameter("@Referer", newContact.Referer)
            };
            return _DBConnection.Insert(query, parameters, true);
        }
    }
}