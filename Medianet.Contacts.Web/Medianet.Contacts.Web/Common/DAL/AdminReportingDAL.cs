﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for AdminReportingDAL
    /// </summary>
    public class AdminReportingDAL
    {
        private DBClass _DBConnection;

        public AdminReportingDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        ///<summary>
        ///Method to retrive all user information from Db According to user and its rights
        ///</summary>

        public DataSet AdminReporting(int UserID)
        {
            DataSet dt = new DataSet();

            try
            {
                OleDbParameter[] parameters = { new OleDbParameter("@userId", UserID) };
                dt = _DBConnection.SelectQueryMultiple("e_AdminReporting", parameters, true);
                return dt;
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet AdminReportingDateWise(int UserID, DateTime datetime)
        {
            DataSet dt = new DataSet();

            try
            {
                OleDbParameter[] parameters = { new OleDbParameter("@userId", UserID), new OleDbParameter("@datetime", datetime) };
                dt = _DBConnection.SelectQueryMultiple("e_AdminReportingsearch", parameters, true);
                return dt;
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}