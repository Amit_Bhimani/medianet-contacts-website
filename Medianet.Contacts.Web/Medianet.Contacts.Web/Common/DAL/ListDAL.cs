﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class ListDAL
    {
        private DBClass db;

        public ListDAL(DBClass dbConn) {
            db = dbConn;
        }

        #region Read

        /// <summary>
        /// Get <c>Lists</c> by group ID.
        /// </summary>
        /// <param name="groupId">Group ID to search by.</param>
        /// <param name="userId">The Id of the current user.</param>
        public DataTable GetListsByGroupId(int groupId, int userId) {
            DataTable dt = null;

            try {
                OleDbParameter[] parameters = { new OleDbParameter("@GroupId", groupId),
                                                new OleDbParameter("@UserID", userId) };
                dt = db.SelectQuery("e_Lists_GroupId_Get", parameters, true);
            }
            catch (OleDbException ex_sql) {
                throw ex_sql;
            }
            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        /// <summary>
        /// Get's lists by a CSV of list ids.
        /// </summary>
        /// <param name="listids">The CSV of list ids.</param>
        public DataTable GetListsByIds(string listIds) {
            DataTable dt = null;

            try {
                OleDbParameter[] parameters = { new OleDbParameter("@ListIds", listIds) };
                dt = db.SelectQuery("e_List_ListIds_Get", parameters, true);
            }
            catch (OleDbException ex_sql) {
                throw ex_sql;
            }
            catch (Exception ex) {
                throw ex;
            }
            return dt;
        }

        #endregion

        #region Create

        /// <summary>
        /// Inserts a new list in the database.
        /// </summary>
        /// <param name="list">The List object to insert.</param>
        /// <returns>The ID of the new list.</returns>
        public int Insert(List list) {
            try {
                string query = "e_List_Insert";

                OleDbParameter[] parameters = { new OleDbParameter("@ListName", list.Name),
                                                new OleDbParameter("@Status", true),                
                                                new OleDbParameter("@GroupId", list.BelongsToGroup.Id),                                        
                                                new OleDbParameter("@IsPrivate",list.IsPrivate),
                                                new OleDbParameter("@UserId", list.UserId)
                                              };

                return db.Insert(query, parameters, true);
            }
            catch (OleDbException sql_ex) {
                throw sql_ex;
            }
            catch (IndexOutOfRangeException index_ex) {
                throw index_ex;
            }
            catch (NullReferenceException null_ex) {
                throw null_ex;
            }
            catch (Exception ex) {
                throw ex;
            }
        }  

        /// <summary>
        /// Merges all list records contained in the list of ids. Deals with duplicates.
        /// </summary>
        /// <param name="listId">The list that the merged list records should be placed in.</param>
        /// <param name="listIds">A <c>List&lt;int&gt;</c> of list IDs to be merged.</param>
        /// <returns></returns>
        public int Merge(int listId, List<int> listIds, int userId) {
            try {
                string query = "e_List_Merge";

                OleDbParameter[] parameters = { new OleDbParameter("@ListIds", string.Join("#", listIds.Select(i => i.ToString()).ToArray())),
                                                new OleDbParameter("@UserId", userId),
                                                new OleDbParameter("@ListId", listId) };

                return db.Insert(query, parameters, true);
            }
            catch (OleDbException sql_ex) {
                throw sql_ex;
            }
            catch (IndexOutOfRangeException index_ex) {
                throw index_ex;
            }
            catch (NullReferenceException null_ex) {
                throw null_ex;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        #endregion
    }
}