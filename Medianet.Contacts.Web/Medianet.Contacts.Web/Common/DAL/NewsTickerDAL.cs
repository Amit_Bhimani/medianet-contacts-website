﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class NewsTickerDAL
    {
        private DBClass _DBConnection;

        public NewsTickerDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetNewsStories()
        {
            try
            {
                string query = "getLatestStories";
                OleDbParameter[] param = { new OleDbParameter("@storyCount", 15) };
                return _DBConnection.SelectQuery(query, param, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

