﻿using System;
using System.Data.OleDb;
using SavedSearch = Medianet.Contacts.Web.Common.Model.SavedSearch;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class SearchDAL
    {
        private DBClass db;

        public SearchDAL(DBClass dbConn) {
            db = dbConn;
        }

        #region Saved Searches

        /// <summary>
        /// Saves a search to the database.
        /// </summary>
        /// <param name="search">The search object to persist.</param>
        /// <returns>ID of the new saved search.</returns>
        public int Insert(SavedSearch search) {
            try {
                OleDbParameter[] parameters = { new OleDbParameter("@SearchName", search.Name),
                    new OleDbParameter("@SearchQuery", search.Query), 
                    new OleDbParameter("@Status", search.Status), 
                    new OleDbParameter("@GroupId", search.Group.Id), 
                    new OleDbParameter("@CreatedDate", search.CreatedDate), 
                    new OleDbParameter("@ModifiedDate", search.ModifiedDate),
                    new OleDbParameter("@UserId", search.UserId),
                    new OleDbParameter("@Visibility", search.Visibility),
                    new OleDbParameter("@ContextText", search.Context),
                    new OleDbParameter("@SearchCriteria", search.Criteria),
                    new OleDbParameter("@ResultsCount", search.ResultsCount)};

                return db.Insert("usp_SavedSearchMaster_Insert", parameters, true);
            }
            catch (OleDbException sql_ex) {
                throw sql_ex;
            }
            catch (IndexOutOfRangeException index_ex) {
                throw index_ex;
            }
            catch (NullReferenceException null_ex) {
                throw null_ex;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        /// <summary>
        /// Update the name and visibility of a saved search.
        /// </summary>
        /// <param name="search">The search object to persist.</param>
        public void Update(SavedSearch search)
        {
            try
            {
                OleDbParameter[] parameters = {
                    new OleDbParameter("@SearchId", search.Id),
                    new OleDbParameter("@SearchName", search.Name),
                    new OleDbParameter("@Visibility", search.Visibility)};

                db.Insert("usp_SavedSearchMaster_Update", parameters, true);
            }
            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }
            catch (IndexOutOfRangeException index_ex)
            {
                throw index_ex;
            }
            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
