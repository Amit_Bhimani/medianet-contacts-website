﻿using System.Data;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class MediaTypeDAL
    {
        private DBClass _DBConnection;

        public MediaTypeDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetMediaTypes()
        {
            return _DBConnection.SelectQuery("e_mn_product_type", true);
        }
    }
}