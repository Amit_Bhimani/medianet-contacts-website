﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for OutletContactXrefDAL
    /// </summary>
    public class OutletContactXrefDAL
    {
        private DBClass _DBConnection;

        public OutletContactXrefDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }
        /// <summary>
        /// gets the lastest added outlet name.
        /// </summary>
        /// <returns></returns>
        public DataTable GetOutlet()
        {
            try
            {
                string query = "usp_RecentlyAddedGetOutlet";

                return _DBConnection.SelectQuery(query, true);
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Gets the details of lastest conatacts.
        /// </summary>
        /// <returns></returns>
        public DataTable GetContacts()
        {
            try
            {
                string query = "usp_RecentlyAddedGetContacts";

                return _DBConnection.SelectQuery(query, true);
            }
            catch (OleDbException ex_sql)
            {
                throw ex_sql;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
   
    }
}