﻿using System.Data;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class GeneralDAL
    {
        private DBClass db;

        public GeneralDAL(DBClass db) {
            this.db = db;
        }

        /// <summary>
        /// Gets Outlet Frequencies from the database table mn_outlet_frequency
        /// </summary>
        /// <returns>DataTable containing Outlet Frequencies. Columns [FrequencyId, FrequencyName ]</returns>
        public DataTable GetFrequencies() {
            return db.SelectQuery("e_mn_outlet_frequency", true);
        }

        /// <summary>
        /// Gets all Languages from the database table mn_working_language.
        /// </summary>
        /// <returns>DataTable for Languages. Columns [WorkingLanguageId, WorkingLanguageName]</returns>    
        public DataTable GetWorkingLanguage() {
            return db.SelectQuery("e_mn_working_language", true);
        }
    }
}