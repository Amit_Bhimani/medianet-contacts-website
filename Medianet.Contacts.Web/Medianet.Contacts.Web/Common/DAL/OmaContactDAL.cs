﻿using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for OmaContactDAL
    /// </summary>
    public class OmaContactDAL
    {
        private DBClass _DBConnection;

        public OmaContactDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetPrivateContact(int ContactId, string OwnerDebornuber)
        {
            string query = "oma_contact_get";

            OleDbParameter[] param = { new OleDbParameter("@ContactId", ContactId), 
                new OleDbParameter("@OwnedBy", OwnerDebornuber)};
            return _DBConnection.SelectQuery(query, param, true);
        }

        public bool UpdatePrivateContact(PrivateContactModel model)
        {
            int contactId = 0;

            string query = "oma_contact_update";

            OleDbParameter[] parameters = {                                         
                new OleDbParameter("@Prefix ",model.Prefix),
                new OleDbParameter("@FirstName ",model.FirstName),
                new OleDbParameter("@MiddleName ",model.MiddleName),
                new OleDbParameter("@LastName ",model.LastName),
                new OleDbParameter("@JobTitle ",model.JobTitle),
                new OleDbParameter("@AddressLine1 ",model.AddressLine1),
                new OleDbParameter("@AddressLine2 ",model.AddressLine2),
                new OleDbParameter("@CityId ",model.CityId),
                new OleDbParameter("@PostCode ",model.PostCode),
                new OleDbParameter("@StateId ",model.StateId),
                new OleDbParameter("@MediaAtlasCountryId ",model.CountryId),
                new OleDbParameter("@PhoneNumber ",model.PhoneNumber),
                new OleDbParameter("@FaxNumber ",model.FaxNumber),
                new OleDbParameter("@MobileNumber ",model.MobileNumber),
                new OleDbParameter("@EmailAddress ",model.EmailAddress),
                new OleDbParameter("@PreferredDeliveryMethod ",model.PreferredDeliveryMethod),
                new OleDbParameter("@Facebook ",model.Facebook),
                new OleDbParameter("@LinkedIn ",model.LinkedIn),
                new OleDbParameter("@Twitter ",model.Twitter),
                new OleDbParameter("@BlogURL ",model.BlogUrl),
                new OleDbParameter("@Notes ",model.Notes),
                new OleDbParameter("@UpdatedBy ",model.UpdatedBy),
                new OleDbParameter("@SubjectCodeIdsCsv ",model.SubjectIdsCsv),
                new OleDbParameter("@RoleIdsCsv ",model.RoleIdsCsv),
                new OleDbParameter("@MediaOutletId ",model.MediaOutletId),
                new OleDbParameter("@PrnOutletId ",model.PrnOutletId),
                new OleDbParameter("@OmaOutletId ",model.PrivateOutletId),
                new OleDbParameter("@ContactId ",model.ContactId.Value)
            };

            contactId = _DBConnection.Insert(query, parameters, true);

            model.ContactId = contactId;

            return true;
        }

        public bool CreatePrivateContact(PrivateContactModel model)
        {
            model.ContactId = 0;
            return UpdatePrivateContact(model);
        }
        
        public int DeletePrivateContact(int ContactId, string OwnerDebornuber)
        {
            string query = "oma_contact_delete";

            OleDbParameter[] param = { new OleDbParameter("@ContactId", ContactId), 
                new OleDbParameter("@OwnedBy", OwnerDebornuber)};
            return _DBConnection.Delete(query, param, true);
        }
    }
}