﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    /// <summary>
    /// Summary description for UserMasterDAL
    /// </summary>
    public class UserMasterDAL
    {
        private DBClass _DBConnection;

        public UserMasterDAL(DBClass dbConn) {
            _DBConnection = dbConn;
        }

        /// <summary>
        /// This function return the userdetails on the basis of UserId
        /// </summary>
        /// <param name="Id">@Id Integer Value</param>
        /// <returns>@tblUser returns datatable</returns>
        public DataSet GetUserDataSet(int UserId) {

            DataSet ds = new DataSet();

            try {
                OleDbParameter[] parameters = { new OleDbParameter("@Id", UserId) };
                ds = _DBConnection.Select("sp_user_getItem", parameters, true);
            }
            catch (Exception e) {
                BasePage.LogException(e,
                    typeof(UserMasterDAL).FullName, "GetUserDataSet", new System.Collections.Specialized.NameValueCollection() {
                        {"UserId", UserId.ToString()}});
            }

            return ds;
        }

        /// <summary>
        /// This give all Active Users  on the basis of DebtorNumber.
        /// </summary>
        /// <param name="DebtorNumber">@DebtorNumber String variables</param>
        /// <returns>@dtCompanyDetails</returns>    
        public DataTable GetUsersOnDebtorId(string DebtorNumber) {
            DataTable dt = new DataTable();

            try {
                OleDbParameter[] parameters = { new OleDbParameter("@DebtorNumber", DebtorNumber) };
                dt = _DBConnection.SelectQuery("e_GetUsersByDebtorNumber", parameters, true);
                return dt;
            }
            catch (OleDbException ex_sql) {
                throw ex_sql;
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}