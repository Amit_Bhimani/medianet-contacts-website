﻿using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Web.Common.DAL
{
    public class SubjectDAL
    {
        private DBClass _DBConnection;

        public SubjectDAL(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetSubjects()
        {
            return _DBConnection.SelectQuery("e_mn_subject", true);
        }

        public DataTable GetSubjectsByGroup(string group)
        {
            return _DBConnection.SelectQuery("Subjects_Get_Group", new OleDbParameter[1]{
                    new OleDbParameter(){
                        DbType = DbType.String,
                        IsNullable = true,
                        Value = group == null ? string.Empty : group
                    }
            }, true);
        }

    }
}
