﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using System.Text;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Common;

/// <summary>
/// Summary description for GroupListDAL
/// </summary>

public class GroupListDAL
{
    private DBClass _DBConnection;

    public GroupListDAL(DBClass dbConn) {
        _DBConnection = dbConn;
    }

    /// <summary>
    /// Soft deletes the given group. Public lists and private lists that belong to the user provided
    /// are also deleted. Private lists that don't belong to the user are not deleted.
    /// </summary>
    /// <param name="groupid">The group ID.</param>
    /// <param name="userid">The user ID.</param>
    public int DeleteGroupList(int groupid, int userid) {
        try {
            string query = "e_GroupListDelete";

            OleDbParameter[] parameters = { new OleDbParameter("@groupid", groupid),
                                            new OleDbParameter("@userid", userid) };

            return _DBConnection.Update(query, parameters, true);
        }
        catch (OleDbException sql_ex) {
            throw sql_ex;
        }
        catch (IndexOutOfRangeException index_ex) {
            throw index_ex;
        }
        catch (NullReferenceException null_ex) {
            throw null_ex;
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    /// <summary>
    /// This function returns the list object on the basis of listid.
    /// </summary>
    /// <param name="id">@id integer value parameter</param>
    /// <returns>@list returns list</returns>
    public GroupList GetById(int id) {
        try {
            string query = "e_get_grouplists";
            OleDbParameter[] param = { new OleDbParameter("@ListId", id) };
            DataTable tbl = _DBConnection.SelectQuery(query, param, true);
            GroupList list = new GroupList();

            if (tbl.Rows.Count > 0) {
                foreach (DataRow row in tbl.Rows) {
                    list = ConvertToObject(row);
                }
            }

            return list;
        }
        catch (OleDbException sql_ex) {
            throw sql_ex;
        }
        catch (IndexOutOfRangeException index_ex) {
            throw index_ex;
        }
        catch (NullReferenceException null_ex) {
            throw null_ex;
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    /// <summary>
    /// This function is used to fill object of GroupList class by DataRow
    /// </summary>
    /// <param name="row">@row DataRow Parameter</param>
    /// <returns>@list return GroupList</returns>
    private GroupList ConvertToObject(DataRow row) {
        try {
            GroupList list = new GroupList();

            if (!row.IsNull("ListId")) {
                list.Grouplistid = Convert.ToInt32(row["listid"]);
            }

            if (!row.IsNull("ListName")) {
                list.Listname = row["listname"].ToString();
            }

            if (!row.IsNull("Status")) {
                list.Status = Convert.ToInt16(row["status"]);
            }

            if (!row.IsNull("CreatedDate")) {
                list.Createddate = Convert.ToDateTime(row["createddate"]);
            }

            if (!row.IsNull("UpdatedDate")) {
                list.Updateddate = Convert.ToDateTime(row["updateddate"]);
            }

            if (!row.IsNull("GroupId")) {
                list.Groupid = Convert.ToInt32(row["groupid"]);
            }

            if (!row.IsNull("IsPrivate")) {
                list.IsPrivate = Convert.ToBoolean(row["IsPrivate"]);
            }

            if (!row.IsNull("UserId")) {
                list.UserId = Convert.ToInt32(row["UserId"]);
            }

            return list;
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    /// <summary>
    /// This function is used to restore the history list.
    /// </summary>
    /// <param name="_activelistid">@_activelistid integer value parameter</param>
    /// <param name="_versiontorestoreid">@_versiontorestoreid integer value parameter</param>
    /// <returns>return int value</returns>
    //public int RestoreList(int _activelistid, int _versiontorestoreid)
    //{
    //    try
    //    {
    //        OleDbParameter[] param = { new OleDbParameter("@activelistid", _activelistid), new OleDbParameter("@versiontorestoreid", _versiontorestoreid) };
    //        return _DBConnection.Update("mn_GroupList_RestoreList", param, true);
    //    }
    //    catch (OleDbException sql_ex)
    //    {
    //        throw sql_ex;
    //    }
    //    catch (IndexOutOfRangeException index_ex)
    //    {
    //        throw index_ex;
    //    }
    //    catch (NullReferenceException null_ex)
    //    {
    //        throw null_ex;
    //    }
    //    catch (DivideByZeroException zero_ex)
    //    {
    //        throw zero_ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    /// <summary>
    /// This method is used to rename the list name on ListId
    /// </summary>
    /// <param name="ListId">Int parameter</param>
    /// <param name="ListName">String parameter</param>
    /// <returns>@Int</returns>

    public int UpdateList(int ListId, string ListName, bool isPrivate, int userId) {
        try {
            OleDbParameter[] param = { new OleDbParameter("@listid", ListId),
                                       new OleDbParameter("@listname", ListName),
                                       new OleDbParameter("@IsPrivate", isPrivate),
                                       new OleDbParameter("@UserId", userId)};
            return _DBConnection.Update("e_List_Update", param, true);
        }
        catch (OleDbException sql_ex) {
            throw sql_ex;
        }
        catch (IndexOutOfRangeException index_ex) {
            throw index_ex;
        }
        catch (NullReferenceException null_ex) {
            throw null_ex;
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    /// <summary>
    /// Get the list of lists on Users of that company and with his private and all public records. 
    /// </summary>
    /// <param name="UserId">Int Parameter</param>
    /// <param name="DebtorNumber">String Parameter</param>
    /// <returns>@DataTable</returns>

    public DataTable GetListsListDebtorUserId(int UserId, string DebtorNumber) {
        try {
            OleDbParameter[] param = { new OleDbParameter("@DebtorNumber", DebtorNumber), new OleDbParameter("@UserId", UserId) };
            return _DBConnection.SelectQuery("e_List_DebtorNo_UserId", param, true);
        }
        catch (OleDbException sql_ex) {
            throw sql_ex;
        }
        catch (IndexOutOfRangeException index_ex) {
            throw index_ex;
        }
        catch (NullReferenceException null_ex) {
            throw null_ex;
        }
        catch (DivideByZeroException zero_ex) {
            throw zero_ex;
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    /// <summary>
    /// Get the list of lists on Users of that company and with his private and all public records for a particular group. 
    /// </summary>
    /// <param name="userId">Int Parameter</param>
    /// <param name="debtorNumber">String Parameter</param>
    /// <param name="groupId"></param>
    /// <returns>@DataTable</returns>
    public DataTable GetListsListDebtorUserIdGroupId(int groupId, int userId, string debtorNumber)
    {
        OleDbParameter[] param = { new OleDbParameter("@DebtorNumber", debtorNumber), new OleDbParameter("@UserId", userId), new OleDbParameter("@GroupId", groupId) };
        return _DBConnection.SelectQuery("e_List_DebtorNo_UserId_GroupId", param, true);
    }



    /// <summary>
    /// This method is used to delete the lists and its related records in records
    /// </summary>
    /// <param name="UserId">Int parameter which is userid or the loggedIn user</param>
    /// <param name="ListId">Int parameter</param>
    /// <returns>@Int</returns>

    public int DeleteOnListIdAndUserId(int UserId, int ListId) {
        try {
            OleDbParameter[] param = { new OleDbParameter("@ListId", ListId), new OleDbParameter("@UserId", UserId) };
            return _DBConnection.Delete("e_List_Delete_UserId_ListId", param, true);
        }
        catch (OleDbException sql_ex) {
            throw sql_ex;
        }
        catch (IndexOutOfRangeException index_ex) {
            throw index_ex;
        }
        catch (NullReferenceException null_ex) {
            throw null_ex;
        }
        catch (Exception ex) {
            throw ex;
        }
    }
}
