﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Common
{
    public class MedianetHelpers
    {
        /// <summary>
        /// Takes a user defined enumeration, and returns a SelectList - to be used for dropdown/combobox controls
        /// </summary>
        /// <param name="enumType">The Enum type to convert to a select list</param>
        /// <param name="selectedItem">The item that is required to be selected when the markup is rendered</param>
        /// <returns>A Select list (Value/text pair) representing a dropdown list</returns>
        public static List<SelectListItem> EnumToSelectList(Type enumType, string selectedItem)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in Enum.GetValues(enumType))
            {
                FieldInfo fi = enumType.GetField(item.ToString());
                var attribute = fi.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                
                SelectListItem listItem = new SelectListItem
                {
                    Value = item.ToString(),
                    Text = attribute == null ? item.ToString() : ((DescriptionAttribute)attribute).Description,
                    Selected = selectedItem == item.ToString()
                };

                items.Add(listItem);
            }
            
            return items;
        }
    }
}