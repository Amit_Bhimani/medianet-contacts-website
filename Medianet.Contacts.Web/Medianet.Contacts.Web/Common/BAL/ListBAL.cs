﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class ListBAL
    {
        private ListDAL dal;

        public ListBAL(DBClass dbConn) {
            dal = new ListDAL(dbConn);
        }

        #region Read

        /// <summary>
        /// Get lists by group ID.
        /// </summary>
        /// <param name="groupId">The group ID to search on.</param>
        /// <param name="userId">The Id of the current user.</param>
        /// <returns>List of <c>ListBase</c> objects.</returns>
        public List<ListBase> GetListsByGroupId(int groupId, int userId) {
            DataTable dt = dal.GetListsByGroupId(groupId, userId);
            List<ListBase> lists = null;

            lists = (from DataRow row in dt.Rows
                     select new ListBase {
                         Id = (int)row["ListId"],
                         Name = (string)row["ListName"],
                         IsPrivate = (bool)row["IsPrivate"],
                         BelongsToGroup = new GroupBase { Id = (int)row["GroupId"] },
                         UserId = (int)row["UserId"]
                     }).ToList<ListBase>();

            return lists;
        }

        /// <summary>
        /// Get's lists by a CSV of list ids.
        /// </summary>
        /// <param name="listids">The CSV of list ids.</param>
        /// <returns>List of <c>ListBase</c> objects.</returns>
        public List<ListBase> GetListsByIds(string listids) {
            DataTable dt = dal.GetListsByIds(listids);
            List<ListBase> lists = null;

            lists = (from DataRow row in dt.Rows
                     select new ListBase {
                         Id = (int)row["ListId"],
                         Name = (string)row["ListName"],
                         IsPrivate = (bool)row["IsPrivate"],
                         BelongsToGroup = new GroupBase { Id = (int)row["GroupId"] },
                         UserId = (int)row["UserId"]
                     }).ToList<ListBase>();

            return lists;
        }

        #endregion

        #region Create

        /// <summary>
        /// Inserts list into the database.
        /// </summary>
        /// <param name="list">The list to insert.</param>
        /// <returns>The ID of the new list.</returns>
        public int Insert(List list) {
            return dal.Insert(list);
        }

        /// <summary>
        /// Merges the provided list with each list in the <c>ListIds</c>.
        /// </summary>
        /// <param name="listId">The mergee.</param>
        /// <param name="ListIds">The merger(s).</param>
        /// <param name="userId">The current user Id.</param>
        /// <returns>The ID of the last record merged.</returns>
        public int MergeList(int listId, List<int> ListIds, int userId) {
            return dal.Merge(listId, ListIds, userId);
        }

        #endregion
    }
}