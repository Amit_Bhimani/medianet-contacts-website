﻿using System.Collections.Generic;
using System.Data;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class ReminderTypeBAL
    {
        private ReminderTypeDAL _DAL;

        public ReminderTypeBAL(DBClass dbConn) {
            _DAL = new ReminderTypeDAL(dbConn);
        }

        public List<ReminderType> GetReminderTypes() {
            DataTable dt = _DAL.GetReminderTypes();
            List<ReminderType> reminderTypes = null;

            if (dt != null && dt.Rows.Count > 0) {
                reminderTypes = new List<ReminderType>();

                foreach (DataRow row in dt.Rows) {
                    ReminderType r = new ReminderType();

                    r.Id = (int)row["Id"];
                    r.Name = (string)row["RemType"];
                    r.Minutes = (int)row["Minutes"];
                    reminderTypes.Add(r);
                }
            }
            return reminderTypes;
        }
    }
}
