﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for GeographicBAL
    /// </summary>
    public class GeographicBAL
    {
        private const int CACH_GEOGRAPHIC_TIME = 1800; // 30 minutes.
        private const string CACHE_KEY_CONTINENTS = "continents";
        private const string CACHE_KEY_COUNTRIES = "countries";
        private const string CACHE_KEY_STATES = "states";
        private const string CACHE_KEY_CITIES = "cities";

        private GeographicDAL _DAL;

        public GeographicBAL(DBClass dbConn) {
            _DAL = new GeographicDAL(dbConn);
        }

        public List<Continent> GetContinents() {
            List<Continent> continents = Cacher.Get(CACHE_KEY_CONTINENTS, CACH_GEOGRAPHIC_TIME, (Func<List<Continent>>)GetContinentsFromDAL);

            return continents;
        }

        public Country GetCountry(int countryId) {
            // Get all the countries from either database or cache.
            List<Country> countries = Cacher.Get(CACHE_KEY_COUNTRIES, CACH_GEOGRAPHIC_TIME, (Func<List<Country>>)GetCountriesFromDAL);

            // Find the country requested.
            List<Country> countryItem = countries.Where(c => c.Id == countryId).ToList();

            if (countryItem.Count > 0)
                return countryItem[0];
            else
                return null;
        }

        public List<Country> GetCountries()
        {
            List<Country> countries = Cacher.Get(CACHE_KEY_COUNTRIES, CACH_GEOGRAPHIC_TIME, (Func<List<Country>>)GetCountriesFromDAL);

            return countries;
        }

        public List<Country> GetCountries(int? continentId)
        {
            List<Country> countries = GetCountries();
                        
            return continentId.HasValue ? countries.Where(c => c.ContinentId == continentId).ToList() : countries;
        }

        public List<State> GetStates()
        {
            List<State> states = Cacher.Get(CACHE_KEY_STATES, CACH_GEOGRAPHIC_TIME, (Func<List<State>>)GetStatesFromDAL);

            return states;
        }

        public State GetState(int stateId)
        {
            return GetStates().Where(st => st.Id == stateId).FirstOrDefault();
        }

        public List<State> GetStates(int countryId)
        {
            return GetStates().Where(s => s.CountryId == countryId).ToList();
        }

        public List<City> GetCities()
        {
            List<City> cities = Cacher.Get(CACHE_KEY_CITIES, CACH_GEOGRAPHIC_TIME, (Func<List<City>>)GetCitiesFromDAL);

            return cities;
        }

        public City GetCity(int cityId) {
            if (!Cacher.Contains(CACHE_KEY_CITIES)) {
                // Fetch the city from the database and then start a thread to get the rest.
                City city = GetCityFromDAL(cityId);
                GetCitiesViaThread();
                return city;
            }
            else {
                // We have in the cache so fetch from here.
                List<City> cities = Cacher.Get(CACHE_KEY_CITIES, CACH_GEOGRAPHIC_TIME, (Func<List<City>>)GetCitiesFromDAL);
                List<City> cityItem = cities.Where(c => c.Id == cityId).ToList();

                if (cityItem.Count > 0)
                    return cityItem[0];
                else
                    return null;
            }
        }

        public List<City> GetCities(int? continentId, int? countryId, int? stateId, string name) {
            List<City> cities;

            if (!Cacher.Contains(CACHE_KEY_CITIES)) {
                // Fetch the cities from the database and then start a thread to get the rest.
                cities = SearchCitiesFromDAL(continentId, countryId, stateId, name);
                GetCitiesViaThread();
                return cities;
            }
            else {
                // We have in the cache so fetch from here.
                cities = Cacher.Get(CACHE_KEY_CITIES, CACH_GEOGRAPHIC_TIME, (Func<List<City>>)GetCitiesFromDAL);

                return cities.Where(c => (continentId.HasValue ? c.ContinentId == continentId.Value : true) &&
                                         (countryId.HasValue ? c.CountryId == countryId.Value : true) &&
                                         (stateId.HasValue ? c.StateId == stateId.Value : true) &&
                                         (!string.IsNullOrWhiteSpace(name) ? c.Name.IndexOf(name, StringComparison.CurrentCultureIgnoreCase) >= 0 : true)).ToList();
            }
        }

        #region Private methods

        private List<Continent> GetContinentsFromDAL() {
            List<Continent> continents = null;
            DataTable tbl = _DAL.GetContinents();

            continents = (from DataRow row in tbl.Rows
                          select new Continent {
                              Name = row["ContinentName"].ToString(),
                              Id = Convert.ToInt32(row["ContinentId"].ToString()),
                              DataModuleId = Convert.ToInt32(row["DataModuleId"].ToString())
                          }).ToList();

            return continents;
        }

        private List<Country> GetCountriesFromDAL() {
            List<Country> countries = null;
            DataTable tbl = _DAL.GetCountries();

            countries = (from DataRow row in tbl.Rows
                         select new Country {
                             Name = row["CountryName"].ToString(),
                             Id = Convert.ToInt32(row["MediaAtlasCountryId"].ToString()),
                             ContinentId = (DBNull.Value.Equals(row["MediaAtlasContinentId"]) ? (int?)null : Convert.ToInt32(row["MediaAtlasContinentId"])),
                             DataModuleId = Convert.ToInt32(row["DataModuleId"])
                         }).ToList();
            
            return countries;
        }

        private List<State> GetStatesFromDAL() {
            List<State> states = null;
            DataTable tbl = _DAL.GetStates();

            states = (from DataRow row in tbl.Rows
                      select new State {
                          Name = row["StateName"].ToString(),
                          Id = Convert.ToInt32(row["StateId"].ToString()),
                          CountryId = Convert.ToInt32(row["MediaAtlasCountryId"].ToString())
                      }).ToList();

            return states;
        }

        private List<City> GetCitiesFromDAL() {
            DataTable tbl = _DAL.GetCities();

            return PopulateCities(tbl);
        }

        private City GetCityFromDAL(int cityId) {
            DataTable tbl = _DAL.GetCity(cityId);

            if (tbl != null && tbl.Rows.Count > 0) {
                DataRow row = tbl.Rows[0];

                return new City {
                    Name = row["CityName"].ToString(),
                    Id = Convert.ToInt32(row["CityId"].ToString()),
                    StateId = (DBNull.Value.Equals(row["StateId"]) ? (int?)null : Convert.ToInt32(row["StateId"])),
                    CountryId = (DBNull.Value.Equals(row["CountryId"]) ? (int?)null : Convert.ToInt32(row["CountryId"])),
                    ContinentId = (DBNull.Value.Equals(row["ContinentId"]) ? (int?)null : Convert.ToInt32(row["ContinentId"]))
                };
            }
            else
                return null;
        }

        private List<City> SearchCitiesFromDAL(int? continentId, int? countryId, int? stateId, string name) {
            DataTable tbl = _DAL.SearchCities(continentId, countryId, stateId, name);

            return PopulateCities(tbl);
        }

        private static List<City> PopulateCities(DataTable tbl) {
            List<City> cities = null;

            cities = (from DataRow row in tbl.Rows
                      select new City {
                          Name = row["CityName"].ToString(),
                          Id = Convert.ToInt32(row["CityId"].ToString()),
                          StateId = (DBNull.Value.Equals(row["StateId"]) ? (int?)null : Convert.ToInt32(row["StateId"])),
                          CountryId = (DBNull.Value.Equals(row["CountryId"]) ? (int?)null : Convert.ToInt32(row["CountryId"])),
                          ContinentId = (DBNull.Value.Equals(row["ContinentId"]) ? (int?)null : Convert.ToInt32(row["ContinentId"]))
                      }).ToList();

            return cities;
        }

        #endregion

        #region Thread fetching methods

        private void GetCitiesViaThread() {
            try {
                Thread myThread = new Thread(GetCitiesForThread);
                myThread.Priority = ThreadPriority.Lowest;
                myThread.Start();
            }
            catch (Exception ex) {
                BasePage.LogException(ex, "GeographicBAL", "GetCitiesInThread", null);
            }
        }

        private static void GetCitiesForThread() {
            List<City> cities = Cacher.Get(CACHE_KEY_CITIES, CACH_GEOGRAPHIC_TIME, (Func<List<City>>)GetCitiesForThreadFromDAL);
        }

        private static List<City> GetCitiesForThreadFromDAL() {
            try {
                using (DBClass db = new DBClass()) {
                    GeographicDAL threadDAL = new GeographicDAL(db); ;
                    DataTable tbl = threadDAL.GetCities();

                    return PopulateCities(tbl);
                }
            }
            catch (Exception ex) {
                BasePage.LogException(ex, "GeographicBAL", "GetCitiesForThreadFromDAL", null);
                return new List<City>();
            }
        }

        #endregion
    }
}
