﻿using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.Web.Common.DAL;
using System.Data;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common;
using System;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class GroupBAL
    {
        private GroupDAL dal;

        public GroupBAL(DBClass dbConn) {
            dal = new GroupDAL(dbConn);
        }

        #region Read

        /// <summary>
        /// Get a group by ID.
        /// </summary>
        /// <param name="groupId">The group ID.</param>
        /// <returns><c>GroupBase</c> object representing the group. If no group is found will return null.</returns>
        public GroupBase GetGroupById(int groupId, int userId) {
            DataTable dt = dal.GetGroupById(groupId, userId);
            GroupBase g = null;

            if (dt != null && dt.Rows.Count > 0) {
                g = new GroupBase();
                g.Id = (int)dt.Rows[0]["GroupId"];
                g.Name = dt.Rows[0]["GroupName"] == DBNull.Value ? string.Empty : (string)dt.Rows[0]["GroupName"];
            }
            return g;
        }

        public List<GroupBase> GetGroupsByIds(string groupIds, int userId) {
            DataTable dt = dal.GetGroupsByIds(groupIds, userId);
            List<GroupBase> groups = null;

            groups = (from DataRow row in dt.Rows
                      select new GroupBase {
                          Id = (int)row["GroupId"],
                          Name = (string)row["GroupName"]
                      }).ToList<GroupBase>();

            return groups;
        }

        public int Rename(int groupId, string name)
        {
            return dal.Rename(groupId, name);
        }

        #endregion
    }
}