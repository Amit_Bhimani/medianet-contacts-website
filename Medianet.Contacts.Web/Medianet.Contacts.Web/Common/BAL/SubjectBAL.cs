﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for SubjectBAL
    /// </summary>
    public class SubjectBAL
    {
        private const int CACH_SUBJECTS_TIME = 1800; // 30 minutes.
        private static readonly string CACH_SUBJECTS_KEY = "subjects";
        private static readonly string CACH_SUBJECT_GROUPS_KEY = "subject-groups";

        private SubjectDAL _DAL;

        public SubjectBAL(DBClass dbConn)
        {
            _DAL = new SubjectDAL(dbConn);
        }

        public List<Subject> GetSubjects()
        {
            return Cacher.Get(CACH_SUBJECTS_KEY, CACH_SUBJECTS_TIME, (Func<List<Subject>>)GetSubjectsFromDAL);
        }

        public List<SubjectGroup> GetSubjectGroups()
        {
            return Cacher.Get(CACH_SUBJECT_GROUPS_KEY, CACH_SUBJECTS_TIME, (Func<List<SubjectGroup>>)GetGroupsFromDAL);
        }

        public List<Subject> GetSubjectInGroup(SubjectGroup group)
        {
            return group != null && GetSubjectGroups().Exists(g => g.Id == group.Id)
                ? GetSubjectGroups()
                    .Where(g => g.Id == group.Id).FirstOrDefault().Subjects.OrderBy(s => s.Name.ToLower()).ToList<Subject>()
                : new List<Subject>();
        }

        public List<Subject> GetSubjectsInGroups(List<string> groups)
        {
            List<Subject> subjects = new List<Subject>();

            if (groups != null)
            {
                (from g in GetSubjectGroups()
                 where groups.Contains(g.Id.ToString())
                 select g).ToList<SubjectGroup>().ForEach(sg => subjects.AddRange(sg.Subjects));
            }

            return subjects;
        }

        private List<SubjectGroup> GetGroupsFromDAL()
        {
            List<SubjectGroup> groups = new List<SubjectGroup>();
            DataTable tbl = _DAL.GetSubjectsByGroup(null);

            groups = (from DataRow row in tbl.Rows
                      group row by new { gid = row["SubjectGroupId"].ToString(), gname = row["SubjectGroupName"].ToString() } into g
                      let subs = (from DataRow dr in tbl.Rows
                                  where dr["SubjectGroupId"].ToString() == g.Key.gid.ToString()
                                  select new Subject
                                  {
                                      Id = int.Parse(dr["SubjectCodeId"].ToString()),
                                      Name = dr["SubjectName"].ToString()
                                  }).ToList<Subject>()
                      orderby g.Key.gname.ToLower()
                      select new SubjectGroup
                      {
                          Id = int.Parse(g.Key.gid),
                          Name = g.Key.gname,
                          Subjects = subs.OrderBy(sb => sb.Name.ToLower()).ToList<Subject>()

                      }
                      ).ToList<SubjectGroup>();

            return groups;
        }

        private List<Subject> GetSubjectsFromDAL()
        {
            List<Subject> subjects = null;
            DataTable tbl = _DAL.GetSubjects();

            subjects = (from DataRow row in tbl.Rows
                        select new Subject { Name = row["SubjectName"].ToString(), Id = Convert.ToInt32(row["SubjectCodeId"].ToString()) }).ToList();

            return subjects;
        }
    }
}
