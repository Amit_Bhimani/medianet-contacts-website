﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class GeneralBAL
    {
        private DBClass db;
        private GeneralDAL gd;

        public GeneralBAL(DBClass db) {
            this.db = db;
            gd = new GeneralDAL(db);

        }

        /// <summary>
        /// Get all Frequencies for Outlets.
        /// </summary>
        /// <returns>Strongly types list of Outlet Frequencies.</returns>
        public List<OutletFrequency> GetFrequencies() {
            List<OutletFrequency> frequencies = new List<OutletFrequency>();

            try {

                DataTable dt = gd.GetFrequencies();

                if (dt != null && dt.Rows.Count > 0) {
                    frequencies = (from dr in dt.AsEnumerable()
                                   select new OutletFrequency() {
                                       Id = Convert.ToInt32(dr["FrequencyId"]),
                                       Name = Convert.ToString(dr["FrequencyName"])
                                   }).ToList();
                }
            }
            catch (Exception ex) {
                BasePage.LogException(ex, typeof(GeneralBAL).FullName, "GetFrequencies", null);
            }

            return frequencies;
        }

        /// <summary>
        /// Get all Languages.
        /// </summary>
        /// <returns>A list of strongly typed WorkingLanguage.</returns>
        public List<Language> GetLanguages() {
            List<Language> languages = new List<Language>();

            try {
                DataTable dt = gd.GetWorkingLanguage();

                if (dt != null && dt.Rows.Count > 0) {
                    languages = (from dr in dt.AsEnumerable()
                                 select new Language {
                                     Id = Convert.ToInt32(dr["WorkingLanguageId"]),
                                     Name = Convert.ToString(dr["WorkingLanguageName"])
                                 }).ToList();
                }
            }
            catch (Exception ex) {
                BasePage.LogException(ex, typeof(GeneralBAL).Name, "GetLanguages", null);
            }

            return languages;
        }
    }
}