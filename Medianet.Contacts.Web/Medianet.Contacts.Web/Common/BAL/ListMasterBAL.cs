﻿using System.Data;
using Medianet.Contacts.Web.Common.DAL;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class ListMasterBAL
    {
        private const int DEFAULT_MAX_ROWS = 5000;

        private ListMasterDAL _DAL;

        private int MaxRows
        {
            get
            {
                int maxRows;

                if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings["ListsAndTasksMaxRows"], out maxRows))
                    maxRows = DEFAULT_MAX_ROWS;

                return maxRows;
            }
        }

        public ListMasterBAL(DBClass dbConn) {
            _DAL = new ListMasterDAL(dbConn);
        }

        public DataTable GetLlistUpdates() {
            return _DAL.GetLlistUpdates();
        }

        public DataTable GetExportListAll(int listId, int userId) {
            return _DAL.GetExportListAll(listId, userId, MaxRows);
        }

        public int CountListRecords(int listId, int userId)
        {
            DataTable dt = _DAL.CountListRecords(listId, userId);

            if (dt.Rows.Count > 0)
            {
                return (int)dt.Rows[0][0];
            }

            return 0;
        }
    }
}
