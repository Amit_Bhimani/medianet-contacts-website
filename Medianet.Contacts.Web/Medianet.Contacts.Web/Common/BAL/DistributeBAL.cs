﻿using System;
using System.Data;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Common.DAL;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for DistributeBAL
    /// </summary>
    public class DistributeBAL
    {
        private DistributeDAL _DAL;

        public DistributeBAL(DBClass dbConn) {
            _DAL = new DistributeDAL(dbConn);
        }

        public DataSet GetDistribution(int UserID, string debtornumber) {
            return _DAL.GetDistribution(UserID, debtornumber);
        }

        public DataSet GetEmailFax(String ListIDCsv) {
            return _DAL.GetEmailFax(ListIDCsv);
        }

        public int InsertDistributionList(Distribute distribute) {
            return _DAL.InsertDistributionList(distribute);
        }
    }
}
