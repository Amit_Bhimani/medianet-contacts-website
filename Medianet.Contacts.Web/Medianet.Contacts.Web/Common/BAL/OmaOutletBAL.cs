﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Ninject;
using Medianet.Contacts.Web.ProxyWrappers;
using Ninject;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for OmaOutletBAL
    /// </summary>
    public class OmaOutletBAL
    {
        string CACH_LIVING_LIST_KEY = "living-lists";
        // int CACH_LIVING_LIST_TIME = 1800;

        private OmaOutletDAL _DAL;
        private IKernel _kernel;

        public OmaOutletBAL(DBClass dbConn)
        {
            _DAL = new OmaOutletDAL(dbConn);
            _kernel = new StandardKernel(new ServiceProxyModule());
        }

        public PrivateOutletModel GetPrivateOutlet(string outletId, string ownedBy)
        {
            DataTable dt = _DAL.GetPrivateOutlet(Convert.ToInt32(outletId), ownedBy);

            if (dt == null || dt.Rows.Count == 0)
                return null;

            var pc = new PrivateOutletModel();

            DataRow dr = dt.Rows[0];

            pc.OutletId = Convert.ToInt32(outletId);
            pc.CompanyName = DBClass.FormatDatabaseString(dr["CompanyName"], "");

            if (!DBNull.Value.Equals(dr["CityId"])) pc.CityId = Convert.ToInt32(dr["CityId"]);
            if (!DBNull.Value.Equals(dr["StateId"])) pc.StateId = Convert.ToInt32(dr["StateId"]);
            if (!DBNull.Value.Equals(dr["MediaAtlasCountryId"])) pc.MediaAtlasCountryId = Convert.ToInt32(dr["MediaAtlasCountryId"]);

            pc.PhoneNumber = DBClass.FormatDatabaseString(dr["PhoneNumber"], "");
            pc.FaxNumber = DBClass.FormatDatabaseString(dr["FaxNumber"], "");
            pc.Email = DBClass.FormatDatabaseString(dr["EmailAddress"], "");
            pc.AddressLine1 = Convert.ToString(dr["AddressLine1"]);
            pc.AddressLine2 = Convert.ToString(dr["AddressLine2"]);
            pc.PostCode = Convert.ToString(dr["PostCode"]);
            pc.Website = Convert.ToString(dr["WebSite"]);

            pc.Facebook = DBClass.FormatDatabaseString(dr["Facebook"], "");
            pc.LinkedIn = DBClass.FormatDatabaseString(dr["LinkedIn"], "");
            pc.Twitter = DBClass.FormatDatabaseString(dr["Twitter"], "");
            pc.BlogUrl = DBClass.FormatDatabaseString(dr["BlogUrl"], "");
            pc.Notes = DBClass.FormatDatabaseString(dr["Notes"], "");

            pc.Subjects = ParseSubjects(Convert.ToString(dr["SubjectIdsAndNames"]));

            if (!DBNull.Value.Equals(dr["FrequencyId"])) pc.FrequencyId = Convert.ToInt32(dr["FrequencyId"]);
            if (!DBNull.Value.Equals(dr["ProductTypeId"])) pc.ProductTypeId = Convert.ToInt32(dr["ProductTypeId"]);
            pc.WorkingLanguages = ParseWorkingLanguages(Convert.ToString(dr["WorkingLanguageIdsAndNames"]));

            return pc;
        }

        private List<WorkingLanguage> ParseWorkingLanguages(string workingLanguageIdsAndNames)
        {
            var languages = new List<WorkingLanguage>();

            if (!string.IsNullOrEmpty(workingLanguageIdsAndNames))
                workingLanguageIdsAndNames.Split('#').ToList().ForEach(delegate (string token)
                {
                    languages.Add(new WorkingLanguage()
                    {
                        Id = Convert.ToInt32(token.Split(':')[0]),
                        Name = token.Split(':')[1]
                    });
                });

            return languages;
        }

        private List<Subject> ParseSubjects(string subjectsIdsAndNames)
        {
            var subjects = new List<Subject>();

            if (!string.IsNullOrEmpty(subjectsIdsAndNames))
                subjectsIdsAndNames.Split('#').ToList().ForEach(delegate (string token)
                {
                    subjects.Add(new Subject
                    {
                        Id = Convert.ToInt32(token.Split(':')[0]),
                        Name = token.Split(':')[1]
                    });
                });

            return subjects;
        }

        public bool CreatePrivateOutlet(PrivateOutletModel model)
        {
            var returnValue = _DAL.CreatePrivateOutlet(model);

            if (returnValue && model.OutletId > 0)
                UpdateSolr(model.OutletId, new List<int>());
                        
            return true;
        }

        private List<int> GetContactIds(int outletId, string sessionKey)
        {
            List<int> contactIds = new List<int>();

            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                contactIds = mediaContactsService.GetOmaContactIds(outletId, sessionKey);
            }

            return contactIds;
        }

        private void UpdateSolr(int outletId, List<int> contactIds, bool isDeleted = false)
        {
            var ri = new RecordIdentifier(outletId.ToString(), null, RecordType.OmaOutlet);

            using (var proxy = _kernel.Get<SolrServiceProxy>())
            {
                proxy.UpdateContact(ri);

                foreach (int contactId in contactIds)
                {
                    ri = new RecordIdentifier(
                        isDeleted ? null : outletId.ToString(), 
                        contactId.ToString(), 
                        isDeleted ? RecordType.OmaContactNoOutlet : RecordType.OmaContactAtOmaOutlet);

                    proxy.UpdateContact(ri);
                }
            }
        }

        public bool UpdatePrivateOutlet(PrivateOutletModel model, string sessionKey)
        {
            var returnValue = _DAL.UpdatePrivateOutlet(model);

            if (returnValue && model.OutletId > 0)
            {
                List<int> contactIds = GetContactIds(model.OutletId, sessionKey);
                UpdateSolr(model.OutletId, contactIds);
            }

            return true;
        }

        public int DeletePrivateOutlet(string outletId, string ownedBy, int userId, string sessionKey)
        {
            var record = GetPrivateOutlet(outletId, ownedBy);

            List<int> contactIds = GetContactIds(record.OutletId, sessionKey);

            int result = _DAL.DeletePrivateOutlet(Convert.ToInt32(outletId), ownedBy);

            if (result > 0 && record.OutletId > 0)
            {
                // We are changing living lists data so remove the global list from cache.
                Cacher.Remove(CACH_LIVING_LIST_KEY + "-" + userId.ToString());

                UpdateSolr(record.OutletId, contactIds, true);
            }

            return result;
        }
    }
}
