﻿using System.Data;
using Medianet.Contacts.Web.Common.DAL;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class OutletContactMasterBAL
    {
        private OutletContactMastetDAL _DAL;

        public OutletContactMasterBAL(DBClass dbConn) {
            _DAL = new OutletContactMastetDAL(dbConn);
        }

        public DataTable GetAllOutletContactHomePageDAL(string OutletId, string ContactId) {
            return _DAL.GetAllOutletContactHomePageDAL(OutletId, ContactId);
        }
    }
}
