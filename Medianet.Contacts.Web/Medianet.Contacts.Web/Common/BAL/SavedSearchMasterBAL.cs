﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;
using SavedSearch = Medianet.Contacts.Web.Common.Model.SavedSearch;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for SavedSearchMasterBAL
    /// </summary>
    public class SavedSearchMasterBAL
    {
        public const string CONTEXT_MODE_PEOPLE = "people";
        public const string CONTEXT_MODE_OUTLET = "outlet";
        public const string CONTEXT_MODE_BOTH = "both";

        private SavedSearchMasterDAL _DAL;

        public SavedSearchMasterBAL(DBClass dbConn) {
            _DAL = new SavedSearchMasterDAL(dbConn);
        }

        public int Delete(int SearchId) {
            return _DAL.Delete(SearchId);
        }
        public int DeleteGroupSearch(int groupId,int userId)
        {
            return _DAL.DeleteGroupSearch(groupId, userId);
        }
        public DataTable GetSearchList(int groupId, int UserId, string DebtorNumber) {
            return _DAL.GetSearchList(groupId, UserId, DebtorNumber);
        }

        public DataTable GetSavedSearches(int UserId) {
            return _DAL.GetSavedSearches(UserId);
        }

        public List<SavedSearch> GetStrongSavedSearches(int userid) {
            DataTable table = _DAL.GetSavedSearches(userid);
            List<SavedSearch> sOptions = new List<SavedSearch>();

            if (table != null && table.Rows.Count > 0) {
                foreach (DataRow row in table.Rows) {
                    SavedSearch s = new SavedSearch() {
                        Id = (int)row["SearchId"],
                        Name = (string)row["SearchName"],
                        Group = new GroupBase() {
                            Id = (int)row["GroupId"],
                            Name = (string)row["GroupName"]
                        },
                        Criteria = (string)row["SearchCriteria"],
                        Visibility = (string)row["Visibility"] == "Private"
                    };

                    NameValueCollection values = HttpUtility.ParseQueryString(Convert.ToString(row["searchcriteria"]));

                    if (values["QuickSearch"] == "true" && values["AdvancedSearch"] != "true")
                        s.Type = SearchType.Quick;
                    else if (values["OutletSearch"] == "true")
                        s.Type = SearchType.Outlet;
                    else
                        s.Type = SearchType.Advanced;

                    sOptions.Add(s);
                }
            }

            return sOptions;
        }
    }
}
