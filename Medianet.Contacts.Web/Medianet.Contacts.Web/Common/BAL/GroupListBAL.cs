﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Common;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for GroupListBAL
    /// </summary>
    public class GroupListBAL
    {
        private GroupListDAL _DAL;

        public GroupListBAL(DBClass dbConn) {
            _DAL = new GroupListDAL(dbConn);
        }

        /// <summary>
        /// Soft deletes the given group list.
        /// </summary>
        public int DeleteGroupList(int groupid, int userid) {
            return _DAL.DeleteGroupList(groupid, userid);
        }

        public GroupList GetById(int id) {
            return _DAL.GetById(id);
        }

        public int UpdateList(int listId, string listName, bool isPrivate, int userId) {
            return _DAL.UpdateList(listId, listName, isPrivate, userId);
        }

        public DataTable GetListsListDebtorUserId(int UserId, string DebtorNumber) {
            return _DAL.GetListsListDebtorUserId(UserId, DebtorNumber);
        }

        public DataTable GetListsListDebtorUserIdGroupId(int groupId, int userId, string debtorNumber)
        {
            return _DAL.GetListsListDebtorUserIdGroupId(groupId, userId, debtorNumber);
        }

        public int DeleteOnListIdAndUserId(int UserId, int ListId) {
            return _DAL.DeleteOnListIdAndUserId(UserId, ListId);
        }
    }
}
