﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Article = Medianet.Contacts.Web.NewsCentreArticle.Article;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class ContactBAL : OutletContactBALBase
    {
        //private OutletBAL ob;
        private OutletBAL ob;
        public int MaxNumberOfArticleToLoad { get; set; }
        public int PagedArticlesPageSize { get; set; }

        public ContactBAL(DBClass dbConn) : base(dbConn)
        {
            //ob = new OutletBAL(dbConn);
            ob = new OutletBAL(dbConn);
            MaxNumberOfArticleToLoad = 10;
            PagedArticlesPageSize = 10;
        }

        public Contact GetContact(string ContactId, string OutletId, RecordType rt, User LoggedInUser, bool fillArticles)
        {
            Contact ct = null;
            DataSet ds = new DataSet();

            if (rt == RecordType.MediaContact)
                ds = cd.GetMediaContact(ContactId, OutletId, LoggedInUser);
            else if (rt == RecordType.PrnContact)
            {
                long cid, oid;
                if (long.TryParse(ContactId, out cid) && long.TryParse(OutletId, out oid))
                    ds = cd.GetPrnContact(cid, oid, LoggedInUser);
                else
                {
                    BasePage.LogException(new FormatException("Either (PRN) ContactId or OutletId could not be parsed to long. Next step would be to find out why/how the web layer is getting wrong IDs"), typeof(ContactBAL).FullName, "GetContact", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId}, {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                    });

                    return ct;
                }

            }
            else if (rt.IsOmaContact())
            {
                int cid;

                if (int.TryParse(ContactId, out cid))
                    ds = cd.GetOmaContact(cid, OutletId, LoggedInUser);
                else
                {
                    BasePage.LogException(new FormatException("ContactId could not be parsed to int. Private contact ID are of type int. Next step would be to find out why/how the web layer is getting wrong IDs"), typeof(ContactBAL).FullName, "GetContact", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId}, {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                    });

                    return ct;
                }
            }

            try
            {
                ct = DataSetToContact(ds, OutletId, LoggedInUser, rt);
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(ContactBAL).FullName, "GetContact", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId}, {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                });

                return ct;
            }

            if (fillArticles && ct != null)
            {
                if (rt != RecordType.PrnContact)FillArticles(ct);
                else
                    ct.Articles = new PagedList<Article>(0, PagedArticlesPageSize, 0, new List<Article>());
                ct.Type = rt;
            }


            return ct;
        }
        public Contact GetContactPreview(string ContactId, string OutletId, RecordType rt, User LoggedInUser)
        {
            Contact ct = null;
            DataSet ds = new DataSet();

            if (rt == RecordType.MediaContact)
                ds = cd.GetMediaContactPreview(ContactId, OutletId, LoggedInUser);

            try
            {
                ct = DataSetToContactPreview(ds, OutletId, LoggedInUser, rt);
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(ContactBAL).FullName, "GetContactPreview", new System.Collections.Specialized.NameValueCollection() {
                    {"ContactId", ContactId}, {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                });

                return ct;
            }

            return ct;
        }

        private Contact DataSetToContact(DataSet ds, string OutletId, User LoggedInUser, RecordType recordType)
        {
            Contact ct = new Contact();
            //order of tables is stricly...
            //ContactDetails Notes Roles Subjects Lists Tasks Documents Outlets OutletRecordType
            if (ds.Tables.Count != 8 && ds.Tables.Count != 9)
                return null;

            FillContactDetails(ds.Tables[0], ct, recordType);

            ct.Positions = GetRoles(ds.Tables[1]);
            ct.Subjects = GetSubjects(ds.Tables[2]);
            ct.BelongsToLists = GetLists(ds.Tables[3]);
            ct.Outlets = GetOutlets(ds.Tables[6]);

            FillAtOutlet(ds.Tables[7], ct, OutletId, LoggedInUser);

            if (ds.Tables.Count > 8)
                ct.WorkHistory = GetWorkHistory(ds.Tables[8]);

            return ct;
        }

        private Contact DataSetToContactPreview(DataSet ds, string OutletId, User LoggedInUser, RecordType recordType)
        {
            Contact ct = new Contact();
            //order of tables is stricly...
            //ContactDetails Notes Roles Subjects
            if (ds.Tables.Count != 3)
                return null;

            var dtContactDetails = ds.Tables[0];

            if (dtContactDetails != null && dtContactDetails.Rows.Count > 0)
            {
                DataRow dr = dtContactDetails.Rows[0];

                ct.ContactId = Convert.ToString(dr["ContactId"]);
                ct.Mobile = Convert.ToString(dr["MobileNumber"]);
               
                ct.Email = Convert.ToString(dr["EmailAddress"]);

                ct.Name = new ContactName()
                {
                    FirstName = Convert.ToString(dr["FirstName"]),
                    LastName = Convert.ToString(dr["LastName"]),
                };

                ct.JobTitle = Convert.ToString(dr["JobTitle"]);

                var socialDetailsModel = new SocialInfo();

                if (recordType == RecordType.MediaContact)
                {
                    ct.MediaInfluencerScore = dr["MediaInfluencerScore"] is DBNull ? 0 : Convert.ToInt32(dr["MediaInfluencerScore"]);

                    var pitchingProfileModel = new PitchingProfile()
                    {
                        BugBears = Convert.ToString(dr["BugBears"]),
                        AlsoKnownAs = Convert.ToString(dr["AlsoKnownAs"]),
                        PressReleaseInterests = Convert.ToString(dr["PressReleaseInterests"]),
                        Awards = Convert.ToString(dr["Awards"]),
                        PersonalInterests = Convert.ToString(dr["PersonalInterests"]),
                        PoliticalParty = Convert.ToString(dr["PoliticalParty"]),
                        NamePronunciation = Convert.ToString(dr["NamePronunciation"]),
                        AppearsIn = Convert.ToString(dr["AppearsIn"]),
                        CurrentStatus = Convert.ToString(dr["CurrentStatus"]),
                        BasedInLocation = Convert.ToString(dr["BasedInLocation"]),
                        Deadlines = Convert.ToString(dr["Deadlines"]),
                        AuthorOf = Convert.ToString(dr["AuthorOf"]),
                        CoffeeOrder = Convert.ToString(dr["CoffeeOrder"]),
                        BusyTimes = Convert.ToString(dr["BusyTimes"]),
                        GiftsAccepted = Convert.ToString(dr["GiftsAccepted"]) == "" ? "" :
                        Convert.ToString(dr["GiftsAccepted"]) == "True" ? "Yes" : "No",
                        PitchingDos = Convert.ToString(dr["PitchingDos"]),
                        PitchingDonts = Convert.ToString(dr["PitchingDonts"]),

                    };

                    ct.PitchingProfile = AnyStringPropertyValuesAreNonEmpty(pitchingProfileModel) ? pitchingProfileModel : null;
                }

                socialDetailsModel.Facebook = Convert.ToString(dr["Facebook"]);
                socialDetailsModel.Twitter = Convert.ToString(dr["Twitter"]);

                ct.SocialDetails = AnyStringPropertyValuesAreNonEmpty(socialDetailsModel) ? socialDetailsModel : null;
                if (ct.SocialDetails != null)
                {
                    ct.SocialDetails.Name = ct.Name.FirstName + " " + ct.Name.LastName;
                }

                if (dr.ItemArray[dtContactDetails.Columns.IndexOf("DeliveryMethod")] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(dr["DeliveryMethod"])))
                    ct.DeliveryMethod = (PreferredDeliveryMethod)CharEnum.Parse(typeof(PreferredDeliveryMethod), Convert.ToChar(Convert.ToString(dr["DeliveryMethod"]).ToUpper()));
            }

            ct.Subjects = GetSubjects(ds.Tables[1]);

            FillAtOutlet(ds.Tables[2], ct, OutletId, LoggedInUser);

            return ct;
        }

        private void FillContactDetails(DataTable dt, Contact ct, RecordType recordType)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                ct.ContactId = Convert.ToString(dr["ContactId"]);
                ct.Phone = Convert.ToString(dr["PhoneNumber"]);
                ct.Mobile = Convert.ToString(dr["MobileNumber"]);
                ct.Fax = Convert.ToString(dr["FaxNumber"]);
                ct.Email = Convert.ToString(dr["EmailAddress"]);
                ct.Name = new ContactName()
                {
                    FirstName = Convert.ToString(dr["FirstName"]),
                    LastName = Convert.ToString(dr["LastName"]),
                    MiddleName = Convert.ToString(dr["MiddleName"]),
                    Prefix = Convert.ToString(dr["Prefix"])
                };
                ct.JobTitle = Convert.ToString(dr["JobTitle"]);
                ct.ProfileNotes = Convert.ToString(dr["Notes"]).Trim();
                ct.UpdatedBy = dr["LastModifiedBy"] is DBNull ? 0 : Convert.ToInt32(dr["LastModifiedBy"]);
                ct.BlogUrl = Convert.ToString(dr["Blog"]);
                ct.WebUrl = Convert.ToString(dr["Web"]);
                ct.InterviewLink = Convert.ToString(dr["InterviewLink"]);
                ct.InterviewPhotoLink = Convert.ToString(dr["InterviewPhotoLink"]);

                var socialDetailsModel = new SocialInfo();

                if (recordType == RecordType.MediaContact)
                {
                    ct.Bio = Convert.ToString(dr["Bio"]);
                    ct.MediaInfluencerScore = dr["MediaInfluencerScore"] is DBNull
                        ? 0
                        : Convert.ToInt32(dr["MediaInfluencerScore"]);
                    ct.AlternativeEmailAddress = Convert.ToString(dr["AlternativeEmailAddress"]);
                    ct.AdditionalMobileNumber = Convert.ToString(dr["AdditionalMobile"]);
                    ct.AdditionalPhoneNumber = Convert.ToString(dr["AdditionalPhoneNumber"]);
                    ct.OfficeHours = Convert.ToString(dr["OfficeHours"]);

                    var pitchingProfileModel = new PitchingProfile()
                    {
                        BugBears = Convert.ToString(dr["BugBears"]),
                        AlsoKnownAs = Convert.ToString(dr["AlsoKnownAs"]),
                        PressReleaseInterests = Convert.ToString(dr["PressReleaseInterests"]),
                        Awards = Convert.ToString(dr["Awards"]),
                        PersonalInterests = Convert.ToString(dr["PersonalInterests"]),
                        PoliticalParty = Convert.ToString(dr["PoliticalParty"]),
                        NamePronunciation = Convert.ToString(dr["NamePronunciation"]),
                        AppearsIn = Convert.ToString(dr["AppearsIn"]),
                        CurrentStatus = Convert.ToString(dr["CurrentStatus"]),
                        BasedInLocation = Convert.ToString(dr["BasedInLocation"]),
                        Deadlines = Convert.ToString(dr["Deadlines"]),
                        BestContactTime = Convert.ToString(dr["BestContactTime"]),
                        AuthorOf = Convert.ToString(dr["AuthorOf"]),
                        CoffeeOrder = Convert.ToString(dr["CoffeeOrder"]),
                        BusyTimes = Convert.ToString(dr["BusyTimes"]),
                        GiftsAccepted = Convert.ToString(dr["GiftsAccepted"]) == "" ? "":
                        Convert.ToString(dr["GiftsAccepted"]) == "True" ? "Yes" : "No",
                        PitchingDos = Convert.ToString(dr["PitchingDos"]),
                        PitchingDonts = Convert.ToString(dr["PitchingDonts"]),
                    };

                    ct.PitchingProfile = AnyStringPropertyValuesAreNonEmpty(pitchingProfileModel)
                        ? pitchingProfileModel
                        : null;

                    socialDetailsModel.Instagram = Convert.ToString(dr["Instagram"]);
                    socialDetailsModel.SnapChat = Convert.ToString(dr["SnapChat"]);
                    socialDetailsModel.Youtube = Convert.ToString(dr["Youtube"]);
                    socialDetailsModel.Skype = Convert.ToString(dr["Skype"]);
                    
                    ct.CurrentlyOutOfOffice = dr["CurrentlyOutOfOffice"] is DBNull ? false : Convert.ToBoolean(dr["CurrentlyOutOfOffice"]);
                    ct.OutOfOfficeReturnDate = dr["OutOfOfficeReturnDate"] is DBNull ? (DateTime?)null : Convert.ToDateTime(dr["OutOfOfficeReturnDate"]);
                    ct.OutOfOfficeStartDate = dr["OutOfOfficeStartDate"] is DBNull ? (DateTime?)null : Convert.ToDateTime(dr["OutOfOfficeStartDate"]);
                    ct.PriorityNotice = Convert.ToString(dr["PriorityNotice"]);
                }

                socialDetailsModel.Facebook = Convert.ToString(dr["Facebook"]);
                socialDetailsModel.Twitter = Convert.ToString(dr["Twitter"]);
                socialDetailsModel.LinkedIn = Convert.ToString(dr["LinkedIn"]);

                ct.SocialDetails = AnyStringPropertyValuesAreNonEmpty(socialDetailsModel) ? socialDetailsModel : null;
                if (ct.SocialDetails != null)
                {
                    ct.SocialDetails.Name = ct.Name.FirstName + " " + ct.Name.LastName;
                }

                ct.address = new Address()
                {
                    AddressLine1 = Convert.ToString(dr["AddressLine1"]),
                    AddressLine2 = Convert.ToString(dr["AddressLine2"]),
                    City = Convert.ToString(dr["City"]),
                    Country = Convert.ToString(dr["Country"]),
                    PostCode = Convert.ToString(dr["PostCode"]),
                    State = Convert.ToString(dr["State"])
                };

                if (dr.ItemArray[dt.Columns.IndexOf("DeliveryMethod")] != DBNull.Value &&
                    !string.IsNullOrEmpty(Convert.ToString(dr["DeliveryMethod"])))
                    ct.DeliveryMethod =
                        (PreferredDeliveryMethod)
                            CharEnum.Parse(typeof (PreferredDeliveryMethod),
                                Convert.ToChar(Convert.ToString(dr["DeliveryMethod"]).ToUpper()));

                if (recordType == RecordType.MediaContact || recordType == RecordType.PrnContact)
                {
                    if (dr.ItemArray[dt.Columns.IndexOf("PrimaryContact")] != DBNull.Value)
                    {
                        ct.PrimaryContact = (bool) dr["PrimaryContact"];
                    }
                }
            }
        }

        private void FillAtOutlet(DataTable dt, Contact ct, string OutletId, User LoggedInUser)
        {

            if (dt.Rows.Count == 1 && !DBNull.Value.Equals(dt.Rows[0][0]))
            {
                RecordType rt = (RecordType)Enum.Parse(typeof(RecordType), Convert.ToString(dt.Rows[0][0]));
                ct.OutletId = OutletId;
                ct.AtOutlet = ob.GetOutlet(OutletId, rt, LoggedInUser);

            }
        }

        private void FillArticles(Contact ct)
        {
            string contactName = ct.Name.FirstName + " " + ct.Name.MiddleName + " " + ct.Name.LastName;

            NewsCentreArticle.Search ncSearch = new NewsCentreArticle.Search();
            ncSearch.Url = ConfigurationManager.AppSettings["NewsCentreArticleSearchURL"];
            List<Article> allArticles = new List<Article>();

            try
            {
                int custID = int.Parse(ConfigurationManager.AppSettings["NewsCentreCustomerID"]);
                int pageNumber = 1;

                NewsCentreArticle.ArticleResponse objarticle = ncSearch.SearchArticlesByAuthor(custID, "\"" + contactName + "\"", pageNumber++, PagedArticlesPageSize);

                int totalRecord = objarticle.TotalResults > MaxNumberOfArticleToLoad ? MaxNumberOfArticleToLoad : objarticle.TotalResults;
                allArticles.AddRange(objarticle.ArticleManager.ToList());

                ct.Articles = new PagedList<Article>(0, PagedArticlesPageSize, totalRecord, allArticles);
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(ContactBAL).FullName, "FillArticles", new System.Collections.Specialized.NameValueCollection()
                {
                    {"ContactName", contactName}
                });
            }

        }
    }
}