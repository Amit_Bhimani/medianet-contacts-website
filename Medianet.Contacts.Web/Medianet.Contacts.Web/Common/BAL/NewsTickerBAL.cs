﻿using System;
using System.Data;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.DAL;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for GroupListBAL
    /// </summary>
    public class NewsTickerBAL
    {
        private const int CACH_STORY_TIME = 300; // 5 minutes.

        private NewsTickerDAL _DAL;
        private DBClass _DBConnection = null;

        public NewsTickerBAL() {
        }

        public DataTable GetNewsStories() {
            return Cacher.Get("newsstories", CACH_STORY_TIME, (Func<DataTable>)GetNewsStoriesFromDAL);
        }

        private DataTable GetNewsStoriesFromDAL() {
            try {
                _DBConnection = new DBClass(System.Configuration.ConfigurationManager.AppSettings["WebDataServerTickerAlias"]);
                _DAL = new NewsTickerDAL(_DBConnection);

                return _DAL.GetNewsStories();
            }
            catch (Exception ex) {
                throw ex;
            }
            finally {
                if (!(_DBConnection == null)) {
                    _DBConnection.CloseDBConnection();
                    _DBConnection = null;
                }
            }
        }
    }
}
