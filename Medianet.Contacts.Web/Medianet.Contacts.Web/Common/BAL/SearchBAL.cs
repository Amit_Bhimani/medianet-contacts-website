﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.DAL;
using SavedSearch = Medianet.Contacts.Web.Common.Model.SavedSearch;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class SearchBAL
    {
        private const char SEPARATOR = '#';

        private SearchDAL dal;

        public SearchBAL(DBClass dbConn) {
            dal = new SearchDAL(dbConn);
        }

        #region Saved Searches

        /// <summary>
        /// Inserts the given search to the database.
        /// </summary>
        /// <param name="search">The Search BO to insert into the database.</param>
        /// <returns>ID of the inserted object.</returns>
        public int Insert(SavedSearch search) {
            return dal.Insert(search);
        }

        /// <summary>
        /// Update the name and visibility of the given search.
        /// </summary>
        /// <param name="search">The Search BO to insert into the database.</param>
        public void Update(SavedSearch search)
        {
            dal.Update(search);
        }

        /// <summary>
        /// Converts a <c>SearchOption</c> object to a query string.
        /// </summary>
        /// <param name="pSearchOptions">The SearchOption object.</param>
        /// <returns>Query string representation of the SearchOption object.</returns>
        public static string GetSearchCriteria(SearchOptions pSearchOptions) {
            string searchCriteria = "";

            if (pSearchOptions.Type == SearchType.Quick)
            {
                SearchOptionsQuick so = (SearchOptionsQuick)pSearchOptions;
                searchCriteria += "&QuickSearch=true";

                searchCriteria += "&SearchAustralia=" + so.SearchAustralia.ToString();
                searchCriteria += "&SearchNewZealand=" + so.SearchNewZealand.ToString();
                searchCriteria += "&SearchRestOfWorld=" + so.SearchAllCountries.ToString();

                if (so.LocationList != null && so.LocationList.Count > 0)
                    searchCriteria += "&LocationRegionWiseContact=" + SearchOptionsQuick.GetLocation(so.LocationList);

                if (so.PositionList != null && so.PositionList.Count > 0)
                    searchCriteria += "&RoleContact=" + (so.PositionList != null && so.PositionList.Count > 0 ? string.Join(",", so.PositionList) : string.Empty);

                if (so.MediaTypeList != null && so.MediaTypeList.Count > 0)
                    searchCriteria += "&MediaContact=" + (so.MediaTypeList != null && so.MediaTypeList.Count > 0 ? string.Join(",", so.MediaTypeList) : string.Empty);

                if (so.SubjectList != null && so.SubjectList.Count > 0)
                    searchCriteria += "&SubjectContact=" + (so.SubjectList != null && so.SubjectList.Count > 0 ? string.Join(",", so.SubjectList) : string.Empty);

                if (so.SubjectGroupList != null && so.SubjectGroupList.Count > 0)
                    searchCriteria += "&SubjectGroupContact=" + (so.SubjectGroupList != null & so.SubjectGroupList.Count > 0 ? string.Join(",", so.SubjectGroupList) : string.Empty);

                if (!string.IsNullOrEmpty(so.SearchText))
                    searchCriteria += "&ContactContact=" + so.SearchText;

                if (!string.IsNullOrEmpty(so.SelectedId) && so.SelectedRecordType != RecordType.Unknown)
                {
                    searchCriteria += "&SelectedId=" + so.SelectedId;
                    searchCriteria += "&SelectedRecordType=" + so.SelectedRecordType.ToString();
                }
            }
            else if (pSearchOptions.Type == SearchType.Outlet)
            {
                SearchOptionsOutlet so = (SearchOptionsOutlet)pSearchOptions;
                searchCriteria += "&OutletSearch=true";
                searchCriteria += "&OutletId=" + so.OutletID;
                searchCriteria += "&OutletType=" + so.OutletType.ToString();
            }
            else
            {
                SearchOptionsAdvanced so = (SearchOptionsAdvanced)pSearchOptions;

                searchCriteria += "&AdvancedSearch=true";

                // Search Text
                if (!string.IsNullOrEmpty(so.SearchText))
                {
                    searchCriteria += "&SearchText=" + so.SearchText;
                }
                // People region.
                if (!string.IsNullOrEmpty(so.FirstName))
                    searchCriteria += "&ContactFirstName=" + so.FirstName;
                if (!string.IsNullOrEmpty(so.FullName))
                    searchCriteria += "&ContactFullName=" + so.FullName;

                if (!string.IsNullOrEmpty(so.LastName))
                    searchCriteria += "&ContactLastName=" + so.LastName;

                if (!string.IsNullOrEmpty(so.JobTitle))
                    searchCriteria += "&JobTitle=" + so.JobTitle;

                if (so.PositionList != null && so.PositionList.Count > 0)
                    searchCriteria += "&RoleContact=" + (so.PositionList != null && so.PositionList.Count > 0 ? string.Join(",", so.PositionList) : string.Empty);

                if (so.ContactSubjectList != null && so.ContactSubjectList.Count > 0)
                    searchCriteria += "&SubjectContact=" + (so.ContactSubjectList != null && so.ContactSubjectList.Count > 0 ? string.Join(",", so.ContactSubjectList) : string.Empty);

                if (so.ContactSubjectGroupList != null && so.ContactSubjectGroupList.Count > 0)
                    searchCriteria += "&SubjectGroupContact=" + (so.ContactSubjectGroupList != null && so.ContactSubjectGroupList.Count > 0 ? string.Join(",", so.ContactSubjectGroupList) : string.Empty);

                searchCriteria += "&PrimaryNewsContactOnly=" + so.PrimaryNewsContactOnly.ToString();

                if (!string.IsNullOrEmpty(so.ContactInfluencerScore))
                {
                    searchCriteria += "&ContactInfluencerScore=" + so.ContactInfluencerScore;
                }

                // Contact Geographic region.
                if (so.ContactContinentIDList != null && so.ContactContinentIDList.Count > 0)
                    searchCriteria += "&ContinentGeoGraphicContact=" + (so.ContactContinentIDList != null && so.ContactContinentIDList.Count > 0 ? string.Join(",", so.ContactContinentIDList) : string.Empty);

                if (so.ContactCountryIDList != null && so.ContactCountryIDList.Count > 0)
                    searchCriteria += "&CountryGeographicContact=" + (so.ContactCountryIDList != null && so.ContactCountryIDList.Count > 0 ? string.Join(",", so.ContactCountryIDList) : string.Empty);

                if (so.ContactStateIDList != null && so.ContactStateIDList.Count > 0)
                    searchCriteria += "&StateGeographicContact=" + (so.ContactStateIDList != null && so.ContactStateIDList.Count > 0 ? string.Join(",", so.ContactStateIDList) : string.Empty);

                if (so.ContactCityIDList != null && so.ContactCityIDList.Count > 0)
                    searchCriteria += "&CityGeographicContact=" + (so.ContactCityIDList != null && so.ContactCityIDList.Count > 0 ? string.Join(",", so.ContactCityIDList) : string.Empty);

                if (so.ContactPostCodeList != null && so.ContactPostCodeList.Count > 0)
                    searchCriteria += "&PostCodeGeographicContact=" + (so.ContactPostCodeList != null && so.ContactPostCodeList.Count > 0 ? string.Join(",", so.ContactPostCodeList) : string.Empty);

                // Outlet region.
                if (!string.IsNullOrEmpty(so.OutletName))
                    searchCriteria += "&OutletOutlet=" + so.OutletName;

                if (so.MediaTypeList != null && so.MediaTypeList.Count > 0)
                    searchCriteria += "&MediaOutlet=" + (so.MediaTypeList != null && so.MediaTypeList.Count > 0 ? string.Join(",", so.MediaTypeList) : string.Empty);

                if (so.OutletSubjectList != null && so.OutletSubjectList.Count > 0)
                    searchCriteria += "&SubjectOutlet=" + (so.OutletSubjectList != null && so.OutletSubjectList.Count > 0 ? string.Join(",", so.OutletSubjectList) : string.Empty);

                if (so.OutletSubjectGroupList != null && so.OutletSubjectGroupList.Count > 0)
                    searchCriteria += "&SubjectGroupOutlet=" + (so.OutletSubjectGroupList != null && so.OutletSubjectGroupList.Count > 0 ? string.Join(",", so.OutletSubjectGroupList) : string.Empty);

                if (so.OutletFrequencyList != null && so.OutletFrequencyList.Count > 0)
                    searchCriteria += "&FrequencyOutlet=" + (so.OutletFrequencyList != null && so.OutletFrequencyList.Count > 0 ? string.Join(",", so.OutletFrequencyList) : string.Empty);

                if (so.OutletCirculationMin > 0)
                    searchCriteria += "&CirculationMinOutlet=" + so.OutletCirculationMin.ToString();

                if (so.OutletCirculationMax > 0)
                    searchCriteria += "&CirculationMaxOutlet=" + so.OutletCirculationMax.ToString();

                if (so.OutletLanguageList != null && so.OutletLanguageList.Count > 0)
                    searchCriteria += "&LanguageOutlet=" + (so.OutletLanguageList != null && so.OutletLanguageList.Count > 0 ? string.Join(",", so.OutletLanguageList) : string.Empty);

                if (so.OutletStationFrequencyList != null && so.OutletStationFrequencyList.Count > 0)
                    searchCriteria += "&StationFrequencyOutlet=" + (so.OutletStationFrequencyList != null & so.OutletStationFrequencyList.Count > 0 ? string.Join(",", so.OutletStationFrequencyList) : string.Empty);

                // Outlet Geographic region.
                if (so.OutletContinentIDList != null && so.OutletContinentIDList.Count > 0)
                    searchCriteria += "&ContinentGeoGraphic=" + (so.OutletContinentIDList != null && so.OutletContinentIDList.Count > 0 ? string.Join(",", so.OutletContinentIDList) : string.Empty);

                if (so.OutletCountryIDList != null && so.OutletCountryIDList.Count > 0)
                    searchCriteria += "&CountryGeographic=" + (so.OutletCountryIDList != null && so.OutletCountryIDList.Count > 0 ? string.Join(",", so.OutletCountryIDList) : string.Empty);

                if (so.OutletStateIDList != null && so.OutletStateIDList.Count > 0)
                    searchCriteria += "&StateGeographic=" + (so.OutletStateIDList != null && so.OutletStateIDList.Count > 0 ? string.Join(",", so.OutletStateIDList) : string.Empty);

                if (so.OutletCityIDList != null && so.OutletCityIDList.Count > 0)
                    searchCriteria += "&CityGeographicOutlet=" + (so.OutletCityIDList != null && so.OutletCityIDList.Count > 0 ? string.Join(",", so.OutletCityIDList) : string.Empty);

                if (so.OutletPostCodeList != null && so.OutletPostCodeList.Count > 0)
                    searchCriteria += "&PostCodeGeographicOutlet=" + (so.OutletPostCodeList != null && so.OutletPostCodeList.Count > 0 ? string.Join(",", so.OutletPostCodeList) : string.Empty);

                searchCriteria += "&IncludeMetroOutlets=" + so.IncludeMetroOutlets.ToString();
                searchCriteria += "&IncludeRegionalOutlets=" + so.IncludeRegionalOutlets.ToString();
                searchCriteria += "&IncludeSuburbanOutlets=" + so.IncludeSuburbanOutlets.ToString();

                // Notes region.
                if (!string.IsNullOrEmpty(so.SystemNotes))
                    searchCriteria += "&SystemNotes=" + so.SystemNotes;

                if (!string.IsNullOrEmpty(so.MyNotes))
                    searchCriteria += "&MyNotes=" + so.MyNotes;

                // Communication Preferences region.
                searchCriteria += "&EmailChecked=" + so.PrefersEmail.ToString();
                searchCriteria += "&FaxChecked=" + so.PrefersFax.ToString();
                searchCriteria += "&MailChecked=" + so.PrefersMail.ToString();
                searchCriteria += "&PrefersPhoneChecked=" + so.PrefersPhone.ToString();
                searchCriteria += "&PrefersMobileChecked=" + so.PrefersMobile.ToString();
                searchCriteria += "&PrefersTwitterChecked=" + so.PrefersTwitter.ToString();
                searchCriteria += "&PrefersLinkedInChecked=" + so.PrefersLinkedIn.ToString();
                searchCriteria += "&PrefersFacebookChecked=" + so.PrefersFacebook.ToString();
                searchCriteria += "&PrefersInstagramChecked=" + so.PrefersInstagram.ToString();

                // Show only results with section.
                searchCriteria += "&EmailAddressChecked=" + so.ShowOnlyWithEmail.ToString();
                searchCriteria += "&FaxNumberChecked=" + so.ShowOnlyWithFax.ToString();
                searchCriteria += "&PostalAddressChecked=" + so.ShowOnlyWithPostalAddress.ToString();
                searchCriteria += "&TwitterChecked=" + so.ShowOnlyWithTwitter.ToString();
                searchCriteria += "&LinkedinChecked=" + so.ShowOnlyWithLinkedIn.ToString();
                searchCriteria += "&FacebookChecked=" + so.ShowOnlyWithFacebook.ToString();
                searchCriteria += "&InstagramChecked=" + so.ShowOnlyWithInstagram.ToString();
                searchCriteria += "&YouTubeChecked=" + so.ShowOnlyWithYouTube.ToString();
                searchCriteria += "&SnapchatChecked=" + so.ShowOnlyWithSnapchat.ToString();

                // Lists section.
                if (so.ListList != null && so.ListList.Count > 0)
                    searchCriteria += "&WithinListId=" + (so.ListList != null && so.ListList.Count > 0 ? string.Join(",", so.ListList) : string.Empty);

                // Exclusion section.
                if (so.RecordsToShow == SearchOptionsAdvanced.SHOW_RECORDS_MEDIA_DIR)
                    searchCriteria += "&MediaDirectoryRecords=true";
                else if (so.RecordsToShow == SearchOptionsAdvanced.SHOW_RECORDS_PRIVATE)
                    searchCriteria += "&PrivateRecords=true";
                else
                    searchCriteria += "&AllRecordsChecked=true";

                if (!string.IsNullOrEmpty(so.ExcludeKeywords))
                    searchCriteria += "&ExcludeKeyword=" + so.ExcludeKeywords;

                searchCriteria += "&ExcludeOPS=" + so.ExcludeOPS.ToString();
                searchCriteria += "&ExcludeFinance=" + so.ExcludeFinanceInst.ToString();
                searchCriteria += "&ExcludePoliticians=" + so.ExcludePoliticians.ToString();
                searchCriteria += "&ExcludeOutOfOffice=" + so.ExcludeOutOfOffice.ToString();
            }
            return searchCriteria;
        }

        #endregion
    }
}
