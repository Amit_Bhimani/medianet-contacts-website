﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;
using ReminderType = Medianet.Contacts.Web.Common.Model.ReminderType;
using Medianet.Contacts.Web.Helpers;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class OutletContactBALBase
    {
        protected ContactDAL cd;
        protected OutletDAL od;

        public OutletContactBALBase()
        { }

        public OutletContactBALBase(DBClass dbConn)
        {
            cd = new ContactDAL(dbConn);
            od = new OutletDAL(dbConn);
        }

        public ICollection<OutletContactBase> GetRecentlyAddedItems()
        {
            List<OutletContactBase> updates = new List<OutletContactBase>();

            try
            {
                DataSet ds = cd.GetRecentlyAdded();

                if (ds != null && ds.Tables.Count > 0)
                {
                    updates.AddRange(GetRecentlyAddedContacts(ds.Tables[0]));
                }

                ds = od.GetRecentlyAdded();

                if (ds != null && ds.Tables.Count > 0)
                {
                    updates.AddRange(GetRecentlyAddedOutlets(ds.Tables[0]));
                }
            }
            catch (Exception e)
            {
                BasePage.LogException(e, typeof(OutletContactBALBase).FullName, "GetRecentlyAddedItems", null);
            }

            return updates;
        }
        private List<OutletContactBase> GetRecentlyAddedOutlets(DataTable dt)
        {
            List<OutletContactBase> updates = new List<OutletContactBase>();

            if (dt != null && dt.Rows.Count > 0)
            {
                updates = (from dr in dt.AsEnumerable()
                           select new OutletBase()
                           {
                               ContactId = string.Empty,
                               OutletId = Convert.ToString(dr["OutletId"]).Trim(),
                               Type = (RecordType)Enum.Parse(typeof(RecordType), Convert.ToString(Convert.ToInt32(dr["RecordType"]))),
                               Name = Convert.ToString(dr["Name"]),
                               LogoFileName = Convert.ToString(dr["LogoFileName"]),
                               MediaType = new MediaType() { Id = Convert.ToInt32(dr["ProductTypeId"]), Name = Convert.ToString(dr["ProductTypeName"]) },
                               CreatedDate = Convert.ToDateTime(dr["Added"])
                           }
                           ).ToList().Cast<OutletContactBase>().ToList();
            }

            return updates;
        }

        private List<OutletContactBase> GetRecentlyAddedContacts(DataTable dt)
        {
            List<OutletContactBase> updates = new List<OutletContactBase>();

            if (dt != null && dt.Rows.Count > 0)
            {
                updates = (from dr in dt.AsEnumerable()
                           select new ContactBase()
                           {
                               ContactId = Convert.ToString(dr["ContactId"]).Trim(),
                               OutletId = Convert.ToString(dr["OutletId"]).Trim(),
                               Type = (RecordType)Enum.Parse(typeof(RecordType), Convert.ToString(Convert.ToInt32(dr["RecordType"]))),
                               Name = new ContactName()
                               {
                                   FirstName = Convert.ToString(dr["FirstName"]),
                                   LastName = Convert.ToString(dr["LastName"]),
                                   MiddleName = Convert.ToString(dr["MiddleName"]),
                                   Prefix = Convert.ToString(dr["Prefix"])
                               },
                               JobTitle =  Convert.ToString(dr["JobTitle"]),
                               SocialDetails = new SocialInfo()
                               {
                                   Twitter = Convert.ToString(dr["Twitter"])
                               },
                               CompanyName = Convert.ToString(dr["CompanyName"]),
                               CreatedDate = Convert.ToDateTime(dr["Added"])
                           }
                           ).ToList().Cast<OutletContactBase>().ToList();
            }

            return updates;
        }
        protected List<Position> GetRoles(DataTable dt)
        {
            List<Position> roles = new List<Position>();

            if (dt != null && dt.Rows.Count > 0)
            {
                roles = (from dr in dt.AsEnumerable()
                         select new Position()
                         {
                             Id = Convert.ToInt32(dr["id"]),
                             Name = Convert.ToString(dr["Name"])
                         }).ToList();
            }

            return roles;
        }

        protected List<Language> GetLanguages(DataTable dt)
        {
            List<Language> langs = new List<Language>();

            if (dt != null && dt.Rows.Count > 0)
            {
                langs = (from dr in dt.AsEnumerable()
                         select new Language()
                         {
                             Id = Convert.ToInt32(dr["id"]),
                             IsPreferred = !(dr.ItemArray[dt.Columns.IndexOf("IsPreferred")] == DBNull.Value) && Convert.ToBoolean(dr["IsPreferred"]),
                             Name = Convert.ToString(dr["Name"])
                         }).ToList();
            }

            return langs;
        }

        protected List<Subject> GetSubjects(DataTable dt)
        {
            List<Subject> subjects = new List<Subject>();

            if (dt != null && dt.Rows.Count > 0)
            {
                subjects = (from dr in dt.AsEnumerable()
                            select new Subject()
                            {
                                Id = Convert.ToInt32(dr["id"]),
                                Name = Convert.ToString(dr["Name"])
                            }).ToList();
            }

            return subjects;
        }

        protected List<ListBase> GetLists(DataTable dt)
        {
            List<ListBase> lists = new List<ListBase>();

            if (dt != null && dt.Rows.Count > 0)
            {
                lists = (from dr in dt.AsEnumerable()
                         select new ListBase()
                         {
                             Id = Convert.ToInt32(dr["id"]),
                             Name = Convert.ToString(dr["Name"]),
                             Status = Convert.ToBoolean(dr["ListStatus"]),
                             IsPrivate = Convert.ToBoolean(dr["IsPrivate"]),
                             UpdatedDate = Convert.ToDateTime(dr["UpdateDate"]),
                             UserId = Convert.ToInt32(dr["UserId"]),
                             BelongsToGroup = new GroupBase()
                             {
                                 Id = Convert.ToInt32(dr["GroupId"]),
                                 Name = Convert.ToString(dr["GroupName"]),
                                 Status = Convert.ToBoolean(dr["GroupStatus"]),
                                 Type = GroupType.List
                             }
                         }).ToList();
            }

            return lists;
        }
        
        protected List<ContactBase> GetContacts(DataTable dt)
        {
            List<ContactBase> contacts = new List<ContactBase>();

            if (dt != null && dt.Rows.Count > 0)
            {
                contacts = (from dr in dt.AsEnumerable()
                            select new ContactBase()
                            {
                                ContactId = Convert.ToString(dr["ContactId"]),
                                OutletId = Convert.ToString(dr["OutletId"]),
                                JobTitle = Convert.ToString(dr["JobTitle"]),
                                Type = (RecordType)Enum.Parse(typeof(RecordType), Convert.ToString(Convert.ToInt32(dr["ContactRecordType"]))),
                                Name = new ContactName()
                                {
                                    FirstName = Convert.ToString(dr["FirstName"]),
                                    LastName = Convert.ToString(dr["LastName"]),
                                    MiddleName = Convert.ToString(dr["MiddleName"]),
                                    Prefix = Convert.ToString(dr["Prefix"])
                                },
                                SocialDetails = new Model.SocialInfo
                                {
                                    Twitter = Convert.ToString(dr["TwitterHandle"])
                                },
                                IsPrimaryNewsContact = Convert.ToBoolean(dr["IsPrimaryNewsContact"])
                            }).ToList();
            }

            return contacts;
        }

        protected List<OutletBase> GetOutlets(DataTable dt)
        {
            List<OutletBase> outlets = new List<OutletBase>();

            if (dt != null && dt.Rows.Count > 0)
            {
                outlets = (from dr in dt.AsEnumerable()
                           select new OutletBase()
                           {
                               OutletId = Convert.ToString(dr["OutletId"]),
                               ContactId = Convert.ToString(dr["ContactId"]),
                               Name = Convert.ToString(dr["Name"]),
                               Type = (RecordType)Enum.Parse(typeof(RecordType), Convert.ToString(Convert.ToInt32(dr["OutletRecordType"]))),
                               IsGeneric = dr["IsGeneric"] is DBNull? false : Convert.ToBoolean(dr["IsGeneric"])
                           }).ToList();
            }

            return outlets;
        }

        protected List<WorkHistory> GetWorkHistory(DataTable dt)
        {
            List<WorkHistory> history = new List<WorkHistory>();

            if (dt != null && dt.Rows.Count > 0)
            {
                history = (from dr in dt.AsEnumerable()
                           select new WorkHistory()
                           {
                               ContactId = Convert.ToString(dr["ContactId"]),
                               JobTitle = Convert.ToString(dr["JobTitle"]),
                               StartYear = (DBNull.Value.Equals(dr["StartYear"]) ? (int?)null : Convert.ToInt32(dr["StartYear"])),
                               StartMonth = (DBNull.Value.Equals(dr["StartMonth"]) ? (int?)null : Convert.ToInt32(dr["StartMonth"])),
                               EndYear = (DBNull.Value.Equals(dr["EndYear"]) ? (int?)null : Convert.ToInt32(dr["EndYear"])),
                               EndMonth = (DBNull.Value.Equals(dr["EndMonth"]) ? (int?)null : Convert.ToInt32(dr["EndMonth"])),
                               OutletId = Convert.ToString(dr["OutletId"]),
                               CompanyName = Convert.ToString(dr["CompanyName"]),
                               LogoFileName = Convert.ToString(dr["LogoFileName"]),
                               Location = Convert.ToString(dr["Location"])
                           }).ToList();
            }

            return history.OrderByDescending(h => h.StartYear).ThenByDescending(h => h.StartMonth ?? 1).ToList();
        }

        protected static bool AnyStringPropertyValuesAreNonEmpty(object myObject)
        {
            var allStringPropertyValues =
                from property in myObject.GetType().GetProperties()
                where property.PropertyType == typeof(string) && property.CanRead
                select (string)property.GetValue(myObject);

            return allStringPropertyValues.Any(value => value.HasValue());
        }
    }
}