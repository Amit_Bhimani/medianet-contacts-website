﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for RoleBAL
    /// </summary>
    public class MediaTypeBAL
    {
        private const int CACH_MEDIA_TYPE_TIME = 1800; // 30 minutes.

        private MediaTypeDAL _DAL;

        public MediaTypeBAL(DBClass dbConn)
        {
            _DAL = new MediaTypeDAL(dbConn);
        }

        public List<MediaType> GetMediaTypes()
        {
            return Cacher.Get("media-types", CACH_MEDIA_TYPE_TIME, (Func<List<MediaType>>)GetMediaTypesFromDAL);
        }

        private List<MediaType> GetMediaTypesFromDAL()
        {
            List<MediaType> mediaTypes = null;
            DataTable tbl = _DAL.GetMediaTypes();

            mediaTypes = (from DataRow row in tbl.Rows
                          select new MediaType { Name = row["ProductTypeName"].ToString().Trim(), Id = Convert.ToInt32(row["ProductTypeId"].ToString()) }).ToList();

            return mediaTypes;
        }
    }
}