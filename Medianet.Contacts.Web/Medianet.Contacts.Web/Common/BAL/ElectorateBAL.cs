﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.DAL;
using LocationModel = Medianet.Contacts.Web.Common.Model.Location;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for ElectorateBAL
    /// </summary>
    public class ElectorateBAL
    {
        private ElectorateDAL _DAL;

        public ElectorateBAL(DBClass dbConn) {
            _DAL = new ElectorateDAL(dbConn);
        }

        public List<LocationModel> GetLocations() {
            DataTable dt = _DAL.GetStateName();
            List<string> regions = new List<string> { "Metro", "Regional", "Suburban" };

            return (from dr in dt.AsEnumerable()
                    from r in regions
                    select new LocationModel {
                        Id = dr["statename"].ToString() + "/" + r,
                        Name = r,
                        ParentId = dr["statename"].ToString()
                    }).ToList().Union(from r in dt.AsEnumerable()
                                      select new LocationModel {
                                          Id = r["statename"].ToString(),
                                          Name = r["statename"].ToString(),
                                          ParentId = null
                                      }).OrderBy(l => l.Id).ToList();
        }

    }
}
