﻿using System;
using System.Data;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using System.Configuration;

namespace Medianet.Contacts.Web.Common.BAL
{
    public class OutletBAL : OutletContactBALBase
    {
        public OutletBAL(DBClass dbConn) : base(dbConn)
        {
        }

        public OutletDetails GetOutlet(string OutletId, RecordType rt, User LoggedInUser)
        {
            OutletDetails ot = null;
            DataSet ds = new DataSet();
            short contactCount = short.Parse(ConfigurationManager.AppSettings["OutletContactCount"] ?? "10");

            switch (rt)
            {
                case RecordType.MediaOutlet:
                    ds = od.GetMediaOutlet(OutletId, LoggedInUser, contactCount);
                    break;

                case RecordType.PrnOutlet:
                    long oid;
                    if (long.TryParse(OutletId, out oid))
                        ds = od.GetPrnOutlet(oid, LoggedInUser, contactCount);
                    else
                    {
                        BasePage.LogException(new FormatException("Either (PRN) ContactId or OutletId could not be parsed to long. Next step would be to find out why/how the web layer is getting wrong IDs"),
                            typeof(OutletBAL).FullName, "GetOutlet", new System.Collections.Specialized.NameValueCollection() {
                            {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                        });

                        return ot;
                    }
                    break;

                case RecordType.OmaOutlet:
                    int id;

                    if (int.TryParse(OutletId, out id))
                        ds = od.GetOmaOutlet(id, LoggedInUser, contactCount);
                    else
                    {
                        BasePage.LogException(new FormatException("OutletId could not be parsed to int. Private contact ID are of type int. Next step would be to find out why/how the web layer is getting wrong IDs"),
                            typeof(OutletBAL).FullName, "GetOutlet", new System.Collections.Specialized.NameValueCollection() {
                            {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                        });

                        return ot;
                    }
                    break;
            }

            try
            {
                ot = DataSetToOutlet(ds, rt);
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(OutletBAL).FullName, "GetOutlet", new System.Collections.Specialized.NameValueCollection() {
                    {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                });

                return ot;
            }

            if (ot != null)
                ot.Type = rt;

            return ot;
        }
        public OutletDetails GetOutletPreview(string OutletId, RecordType rt, User LoggedInUser)
        {
            OutletDetails ot = null;
            DataSet ds = new DataSet();

            ds = od.GetMediaOutletPreview(OutletId, LoggedInUser);

            try
            {
                ot = DataSetToOutletPreview(ds, rt);
            }
            catch (Exception ex)
            {
                BasePage.LogException(ex, typeof(OutletBAL).FullName, "GetOutletPreview", new System.Collections.Specialized.NameValueCollection() {
                    {"OutletId", OutletId}, {"RecordType", rt.ToString()}, {"LoggedInUser", LoggedInUser.ToString()}
                });

                return ot;
            }

            if (ot != null)
                ot.Type = rt;

            return ot;
        }

        private OutletDetails DataSetToOutlet(DataSet ds, RecordType recordType)
        {
            OutletDetails ot = new OutletDetails();
            //order of tables expected in the DataSet...
            //OutletDetails Subjects Lists Tasks Documents Contacts Languages
            if (ds.Tables.Count != 7)
                return null;

            FillOutletDetails(ds.Tables[0], ot, recordType);

            if (string.IsNullOrEmpty(ot.OutletId))
                return null;

            ot.Subjects = GetSubjects(ds.Tables[1]);
            ot.BelongsToLists = GetLists(ds.Tables[2]);
            ot.Contacts = GetContacts(ds.Tables[5]);
            ot.Languages = GetLanguages(ds.Tables[6]);

            return ot;
        }

        private OutletDetails DataSetToOutletPreview(DataSet ds, RecordType recordType)
        {
            OutletDetails ot = new OutletDetails();

            if (ds.Tables.Count != 2)
                return null;
            var dt = ds.Tables[0];

            DataRow dr = dt.Rows[0];

            ot.OutletId = Convert.ToString(dr["OutletId"]);
            ot.Name = Convert.ToString(dr["CompanyName"]);

            ot.Circulation = Convert.ToString(dr["Circulation"]);

            string audience = dr["Audience"].ToString();
            ot.Audience = string.IsNullOrEmpty(audience) ? (int?)null : int.Parse(audience);

            ot.Mobile = Convert.ToString(dr["MobileNumber"]);
            ot.Email = Convert.ToString(dr["EmailAddress"]);
            ot.Deadlines = Convert.ToString(dr["Deadlines"]);
            ot.LogoFileName = Convert.ToString(dr["LogoFileName"]);
            ot.CopyPrice = Convert.ToString(dr["CopyPrice"]);
            ot.YearEstablished = Convert.ToString(dr["YearEstablished"]);
            ot.NewsFocusName = Convert.ToString(dr["NewsFocusName"]);
            ot.CallLetters = Convert.ToString(dr["CallLetters"]);
            ot.NamePronunciation = Convert.ToString(dr["NamePronunciation"]);
            ot.AlsoKnownAs = Convert.ToString(dr["AlsoKnownAs"]);
            ot.BroadcastTime = Convert.ToString(dr["BroadcastTime"]);
            ot.PressReleaseInterests = Convert.ToString(dr["PressReleaseInterests"]);
            ot.RegionsCovered = Convert.ToString(dr["RegionsCovered"]);

            var socialDetailsModel = new SocialInfo();

            socialDetailsModel.Facebook = Convert.ToString(dr["Facebook"]);
            socialDetailsModel.Twitter = Convert.ToString(dr["Twitter"]);
           
            string ABs = Convert.ToString(dr["ABs"]);
            string F16Plus = Convert.ToString(dr["F16"]);
            string GBs = Convert.ToString(dr["GBs"]);
            string M16Plus = Convert.ToString(dr["M16"]);
            string P16Plus = Convert.ToString(dr["P16"]);

            if (!string.IsNullOrEmpty(ABs) || !string.IsNullOrEmpty(F16Plus) || !string.IsNullOrEmpty(GBs) || !string.IsNullOrEmpty(M16Plus) || !string.IsNullOrEmpty(P16Plus))
                ot.OutletDemographics = new Demographics()
                {
                    ABs = ABs,
                    F16Plus = F16Plus,
                    GBs = GBs,
                    M16Plus = M16Plus,
                    P16Plus = P16Plus
                };

            if (dr.ItemArray[dt.Columns.IndexOf("MediaTypeId")] != DBNull.Value)
            {
                ot.MediaType = new MediaType()
                {
                    Id = Convert.ToInt32(dr["MediaTypeId"]),
                    Name = Convert.ToString(dr["MediaTypeName"]),
                    Category = Convert.ToString(dr["MediaTypeCategory"])
                };
            }

            ot.address = new Address()
            {
                AddressLine1 = Convert.ToString(dr["AddressLine1"]),
                AddressLine2 = Convert.ToString(dr["AddressLine2"]),
                City = Convert.ToString(dr["City"]),
                Country = Convert.ToString(dr["Country"]),
                PostCode = Convert.ToString(dr["PostCode"]),
                State = Convert.ToString(dr["State"])
            };

            if (string.IsNullOrEmpty(ot.OutletId))
                return null;

            ot.Subjects = GetSubjects(ds.Tables[1]);

            return ot;
        }

        private void FillOutletDetails(DataTable dt, OutletDetails ot, RecordType recordType)
        {
            if (dt == null || dt.Rows.Count == 0)
                return;

            DataRow dr = dt.Rows[0];

            ot.OutletId = Convert.ToString(dr["OutletId"]);
            ot.Name = Convert.ToString(dr["CompanyName"]);
            ot.ProfileNotes = Convert.ToString(dr["Notes"]).Trim();

            string updatedBy = Convert.ToString(dr["LastModifiedBy"]);
            ot.UpdatedBy = string.IsNullOrEmpty(updatedBy) ? (int?)null : int.Parse(updatedBy);

            ot.Circulation = Convert.ToString(dr["Circulation"]);

            string audience = dr["Audience"].ToString();
            ot.Audience = string.IsNullOrEmpty(audience) ? (int?)null : int.Parse(audience);

            ot.Phone = Convert.ToString(dr["PhoneNumber"]);
            ot.Mobile = Convert.ToString(dr["MobileNumber"]);
            ot.Fax = Convert.ToString(dr["FaxNumber"]);
            ot.Email = Convert.ToString(dr["EmailAddress"]);
            ot.BlogUrl = Convert.ToString(dr["Blog"]);
            ot.WebUrl = Convert.ToString(dr["Web"]);


            if (recordType == RecordType.MediaOutlet)
            {
                ot.Bio = Convert.ToString(dr["BIO"]);
                ot.PublishedOn = Convert.ToString(dr["PublishedOn"]);
                ot.OfficeHours = Convert.ToString(dr["OfficeHours"]);
                ot.Mobile = Convert.ToString(dr["MobileNo"]);
                ot.Deadlines = Convert.ToString(dr["Deadlines"]);
                ot.LogoFileName = Convert.ToString(dr["LogoFileName"]);
                ot.CopyPrice = Convert.ToString(dr["CopyPrice"]);

                string readership = dr["Readership"].ToString();
                ot.Readership = string.IsNullOrEmpty(readership) ? (int?)null : int.Parse(readership);

                ot.IsGeneric = dr["IsGeneric"] is DBNull ? false : Convert.ToBoolean(dr["IsGeneric"]);

                ot.MediaInfluencerScore = !dr.Table.Columns.Contains("MediaInfluencerScore") || dr["MediaInfluencerScore"] is DBNull ?
                                     0 : Convert.ToInt32(dr["MediaInfluencerScore"]);

                ot.YearEstablished = Convert.ToString(dr["YearEstablished"]);
                ot.NewsFocusName = Convert.ToString(dr["NewsFocusName"]);
                ot.CallLetters = Convert.ToString(dr["CallLetters"]);
                ot.TrackingReference = Convert.ToString(dr["TrackingReference"]);
                ot.NamePronunciation = Convert.ToString(dr["NamePronunciation"]);
                ot.SubscriptionOnlyPublication = dr["SubscriptionOnlyPublication"] is DBNull ? false : Convert.ToBoolean(dr["SubscriptionOnlyPublication"]);
                ot.AlsoKnownAs = Convert.ToString(dr["AlsoKnownAs"]);
                ot.AlternativeEmailAddress = Convert.ToString(dr["AlternativeEmailAddress"]);
                ot.AdditionalPhoneNumber = Convert.ToString(dr["AdditionalPhoneNumber"]);
                ot.AdditionalWebsite = Convert.ToString(dr["AdditionalWebsite"]);
                ot.BroadcastTime = Convert.ToString(dr["BroadcastTime"]);
                ot.PressReleaseInterests = Convert.ToString(dr["PressReleaseInterests"]);
                ot.RegionsCovered = Convert.ToString(dr["RegionsCovered"]);

                ot.PostalAddress = new Address()
                {
                    AddressLine1 = Convert.ToString(dr["PostalAddressLine1"]),
                    AddressLine2 = Convert.ToString(dr["PostalAddressLine2"]),
                    City = Convert.ToString(dr["PostalCity"]),
                    Country = Convert.ToString(dr["PostalCountry"]),
                    PostCode = Convert.ToString(dr["PostalPostcode"]),
                    State = Convert.ToString(dr["PostalState"])
                };

                if (string.IsNullOrEmpty(ot.PostalAddress.AddressLine1) && string.IsNullOrEmpty(ot.PostalAddress.AddressLine2) &&
                    string.IsNullOrEmpty(ot.PostalAddress.City) && string.IsNullOrEmpty(ot.PostalAddress.Country) &&
                    string.IsNullOrEmpty(ot.PostalAddress.PostCode) && string.IsNullOrEmpty(ot.PostalAddress.State))
                    ot.PostalAddress = null;

                ot.PriorityNotice = Convert.ToString(dr["PriorityNotice"]);
            }

            if (dr.ItemArray[dt.Columns.IndexOf("FrequencyId")] != DBNull.Value)
            {
                ot.Frequency = new OutletFrequency()
                {
                    Id = Convert.ToInt32(dr["FrequencyId"]),
                    Name = Convert.ToString(dr["FrequencyName"])
                };
            }

            if ((recordType == RecordType.MediaOutlet || recordType == RecordType.PrnOutlet) && dr.ItemArray[dt.Columns.IndexOf("ParentId")] != DBNull.Value)
            {
                ot.ParentOutlet = new OutletDetails()
                {
                    OutletId = Convert.ToString(dr["ParentId"]),
                    Name = Convert.ToString(dr["ParentName"]),
                    LogoFileName = Convert.ToString(dr["ParentLogoFileName"]),
                    IsActive = Convert.ToBoolean(dr["ParentIsActive"])
                };
            }

            var socialDetailsModel = new SocialInfo();

            if (recordType == RecordType.MediaOutlet)
            {
                socialDetailsModel.Instagram = Convert.ToString(dr["Instagram"]);
                socialDetailsModel.SnapChat = Convert.ToString(dr["SnapChat"]);
                socialDetailsModel.Youtube = Convert.ToString(dr["Youtube"]);
                socialDetailsModel.Skype = Convert.ToString(dr["Skype"]);
            }

            socialDetailsModel.Facebook = Convert.ToString(dr["Facebook"]);
            socialDetailsModel.Twitter = Convert.ToString(dr["Twitter"]);
            socialDetailsModel.LinkedIn = Convert.ToString(dr["LinkedIn"]);

            ot.SocialDetails = AnyStringPropertyValuesAreNonEmpty(socialDetailsModel) ? socialDetailsModel : null;
            if (ot.SocialDetails != null)
            {
                ot.SocialDetails.Name = ot.Name;
            }

            ot.address = new Address()
            {
                AddressLine1 = Convert.ToString(dr["AddressLine1"]),
                AddressLine2 = Convert.ToString(dr["AddressLine2"]),
                City = Convert.ToString(dr["City"]),
                Country = Convert.ToString(dr["Country"]),
                PostCode = Convert.ToString(dr["PostCode"]),
                State = Convert.ToString(dr["State"])
            };
            if (string.IsNullOrEmpty(ot.address.AddressLine1) && string.IsNullOrEmpty(ot.address.AddressLine2) &&
                 string.IsNullOrEmpty(ot.address.City) && string.IsNullOrEmpty(ot.address.Country) &&
                 string.IsNullOrEmpty(ot.address.PostCode) && string.IsNullOrEmpty(ot.address.State))
                ot.PostalAddress = null;

            string ABs = Convert.ToString(dr["ABs"]);
            string F16Plus = Convert.ToString(dr["F16"]);
            string GBs = Convert.ToString(dr["GBs"]);
            string M16Plus = Convert.ToString(dr["M16"]);
            string P16Plus = Convert.ToString(dr["P16"]);

            if (!string.IsNullOrEmpty(ABs) || !string.IsNullOrEmpty(F16Plus) || !string.IsNullOrEmpty(GBs) || !string.IsNullOrEmpty(M16Plus) || !string.IsNullOrEmpty(P16Plus))
                ot.OutletDemographics = new Demographics()
                {
                    ABs = ABs,
                    F16Plus = F16Plus,
                    GBs = GBs,
                    M16Plus = M16Plus,
                    P16Plus = P16Plus
                };

            if (dr.ItemArray[dt.Columns.IndexOf("MediaTypeId")] != DBNull.Value)
            {
                ot.MediaType = new MediaType()
                {
                    Id = Convert.ToInt32(dr["MediaTypeId"]),
                    Name = Convert.ToString(dr["MediaTypeName"]),
                    Category = Convert.ToString(dr["MediaTypeCategory"])
                };
            }

            if (dr.ItemArray[dt.Columns.IndexOf("DeliveryMethod")] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(dr["DeliveryMethod"])))
                ot.DeliveryMethod = (PreferredDeliveryMethod)CharEnum.Parse(typeof(PreferredDeliveryMethod), Convert.ToChar(Convert.ToString(dr["DeliveryMethod"])));
        }
    }
}