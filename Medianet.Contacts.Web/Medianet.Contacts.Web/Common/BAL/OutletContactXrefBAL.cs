﻿using System.Data;
using Medianet.Contacts.Web.Common.DAL;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for OutletContactXrefBAL
    /// </summary>
    public class OutletContactXrefBAL
    {
        private OutletContactXrefDAL _DAL;

        public OutletContactXrefBAL(DBClass dbConn) {
            _DAL = new OutletContactXrefDAL(dbConn);
        }
        public DataTable GetOutlet() {
            return _DAL.GetOutlet();
        }
        public DataTable GetContacts() {
            return _DAL.GetContacts();
        }
    }
}
