﻿#region UserMasterBAL.cs
/// <summary>
/// Author : Exa Web Solutions, PS
/// Description : Contain User Details Functions 
/// </summary>
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for UserMasterBAL
    /// </summary>
    public class UserBAL
    {
        private UserMasterDAL _DAL;

        public UserBAL(DBClass dbConn) {
            _DAL = new UserMasterDAL(dbConn);
        }

        public User GetStrongUser(int id) {
            DataSet ds = _DAL.GetUserDataSet(id);

            User user = new User();
            if (ds != null && ds.Tables.Count == 2 && ds.Tables[0].Rows.Count > 0){
                //if (ds != null && ds.Tables.Count == 2 && ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0) {
                DataRow row = ds.Tables[0].Rows[0];

                if (row["Id"] != DBNull.Value)
                    user.UserID = Convert.ToInt32(row["Id"]);

                if (row["OutletId"] != DBNull.Value)
                    user.OutletId = Convert.ToString(row["OutletId"]).Trim();

                if (row["DebtorNumber"] != DBNull.Value)
                    user.DebtorNumber = Convert.ToString(row["DebtorNumber"]).Trim();

                if (row["EmailAddress"] != DBNull.Value)
                    user.EmailAddress = Convert.ToString(row["EmailAddress"]);

                if (row["FaxNumber"] != DBNull.Value)
                    user.FaxNumber = Convert.ToString(row["FaxNumber"]);

                if (row["FullName"] != DBNull.Value && row["LastName"] != DBNull.Value)
                    user.FullName = string.Format("{0} {1}", Convert.ToString(row["FullName"]), Convert.ToString(row["LastName"]));
                else if (row["FullName"] != DBNull.Value)
                    user.FullName = Convert.ToString(row["FullName"]);
                else if (row["LastName"] != DBNull.Value)
                    user.FullName = Convert.ToString(row["LastName"]);

                if (row["LogonName"] != DBNull.Value)
                    user.LogonName = Convert.ToString(row["LogonName"]);

                if (row["OMAAccessRights"] != DBNull.Value)
                    user.OMAAccessRights = Convert.ToChar(row["OMAAccessRights"]);

                if (row["OMAAccess"] != DBNull.Value)
                    user.OMAAccess = Convert.ToBoolean(row["OMAAccess"]);

                user.DataModules = new List<int>();

                foreach (DataRow dr in ds.Tables[1].Rows) {
                    user.DataModules.Add(int.Parse(dr["DataModuleId"].ToString()));
                }
            }
            return user;
        }

        /// <summary>
        /// Return a list of users that belong to this company.
        /// </summary>
        /// <param name="DebtorNumber">The company ID.</param>
        /// <returns>A list of UserMaster objects.</returns>
        public List<User> GetStrongUsersByDebtorId(string DebtorNumber) {
            DataTable dtUsers = null;
            try {
                dtUsers = _DAL.GetUsersOnDebtorId(DebtorNumber);
            }
            catch {
                return null;
            }

            List<User> users = new List<User>();

            foreach (DataRow row in dtUsers.Rows) {
                User user = new User();

                user.UserID = (int)row["Id"];
                user.FullName = (string)row["FullName"];
                user.DebtorNumber = (string)row["DebtorNumber"];
                users.Add(user);
            }
            return users;
        }
    }
}
