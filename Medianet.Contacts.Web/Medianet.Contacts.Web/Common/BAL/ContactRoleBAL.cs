﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for ContactRoleBAL
    /// </summary>
    public class ContactRoleBAL
    {
        private ContactRoleDAL _DAL;

        public ContactRoleBAL(DBClass dbConn) {
            _DAL = new ContactRoleDAL(dbConn);
        }

        public List<Position> GetRoles() {
            List<Position> roles = null;

            DataTable tbl = _DAL.GetAllContactRole();

            roles = (from DataRow row in tbl.Rows
                     select new Position { Name = row["RoleName"].ToString(), Id = Convert.ToInt32(row["RoleId"].ToString()) }).ToList();

            return roles;

        }
    }
}
