﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Ninject;
using Medianet.Contacts.Web.ProxyWrappers;
using Ninject;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for OmaContactBAL
    /// </summary>
    public class OmaContactBAL
    {
        string CACH_LIVING_LIST_KEY = "living-lists";
        //int CACH_LIVING_LIST_TIME = 1800;

        private OmaContactDAL _DAL;
        private IKernel _kernel;

        public OmaContactBAL(DBClass dbConn)
        {
            _DAL = new OmaContactDAL(dbConn);
            _kernel = new StandardKernel(new ServiceProxyModule());
        }

        public int DeletePrivateContact(string ContactId, string OwnerDebornumber, int userId)
        {
            PrivateContactModel record = GetPrivateContact(ContactId, OwnerDebornumber);
            int result = _DAL.DeletePrivateContact(Convert.ToInt32(ContactId), OwnerDebornumber);

            if (result > 0 && record.ContactId.HasValue)
            {
                // We are changing living lists data so remove the global list from cache.
                Cacher.Remove(CACH_LIVING_LIST_KEY + "-" + userId.ToString());

                UpdateSolr(record);
            }

            return result;
        }

        public PrivateContactModel GetPrivateContact(string ContactId, string OwnerDebornuber)
        {
            DataTable dt = _DAL.GetPrivateContact(Convert.ToInt32(ContactId), OwnerDebornuber);

            if (dt == null || dt.Rows.Count == 0)
                return null;

            DataRow dr = dt.Rows[0];

            var pc = new PrivateContactModel();

            pc.ContactId = Convert.ToInt32(ContactId);
            pc.Prefix = Convert.ToString(dr["Prefix"]);
            pc.FirstName = Convert.ToString(dr["FirstName"]);
            pc.MiddleName = Convert.ToString(dr["MiddleName"]);
            pc.LastName = Convert.ToString(dr["LastName"]);
            pc.JobTitle = Convert.ToString(dr["JobTitle"]);
            pc.AddressLine1 = Convert.ToString(dr["AddressLine1"]);
            pc.AddressLine2 = Convert.ToString(dr["AddressLine2"]);

            if (!DBNull.Value.Equals(dr["CityId"])) pc.CityId = Convert.ToInt32(dr["CityId"]);
            pc.PostCode = Convert.ToString(dr["PostCode"]);
            if (!DBNull.Value.Equals(dr["StateId"])) pc.StateId = Convert.ToInt32(dr["StateId"]);
            if (!DBNull.Value.Equals(dr["MediaAtlasCountryId"])) pc.CountryId = Convert.ToInt32(dr["MediaAtlasCountryId"]);

            pc.PhoneNumber = DBClass.FormatDatabaseString(dr["PhoneNumber"], "");
            pc.FaxNumber = DBClass.FormatDatabaseString(dr["FaxNumber"], "");
            pc.MobileNumber = DBClass.FormatDatabaseString(dr["MobileNumber"], "");
            pc.EmailAddress = DBClass.FormatDatabaseString(dr["EmailAddress"], "");

            if (!DBNull.Value.Equals(dr["PreferredDeliveryMethod"])) pc.PreferredDeliveryMethod = Convert.ToChar(dr["PreferredDeliveryMethod"].ToString().Trim());

            pc.Facebook = DBClass.FormatDatabaseString(dr["Facebook"], "");
            pc.LinkedIn = DBClass.FormatDatabaseString(dr["LinkedIn"], "");
            pc.Twitter = DBClass.FormatDatabaseString(dr["Twitter"], "");
            pc.BlogUrl = DBClass.FormatDatabaseString(dr["BlogUrl"], "");
            pc.Notes = DBClass.FormatDatabaseString(dr["Notes"], "");

            if (!DBNull.Value.Equals(dr["OmaOutletId"]))
                pc.PrivateOutletId = Convert.ToInt32(dr["OmaOutletId"]);

            pc.MediaOutletId = DBClass.FormatDatabaseString(dr["MediaOutletId"], "");

            if (!DBNull.Value.Equals(dr["PrnOutletId"]))
                pc.PrnOutletId = Convert.ToInt32(dr["PrnOutletId"]);

            pc.OutletName = pc.PrivateOutletId.HasValue ? DBClass.FormatDatabaseString(dr["OmaCompanyName"], "") :
               pc.PrnOutletId.HasValue ? DBClass.FormatDatabaseString(dr["PrnCompanyName"], "") : DBClass.FormatDatabaseString(dr["MediaCompanyName"], "");

            pc.Subjects = ParseSubjects(Convert.ToString(dr["SubjectIdsAndNames"]));
            pc.Roles = ParseRoles(Convert.ToString(dr["RoleIdsAndNames"]));

            return pc;
        }

        public bool UpdatePrivateContact(PrivateContactModel model)
        {
            var returnValue = _DAL.UpdatePrivateContact(model);

            if (returnValue && model.ContactId.HasValue)
                UpdateSolr(model);

            return returnValue;

        }

        public bool CreatePrivateContact(PrivateContactModel model)
        {
            var returnValue = _DAL.CreatePrivateContact(model);

            if (returnValue && model.ContactId.HasValue)
                UpdateSolr(model);

            return returnValue;
        }

        private void UpdateSolr(PrivateContactModel model)
        {
            var ri = model.GetRecordIdentifier();

            string outletId = null;
            RecordType recordType = RecordType.Unknown;

            if (!string.IsNullOrEmpty(model.MediaOutletId))
            {
                outletId = model.MediaOutletId;
                recordType = RecordType.MediaOutlet;
            }
            else if (model.PrnOutletId.HasValue)
            {
                outletId = model.PrnOutletId.Value.ToString();
                recordType = RecordType.PrnOutlet;
            }
            else if (model.PrivateOutletId.HasValue)
            {
                outletId = model.PrivateOutletId.ToString();
                recordType = RecordType.OmaOutlet;
            }

            using (var proxy = _kernel.Get<SolrServiceProxy>())
            {
                proxy.UpdateContact(ri);

                if (!string.IsNullOrEmpty(outletId))
                {
                    ri = new RecordIdentifier(outletId, null, recordType);
                    proxy.UpdateContact(ri);
                }
            }
        }

        private List<Position> ParseRoles(string roleIdsAndNames)
        {
            List<Position> roles = new List<Position>();

            if (!string.IsNullOrEmpty(roleIdsAndNames))
            {
                roleIdsAndNames.Split('#').ToList().ForEach(delegate (string token)
                {
                    roles.Add(new Position
                    {
                        Id = Convert.ToInt32(token.Split(':')[0]),
                        Name = token.Split(':')[1]
                    });
                });
            }

            return roles;
        }

        private List<Subject> ParseSubjects(string subjectsIdsAndNames)
        {
            List<Subject> subjects = new List<Subject>();

            if (!string.IsNullOrEmpty(subjectsIdsAndNames))
                subjectsIdsAndNames.Split('#').ToList().ForEach(delegate (string token)
                {
                    subjects.Add(new Subject
                    {
                        Id = Convert.ToInt32(token.Split(':')[0]),
                        Name = token.Split(':')[1]
                    });
                });

            return subjects;
        }
    }
}