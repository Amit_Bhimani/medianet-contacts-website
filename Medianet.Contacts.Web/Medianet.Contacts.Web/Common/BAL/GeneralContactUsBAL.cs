﻿using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Common.DAL;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for GeneralContactUsBAL
    /// </summary>
    public class GeneralContactUsBAL
    {
        private GeneralContactUsDAL _DAL;

        public GeneralContactUsBAL(DBClass dbConn) {
            _DAL = new GeneralContactUsDAL(dbConn);
        }
        public int Insert(GeneralContactUs newContact) {
            return _DAL.Insert(newContact);
        }
    }
}
