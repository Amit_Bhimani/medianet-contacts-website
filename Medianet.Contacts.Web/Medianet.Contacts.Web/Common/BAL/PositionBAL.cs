﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.DAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.BAL
{
    /// <summary>
    /// Summary description for RoleBAL
    /// </summary>
    public class PositionBAL
    {
        private const int CACH_POSITION_TIME = 1800; // 30 minutes.

        private PositionDAL _DAL;

        public PositionBAL(DBClass dbConn)
        {
            _DAL = new PositionDAL(dbConn);
        }

        public List<Position> GetPositions()
        {
            return Cacher.Get("positions", CACH_POSITION_TIME, (Func<List<Position>>)GetPositionsFromDAL);
        }

        private List<Position> GetPositionsFromDAL()
        {
            List<Position> positions = null;
            DataTable tbl = _DAL.GetPositions();

            positions = (from DataRow row in tbl.Rows
                     select new Position { Name = row["RoleName"].ToString().Trim(), Id = Convert.ToInt32(row["RoleId"].ToString()) }).ToList();

            return positions;
        }
    }
}