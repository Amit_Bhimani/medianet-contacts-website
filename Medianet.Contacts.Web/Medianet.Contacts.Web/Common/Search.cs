﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using Medianet.Contacts.Web.Common.Model;

/// <summary>
/// Summary description for Search
/// </summary>
namespace Medianet.Contacts.Web.Common
{
    public class SearchUtil
    {
        private DBClass _DBConnection;

        public SearchUtil(DBClass dbConn)
        {
            _DBConnection = dbConn;
        }

        public DataTable GetOutletsSuggestions(string text, string ownedBy)
        {
            OleDbParameter[] parameter = { new OleDbParameter("@OutletName", text), 
                                         new OleDbParameter("@OwnedBy", ownedBy)};
            DataTable tbl = _DBConnection.SelectQuery("oma_outlet_find", parameter, true);
            return tbl;
        }
        
        /// <summary>
        /// This method return the list of working languages.
        /// </summary>
        /// <returns>List<WorkingLanguage></returns>    
        public List<WorkingLanguage> GetWorkingLanguage()
        {
            try
            {
                DataTable dt =  _DBConnection.SelectQuery("e_mn_working_language", true);

                return (from DataRow row in dt.Rows
                        select new WorkingLanguage { Name = row["WorkingLanguageName"].ToString(), Id = Convert.ToInt32(row["WorkingLanguageId"].ToString()) }).ToList();
            }
            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method return the list of Media.
        /// </summary>   
        /// <returns>List<MediaType></returns> 
        public List<MediaType> GetMedia()
        {
            try
            {
                DataTable tbl = _DBConnection.SelectQuery("e_mn_product_type", true);

                return (from DataRow row in tbl.Rows
                        select new MediaType { Name = row["ProductTypeName"].ToString(), Id = Convert.ToInt32(row["ProductTypeId"].ToString()) }).ToList();
            }
            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is returns the Frequency
        /// </summary>    
        /// <returns>List<Frequency></returns>
        public List<Frequency> GetFrequency()
        {
            try
            {
                DataTable tbl = _DBConnection.SelectQuery("e_mn_outlet_frequency", true);

                return (from DataRow row in tbl.Rows
                        select new Frequency { Name = row["FrequencyName"].ToString(), Id = Convert.ToInt32(row["FrequencyId"].ToString()) }).ToList();
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}


