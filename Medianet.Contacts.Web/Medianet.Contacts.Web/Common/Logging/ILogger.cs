﻿using System;
using System.Diagnostics;

namespace Medianet.Contacts.Web.Common.Logging
{
    /// <summary>
    /// Common interface of an arbitrary implementation that provides
    /// logging capabilities.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Logs an informational message (<see cref="TraceEventType.Information"/>).
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        void Log(string message);

        /// <summary>
        /// Logs an exception with a logging level of
        /// <see cref="TraceEventType.Error"/>.
        /// </summary>
        /// <param name="exception">Exception to be logged.</param>
        void Log(Exception exception);

        /// <summary>
        /// Logs an error with an additional message with a logging level of
        /// <see cref="TraceEventType.Error"/>.
        /// </summary>
        /// <param name="message">Additional information regarding the
        /// logged exception.</param>
        /// <param name="exception">Exception to be logged.</param>
        void Log(string message, Exception exception);

        /// <summary>
        /// Logs a message with a given logging severity.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="severity">Logging category.</param>
        void Log(string message, TraceEventType severity);

        /// <summary>
        /// Logs an exception with a given logging severity and an
        /// additional message.
        /// </summary>
        /// <param name="message">Additional information regarding the
        /// logged exception.</param>
        /// <param name="exception">Exception to be logged.</param>
        /// <param name="severity">Logging category.</param>
        void Log(string message, Exception exception, TraceEventType severity);

        /// <summary>
        /// Creates a new log entry based on a given log item.
        /// </summary>
        /// <param name="item">Encaspulates logging information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="item"/>
        /// is a null reference.</exception>
        void Log(LogItem item);
    }
}
