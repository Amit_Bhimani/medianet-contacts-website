﻿using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Medianet.Contacts.Web.Common.Logging
{
    /// <summary>
    /// An implementation of the <see cref="ILogger"/>
    /// interface which outputs logged data using
    /// the <see cref="Logger"/> of the MS Enterprise
    /// Library.
    /// </summary>
    public class EnterpriseLibraryLogger : LoggerBase
    {
        public const string GeneralCategory = "General";

        /// <summary>
        /// Writes a log entry to the Enterprise Library's
        /// logging block. Output depends on the logging
        /// block's configuration.
        /// </summary>
        /// <param name="item">A <see cref="LogItem"/> which encapsulates
        /// information to be logged.</param>
        public override void Log(LogItem item) {
            LogEntry entry = ConvertLogItem(item);
            Logger.Write(entry);
        }

        /// <summary>
        /// Writes a single message to Enterprise Library's logging block.
        /// </summary>
        /// <param name="message">The message to log.</param>
        public override void Log(string message) {
            LogEntry entry = new LogEntry();
            entry.Message = message;
            entry.Severity = TraceEventType.Information;
            entry.Categories.Add(GeneralCategory);

            Logger.Write(entry);
        }

        /// <summary>
        /// Creates a <c>LogEntry</c> instance which can be processed
        /// by the Enterprise Library based on a <see cref="LogItem"/>.
        /// </summary>
        /// <param name="item">A <see cref="LogItem"/> which encapsulates information
        /// to be logged.</param>
        /// <returns>An Enterprise Library item which corresponds
        /// to the submitted <c>LogItem</c>.</returns>
        private static LogEntry ConvertLogItem(LogItem item) {
            // Assign properties
            LogEntry entry = new LogEntry();
            entry.Message = item.Message;
            entry.Title = item.Title;
            entry.AddErrorMessage(item.ErrorMessage);
            entry.EventId = item.EventId;
            entry.Priority = item.Priority;
            entry.Severity = item.Severity;
            entry.TimeStamp = item.TimeStamp;

            foreach (string category in item.Categories) {
                item.Categories.Add(category);
            }
            return entry;
        }
    }
}