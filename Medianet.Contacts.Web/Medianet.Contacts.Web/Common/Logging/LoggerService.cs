﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediaPeople.Common.Logging
{
    /// <summary>
    /// Provides a global repository for a given <see cref="ILogger"/>
    /// instance. This class ensures that the <see cref="Logger"/>
    /// property is never null - in case no logger is defined, it
    /// automatically installs a <see cref="NullLogger"/>
    /// instance.
    /// </summary>
    public static class LoggerService
    {
        private static ILogger logger = new NullLogger();

        /// <summary>
        /// Gets the installed <see cref="ILogger"/> implementation.
        /// </summary>
        /// <remarks>This property always returns a valid
        /// logger.</remarks>
        public static ILogger Logger {
            get { return logger; }
        }

        /// <summary>
        /// Installs a given logger or resets the <see cref="Logger"/>
        /// to a <see cref="NullLogger"/> instance if the
        /// <paramref name="loggerImplementation"/> is a null
        /// reference.
        /// </summary>
        /// <param name="loggerImplementation">The logger to be
        /// used globally, or a null reference in order to reset
        /// the service.</param>
        public static void SetLogger(ILogger loggerImplementation) {
            logger = loggerImplementation ?? new NullLogger();
        }
    }
}