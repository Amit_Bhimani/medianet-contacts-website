using System;

namespace MediaPeople.Common.Logging
{
    /// <summary>
    /// A very simple implementation of <see cref="ILogger"/>
    /// that outputs all messages to the system console.
    /// </summary>
    public class ConsoleLogger : LoggerBase
    {
        /// <summary>
        /// Logs a given item to the console.
        /// </summary>
        /// <param name="item">The item to be logged.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="item"/>
        /// is a null reference.</exception>
        public override void Log(LogItem item) {
            if (item == null) throw new ArgumentNullException("item");
            Console.Out.WriteLine(item.ToLogMessage());
        }
    }
}