﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Medianet.Contacts.Web.Common.Logging
{
    /// <summary>
    /// Encapsulates logging data that can be submitted to any
    /// given <see cref="ILogger"/> implementation.
    /// </summary>
    public class LogItem : ICloneable
    {
        #region Properties

        /// <summary>
        /// A summarizing title for the logged entry. Defaults to
        /// <c>String.Empty</c>.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The logged message body. Defaults to
        /// <c>String.Empty</c>.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// An optional error message, which is appended
        /// to the <see cref="Message"/> body. Defaults to
        /// <c>String.Empty</c>.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Category name which is used to group logged entries, or
        /// to log to different channels (implementation is up to
        /// the logging framework).
        /// </summary>
        public List<string> Categories { get; private set; }

        /// <summary>
        /// Importance of the logged entry. This value can be used for informational purpose
        /// only, or to filter messages depending on their priority (implementation is up
        /// to the logging framework).
        /// <para>
        /// Defaults to -1.
        /// </para>
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Event number or identifier. Defaults to 0.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Log entry severity as a <see cref="Severity"/> enumeration.
        /// (Unspecified, Information, Warning or Error).
        /// Defaults to <see cref="System.Diagnostics.TraceEventType.Information"/>.
        /// </summary>
        public TraceEventType Severity { get; set; }

        /// <summary>
        /// Date and time of the log entry. If no explicietly
        /// set, this property is provides the timestamp of
        /// the object's creation.
        /// </summary>
        public DateTime TimeStamp { get; set; }

        #endregion

        /// <summary>
        /// Inits an new <see cref="LogItem"/> instance which
        /// is initialized with default values.
        /// </summary>
        public LogItem() {
            Title = String.Empty;
            Message = String.Empty;
            ErrorMessage = String.Empty;
            Severity = TraceEventType.Information;
            Priority = -1;

            TimeStamp = DateTime.Now;
            Categories = new List<string>();
        }

        #region ICloneable Members

        /// <summary>
        /// Creates a new <see cref="LogItem"/> that is a copy of the current instance.
        /// </summary>
        /// <implements>ICloneable.Clone</implements>
        /// <returns>A new <c>LogItem</c> that is a copy of the current instance.</returns>
        public object Clone() {
            LogItem clone = new LogItem();

            clone.Title = Title;
            clone.Message = Message;
            clone.EventId = EventId;
            clone.Severity = Severity;
            clone.Priority = Priority;
            clone.TimeStamp = TimeStamp;

            // Copy categories
            clone.Categories.AddRange(Categories);

            return clone;
        }

        #endregion

        /// <summary>
        /// Generates a formatted string-representation of the item.
        /// </summary>
        /// <returns>Formatted log message.</returns>
        public string ToLogMessage() {
            StringBuilder builder = new StringBuilder();

            // Init entry
            builder.AppendLine();

            int titleLength = Math.Max((Title ?? "").Length, 20);
            builder.AppendLine("".PadRight(titleLength, '*'));

            if (!String.IsNullOrEmpty(Title)) {
                builder.AppendLine(Title);
            }

            // Write timestamp
            builder.AppendLine(TimeStamp.ToString("G"));


            // Write categories, if any
            if (Categories.Count > 0) {
                for (int i = 0; i < Categories.Count; i++) {
                    string cat = Categories[i];

                    builder.Append(cat);
                    if (i < Categories.Count - 1) builder.Append(", ");
                }

                builder.AppendLine();
            }

            // Write logging severity
            builder.AppendLine(String.Format("Severity: {0}\n", Severity));

            // Write IDs
            builder.AppendLine(String.Format("Event ID = {0}", EventId));
            builder.AppendLine(String.Format("Priority = {0}", Priority));

            // Write message
            builder.AppendLine(Message);

            // Write error message, if any
            if (!String.IsNullOrEmpty(ErrorMessage)) {
                builder.AppendLine("\n--\n");
                builder.AppendLine(ErrorMessage);
            }

            // Close entry
            builder.AppendLine("".PadRight(titleLength, '*'));
            builder.AppendLine("");

            return builder.ToString();
        }
    }
}