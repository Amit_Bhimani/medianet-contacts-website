﻿using System;
using System.Diagnostics;

namespace MediaPeople.Common.Logging
{
    /// <summary>
    /// Logger implementation that writes trace messages.
    /// </summary>
    public class TraceLogger : LoggerBase
    {
        /// <summary>
        /// Creates a new log entry based on a given log item.
        /// </summary>
        /// <param name="item">Encaspulates logging information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="item"/>
        /// is a null reference.</exception>
        public override void Log(LogItem item) {
            if (item == null) throw new ArgumentNullException("item");

            Trace.WriteLine(item.ToLogMessage());
        }
    }
}