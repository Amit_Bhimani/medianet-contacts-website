﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Logging
{
    public class MPLogger
    {
        private const int TitleLength = 80;

        private static EnterpriseLibraryLogger _logger;
        private static EnterpriseLibraryLogger Logger {
            get {
                if (_logger == null)
                    _logger = new EnterpriseLibraryLogger();
                return _logger;
            }
        }

        public static void LogMessage(string message)
        {
            Logger.Log(message);
        }

        public static void LogException(Exception ex) {
            if (ex == null)
                return;

            StringBuilder sb = new StringBuilder();
            sb.Append("==================== Request ========================" + Environment.NewLine);
            sb.Append(GetRequestDetails(HttpContext.Current.Request));

            sb.Append("==================== Request Params =================" + Environment.NewLine);
            sb.Append(GetRequestParams(HttpContext.Current.Request));

            sb.Append("==================== Exception ======================" + Environment.NewLine);
            sb.Append(GetException(ex));

            Logger.Log(sb.ToString());
        }

        /// <summary>
        /// Logs a normal exception from an exception context.
        /// </summary>
        /// <param name="ex">The exception context.</param>
        public static void LogException(ExceptionContext ex) {
            if (ex == null)
                return;

            StringBuilder sb = new StringBuilder();
            sb.Append("==================== Request ========================" + Environment.NewLine);
            sb.Append(GetRequestDetails(ex.RequestContext.HttpContext.Request));

            sb.Append("==================== Request Params =================" + Environment.NewLine);
            sb.Append(GetRequestParams(ex.RequestContext.HttpContext.Request));

            sb.Append("==================== Browser ========================" + Environment.NewLine);
            sb.Append(GetBrowserDetails(ex.RequestContext.HttpContext.Request.Browser));

            sb.Append("==================== ViewData =======================" + Environment.NewLine);
            sb.Append(GetViewData(ex.Controller.ViewData));

            sb.Append("==================== Exception ======================" + Environment.NewLine);
            sb.Append(GetException(ex.Exception));

            Logger.Log(sb.ToString());
        }

        private static string GetException(Exception ex) {
            StringBuilder sb = new StringBuilder();

            if (ex != null) {
                sb.Append("Exception Type Name: " + ex.GetType().Name + Environment.NewLine);

                if (ex.GetType() == typeof(FaultException<ServiceFault>)) {
                    FaultException<ServiceFault> sfe = (object) ex as FaultException<ServiceFault>;
                    ServiceFault sf = sfe.Detail;

                    sb.Append("Fault Exception Guid: " + sf.Id);
                    sb.Append("Fault Exception MessageText: " + sf.MessageText);
                    if (sf.Data.Count > 0) {
                        sb.Append("Fault Exception Data[Dictionary<string, string>]:- ");

                        foreach (var pair in sf.Data) {
                            sb.Append(string.Format("Key: {0}, Value: {1}: ",
                            pair.Key,
                            pair.Value));
                        }
                    }
                }

                sb.Append("Message: " + (string.IsNullOrEmpty(ex.Message) ? "None." : ex.Message) + Environment.NewLine);
                sb.Append("Stack Trace:" + (string.IsNullOrEmpty(ex.StackTrace) ? "" : ex.StackTrace) + Environment.NewLine);

                sb.Append("Inner Exception Message: " + (ex.InnerException != null && ex.InnerException.Message != null
                    ? ex.InnerException.Message : "") + Environment.NewLine);
                sb.Append("Inner Exception Stack Trace: " + (ex.InnerException != null && ex.InnerException.StackTrace != null
                    ? ex.InnerException.StackTrace : "") + Environment.NewLine);

                if (ex.InnerException != null && ex.InnerException.InnerException != null) {
                    var innerInner = ex.InnerException.InnerException;

                    sb.Append("Inner Inner Exception Message: " + (innerInner.Message != null ? innerInner.Message : "") + Environment.NewLine);
                    sb.Append("Inner Inner Exception Stack Trace: " + (innerInner.StackTrace != null ? innerInner.StackTrace : "") + Environment.NewLine);
                }
            }
            return sb.ToString();
        }

        private static string GetBrowserDetails(HttpBrowserCapabilitiesBase browser) {
            StringBuilder s = new StringBuilder();

            s.Append("Type: " + (string.IsNullOrEmpty(browser.Type) ? "" : browser.Type) + Environment.NewLine);
            s.Append("Name: " + (string.IsNullOrEmpty(browser.Browser) ? "" : browser.Browser) + Environment.NewLine);
            s.Append("Version: " + (string.IsNullOrEmpty(browser.Version) ? "" : browser.Version) + Environment.NewLine);
            s.Append("Major Version: " + Convert.ToString(browser.MajorVersion) + Environment.NewLine);
            s.Append("Minor Version: " + Convert.ToString(browser.MinorVersion) + Environment.NewLine);
            s.Append("Platform: " + (string.IsNullOrEmpty(browser.Platform) ? "" : browser.Platform) + Environment.NewLine);
            s.Append("Supports Cookies: " + Convert.ToString(browser.Cookies) + Environment.NewLine);

            return s.ToString();
        }

        private static string GetRequestDetails(HttpRequest request) {
            StringBuilder sb = new StringBuilder();

            sb.Append("Raw Url: " + request.RawUrl + Environment.NewLine);
            sb.Append("Request Url: " + Convert.ToString(request.Url == null ? "" : request.Url.ToString()) + Environment.NewLine);
            sb.Append("Method: " + request.HttpMethod.ToString() + Environment.NewLine);
            sb.Append("File Path: " + request.FilePath.ToString() + Environment.NewLine);
            sb.Append("User Host Address: " + request.UserHostAddress + Environment.NewLine);

            return sb.ToString();
        }

        private static string GetRequestParams(HttpRequest request) {
            StringBuilder sb = new StringBuilder();

            if (request != null) {
                if (request.Form != null && request.Form.Keys.Count > 0) {
                    foreach (string ky in request.Form.Keys) {
                        if (!string.IsNullOrEmpty(ky) && !ky.StartsWith("__")) {
                            sb.Append(string.Format("{0} => {1}", ky, ky.ToLower().Contains("password") ? "**************" : request.Form[ky].ToString()));
                            sb.Append(Environment.NewLine);
                        }
                    }
                }

                if (request.QueryString != null && request.QueryString.Keys.Count > 0) {
                    foreach (string ky in request.QueryString.Keys) {
                        if (!string.IsNullOrEmpty(ky) && !ky.StartsWith("__")) {
                            sb.Append(string.Format("{0} => {1}", ky, request.QueryString[ky].ToString()));
                            sb.Append(Environment.NewLine);
                        }
                    }
                }
            }
            else
                sb.Append("No request parameters." + Environment.NewLine);

            return sb.ToString();
        }


        private static string GetRequestDetails(HttpRequestBase request) {
            StringBuilder sb = new StringBuilder();

            sb.Append("Raw Url: " + request.RawUrl + Environment.NewLine);
            sb.Append("Request Url: " + Convert.ToString(request.Url == null ? "" : request.Url.ToString()) + Environment.NewLine);
            sb.Append("Method: " + request.HttpMethod.ToString() + Environment.NewLine);
            sb.Append("File Path: " + request.FilePath.ToString() + Environment.NewLine);
            sb.Append("User Host Address: " + request.UserHostAddress + Environment.NewLine);

            return sb.ToString();
        }

        private static string GetRequestParams(HttpRequestBase request) {
            StringBuilder sb = new StringBuilder();

            if (request != null) {
                if (request.Form != null && request.Form.Keys.Count > 0) {
                    foreach (string ky in request.Form.Keys) {
                        if (!string.IsNullOrEmpty(ky) && !ky.StartsWith("__")) {
                            sb.Append(string.Format("Key: {0} => Value: {1}", ky, ky.ToLower().Contains("password") ? "**************" : request.Form[ky].ToString()));
                            sb.Append(Environment.NewLine);
                        }
                    }
                }

                if (request.QueryString != null && request.QueryString.Keys.Count > 0) {
                    foreach (string ky in request.QueryString.Keys) {
                        if (!string.IsNullOrEmpty(ky) && !ky.StartsWith("__")) {
                            sb.Append(string.Format("Key: {0} => Value: {1}", ky, request.QueryString[ky].ToString()));
                            sb.Append(Environment.NewLine);
                        }
                    }
                }
            }
            else
                sb.Append("No request parameters." + Environment.NewLine);

            return sb.ToString();
        }


        private static string GetViewData(ViewDataDictionary viewdata) {
            StringBuilder sb = new StringBuilder();
            if (viewdata != null && viewdata.Keys.Count > 0) {
                sb.Append(string.Join(", ", viewdata.Select(kv => kv.Key.ToString())));

                return sb.ToString();
            }
            else
                return "ViewData empty." + Environment.NewLine;
        }

        private static string GetSessionDetails(HttpSessionStateBase session) {
            StringBuilder sb = new StringBuilder();

            if (session != null) {
                sb.Append("Session.Timeout       :  " + Convert.ToString(session.Timeout) + Environment.NewLine);
                sb.Append("Session(\"SessionID\")  :" + (SessionManager.Instance.CurrentUser != null ? SessionManager.Instance.CurrentUser.SessionKey : "Null") + Environment.NewLine);

                if (session.Keys != null) {
                    sb.Append("Keys: ");

                    foreach (string ky in session.Keys)
                        sb.Append(string.IsNullOrEmpty(ky) ? "None." : ky + ", ");

                    sb.Append(Environment.NewLine);
                }
                else
                    sb.Append("Keys: None" + Environment.NewLine);
            }
            else
                sb.Append("Session is empty." + Environment.NewLine);

            return sb.ToString();
        }
    }
}