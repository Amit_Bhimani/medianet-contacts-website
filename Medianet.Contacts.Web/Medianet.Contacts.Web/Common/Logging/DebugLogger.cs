﻿using System;
using System.Diagnostics;

namespace MediaPeople.Common.Logging
{
    /// <summary>
    /// A very simple implementation of <see cref="ILogger"/>
    /// that outputs all messages through <see cref="Debug.WriteLine(string)"/>.
    /// </summary>
    public class DebugLogger : LoggerBase
    {
        /// <summary>
        /// Logs a given item to the debugger.
        /// </summary>
        /// <param name="item">The item to be logged.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="item"/>
        /// is a null reference.</exception>
        public override void Log(LogItem item) {
            if (item == null) throw new ArgumentNullException("item");
            Debug.WriteLine(item.ToLogMessage());
        }
    }
}