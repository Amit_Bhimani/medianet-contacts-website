using System;
using System.Diagnostics;

namespace Medianet.Contacts.Web.Common.Logging
{
    /// <summary>
    /// Abstract base class which provides most logging method
    /// overloads for an <see cref="ILogger"/> implementation.
    /// </summary>
    public abstract class LoggerBase : ILogger
    {
        /// <summary>
        /// Logs an informational message (<see cref="TraceEventType.Information"/>).
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public virtual void Log(string message) {
            // Call overload
            Log(message, TraceEventType.Information);
        }


        /// <summary>
        /// Logs an exception with a logging level of
        /// <see cref="TraceEventType.Error"/>.
        /// </summary>
        /// <param name="exception">Exception to be logged.</param>
        public virtual void Log(Exception exception) {
            // Call overload
            Log(exception.ToString(), TraceEventType.Error);
        }


        /// <summary>
        /// Logs an error with an additional message with a logging level of
        /// <see cref="TraceEventType.Error"/>.
        /// </summary>
        /// <param name="message">Additional information regarding the
        /// logged exception.</param>
        /// <param name="exception">Exception to be logged.</param>
        public virtual void Log(string message, Exception exception) {
            // Call overload
            Log(message, exception, TraceEventType.Error);
        }

        /// <summary>
        /// Logs a message with a given logging severity.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="severity">Logging category.</param>
        public virtual void Log(string message, TraceEventType severity) {
            LogItem item = new LogItem();
            item.Message = message;
            item.Severity = severity;

            Log(item);
        }


        /// <summary>
        /// Logs an exception with a given logging severity and an
        /// additional message.
        /// </summary>
        /// <param name="message">Additional information regarding the
        /// logged exception.</param>
        /// <param name="exception">Exception to be logged.</param>
        /// <param name="severity">Logging category.</param>
        public virtual void Log(string message, Exception exception, TraceEventType severity) {
            LogItem item = new LogItem();
            item.Message = message;
            item.Severity = severity;

            if (exception != null)
                item.ErrorMessage = exception.ToString();
            else
                item.ErrorMessage = "[No error information available]";

            Log(item);
        }


        /// <summary>
        /// Creates a new log entry based on a given log item.
        /// </summary>
        /// <param name="item">Encaspulates logging information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="item"/>
        /// is a null reference.</exception>
        public abstract void Log(LogItem item);
    }
}