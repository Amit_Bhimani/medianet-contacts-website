﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.Common.Util
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString LabelForExt<TModel, TValue>(
            this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression,
            string labelText,
            object htmlAttributes
        ) {
            return LabelForExt(html, expression, labelText, new RouteValueDictionary(htmlAttributes));
        }

        public static MvcHtmlString LabelForExt<TModel, TValue>(
            this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression,
            string labelText,
            IDictionary<string, object> htmlAttributes
        ) {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            // string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(labelText)) {
                return MvcHtmlString.Empty;
            }

            TagBuilder tag = new TagBuilder("label");
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));
            tag.MergeAttributes(htmlAttributes, true);
            tag.SetInnerText(labelText);
            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString RadioButtonForEnum<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object htmlAttributesForRadio,
            object htmlAttributesForLabel
        ) {
            ModelMetadata metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            var names = Enum.GetNames(metaData.ModelType);
            var sb = new StringBuilder();

            foreach (var name in names) {
                var radioRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributesForRadio);
                var labelRouteValues = new RouteValueDictionary(htmlAttributesForLabel);

                var id = string.Format(
                    "{0}_{1}_{2}",
                    htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix,
                    metaData.PropertyName,
                    name
                );

                string description = Wcf.DataContracts.DTO.EnumHelper.GetDescription((Enum)Enum.Parse(metaData.ModelType, name));

                radioRouteValues.Add("id", id);
                radioRouteValues.Add("displayname", description);
                labelRouteValues.Add("for", id);

                var radio = htmlHelper.RadioButtonFor(expression, name, radioRouteValues).ToHtmlString();
                var label = htmlHelper.LabelFor(expression, description, labelRouteValues).ToHtmlString();

                sb.Append(radio).Append(label);
            }
            return MvcHtmlString.Create(sb.ToString());
        }

        /// <summary>
        /// Returns a HTML radio button for the boolean object specified in the expression.
        /// </summary>
        /// <param name="expression">An expression that identifies the object that contains the properties to render.</param>
        /// <param name="id">The value of the id attribute of the label to render.</param>
        /// <param name="labelText">The text in the label to render.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        public static MvcHtmlString RadioButtonForBool<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            bool selected,
            string labelText,
            object htmlAttributes
        ) {
            var sb = new StringBuilder();
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var name = metaData.PropertyName;
            var htmlAttributesDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            var id = htmlAttributesDictionary["id"];

            var radio = htmlHelper.RadioButtonFor(expression, selected, htmlAttributes).ToHtmlString();
            sb.AppendFormat(
                "{0}\n<label for=\"{1}\">{2}</label>",
                radio,
                id,
                HttpUtility.HtmlEncode(labelText)
            );
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString SelectionCriteriaPropertyRow<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string propName
        ) {
            string hiddenClass = "d-none";
            ModelMetadata metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var sb = new StringBuilder();
            bool emptyProp = false;
            string propValue = null;
            string listItem = "<li><p  class=\"prop_value\">{0}</p></li>";
            string iModelListItem = "<li class=\"{0}\"><p  class=\"prop_value\">{1}</p></li>";
            var entityProp = new TagBuilder("div");
            entityProp.AddCssClass("entity_prop");
            entityProp.AddCssClass(metaData.PropertyName);
            var propSpan = new TagBuilder("span");
            propSpan.AddCssClass("prop_name");

            string defaultValue = null;

            if (metaData.AdditionalValues.Keys.Contains("default"))
                defaultValue = metaData.AdditionalValues["default"].ToString();

            propSpan.SetInnerText(string.IsNullOrWhiteSpace(propName) ? string.IsNullOrWhiteSpace(metaData.DisplayName) ? metaData.PropertyName : metaData.DisplayName : propName);

            var propValueList = new TagBuilder("ul");

            propValueList.AddCssClass("prop_value");
            var li = new StringBuilder();

            if (metaData.ModelType.UnderlyingSystemType.Name == "".GetType().Name) {
                string sval = metaData.Model as string;
                int ival;
                if (string.IsNullOrWhiteSpace(sval) || (int.TryParse(sval, out ival) && ival < 1) || (defaultValue != null && sval.Equals(defaultValue, StringComparison.CurrentCultureIgnoreCase))) {
                    emptyProp = true;
                }
                else {
                    propValue = (string)metaData.Model;
                }
            }else if (metaData.ModelType.UnderlyingSystemType.Name ==  false.GetType().Name) {
                string boolstring = (bool)metaData.Model ? "Yes" : "No";

                if (defaultValue != null && boolstring.Equals(defaultValue, StringComparison.CurrentCultureIgnoreCase)) 
                    emptyProp = true;
                else 
                    li.Append(string.Format(listItem, boolstring));

            }else if(metaData.ModelType.IsNullableEnum()){
                if (metaData.ModelType.GetGenericArguments().Length > 0 && metaData.Model != null)
                    propValue = Wcf.DataContracts.DTO.EnumHelper.GetDescription((Enum)Enum.Parse(metaData.ModelType.GetGenericArguments()[0], metaData.Model.ToString()));

                if ((metaData.Model == null) || (defaultValue != null && propValue.Equals(defaultValue, StringComparison.CurrentCultureIgnoreCase)))
                    emptyProp = true;
            }else if (metaData.ModelType.IsEnum) {
                    propValue = Wcf.DataContracts.DTO.EnumHelper.GetDescription((Enum)Enum.Parse(metaData.ModelType, metaData.Model.ToString()));

                if (defaultValue != null && propValue.Equals(defaultValue, StringComparison.CurrentCultureIgnoreCase))
                    emptyProp = true;
            }
            else if (metaData.ModelType.UnderlyingSystemType.Name == 1.GetType().Name) {
                if ((int)metaData.Model < 1 || (defaultValue != null && metaData.Model.ToString().Equals(defaultValue, StringComparison.CurrentCultureIgnoreCase))) {
                    emptyProp = true;
                }
                else {
                    propValue = (string)metaData.Model;
                }
            }
            else if (metaData.ModelType.IsGenericType) {
                if (metaData.Model is IEnumerable<IModel>) {
                    IEnumerable<IModel> items = metaData.Model as IEnumerable<IModel>;

                    if (items != null && items.ToList().Count > 0) {
                        items.OrderBy(i => i.Name);
                        foreach (IModel item in items) {
                            li.Append(string.Format(iModelListItem, item.Id.ToString(), item.Name));
                        }
                    }
                    else {
                        emptyProp = true;
                    }
                }
                else if (metaData.Model is IEnumerable<int>) {
                    IEnumerable<int> items = metaData.Model as IEnumerable<int>;

                    if (items != null && items.ToList().Count > 0) {
                        items.OrderBy(i => i );
                        foreach (var item in items) {
                            li.Append(string.Format(iModelListItem, item.ToString(), item.ToString()));
                        }
                    }
                    else {
                        emptyProp = true;
                    }
                }
                else if (metaData.Model is IEnumerable<string>) {
                    IEnumerable<string> items = metaData.Model as IEnumerable<string>;

                    if (items != null && items.ToList().Count > 0) {
                        items.OrderBy(i => i);
                        foreach (var item in items) {
                            li.Append(string.Format(iModelListItem, item, item));
                        }
                    }
                    else {
                        emptyProp = true;
                    }
                }                
            }

            if (emptyProp)
                entityProp.AddCssClass(hiddenClass);
            else if (!string.IsNullOrWhiteSpace(propValue))
                li.Append(string.Format(listItem, propValue));

            propValueList.InnerHtml += li.ToString();

            sb.Append(propSpan.ToString()).Append(propValueList.ToString());

            entityProp.InnerHtml += sb.ToString();

            return MvcHtmlString.Create(entityProp.ToString());
        }

        public static MvcHtmlString HiddenForIModel<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression
        ) {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var name = metaData.PropertyName;
            List<IModel> items = null;

            MvcHtmlString hiddenInput = null;

            if (items == null)
                hiddenInput = htmlHelper.Hidden(name, "");
            else
                hiddenInput = htmlHelper.Hidden(name, string.Join(", ", items.ConvertAll<int>(sp => sp.Id).ToArray()));

            return MvcHtmlString.Create(hiddenInput.ToHtmlString());
        }

        public static MvcHtmlString DropDownList(this HtmlHelper helper,
            string name, Dictionary<int, string> dictionary, object htmlAttributes) {
            var selectListItems = new SelectList(dictionary, "Key", "Value");
            return helper.DropDownList(name, selectListItems, htmlAttributes);
        }

        public static MvcHtmlString NameToInitials(this HtmlHelper helper, string name)
        {
            var result = "";

            if (name != null)
            {
                var nameParts = name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (nameParts.Length > 0 && nameParts[0].Length > 0)
                {
                    result += nameParts[0].Substring(0, 1);

                    if (nameParts.Length > 1 && nameParts[nameParts.Length - 1].Length > 0)
                    {
                        result += nameParts[nameParts.Length - 1].Substring(0, 1);
                    }
                }
            }

            return MvcHtmlString.Create(result.ToUpper());
        }
    }
}
