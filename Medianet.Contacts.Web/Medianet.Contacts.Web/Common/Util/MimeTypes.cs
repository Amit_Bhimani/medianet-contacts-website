﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Medianet.Contacts.Web.Common.Util
{
    public sealed class MimeTypes
    {
        private MimeTypes()
        {
        }

        private static Dictionary<string, string> contentTypes;

        private static Dictionary<string, string> imageTypes;
        private static void InitializeImageTypes()
        {
            imageTypes = new Dictionary<string, string>();

            imageTypes.Add("bm", "image/bmp");
            imageTypes.Add("bmp", "image/bmp");
            imageTypes.Add("gif", "image/gif");
            imageTypes.Add("ico", "image/x-icon");
            imageTypes.Add("jfif", "image/jpeg");
            imageTypes.Add("jfif-tbnl", "image/jpeg");
            imageTypes.Add("jpe", "image/jpeg");
            imageTypes.Add("jpeg", "image/jpeg");
            imageTypes.Add("jpg", "image/jpeg");
            imageTypes.Add("png", "image/png");
            imageTypes.Add("tif", "image/tiff");
        }

        private static void InitializeMimeTypes()
        {
            contentTypes = new Dictionary<string, string>();
            contentTypes.Add("3dm", "x-world/x-3dmf");
            contentTypes.Add("3dmf", "x-world/x-3dmf");
            contentTypes.Add("a", "application/octet-stream");
            contentTypes.Add("aab", "application/x-authorware-bin");
            contentTypes.Add("aam", "application/x-authorware-map");
            contentTypes.Add("aas", "application/x-authorware-seg");
            contentTypes.Add("abc", "text/vnd.abc");
            contentTypes.Add("acgi", "text/html");
            contentTypes.Add("afl", "video/animaflex");
            contentTypes.Add("ai", "application/postscript");
            contentTypes.Add("aif", "audio/aiff");
            contentTypes.Add("aifc", "audio/aiff");
            contentTypes.Add("aiff", "audio/aiff");
            contentTypes.Add("aim", "application/x-aim");
            contentTypes.Add("aip", "text/x-audiosoft-intra");
            contentTypes.Add("ani", "application/x-navi-animation");
            contentTypes.Add("aos", "application/x-nokia-9000-communicator-add-on-software");
            contentTypes.Add("aps", "application/mime");
            contentTypes.Add("arc", "application/octet-stream");
            contentTypes.Add("arj", "application/arj");
            contentTypes.Add("art", "image/x-jg");
            contentTypes.Add("asf", "video/x-ms-asf");
            contentTypes.Add("asm", "text/x-asm");
            contentTypes.Add("asp", "text/asp");
            contentTypes.Add("asx", "application/x-mplayer2");
            contentTypes.Add("au", "audio/basic");
            contentTypes.Add("avi", "video/avi");
            contentTypes.Add("avs", "video/avs-video");
            contentTypes.Add("bcpio", "application/x-bcpio");
            contentTypes.Add("bin", "application/octet-stream");
            contentTypes.Add("bm", "image/bmp");
            contentTypes.Add("bmp", "image/bmp");
            contentTypes.Add("boo", "application/book");
            contentTypes.Add("book", "application/book");
            contentTypes.Add("boz", "application/x-bzip2");
            contentTypes.Add("bsh", "application/x-bsh");
            contentTypes.Add("bz", "application/x-bzip");
            contentTypes.Add("bz2", "application/x-bzip2");
            contentTypes.Add("c", "text/plain");
            contentTypes.Add("c++", "text/plain");
            contentTypes.Add("cat", "application/vnd.ms-pki.seccat");
            contentTypes.Add("cc", "text/plain");
            contentTypes.Add("ccad", "application/clariscad");
            contentTypes.Add("cco", "application/x-cocoa");
            contentTypes.Add("cdf", "application/cdf");
            contentTypes.Add("cer", "application/pkix-cert");
            contentTypes.Add("cha", "application/x-chat");
            contentTypes.Add("chat", "application/x-chat");
            contentTypes.Add("class", "application/java");
            contentTypes.Add("com", "application/octet-stream");
            contentTypes.Add("conf", "text/plain");
            contentTypes.Add("cpio", "application/x-cpio");
            contentTypes.Add("cpp", "text/x-c");
            contentTypes.Add("cpt", "application/x-cpt");
            contentTypes.Add("crl", "application/pkcs-crl");
            contentTypes.Add("css", "text/css");
            contentTypes.Add("def", "text/plain");
            contentTypes.Add("der", "application/x-x509-ca-cert");
            contentTypes.Add("dif", "video/x-dv");
            contentTypes.Add("dir", "application/x-director");
            contentTypes.Add("dl", "video/dl");
            contentTypes.Add("doc", "application/msword");
            contentTypes.Add("dot", "application/msword");
            contentTypes.Add("dp", "application/commonground");
            contentTypes.Add("drw", "application/drafting");
            contentTypes.Add("dump", "application/octet-stream");
            contentTypes.Add("dv", "video/x-dv");
            contentTypes.Add("dvi", "application/x-dvi");
            contentTypes.Add("dwf", "drawing/x-dwf (old)");
            contentTypes.Add("dwg", "application/acad");
            contentTypes.Add("dxf", "application/dxf");
            contentTypes.Add("eps", "application/postscript");
            contentTypes.Add("es", "application/x-esrehber");
            contentTypes.Add("etx", "text/x-setext");
            contentTypes.Add("evy", "application/envoy");
            contentTypes.Add("exe", "application/octet-stream");
            contentTypes.Add("f", "text/plain");
            contentTypes.Add("f90", "text/x-fortran");
            contentTypes.Add("fdf", "application/vnd.fdf");
            contentTypes.Add("fif", "image/fif");
            contentTypes.Add("fli", "video/fli");
            contentTypes.Add("for", "text/x-fortran");
            contentTypes.Add("fpx", "image/vnd.fpx");
            contentTypes.Add("g", "text/plain");
            contentTypes.Add("g3", "image/g3fax");
            contentTypes.Add("gif", "image/gif");
            contentTypes.Add("gl", "video/gl");
            contentTypes.Add("gsd", "audio/x-gsm");
            contentTypes.Add("gtar", "application/x-gtar");
            contentTypes.Add("gz", "application/x-compressed");
            contentTypes.Add("h", "text/plain");
            contentTypes.Add("help", "application/x-helpfile");
            contentTypes.Add("hgl", "application/vnd.hp-hpgl");
            contentTypes.Add("hh", "text/plain");
            contentTypes.Add("hlp", "application/x-winhelp");
            contentTypes.Add("htc", "text/x-component");
            contentTypes.Add("htm", "text/html");
            contentTypes.Add("html", "text/html");
            contentTypes.Add("htmls", "text/html");
            contentTypes.Add("htt", "text/webviewhtml");
            contentTypes.Add("htx", "text/html");
            contentTypes.Add("ice", "x-conference/x-cooltalk");
            contentTypes.Add("ico", "image/x-icon");
            contentTypes.Add("idc", "text/plain");
            contentTypes.Add("ief", "image/ief");
            contentTypes.Add("iefs", "image/ief");
            contentTypes.Add("iges", "application/iges");
            contentTypes.Add("igs", "application/iges");
            contentTypes.Add("ima", "application/x-ima");
            contentTypes.Add("imap", "application/x-httpd-imap");
            contentTypes.Add("inf", "application/inf");
            contentTypes.Add("ins", "application/x-internett-signup");
            contentTypes.Add("ip", "application/x-ip2");
            contentTypes.Add("isu", "video/x-isvideo");
            contentTypes.Add("it", "audio/it");
            contentTypes.Add("iv", "application/x-inventor");
            contentTypes.Add("ivr", "i-world/i-vrml");
            contentTypes.Add("ivy", "application/x-livescreen");
            contentTypes.Add("jam", "audio/x-jam");
            contentTypes.Add("jav", "text/plain");
            contentTypes.Add("java", "text/plain");
            contentTypes.Add("jcm", "application/x-java-commerce");
            contentTypes.Add("jfif", "image/jpeg");
            contentTypes.Add("jfif-tbnl", "image/jpeg");
            contentTypes.Add("jpe", "image/jpeg");
            contentTypes.Add("jpeg", "image/jpeg");
            contentTypes.Add("jpg", "image/jpeg");
            contentTypes.Add("jps", "image/x-jps");
            contentTypes.Add("js", "application/x-javascript");
            contentTypes.Add("jut", "image/jutvision");
            contentTypes.Add("kar", "audio/midi");
            contentTypes.Add("ksh", "application/x-ksh");
            contentTypes.Add("la", "audio/nspaudio");
            contentTypes.Add("lam", "audio/x-liveaudio");
            contentTypes.Add("latex", "application/x-latex");
            contentTypes.Add("lha", "application/lha");
            contentTypes.Add("lhx", "application/octet-stream");
            contentTypes.Add("list", "text/plain");
            contentTypes.Add("lma", "audio/nspaudio");
            contentTypes.Add("log", "text/plain");
            contentTypes.Add("lsp", "application/x-lisp");
            contentTypes.Add("lst", "text/plain");
            contentTypes.Add("lsx", "text/x-la-asf");
            contentTypes.Add("ltx", "application/x-latex");
            contentTypes.Add("lzh", "application/octet-stream");
            contentTypes.Add("lzx", "application/lzx");
            contentTypes.Add("m", "text/plain");
            contentTypes.Add("m1v", "video/mpeg");
            contentTypes.Add("m2a", "audio/mpeg");
            contentTypes.Add("m2v", "video/mpeg");
            contentTypes.Add("m3u", "audio/x-mpequrl");
            contentTypes.Add("man", "application/x-troff-man");
            contentTypes.Add("map", "application/x-navimap");
            contentTypes.Add("mar", "text/plain");
            contentTypes.Add("mbd", "application/mbedlet");
            contentTypes.Add("mc$", "application/x-magic-cap-package-1.0");
            contentTypes.Add("mcd", "application/mcad");
            contentTypes.Add("mcf", "image/vasa");
            contentTypes.Add("mcp", "application/netmc");
            contentTypes.Add("me", "application/x-troff-me");
            contentTypes.Add("mht", "message/rfc822");
            contentTypes.Add("mhtml", "message/rfc822");
            contentTypes.Add("mid", "audio/midi");
            contentTypes.Add("midi", "audio/midi");
            contentTypes.Add("mif", "application/x-frame");
            contentTypes.Add("mime", "message/rfc822");
            contentTypes.Add("mjf", "audio/x-vnd.audioexplosion.mjuicemediafile");
            contentTypes.Add("mjpg", "video/x-motion-jpeg");
            contentTypes.Add("mm", "application/base64");
            contentTypes.Add("mme", "application/base64");
            contentTypes.Add("mod", "audio/mod");
            contentTypes.Add("moov", "video/quicktime");
            contentTypes.Add("mov", "video/quicktime");
            contentTypes.Add("movie", "video/x-sgi-movie");
            contentTypes.Add("mp2", "audio/mpeg");
            contentTypes.Add("mp3", "audio/mpeg3");
            contentTypes.Add("mpa", "audio/mpeg");
            contentTypes.Add("mpc", "application/x-project");
            contentTypes.Add("mpe", "video/mpeg");
            contentTypes.Add("mpeg", "video/mpeg");
            contentTypes.Add("mpg", "video/mpeg");
            contentTypes.Add("mpga", "audio/mpeg");
            contentTypes.Add("mpp", "application/vnd.ms-project");
            contentTypes.Add("mpt", "application/x-project");
            contentTypes.Add("mpv", "application/x-project");
            contentTypes.Add("mpx", "application/x-project");
            contentTypes.Add("mrc", "application/marc");
            contentTypes.Add("ms", "application/x-troff-ms");
            contentTypes.Add("mv", "video/x-sgi-movie");
            contentTypes.Add("my", "audio/make");
            contentTypes.Add("mzz", "application/x-vnd.audioexplosion.mzz");
            contentTypes.Add("nap", "image/naplps");
            contentTypes.Add("naplps", "image/naplps");
            contentTypes.Add("nc", "application/x-netcdf");
            contentTypes.Add("ncm", "application/vnd.nokia.configuration-message");
            contentTypes.Add("nif", "image/x-niff");
            contentTypes.Add("niff", "image/x-niff");
            contentTypes.Add("nix", "application/x-mix-transfer");
            contentTypes.Add("nsc", "application/x-conference");
            contentTypes.Add("nvd", "application/x-navidoc");
            contentTypes.Add("o", "application/octet-stream");
            contentTypes.Add("oda", "application/oda");
            contentTypes.Add("omc", "application/x-omc");
            contentTypes.Add("omcd", "application/x-omcdatamaker");
            contentTypes.Add("omcr", "application/x-omcregerator");
            contentTypes.Add("p", "text/x-pascal");
            contentTypes.Add("p10", "application/pkcs10");
            contentTypes.Add("p12", "application/pkcs-12");
            contentTypes.Add("p7a", "application/x-pkcs7-signature");
            contentTypes.Add("p7c", "application/pkcs7-mime");
            contentTypes.Add("pas", "text/pascal");
            contentTypes.Add("pbm", "image/x-portable-bitmap");
            contentTypes.Add("pcl", "application/vnd.hp-pcl");
            contentTypes.Add("pct", "image/x-pict");
            contentTypes.Add("pcx", "image/x-pcx");
            contentTypes.Add("pdf", "application/pdf");
            contentTypes.Add("pfunk", "audio/make");
            contentTypes.Add("pgm", "image/x-portable-graymap");
            contentTypes.Add("pic", "image/pict");
            contentTypes.Add("pict", "image/pict");
            contentTypes.Add("pkg", "application/x-newton-compatible-pkg");
            contentTypes.Add("pko", "application/vnd.ms-pki.pko");
            contentTypes.Add("pl", "text/plain");
            contentTypes.Add("plx", "application/x-pixclscript");
            contentTypes.Add("pm", "image/x-xpixmap");
            contentTypes.Add("png", "image/png");
            contentTypes.Add("pnm", "application/x-portable-anymap");
            contentTypes.Add("pot", "application/mspowerpoint");
            contentTypes.Add("pov", "model/x-pov");
            contentTypes.Add("ppa", "application/vnd.ms-powerpoint");
            contentTypes.Add("ppm", "image/x-portable-pixmap");
            contentTypes.Add("pps", "application/mspowerpoint");
            contentTypes.Add("ppt", "application/mspowerpoint");
            contentTypes.Add("ppz", "application/mspowerpoint");
            contentTypes.Add("pre", "application/x-freelance");
            contentTypes.Add("prt", "application/pro_eng");
            contentTypes.Add("ps", "application/postscript");
            contentTypes.Add("psd", "application/octet-stream");
            contentTypes.Add("pvu", "paleovu/x-pv");
            contentTypes.Add("pwz", "application/vnd.ms-powerpoint");
            contentTypes.Add("py", "text/x-script.phyton");
            contentTypes.Add("pyc", "applicaiton/x-bytecode.python");
            contentTypes.Add("qcp", "audio/vnd.qcelp");
            contentTypes.Add("qd3", "x-world/x-3dmf");
            contentTypes.Add("qd3d", "x-world/x-3dmf");
            contentTypes.Add("qif", "image/x-quicktime");
            contentTypes.Add("qt", "video/quicktime");
            contentTypes.Add("qtc", "video/x-qtc");
            contentTypes.Add("qti", "image/x-quicktime");
            contentTypes.Add("qtif", "image/x-quicktime");
            contentTypes.Add("ra", "audio/x-pn-realaudio");
            contentTypes.Add("ram", "audio/x-pn-realaudio");
            contentTypes.Add("ras", "application/x-cmu-raster");
            contentTypes.Add("rast", "image/cmu-raster");
            contentTypes.Add("rexx", "text/x-script.rexx");
            contentTypes.Add("rf", "image/vnd.rn-realflash");
            contentTypes.Add("rgb", "image/x-rgb");
            contentTypes.Add("rm", "application/vnd.rn-realmedia");
            contentTypes.Add("rmi", "audio/mid");
            contentTypes.Add("rmm", "audio/x-pn-realaudio");
            contentTypes.Add("rmp", "audio/x-pn-realaudio");
            contentTypes.Add("rng", "application/ringing-tones");
            contentTypes.Add("rnx", "application/vnd.rn-realplayer");
            contentTypes.Add("roff", "application/x-troff");
            contentTypes.Add("rp", "image/vnd.rn-realpix");
            contentTypes.Add("rpm", "audio/x-pn-realaudio-plugin");
            contentTypes.Add("rt", "text/richtext");
            contentTypes.Add("rtf", "text/richtext");
            contentTypes.Add("rtx", "application/rtf");
            contentTypes.Add("rv", "video/vnd.rn-realvideo");
            contentTypes.Add("s", "text/x-asm");
            contentTypes.Add("s3m", "audio/s3m");
            contentTypes.Add("saveme", "application/octet-stream");
            contentTypes.Add("sbk", "application/x-tbook");
            contentTypes.Add("scm", "application/x-lotusscreencam");
            contentTypes.Add("sdml", "text/plain");
            contentTypes.Add("sdp", "application/sdp");
            contentTypes.Add("sdr", "application/sounder");
            contentTypes.Add("sea", "application/sea");
            contentTypes.Add("set", "application/set");
            contentTypes.Add("sgm", "text/sgml");
            contentTypes.Add("sgml", "text/sgml");
            contentTypes.Add("sh", "application/x-bsh");
            contentTypes.Add("shtml", "text/html");
            contentTypes.Add("sid", "audio/x-psid");
            contentTypes.Add("sit", "application/x-sit");
            contentTypes.Add("skd", "application/x-koan");
            contentTypes.Add("skm", "application/x-koan");
            contentTypes.Add("skp", "application/x-koan");
            contentTypes.Add("skt", "application/x-koan");
            contentTypes.Add("sl", "application/x-seelogo");
            contentTypes.Add("smi", "application/smil");
            contentTypes.Add("smil", "application/smil");
            contentTypes.Add("snd", "audio/basic");
            contentTypes.Add("sol", "application/solids");
            contentTypes.Add("spc", "application/x-pkcs7-certificates");
            contentTypes.Add("spl", "application/futuresplash");
            contentTypes.Add("spr", "application/x-sprite");
            contentTypes.Add("sprite", "application/x-sprite");
            contentTypes.Add("src", "application/x-wais-source");
            contentTypes.Add("ssi", "text/x-server-parsed-html");
            contentTypes.Add("ssm", "application/streamingmedia");
            contentTypes.Add("sst", "application/vnd.ms-pki.certstore");
            contentTypes.Add("step", "application/step");
            contentTypes.Add("stl", "application/sla");
            contentTypes.Add("stp", "application/step");
            contentTypes.Add("sv4cpio", "application/x-sv4cpio");
            contentTypes.Add("sv4crc", "application/x-sv4crc");
            contentTypes.Add("svf", "image/vnd.dwg");
            contentTypes.Add("svr", "application/x-world");
            contentTypes.Add("swf", "application/x-shockwave-flash");
            contentTypes.Add("t", "application/x-troff");
            contentTypes.Add("talk", "text/x-speech");
            contentTypes.Add("tar", "application/x-tar");
            contentTypes.Add("tbk", "application/toolbook");
            contentTypes.Add("tcl", "application/x-tcl");
            contentTypes.Add("tcsh", "text/x-script.tcsh");
            contentTypes.Add("tex", "application/x-tex");
            contentTypes.Add("texi", "application/x-texinfo");
            contentTypes.Add("texinfo", "application/x-texinfo");
            contentTypes.Add("text", "text/plain");
            contentTypes.Add("tgz", "application/x-compressed");
            contentTypes.Add("tif", "image/tiff");
            contentTypes.Add("tr", "application/x-troff");
            contentTypes.Add("tsi", "audio/tsp-audio");
            contentTypes.Add("tsp", "audio/tsplayer");
            contentTypes.Add("tsv", "text/tab-separated-values");
            contentTypes.Add("turbot", "image/florian");
            contentTypes.Add("txt", "text/plain");
            contentTypes.Add("uil", "text/x-uil");
            contentTypes.Add("uni", "text/uri-list");
            contentTypes.Add("unis", "text/uri-list");
            contentTypes.Add("unv", "application/i-deas");
            contentTypes.Add("uri", "text/uri-list");
            contentTypes.Add("uris", "text/uri-list");
            contentTypes.Add("ustar", "application/x-ustar");
            contentTypes.Add("uu", "application/octet-stream");
            contentTypes.Add("vcd", "application/x-cdlink");
            contentTypes.Add("vcs", "text/x-vcalendar");
            contentTypes.Add("vda", "application/vda");
            contentTypes.Add("vdo", "video/vdo");
            contentTypes.Add("vew", "application/groupwise");
            contentTypes.Add("viv", "video/vivo");
            contentTypes.Add("vivo", "video/vivo");
            contentTypes.Add("vmd", "application/vocaltec-media-desc");
            contentTypes.Add("vmf", "application/vocaltec-media-file");
            contentTypes.Add("voc", "audio/voc");
            contentTypes.Add("vos", "video/vosaic");
            contentTypes.Add("vox", "audio/voxware");
            contentTypes.Add("vqe", "audio/x-twinvq-plugin");
            contentTypes.Add("vqf", "audio/x-twinvq");
            contentTypes.Add("vql", "audio/x-twinvq-plugin");
            contentTypes.Add("vrml", "application/x-vrml");
            contentTypes.Add("vrt", "x-world/x-vrt");
            contentTypes.Add("vsd", "application/x-visio");
            contentTypes.Add("vst", "application/x-visio");
            contentTypes.Add("vsw", "application/x-visio");
            contentTypes.Add("w60", "application/wordperfect6.0");
            contentTypes.Add("w61", "application/wordperfect6.1");
            contentTypes.Add("w6w", "application/msword");
            contentTypes.Add("wav", "audio/wav");
            contentTypes.Add("wb1", "application/x-qpro");
            contentTypes.Add("wbmp", "image/vnd.wap.wbmp");
            contentTypes.Add("web", "application/vnd.xara");
            contentTypes.Add("wiz", "application/msword");
            contentTypes.Add("wk1", "application/x-123");
            contentTypes.Add("wmf", "windows/metafile");
            contentTypes.Add("wml", "text/vnd.wap.wml");
            contentTypes.Add("wmlc", "application/vnd.wap.wmlc");
            contentTypes.Add("wmls", "text/vnd.wap.wmlscript");
            contentTypes.Add("wmlsc", "application/vnd.wap.wmlscriptc");
            contentTypes.Add("word", "application/msword");
            contentTypes.Add("wp", "application/wordperfect");
            contentTypes.Add("wp5", "application/wordperfect");
            contentTypes.Add("wp6", "application/wordperfect");
            contentTypes.Add("wpd", "application/wordperfect");
            contentTypes.Add("wq1", "application/x-lotus");
            contentTypes.Add("wri", "application/mswrite");
            contentTypes.Add("wrl", "application/x-world");
            contentTypes.Add("wrz", "model/vrml");
            contentTypes.Add("wsc", "text/scriplet");
            contentTypes.Add("wsrc", "application/x-wais-source");
            contentTypes.Add("wtk", "application/x-wintalk");
            contentTypes.Add("xbm", "image/x-xbitmap");
            contentTypes.Add("xdr", "video/x-amt-demorun");
            contentTypes.Add("xgz", "xgl/drawing");
            contentTypes.Add("xif", "image/vnd.xiff");
            contentTypes.Add("xl", "application/excel");
            contentTypes.Add("xla", "application/excel");
            contentTypes.Add("xlb", "application/excel");
            contentTypes.Add("xlc", "application/excel");
            contentTypes.Add("xld", "application/excel");
            contentTypes.Add("xlk", "application/excel");
            contentTypes.Add("xll", "application/excel");
            contentTypes.Add("xlm", "application/excel");
            contentTypes.Add("xls", "application/excel");
            contentTypes.Add("xlt", "application/excel");
            contentTypes.Add("xlv", "application/excel");
            contentTypes.Add("xlw", "application/excel");
            contentTypes.Add("xm", "audio/xm");
            contentTypes.Add("xml", "text/xml");
            contentTypes.Add("xmz", "xgl/movie");
            contentTypes.Add("xpix", "application/x-vnd.ls-xpix");
            contentTypes.Add("xpm", "image/x-xpixmap");
            contentTypes.Add("x-png", "image/png");
            contentTypes.Add("xsr", "video/x-amt-showrun");
            contentTypes.Add("xwd", "image/x-xwd");
            contentTypes.Add("xyz", "chemical/x-pdb");
            contentTypes.Add("z", "application/x-compress");
            contentTypes.Add("zip", "application/x-compressed");
            contentTypes.Add("zoo", "application/octet-stream");
            contentTypes.Add("zsh", "text/x-script.zsh");

        }

        /// <summary>
        /// Given a file name, determines the MIME type
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <returns>String containing the MIME type</returns>
        public static string GetContentType(string fileName)
        {
            if (contentTypes == null || !(contentTypes.Count > 0))
            {
                InitializeMimeTypes();
            }

            string extension = null;

            FileInfo fi = new FileInfo(fileName);
            extension = fi.Extension.Replace(".", "");

            string contentType = null;
            contentTypes.TryGetValue(extension.ToLower(), out contentType);

            if (String.IsNullOrEmpty(contentType))
            {
                contentType = "application/octet-stream";
            }

            return contentType;
        }

        public static bool IsImageType(string fileName)
        {
            bool result = false;

            if (imageTypes == null || !(imageTypes.Count > 0))
            {
                InitializeImageTypes();
            }

            string extension = null;
            FileInfo fi = new FileInfo(fileName);
            extension = fi.Extension.Replace(".", "");

            if (imageTypes.ContainsKey(extension))
                result = true;

            return result;

        }

    }
}