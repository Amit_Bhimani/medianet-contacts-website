﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Medianet.Contacts.Web.Common.Util
{
    public class FilesStatus
    {
        public static string AllowedTypesRegex = @"^.*\.(pdf|xlsx?|docx?)$";
        private static readonly int MaxFilesAllowed = 5;
        public static readonly int MaxFileSize = 4194304;
        public static readonly string HandlerPath = "/";
        public static readonly string FileTooBig = "Max allowed size is: " + (GetMaxAllowedFileSize() / 1024).ToString() + " MB.";
        public static readonly string TransportError = "Corrupted file received.";
        public static readonly string MaxNumberOfFiles = "Max. " + MaxFilesAllowed.ToString() + " files are allowed";
        public static readonly string FileTypeNotAllowed = "Only pdf, xls(x), doc(x) are allowed";
        public static readonly string ServerError = "Server error.";

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }
        public bool isPrivate { get; set; }

        public static int GetMaxAllowedFileSize()
        {
            return MaxFileSize / 1024;
        }

        public FilesStatus() { }

        public FilesStatus(FileInfo fileInfo) { SetValues(fileInfo.Name, (int)fileInfo.Length); }

        public FilesStatus(string fileName, int fileLength) { SetValues(fileName, fileLength); }

        private void SetValues(string fileName, int fileLength)
        {
            name = fileName;
            type = "image/png";
            size = fileLength;
            progress = "1.0";
            url = HandlerPath + "Document/Upload";
            thumbnail_url = HandlerPath + "Thumbnail.ashx?f=" + fileName;
            delete_url = HandlerPath + "Document/Delete?f=" + fileName;
            delete_type = "DELETE";
            isPrivate = false;
        }

        public static string GetValidFilename(string filename)
        {
            string validname = filename;

            Path.GetInvalidFileNameChars().ToList().ForEach(e => validname = validname.Replace(e.ToString(), ""));

            return validname;
        }

        public static bool IsFileUploadValid(HttpPostedFileBase f, out FilesStatus fs)
        {
            if (string.IsNullOrEmpty(f.FileName) || f.ContentLength == 0 || f.InputStream == null)
            {
                fs = new FilesStatus
                {
                    name = string.IsNullOrEmpty(f.FileName) ? "" : Path.GetFileName(f.FileName),
                    error = FilesStatus.TransportError
                };
                return false;
            }

            if (!Regex.IsMatch(f.FileName, FilesStatus.AllowedTypesRegex))
            {
                fs = new FilesStatus
                {
                    name = Path.GetFileName(f.FileName),
                    error = FilesStatus.FileTypeNotAllowed
                };
                return false;
            }

            if (!Regex.IsMatch(f.FileName, FilesStatus.AllowedTypesRegex))
            {
                fs = new FilesStatus
                {
                    name = Path.GetFileName(f.FileName),
                    error = FilesStatus.FileTypeNotAllowed
                };
                return false;
            }

            if (f.ContentLength > FilesStatus.GetMaxAllowedFileSize() * 1024)
            {
                fs = new FilesStatus
                {
                    name = Path.GetFileName(f.FileName),
                    error = FilesStatus.FileTooBig
                };

                return false;
            }

            fs = new FilesStatus();

            return true;
        }
    }
}