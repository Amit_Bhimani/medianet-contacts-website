﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Common.Util
{
    public static class ExtensionMethods
    {
        const int TimedOutExceptionCode = -2147467259;

        public static int? ToNullableInt32(this string s)
        {
            int i;
            if (Int32.TryParse(s, out i)) return i;
            return null;
        }

        public static bool IsMaxRequestExceededException(Exception e)
        {
            // unhandeled errors = caught at global.ascx level
            // http exception = caught at page level

            Exception main;
            var unhandeled = e as HttpUnhandledException;

            if (unhandeled != null && unhandeled.ErrorCode == TimedOutExceptionCode)
            {
                main = unhandeled.InnerException;
            }
            else
            {
                main = e;
            }
            
            var http = main as HttpException;

            if (http != null && http.ErrorCode == TimedOutExceptionCode)
            {
                // hack: no real method of identifing if the error is max request exceeded as 
                // it is treated as a timeout exception
                if (http.StackTrace.Contains("GetEntireRawContent"))
                {
                    // MAX REQUEST HAS BEEN EXCEEDED
                    return true;
                }
            }

            return false;
        }

        public static List<int> ParseIntCSV(this string pValue)
        {
            int iVal;

            return string.IsNullOrWhiteSpace(pValue) 
                ? new List<int>() 
                : pValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                               .ToList().Where(pv => int.TryParse(pv, out iVal))
                               .ToList().ConvertAll<int>( pv => int.Parse(pv)).ToList();
        }

        public static List<int> ParsePrefixedIntCSV(this string pValue, string pPrefix) {
            List<int> result = new List<int>();

            if (!string.IsNullOrEmpty(pValue)) {
                string[] idList = pValue.Split(',');
                int id;

                foreach (string s in idList) {
                    if (s.StartsWith(pPrefix)) {

                        if (int.TryParse(s.Substring(pPrefix.Length), out id))
                            result.Add(id);
                    }
                }
            }

            return result;
        }

        public static List<string> ParseStringCSV(this string pValue)
        {
            return string.IsNullOrWhiteSpace(pValue) 
                ? new List<string>()
                : pValue.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static string ConvertToCSV(this DataTable dtable) {
            StringBuilder sb = new StringBuilder();

            int icolcount = dtable.Columns.Count;

            for (int i = 0; i < icolcount; i++) {
                sb.Append(dtable.Columns[i]);

                if (i < icolcount - 1) {
                    sb.Append(",");
                }
            }

            sb.Append(System.Environment.NewLine);

            foreach (DataRow drow in dtable.Rows) {
                for (int i = 0; i < icolcount; i++) {
                    if (!Convert.IsDBNull(drow[i])) {
                        sb.Append("\"" + drow[i].ToString().Replace("\"", "\"\"") + "\"");
                    }
                    else {
                        sb.Append("\"\"");
                    }

                    if (i < icolcount - 1) {
                        sb.Append(",");
                    }
                }

                sb.Append(System.Environment.NewLine);
            }

            return sb.ToString();
        }
    }

    public static class RecordTypeExtensions
    {
        public static bool IsOmaContact(this RecordType val) {
            return (val == RecordType.OmaContactAtMediaOutlet) || (val == RecordType.OmaContactAtOmaOutlet) || (val == RecordType.OmaContactAtPrnOutlet) || (val == RecordType.OmaContactNoOutlet);
        }

        public static bool IsMediaType(this RecordType val) {
            return (val == RecordType.MediaContact) || (val == RecordType.MediaOutlet);
        }

        public static bool IsPrnType(this RecordType val) {
            return (val == RecordType.PrnOutlet) || (val == RecordType.PrnContact);
        }

        public static bool IsOmaType(this RecordType val) {
            return (val == RecordType.OmaOutlet) || val.IsOmaContact();
        }

        public static bool IsContact(this RecordType val) {
            return (val == RecordType.MediaContact) || (val == RecordType.PrnContact) || (val.IsOmaContact());
        }

        public static bool IsOutlet(this RecordType val) {
            return (val == RecordType.MediaOutlet) || (val == RecordType.OmaOutlet) || (val == RecordType.PrnOutlet);
        }
    }
}