﻿using System.Collections.Generic;
using Medianet.Contacts.CSharp.ExtensionMethods;
using AutoMapper;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Services.List;

namespace Medianet.Contacts.Web.Common.ValueResolvers
{
    public class ListResolver : ValueResolver<List<int>, List<ListBase>>
    {
        protected override List<ListBase> ResolveCore(List<int> source) {
            List<ListBase> lists = null;

            if (source != null && source.Count > 0) {
                var _listService = ListService.CreateListService();
                var listids = source.JoinDelimited(",", x => x.ToString());

                lists = _listService.GetListsByIds(listids);
            }

            return lists;
        }
    }
}