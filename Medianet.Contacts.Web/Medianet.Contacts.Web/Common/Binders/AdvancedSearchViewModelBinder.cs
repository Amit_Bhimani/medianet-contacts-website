﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.ViewModels;
using City = Medianet.Contacts.Web.Common.Model.City;
using Country = Medianet.Contacts.Web.Common.Model.Country;
using State = Medianet.Contacts.Web.Common.Model.State;

namespace Medianet.Contacts.Web.Common.Binders
{
    public class AdvancedSearchViewModelBinder : TrimModelBinder
    {
        public AdvancedSearchViewModelBinder() {     
        }

        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext) {
            base.OnModelUpdated(controllerContext, bindingContext);
        }

        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, System.ComponentModel.PropertyDescriptor propertyDescriptor) {
            switch (propertyDescriptor.Name) {
                case "SelectedPositions":
                    BindSelectedPositionsProperty(controllerContext, bindingContext);
                    break;
                case "SelectedCSubjects":
                case "SelectedOSubjects":
                    BindSelectedSubjectsProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedCSubjectGroups":
                case "SelectedOSubjectGroups":
                    BindSelectedSubjectGroupsProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedCContinents":
                case "SelectedOContinents":
                    BindSelectedContinentsProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedCCountries":
                case "SelectedOCountries":
                    BindSelectedCountriesProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedCStates":
                case "SelectedOStates":
                    BindSelectedStatesProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedCCities":
                case "SelectedOCities":
                    BindSelectedCitiesProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedMediaTypes":
                    BindSelectedMediaTypesProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedLanguages":
                    BindSelectedLanguagesProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedFrequencies":
                    BindSelectedFrequenciesProperty(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                case "SelectedListIds":
                    BindSelectedPropertyIds(controllerContext, bindingContext, propertyDescriptor.Name);
                    break;
                default:
                    base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
                    break;
            }
        }

        //TODO: Remove this function if tasks is gone for good
        private void BindSelectedTaskStatusProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            // this code doesn't work while tasks is commented out on the advanced search page.
            //string taskStatus = "";
            //var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);

            //if (valueProvider != null)
            //    taskStatus = valueProvider.AttemptedValue;

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            //if (Enum.IsDefined(typeof(TaskStatus), Enum.Parse(typeof(TaskStatus), taskStatus))) {
            //    pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), Enum.Parse(typeof(TaskStatus), taskStatus), null);
            //}
            //else {
                pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), null, null);
            //}
        }

        private void BindSelectedPropertyIds(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string listIds = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);
            
            if (valueProvider != null)
                listIds = valueProvider.AttemptedValue;

            List<int> selectedIds = string.IsNullOrWhiteSpace(listIds) ? new List<int>() : listIds.ParseIntCSV();

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), selectedIds, null);
        }

        private void BindSelectedMediaTypesProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedMediaTypes = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);

            if (valueProvider != null)
                selectedMediaTypes = valueProvider.AttemptedValue;

            List<MediaType> mediaTypes = new List<MediaType>();
            List<int> selectedMediaTyps = string.IsNullOrWhiteSpace(selectedMediaTypes) ? new List<int>() : selectedMediaTypes.ParseIntCSV();

            foreach (int con in selectedMediaTyps) {
                MediaType continent = (MediaType)TypeDescriptor.GetConverter(typeof(MediaType)).ConvertFrom(con);
                if (continent != null)
                    mediaTypes.Add(continent);
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), mediaTypes, null);
        }

        private void BindSelectedSubjectGroupsProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string propname = propertyName.Equals("SelectedCSubjectGroups") ? "SelectedCSubjects" : "SelectedOSubjects";
            int subjectId;
            string selectedSubs = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propname);

            if (valueProvider != null)
                selectedSubs = valueProvider.AttemptedValue;

            List<SubjectGroup> selectedGroups = new List<SubjectGroup>();

            if (!string.IsNullOrWhiteSpace(selectedSubs)) {                

                List<int> selectedGrps = (selectedSubs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList()
                                               .Where(sub => !int.TryParse(sub, out subjectId)
                                                              && sub.StartsWith("g", StringComparison.CurrentCultureIgnoreCase)
                                                              && sub.Length >= 2
                                                              && int.TryParse(sub.Substring(1), out subjectId)).ToList()
                                               .ConvertAll(sub => int.Parse(sub.Substring(1))).ToList());

                foreach (int groupId in selectedGrps) {
                    selectedGroups.Add((SubjectGroup)TypeDescriptor.GetConverter(typeof(SubjectGroup)).ConvertFrom(groupId));
                }
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), selectedGroups, null);
        }

        private void BindSelectedSubjectsProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedSubs = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);

            if (valueProvider != null)
                selectedSubs = valueProvider.AttemptedValue;

            List<Subject> selectedSubjects = new List<Subject>();
            List<int> selectedPositions = string.IsNullOrWhiteSpace(selectedSubs) ? new List<int>() : selectedSubs.ParseIntCSV();

            foreach (int subId in selectedPositions)
                selectedSubjects.Add((Subject)TypeDescriptor.GetConverter(typeof(Subject)).ConvertFrom(subId));

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), selectedSubjects, null);
        }

        private void BindSelectedPositionsProperty(ControllerContext controllerContext, ModelBindingContext bindingContext) {
            string selectedPos = "";
            var valueProvider = bindingContext.ValueProvider.GetValue("SelectedPositions");

            if (valueProvider != null)
                selectedPos = valueProvider.AttemptedValue;

            List<Position> positions = new List<Position>();
            List<int> selectedPositions = string.IsNullOrWhiteSpace(selectedPos) ? new List<int>() : selectedPos.ParseIntCSV();

            foreach (int item in selectedPositions) {
                Position pos = (Position)TypeDescriptor.GetConverter(typeof(Position)).ConvertFrom(item);
                if (pos != null)
                    positions.Add(pos);
            }

            ((AdvancedSearchViewModel)bindingContext.Model).SelectedPositions = positions;
        }

        private void BindSelectedContinentsProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedCons = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);

            if (valueProvider != null)
                selectedCons = valueProvider.AttemptedValue;

            List<Continent> continents = new List<Continent>();
            List<int> selectedContinents = string.IsNullOrWhiteSpace(selectedCons) ? new List<int>() : selectedCons.ParseIntCSV();

            foreach (int con in selectedContinents) {
                Continent continent = (Continent)TypeDescriptor.GetConverter(typeof(Continent)).ConvertFrom(con);
                if (continent != null)
                    continents.Add(continent);
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), continents, null);
        }

        private void BindSelectedCountriesProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedCountries = "";

            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);
            if (valueProvider != null)
                selectedCountries = valueProvider.AttemptedValue;

            List<Country> countries = new List<Country>();
            List<int> selectedCons = string.IsNullOrWhiteSpace(selectedCountries) ? new List<int>() : selectedCountries.ParseIntCSV();

            foreach (int country in selectedCons) {
                Country ctry = (Country)TypeDescriptor.GetConverter(typeof(Country)).ConvertFrom(country);
                if (ctry != null)
                    countries.Add(ctry);
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), countries, null);
        }

        private void BindSelectedStatesProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedStates = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);
            if (valueProvider != null)
                selectedStates = valueProvider.AttemptedValue;

            List<int> selectedSts = string.IsNullOrWhiteSpace(selectedStates) ? new List<int>() : selectedStates.ParseIntCSV();
            List<State> states = new List<State>();

            foreach (int st in selectedSts) {
                State state = (State)TypeDescriptor.GetConverter(typeof(State)).ConvertFrom(st);

                if (state != null)
                    states.Add(state);
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), states, null);
        }

        private void BindSelectedCitiesProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedCities = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);

            if (valueProvider != null)
                selectedCities = valueProvider.AttemptedValue;

            List<int> selectedCts = string.IsNullOrWhiteSpace(selectedCities) ? new List<int>() : selectedCities.ParseIntCSV();
            List<City> cities = new List<City>();

            foreach (int ct in selectedCts) {
                City city = (City)TypeDescriptor.GetConverter(typeof(City)).ConvertFrom(ct);

                if (city != null)
                    cities.Add(city);
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), cities, null);
        }

        private void BindSelectedFrequenciesProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedFrequencies = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);

            if (valueProvider != null)
                selectedFrequencies = valueProvider.AttemptedValue;

            List<int> selectedFrqs = string.IsNullOrWhiteSpace(selectedFrequencies) ? new List<int>() : selectedFrequencies.ParseIntCSV();
            List<OutletFrequency> frequencies = new List<OutletFrequency>();

            foreach (int fq in selectedFrqs) {
                OutletFrequency freq = (OutletFrequency)TypeDescriptor.GetConverter(typeof(OutletFrequency)).ConvertFrom(fq);

                if (freq != null)
                    frequencies.Add(freq);
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), frequencies, null);
        }

        private void BindSelectedLanguagesProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, string propertyName) {
            string selectedLanguages = "";
            var valueProvider = bindingContext.ValueProvider.GetValue(propertyName);

            if (valueProvider != null)
                selectedLanguages = valueProvider.AttemptedValue;

            List<int> selectedLangs = string.IsNullOrWhiteSpace(selectedLanguages) ? new List<int>() : selectedLanguages.ParseIntCSV();
            List<Language> languages = new List<Language>();

            foreach (int fq in selectedLangs) {
                Language lang = (Language)TypeDescriptor.GetConverter(typeof(Language)).ConvertFrom(fq);

                if (lang != null)
                    languages.Add(lang);
            }

            PropertyInfo pi = typeof(AdvancedSearchViewModel).GetProperty(propertyName);

            pi.SetValue(((AdvancedSearchViewModel)bindingContext.Model), languages, null);
        }
    }
}