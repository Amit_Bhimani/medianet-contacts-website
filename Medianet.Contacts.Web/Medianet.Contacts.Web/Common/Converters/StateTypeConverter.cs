﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.Common.Converters
{
    public class StateTypeConverter : TypeConverter
    {
        const string WrongType = "The type of value is not a State type!";

        /// <summary>
        /// we can convert from a int to this type
        /// </summary>
        /// <param name="context">context descriptor</param>
        /// <param name="sourceType">source type</param>
        /// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            if (sourceType == typeof(int))
                return true;
            else
                return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// we can convert to a int a instance descriptor
        /// </summary>
        /// <param name="context">context descriptor</param>
        /// <param name="destinationType">destination type</param>
        /// <returns></returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) {
            if ((destinationType == typeof(int)) |
                (destinationType == typeof(InstanceDescriptor)))
                return true;
            else
                return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        /// convert from State to an int or instance descriptor
        /// </summary>
        /// <param name="context">context descriptor</param>
        /// <param name="culture">culture info</param>
        /// <param name="value">the value to convert</param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType) {
            // check that the value we got passed on is of type State
            if (value != null)
                if (!(value is State))
                    throw new Exception(WrongType);

            // convert to a int
            if (destinationType == typeof(int)) {
                // no value so we return an empty int
                if (value == null)
                    return -1;

                // strongly typed
                State sub = value as State;

                // convert to a string and return
                return sub.Id;
            }

            // call the base converter
            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>
        /// convert from an int
        /// </summary>
        /// <param name="context">context descriptor</param>
        /// <param name="culture">culture info</param>
        /// <param name="value">value</param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value) {
            // no value so we return a new State instance
            if (value == null)
                return new State();

            // convert from an int
            if (value is int) {
                int ival = (int)value;
                State state;

                using(DBClass db = new DBClass()){
                    state = new GeographicBAL(db).GetState(ival);
                }

                return state == null ? new State() : state;
            }

            // otherwise call the base converter
            else
                return base.ConvertFrom(context, culture, value);
        }
    }
}