﻿using System.Configuration;
using Medianet.Contacts.Web.ProxyWrappers;
using Ninject.Modules;

namespace Medianet.Contacts.Web.Ninject
{
    public class ServiceProxyModule : NinjectModule
    {
        private readonly string _searchServiceBindingName;
        private readonly string _solrServiceBindingName;

        public ServiceProxyModule() {
            _searchServiceBindingName = ConfigurationManager.AppSettings["SearchServiceBinding"] ??
                                     "BufferedBinding_ISearchService";
            _solrServiceBindingName = ConfigurationManager.AppSettings["SolrServiceBinding"] ??
                                     "BufferedBinding_ISolrService";
        }

        public override void Load() {
            Bind<SearchServiceProxy>()
                .ToSelf()
                .WithConstructorArgument("endpointConfigurationName", _searchServiceBindingName);

            Bind<SolrServiceProxy>()
                .ToSelf()
                .WithConstructorArgument("endpointConfigurationName", _solrServiceBindingName);
        }
    }
}