﻿using System.Collections.Generic;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;

namespace Medianet.Contacts.Web.Services.User
{
    public class UserService : ServiceBase, IUserService
    {
        private UserService(DBClass db) : base(db) { }

        /// <summary>
        /// Create a new instance of the <c>UserService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>UserService</c> class.</returns>
        public static UserService CreateUserService() {
            DBClass db = new DBClass();
            return new UserService(db);
        }

        /// <summary>
        /// Get's the users for this company.
        /// </summary>
        /// <returns>A List of <c>User</c> objects.</returns>
        public List<Common.Model.User> GetUsers(string debtorNumber) {
            List<Common.Model.User> users = null;
            using (DBClass db = new DBClass()) {
                UserBAL uBAL = new UserBAL(db);
                users = uBAL.GetStrongUsersByDebtorId(debtorNumber);
            }
            return users;
        }

        /// <summary>
        /// Get a user by it's ID.
        /// </summary>
        /// <param name="id">The user's id.</param>
        /// <returns>A <c>User</c> object.</returns>
        public Common.Model.User GetUserById(int id) {
            Common.Model.User user = null;
            using (DBClass db = new DBClass()) {
                UserBAL uBAL = new UserBAL(db);
                user = uBAL.GetStrongUser(id);
            }
            return user;
        }
    }
}