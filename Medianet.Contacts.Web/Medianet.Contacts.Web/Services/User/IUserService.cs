﻿using System;
using System.Collections.Generic;

namespace Medianet.Contacts.Web.Services.User
{
    public interface IUserService : IDisposable
    {
        List<Common.Model.User> GetUsers(string debtorNo);
        Common.Model.User GetUserById(int id);
    }
}
