﻿using System;
using System.Data;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels.Contact;
using Medianet.Contacts.Web.ViewModels;
using System.Collections.Generic;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.ViewModels.Dashboard;

namespace Medianet.Contacts.Web.Services.Contact
{
    public interface IContactService : IDisposable
    {
        ContactViewModel GetContact(string contactId, string outletId, RecordType rt, Common.Model.User loggedInUser, bool fillArticles);
        ContactViewModel GetContactPreview(string contactId, string outletId, RecordType rt, Common.Model.User loggedInUser);
        DataTable GetAllOutletContactHomePageDal(string outletId, string contactId);
        List<PeopleAlsoViewedViewModel> GetContactViewSummary(string contactId, string outletId,
            string subjectCodeIds, Common.Model.User loggedInUser);

        List<MediaMovementsViewModel> GetContactMediaMovements(string contactId, int pageNo, int pageSize,
            Common.Model.User currentUser);
    }
}