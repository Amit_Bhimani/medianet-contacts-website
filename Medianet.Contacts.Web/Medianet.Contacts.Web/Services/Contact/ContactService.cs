﻿using System.Data;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.Contact;
using Medianet.Contacts.Web.ProxyWrappers;
using System.Collections.Generic;
using System;
using Medianet.Contacts.Web.MediaContactsService;
using System.Configuration;
using System.Linq;
using Medianet.Contacts.Web.ViewModels.Dashboard;

namespace Medianet.Contacts.Web.Services.Contact
{
    public class ContactService : ServiceBase, IContactService
    {
        private ContactService(DBClass db) : base(db) { }

        /// <summary>
        /// Create a new instance of the <c>ContactService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>ContactService</c> class.</returns>
        public static ContactService CreateContactService()
        {
            var db = new DBClass();
            return new ContactService(db);
        }

        public ContactViewModel GetContact(string contactId, string outletId, RecordType rt, Common.Model.User loggedInUser, bool fillArticles)
        {
            ContactViewModel contactViewModel = null;

            using (var db = new DBClass())
            {
                var bal = new ContactBAL(db);
                var contactDetails = bal.GetContact(contactId, outletId, rt, loggedInUser, fillArticles);

                if (contactDetails != null)
                {
                    contactViewModel = Mapper.Map<Common.Model.Contact, ContactViewModel>(contactDetails);
                }
            }

            return contactViewModel;
        }

        public ContactViewModel GetContactPreview(string contactId, string outletId, RecordType rt, Common.Model.User loggedInUser)
        {
            ContactViewModel contactViewModel = null;

            using (var db = new DBClass())
            {
                var bal = new ContactBAL(db);
                var contactDetails = bal.GetContactPreview(contactId, outletId, rt, loggedInUser);

                if (contactDetails != null)
                {
                    contactViewModel = new ContactViewModel();
                    contactViewModel = Mapper.Map<Common.Model.Contact, ContactViewModel>(contactDetails);
                }
            }

            return contactViewModel;
        }

        public DataTable GetAllOutletContactHomePageDal(string outletId, string contactId)
        {
            using (var db = new DBClass())
            {
                var bal = new OutletContactMasterBAL(db);
                return bal.GetAllOutletContactHomePageDAL(outletId, contactId);
            }
        }

        public List<PeopleAlsoViewedViewModel> GetContactViewSummary(string contactId, string outletId, string subjectCodeIds, Common.Model.User loggedInUser)
        {
            int maxCount = int.Parse(ConfigurationManager.AppSettings["PeopleAlsoViewedCount"] ?? "3");
            var continentIds = new List<int>();
            var list = new List<MediaContactAlsoViewedDto>();
            var subjects = new List<string>();

            using (var db = new DBClass())
            {
                GeographicBAL gb = new GeographicBAL(db);
                continentIds = gb.GetContinents().Where(con => loggedInUser.DataModules.Contains(con.DataModuleId))
                    .Select(s => s.Id).ToList();
            }

            subjects = subjectCodeIds.Split(new char[] { ',' })
                .Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();

            if (subjects.Any() && continentIds.Any())
            {
                using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
                {
                    list = mediaContactsService.GetContactViewSummary(contactId, outletId, subjects.Select(int.Parse).ToList(), continentIds,
                        maxCount, loggedInUser.SessionKey);
                }
            }

            return Mapper.Map<List<MediaContactsService.MediaContactAlsoViewedDto>,
                List<PeopleAlsoViewedViewModel>>(list);
        }

        public List<MediaMovementsViewModel> GetContactMediaMovements(string contactId, int pageNo, int pageSize, Common.Model.User loggedInUser)
        {
            List<MediaMovement> model;

            using (var mcs = new MediaContactsServiceProxy())
            {
               model = mcs.GetContactMediaMovements(contactId, pageNo, pageSize, loggedInUser.SessionKey);
            }

            return Mapper.Map<List<MediaMovement>,List<MediaMovementsViewModel>>(model);
        }
    }
}