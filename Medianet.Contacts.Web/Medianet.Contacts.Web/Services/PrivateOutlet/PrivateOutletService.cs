﻿using AutoMapper;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.ViewModels.PrivateOutlet;
using System.Collections.Generic;
using System.Linq;

namespace Medianet.Contacts.Web.Services.PrivateOutlet
{
    public class PrivateOutletService : ServiceBase, IPrivateOutletService
    {
        private PrivateOutletService(DBClass db) : base(db)
        {
        }

        /// <summary>
        /// Create a new instance of the <c>PrivateOutletService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>PrivateOutletService</c> class.</returns>
        public static PrivateOutletService CreatePrivateOutletService()
        {
            var db = new DBClass();
            return new PrivateOutletService(db);
        }
        
        public PrivateOutletViewModel GetById(string outletId, string ownedBy)
        {
            var model = new OmaOutletBAL(Connection).GetPrivateOutlet(outletId, ownedBy);

            PrivateOutletViewModel result = Mapper.Map<PrivateOutletModel, PrivateOutletViewModel>(model);

            if (result.Subjects != null)
                result.SubjectIdsCsv = string.Join(",", result.Subjects.Select(m => m.Id));

            if (result.WorkingLanguages != null)
                result.WorkingLanguageIdsCsv = string.Join(",", result.WorkingLanguages.Select(m => m.Id));

            return result;
        }

        public bool Save(PrivateOutletViewModel model, string sessionKey)
        {
            return model.OutletId > 0 ? 
                new OmaOutletBAL(Connection).UpdatePrivateOutlet(Mapper.Map<PrivateOutletViewModel, PrivateOutletModel>(model), sessionKey) :
                new OmaOutletBAL(Connection).CreatePrivateOutlet(Mapper.Map<PrivateOutletViewModel, PrivateOutletModel>(model));
        }

        public bool Delete(string outletId, string ownedBy, int userId, string sessionKey)
        {
            return new OmaOutletBAL(Connection).DeletePrivateOutlet(outletId, ownedBy, userId, sessionKey) != 0;
        }

        public bool IsOwner(string outletId, string ownedBy)
        {
            var model = new OmaOutletBAL(Connection).GetPrivateOutlet(outletId, ownedBy);

            return model != null;
        }
    }
}