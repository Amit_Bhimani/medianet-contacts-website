﻿using System;
using Medianet.Contacts.Web.ViewModels.PrivateOutlet;
using System.Collections.Generic;
using Medianet.Contacts.Web.MediaContactsService;

namespace Medianet.Contacts.Web.Services.PrivateOutlet
{
    public interface IPrivateOutletService : IDisposable
    {
        PrivateOutletViewModel GetById(string outletId, string ownedBy);
        
        bool Save(PrivateOutletViewModel model, string sessionKey);

        bool Delete(string outletId, string ownedBy, int userId, string sessionKey);

        bool IsOwner(string outletId, string ownedBy);
    }
}