﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.BO;

namespace Medianet.Contacts.Web.Services.Distribution
{
    public class DistributionService: ServiceBase, IDistributionService
    {
        private DistributionService(DBClass db) : base(db) { }

        public static DistributionService Create()
        {
            DBClass db = new DBClass();
            return new DistributionService(db);
        }

        public int CreateJob(Distribute distribution)
        {
            DistributeBAL dBAL = new DistributeBAL(Connection);

            return dBAL.InsertDistributionList(distribution);
        }

        public DataSet GetEmailFax(string listIdCsv)
        {
            DistributeBAL dBAL = new DistributeBAL(Connection);

            return dBAL.GetEmailFax(listIdCsv);
        }
    }
}
