﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Contacts.Web.Common.BO;

namespace Medianet.Contacts.Web.Services.Distribution
{
    interface IDistributionService : IDisposable
    {
        int CreateJob(Distribute distribution);

        DataSet GetEmailFax(string listIdCsv);
    }
}
