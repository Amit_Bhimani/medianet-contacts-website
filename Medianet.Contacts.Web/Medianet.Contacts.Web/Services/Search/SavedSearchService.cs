﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel;
using System.Web;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.ViewModels;

using SavedSearch = Medianet.Contacts.Web.Common.Model.SavedSearch;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.ViewModels.AllSavedSearches;
using Medianet.Contacts.Web.ViewModels.Search;
using Medianet.Contacts.Web.Helpers;

namespace Medianet.Contacts.Web.Services.Search
{
    public class SavedSearchService : ServiceBase, ISavedSearchService
    {
        public const string CONTEXT_MODE_PEOPLE = "people";
        public const string CONTEXT_MODE_OUTLET = "outlet";
        public const string CONTEXT_MODE_BOTH = "both";

        private SavedSearchService(DBClass db) : base(db) { }

        /// <summary>
        /// Create a new instance of the <c>UserService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>UserService</c> class.</returns>
        public static SavedSearchService CreateSavedSearchService()
        {
            DBClass db = new DBClass();
            return new SavedSearchService(db);
        }

        /// <summary>
        /// Gets all the saved searches for the current user.
        /// Get all saved searches for a User.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <param name="debtorNo">The debtor no.</param>
        /// <returns>
        /// The list of <c>SearchOptions</c> object.
        /// </returns>
        public List<SavedSearch> Get(int userid, string debtorNo)
        {
            SavedSearchMasterBAL bal = new SavedSearchMasterBAL(Connection);
            List<SavedSearch> sOptions = bal.GetStrongSavedSearches(userid);
            return sOptions;
        }

        /// <summary>
        /// Get all saved searches for a User.
        /// </summary>
        /// <param name="userId">The current user's ID.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>List of <c>SavedSearchViewModel</c> objects.</returns>
        public List<SavedSearchViewModel> GetAllSavedSearches(int userId, string sessionKey)
        {
            List<MediaContactsService.MediaContactSavedSearch> savedSearches;

            using (var mc = new MediaContactsServiceProxy())
            {
                savedSearches = mc.GetSavedSearches(userId, sessionKey);
            }

            return PopulateSavedSearchViews(savedSearches);
        }

        public List<SavedSearchGroupedViewModel> GetGroupedSavedSearches(int userId, string sessionKey) {
            List<MediaContactsService.MediaContactSavedSearch> allSavedSearches = null;

            using (var mc = new MediaContactsServiceProxy())
            {
                allSavedSearches = mc.GetSavedSearches(userId, sessionKey);
            }

            List<SavedSearchViewModel> savedSearches = PopulateSavedSearchViews(allSavedSearches);

            if (savedSearches.Any())
            {
                List<SavedSearchGroupedViewModel> groups = (from l in savedSearches
                                                            group l by l.Group.Id into g
                                                      select new SavedSearchGroupedViewModel { GroupId = g.Key, Lists = g.ToList() })
                                                      .OrderBy(g => g.Lists != null && g.Lists.Count > 0 ? g.Lists.First().Group.Name : "")
                                                      .ToList();

                return groups;
            }

            return null;
        }

        /// <summary>
        /// Get the most recent saved searches.
        /// </summary>
        /// <param name="userId">The current user's ID.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public List<SavedSearchViewModel> GetRecentSavedSearches(int userId, string sessionKey)
        {
            List<MediaContactsService.MediaContactSavedSearch> savedSearches;

            using (var mc = new MediaContactsServiceProxy())
            {
                savedSearches = mc.GetRecentSavedSearches(userId, sessionKey);
            }

            return PopulateSavedSearchViews(savedSearches);
        }

        private List<SavedSearchViewModel> PopulateSavedSearchViews(List<MediaContactsService.MediaContactSavedSearch> savedSearches)
        {
            List<SavedSearchViewModel> vsavedSearches = null;
            vsavedSearches = (from l in savedSearches
                              select new SavedSearchViewModel
                              {
                                  Name = l.Name,
                                  SearchId = l.Id,
                                  IsPrivate = l.IsPrivate,
                                  Criteria = l.SearchCriteria,
                                  IsQuickSearch = !l.SearchCriteria.StartsWith("&AdvancedSearch"),
                                  Group = new GroupViewModel
                                  {
                                      Id = l.GroupId,
                                      Name = l.Group.Name,
                                      Type = (GroupType)l.Group.GroupType,
                                  },
                                  UserName = $"{l.OwnerUser.FirstName} {l.OwnerUser.LastName}".TrimEnd(),
                                  UserId = l.OwnerUserId,
                                  CreatedDate = l.CreatedDate,
                                  UpdatedDate = l.LastModifiedDate,
                                  ResultsCount = l.ResultsCount
                              }).ToList<SavedSearchViewModel>();

            return vsavedSearches;
        }

        /// <summary>
        /// Gets the SearchOptions of a saved search.
        /// </summary>
        /// <param name="id">The Id of the saved search.</param>
        /// <param name="sessionKey">The session key of the current user.</param>
        /// <returns>/// The SearchOptions of the saved search./// </returns>
        public SearchOptions GetSavedSearchOptions(int id, string sessionKey)
        {
            SearchOptions so = null;
            MediaContactsService.MediaContactSavedSearch search;

            using (var mc = new MediaContactsServiceProxy())
            {
                try
                {
                    search = mc.GetSavedSearch(id, sessionKey);
                    so = ParseSavedSearch(search.Context, search.SearchCriteria);
                    so.SearchName = search.Name;
                }
                catch (FaultException<MediaContactsService.AccessDeniedFault>)
                {
                    throw new Exception(string.Format("Access to SavedSearchId {0} denied", id));
                }
            }

            return so;
        }

        /// <summary>
        /// Gets the recent search options.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="sessionKey">The session key of the current user.</param>
        /// <returns>The <c>SearchOptions</c> object.</returns>
        public SearchOptions GetRecentSearchOptions(SearchLogViewModel model, string sessionKey)
        {
            try
            {
                MediaContactSearchContext context = model.SearchContext.ToLower() == "p" ? MediaContactSearchContext.Contact :
                    model.SearchContext.ToLower() == "o" ? MediaContactSearchContext.Outlet : MediaContactSearchContext.Both;

                return ParseSavedSearch(context, model.SearchCriteria);
            }
            catch (FaultException<MediaContactsService.AccessDeniedFault>)
            {
                throw new Exception(string.Format("Access to RecentSearchId {0} denied", model.Id));
            }
        }
        
        /// <summary>
        /// Inserts a search into the database.
        /// </summary>
        /// <param name="s">The search view model.</param>
        /// <param name="userid">The current userid.</param>
        /// <returns>
        /// ID of the search.
        /// </returns>
        public int Create(SavedSearchViewModel s, SearchOptions so, int userid)
        {
            SavedSearch search = new SavedSearch
            {
                Name = s.Name,
                Criteria = SearchBAL.GetSearchCriteria(so),
                Query = "",
                UserId = userid,
                Visibility = s.IsPrivate,
                Status = true,
                ModifiedDate = DateTime.Now,
                CreatedDate = DateTime.Now,
                Context = so.ContextDBCode,
                ResultsCount = s.ResultsCount,
                Group = new GroupBase
                {
                    Id = s.Group.Id
                }
            };

            SearchBAL bal = new SearchBAL(Connection);
            int result = bal.Insert(search);
            return result;
        }

        public int Delete(int searchId)
        {
            SavedSearchMasterBAL bal = new SavedSearchMasterBAL(Connection);
            return bal.Delete(searchId);
        }

        public int DeleteGroupSearch(int groupid, int userid)
        {
            SavedSearchMasterBAL bal = new SavedSearchMasterBAL(Connection);
            return bal.DeleteGroupSearch(groupid, userid);
        }

        public SavedSearchViewModel GetSavedSearchById(int id, string sessionKey)
        {
            SavedSearchViewModel savedSearchViewModel = null;

            using (var mc = new MediaContactsServiceProxy())
            {
                try
                {
                    var search = mc.GetSavedSearch(id, sessionKey);

                    savedSearchViewModel = new SavedSearchViewModel()
                    {
                        SearchId = search.Id,
                        Name = search.Name,
                        IsPrivate = search.IsPrivate,
                        UserId = search.OwnerUserId
                    };
                }
                catch (FaultException<MediaContactsService.AccessDeniedFault>)
                {
                    throw new Exception(string.Format("Access to SavedSearchId {0} denied", id));
                }
                return savedSearchViewModel;
            }
        }

        public void Edit(SavedSearchViewModel model)
        {
            SavedSearch search = new SavedSearch
            {
                Id = model.SearchId,
                Name = model.Name,
                Visibility = model.IsPrivate
            };

            SearchBAL bal = new SearchBAL(Connection);
            bal.Update(search);
        }

        private SearchOptions ParseSavedSearch(MediaContactSearchContext pSearchContext, string pSearchCriteria)
        {
            SearchOptions sOptions;
            NameValueCollection values = HttpUtility.ParseQueryString(pSearchCriteria);

            // Checking both AdvancedSearch and QuickSearch might seem redundant
            // but it was necessary to make it backward compatible.
            if (values["QuickSearch"] == "true" && values["AdvancedSearch"] != "true")
            {
                SearchOptionsQuick so = new SearchOptionsQuick();
                so.Type = SearchType.Quick;

                if (values["SearchAustralia"] == null && values["SearchNewZealand"] == null && values["SearchRestOfWorld"] == null)
                {
                    // For old saved searches that don't have these values select all countries.
                    so.SearchAustralia = true;
                    so.SearchNewZealand = true;
                    so.SearchAllCountries = true;
                }
                else
                {
                    so.SearchAustralia = ParseBool(values["SearchAustralia"], so.SearchAustralia);
                    so.SearchNewZealand = ParseBool(values["SearchNewZealand"], so.SearchNewZealand);
                    so.SearchAllCountries = ParseBool(values["SearchRestOfWorld"], so.SearchAllCountries);
                }

                so.LocationList = SearchOptionsQuick.ParseLocation(values["LocationRegionWiseContact"]);
                so.PositionList = string.IsNullOrWhiteSpace(values["RoleContact"]) ? new List<int>() : values["RoleContact"].ParseIntCSV();
                so.MediaTypeList = string.IsNullOrWhiteSpace(values["MediaContact"]) ? new List<int>() : values["MediaContact"].ParseIntCSV();
                so.SubjectList = string.IsNullOrWhiteSpace(values["SubjectContact"]) ? new List<int>() : values["SubjectContact"].ParseIntCSV();
                so.SubjectGroupList = string.IsNullOrWhiteSpace(values["SubjectGroupContact"]) ? new List<int>() : values["SubjectGroupContact"].ParseIntCSV();
                so.SearchText = StringHelper.CleanQuery(ParseText(values["ContactContact"]));

                so.SelectedId = ParseText(values["SelectedId"]);
                if (string.IsNullOrWhiteSpace(so.SelectedId))
                    so.SelectedRecordType = RecordType.Unknown;
                else
                    so.SelectedRecordType = (RecordType)Enum.Parse(typeof(RecordType), values["SelectedRecordType"]);

                sOptions = so;
            }
            else if (values["OutletSearch"] == "true")
            {
                SearchOptionsOutlet so = new SearchOptionsOutlet();
                string outletType;

                so.Type = SearchType.Outlet;
                so.OutletID = ParseText(values["OutletId"]);
                outletType = ParseText(values["OutletType"]);

                if (string.IsNullOrEmpty(outletType))
                {
                    // Old Outlet searches don't have the OutletType so guess.
                    int id;

                    if (int.TryParse(so.OutletID, out id))
                        so.OutletType = Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.OmaOutlet;
                    else
                        so.OutletType = Medianet.Contacts.Wcf.DataContracts.DTO.RecordType.MediaOutlet;
                }
                else
                    so.OutletType = (Medianet.Contacts.Wcf.DataContracts.DTO.RecordType)Enum.Parse(typeof(Medianet.Contacts.Wcf.DataContracts.DTO.RecordType), outletType);

                sOptions = so;
            }
            else
            {
                SearchOptionsAdvanced so = new SearchOptionsAdvanced();
                so.Type = SearchType.Advanced;

                // search text
                so.SearchText = StringHelper.CleanQuery(ParseText(values["SearchText"]));

                // People region.
                so.FirstName = ParseText(values["ContactFirstName"]);
                so.LastName = ParseText(values["ContactLastName"]);
                so.FullName = ParseText(values["ContactFullName"]);
                so.JobTitle = ParseText(values["JobTitle"]);
                so.PositionList = string.IsNullOrWhiteSpace(values["RoleContact"]) ? new List<int>() : values["RoleContact"].ParseIntCSV();
                so.ContactSubjectList = string.IsNullOrWhiteSpace(values["SubjectContact"]) ? new List<int>() : values["SubjectContact"].ParseIntCSV();
                so.ContactSubjectGroupList = string.IsNullOrWhiteSpace(values["SubjectGroupContact"]) ? new List<int>() : values["SubjectGroupContact"].ParseIntCSV();
                so.PrimaryNewsContactOnly = ParseBool(values["PrimaryNewsContactOnly"], so.PrimaryNewsContactOnly);

                so.ContactInfluencerScore = ParseText(values["ContactInfluencerScore"]);

                // Contact Geographic region.
                so.ContactContinentIDList = string.IsNullOrWhiteSpace(values["ContinentGeoGraphicContact"]) ? new List<int>() : values["ContinentGeoGraphicContact"].ParseIntCSV();
                so.ContactCountryIDList = string.IsNullOrWhiteSpace(values["CountryGeographicContact"]) ? new List<int>() : values["CountryGeographicContact"].ParseIntCSV();
                so.ContactStateIDList = string.IsNullOrWhiteSpace(values["StateGeographicContact"]) ? new List<int>() : values["StateGeographicContact"].ParseIntCSV();
                so.ContactCityIDList = string.IsNullOrWhiteSpace(values["CityGeographicContact"]) ? new List<int>() : values["CityGeographicContact"].ParseIntCSV();
                so.ContactPostCodeList = string.IsNullOrWhiteSpace(values["PostCodeGeographicContact"]) ? new List<string>() : values["PostCodeGeographicContact"].ParseStringCSV();

                // Outlet region.
                so.OutletName = ParseText(values["OutletOutlet"]);
                so.MediaTypeList = string.IsNullOrWhiteSpace(values["MediaOutlet"]) ? new List<int>() : values["MediaOutlet"].ParseIntCSV();
                so.OutletSubjectList = string.IsNullOrWhiteSpace(values["SubjectOutlet"]) ? new List<int>() : values["SubjectOutlet"].ParseIntCSV();
                so.OutletSubjectGroupList = string.IsNullOrWhiteSpace(values["SubjectGroupOutlet"]) ? new List<int>() : values["SubjectGroupOutlet"].ParseIntCSV();
                so.OutletFrequencyList = string.IsNullOrWhiteSpace(values["FrequencyOutlet"]) ? new List<int>() : values["FrequencyOutlet"].ParseIntCSV();
                int retVal;
                so.OutletCirculationMin = !string.IsNullOrWhiteSpace(values["CirculationMinOutlet"]) && int.TryParse(values["CirculationMinOutlet"], out retVal) ? retVal : so.OutletCirculationMin;

                so.OutletCirculationMax = !string.IsNullOrWhiteSpace(values["CirculationMaxOutlet"]) && int.TryParse(values["CirculationMaxOutlet"], out retVal) ? retVal : so.OutletCirculationMax; ;

                so.OutletLanguageList = string.IsNullOrWhiteSpace(values["LanguageOutlet"]) ? new List<int>() : values["LanguageOutlet"].ParseIntCSV();
                so.OutletStationFrequencyList = string.IsNullOrWhiteSpace(values["StationFrequencyOutlet"]) ? new List<string>() : values["StationFrequencyOutlet"].ParseStringCSV();

                // Outlet Geographic region.
                so.OutletContinentIDList = string.IsNullOrWhiteSpace(values["ContinentGeoGraphic"]) ? new List<int>() : values["ContinentGeoGraphic"].ParseIntCSV();
                so.OutletCountryIDList = string.IsNullOrWhiteSpace(values["CountryGeographic"]) ? new List<int>() : values["CountryGeographic"].ParseIntCSV();
                so.OutletStateIDList = string.IsNullOrWhiteSpace(values["StateGeographic"]) ? new List<int>() : values["StateGeographic"].ParseIntCSV();
                so.OutletCityIDList = string.IsNullOrWhiteSpace(values["CityGeographicOutlet"]) ? new List<int>() : values["CityGeographicOutlet"].ParseIntCSV();
                so.OutletPostCodeList = string.IsNullOrWhiteSpace(values["PostCodeGeographicOutlet"]) ? new List<string>() : values["PostCodeGeographicOutlet"].ParseStringCSV();
                so.IncludeMetroOutlets = ParseBool(values["IncludeMetroOutlets"], so.IncludeMetroOutlets);
                so.IncludeRegionalOutlets = ParseBool(values["IncludeRegionalOutlets"], so.IncludeRegionalOutlets);
                so.IncludeSuburbanOutlets = ParseBool(values["IncludeSuburbanOutlets"], so.IncludeSuburbanOutlets);

                // Notes region.
                so.SystemNotes = ParseText(values["SystemNotes"]);
                so.MyNotes = ParseText(values["MyNotes"]);

                // Communication Preferences region.
                so.PrefersEmail = ParseBool(values["EmailChecked"], so.PrefersEmail);
                so.PrefersFax = ParseBool(values["FaxChecked"], so.PrefersFax);
                so.PrefersMail = ParseBool(values["MailChecked"], so.PrefersMail);
                so.PrefersPhone = ParseBool(values["PrefersPhoneChecked"], so.PrefersPhone);
                so.PrefersMobile = ParseBool(values["PrefersMobileChecked"], so.PrefersMobile);
                so.PrefersTwitter = ParseBool(values["PrefersTwitterChecked"], so.PrefersTwitter);
                so.PrefersLinkedIn = ParseBool(values["PrefersLinkedInChecked"], so.PrefersLinkedIn);
                so.PrefersFacebook = ParseBool(values["PrefersFacebookChecked"], so.PrefersFacebook);
                so.PrefersInstagram = ParseBool(values["PrefersInstagramChecked"], so.PrefersInstagram);
                so.ShowOnlyWithEmail = ParseBool(values["EmailAddressChecked"], so.ShowOnlyWithEmail);
                so.ShowOnlyWithFax = ParseBool(values["FaxNumberChecked"], so.ShowOnlyWithFax);
                so.ShowOnlyWithPostalAddress = ParseBool(values["PostalAddressChecked"], so.ShowOnlyWithPostalAddress);

                // Show only results with section.
                so.ShowOnlyWithTwitter = ParseBool(values["TwitterChecked"], so.ShowOnlyWithTwitter);
                so.ShowOnlyWithLinkedIn = ParseBool(values["LinkedinChecked"], so.ShowOnlyWithLinkedIn);
                so.ShowOnlyWithFacebook = ParseBool(values["FacebookChecked"], so.ShowOnlyWithFacebook);
                so.ShowOnlyWithInstagram = ParseBool(values["InstagramChecked"], so.ShowOnlyWithInstagram);
                so.ShowOnlyWithYouTube = ParseBool(values["YouTubeChecked"], so.ShowOnlyWithYouTube);
                so.ShowOnlyWithSnapchat = ParseBool(values["SnapchatChecked"], so.ShowOnlyWithSnapchat);

                // Lists section.
                so.ListList = string.IsNullOrWhiteSpace(values["WithinListId"]) ? new List<int>() : values["WithinListId"].ParseIntCSV();

                // Exclusion section.
                if (ParseBool(values["MediaDirectoryRecords"], false))
                    so.RecordsToShow = SearchOptionsAdvanced.SHOW_RECORDS_MEDIA_DIR;
                else if (ParseBool(values["PrivateRecords"], false))
                    so.RecordsToShow = SearchOptionsAdvanced.SHOW_RECORDS_PRIVATE;
                else
                    so.RecordsToShow = SearchOptionsAdvanced.SHOW_RECORDS_ALL;

                so.ExcludeKeywords = ParseText(values["ExcludeKeyword"]);
                so.ExcludeOPS = ParseBool(values["ExcludeOPS"], so.ExcludeOPS);
                so.ExcludeFinanceInst = ParseBool(values["ExcludeFinance"], so.ExcludeFinanceInst);
                so.ExcludePoliticians = ParseBool(values["ExcludePoliticians"], so.ExcludePoliticians);
                so.ExcludeOutOfOffice = ParseBool(values["ExcludeOutOfOffice"], so.ExcludeOutOfOffice);

                sOptions = so;
            }

            // The context on the database is either o (outlet), b (both) or p (people).
            if (sOptions.Type == SearchType.Advanced)
                sOptions.Context = SearchContext.Both;
            else
            {
                if (pSearchContext == MediaContactSearchContext.Outlet)
                    sOptions.Context = SearchContext.Outlet;
                else if (pSearchContext == MediaContactSearchContext.Both)
                    sOptions.Context = SearchContext.Both;
                else
                    sOptions.Context = SearchContext.People;
            }

            return sOptions;
        }

        private static string ParseText(string pValue)
        {
            if (pValue == null)
                return string.Empty;
            else
                return pValue;
        }

        private static bool ParseBool(string pValue, bool pDefault)
        {
            if (pValue == null)
                return pDefault;
            else
                return (pValue.ToLower() == "true");
        }
    }
}