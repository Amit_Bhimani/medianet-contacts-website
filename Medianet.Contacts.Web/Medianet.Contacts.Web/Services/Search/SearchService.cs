﻿using AutoMapper;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.ViewModels.Search;
using Medianet.Contacts.Web.MediaContactsService;
using System.Collections.Generic;

namespace Medianet.Contacts.Web.Services.Search
{
    public class SearchService :ISearchService
    {

        public static SearchService CreateSearchService() 
        {
            return new SearchService();
        }

        public void LogSearch(SearchLogViewModel model, string sessionKey)
        {
            var entity = Mapper.Map<SearchLogViewModel,MediaContactSearchLog>(model);

            using (var mc = new MediaContactsServiceProxy())
            {
                mc.LogSearch(entity, sessionKey);
            } 
        }

        public List<SearchLogViewModel> GetRecentSearchList(int maxRecords, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                return Mapper.Map<List<MediaContactSearchLog>, List<SearchLogViewModel>>(mc.GetRecentSearchList(maxRecords, sessionKey));
            }
        }

        public SearchLogViewModel GetRecentSearchById(int searchLogId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                return Mapper.Map<MediaContactSearchLog, SearchLogViewModel>(mc.GetRecentSearchById(searchLogId, sessionKey));
            }
        }

        public void Dispose()
        {
        }
    }
}