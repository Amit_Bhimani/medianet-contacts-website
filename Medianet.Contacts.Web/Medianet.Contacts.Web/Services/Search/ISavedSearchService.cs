﻿using System;
using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels;
using SavedSearch = Medianet.Contacts.Web.Common.Model.SavedSearch;
using Medianet.Contacts.Web.ViewModels.AllSavedSearches;

namespace Medianet.Contacts.Web.Services.Search
{
    public interface ISavedSearchService : IDisposable
    {
        /// <summary>
        /// Gets all the saved searches for the current user.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <param name="debtorNo">The debtor no.</param>
        /// <returns>The list of <c>SearchOptions</c> object.</returns>
        List<SavedSearch> Get(int userid, string debtorNo);

        List<SavedSearchViewModel> GetAllSavedSearches(int userId, string sessionKey);

        /// <summary>
        /// Get the most recent saved searches.
        /// </summary>
        /// <param name="userId">The current user's ID.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        List<SavedSearchViewModel> GetRecentSavedSearches(int userId, string sessionKey);

        /// <summary>
        /// Gets the saved search options.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="sessionKey">The session key of the current user.</param>
        /// <returns>The <c>SearchOptions</c> object.</returns>
        SearchOptions GetSavedSearchOptions(int id, string sessionKey);

        /// <summary>
        /// Gets the recent search options.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="sessionKey">The session key of the current user.</param>
        /// <returns>The <c>SearchOptions</c> object.</returns>
        SearchOptions GetRecentSearchOptions(ViewModels.Search.SearchLogViewModel model, string sessionKey);

        /// <summary>
        /// Creates the saved search.
        /// </summary>
        /// <param name="s">The <c>SearchViewModel</c>.</param>
        /// <param name="so">The <c>SearchOptions</c> object.</param>
        /// <param name="userid">The current userid.</param>
        /// <returns>
        /// The ID of the new saved search.
        /// </returns>
        int Create(SavedSearchViewModel s, SearchOptions so, int userid);
        int Delete(int searchId);
        int DeleteGroupSearch(int groupid, int userid);
        List<SavedSearchGroupedViewModel> GetGroupedSavedSearches(int userId, string sessionKey);
        SavedSearchViewModel GetSavedSearchById(int id, string sessionKey);
        void Edit(SavedSearchViewModel model);
    }
}