﻿using System;
using Medianet.Contacts.Web.ViewModels.Search;
using System.Collections.Generic;

namespace Medianet.Contacts.Web.Services.Search
{
    public interface ISearchService : IDisposable
    {
        void LogSearch(SearchLogViewModel model, string sessionKey);

        List<SearchLogViewModel> GetRecentSearchList(int maxRecords, string sessionKey);

        SearchLogViewModel GetRecentSearchById(int searchLogId, string sessionKey);
    }
}

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using MediaPeople.Common.Model;
//using MediaPeople.ViewModel;
//using MediaPeople.Common.BO;
//using Medianet.Contacts.Wcf.DataContracts.DTO;

//namespace MediaPeople.Servicess
//{
//    /// <summary>
//    /// Exposes methods for searching.
//    /// </summary>
//    public interface ISearchService : IDisposable
//    {
//        /// <summary>
//        /// Total number of results from last search.
//        /// </summary>
//        int TotalResults { get; set; }

//        /// <summary>
//        /// Searches the specified <c>SearchOptions</c> object.
//        /// </summary>
//        /// <param name="so">The <c>SearchOptions</c> object.</param>
//        /// <param name="sortCol">The sort column.</param>
//        /// <param name="sortDir">The sort direction.</param>
//        /// <param name="startPos">The start position to return results from.</param>
//        /// <param name="count">The number of results to return.</param>
//        /// <param name="pinned">The pinned items.</param>
//        /// <param name="deleted">The deleted items.</param>
//        /// <param name="selected">The selected items.</param>
//        /// <param name="newSearch">Perform a new search if <c>true</c>.</param>
//        /// <param name="userid">The current user's id.</param>
//        /// <returns>List of <c>SearchResult</c> objects.</returns>
//        List<SearchResult> Search(
//            SearchOptions so,
//            SortColumn sortCol,
//            SortDirection sortDir,
//            bool newSearch,
//            int userid,
//            int? startPos = null,
//            int? count = null,
//            List<RecordIdentifier> pinned = null,
//            List<RecordIdentifier> deleted = null,
//            List<RecordIdentifier> selected = null
//        );

//        /// <summary>
//        /// Fetches the range.
//        /// </summary>
//        /// <param name="results">The results.</param>
//        /// <param name="startPos">The start pos.</param>
//        /// <param name="count">The count.</param>
//        /// <param name="pinned">The pinned.</param>
//        /// <param name="deleted">The deleted.</param>
//        /// <param name="unselected">The unselected.</param>
//        /// <param name="userid">The userid.</param>
//        /// <returns></returns>
//        List<SearchResult> FetchRange(
//            List<SearchResult> results,
//            int startPos,
//            int count,
//            List<RecordIdentifier> pinned,
//            List<RecordIdentifier> deleted,
//            List<RecordIdentifier> unselected,
//            int userid
//        );

//        /// <summary>
//        /// Returns results for the auto suggest.
//        /// </summary>
//        /// <param name="context">The context.</param>
//        /// <param name="searchText">The search text.</param>
//        /// <param name="searchAustralia">if set to <c>true</c> [search australia].</param>
//        /// <param name="searchNewZealand">if set to <c>true</c> [search new zealand].</param>
//        /// <param name="searchAllCountries">if set to <c>true</c> [search all countries].</param>
//        /// <param name="maxResults">The max results.</param>
//        /// <param name="userid">The userid.</param>
//        /// <returns>List of <c>SearchResult</c> objects.</returns>
//        List<AutoSuggest> AutoSuggest(
//            SearchContext context,
//            string searchText,
//            bool searchAustralia,
//            bool searchNewZealand,
//            bool searchAllCountries,
//            int maxResults,
//            int userid
//        );


//        /// <summary>
//        /// Searches the with expanded outlets.
//        /// </summary>
//        /// <param name="SearchOptions">The search options.</param>
//        /// <param name="sortColumn">The sort column.</param>
//        /// <param name="sortDirection">The sort direction.</param>
//        /// <param name="PinnedItems">The pinned items.</param>
//        /// <param name="DeletedItems">The deleted items.</param>
//        /// <param name="UnselectedItems">The unselected items.</param>
//        /// <param name="userid">The userid.</param>
//        /// <returns>List of <c>SearchResult</c> objects.</returns>
//        List<SearchResult> SearchWithExpandedOutlets(
//            SearchOptions SearchOptions,
//            SortColumn sortColumn,
//            SortDirection sortDirection,
//            List<RecordIdentifier> PinnedItems,
//            List<RecordIdentifier> DeletedItems,
//            List<RecordIdentifier> UnselectedItems,
//            int userid
//        );

//        /// <summary>
//        /// Searches the with expanded outlets.
//        /// </summary>
//        /// <param name="so">The current outlet SearchOptions.</param>
//        /// <param name="sortCol">The sort column.</param>
//        /// <param name="sortDir">The sort direction.</param>
//        /// <param name="userid">The current user id.</param>
//        /// <returns>A list of contact <c>RecordIdentifier</c> objects.</returns>
//        List<SearchResult> SearchWithExpandedOutlets(
//                SearchOptions so,
//                List<RecordIdentifier> deleted,
//                SortColumn sortCol,
//                SortDirection sortDir,
//                int userid
//        );


//        /// <summary>
//        /// Searches the with expanded outlets.
//        /// </summary>
//        /// <param name="so">The <c>SearchOptions</c> object.</param>
//        /// <param name="sortCol">The sort column.</param>
//        /// <param name="sortDir">The sort direction.</param>
//        /// <param name="startPos">The start position to return results from.</param>
//        /// <param name="count">The number of results to return.</param>
//        /// <param name="pinned">The pinned items.</param>
//        /// <param name="deleted">The deleted items.</param>
//        /// <param name="selected">The selected items.</param>
//        /// <param name="userid">The current user's id.</param>
//        /// <returns>List of <c>SearchResult</c> objects.</returns>
//        List<SearchResult> SearchWithExpandedOutlets(
//            SearchOptions so,
//            SortColumn sortCol,
//            SortDirection sortDir,
//            int startPos,
//            int count,
//            List<RecordIdentifier> pinned,
//            List<RecordIdentifier> deleted,
//            List<RecordIdentifier> selected,
//            int userid
//        );
//    }
//}
