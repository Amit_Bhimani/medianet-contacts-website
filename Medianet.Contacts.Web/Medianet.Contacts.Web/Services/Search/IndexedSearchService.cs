﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using MediaPeople.Common.Model;
//using MediaPeople.Common.Caching;
//using MediaPeople.ViewModel;
//using MediaPeople.Common.BO;
//using Medianet.Contacts.Wcf.DataContracts.DTO;

//namespace MediaPeople.Servicess
//{
//    public class IndexedSearchService : ISearchService
//    {
//        /// <summary>
//        /// Total number of results from last search.
//        /// </summary>
//        public int TotalResults { get; set; }

//        private IndexedSearchService() { }

//        /// <summary>
//        /// Create a new instance of the <c>IndexedSearchService</c> class.
//        /// </summary>
//        /// <returns>A new instance of the <c>IndexedSearchService</c> class.</returns>
//        public static IndexedSearchService CreateIndexedSearchService() {
//            return new IndexedSearchService();
//        }

//        public void Dispose() {
//        }

//        public SearchOptions GetSavedSearchOptions(int id, int userid, string debtorNo) {
//            throw new NotImplementedException();
//        }

//        public List<SearchResult> Search(
//            SearchOptions so,
//            SortColumn sortCol,
//            SortDirection sortDir,
//            bool newSearch,
//            int userid,
//            int? startPos = null,
//            int? count = null,
//            List<RecordIdentifier> pinned = null,
//            List<RecordIdentifier> deleted = null,
//            List<RecordIdentifier> selected = null
//        ) {
//            throw new NotImplementedException();
//        }

//        private List<SearchResult> Search(
//            SearchOptions so,
//            SortColumn sortCol,
//            SortDirection sortDir,
//            bool newSearch,
//            int userid
//        ) {
//            throw new NotImplementedException();
//        }

//        public List<SearchResult> SearchWithExpandedOutlets(
//            SearchOptions so,
//            SortColumn sortCol,
//            SortDirection sortDir,
//            int startPos,
//            int count,
//            List<RecordIdentifier> pinned,
//            List<RecordIdentifier> deleted,
//            List<RecordIdentifier> unselected,
//            int userid
//        ) {
//            throw new NotImplementedException();
//        }

//        public List<AutoSuggest> AutoSuggest(
//            SearchContext context,
//            string searchText,
//            bool searchAustralia,
//            bool searchNewZealand,
//            bool searchAllCountries,
//            int maxResults,
//            int userid
//        ) {
//            throw new NotImplementedException();
//        }

//        /// <summary>
//        /// Inserts a search into the database.
//        /// </summary>
//        /// <param name="s">The search view model.</param>
//        /// <param name="so">The <c>SearchOptions</c> object.</param>
//        /// <param name="userid">The current userid.</param>
//        /// <returns>
//        /// ID of the search.
//        /// </returns>
//        public int Create(SavedSearchViewModel s, SearchOptions so, int userid) {
//            throw new NotImplementedException();
//        }


//        /// <summary>
//        /// Searches the with expanded outlets.
//        /// </summary>
//        /// <param name="SearchOptions">The search options.</param>
//        /// <param name="sortColumn">The sort column.</param>
//        /// <param name="sortDirection">The sort direction.</param>
//        /// <param name="PinnedItems">The pinned items.</param>
//        /// <param name="DeletedItems">The deleted items.</param>
//        /// <param name="UnselectedItems">The unselected items.</param>
//        /// <param name="userid">The userid.</param>
//        /// <returns>
//        /// List of <c>SearchResult</c> objects.
//        /// </returns>
//        public List<SearchResult> SearchWithExpandedOutlets(
//            SearchOptions SearchOptions,
//            SortColumn sortColumn,
//            SortDirection sortDirection,
//            List<RecordIdentifier> PinnedItems,
//            List<RecordIdentifier> DeletedItems,
//            List<RecordIdentifier> UnselectedItems,
//            int userid
//        ) {
//            throw new NotImplementedException();
//        }

//        /// <summary>
//        /// Fetches the range.
//        /// </summary>
//        /// <param name="results">The results.</param>
//        /// <param name="startPos">The start pos.</param>
//        /// <param name="count">The count.</param>
//        /// <param name="pinned">The pinned.</param>
//        /// <param name="deleted">The deleted.</param>
//        /// <param name="unselected">The unselected.</param>
//        /// <param name="userid">The userid.</param>
//        /// <returns></returns>
//        public List<SearchResult> FetchRange(
//            List<SearchResult> results,
//            int startPos,
//            int count,
//            List<RecordIdentifier> pinned,
//            List<RecordIdentifier> deleted,
//            List<RecordIdentifier> unselected,
//            int userid
//        ) {
//            throw new NotImplementedException();
//        }

//        public List<SearchResult> SearchWithExpandedOutlets(
//            SearchOptions SearchOptions,
//            List<RecordIdentifier> deleted,
//            SortColumn sortColumn,
//            SortDirection sortDirection,
//            int userid
//        ) {
//            throw new NotImplementedException();
//        }

//        public List<MediaPeople.Common.Model.SavedSearch> Get(int userid, string debtorNo) {
//            throw new NotImplementedException();
//        }

//    }
//}