﻿using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Services.Geographic
{
    public class GeographicService : ServiceBase
    {
        #region Fields & Properties

        private GeographicBAL _b;
        private GeographicBAL _BAL
        {
            get
            {
                if (_b == null)
                    _b = new GeographicBAL(Connection);

                return _b;
            }
        }

        #endregion

        public GeographicService(DBClass db) : base(db) { }

        public static GeographicService CreateGeographicService()
        {
            var db = new DBClass();

            return new GeographicService(db);
        }

        public List<Continent> GetContinents()
        {
            return _BAL.GetContinents();
        }

        public List<Country> GetCountries()
        {
            return _BAL.GetCountries();
        }

        public List<Country> GetCountries(int continentId)
        {
            return _BAL.GetCountries(continentId);
        }

        public Country GetCountry(int countryId)
        {
            return _BAL.GetCountry(countryId);
        }

        public List<State> GetStates(int countryId)
        {
            return _BAL.GetStates(countryId);
        }

        public List<City> GetCities(int? continentId, int? countryId, int? stateId = null, string name = null)
        {
            return _BAL.GetCities(continentId, countryId, stateId, name);
        }

        public List<City> GetCities(int? countryId, int? stateId)
        {
            return _BAL.GetCities(null, countryId, stateId, null);
        }
    }
}