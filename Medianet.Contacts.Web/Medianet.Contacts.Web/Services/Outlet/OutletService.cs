﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.Outlet;
using Medianet.Contacts.Web.ProxyWrappers;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Web.Hosting;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.ViewModels.Dashboard;

namespace Medianet.Contacts.Web.Services.Outlet
{
    public class OutletService : ServiceBase, IOutletService
    {
        #region Fields & Properties

        private const int MediaOwnerTypeId = 2;

        #endregion

        private OutletService(DBClass db) : base(db)
        {
        }

        /// <summary>
        /// Create a new instance of the <c>OutletService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>OutletService</c> class.</returns>
        public static OutletService CreateOutletService()
        {
            var db = new DBClass();
            return new OutletService(db);
        }

        public OutletViewModel GetOutlet(string outletId, RecordType recordType, Common.Model.User loggedInUser)
        {
            OutletViewModel outletViewModel = null;

            var outletDetails = new OutletBAL(Connection).GetOutlet(outletId, recordType, loggedInUser);
            if (outletDetails != null)
            {
                outletViewModel = new OutletViewModel();
                outletViewModel = Mapper.Map<OutletDetails, OutletViewModel>(outletDetails);

                if (recordType == RecordType.MediaOutlet)
                {
                    using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
                    {
                        outletViewModel.SyndicatingToMediaOutlets =
                            mediaContactsService.GetSyndicatingToMediaOutlets(outletId, loggedInUser.SessionKey);

                        outletViewModel.SyndicatedFromMediaOutlets =
                            mediaContactsService.GetSyndicatedFromMediaOutlets(outletId, loggedInUser.SessionKey);

                        outletViewModel.RelatedMediaOutlets =
                            mediaContactsService.GetRelatedMediaOutlets(outletId, loggedInUser.SessionKey);

                        if (outletViewModel.MediaType != null && outletViewModel.MediaType.Id == MediaOwnerTypeId)
                            outletViewModel.MediaOwnerOutlets =
                                mediaContactsService.GetMediaOwnerBusinesses(outletId, loggedInUser.SessionKey);
                    }
                }
                else
                {
                    outletViewModel.SyndicatingToMediaOutlets = new List<MediaOutletPartial>();
                    outletViewModel.SyndicatedFromMediaOutlets = new List<MediaOutletPartial>();
                    outletViewModel.MediaOwnerOutlets = new List<MediaOutletPartial>();
                    outletViewModel.RelatedMediaOutlets = new List<MediaOutletPartial>();
                }
            }

            return outletViewModel;
        }

        public OutletViewModel GetOutletPreview(string outletId, RecordType recordType, Common.Model.User loggedInUser)
        {
            OutletViewModel outletViewModel = null;

            var bal = new OutletBAL(Connection);

            var outletDetails = bal.GetOutletPreview(outletId, recordType, loggedInUser);

            if (outletDetails != null)
            {
                outletViewModel = new OutletViewModel();
                outletViewModel = Mapper.Map<OutletDetails, OutletViewModel>(outletDetails);

                using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
                {
                    if (outletViewModel.MediaType != null && outletViewModel.MediaType.Id == MediaOwnerTypeId)
                        outletViewModel.MediaOwnerOutlets = mediaContactsService.GetMediaOwnerBusinesses(outletId, loggedInUser.SessionKey);
                }
            }

            return outletViewModel;
        }
        
        public string GetOutletLogo(string logoFileName, Common.Model.User loggedInUser)
        {
            string outletFolder = ConfigurationManager.AppSettings["OutletLogoUrl"];
            string path = Path.Combine(HostingEnvironment.MapPath(outletFolder), logoFileName);

            if (!System.IO.File.Exists(path))
            {
                string directoy = Path.GetDirectoryName(path);

                if (!Directory.Exists(directoy))
                    Directory.CreateDirectory(directoy);

                using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
                {
                    Stream fileData = mediaContactsService.GetOutletLogo(logoFileName, loggedInUser.SessionKey);

                    Image image = Image.FromStream(fileData);

                    image.Save(path);
                }
            }

            return Path.Combine(outletFolder, logoFileName);
        }

        public List<OutletSuggestViewModel> GetOutletsSuggestions(string term, string debtorNumber)
        {
            DataTable data = new SearchUtil(Connection).GetOutletsSuggestions(term.Replace("'", "''").Replace("\"", ""), debtorNumber);

            List<OutletSuggestViewModel> list = null;


            if (data != null && data.Rows.Count > 0)
                list = (from DataRow row in data.Rows
                        select new OutletSuggestViewModel
                        {
                            Id = !DBNull.Value.Equals(row["MediaOutletId"]) ? Convert.ToString(row["MediaOutletId"]).Trim() :
                                !DBNull.Value.Equals(row["OmaOutletId"]) ? Convert.ToString(row["OmaOutletId"]).Trim() : Convert.ToString(row["PrnOutletId"]).Trim(),
                            Name = Convert.ToString(row["OutletName"]),
                            IsPrivate = !DBNull.Value.Equals(row["OmaOutletId"]),
                            RecordType = !DBNull.Value.Equals(row["OmaOutletId"]) ? (int)RecordType.OmaOutlet :
                                    !DBNull.Value.Equals(row["MediaOutletId"]) ? (int)RecordType.MediaOutlet : (int)RecordType.PrnOutlet
                        }).ToList();

         return list;
        }

        public List<MediaMovementsViewModel> GetOutletMediaMovements(string outletId, int pageNo, int pageSize, Common.Model.User currentUser)
        {
            List<MediaMovement> model;

            using (var mcs = new MediaContactsServiceProxy())
            {
               model = mcs.GetOutletMediaMovements(outletId, pageNo, pageSize, currentUser.SessionKey);
            }

           return Mapper.Map<List<MediaContactsService.MediaMovement>, List<MediaMovementsViewModel>>(model);
        }
    }
}