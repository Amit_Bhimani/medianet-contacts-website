﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels.Outlet;
using System.Collections.Generic;
using Medianet.Contacts.Web.ViewModels.Dashboard;

namespace Medianet.Contacts.Web.Services.Outlet
{
    public interface IOutletService : IDisposable
    {
        OutletViewModel GetOutlet(string outletId, RecordType rt, Common.Model.User loggedInUser);

        OutletViewModel GetOutletPreview(string outletId, RecordType rt, Common.Model.User loggedInUser);

        string GetOutletLogo(string logoFileName, Common.Model.User loggedInUser);
        
        List<OutletSuggestViewModel> GetOutletsSuggestions(string term, string debtorNumber);

        List<MediaMovementsViewModel> GetOutletMediaMovements(string outletId, int pageNo, int pageSize, Common.Model.User currentUser);

    }
}