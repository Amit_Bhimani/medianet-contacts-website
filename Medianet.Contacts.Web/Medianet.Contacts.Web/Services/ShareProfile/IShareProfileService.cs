﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Contacts.Web.Services.ShareProfile
{
    public interface IShareProfileService : IDisposable
    {
        List<CustomerService.User> GetUsers(Common.Model.User loggedInUser);
    }
}
