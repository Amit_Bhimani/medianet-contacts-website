﻿using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.CustomerService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Services.ShareProfile
{
    public class ShareProfileService : ServiceBase, IShareProfileService
    {
        private ShareProfileService(DBClass db) : base(db)
        {
        }

        /// <summary>
        /// Create a new instance of the <c>ShareProfileService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>ShareProfileService</c> class.</returns>
        public static ShareProfileService CreateShareProfileService()
        {
            var db = new DBClass();
            return new ShareProfileService(db);
        }

        public List<CustomerService.User> GetUsers(Common.Model.User loggedInUser)
        {
            using (CustomerServiceClient service = new CustomerServiceClient())
            {
               return service.GetAllUsersByCustomer(loggedInUser.DebtorNumber, loggedInUser.SessionKey)
                    .Where(user => user.RowStatus == RowStatusType.Active)
                    .OrderBy(user => user.FirstName).ThenBy(user => user.LastName).ToList();
            }
        }
    }
}