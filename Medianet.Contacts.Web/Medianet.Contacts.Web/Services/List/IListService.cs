﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.List;

namespace Medianet.Contacts.Web.Services.List
{
    public interface IListService : IDisposable
    {
        List<ListViewModel> GetLists(int userId, string sessionKey);
        List<ListViewModel> GetListsByGroupId(int groupId, int userId);
        int CreateList(ListViewModel l, int userId);
        ListMergeViewModel GetListById(int id, string sessionKey);
        List<KeyValuePair<string, List<SelectListItem>>> GetListsByGroup(int listId, int userId, List<int> selected, string sessionKey);
        void MergeList(int listId, List<int> listIds, int userId);
        int CoutListRecords(int id, int userId);

        DataTable ExportList(int id, int userId);

        /// <summary>
        /// Get's a lists by CSV of list ids.
        /// </summary>
        /// <param name="listids">The CSV of list ids.</param>
        /// <returns>A List of List objects.</returns>
        List<ListBase> GetListsByIds(string listids);

        /// <summary>
        /// Get list items by listId
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listSetId"></param>
        /// <param name="userId"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        ListResultsViewModel GetListItems(int listId, int listSetId, int userId, string sessionKey);

        /// <summary>
        /// Get all list items by listId
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listSetId"></param>
        /// <param name="userId"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        ListResultsViewModel GetListItemsAll(int listId, int listSetId, int userId, string sessionKey);

        /// <summary>
        /// Restore list set
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listSetId"></param>
        /// <param name="userId"></param>
        /// <param name="sessionKey"></param>
        void RestoreListSet(int listId, int listSetId, int userId, string sessionKey);

        void DeleteItemsFromList(int listId, List<int> listItemsToDelete,string sessionKey);

        void AddToList(int listId, int userId, List<string> listItemsToAdd, bool createNewVersion, string sessionKey);

        ListVersionViewModel GetListVersions(int listId,int listSetId,string sessionKey);

        List<ExportProfileViewModel> GetExportProfiles(string debtorNumber, int userId,MediaContactsService.MediaContactsExportType exportType, string sessionKey);

        ExportProfileViewModel GetExportProfile(int exportId, string sessionKey);

        int SaveExportProfile(ExportProfileViewModel exportView, string sessionKey);

        void DeleteExportProfile(int exportId, string sessionKey);

        void UpdateList(int listId, string listName, bool isPrivate, int userId);

        void LivingListReject(int userId, int recordId, string sessionKey);

        void LivingListAccept(int userId, int recordId, string sessionKey);

        void LivingListAcceptRejectAll(int userId, int listId, string sessionKey);

        Dictionary<string, string> GetLivingListOutletContactsByRecordId(int recordId, int maxRowCount, string sessionKey);

        void LivingListReplaceContact(int userId, int recordId, string newContactId, string sessionKey);

        List<ListContactReplacementViewModel> GetLivingListContactReplacements(string sessionKey, int userId = 0, int listId = 0, int maxRows = 100);

        List<ListRecord> GetByGroupListsListIdAndListSet(int listId, int listSet, int userId,  string sessionKey, out int pTotalRecords,
                                                         int pPageNumber = 1, int pRecordsPerPage = 200,
                                                         SortColumn pSortColumn = SortColumn.OutletName,
                                                         SortDirection pSortDirection = SortDirection.ASC);
    }
}
