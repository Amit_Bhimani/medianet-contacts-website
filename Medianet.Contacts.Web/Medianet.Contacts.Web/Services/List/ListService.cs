﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.List;
using Medianet.Contacts.Web.MediaContactsService;

namespace Medianet.Contacts.Web.Services.List
{
    public class ListService : ServiceBase, IListService
    {
        private ListService(DBClass db) : base(db) { }

        public static ListService CreateListService()
        {
            DBClass db = new DBClass();
            return new ListService(db);
        }

        /// <summary>
        /// Get all lists for a User.
        /// </summary>
        /// <param name="userId">The current user's ID.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>List of <c>ListViewModel</c> objects.</returns>
        public List<ListViewModel> GetLists(int userId, string sessionKey)
        {
            List<MediaContactsService.MediaContactList> lists;

            using (var mc = new MediaContactsServiceProxy())
            {
                lists = mc.GetLists(userId, sessionKey);
            }

            List<ListViewModel> vlists = null;
            vlists = (from l in lists
                      select new ListViewModel
                      {
                          Name = l.Name,
                          ListId = l.Id,
                          IsPrivate = l.IsPrivate,
                          Group = new GroupViewModel { Id = l.GroupId, Name = l.Group.Name, Type = (GroupType)l.Group.GroupType }
                      }).ToList<ListViewModel>();

            return vlists;
        }

        /// <summary>
        /// Get all lists by group Id.
        /// </summary>
        /// <param name="groupId">The group to search by.</param>
        /// <param name="userId">The current user's ID.</param>
        /// <returns>List of <c>ListViewModel</c> objects that belong to the provided group.</returns>
        public List<ListViewModel> GetListsByGroupId(int groupId, int userId)
        {
            List<ListBase> lists = null;
            using (DBClass db = new DBClass())
            {
                ListBAL bal = new ListBAL(db);
                lists = bal.GetListsByGroupId(groupId, userId);
            }

            List<ListViewModel> vlists = null;
            vlists = (from l in lists
                      select new ListViewModel
                      {
                          Name = l.Name,
                          ListId = l.Id,
                          IsPrivate = l.IsPrivate
                      }).ToList<ListViewModel>();

            return vlists;
        }

        /// <summary>
        /// Adds the specified records to the specified list.
        /// </summary>
        /// <param name="listId">The ID of the list to add the records to.</param>
        /// <param name="listItemsToAdd">The records to add.</param>
        /// <param name="userId">The current user's ID.</param>
        /// <param name="createNewVersion">Flag to indicate whether to create a new version. True to create a new version, false to not.</param>
        /// <returns>True on success, false otherwise.</returns>
        public void AddToList(int listId, int userId, List<string> listItemsToAdd, bool createNewVersion, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                mc.AddToList(listId, userId, listItemsToAdd, createNewVersion, sessionKey);
            }
        }

        /// <summary>
        /// Inserts the given list into the database.
        /// </summary>
        /// <param name="l">The <c>ListViewModel</c> object to insert.</param>
        /// <returns>The ID of the new list.</returns>
        /// <remarks>This method coerces the <c>ListViewModel</c> into a concrete <c>List</c>.</remarks>
        public int CreateList(ListViewModel l, int userId)
        {
            int id = 0;
            Common.Model.List list = new Common.Model.List();

            list.BelongsToGroup = new GroupBase();
            list.BelongsToGroup.Id = l.Group.Id;
            list.UserId = userId;
            list.Name = l.Name;
            list.IsPrivate = l.IsPrivate;

            using (DBClass db = new DBClass())
            {
                ListBAL bal = new ListBAL(db);
                id = bal.Insert(list);
            }
            return id;
        }

        /// <summary>
        /// Get's a list by ID.
        /// </summary>
        /// <param name="id">The ID of the list to retrieve.</param>
        /// <param name="userId">The current user's ID.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A <c>ListMergeViewModel</c> representation of the list. Null if the list can't be found.</returns>
        public ListMergeViewModel GetListById(int id, string sessionKey)
        {
            ListMergeViewModel m = null;

            MediaContactsService.MediaContactList list;

            using (var mc = new MediaContactsServiceProxy())
            {
                list = mc.GetList(id, sessionKey);
            }

            m = new ListMergeViewModel
                {
                    Group =
                        new GroupViewModel
                            {
                                Id = list.GroupId,
                                Name = list.Group.Name,
                                UserId = list.OwnerUserId,
                                Type = (GroupType)list.Group.GroupType,
                                CreatedDate = list.Group.CreatedDate,
                                UpdatedDate = list.Group.LastModifiedDate
                            },
                    ListId = list.Id,
                    Name = list.Name,
                    IsPrivate = list.IsPrivate,
                    UserId = list.OwnerUserId
                };

            return m;
        }

        public int CoutListRecords(int id, int userId)
        {
            using (DBClass db = new DBClass())
            {
                ListMasterBAL bal = new ListMasterBAL(db);
                return bal.CountListRecords(id, userId);
            }    
        }

        /// <summary>
        /// Returns all lists keyed by group name.
        /// </summary>
        /// <param name="listId">List ID to not include.</param>
        /// <param name="userId">The current user ID.</param>
        /// <param name="selected">List of ID's that are selected.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>We use a list of key value pair objects to cater for duplicate group names.</returns>
        public List<KeyValuePair<string, List<SelectListItem>>> GetListsByGroup(int listId, int userId, List<int> selected, string sessionKey)
        {
            // Populate the select list.
            var listsByGroup = new List<KeyValuePair<string, List<SelectListItem>>>();
            List<MediaContactList> lists;
            List<SelectListItem> selectedList = null;

            using (var mc = new MediaContactsServiceProxy())
            {
                lists = mc.GetLists(userId, sessionKey);
            }

            foreach (var l in lists)
            {
                if (!listsByGroup.Exists(g => g.Key == l.Group.Name))
                {
                    selectedList = (from x in lists
                                    where x.Id != listId
                                    && x.GroupId == l.GroupId
                                    select new SelectListItem()
                                    {
                                        Text = x.Name,
                                        Value = x.Id.ToString(),
                                        Selected = selected.Contains(x.Id)
                                    }).ToList<SelectListItem>();

                    if (selectedList.Count > 0)
                    {
                        var kvp = new KeyValuePair<string, List<SelectListItem>>(l.Group.Name, selectedList);
                        listsByGroup.Add(kvp);
                    }
                }
            }

            return listsByGroup.OrderBy(g => g.Key).ToList();
        }

        /// <summary>
        /// Merges the provided list with each list in the <c>ListIds</c>.
        /// </summary>
        /// <param name="listId">The destination list..</param>
        /// <param name="ListIds">The lists to be merged.</param>
        /// <param name="userId">The current user Id.</param>
        public void MergeList(int listId, List<int> listIds, int userId)
        {
            ListBAL bal = new ListBAL(Connection);
            bal.MergeList(listId, listIds, userId);
        }

        /// <summary>
        /// Get's lists by a CSV of list ids.
        /// </summary>
        /// <param name="listids">The CSV of list ids.</param>
        /// <returns>A List of List objects.</returns>
        public List<ListBase> GetListsByIds(string listids)
        {
            ListBAL bal = new ListBAL(Connection);
            return bal.GetListsByIds(listids);
        }

        public ListResultsViewModel GetListItems(int listId, int listSetId, int userId, string sessionKey)
        {
            List<ListRecord> listRecords = null;
            int myResultCount = 0;
            var m = new ListResultsViewModel
            {
                Pagination = { ListId = listId, ListSet = listSetId }
            };

            listRecords = GetByGroupListsListIdAndListSet(listId, listSetId, userId, sessionKey, out myResultCount, m.Pagination.Page, m.Pagination.PageSize);
            m.Pagination.Results = listRecords;
            m.Pagination.ResultCount = myResultCount;

            return m;
        }

        public ListResultsViewModel GetListItemsAll(int listId, int listSetId, int userId, string sessionKey)
        {
            List<ListRecord> listRecords = null;
            int myResultCount = 0;

            listRecords = GetByGroupListsListIdAndListSet(listId, listSetId, userId, sessionKey, out myResultCount, 1, Int32.MaxValue);
            var m = new ListResultsViewModel
            {
                Pagination = { Results = listRecords, ResultCount = myResultCount, ListId = listId, ListSet = listSetId }
            };

            return m;
        }

        public DataTable ExportList(int id, int userId)
        {
            ListMasterBAL lmBAL = new ListMasterBAL(Connection);
            return lmBAL.GetExportListAll(id, userId);
        }

        public List<ExportProfileViewModel> GetExportProfiles(string debtorNumber, int userId, MediaContactsService.MediaContactsExportType exportType, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                List<MediaContactsService.MediaContactsExport> profiles = mc.GetExports(debtorNumber, userId, exportType, sessionKey);

                List<ExportProfileViewModel> exportViews = null;
                exportViews = (from p in profiles
                               select new ExportProfileViewModel
                               {
                                   Id = p.Id,
                                   Name = p.Name,
                                   IsPrivate = p.IsPrivate,
                                   DebtorNumber = p.DebtorNumber,
                                   UserId = p.UserId
                               }).ToList<ExportProfileViewModel>();
                return exportViews;
            }
        }

        public ExportProfileViewModel GetExportProfile(int exportId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                MediaContactsService.MediaContactsExport profile = mc.GetExport(exportId, sessionKey);

                ExportProfileViewModel exportView = new ExportProfileViewModel
                {
                    Id = profile.Id,
                    Name = profile.Name,
                    IsPrivate = profile.IsPrivate,
                    DebtorNumber = profile.DebtorNumber,
                    UserId = profile.UserId,
                    ExportFields = profile.ExportFields
                };

                return exportView;
            }
        }

        public int SaveExportProfile(ExportProfileViewModel exportView, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                MediaContactsService.MediaContactsExport exportProfile;

                if (exportView.Id > 0)
                {
                    exportProfile = mc.GetExport(exportView.Id, sessionKey);
                    exportProfile.Name = exportView.Name;
                    exportProfile.ExportFields = exportView.ExportFields;
                    exportProfile.ExportType = exportView.ExportType;

                    // Only update the IsPrivate flag if they are the owner.
                    if (exportProfile.UserId == exportView.UserId)
                        exportProfile.IsPrivate = exportView.IsPrivate;

                    mc.UpdateExportProfile(exportProfile, sessionKey);
                }
                else
                {
                    exportProfile = new MediaContactsService.MediaContactsExport
                    {
                        Id = exportView.Id,
                        Name = exportView.Name,
                        IsPrivate = exportView.IsPrivate,
                        DebtorNumber = exportView.DebtorNumber,
                        UserId = exportView.UserId,
                        ExportFields = exportView.ExportFields,
                        IsDeleted = false,
                        ExportType = exportView.ExportType
                    };

                    exportView.Id = mc.AddExportProfile(exportProfile, sessionKey);
                }

                return exportView.Id;
            }
        }

        public void DeleteExportProfile(int exportId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                mc.DeleteExportProfile(exportId, sessionKey);
            }
        }

        public void UpdateList(int listId, string listName, bool isPrivate, int userId)
        {
            var bal = new GroupListBAL(Connection);
            bal.UpdateList(listId, listName, isPrivate, userId);
        }

        #region List Version

        public ListVersionViewModel GetListVersions(int listId, int listSetId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                List<MediaContactsService.MediaContactListVersion> listVersions = mc.GetListVersions(listId, sessionKey);
                ListVersionViewModel listVersionViews = null;
                if (listVersions != null)
                {
                    listVersionViews = (from p in listVersions
                                        select new ListVersionViewModel
                                            {
                                                SelectedListSetId = listVersions.Any(lv => (lv.Id == listSetId)) ? listSetId : 0,
                                                Results = listVersions
                                            }).FirstOrDefault();
                }

                return listVersionViews;
            }
        }

        public void RestoreListSet(int listId, int listSetId, int userId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                mc.RestoreListSet(listId, listSetId, userId, sessionKey);
            }
        }

        #endregion

        #region Living List

        public List<ListContactReplacementViewModel> GetLivingListContactReplacements(string sessionKey, int userId, int listId, int maxRows)
        {
            List<ListContactReplacementViewModel> deletionsViewModel = null;

            if (listId == 0)
            {
                deletionsViewModel = Cacher.Get(CACH_LIVING_LIST_KEY + "-" + userId.ToString(),
                    CACH_LIVING_LIST_TIME, () => GetLivingListContactReplacementsFromDB(sessionKey, userId));
            }
            else
            {
                using (var mc = new MediaContactsServiceProxy())
                {
                    var deletions = mc.GetLivingListContactReplacements(sessionKey, userId, listId, maxRows);
                    deletionsViewModel = Mapper.Map<List<MediaContactLivingListContactReplacement>, List<ListContactReplacementViewModel>>(deletions);
                }
            }

            return deletionsViewModel;
        }

        public Dictionary<string, string> GetLivingListOutletContactsByRecordId(int recordId, int maxRow, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                return mc.GetLivingListOutletContactsByRecordId(recordId, maxRow, sessionKey);
            }
        }

        public void LivingListReplaceContact(int userId, int recordId, string newContactId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                //We are changing living lists data so remove the global list from cache.
                Cacher.Remove(CACH_LIVING_LIST_KEY + "-" + userId.ToString());
                mc.LivingListReplaceContact(recordId, newContactId, sessionKey);
            }

        }

        public void LivingListAccept(int userId, int recordId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                // We are changing living lists data so remove the global list from cache.
                Cacher.Remove(CACH_LIVING_LIST_KEY + "-" + userId.ToString());
                mc.LivingListAccept(recordId, sessionKey);
            }
        }

        public void LivingListReject(int userId, int recordId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                // We are changing living lists data so remove the global list from cache.
                Cacher.Remove(CACH_LIVING_LIST_KEY + "-" + userId.ToString());
                mc.LivingListReject(recordId, sessionKey);
            }
        }

        public void LivingListAcceptRejectAll(int userId, int listId, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                // We are changing living lists data so remove the global list from cache.
                Cacher.Remove(CACH_LIVING_LIST_KEY + "-" + userId.ToString());
                mc.LivingListAcceptRejectAll(listId, sessionKey);
            }
        }

        private List<ListContactReplacementViewModel> GetLivingListContactReplacementsFromDB(string sessionKey, int userId)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                var deletions = mc.GetLivingListContactReplacements(sessionKey, userId, 0, 6);

                return Mapper.Map<List<MediaContactLivingListContactReplacement>, List<ListContactReplacementViewModel>>(deletions);
            }
        }

        #endregion

        #region Group Records listset

        public List<ListRecord> GetByGroupListsListIdAndListSet(int listId, int listSet, int userId, string sessionKey, out int pTotalRecords, int pPageNumber = 1,
                                                    int pRecordsPerPage = 200, Medianet.Contacts.Wcf.DataContracts.DTO.SortColumn pSortColumn = Medianet.Contacts.Wcf.DataContracts.DTO.SortColumn.OutletName,
                                                    Medianet.Contacts.Wcf.DataContracts.DTO.SortDirection pSortDirection = Medianet.Contacts.Wcf.DataContracts.DTO.SortDirection.ASC)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                var records = mc.GetPaginatedRecordsByListsetAndListId(listId, listSet, pPageNumber, pRecordsPerPage,
                                                                       (MediaContactsService.SortColumn)pSortColumn, (MediaContactsService.SortDirection)pSortDirection, userId, sessionKey);


                pTotalRecords = records.TotalRecordsCount;
                var returnlist = Mapper.Map<List<MediaContactListRecordDetails>, List<ListRecord>>(new List<MediaContactListRecordDetails>(records.ListRecord));
                return returnlist;
            }
        }

        public void DeleteItemsFromList(int listId, List<int> listItemsToDelete, string sessionKey)
        {
            try
            {
                using (var mc = new MediaContactsServiceProxy())
                {
                    mc.DeleteItemsFromList(listId, listItemsToDelete, sessionKey);
                }
            }
            catch (Exception e)
            {
                BasePage.LogException(e, "", "DeleteListItems", new System.Collections.Specialized.NameValueCollection()
                        {
                            {"listid", listId.ToString()},
                            {"recordIds", string.Join(",", listItemsToDelete.Select(n => n.ToString()).ToArray())}
                        });
            }
        }

        #endregion
    }
}