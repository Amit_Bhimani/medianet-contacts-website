﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.PrivateContact;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.MediaContactsService;

namespace Medianet.Contacts.Web.Services.PrivateContact
{
    public class PrivateContactService : ServiceBase, IPrivateContactService
    {
        private PrivateContactService(DBClass db) : base(db)
        {
        }

        /// <summary>
        /// Create a new instance of the <c>PrivateContactService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>PrivateContactService</c> class.</returns>
        public static PrivateContactService CreatePrivateContactService()
        {
            var db = new DBClass();
            return new PrivateContactService(db);
        }
        
        public PrivateContactViewModel GetById(string contactId, string debtorNumber)
        {
            var privateContact = new OmaContactBAL(Connection).GetPrivateContact(contactId, debtorNumber);

            var result = Mapper.Map<PrivateContactModel, PrivateContactViewModel>(privateContact);

            if (result.Subjects != null)
                result.SubjectIdsCsv = string.Join(",", result.Subjects.Select(m => m.Id));

            if (result.Roles != null)
                result.RoleIdsCsv = string.Join(",", result.Roles.Select(m => m.Id));

            return result;
        }

        public bool Save(PrivateContactViewModel model)
        {
            return model.ContactId > 0 ?
                new OmaContactBAL(Connection).UpdatePrivateContact(Mapper.Map<PrivateContactViewModel, PrivateContactModel>(model)) :
                new OmaContactBAL(Connection).CreatePrivateContact(Mapper.Map<PrivateContactViewModel, PrivateContactModel>(model));
        }

        public bool Delete(string contactId, string debtorNumber, int userId)
        {
            return new OmaContactBAL(Connection).DeletePrivateContact(contactId, debtorNumber, userId) > 0;
        }
    }
}