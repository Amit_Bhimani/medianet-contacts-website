﻿using System;
using Medianet.Contacts.Web.ViewModels.PrivateContact;

namespace Medianet.Contacts.Web.Services.PrivateContact
{
    public interface IPrivateContactService : IDisposable
    {
        PrivateContactViewModel GetById(string contactId, string debtorNumber);

        bool Save(PrivateContactViewModel model);

        bool Delete(string contactId, string debtorNumber, int userId);
    }
}