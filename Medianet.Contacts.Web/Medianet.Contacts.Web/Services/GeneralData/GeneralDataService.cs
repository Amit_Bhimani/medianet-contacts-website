﻿using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Services.BaseInfo
{
    public class GeneralDataService : ServiceBase
    {
        public GeneralDataService(DBClass db) : base(db)
        {
        }

        public static GeneralDataService CreateSubjectService()
        {
            var db = new DBClass();

            return new GeneralDataService(db);
        }

        public List<Subject> GetSubjects()
        {
            var sb = new SubjectBAL(Connection);

            return sb.GetSubjects();
        }

        public List<SubjectGroup> GetSubjectGroups()
        {
            var sb = new SubjectBAL(Connection);

            return sb.GetSubjectGroups();
        }

        public List<WorkingLanguage> GetWorkingLanguage()
        {
            var sb = new SearchUtil(Connection);

            return sb.GetWorkingLanguage();
        }

        public List<Frequency> GetFrequency()
        {
            var sb = new SearchUtil(Connection);

            return sb.GetFrequency();
        }

        public List<MediaType> GetMediaType()
        {
            var sb = new SearchUtil(Connection);

            return sb.GetMedia();
        }

        public List<Position> GetRoles()
        {
            var crBAL = new ContactRoleBAL(Connection);

            return crBAL.GetRoles();
        }
    }
}