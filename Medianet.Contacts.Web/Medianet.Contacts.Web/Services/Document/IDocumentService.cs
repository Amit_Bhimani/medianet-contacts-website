﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Contacts.Web.Services.Document
{
    public interface IDocumentService
    {
        void Save(DocumentViewModel model, Common.Model.User LoggedInUser);

        DocumentViewModel GetById(int documentId, Common.Model.User LoggedInUser);

        bool IsAuthorized(bool isPrivate, int uploadedByUserId, Common.Model.User LoggedInUser);

        void Delete(int documentId, Common.Model.User LoggedInUser);

        dynamic GetDocuments(string outletId, string contactId, RecordType recordType, Common.Model.User loggedInUser);

        DocumentViewModel GetDocumentFile(int documentId, Common.Model.User LoggedInUser);
    }
}
