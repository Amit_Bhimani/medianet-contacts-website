﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Common.BAL;
using System.IO;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using AutoMapper;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.ProxyWrappers;

namespace Medianet.Contacts.Web.Services.Document
{
    public class DocumentService : IDocumentService
    {
        #region Constructors

        public DocumentService()
        {
        }

        #endregion

        #region Public methods

        public static DocumentService CreateDocumentService()
        {
            return new DocumentService();
        }

        public void Save(DocumentViewModel model, Common.Model.User LoggedInUser)
        {
            OmaDocument entity = new OmaDocument
            {
                Name = string.IsNullOrEmpty(model.Name) ? Path.GetFileNameWithoutExtension(model.File.FileName) : model.Name,
                IsPrivate = model.IsPrivate,
                CreatedDate = DateTime.Now,
                Status = true,
                UploadedByUserId = LoggedInUser.UserID,
                DocumentId = model.DocumentId,
                RecordType = (int)model.RecordType
            };

            if (model.DocumentId <= 0)
            {
                entity.DocumentType = Path.GetExtension(model.File.FileName);
                entity.ContactId = model.ContactId;
                entity.OutletId = model.OutletId;

                using (var binaryReader = new BinaryReader(model.File.InputStream))
                {
                    entity.DocumentContent = binaryReader.ReadBytes(model.File.ContentLength);
                }
            }

            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                mediaContactsService.SaveOmaDocument(entity, LoggedInUser.SessionKey);
            }
        }

        public DocumentViewModel GetById(int documentId, Common.Model.User LoggedInUser)
        {
            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                OmaDocument entity = mediaContactsService.GetDocumentById(documentId, LoggedInUser.SessionKey);

                return Mapper.Map<OmaDocument, DocumentViewModel>(entity);
            }
        }

        public bool IsAuthorized(bool isPrivate, int uploadedByUserId, Common.Model.User LoggedInUser)
        {
            return !isPrivate || uploadedByUserId == LoggedInUser.UserID;
        }

        public void Delete(int documentId, Common.Model.User LoggedInUser)
        {
            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                mediaContactsService.DeleteOmaDocument(documentId, LoggedInUser.SessionKey);
            }
        }

        public dynamic GetDocuments(string outletId, string contactId, RecordType recordType, Common.Model.User loggedInUser)
        {
            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                return mediaContactsService.GetDocuments(outletId, contactId, recordType, loggedInUser.SessionKey);
            }
        }
        public DocumentViewModel GetDocumentFile(int documentId, Common.Model.User LoggedInUser)
        {
            using (MediaContactsServiceProxy mediaContactsService = new MediaContactsServiceProxy())
            {
                OmaDocument entity = mediaContactsService.GetDocumentFile(documentId, LoggedInUser.SessionKey);

                return Mapper.Map<OmaDocument, DocumentViewModel>(entity);
            }
        }

        #endregion

    }
}