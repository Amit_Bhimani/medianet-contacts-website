﻿using System.Data;
using AutoMapper;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ProxyWrappers;
using System.Collections.Generic;
using System;
using Medianet.Contacts.Web.Services.Note;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.Note;

namespace Medianet.Contacts.Web.Services.Note
{
    public class NoteService : INoteService
    {
        /// <summary>
        /// Create a new instance of the <c>NoteService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>NoteService</c> class.</returns>
        public static NoteService CreateNoteService()
        {
            return new NoteService();
        }

        public void DeleteNote(int id, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                mc.DeleteNote(id, sessionKey);
            }
        }

        public void PinUnpinnNote(int id, bool pinned, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                mc.PinUnpinNote(id, pinned, sessionKey);
            }
        }

        public NoteViewModel GetNote(int id, string rt, string sessionKey)
        {
            var note = new MediaContactsService.Note();

            using (var mc = new MediaContactsServiceProxy())
            {
                note = mc.GetNote(id, sessionKey);
            }

            return MapNoteToViewModel(note, rt);
        }

        public List<NoteViewModel> GetContactNotes(string contactId, string outletId, string recordType, int userId, string sessionKey)
        {
            var notes = new List<MediaContactsService.Note>();

            using (var mc = new MediaContactsServiceProxy())
            {
                notes = mc.GetContactNotes(outletId, contactId, recordType, sessionKey);
            }

            var noteViewModels = new List<NoteViewModel>();

            foreach (var note in notes)
            {
                noteViewModels.Add(MapNoteToViewModel(note, recordType));
            }

            return noteViewModels;
        }

        public List<NoteViewModel> GetOutletNotes(string outletId, string recordType, int userId, string sessionKey)
        {
            var notes = new List<Medianet.Contacts.Web.MediaContactsService.Note>();

            using (var mc = new MediaContactsServiceProxy())
            {
                notes = mc.GetOutletNotes(outletId, recordType, sessionKey);
            }

            var noteViewModels = new List<NoteViewModel>();

            foreach (var note in notes)
            {
                noteViewModels.Add(MapNoteToViewModel(note, recordType));
            }

            return noteViewModels;
        }

        public int AddNote(NoteViewModel note, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                var entity = MapViewModelToNote(note);

                return mc.AddNote(entity, sessionKey);
            }
        }

        public void UpdateNote(NoteViewModel note, string sessionKey)
        {
            using (var mc = new MediaContactsServiceProxy())
            {
                var entity = MapViewModelToNote(note);

                mc.UpdateNote(entity, sessionKey);
            }
        }

        public void AddNoteBulk(NoteViewModel note, List<ListRecord> recordsToAdd, string sessionKey)
        {
            var entity = AutoMapper.Mapper.Map<NotesBulk>(note);

            entity.Records = AutoMapper.Mapper.Map<List<MediaRecordIdentifier>>(recordsToAdd);

            using (var mc = new MediaContactsServiceProxy())
            {
                mc.AddNoteBulk(entity, sessionKey);
            }
        }

        private NoteViewModel MapNoteToViewModel(MediaContactsService.Note note, string recordType)
        {
            var noteViewModel = new NoteViewModel();

            RecordType rt= (RecordType)Enum.Parse(typeof(RecordType), recordType.ToString());

            var contacts = new List<dynamic>();
            contacts.Add(note.MediaContactId);
            contacts.Add(note.PrnContactId);
            contacts.Add(note.OmaContactId);
            var contactId = contacts.Find(item => item != null);

            var outlets = new List<dynamic>();
            outlets.Add(note.MediaOutletId);
            outlets.Add(note.PrnOutletId);
            outlets.Add(note.OmaOutletId);
            var outletId = outlets.Find(item => item != null);

            noteViewModel.ContactId = Convert.ToString(contactId);
            noteViewModel.OutletId = Convert.ToString(outletId);
            noteViewModel.Id = note.Id;
            noteViewModel.CreatedDate = note.CreatedDate;
            noteViewModel.IsPrivate = note.IsPrivate;
            noteViewModel.Pinned = note.Pinned;
            noteViewModel.AAPNote = note.AAPNote;
            noteViewModel.Notes = note.Notes;
            noteViewModel.UserId = note.UserId;
            noteViewModel.UserName = note.AAPNote ? "Medianet" : (note.User != null ?
            string.Format("{0} {1}", note.User.FirstName, note.User.LastName) : "");
            noteViewModel.RecordType = rt;

            return noteViewModel;
        }

        private MediaContactsService.Note MapViewModelToNote(NoteViewModel noteViewModel)
        {
            var note = new MediaContactsService.Note();

            note.DebtorNumber = noteViewModel.DebtorNumber;
            note.IsPrivate = noteViewModel.IsPrivate;
            note.Pinned = noteViewModel.Pinned;
            note.Notes = noteViewModel.Notes;
            note.UserId = noteViewModel.UserId;

            note.Id = (noteViewModel.Id > 0 ? noteViewModel.Id : 0);
            note.AAPNote = (noteViewModel.Id > 0 ? noteViewModel.AAPNote : false);
            note.CreatedDate = noteViewModel.Id > 0 ? note.CreatedDate : DateTime.Now;
            note.Status = noteViewModel.Id > 0 ? note.Status : Convert.ToByte(true);

            if (noteViewModel.Id == 0)
            {
                switch (noteViewModel.RecordType)
                {
                    case RecordType.MediaOutlet:
                        note.MediaOutletId = noteViewModel.OutletId;
                        break;
                    case RecordType.OmaOutlet:
                        int omaOId;

                        if (int.TryParse(noteViewModel.OutletId, out omaOId))
                        {
                            note.OmaOutletId = omaOId;
                        }
                        break;
                    case RecordType.PrnOutlet:
                        long prnOId;

                        if (long.TryParse(noteViewModel.OutletId, out prnOId))
                        {
                            note.PrnOutletId = prnOId;
                        }
                        break;
                    case RecordType.MediaContact:
                        note.MediaOutletId = noteViewModel.OutletId;
                         note.MediaContactId = noteViewModel.ContactId;
                        break;
                    case RecordType.PrnContact:
                        long prnCOId;
                        long prnCCId;

                        if (long.TryParse(noteViewModel.OutletId, out prnCOId) && long.TryParse(noteViewModel.ContactId, out prnCCId))
                        {
                            note.PrnOutletId = prnCOId;
                            note.PrnContactId = prnCCId;
                        }
                        break;
                }

                if (RecordTypeExtensions.IsOmaContact(noteViewModel.RecordType))
                {
                    int omaCId;
                    if (int.TryParse(noteViewModel.ContactId, out omaCId))
                    {
                        note.OmaContactId = omaCId;
                    }
                }
            }
            return note;
        }
    }
}