﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.Note;

namespace Medianet.Contacts.Web.Services.Note
{
    public interface INoteService
    {
        void DeleteNote(int id, string sessionKey);
        void PinUnpinnNote(int id, bool pinned, string sessionKey);
        NoteViewModel GetNote(int id, string rt, string sessionKey);
        List<NoteViewModel> GetContactNotes(string contactId, string outletId, string rt, int userId, string sessionKey);
        int AddNote(NoteViewModel note, string sessionKey);
        void UpdateNote(NoteViewModel note, string sessionKey);
        void AddNoteBulk(NoteViewModel note, List<ListRecord> recordsToAdd, string sessionKey);
        List<NoteViewModel> GetOutletNotes(string outletId, string recordType, int userId, string sessionKey);
    }
}