﻿using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.Web.Common.BAL;
using System.Data;
using Medianet.Contacts.Web.ViewModels.AllLists;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Services;
using Medianet.Contacts.Web.Common;
using System;

namespace Medianet.Contacts.Web.Services
{
    public class GroupListService : ServiceBase, IGroupListService
    {
        private GroupListService(DBClass db) : base(db) { }

        private GroupListBAL _bal;

        public static GroupListService CreateGroupListService() {
            DBClass db = new DBClass();
            return new GroupListService(db);
        }

        public int DeleteGroupList(int groupid, int userid)
        {
            _bal = new GroupListBAL(Connection);
            return _bal.DeleteGroupList(groupid, userid);
        }
        public int DeleteOnListIdAndUserId(int UserId, int ListId)
        {
            _bal = new GroupListBAL(Connection);
            return _bal.DeleteOnListIdAndUserId(UserId, ListId);
        }

        public GroupList GetById(int id)
        {
            _bal = new GroupListBAL(Connection);
            return _bal.GetById(id);
        }

        public List<ListsGroupedViewModel> GetGroupedListsDebtorUserId(int UserId, string DebtorNumber)
        {
            List<ViewModels.AllLists.ListViewModel> AllLists = null;

            AllLists = GetAllLists(UserId,DebtorNumber);

            if (AllLists != null && AllLists.Any())
            {
                List<ListsGroupedViewModel> groups = (from l in AllLists
                                                      group l by l.GroupId into g
                                                      select new ListsGroupedViewModel { GroupId = g.Key, Lists = g.ToList() })
                                                      .OrderBy(g => g.Lists != null && g.Lists.Count > 0 ? g.Lists.First().GroupName : "")
                                                      .ToList();

                return groups;
            }

            return null;
        }

        public List<ListViewModel> GetGroupedListsDebtorUserIdGroupId(int groupId, int userId, string debtorNumber)
        {
            _bal = new GroupListBAL(Connection);
            DataTable dt = _bal.GetListsListDebtorUserIdGroupId(groupId, userId, debtorNumber);

            List<ListViewModel> groupedList = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                groupedList = (from DataRow row in dt.Rows
                            select new ViewModels.AllLists.ListViewModel
                            {
                                ListId = (int)row["ListId"],
                                ListName = (string)row["ListName"],
                                UserId = (int)row["UserId"],
                                UserFullName = (string)row["FullName"],
                                Visibility = (string)row["Visibility"],
                                CreatedDate = Convert.ToDateTime(row["CreatedDate"]),
                                GroupId = (int)row["GroupId"],
                                GroupName = (string)row["GroupName"]
                            }).ToList<ViewModels.AllLists.ListViewModel>();
            }
            return groupedList;


        }


        public List<ListViewModel> GetAllLists(int UserId, string DebtorNumber)
        {

            _bal = new GroupListBAL(Connection);
            DataTable dt = _bal.GetListsListDebtorUserId(UserId, DebtorNumber);

            List<ListViewModel> AllLists = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                AllLists = (from DataRow row in dt.Rows
                            select new ViewModels.AllLists.ListViewModel
                            {
                                ListId = (int)row["ListId"],
                                ListName = (string)row["ListName"],
                                UserId = (int)row["UserId"],
                                UserFullName = (string)row["FullName"],
                                Visibility = (string)row["Visibility"],
                                CreatedDate = Convert.ToDateTime(row["CreatedDate"]),
                                GroupId = (int)row["GroupId"],
                                GroupName =  row["GroupName"] is DBNull ? string.Empty : (string)row["GroupName"]
                            }).ToList<ViewModels.AllLists.ListViewModel>();
            }
            return AllLists;
        }
    }
}