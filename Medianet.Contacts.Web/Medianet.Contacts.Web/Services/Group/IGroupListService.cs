﻿using System;
using System.Collections.Generic;
using Medianet.Contacts.Web.ViewModels.AllLists;

namespace Medianet.Contacts.Web.Services
{
    public interface IGroupListService : IDisposable
    {
        int DeleteGroupList(int groupid, int userid);
        List<ListsGroupedViewModel> GetGroupedListsDebtorUserId(int UserId, string DebtorNumber);
        List<ListViewModel> GetGroupedListsDebtorUserIdGroupId(int groupId, int userId, string debtorNumber);
        List<ListViewModel> GetAllLists(int UserId, string DebtorNumber);
        int DeleteOnListIdAndUserId(int UserId, int ListId);
    }
}