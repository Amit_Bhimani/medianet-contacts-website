﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.Services;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.Common;

namespace Medianet.Contacts.Web.Services
{
    public class GroupService : ServiceBase, IGroupService
    {
        private GroupService(DBClass db) : base(db) { }

        public static GroupService CreateGroupService() {
            DBClass db = new DBClass();
            return new GroupService(db);
        }

        /// <summary>
        /// Try to rename a group.
        /// </summary>
        /// <param name="id">The id of the group.</param>
        /// <param name="name">The new name of the group.</param>
        /// <returns>True on success, false on failure.</returns>
        public bool RenameGroup(int id, string name) {
            GroupBAL bal = new GroupBAL(Connection);
            return bal.Rename(id, name) != 0;
        }

        /// <summary>
        /// Get's a group by it's ID.
        /// </summary>
        /// <param name="groupId">The ID of the group to get.</param>
        /// <returns>A GroupViewModel representing the group.</returns>
        public GroupViewModel GetGroupById(int groupId, int userId) {
            GroupViewModel g = new GroupViewModel();

            GroupBAL bal = new GroupBAL(Connection);
            GroupBase gBase = bal.GetGroupById(groupId, userId);

            g.Id = gBase.Id;
            g.Name = gBase.Name;
            g.Type = gBase.Type;
            
            return g;
        }

        /// <summary>
        /// Gets all the groups by a user.
        /// </summary>
        /// <param name="userId">The current user's id.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A <c>List&lt;GroupViewModel&gt;</c>.</returns>
        public List<GroupViewModel> GetGroups(int userId, string sessionKey)
        {
            List<MediaContactGroup> groups;

            using (var mc = new MediaContactsServiceProxy())
            {
                groups = mc.GetGroups(userId, sessionKey);
            }

            List<GroupViewModel> vgroups = null;
            vgroups = (from g in groups
                       select new GroupViewModel
                       {
                           Id = g.Id,
                           Name = g.Name,
                           Type = (GroupType)g.GroupType
                       }).ToList<GroupViewModel>();
            return vgroups;
        }

        /// <summary>
        /// Gets all the groups by type.
        /// </summary>
        /// <param name="type">The type of groups to retrieve.</param>
        /// <param name="userId">The current user's id.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A <c>List&lt;GroupViewModel&gt;</c>.</returns>
        public List<GroupViewModel> GetGroupsByType(GroupType type, int userId, string sessionKey)
        {
            List<MediaContactGroup> groups;

            using (var mc = new MediaContactsServiceProxy())
            {
                groups = mc.GetGroupsByType((MediaContactGroupType)type, userId, sessionKey);
            }

            List<GroupViewModel> vgroups = null;
            vgroups = (from g in groups
                       select new GroupViewModel
                       {
                           Id = g.Id,
                           Name = g.Name
                       }).OrderBy(g => g.Name).ToList<GroupViewModel>();
            return vgroups;
        }

        /// <summary>
        /// Insert a new group into the database from a GroupViewModel.
        /// </summary>
        /// <param name="vGroup">The GroupViewModel to insert.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>ID of the new group.</returns>
        public int CreateGroup(GroupViewModel vGroup, string sessionKey) {
            var group = new MediaContactGroup()
            {
                Name = vGroup.Name,
                GroupType = (MediaContactGroupType)vGroup.Type,
                IsActive = true,
                CreatedByUserId = vGroup.UserId
            };

            using (var mc = new MediaContactsServiceProxy())
            {
                return mc.AddGroup(group, sessionKey);
            }
        }

        /// <summary>
        /// Updates the group.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <param name="userid">the curernt user id.</param>
        /// <returns><c>true</c> if succesful, <c>false</c> otherwise.</returns>
        public bool UpdateGroup(GroupViewModel t, int userid) {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the groups by group ids.
        /// </summary>
        /// <param name="ids">The CSV of group ids.</param>
        /// <returns>
        /// List of <c>GroupBase</c> objects.
        /// </returns>
        public List<GroupBase> GetGroupsByIds(string ids) {
            return GetGroupsByIds(ids, 0);
        }

        /// <summary>
        /// Gets the groups by group ids.
        /// </summary>
        /// <param name="ids">The CSV of group ids.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>
        /// List of <c>GroupBase</c> objects.
        /// </returns>
        public List<GroupBase> GetGroupsByIds(string ids, int userid) {
            List<GroupBase> groups = null;

            GroupBAL gBAL = new GroupBAL(Connection);
            groups = gBAL.GetGroupsByIds(ids, userid);

            return groups;
        }
    }
}