﻿using System;
using System.Collections.Generic;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.Services
{
    public interface IGroupService : IDisposable
    {
        /// <summary>
        /// Creates the group.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <param name="sessionKey">The session key of the user.</param>
        /// <returns></returns>
        int CreateGroup(GroupViewModel t, string sessionKey);

        /// <summary>
        /// Gets the group by id.
        /// </summary>
        /// <param name="groupid">The groupid.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>A GroupViewModel representing the group.</returns>
        GroupViewModel GetGroupById(int groupid, int userid);

        /// <summary>
        /// Updates the group.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        bool UpdateGroup(GroupViewModel t, int userid);

        /// <summary>
        /// Gets all the groups by a user.
        /// </summary>
        /// <param name="userId">The current user's id.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A <c>List&lt;GroupViewModel&gt;</c>.</returns>
        List<GroupViewModel> GetGroups(int userId, string sessionKey);

        /// <summary>
        /// Gets all the groups by type.
        /// </summary>
        /// <param name="type">The type of groups to retrieve.</param>
        /// <param name="userId">The current user's id.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A <c>List&lt;GroupViewModel&gt;</c>.</returns>
        List<GroupViewModel> GetGroupsByType(GroupType type, int userId, string sessionKey);

        /// <summary>
        /// Gets the groups by task ids.
        /// </summary>
        /// <param name="ids">The CSV of ids.</param>
        /// <returns>List of <c>GroupBase</c> objects.</returns>
        List<GroupBase> GetGroupsByIds(string ids);

        /// <summary>
        /// Gets the groups by ids.
        /// </summary>
        /// <param name="taskids">The CSV of ids.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>List of <c>GroupBase</c> objects.</returns>
        List<GroupBase> GetGroupsByIds(string ids, int userid);

        /// <summary>
        /// Try to rename a group.
        /// </summary>
        /// <param name="id">The id of the group.</param>
        /// <param name="name">The new name of the group.</param>
        /// <returns>True on success, false on failure.</returns>
        bool RenameGroup(int id, string name);
        //int DeleteGroupList(int groupid, int userid);
    }
}