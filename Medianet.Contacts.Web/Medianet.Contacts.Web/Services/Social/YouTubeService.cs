﻿using AutoMapper;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.Social;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace Medianet.Contacts.Web.Services.Social
{
    public class YouTubeService : IYouTubeService
    {
        #region Fields & Properties

        private const string _channelUri = "https://www.googleapis.com/youtube/v3/channels?{0}={1}&part=statistics&key={2}";

        private string _key;

        #endregion

        #region Constructors

        public YouTubeService()
        {

        }

        #endregion

        #region Private methods

        private string GetUrl(string url, bool forUsername)
        {
            string[] _url = new Uri(url).AbsolutePath.Trim('/').Split('/');

            return string.Format(_channelUri,
                forUsername ? "forUsername" : "id",
                _url.Last().Trim(),
                ConfigurationManager.AppSettings["YouTubeConsumerSecret"]);
        }

        private YouTube SendHttpRequest(string url, bool forUsername)
        {
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(GetUrl(url, forUsername)),
                    Method = HttpMethod.Get,
                };

                HttpResponseMessage response = client.SendAsync(request).Result;

                if (!response.IsSuccessStatusCode)
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                        throw new AccessViolationException($"Failed to get youtube details for {url}.", new Exception(response.Content.ReadAsStringAsync().Result));

                string jsonString = response.Content.ReadAsStringAsync().Result;

                return new JavaScriptSerializer().Deserialize<YouTube>(jsonString);
            }
        }

        #endregion

        #region Public methods

        public static YouTubeService CreateYouTubeService()
        {
            return new YouTubeService();
        }

        public YouTubeViewModel GetData(string url)
        {
            YouTube youTube = SendHttpRequest(url, true);
            YouTubeViewModel model = null;

            if (youTube == null || youTube.items.Count == 0)
                youTube = SendHttpRequest(url, false);

            if (youTube == null || youTube.items.Count == 0)
                model = new YouTubeViewModel(); // return an empty model;
            else
                model = Mapper.Map<statistics, YouTubeViewModel>(youTube.items.First().statistics);

            model.Url = url;

            return model;
        }

        #endregion
    }
}