﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Contacts.Web.ViewModels.Social;

namespace Medianet.Contacts.Web.Services.Social
{
    interface ITwitterService
    {
        Task<List<TweetViewModel>> GetUserTweets(string twitterHandle);

        Task<TwitterUserViewModel> GetUserTwitterInfo(string twitterHandle);

        Task<List<TwitterUserViewModel>> GetUsersTwitterInfo(string twitterHandles);
    }
}
