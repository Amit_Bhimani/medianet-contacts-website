﻿using Medianet.Contacts.Web.ViewModels.Social;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Contacts.Web.Services.Social
{
    interface IYouTubeService
    {
        YouTubeViewModel GetData(string username);
    }
}
