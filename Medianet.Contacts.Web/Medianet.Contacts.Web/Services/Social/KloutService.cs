﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.Social;

namespace Medianet.Contacts.Web.Services.Social
{
    public class KloutService: IKloutService
    {
        public static KloutService CreateKloutService()
        {
            return new KloutService();
        }

        private KloutIdentity GetKloutId(string twitterHandle)
        {
            var js = new JavaScriptSerializer();
            var kloutApiUrl = ConfigurationManager.AppSettings.Get("KloutApiUrl");
            var kloutKey = ConfigurationManager.AppSettings.Get("KloutKey");
            var identityUrl = string.Format("{0}/identity.json/twitter?screenName={1}&key={2}",
                kloutApiUrl, HttpUtility.UrlEncode(twitterHandle), kloutKey);
            string jsonString;

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(identityUrl).Result;

                if (response.StatusCode == HttpStatusCode.NotFound)
                    return null;

                jsonString = response.Content.ReadAsStringAsync().Result;
            }

            return js.Deserialize<KloutIdentity>(jsonString);
        }

        public KloutViewModel GetKloutScore(string twitterHandle)
        {
            var js = new JavaScriptSerializer();
            var kloutApiUrl = ConfigurationManager.AppSettings.Get("KloutApiUrl");
            var kloutKey = ConfigurationManager.AppSettings.Get("KloutKey");
            string scoreUrl;
            string jsonString;
            KloutIdentity identity;
            KloutUser user;
            var model = new KloutViewModel();

            identity = GetKloutId(twitterHandle);

            if (identity == null)
            {
                throw new AccessViolationException($"Klout user with twitter handle {twitterHandle} not found.");
            }

            scoreUrl = string.Format("{0}/user.json/{1}/score?key={2}",
                kloutApiUrl, identity.id, kloutKey);

            using (var client = new HttpClient())
            {
                jsonString = client.GetStringAsync(scoreUrl).Result;
            }

            user = js.Deserialize<KloutUser>(jsonString);

            model.TwitterHandle = twitterHandle;
            model.Score = user.score.ToString("N0");
            model.Link = $"{ConfigurationManager.AppSettings.Get("KloutUrl")}/{twitterHandle}/";

            return model;
        }
    }
}