﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.Social;

namespace Medianet.Contacts.Web.Services.Social
{
    interface IKloutService
    {
        KloutViewModel GetKloutScore(string kloutId);
    }
}
