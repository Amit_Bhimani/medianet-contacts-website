﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using AutoMapper;
using System.Threading.Tasks;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ViewModels.Social;
using System.Runtime.Caching;
using System.Net.Http.Headers;

namespace Medianet.Contacts.Web.Services.Social
{
    public class TwitterService : ITwitterService
    {
        #region Fields & Properties

        private string _apiEndpoint;
        private string _authEndpoint;
        private string _consumerKey;
        private string _consumerSecret;

        private string _accessToken
        {
            get
            {
                dynamic item = MemoryCache.Default.Get("AccessToken");

                if (item == null || string.IsNullOrEmpty(item))
                {
                    item = Authorise();

                    AddTokenToCache(item);
                }

                return item;
            }
            set
            {
                AddTokenToCache(value);
            }
        }

        private bool IsSecondAttempt;

        #endregion

        #region Constructors

        public TwitterService()
        {
            _authEndpoint = ConfigurationManager.AppSettings.Get("TwitterApiAuthUrl");
            _apiEndpoint = ConfigurationManager.AppSettings.Get("TwitterApiUrl");
            _consumerKey = ConfigurationManager.AppSettings.Get("TwitterConsumerKey");
            _consumerSecret = ConfigurationManager.AppSettings.Get("TwitterConsumerSecret");
        }

        #endregion

        #region Public methods

        public static TwitterService CreateTwitterService()
        {
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;

            return new TwitterService();
        }
        public async Task<List<TwitterUserViewModel>> GetUsersTwitterInfo(string twitterHandles)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri($"{_apiEndpoint}/users/lookup.json?screen_name={twitterHandles}&include_entities=true"),
                        Method = HttpMethod.Post,
                    };

                    request.Headers.Add("Authorization", $"Bearer {_accessToken}");

                    HttpResponseMessage response = await client.SendAsync(request);

                    if (!response.IsSuccessStatusCode)
                    {
                        var exception = new AccessViolationException($"Failed to get Twitter details for user {twitterHandles}.", new Exception(response.Content.ReadAsStringAsync().Result));

                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                            throw exception;

                        return new List<TwitterUserViewModel> {  };
                    }

                    string jsonString = response.Content.ReadAsStringAsync().Result;

                    var data = new JavaScriptSerializer().Deserialize<List<TwitterUser>>(jsonString);

                    List<TwitterUserViewModel> model = Mapper.Map<List<TwitterUser>, List<TwitterUserViewModel>>(data);

                    return model;
                }
            }
            catch (AccessViolationException ex)
            {
                if (IsSecondAttempt)
                    throw ex;

                IsSecondAttempt = true;
                MemoryCache.Default.Remove("AccessToken");
                return await GetUsersTwitterInfo(twitterHandles);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TwitterUserViewModel> GetUserTwitterInfo(string twitterHandle)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri($"{_apiEndpoint}/users/show.json?&screen_name={twitterHandle}&include_entities=true"),
                        Method = HttpMethod.Get,
                    };

                    request.Headers.Add("Authorization", $"Bearer {_accessToken}");

                    HttpResponseMessage response = await client.SendAsync(request);

                    if (!response.IsSuccessStatusCode)
                    {
                        var exception = new AccessViolationException($"Failed to get Twitter details for user {twitterHandle}.", new Exception(response.Content.ReadAsStringAsync().Result));

                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                            throw exception;

                        return new TwitterUserViewModel { Handle = twitterHandle };
                    }

                    string jsonString = response.Content.ReadAsStringAsync().Result;

                    var data = new JavaScriptSerializer().Deserialize<TwitterUser>(jsonString);

                    TwitterUserViewModel model = Mapper.Map<TwitterUser, TwitterUserViewModel>(data);

                    return model;
                }
            }
            catch (AccessViolationException ex)
            {
                if (IsSecondAttempt)
                    throw ex;

                IsSecondAttempt = true;
                MemoryCache.Default.Remove("AccessToken");
                return await GetUserTwitterInfo(twitterHandle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<TweetViewModel>> GetUserTweets(string twitterHandle)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri($"{_apiEndpoint}/statuses/user_timeline.json?screen_name={twitterHandle}&count=" + ConfigurationManager.AppSettings["UserTweetsCount"]),
                        Method = HttpMethod.Get,
                    };

                    request.Headers.Add("Authorization", $"Bearer {_accessToken}");

                    HttpResponseMessage response = await client.SendAsync(request);
                    
                    if (!response.IsSuccessStatusCode)
                    {
                        var exception = new AccessViolationException($"Failed to get Twitter details for user {twitterHandle}.", new Exception(response.Content.ReadAsStringAsync().Result));

                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                            throw exception;

                        return new List<TweetViewModel>();
                    }

                    string jsonString = await response.Content.ReadAsStringAsync();

                    var data = new JavaScriptSerializer().Deserialize<List<Tweet>>(jsonString);

                    List<TweetViewModel> model = Mapper.Map<List<Tweet>, List<TweetViewModel>>(data);

                    return model;
                }
            }
            catch (AccessViolationException ex)
            {
                if (IsSecondAttempt)
                    throw ex;

                IsSecondAttempt = true;

                MemoryCache.Default.Remove("AccessToken");

                return await GetUserTweets(twitterHandle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private methods

        private void AddTokenToCache(string token)
        {
            CacheItemPolicy policy = new CacheItemPolicy
            {
                Priority = CacheItemPriority.NotRemovable
            };

            MemoryCache.Default.Add("AccessToken", token, policy);
        }

        private string Authorise()
        {
            string authorizationCode = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{HttpUtility.UrlEncode(_consumerKey)}:{HttpUtility.UrlEncode(_consumerSecret)}"));

            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(_authEndpoint),
                    Method = HttpMethod.Post,
                };

                request.Headers.Add("Authorization", $"Basic {authorizationCode}");
                request.Content = new StringContent("grant_type=client_credentials", Encoding.UTF8, "application/x-www-form-urlencoded");

                HttpResponseMessage response = client.SendAsync(request).Result;

                if (!response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    throw new AccessViolationException("Failed to authorise", new Exception(result));
                }

                string jsonString = response.Content.ReadAsStringAsync().Result;

                var resp = new JavaScriptSerializer().Deserialize<TwitterAuth>(jsonString);

                return resp.access_token;
            }
        }

        #endregion
    }
}