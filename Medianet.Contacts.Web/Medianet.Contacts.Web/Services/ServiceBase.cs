﻿using Medianet.Contacts.Web.Common;

namespace Medianet.Contacts.Web.Services
{
    public class ServiceBase
    {
        public const string CACH_LIVING_LIST_KEY = "living-lists";
        public const int CACH_LIVING_LIST_TIME = 1800;

        /// <summary>
        /// The database connection.
        /// </summary>
        public DBClass Connection { get; set; }

        public void Dispose() {
            if (Connection != null) {
                Connection.Dispose();
                Connection = null;
            }
        }

        public ServiceBase(DBClass db) {
            Connection = db;
        }
    }
}