﻿using Medianet.Contacts.Web.ViewModels.Dashboard;
using System.Collections.Generic;
using Medianet.Contacts.Web.GeneralService;
using Medianet.Contacts.Web.ViewModels.AllLists;

namespace Medianet.Contacts.Web.Services.Dashboard
{
    public interface IDashboardService
    {
        List<MediaMovementsViewModel> GetMediaMovements(bool includeHidden, int pageNo, int pageSize, Common.Model.User currentUser);

        List<ContactSummaryViewModel> GetRecentlyAdded();

        List<ContactSummaryViewModel> GetRecentlyUpdated(int total, Common.Model.User currentUser);

        List<ListViewModel> GetRecentlyUpdatedLists(Common.Model.User currentUser);

        MediaStatistics GetMediaStatistics();
    }
}
