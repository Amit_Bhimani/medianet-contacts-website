﻿using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.BAL;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.ProxyWrappers;
using Medianet.Contacts.Web.ViewModels.Contact;
using Org.BouncyCastle.Asn1.X509.Qualified;
using Medianet.Contacts.Web.GeneralService;
using Medianet.Contacts.Web.MediaContactsService;
using Medianet.Contacts.Web.ViewModels;
using Medianet.Contacts.Web.ViewModels.AllLists;
using SocialInfo = Medianet.Contacts.Web.Common.Model.SocialInfo;

namespace Medianet.Contacts.Web.Services.Dashboard
{
    public class DashboardService : ServiceBase, IDashboardService
    {
        private DashboardService(DBClass db) : base(db) { }

        /// <summary>
        /// Create a new instance of the <c>DashboardService</c> class.
        /// </summary>
        /// <returns>A new instance of the <c>DashboardService</c> class.</returns>
        public static DashboardService CreateDashboardService()
        {
            var db = new DBClass();
            return new DashboardService(db);
        }

        public List<MediaMovementsViewModel> GetMediaMovements(bool includeHidden, int pageNo, int pageSize, Common.Model.User currentUser)
        {
            List<MediaMovementsViewModel> result = new List<MediaMovementsViewModel>();
           
            using (var mc = new MediaContactsServiceProxy())
            {

                var movements = mc.GetMediaMovements(includeHidden, pageNo, pageSize, currentUser.SessionKey);
                result =  Mapper.Map<List<MediaContactsService.MediaMovement>, List<MediaMovementsViewModel>>(movements);
            }

            return result;

        }

        public List<ContactSummaryViewModel> GetRecentlyAdded()
        {
            var result = new List<ContactSummaryViewModel>();
            var bal = new OutletContactBALBase(Connection);
            var contactDetails = bal.GetRecentlyAddedItems();

            if (contactDetails != null)
            {
                foreach (var recent in contactDetails)
                {
                    var cs = new ContactSummaryViewModel();

                    cs.Name = new ContactName();
                    cs.SocialDetails = new SocialInfo();
                    cs.OutletId = recent.OutletId;
                    cs.ContactId = recent.ContactId;
                    cs.Type = recent.Type;
                    cs.CreatedDate = recent.CreatedDate;

                    if (recent.GetType() == typeof (ContactBase))
                    {
                        var contact = (ContactBase) recent;
                        cs.Name = contact.Name;
                        cs.JobTitle = contact.JobTitle;
                        cs.SocialDetails = contact.SocialDetails;
                        cs.CompanyName = contact.CompanyName;
                    }
                    else
                    {
                        var outlet = (OutletBase)recent;
                        cs.CompanyName = outlet.Name;
                        cs.LogoFileName = outlet.LogoFileName;
                        cs.MediaType = outlet.MediaType;
                    }

                    result.Add(cs);
                }
            }

            return result.OrderByDescending(r => r.CreatedDate).Take(3).ToList();
        }

        public List<ContactSummaryViewModel> GetRecentlyUpdated(int total,Common.Model.User currentUser)
        {
            var result = new List<ContactSummaryViewModel>();

            using (var mc = new MediaContactsServiceProxy())
            {

                var recentUpdates = mc.GetMediaContactRecentlyUpdated(total, currentUser.SessionKey);
                
                if (recentUpdates != null)
                {
                    foreach (var recent in recentUpdates)
                    {
                        var cs = new ContactSummaryViewModel
                        {
                            Name = new ContactName()
                            {
                                FirstName = recent.FirstName,
                                LastName = recent.LastName
                            },
                            OutletId = recent.OutletId,
                            ContactId = recent.ContactId,
                            Type = (RecordType) recent.Type,
                            CreatedDate = recent.Date,
                            JobTitle = recent.JobTitle,
                            CompanyName = recent.CompanyName
                        };

                        result.Add(cs);
                    }
                }

                return result.OrderByDescending(r => r.CreatedDate).Take(3).ToList();
            }
        }

        public List<ListViewModel> GetRecentlyUpdatedLists(Common.Model.User currentUser)
        {
            List<MediaContactsService.MediaContactList> lists;

            using (var mc = new MediaContactsServiceProxy())
            {
                lists = mc.GetRecentLists(currentUser.UserID, currentUser.SessionKey);
            }

            List<ListViewModel> vlists = null;
            vlists = (from l in lists
                      select new ListViewModel
                      {
                          ListName = l.Name,
                          ListId = l.Id,
                          Visibility = l.IsPrivate ? "Private" : "Public",
                          CreatedDate = l.CreatedDate
                      }).ToList<ListViewModel>();

            return vlists;
        }

        /// <summary>
        /// Gets media statistics
        /// </summary>
        public MediaStatistics GetMediaStatistics()
        {
            using (var gs = new GeneralServiceProxy())
            {
                MediaStatistics ms = gs.GetMediaStatistics();
                return ms;
            }
        }
    }
}