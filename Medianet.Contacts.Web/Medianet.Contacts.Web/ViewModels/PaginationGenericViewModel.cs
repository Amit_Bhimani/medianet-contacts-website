﻿using System;
using System.Collections.Generic;
using System.Web;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels.Grid;

namespace Medianet.Contacts.Web.ViewModels
{
    public class PaginationGenericViewModel : GridViewModelBase
    {
        private const string CookiePageSize = "SearchPageSize";

        public PaginationGenericViewModel()
        {
            Expand = "";
            IsFirstPage = true;
            IsLastPage = false;
            SortColumn = SortColumn.Default;
            SortDirection = SortDirection.ASC;
            CheckboxState = CheckboxState.All;
            NumberInNavigation = 5;
            PageSizes = new List<int>() { 25, 50, 100 };
            UseCookieForPageSize = true;
        }

        /// <summary>
        /// The current page.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// First page number in navigation numbers.
        /// </summary>
        public int StartPage { get; set; }

        /// <summary>
        /// Last page number in navigation numbers.
        /// </summary>
        public int EndPage { get; set; }

        /// <summary>
        /// Number of pages to display between the start and end pagination
        /// numbers.
        /// </summary>
        public int NumberInNavigation { get; set; }

        /// <summary>
        /// The total number of pages.
        /// </summary>
        public int NumberOfPages { get; set; }

        /// <summary>
        /// The range of page sizes available
        /// </summary>
        public List<int> PageSizes { get; set; }

        /// <summary>
        /// Default page size.
        /// </summary>
        public int PageSizeDefault = 50;

        /// <summary>
        /// Number of results to display per page.
        /// </summary>
        private int _pageSize;
        public virtual int PageSize
        {
            get
            {
                if (_pageSize == 0)
                {
                    var pageFromCookie = HttpContext.Current.Request.Cookies[CookiePageSize];

                    if (pageFromCookie != null && UseCookieForPageSize)
                    {
                        if (int.TryParse(pageFromCookie.Value, out _pageSize))
                        {
                            SetPageSizeCookie(_pageSize);
                            return _pageSize;
                        }
                    }

                    _pageSize = PageSizeDefault;
                    return _pageSize;
                }
                else
                    return _pageSize;
            }
            set
            {
                _pageSize = value;

                if (UseCookieForPageSize)
                    SetPageSizeCookie(_pageSize);
            }
        }

        public bool UseCookieForPageSize { get; set; }

        private void SetPageSizeCookie(int pageSize)
        {
            var pageSizeCookie = new HttpCookie(CookiePageSize);
            pageSizeCookie.Value = pageSize.ToString();
            pageSizeCookie.Expires = DateTime.MaxValue;
            pageSizeCookie.Secure = false;
            pageSizeCookie.HttpOnly = true;
            //pageSizeCookie.Domain = ConfigHelper.CookieDomain;
            HttpContext.Current.Response.Cookies.Add(pageSizeCookie);
        }

        /// <summary>
        /// Calculates the pagination values using the result count and page size.
        /// </summary>
        public void CalculatePagination()
        {
            if (ResultCount > 0)
            {
                // If the result count cannot be divided evenly by the page size,
                // we add an additional page for the remaining records.
                NumberOfPages = (ResultCount % PageSize > 0 ? 1 : 0) +
                    (ResultCount / (PageSize == 0 ? PageSizeDefault : PageSize));

                // Page can't be 0 and can't be greater than total pages.
                // If it is reset to page 1.
                Page = Page == 0 || Page > NumberOfPages ? 1 : Page;

                // If we only have one page, set current page only to true.
                //if (NumberOfPages == 1) CurrentPageOnly = true;

                IsLastPage = Page == NumberOfPages;
                IsFirstPage = Page == 1;

                StartPage = Page;
                EndPage = StartPage + NumberInNavigation;
            }
        }

        /// <summary>
        /// The total number of results.
        /// </summary>
        public int ResultCount { get; set; }

        /// <summary>
        /// The column that is being sorted.
        /// </summary>
        public SortColumn SortColumn { get; set; }

        /// <summary>
        /// Direction of the sorting.
        /// </summary>
        public SortDirection SortDirection { get; set; }

        /// <summary>
        /// Expanded contact.
        /// </summary>
        public string Expand { get; set; }

        /// <summary>
        /// True if the current page is the last page.
        /// </summary>
        public bool IsLastPage { get; set; }

        /// <summary>
        /// True if the current page is the first page.
        /// </summary>
        public bool IsFirstPage { get; set; }
    }
}