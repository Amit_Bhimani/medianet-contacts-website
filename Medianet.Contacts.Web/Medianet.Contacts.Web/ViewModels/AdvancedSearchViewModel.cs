﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.BO;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Model.Interfaces;

namespace Medianet.Contacts.Web.ViewModels
{
    public class AdvancedSearchViewModel
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public AdvancedSearchViewModel() {
            ShowResults = false;
            //PrefersEmail = true;
            //PrefersFax = true;
            //PrefersMail = true;
            //PrefersTwitter = true;
            //PrefersLinkedIn = true;
            //PrefersFacebook = true;
            //PrefersInstagram = true;
            //PrefersSkype = true;

            ExcludePoliticians = true;
            ExcludeFinanceInst = true;
            ExcludeOutOfOffice = false;
            RecordsToShow = ShowResultType.A;
        }

        /// <summary>
        /// True if we are showing search results, false if we need to show the form.
        /// </summary>
        public bool ShowResults { get; set; }

        /// <summary>
        /// Type, though this should always be advanced search.
        /// </summary>
        public SearchType Type = SearchType.Advanced;

        // Any Field
        [Display(Name = "Keywords")]
        [AdditionalMetadata("ContactField", "")]
        public string SearchText { get; set; }

        // Contact
        [Display(Name = "First name")]
        [AdditionalMetadata("ContactField", "")]
        public string FirstName { get; set; }
        [Display(Name = "Surname")]
        [AdditionalMetadata("ContactField", "")]
        public string LastName { get; set; }
        [Display(Name = "FullName")]
        [AdditionalMetadata("ContactField", "")]
        public string FullName { get; set; }
        [Display(Name = "Job title")]
        [AdditionalMetadata("ContactField", "")]
        public string JobTitle { get; set; }
        [Display(Name = "Only display \"Primary News Contact\"")]
        [AdditionalMetadata("default", "No")]
        [AdditionalMetadata("ContactField", "false")]
        public bool PrimaryNewsContactOnly { get; set; }

        // Outlet
        [Display(Name = "Name")]
        [AdditionalMetadata("OutletField", "")]
        public string OutletName { get; set; }
        [Display(Name = "Media type")]
        [AdditionalMetadata("OutletField", "")]
        public List<MediaType> MediaTypes { get; set; }
        [Display(Name = "Circulation min")]
        [AdditionalMetadata("OutletField", "")]
        public string OutletCirculationMin { get; set; }
        [Display(Name = "Circulation max")]
        [AdditionalMetadata("OutletField", "")]
        public string OutletCirculationMax { get; set; }

        // Acts as a data source for dropdowns
        public List<Position> Positions { get; set; }
        public List<Subject> Subjects { get; set; }
        public List<SubjectGroup> SubjectGroups { get; set; }
        public List<OutletFrequency> Frequencies { get; set; }
        public List<Language> Languages { get; set; }
        public List<Continent> Continents { get; set; }


        // Selected fields - this is to pre-load or keep state accross postbacks
        // Contact 
        [Display(Name = "Position")]
        [AdditionalMetadata("ContactField", "")]
        public List<Position> SelectedPositions { get; set; }
        [Display(Name = "Subject")]
        [AdditionalMetadata("ContactField", "")]
        public List<Subject> SelectedCSubjects { get; set; }
        [Display(Name = "Subject group")]
        [AdditionalMetadata("ContactField", "")]
        public List<SubjectGroup> SelectedCSubjectGroups { get; set; }

        [Display(Name = "Influencer score")]
        [AdditionalMetadata("ContactField", "")]
        public string ContactInfluencerScore { get; set; }

        //outlet 
        [Display(Name = "Subject")]
        [AdditionalMetadata("OutletField", "")]
        public List<Subject> SelectedOSubjects { get; set; }
        [Display(Name = "Subject group")]
        [AdditionalMetadata("OutletField", "")]

        public List<SubjectGroup> SelectedOSubjectGroups { get; set; }

        //contact geo
        [Display(Name = "Continent")]
        [AdditionalMetadata("ContactField", "")]
        public List<Continent> SelectedCContinents { get; set; }
        [Display(Name = "Country")]
        [AdditionalMetadata("ContactField", "")]
        public List<Common.Model.Country> SelectedCCountries { get; set; }
        [Display(Name = "State")]
        [AdditionalMetadata("ContactField", "")]
        public List<Common.Model.State> SelectedCStates { get; set; }
        [Display(Name = "City")]
        [AdditionalMetadata("ContactField", "")]
        public List<Common.Model.City> SelectedCCities { get; set; }
        [Display(Name = "Postcode")]
        [AdditionalMetadata("ContactField", "")]
        public string SelectedCPostCodes { get; set; }



        //outlet geo
        [Display(Name = "Continent")]
        [AdditionalMetadata("OutletField", "")]
        public List<Continent> SelectedOContinents { get; set; }
        [Display(Name = "Country")]
        [AdditionalMetadata("OutletField", "")]
        public List<Common.Model.Country> SelectedOCountries { get; set; }
        [Display(Name = "State")]
        [AdditionalMetadata("OutletField", "")]
        public List<Common.Model.State> SelectedOStates { get; set; }
        [Display(Name = "City")]
        [AdditionalMetadata("OutletField", "")]
        public List<Common.Model.City> SelectedOCities { get; set; }
        [Display(Name = "Postcode")]
        [AdditionalMetadata("OutletField", "")]
        public string SelectedOPostCodes { get; set; }
        [Display(Name = "Include \"Metro\"")]
        [AdditionalMetadata("default", "No")]
        [AdditionalMetadata("OutletField", "false")]
        public bool IncludeMetroOutlets { get; set; }
        [Display(Name = "Include \"Regional\"")]
        [AdditionalMetadata("default", "No")]
        [AdditionalMetadata("OutletField", "false")]
        public bool IncludeRegionalOutlets { get; set; }
        [Display(Name = "Include \"Suburban\"")]
        [AdditionalMetadata("default", "No")]
        [AdditionalMetadata("OutletField", "false")]
        public bool IncludeSuburbanOutlets { get; set; }

        //outlet details
        [Display(Name = "Media type")]
        [AdditionalMetadata("OutletField", "")]
        public List<MediaType> SelectedMediaTypes { get; set; }
        [Display(Name = "Language")]
        [AdditionalMetadata("OutletField", "")]
        public List<Language> SelectedLanguages { get; set; }
        [Display(Name = "Frequency")]
        [AdditionalMetadata("OutletField", "")]
        public List<OutletFrequency> SelectedFrequencies { get; set; }
        [Display(Name = "Station frequency")]
        [AdditionalMetadata("OutletField", "")]
        public string OutletStationFrequencyList { get; set; }

        // Notes section.
        [Display(Name = "System notes")]
        public string SystemNotes { get; set; }
        [Display(Name = "My notes")]
        public string MyNotes { get; set; }

        // Communication Preferences section.
        [Display(Name = "Prefers email")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersEmail { get; set; }
        [Display(Name = "Prefers fax")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersFax { get; set; }

        [Display(Name = "Prefers mail")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersMail { get; set; }

        [Display(Name = "Prefers Phone")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersPhone { get; set; }
        [Display(Name = "Prefers Mobile")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersMobile { get; set; }

        [Display(Name = "Prefers Twitter")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersTwitter { get; set; }

        [Display(Name = "Prefers LinkedIn")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersLinkedIn { get; set; }

        [Display(Name = "Prefers Facebook")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersFacebook { get; set; }

        [Display(Name = "Prefers Instagram")]
        [AdditionalMetadata("default", "No")]
        public bool PrefersInstagram { get; set; }

        // Show only results with section.
        [Display(Name = "Show only with Twitter")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithTwitter { get; set; }
        [Display(Name = "Show only with LinkedIn")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithLinkedIn { get; set; }
        [Display(Name = "Show only with Facebook")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithFacebook { get; set; }

        [Display(Name = "Show only with Instagram")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithInstagram { get; set; }

        [Display(Name = "Show only with YouTube")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithYouTube { get; set; }

        [Display(Name = "Show only with Snapchat")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithSnapchat { get; set; }

        [Display(Name = "Show only with email")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithEmail { get; set; }
        [Display(Name = "Show only with fax")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithFax { get; set; }
        [Display(Name = "Show only with postal address")]
        [AdditionalMetadata("default", "No")]
        public bool ShowOnlyWithPostalAddress { get; set; }

        //Lists section
        public List<ListBase> Lists { get; set; }
        [Display(Name = "List")]
        public List<int> SelectedListIds { get; set; }

        // Exclusion section.     
        [Display(Name = "Records To Show")]
        [AdditionalMetadata("default", "All Records")]
        public ShowResultType RecordsToShow { get; set; }

        [Display(Name = "Excluded keywords")]
        public string ExcludeKeywords { get; set; }
        [Display(Name = "Exclude offices, programmes, supplements")]
        [AdditionalMetadata("default", "No")]
        public bool ExcludeOPS { get; set; }
        [Display(Name = "Exclude financial institutes")]
        [AdditionalMetadata("default", "Yes")]
        public bool ExcludeFinanceInst { get; set; }
        [Display(Name = "Exclude politicians")]
        [AdditionalMetadata("default", "Yes")]
        public bool ExcludePoliticians { get; set; }

        [Display(Name = "Exclude Out of Office")]
        [AdditionalMetadata("default", "No")]
        public bool ExcludeOutOfOffice { get; set; }
        
        public List<SelectListItem> TaskStatusTypes {
            get {
                List<SelectListItem> statusTypes = new List<SelectListItem>() {
                  new SelectListItem() { Text = "--- Select a status ---", Value = "-1" },
                  new SelectListItem() { Text = "Not Started", Value = Medianet.Contacts.Wcf.DataContracts.DTO.TaskStatus.Not_Started.ToString() },
                  new SelectListItem() { Text = "In Progress", Value = Medianet.Contacts.Wcf.DataContracts.DTO.TaskStatus.In_Progress.ToString() },
                  new SelectListItem() { Text = "Completed", Value = Medianet.Contacts.Wcf.DataContracts.DTO.TaskStatus.Completed.ToString() },
                  new SelectListItem() { Text = "On Hold", Value = Medianet.Contacts.Wcf.DataContracts.DTO.TaskStatus.On_Hold.ToString() }
            };
                return statusTypes;
            }
        }

        [Display(Name = "Influencer score")]
        public List<SelectListItem> MediaInfluencerScores
        {
            get
            {
                List<SelectListItem> scores = new List<SelectListItem>()
                {
                    new SelectListItem() { Selected = false,  Text = "Influencer score - none", Value = ""},
                    new SelectListItem() { Selected = false, Text = "> 0", Value = "> 0"},
                    new SelectListItem() { Selected = false, Text = "> 25", Value = "> 25"},
                    new SelectListItem() { Selected = false, Text = "> 50", Value = "> 50"},
                    new SelectListItem() { Selected = false, Text = "> 75", Value = "> 75"}
                };
                return scores;
            }

        }


        [Display(Name = "Within list")]
        public List<ListBase> SelectedLists {
            get {
                List<ListBase> lists = null;

                if (this.SelectedListIds != null && this.Lists != null) {
                    lists = this.Lists.Where(lst => this.SelectedListIds.Contains(lst.Id)).ToList();
                }

                return lists;
            }
        }

        public SearchContext GetContext() {
            Type svt = typeof(AdvancedSearchViewModel);
            Type attrType = typeof(AdditionalMetadataAttribute);
            bool people = false;
            bool outlet = false;

            foreach (PropertyInfo prop in svt.GetProperties().ToList()) {
                if (people)
                    break;
                var attr = prop.GetCustomAttributes(attrType, false);

                if (attr != null && attr.Count() > 0) {
                    var cf = attr.ToList().Where(atr => (atr as AdditionalMetadataAttribute).Name.Equals("ContactField")).ToList();
                    var of = attr.ToList().Where(atr => (atr as AdditionalMetadataAttribute).Name.Equals("OutletField")).ToList();
                    string defaultValue = null;

                    //get the defaultValue. We could get it from the defaultValue attribute but that is primarily for display purposes on Selection Criteria.
                    if (cf != null && cf.Count() > 0) {
                        AdditionalMetadataAttribute cfd = (AdditionalMetadataAttribute)attr.ToList().Where(atr => (atr as AdditionalMetadataAttribute).Name.Equals("ContactField")).FirstOrDefault();
                        if (cfd != null)
                            defaultValue = cfd.Value.ToString();
                    }
                    else {
                        AdditionalMetadataAttribute ofd = (AdditionalMetadataAttribute)attr.ToList().Where(atr => (atr as AdditionalMetadataAttribute).Name.Equals("OutletField")).FirstOrDefault();
                        if (ofd != null)
                            defaultValue = ofd.Value.ToString();
                    }

                    if (cf != null && of != null && (cf.Count() > 0 || of.Count() > 0)) {
                        var value = prop.GetValue(this, null);

                        switch (prop.PropertyType.FullName) {
                            case "System.String":
                                string strVal = prop.GetValue(this, null) as string;
                                if (!string.IsNullOrWhiteSpace(strVal) && (defaultValue == null || !defaultValue.Equals(strVal)))
                                    if (cf.Count() > 0)
                                        people = true;
                                    else
                                        outlet = true;
                                break;
                            case "System.Boolean":
                                bool bVal = (bool)prop.GetValue(this, null);
                                if ((defaultValue == null || !defaultValue.Equals(bVal.ToString(), StringComparison.CurrentCultureIgnoreCase)) || bVal) {
                                    if (cf.Count() > 0)
                                        people = true;
                                    else
                                        outlet = true;
                                }
                                break;
                            default:
                                if (prop.PropertyType.IsGenericType && value is IEnumerable<IModel>) {
                                    IEnumerable<IModel> mVal = prop.GetValue(this, null) as IEnumerable<IModel>;
                                    if (mVal != null && mVal.Count() > 0) {
                                        if (cf.Count() > 0)
                                            people = true;
                                        else
                                            outlet = true;
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            if (people)
                return SearchContext.People;
            else if (outlet)
                return SearchContext.Outlet;

            return SearchContext.Both;
        }
    }
}

