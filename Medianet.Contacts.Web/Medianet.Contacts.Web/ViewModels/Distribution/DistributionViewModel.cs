﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.ViewModels.Distribution
{
    public class DistributionViewModel
    {
        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [Display(Name = "Customer Id")]
        public string DebtorNumber { get; set; }
        public string SenderName { get; set; }
        [Required]
        [Display(Name = "Work Phone Number")]
        public string WorkPhoneNumber { get; set; }
        [Required]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
        public string AfterHoursNumber { get; set; }
        public string CustomerReference { get; set; }
        public string SelectedLists { get; set; }
        public List<int> SelectedListIds { get; set; }
        public bool DistributionWire { get; set; }

        public bool WireAll { get; set; }
        public bool WireTas { get; set; }
        public bool WireVic { get; set; }
        public bool WireNsw { get; set; }
        public bool WireAct { get; set; }
        public bool WireSa { get; set; }
        public bool WireWa { get; set; }
        public bool WireQld { get; set; }
        public bool WireNt { get; set; }

        public bool OnHold { get; set; }
        public string HoldTime { get; set; }
        public string SpecialInstructions { get; set; }
        public HttpPostedFileBase Release { get; set; }
        public HttpPostedFileBase Attachment { get; set; }
        public int ReleaseId { get; set; }
    }
}
