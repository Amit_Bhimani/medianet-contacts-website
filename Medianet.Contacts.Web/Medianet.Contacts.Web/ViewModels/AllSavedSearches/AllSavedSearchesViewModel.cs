﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels.AllSavedSearches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.AllSavedSearches
{
    public class SavedSearchGroupedViewModel : PaginationGenericViewModel
    {
        public List<SavedSearchViewModel> Lists { get; set; }
        public int GroupId { get; set; }
        public AllSavedSearchesSortColumn AllSavedSearchesSortColumn { get; set; }
    }
}