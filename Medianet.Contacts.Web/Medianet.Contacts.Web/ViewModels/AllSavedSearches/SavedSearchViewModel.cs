﻿using System;
using System.ComponentModel.DataAnnotations;
using Medianet.Contacts.Web.ViewModels.Grid;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.ViewModels.AllSavedSearches
{
    public class SavedSearchViewModel : PopupViewModelBase
    {
        public int SearchId { get; set; }
        public string Query { get; set; }
        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }
        public string Criteria { get; set; }
        public bool IsQuickSearch { get; set; }
        public int ResultsCount { get; set; }
        public bool Status { get; set; }
        public bool IsPrivate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public GroupViewModel Group { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public SavedSearchViewModel()
        {
            Group = new GroupViewModel();
        }
    }
}