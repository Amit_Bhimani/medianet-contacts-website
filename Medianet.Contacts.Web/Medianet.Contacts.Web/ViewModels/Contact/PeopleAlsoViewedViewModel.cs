﻿using System;
using System.Collections.Generic;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.NewsCentreArticle;
using Medianet.Contacts.Web.ViewModels.Outlet;

namespace Medianet.Contacts.Web.ViewModels.Contact
{
    public class PeopleAlsoViewedViewModel 
    {
        public string ContactId { get; set; }
        public ContactName Contact { get; set; }
        public string OutletId { get; set; }
        public string OutletName { get; set; }
        public int RecordType { get; set; }
        public string Title { get; set; }
        public bool IsGeneric { get; set; }
        public string TwitterHandle { get; set; }
    }
}