﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels
{
    public class PitchingProfileViewModel
    {
        public string BugBears { get; set; }
        public string AlsoKnownAs { get; set; }
        public string PressReleaseInterests { get; set; }
        public string Awards { get; set; }
        public string PersonalInterests { get; set; }
        public string PoliticalParty { get; set; }
        public string NamePronunciation { get; set; }
        public string AppearsIn { get; set; }
        public string CurrentStatus { get; set; }
        public string BasedInLocation { get; set; }
        public string Deadlines { get; set; }
        public string BestContactTime { get; set; }
        public string BusyTimes { get; set; }
        public string AuthorOf { get; set; }
        public string GiftsAccepted { get; set; }
        public string CoffeeOrder { get; set; }
        public string PitchingDos { get; set; }
        public string PitchingDonts { get; set; }

    }
}