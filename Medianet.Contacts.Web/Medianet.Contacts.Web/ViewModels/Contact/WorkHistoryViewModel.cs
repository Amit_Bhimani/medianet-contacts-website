﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Contact
{
    public class WorkHistoryViewModel
    {
        public string ContactId { get; set; }
        public string JobTitle { get; set; }

        public int StartYear { get; set; }
        public int? StartMonth { get; set; }
        public int? EndYear { get; set; }
        public int? EndMonth { get; set; }

        public string OutletId { get; set; }
        public string CompanyName { get; set; }
        public string LogoFileName { get; set; }
        public string Location { get; set; }

        public string DateRange
        {
            get
            {
                var result = string.Empty;

                if (StartMonth.HasValue)
                {
                    var startDate = new DateTime(StartYear, StartMonth.Value, 1);
                    result += startDate.ToString("MMM yyyy");
                }
                else
                    result += StartYear.ToString();

                result += " - ";

                if (EndYear.HasValue)
                {
                    if (EndMonth.HasValue)
                    {
                        var endDate = new DateTime(EndYear.Value, EndMonth.Value, 1);
                        result += endDate.ToString("MMM yyyy");
                    }
                    else
                        result += EndYear.ToString();
                }
                else
                    result += "Present";

                return result;
            }
        }

        public string Period
        {
            get
            {
                string result = string.Empty;
                int localEndYear;

                if (!EndYear.HasValue)
                    localEndYear = DateTime.Now.Year;
                else
                    localEndYear = EndYear.Value;

                var startDate = new DateTime(StartYear, StartMonth ?? 1, 1);
                var endDate = new DateTime(localEndYear, EndMonth ?? 12, 1);
                var zeroTime = new DateTime(1, 1, 1);

                if (startDate < endDate)
                {
                    TimeSpan span = endDate - startDate;
                    int years = (zeroTime + span).Year - 1;

                    if (years > 0)
                        result = String.Format("{0} {1}", years, years > 1 ? "yrs" : "yr");

                    if (EndMonth.HasValue)
                    {
                        int months = (zeroTime + span).Month - 1;

                        if (months > 0 || years == 0)
                            result += String.Format("{0}{1} {2}", result.Length > 0 ? " " : "", months, months > 1 ? "mos" : "mo");
                    }
                }

                return result;
            }
        }
    }
}