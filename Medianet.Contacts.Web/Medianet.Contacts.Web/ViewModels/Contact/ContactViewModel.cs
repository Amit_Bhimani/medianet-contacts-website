﻿using System.Collections.Generic;
using Medianet.Contacts.Web.Common.Model;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.NewsCentreArticle;
using Medianet.Contacts.Web.ViewModels.Outlet;

namespace Medianet.Contacts.Web.ViewModels.Contact
{
    public class ContactViewModel : ContactOutletBaseViewModel
    {
        public ContactName Name { get; set; }
        public string JobTitle { get; set; }
        public Subject PrimarySubject { get; set; }
        public bool? PrimaryContact { get; set; }
        public ICollection<Position> Positions { get; set; }
        public PagedList<Article> Articles { get; set; }
        public OutletViewModel AtOutlet { get; set; }
        public ICollection<OutletBaseViewModel> Outlets { get; set; }
        public string Bio { get; set; }   
        public PitchingProfileViewModel PitchingProfileViewModel { get; set; }

        public List<WorkHistoryViewModel> WorkHistory { get; set; }
        
        public int? MediaInfluencerScore { get; set; }
        public string PriorityNotice { get; set; }
        public string InterviewLink { get; set; }
        public string InterviewPhotoLink { get; set; }
        public string Initials
        {
            get
            {
                var result = string.Empty;
                result += (string.IsNullOrEmpty(this.Name.FirstName) ? string.Empty : this.Name.FirstName.Substring(0, 1));
                result += (string.IsNullOrEmpty(this.Name.LastName) ? string.Empty : this.Name.LastName.Substring(0, 1));

                return result.Trim().ToUpper();
            }
        }
    }
}