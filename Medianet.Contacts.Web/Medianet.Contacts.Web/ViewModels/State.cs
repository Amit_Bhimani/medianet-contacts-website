﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.ViewModels
{
    public class State
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public List<City> Cities { get; set; }
    }
}