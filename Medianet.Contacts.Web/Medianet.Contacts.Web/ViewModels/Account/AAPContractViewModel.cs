﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Account
{
    public class AAPContractViewModel
    {
        public string Name { get; set; }
        public string Product { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Decimal? ContractPrice { get; set; }
        public int? ContractTerm { get; set; }


    }
}