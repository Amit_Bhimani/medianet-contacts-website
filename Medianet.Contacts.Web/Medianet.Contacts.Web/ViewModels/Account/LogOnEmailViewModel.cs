﻿using System.ComponentModel.DataAnnotations;

namespace Medianet.Contacts.Web.ViewModels.Account
{
    public class LogOnEmailViewModel
    {
        public LogOnEmailViewModel() {
            RememberMe = true;
        }

        [Required]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}