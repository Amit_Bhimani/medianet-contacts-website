﻿using System.ComponentModel.DataAnnotations;

namespace Medianet.Contacts.Web.ViewModels.Account
{
    public class ChangePasswordViewModel
    {
        public const int MinPasswordLength = 8;
        public const int MaxPasswordLength = 16;
        public const string PasswordError = "Please enter password 8 - 16 characters in length and made up of alpha and numeric characters.";

        [Required]
        [StringLength(MaxPasswordLength, ErrorMessage = PasswordError, MinimumLength = MinPasswordLength)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [RegularExpression("^((?=.*[a-zA-Z])(?=.*\\d)).+$", ErrorMessage = PasswordError)]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string SessionKey { get; set; }

        public string ReturnUrl { get; set; }
    }
}
