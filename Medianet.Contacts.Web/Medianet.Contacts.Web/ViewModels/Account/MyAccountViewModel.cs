﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Account
{
    public class MyAccountViewModel
    {
        public ProfileViewModel Profile { get; set; }
        public List<AAPContractViewModel> Contracts { get; set; }
    }
}