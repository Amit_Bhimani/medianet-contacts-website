﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.ViewModels.Account
{
    public class EditProfileViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Company")]
        public string Company { get; set; }
        public string DebtorNumber { get; set; }
        [Required]
        [Display(Name = "Position")]
        public string PositionTitle { get; set; }
        [Required]
        [Display(Name = "Phone")]
        [RegularExpression(@"^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$", ErrorMessage = "The phone number must be input in this format : 0123456789")]
        public string Phone { get; set; }
        [Required]
        [Display(Name = "Business Type")]
        public string BusinessType { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Subjects of Interest")]
        public string RelevantSubjects { get; set; }
        [Display(Name = "ABN")]
        public string ABN { get; set; }

        public IList<SelectListItem> Industries;
    }
}