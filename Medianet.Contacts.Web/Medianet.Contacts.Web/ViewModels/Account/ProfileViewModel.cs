﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Account
{
    public class ProfileViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string DebtorNumber { get; set; }
        public string PositionTitle { get; set; }
        public string Phone { get; set; }
        public string RecordType { get; set; }
        public string BusinessType { get; set; }
        public string Email { get; set; }
        public bool MainContact { get; set; }
        public string RelevantSubjects { get; set; }
        public string AccountOwnerName { get; set; }
        public string ABN { get; set; }

    }
}