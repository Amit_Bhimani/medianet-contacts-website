﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Account
{
    public class AgreeToTermsViewModel
    {
        public bool IsTrue => true;

        [Compare("IsTrue", ErrorMessage = "Please agree to Terms and Conditions")]
        public bool AgreeToTerms { get; set; }

        [Compare("IsTrue", ErrorMessage = "Please agree to Privacy Policy and Anti-Spam Policy")]
        public bool AgreeToPolicy { get; set; }

        public string EmailAddress { get; set; }

        public string ReturnUrl { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}