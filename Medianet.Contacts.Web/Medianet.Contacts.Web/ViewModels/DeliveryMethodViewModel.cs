﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels
{
    public class DeliveryMethodViewModel
    {
        public string CssClass { get; set; }
        public string DeliveryMethod { get; set; }
    }
}