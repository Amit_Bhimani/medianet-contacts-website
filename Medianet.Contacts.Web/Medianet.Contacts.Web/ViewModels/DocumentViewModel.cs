﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels
{
    public class DocumentViewModel
    {
        public int DocumentId { get; set; }

        [Required]
        public bool IsPrivate { get; set; }

        [DocumentFileName]
        public string Name { get; set; }

        [DocumentFile]
        public HttpPostedFileBase File { get; set; }

        public string ContactId { get; set; }

        public string OutletId { get; set; }

        [Required]
        public RecordType RecordType { get; set; }

        public int UploadedByUserId { get; set; }

        public byte[] DocumentContent { get; set; }

        public string DocumentType { get; set; }
    }
}
