﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels
{
    public class ShareProfileViewModel
    {
        public string ContactId { get; set; }

        public string OutletId { get; set; }

        public Wcf.DataContracts.DTO.RecordType RecordType { get; set; }

        [Display(Name = "User")]
        [Required]
        public string EmailAddress { get; set; }

        public bool IsBodyHtml { get; set; }
    }
}