﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Note
{
    public class NoteResultsViewModel
    {
        /// <summary>
        /// Pagination object.
        /// </summary>
        public PaginationNoteViewModel Pagination { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public NoteResultsViewModel()
        {
            Pagination = new PaginationNoteViewModel();
            Pagination.ResultCount = 0;
        }
    }
}