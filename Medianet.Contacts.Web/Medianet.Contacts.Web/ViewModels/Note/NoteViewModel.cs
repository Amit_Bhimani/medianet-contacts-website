﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Medianet.Contacts.Web.ViewModels.Grid;

namespace Medianet.Contacts.Web.ViewModels.Note
{
    public class NoteViewModel : GridViewModelBase
    {
        public int Id { get; set; }

        [Display(Name = "Notes")]
        [Required]
        public string Notes { get; set; }

        public DateTime CreatedDate { get; set; }
        //public byte Status { get; set; }
        public Boolean IsPrivate { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public Boolean Pinned { get; set; }
        public RecordType RecordType { get; set; }
        public Boolean AAPNote { get; set; }
        public string DebtorNumber { get; set; }
        public int ListId { get; set; }
        public int ListSetId { get; set; }
    }
}