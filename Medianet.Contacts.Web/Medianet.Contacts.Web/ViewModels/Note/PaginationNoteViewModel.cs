﻿using System.Collections.Generic;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.ViewModels.Note
{
    /// <summary>
    /// View model to represent pagination of a search results page.
    /// </summary>
    public class PaginationNoteViewModel : PaginationGenericViewModel
    {
        /// <summary>
        /// Number of results to display per page.
        /// </summary>
        private int _pageSize;
        public override int PageSize
        {
            get
           {
                _pageSize = 8;
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        /// <summary>
        /// Note results.
        /// </summary>
        public List<NoteViewModel> Notes { get; set; }
    }
}