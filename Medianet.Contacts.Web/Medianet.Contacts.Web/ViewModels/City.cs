﻿namespace Medianet.Contacts.Web.ViewModels
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PostCode { get; set; }
    }
}