﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Contacts.Web.ViewModels
{
    public class ContactUs
    {
        [Required(ErrorMessage = "Please enter a subject")]
        [DisplayName("Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Please enter a comment")]
        [DisplayName("Comment")]
        public string Comment { get; set; }

        public bool Submitted { get; set; }
    }
}
