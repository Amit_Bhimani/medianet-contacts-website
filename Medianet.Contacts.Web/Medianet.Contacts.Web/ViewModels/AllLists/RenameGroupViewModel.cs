﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.AllLists
{
    public class RenameGroupViewModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        [Display(Name = "New Group Name")]
        [Required]
        public string NewGroupName { get; set; }
    }
}