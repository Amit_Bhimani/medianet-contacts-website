﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.ViewModels.AllLists
{
    public class RebindGroupViewModel
    {
        public int GroupId { get; set; }
        public AllListsSortColumn SortColumn { get; set; }
        public SortDirection SortDirection { get; set; }
       
    }
}