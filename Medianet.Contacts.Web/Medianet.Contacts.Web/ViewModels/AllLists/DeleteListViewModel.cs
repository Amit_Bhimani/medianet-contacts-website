﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.AllLists
{
    public class DeleteListViewModel
    {
        public int ListId { get; set; }
        public string ListName { get; set; }
    }
}