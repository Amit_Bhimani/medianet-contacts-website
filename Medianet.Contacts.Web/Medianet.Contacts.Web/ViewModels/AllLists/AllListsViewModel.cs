﻿using System;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using System.Collections.Generic;

namespace Medianet.Contacts.Web.ViewModels.AllLists
{
    public class ListsGroupedViewModel : PaginationGenericViewModel
    {
        public List<ListViewModel> Lists { get; set; }
        public int GroupId { get; set; }
        public AllListsSortColumn AllListsSortColumn { get; set; }

    }

    public class ListViewModel
    {
        public int ListId { get; set; }

        public string ListName { get; set; }

        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public string Visibility { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UserId { get; set; }

        public string UserFullName { get; set; }
    } 
}