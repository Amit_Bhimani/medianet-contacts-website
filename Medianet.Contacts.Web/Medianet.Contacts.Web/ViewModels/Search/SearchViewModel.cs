﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using System;

namespace Medianet.Contacts.Web.ViewModels.Search
{
    public class SearchViewModel
    {
        public int SearchId { get; set; }
        //public string Query { get; set; }

        //[Required(ErrorMessage = "Please choose a name.")]
        public string Name { get; set; }
        public bool HasViewedTermsAndConditions { get; set; }

        //public string Criteria { get; set; }
        //public bool Status { get; set; }
        //public bool IsPrivate { get; set; }

        //public DateTime CreatedDate { get; set; }
        //public DateTime UpdatedDate { get; set; }

        //public GroupViewModel Group { get; set; }

        // For searching.
        public SearchContext Context { get; set; }

        public int CountryID { get; set; }

        public Countries Country
        {
            get
            {
                return (Countries)Enum.Parse(typeof(Countries), CountryID.ToString());
            }
        }
        
        public string Locations { get; set; }
        public string Positions { get; set; }
        public string MediaTypes { get; set; }
        public string Subjects { get; set; }
        public string SearchText { get; set; }
        public int SelectedRecordType { get; set; }
        public string SelectedId { get; set; }

        public bool NavigateBack { get; set; }

        public PaginationSearchViewModel Pagination { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public SearchViewModel()
        {
            Context = SearchContext.Both;
            //SearchAustralia = true; Sahar changed it
            Pagination = new PaginationSearchViewModel();
            Pagination.ResultCount = 0;
            NavigateBack = false;
        }

        public AdvancedSearchViewModel AdvancedSearch { get; set; }


        /// <summary>
        /// Determines whether to display the expand all link.
        /// </summary>
        /// <returns></returns>
        public bool Expandable()
        {
            // No results so don't display expand link.
            if (Pagination.ResultCount <= 0)
                return false;

            // If this is an advanced search, check the advanced search view model.
            if (AdvancedSearch != null)
            {
                return Context != SearchContext.People &&
                    AdvancedSearch.GetContext() != SearchContext.People;
            }
            else if (Context == SearchContext.Outlet || Context == SearchContext.Both)
            {
                return !(!string.IsNullOrWhiteSpace(Pagination.Expand) && (Pagination.Expand.Contains("#") || Pagination.Expand.Equals("all")));
            }
            else
            {
                return Context != SearchContext.People;
            }
        }
    }
}