﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels.Grid;

namespace Medianet.Contacts.Web.ViewModels.Search
{
    /// <summary>
    /// View model to represent pagination of a search results page.
    /// </summary>
    public class PaginationSearchViewModel : PaginationGenericViewModel
    {
        /// <summary>
        /// Flag to indicate if all items in the current search are pinned.
        /// </summary>
        public PinningViewModel Pinning { get; set; }

        /// <summary>
        /// Search results.
        /// </summary>
        public List<SearchResult> Results { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public PaginationSearchViewModel() {
            Pinning = new PinningViewModel();
        }
    }
}