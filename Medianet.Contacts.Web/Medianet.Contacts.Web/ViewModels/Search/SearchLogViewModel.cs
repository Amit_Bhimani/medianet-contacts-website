﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using System;

namespace Medianet.Contacts.Web.ViewModels.Search
{
    public class SearchLogViewModel
    {
        public int Id { get; set; }
        public string DebtorNumber { get; set; }
        public int UserId { get; set; }
        public string SearchCriteria { get; set; }
        public int SearchType { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? SavedSearchId { get; set; }
        public MediaContactsService.SystemType System { get; set; }
        public string SearchContext { get; set; }
        public int? ResultsCount { get; set; }

        public string OutletId { get; set; }
        public int OutletType{ get; set; }
    }
}