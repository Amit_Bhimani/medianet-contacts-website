﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Medianet.Contacts.CSharp.ExtensionMethods;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using System.ComponentModel;
using Medianet.Contacts.Web.Helpers.Validation;

namespace Medianet.Contacts.Web.ViewModels.PrivateContact
{
    [Bind(Exclude = "Roles, Subjects")]
    public class PrivateContactViewModel
    {
        public int ContactId { get; set; }

        public string Prefix { get; set; }

        [DisplayName("First name")]
        [Required]
        [StringLength(128, ErrorMessage = "First name cannot be larger than 128 characters.")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        [Required]
        [StringLength(128, ErrorMessage = "Last name cannot be larger than 128 characters.")]
        public string LastName { get; set; }

        [DisplayName("Middle name")]
        [StringLength(128, ErrorMessage = "Middle name cannot be larger than 128 characters.")]
        public string MiddleName { get; set; }

        [StringLength(50, ErrorMessage = "Job title cannot be larger than 50 characters.")]
        public string JobTitle { get; set; }

        [DisplayName("Address line 1")]
        [StringLength(50, ErrorMessage = "Address line 1 cannot be larger than 50 characters.")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address line 2")]
        [StringLength(50, ErrorMessage = "Address line 2 cannot be larger than 50 characters.")]
        public string AddressLine2 { get; set; }

        [DisplayName("City")]
        public int? CityId { get; set; }

        [DisplayName("Postcode")]
        public string PostCode { get; set; }

        [DisplayName("State")]
        public int? StateId { get; set; }

        [DisplayName("Country")]
        [Required]
        public int? CountryId { get; set; }

        [DisplayName("Phone")]
        [StringLength(20, ErrorMessage = "Phone number cannot be larger than 20 characters.")]
        public string PhoneNumber { get; set; }

        //[DisplayName("Fax")]
        //[StringLength(20, ErrorMessage = "Fax Number cannot be larger than 20 characters.")]
        //public string FaxNumber { get; set; }

        [DisplayName("Mobile")]
        [StringLength(20, ErrorMessage = "Mobile number cannot be larger than 20 characters.")]
        public string MobileNumber { get; set; }

        public string Notes { get; set; }

        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please provide a preferred delivery method.")]
        public PreferredDeliveryMethod PreferredDelivery { get; set; }

        public char PreferredDeliveryMethod
        {
            get
            {
                return CharEnum.GetCharValue(PreferredDelivery);
            }
        }


        [DisplayName("Facebook")]
        [StringLength(150, ErrorMessage = "Facebook text cannot be larger than 150 characters.")]
        public string Facebook { get; set; }

        [DisplayName("LinkedIn")]
        [StringLength(120, ErrorMessage = "LinkedIn cannot be larger than 120 characters.")]
        public string LinkedIn { get; set; }

        [DisplayName("Twitter")]
        [StringLength(32, ErrorMessage = "Twitter handle cannot be larger than 32 characters.")]
        public string Twitter { get; set; }

        [DisplayName("Blog URL")]
        [StringLength(80, ErrorMessage = "Blog URL cannot be larger than 80 characters.")]
        public string BlogUrl { get; set; }

        public int UpdatedBy { get; set; }

        [DisplayName("Subject")]
        public string SubjectIdsCsv { get; set; }
        
        [DisplayName("Role")]
        public string RoleIdsCsv { get; set; }

        public string MediaOutletId { get; set; }

        public int? PrnOutletId { get; set; }

        public int? PrivateOutletId { get; set; }

        [OutletName]
        public string OutletName { get; set; }

        private List<Position> _roles;
        public List<Position> Roles
        {
            get
            {
                if (_roles == null)
                    _roles = new List<Position>();
                return _roles;
            }
            set { _roles = (List<Position>)value; }
        }

        private List<Subject> _subjects;
        public List<Subject> Subjects
        {
            get
            {
                if (_subjects == null)
                    _subjects = new List<Subject>();
                return _subjects;
            }
            set { _subjects = (List<Subject>)value; }
        }
    }
}