﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels
{
    public class SuggestedChangesViewModel
    {
        public string OutletId { get; set; }

        public string ContactId { get; set; }

        public RecordType RecordType { get; set; }

        [Required]
        [Display(Name = "Message")]
        public string Message { get; set; }
    }
}