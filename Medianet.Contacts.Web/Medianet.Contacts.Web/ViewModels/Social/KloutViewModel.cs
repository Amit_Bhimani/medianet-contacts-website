﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Social
{
    public class KloutViewModel
    {
        public string TwitterHandle { get; set; }
        public string Score { get; set; }
        public string Link { get; set; }
    }
}