﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Social
{
    public class YouTubeViewModel
    {
        public string Id { get; set; }
        
        public int SubscriberCount { get; set; }

        public long ViewCount { get; set; }

        public int VideoCount { get; set; }

        public string Url { get; set; }
    }
}