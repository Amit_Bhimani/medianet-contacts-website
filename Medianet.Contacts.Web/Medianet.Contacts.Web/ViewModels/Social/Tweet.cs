﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Social
{
    public class TweetViewModel
    {
        public string Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Text { get; set; }

        public string Url { get; set; }

        public TweetViewModel Retweet { get; set; }

        public TwitterUserViewModel User { get; set; }
    }
}