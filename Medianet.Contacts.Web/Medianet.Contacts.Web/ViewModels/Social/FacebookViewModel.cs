﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Social
{
    public class FacebookViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public int Friends { get; set; }
        public int Likes { get; set; }
        public string ProfileImage { get; set; }
    }
}