﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Social
{
    public class TwitterUserViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Handle { get; set; }

        public string Description { get; set; }

        public int? Followers { get; set; }

        public int? Following { get; set; }

        public int? Likes { get; set; }

        public int? Tweets { get; set; }

        public string ProfileImage { get; set; }

        public Boolean DefaultProfileImage { get; set; }
    }
}