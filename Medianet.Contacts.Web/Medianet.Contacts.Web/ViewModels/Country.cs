﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.ViewModels
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<State> States { get; set; }
    } 
}