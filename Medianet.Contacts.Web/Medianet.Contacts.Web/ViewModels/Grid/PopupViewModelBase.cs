﻿using System.Web.Mvc;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.ViewModels.Grid
{
    public class PopupViewModelBase : GridViewModelBase
    {
        /// <summary>
        /// The list of groups to display when an existing group
        /// radio button is selected.
        /// </summary>
        public SelectList Groups { get; set; }

        /// <summary>
        /// Indicate whether this is an expanded result set or not.
        /// </summary>
        public string Expand { get; set; }

        public PopupType Type { get; set; }
    }
}