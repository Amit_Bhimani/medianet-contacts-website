﻿using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.ViewModels.Grid
{
    public class GridViewModelBase
    {
        /// <summary>
        /// Unselected items.
        /// </summary>
        public string UncheckedItems { get; set; }

        /// <summary>
        /// Selected items.
        /// </summary>
        public string CheckedItems { get; set; }

        /// <summary>
        /// True if the items that are checked/unchecked are present only
        /// on the current page.
        /// </summary>
        public bool CurrentPageOnly { get; set; }

        /// <summary>
        /// The state of the tristate checkbox.
        /// </summary>
        public CheckboxState CheckboxState { get; set; }
    }
}