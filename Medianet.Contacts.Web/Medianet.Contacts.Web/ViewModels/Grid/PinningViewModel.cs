﻿using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.ViewModels.Grid
{
    public class PinningViewModel
    {
        /// <summary>
        /// The records to pin.
        /// </summary>
        public string Items { get; set; }

        /// <summary>
        /// The type of pin action.
        /// </summary>
        public PinAction Type { get; set; }

        /// <summary>
        /// Flag to indicate if all records in the current search are pinned.
        /// </summary>
        public bool AllPinned { get; set; }

        /// <summary>
        /// Indicates if the current search is expanded.
        /// </summary>
        public string Expand { get; set; }
    }
}