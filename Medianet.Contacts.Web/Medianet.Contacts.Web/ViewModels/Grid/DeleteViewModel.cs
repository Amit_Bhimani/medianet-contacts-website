﻿using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.ViewModels.Grid
{
    public class DeleteViewModel : GridViewModelBase
    {
        public DeleteAction Type { get; set; }

        public string Expand { get; set; }
    }
}