﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Contacts.Web.ViewModels
{
    public class ContactDetailsViewModel
    {
        [Required(ErrorMessage = "Please enter your first name")]
        [DisplayName("FirstName:")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your surname")]
        [DisplayName("Surname")]
        public string Surname { get; set; }

        [DisplayName("Position")]
        public string Position { get; set; }

        [DisplayName("Organisation")]
        public string Organisation { get; set; }

        [DisplayName("WorkNumber")]
        public string WorkNumber { get; set; }

        [Required(ErrorMessage = "Please enter an email address")]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Invalid email address.")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a comment")]
        [DisplayName("Comments")]
        public string Comments { get; set; }


        [DisplayName("Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please choose a state")]
        [DisplayName("State")]
        public string State { get; set; }

        [Required(ErrorMessage = "Please type the verification word correctly")]
        [DisplayName("Please enter the string as shown above:")]
        public string CaptchaText { get; set; }
    }
}
