﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.ViewModels
{
    public class GroupViewModel : IValidatableObject
    {
        public int Id { get; set; }

        [Display(Name = "Group name")]
        public string Name { get; set; }

        public int UserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public GroupType Type { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext) {
            if (string.IsNullOrEmpty(Name) && Id == 0) {
                yield return new ValidationResult("Please create a new group or select an existing one.", new[] { "Name" });
            }
        }
    }
}