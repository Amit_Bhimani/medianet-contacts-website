﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Medianet.Contacts.Web.Common.Model;
using System.ComponentModel;
using Medianet.Contacts.Web.Helpers.Validation;

namespace Medianet.Contacts.Web.ViewModels.PrivateOutlet
{
    public class PrivateOutletViewModel
    {
        [Required]
        [Display(Name = "Name")]
        [StringLength(75, ErrorMessage = "Outlet/Company Name cannot be larger than 75 characters.")]
        public string CompanyName { get; set; }
        
        [StringLength(50, ErrorMessage = "Address Line 1 cannot be larger than 50 characters.")]
        public string AddressLine1 { get; set; }

        [StringLength(50, ErrorMessage = "Address Line 2 cannot be larger than 50 characters.")]
        public string AddressLine2 { get; set; }

        public int? CityId { get; set; }

        public int? StateId { get; set; }

        [StringLength(50, ErrorMessage = "Post Code cannot be larger than 50 characters.")]
        public string PostCode { get; set; }

        [Required]
        [Display(Name = "Country")]
        public int? MediaAtlasCountryId { get; set; }
        
        [StringLength(50, ErrorMessage = "Phone cannot be larger than 50 characters.")]
        public string PhoneNumber { get; set; }

        //[DisplayName("Mobile")]
        //[StringLength(255, ErrorMessage = "Mobile Number cannot be larger than 50 characters.")]
        //public string Mobile { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        public int OutletTypeId { get; set; }

        [Required]
        [Display(Name = "Media type")]
        public int ProductTypeId { get; set; }

        public int Circulation { get; set; }

        public char PreferredDeliveryMethod { get; set; }
        
        [StringLength(80, ErrorMessage = "Website address cannot be larger than 80 characters.")]
        public string Website { get; set; }

        public string Ownership { get; set; }

        [StringLength(10, ErrorMessage = "Station frequency cannot be larger than 10 characters.")]
        public string StationFrequency { get; set; }
        
        [Display(Name = "Frequency")]
        public int? FrequencyId { get; set; }

        [StringLength(150, ErrorMessage = "Facebook address cannot be larger than 150 characters.")]
        public string Facebook { get; set; }

        [StringLength(120, ErrorMessage = "LinkedIn address cannot be larger than 120 characters.")]
        public string LinkedIn { get; set; }
        
        [StringLength(32, ErrorMessage = "Twitter handle cannot be larger than 32 characters.")]
        public string Twitter { get; set; }
        
        [StringLength(80, ErrorMessage = "Blog URL cannot be larger than 80 characters.")]
        public string BlogUrl { get; set; }

        public int UpdatedBy { get; set; }

        [Display(Name = "Subject")]
        public string SubjectIdsCsv { get; set; }

        //[TokenInputRequired(ErrorMessage = "The Working Language field is required")]
        [Display(Name = "Working Language")]
        public string WorkingLanguageIdsCsv { get; set; }

        public int OutletId { get; set; }

        private List<Subject> _subjects;
        public List<Subject> Subjects
        {
            get
            {
                if (_subjects == null)
                    _subjects = new List<Subject>();
                return _subjects;
            }
            set { _subjects = (List<Subject>)value; }
        }

        private List<WorkingLanguage> _workingLanguages;
        
        public List<WorkingLanguage> WorkingLanguages
        {
            get
            {
                if (_workingLanguages == null)
                    _workingLanguages = new List<WorkingLanguage>();
                return _workingLanguages;
            }
            set { _workingLanguages = (List<WorkingLanguage>)value; }
        }

        #region Postal address

        //[DisplayName("Address Line 1")]
        //[StringLength(50, ErrorMessage = "Postal Address Line 1 cannot be larger than 50 characters.")]
        //public string PostalAddressLine1 { get; set; }

        //[DisplayName("Address Line 2")]
        //[StringLength(50, ErrorMessage = "Postal Address Line 2 cannot be larger than 50 characters.")]
        //public string PostalAddressLine2 { get; set; }

        //[DisplayName("City")]
        //public int? PostalCityId { get; set; }

        //public string PostalCityName { get; set; }

        //[DisplayName("Continent")]
        //public int? PostalContinentId { get; set; }

        //[DisplayName("Country")]
        //public int? PostalCountryId { get; set; }

        //[DisplayName("State")]
        //public int? PostalStateId { get; set; }

        //[DisplayName("Post Code")]
        //public string PostalPostCode { get; set; }

        #endregion
    }
}