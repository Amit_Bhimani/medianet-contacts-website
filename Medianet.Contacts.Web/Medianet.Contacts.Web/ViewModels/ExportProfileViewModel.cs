﻿using Medianet.Contacts.Web.MediaContactsService;

namespace Medianet.Contacts.Web.ViewModels
{
    public class ExportProfileViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DebtorNumber { get; set; }
        public int UserId { get; set; }
        public string ExportFields { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsSelected { get; set; }
        public MediaContactsExportType ExportType { get; set; }
    }
}