﻿using System.Collections.Generic;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;
using System;

namespace Medianet.Contacts.Web.ViewModels
{
    public class ContactOutletBaseViewModel
    {
        public ContactOutletBaseViewModel()
        {
        }

        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public RecordType Type { get; set; }
        public Address Address { get; set; }

        public Address PostalAddress { get; set; }

        public string ProfileNotes { get; set; }
        
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string AlternativeEmailAddress { get; set; }
        public string AdditionalPhoneNumber { get; set; }
        public string AdditionalMobileNumber { get; set; }
        public PreferredDeliveryMethod? DeliveryMethod { get; set; }
        public SocialInfo SocialDetails { get; set; }
        public string BlogUrl { get; set; }
        public string WebUrl { get; set; }
        public string OfficeHours { get; set; }
        public int UpdatedBy { get; set; }
        public bool CurrentlyOutOfOffice { get; set; }
        public DateTime? OutOfOfficeStartDate { get; set; }
        public DateTime? OutOfOfficeReturnDate { get; set; }

        public List<Subject> Subjects { get; set; }

        public List<ListBase> BelongsToLists { get; set; }

        public bool SoonToBeOutOfOffice
        {
            get
            {
                // If no start date then they aren't out of office now or soon
                if (!OutOfOfficeStartDate.HasValue) return false;

                var nowDate = DateTime.Now.Date;
                var soonDate = nowDate.AddDays(30);

                // If the start date is in the past or than 30 days from now they aren't out of the office soon
                if (OutOfOfficeStartDate.Value <= nowDate || OutOfOfficeStartDate.Value > soonDate) return false;

                return true;
            }
        }
    }
}