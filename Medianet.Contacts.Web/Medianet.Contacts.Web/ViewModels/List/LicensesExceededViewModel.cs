﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.ViewModels.List
{
    public class LicensesExceededViewModel
    {
        public int LicenseCount;
        public List<ChargeBeeService.DBSession> ActiveSessions;
    }
}