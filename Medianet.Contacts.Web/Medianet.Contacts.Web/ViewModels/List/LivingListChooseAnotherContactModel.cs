﻿using System.Web.Mvc;

namespace Medianet.Contacts.Web.ViewModels.List {
    public class LivingListChooseAnotherContactModel {
        public int UserId { get; set; }
        public int RecordId { get; set; }
        public string ContactId { get; set; }
        public SelectList Contacts { get; set; }
    }
}