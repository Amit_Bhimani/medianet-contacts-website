﻿using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web.ViewModels.List
{
    public class ListContactReplacementViewModel
    {
        public int RecordId { get; set; }
        public int ListSetId { get; set; }
        public int ListId { get; set; }
        public string ListName { get; set; }
        public string MediaContactId { get; set; }
        public string PrnContactId { get; set; }
        public string OmaContactId { get; set; }
        public string ContactDeletedName { get; set; }
        public string OutletDeletedName { get; set; }

        public string ContactReplacementId { get; set; }

        public string ContactReplacementName { get; set; }

        public string OutletDeletedId { get; set; }

        public int RecordType { get; set; }
    }
}