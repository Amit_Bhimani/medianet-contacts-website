﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.ViewModels.Grid;

namespace Medianet.Contacts.Web.ViewModels.List
{
    public class ListDeleteItemsViewModel : GridViewModelBase
    {
        public DeleteAction Type { get; set; }
        public int ListId { get; set; }
        public int ListSetId { get; set; }
    }
}