﻿using Medianet.Contacts.Web.Common.BO;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Contacts.Web.ViewModels.List
{
    public class ListDuplicateViewModel : ListViewModel
    {
        public GroupState GroupState { get; set; }

        public int NewListId { get; set; }

        [Required(ErrorMessage = "Please enter a new list name")]
        public string NewListName { get; set; }
    }
}