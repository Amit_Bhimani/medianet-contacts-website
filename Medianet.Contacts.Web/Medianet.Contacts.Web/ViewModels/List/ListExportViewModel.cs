﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Medianet.Contacts.Web.MediaContactsService;

namespace Medianet.Contacts.Web.ViewModels.List
{
    public class ListExportViewModel
    {
        public int Id { get; set; }

        /// <summary>
        /// The name of the list.
        /// </summary>
        [Required(ErrorMessage = "Please choose a saved export name.")]
        public string Name { get; set; }

        /// <summary>
        /// Flag to indicate whether the export profile is private.
        /// </summary>
        public bool IsPrivate { get; set; }

        public MediaContactsExportType ExportType { get; set; }

        //public int UserId { get; set; }
        //public string DebtorNumber { get; set; }

        /// <summary>
        /// The list of export profiles to display.
        /// </summary>
        public SelectList ExportProfiles { get; set; }

        public static readonly SelectList FieldsToExport = new SelectList(new List<SelectListItem>()
        {
            new SelectListItem() {Text = "Contact Title", Value = "Contact_Title"},
            new SelectListItem() {Text = "Contact FirstName", Value = "Contact_Firstname"},
            new SelectListItem() {Text = "Contact LastName", Value = "Contact_Lastname"},
            new SelectListItem() {Text = "Contact FullName", Value = "Contact_Fullname"},
            new SelectListItem() {Text = "Contact JobTitle", Value = "Contact_JobTitle"},
            new SelectListItem() {Text = "Contact RoleName", Value = "Contact_Rolename"},
            new SelectListItem() {Text = "Contact SubjectName", Value = "Contact_Subjectname"},
            new SelectListItem() {Text = "Contact SystemNotes", Value = "Contact_SystemNotes"},
            new SelectListItem() {Text = "Contact EmailAddress", Value = "Contact_EmailAddress"},
            new SelectListItem() {Text = "Contact PhoneNumber", Value = "Contact_PhoneNumber"},
            new SelectListItem() {Text = "Contact MobileNumber", Value = "Contact_MobileNumber"},
            new SelectListItem() {Text = "Contact AddressLine1", Value = "Contact_AddressLine1"},
            new SelectListItem() {Text = "Contact AddressLine2", Value = "Contact_AddressLine2"},
            new SelectListItem() {Text = "Contact CityName", Value = "Contact_CityName"},
            new SelectListItem() {Text = "Contact StateName", Value = "Contact_Statename"},
            new SelectListItem() {Text = "Contact PostalCode", Value = "Contact_PostalCode"},
            new SelectListItem() {Text = "Contact CountryName", Value = "Contact_countryName"},
            new SelectListItem() {Text = "Contact ContinentName", Value = "Contact_Continentname"},
            new SelectListItem() {Text = "Contact Facebook", Value = "Contact_Facebook"},
            new SelectListItem() {Text = "Contact Twitter", Value = "Contact_Twitter"},
            new SelectListItem() {Text = "Contact LinkedIn", Value = "Contact_LinkedIn"},
            new SelectListItem() {Text = "Contact BlogURL", Value = "Contact_BlogURL"},
            new SelectListItem() {Text = "Contact MediaDirectoryNotes", Value = "Contact_MediaDirectoryNotes"},

            new SelectListItem() {Text = "Contact Instagram", Value = "Contact_Instagram"},
            new SelectListItem() {Text = "Contact YouTube", Value = "Contact_YouTube"},
            new SelectListItem() {Text = "Contact Snapchat", Value = "Contact_Snapchat"},
            new SelectListItem() {Text = "Contact Bio", Value = "Contact_Bio"},
            new SelectListItem() {Text = "Contact BugBears", Value = "Contact_BugBears"},
            new SelectListItem() {Text = "Contact AlsoKnown As", Value = "Contact_AlsoKnownAs"},
            new SelectListItem() {Text = "Contact PressReleaseInterests", Value = "Contact_PressReleaseInterests"},
            new SelectListItem() {Text = "Contact Awards", Value = "Contact_Awards"},
            new SelectListItem() {Text = "Contact PersonalInterests", Value = "Contact_PersonalInterests"},
            new SelectListItem() {Text = "Contact PoliticalParty", Value = "Contact_PoliticalParty"},
            new SelectListItem() {Text = "Contact NamePronunciation", Value = "Contact_NamePronunciation"},
            new SelectListItem() {Text = "Contact AppearsIn", Value = "Contact_AppearsIn"},
            new SelectListItem() {Text = "Contact BusyTimes", Value = "Contact_BusyTimes"},
            new SelectListItem() {Text = "Contact CoffeeOrder", Value = "Contact_CoffeeOrder"},
            new SelectListItem() {Text = "Contact PitchingDos", Value = "Contact_PitchingDos"},
            new SelectListItem() {Text = "Contact PitchingDonts", Value = "Contact_PitchingDonts"},
            new SelectListItem() {Text = "Contact AuthorOf", Value = "Contact_AuthorOf"},
            new SelectListItem() {Text = "Contact GiftsAccepted", Value = "Contact_GiftsAccepted"},
            new SelectListItem() {Text = "Contact MediaInfluencerScore", Value = "Contact_MediaInfluencerScore"},
            new SelectListItem() {Text = "Contact CurrentStatus", Value = "Contact_CurrentStatus"},
            new SelectListItem() {Text = "Contact BasedInLocation", Value = "Contact_BasedInLocation"},
            new SelectListItem() {Text = "Contact Deadlines", Value = "Contact_Deadlines"},
            new SelectListItem() {Text = "Contact BestTimeToContact", Value = "Contact_BestContactTime"},
            new SelectListItem() {Text = "Contact AlternativeEmailAddress", Value = "Contact_AlternativeEmailAddress"},
            new SelectListItem() {Text = "Contact AdditionalMobile", Value = "Contact_AdditionalMobile"},
            new SelectListItem() {Text = "Contact AdditionalPhoneNumber", Value = "Contact_AdditionalPhoneNumber"},
            new SelectListItem() {Text = "Contact Skype", Value = "Contact_Skype"},
            new SelectListItem() {Text = "Contact Website", Value = "Contact_Website"},
            new SelectListItem() {Text = "Contact OfficeHours", Value = "Contact_OfficeHours"},

            new SelectListItem() {Text = "Contact Out of Office", Value = "Contact_CurrentlyOutOfOffice"},
            new SelectListItem() {Text = "Contact Out of Office Start Date", Value = "Contact_OutOfOfficeStartDate"},
            new SelectListItem() {Text = "Contact Out of Office Return Date", Value = "Contact_OutOfOfficeReturnDate"},

            new SelectListItem() {Text = "Outlet Name", Value = "OutletName"},
            new SelectListItem() {Text = "Outlet MediaType", Value = "Outlet_MediaType"},
            new SelectListItem() {Text = "Outlet Subjects", Value = "Outlet_Subjects"},
            new SelectListItem() {Text = "Outlet Frequency", Value = "Outlet_Frequency"},
            new SelectListItem() {Text = "Outlet Circulation", Value = "Outlet_Circulation"},
            //new SelectListItem() {Text = "Outlet Audience", Value = "Outlet_Audience"},
            new SelectListItem() {Text = "Outlet Language", Value = "Outlet_Language"},
            new SelectListItem() {Text = "Outlet StationFrequency", Value = "Outlet_StationFrequency"},
            new SelectListItem() {Text = "Outlet EmailAddress", Value = "Outlet_EmailAddress"},
            new SelectListItem() {Text = "Outlet PhoneNumber", Value = "Outlet_PhoneNumber"},
            new SelectListItem() {Text = "Outlet MobileNumber", Value = "Outlet_MobileNumber"},
            new SelectListItem() {Text = "Outlet AddressLine1", Value = "Outlet_AddressLine1"},
            new SelectListItem() {Text = "Outlet AddressLine2", Value = "Outlet_AddressLine2"},
            new SelectListItem() {Text = "Outlet CityName", Value = "Outlet_CityName"},
            new SelectListItem() {Text = "Outlet StateName", Value = "Outlet_Statename"},
            new SelectListItem() {Text = "Outlet PostalCode", Value = "Outlet_PostalCode"},
            new SelectListItem() {Text = "Outlet CountryName", Value = "Outlet_countryName"},
            new SelectListItem() {Text = "Outlet ContinentName", Value = "Outlet_Continentname"},
            new SelectListItem() {Text = "Outlet Website", Value = "Outlet_Website"},
            new SelectListItem() {Text = "Outlet Facebook", Value = "Outlet_Facebook"},
            new SelectListItem() {Text = "Outlet Twitter", Value = "Outlet_Twitter"},
            new SelectListItem() {Text = "Outlet LinkedIn", Value = "Outlet_LinkedIn"},
            new SelectListItem() {Text = "Outlet BlogURL", Value = "Outlet_BlogURL"},
            new SelectListItem() {Text = "Outlet MediaDirectoryNotes", Value = "Outlet_MediaDirectoryNotes"},

            new SelectListItem() {Text = "Outlet Instagram", Value = "Outlet_Instagram"},
            new SelectListItem() {Text = "Outlet YouTube", Value = "Outlet_YouTube"},
            new SelectListItem() {Text = "Outlet Snapchat", Value = "Outlet_Snapchat"},
            new SelectListItem() {Text = "Outlet Bio", Value = "Outlet_Bio"},
            new SelectListItem() {Text = "Outlet Postal AddressLine1", Value = "Outlet_PostalAddressLine1"},
            new SelectListItem() {Text = "Outlet Postal AddressLine2", Value = "Outlet_PostalAddressLine2"},
            new SelectListItem() {Text = "Outlet Postal CityName", Value = "Outlet_PostalCityName"},
            new SelectListItem() {Text = "Outlet Postal StateName", Value = "Outlet_PostalStateName"},
            new SelectListItem() {Text = "Outlet Postal PostalCode", Value = "Outlet_PostalPostalCode"},
            new SelectListItem() {Text = "Outlet Postal CountryName", Value = "Outlet_PostalCountryName"},
            new SelectListItem() {Text = "Outlet Postal ContinentName", Value = "Outlet_PostalContinentName"},
            new SelectListItem() {Text = "Outlet NamePronunciation", Value = "Outlet_NamePronunciation"},
            new SelectListItem() {Text = "Outlet AlsoKnownAs", Value = "Outlet_AlsoKnownAs"},
            new SelectListItem() {Text = "Outlet Deadlines", Value = "Outlet_Deadlines"},
            //new SelectListItem() {Text = "Outlet Readership", Value = "Outlet_Readership"},
            new SelectListItem() {Text = "Outlet BroadcastTime", Value = "Outlet_BroadcastTime"},
            new SelectListItem() {Text = "Outlet AlternativeEmailAddress", Value = "Outlet_AlternativeEmailAddress"},
            new SelectListItem() {Text = "Outlet Mobile", Value = "Outlet_Mobile"},
            new SelectListItem() {Text = "Outlet AdditionalPhoneNumber", Value = "Outlet_AdditionalPhoneNumber"},
            new SelectListItem() {Text = "Outlet AdditionalWebsite", Value = "Outlet_AdditionalWebsite"},
            new SelectListItem() {Text = "Outlet Skype", Value = "Outlet_Skype"},
            new SelectListItem() {Text = "Outlet OfficeHours", Value = "Outlet_OfficeHours"},
            new SelectListItem() {Text = "Outlet PublishedOn", Value = "Outlet_PublishedOn"},
            new SelectListItem() {Text = "Outlet MediaInfluencerScore", Value = "Outlet_MediaInfluencerScore"},
            new SelectListItem() {Text = "Outlet PressReleaseInterests", Value = "Outlet_PressReleaseInterests"},
            new SelectListItem() {Text = "Outlet RegionsCovered", Value = "Outlet_RegionsCovered"},
        }, "Value", "Text");

        /// <summary>
        /// Constructor.
        /// </summary>
        public ListExportViewModel()
        {
            //ExportProfiles = new SelectList();
        }

    }
}