﻿using System.Collections.Generic;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.ViewModels.List
{
    /// <summary>
    /// View model to represent pagination of a search results page.
    /// </summary>
    public class PaginationListViewModel : PaginationGenericViewModel
    {
        /// <summary>
        /// The list id when adding a list to a task.
        /// </summary>
        public int ListId { get; set; }

        /// <summary>
        /// The list set id when adding a list to a task.
        /// </summary>
        public int ListSet { get; set; }

        /// <summary>
        /// List results.
        /// </summary>
        public List<ListRecord> Results { get; set; }
    }
}