﻿using System.Collections.Generic;

namespace Medianet.Contacts.Web.ViewModels.List
{
    public class ListVersionViewModel
    {
        public List<MediaContactsService.MediaContactListVersion> Results { get; set; }
        public int SelectedListSetId { get; set; }
    }
}