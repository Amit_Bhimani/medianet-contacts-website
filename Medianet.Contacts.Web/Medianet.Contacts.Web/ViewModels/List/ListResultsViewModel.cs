﻿namespace Medianet.Contacts.Web.ViewModels.List
{
    public class ListResultsViewModel
    {
        /// <summary>
        /// Pagination object.
        /// </summary>
        public PaginationListViewModel Pagination { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ListResultsViewModel()
        {
            Pagination = new PaginationListViewModel();
            Pagination.ResultCount = 0;
        }
    }
}