﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MediaPeople.Common.BO;
using MediaPeople.DataContracts.DTO;
using MediaPeople.ViewModel;

namespace MediaPeople.ViewModels.List
{
    public class ListViewModel : PopupViewModelBase
    {
        /// <summary>
        /// The group this task belongs to.
        /// </summary>
        public GroupViewModel Group { get; set; }

        /// <summary>
        /// The name of the list.
        /// </summary>
        [Required(ErrorMessage = "Please choose a list name.")]
        public string Name { get; set; }

        /// <summary>
        /// Flag to indicate whether list is private.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        /// The list id when adding a list to a task.
        /// </summary>
        public int ListId { get; set; }

        /// <summary>
        /// The list set id when adding a list to a task.
        /// </summary>
        public int ListSet { get; set; }

        /// <summary>
        /// The type of record that is being added to this task.
        /// </summary>
        public PopupType Type { get; set; }

        /// <summary>
        /// The list of lists to display when an existing group is chosen.
        /// </summary>
        public SelectList Lists { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ListViewModel()
        {
            Group = new GroupViewModel();
        }

        public ListResultsViewModel ListResults { get; set; }
        public ListVersionViewModel ListVersions { get; set; }
        public List<ListContactReplacement> LivingList { get; set; }
    }
}