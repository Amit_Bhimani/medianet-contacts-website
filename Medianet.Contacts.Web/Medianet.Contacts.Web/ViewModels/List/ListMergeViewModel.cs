﻿using Medianet.Contacts.Web.Common.BO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.ViewModels.List
{
    public class ListMergeViewModel : ListViewModel
    {
        public GroupState GroupState { get; set; }

        public int NewListId { get; set; }

        [Required(ErrorMessage = "Please enter a new list name")]
        public string NewListName { get; set; }

        public List<KeyValuePair<string, List<SelectListItem>>> ListsGrouped { get; set; }

        [Required(ErrorMessage = "Please select a list to merge with")]
        public List<int> ListsToMergeWith { get; set; }
    }
}