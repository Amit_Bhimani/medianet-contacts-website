﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using Medianet.Contacts.Web.Common.BO;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp.Pdf;
using PdfSharp;
using HtmlAgilityPack;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Medianet.Contacts.Web.ViewModels
{
    public static class Email
    {
        public const int sendTimeout = 10000;

        public static string customerTemplateFilePath;
        public static string adminTemplateFilePath;
        private static string ClientServicePhoneNo = ConfigurationManager.AppSettings["ClientServicePhoneNo"];
        private static string ClientServiceEmail = ConfigurationManager.AppSettings["ClientServiceEmail"];

        public static void SendCustomerEmail(ContactDetailsViewModel details, bool trial)
        {
            MailMessage email = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            string body = null;

            try
            {
                email.BodyEncoding = System.Text.Encoding.ASCII;
                email.Subject = trial ? ConfigurationManager.AppSettings["TrialCustomerSubject"] : ConfigurationManager.AppSettings["ContactUsCustomerSubject"];
                email.From = new MailAddress(ConfigurationManager.AppSettings["ClientServiceEmail"]);
                email.To.Add(details.Email);
                email.IsBodyHtml = true;

                using (StreamReader sr = new StreamReader(customerTemplateFilePath))
                {
                    body = sr.ReadToEnd();

                    body = body.Replace("#FirstName#", HttpUtility.HtmlEncode(details.FirstName));
                    body = body.Replace("#BaseUrl#", ConfigurationManager.AppSettings["MediaPeopleBaseUrl"]);
                    body = Regex.Replace(body, "#ClientServicePhoneNo#", ConfigurationManager.AppSettings["ClientServicePhoneNo"]);
                    body = Regex.Replace(body, "#ClientServiceEmail#", ConfigurationManager.AppSettings["ClientServiceEmail"]);

                    email.Body = body;

                    smtp.Host = ConfigurationManager.AppSettings["MailServer"];
                    smtp.Timeout = sendTimeout;
                    smtp.Send(email);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (smtp != null) smtp.Dispose();
            }
        }

        public static void SendAdminEmail(ContactDetailsViewModel details, bool trial)
        {
            MailMessage email = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            string body = null;

            try
            {
                email.BodyEncoding = System.Text.Encoding.ASCII;
                email.Subject = trial ? ConfigurationManager.AppSettings["TrialAdminSubject"] : ConfigurationManager.AppSettings["ContactUsAdminSubject"];
                email.From = new MailAddress(details.Email);
                email.To.Add(ConfigurationManager.AppSettings["ClientServiceEmail"]);
                email.IsBodyHtml = true;

                using (StreamReader sr = new StreamReader(adminTemplateFilePath))
                {
                    body = sr.ReadToEnd();

                    body = body.Replace("#FirstName#", HttpUtility.HtmlEncode(details.FirstName));
                    body = body.Replace("#LastName#", HttpUtility.HtmlEncode(details.Surname));
                    body = body.Replace("#Position#", HttpUtility.HtmlEncode(details.Position));
                    body = body.Replace("#Organisation#", HttpUtility.HtmlEncode(details.Organisation));
                    body = body.Replace("#WorkNumber#", HttpUtility.HtmlEncode(details.WorkNumber));
                    body = body.Replace("#Email#", HttpUtility.HtmlEncode(details.Email));
                    body = body.Replace("#Address#", HttpUtility.HtmlEncode(details.Address));
                    body = body.Replace("#State#", HttpUtility.HtmlEncode(details.State));
                    body = body.Replace("#Comments#", HttpUtility.HtmlEncode(details.Comments));
                    body = body.Replace("#BaseUrl#", ConfigurationManager.AppSettings["MediaPeopleBaseUrl"]);
                    body = Regex.Replace(body, "#ClientServicePhoneNo#", ConfigurationManager.AppSettings["ClientServicePhoneNo"]);
                    body = Regex.Replace(body, "#ClientServiceEmail#", ConfigurationManager.AppSettings["ClientServiceEmail"]);

                    email.Body = body;

                    smtp.Host = ConfigurationManager.AppSettings["MailServer"];
                    smtp.Timeout = sendTimeout;
                    smtp.Send(email);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (smtp != null) smtp.Dispose();
            }
        }

        public static void SendAdminEmail(OutletDetailsHistory outlet)
        {
            MailMessage email = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            string body = null;

            try
            {
                email.BodyEncoding = System.Text.Encoding.ASCII;
                email.Subject = "Your update suggestion has been received - " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:tt");

                if (!string.IsNullOrEmpty(outlet.Email))
                {
                    email.ReplyToList.Add(new MailAddress(outlet.Email, outlet.Email));
                }

                email.From = new MailAddress(ConfigurationManager.AppSettings["OmaResearchTeam"]);
                email.To.Add(ConfigurationManager.AppSettings["ChangeRequestTeam"]);
                email.IsBodyHtml = true;

                string template = HttpContext.Current.Server.MapPath("~/Content/Emails/ContactName.htm");

                using (StreamReader sr = new StreamReader(template))
                {
                    body = sr.ReadToEnd();

                    HttpContext ctx = HttpContext.Current;

                    body = Regex.Replace(body, "#fname#", HttpUtility.HtmlEncode(outlet.FirstName).Replace("''", "'"));
                    body = Regex.Replace(body, "#Email#", HttpUtility.HtmlEncode(outlet.Email).Replace("''", "'"));
                    body = Regex.Replace(body, "#ContactName#", HttpUtility.HtmlEncode(outlet.ContactName));
                    body = Regex.Replace(body, "#OutletName#", HttpUtility.HtmlEncode(outlet.OutletName));
                    body = Regex.Replace(body, "#UpdateRequest#", outlet.UpdateRequest.Replace(System.Environment.NewLine,@"<br/>"));
                    body = Regex.Replace(body, "#UserLogoName#", HttpUtility.HtmlEncode(outlet.LogonName));
                    body = Regex.Replace(body, "#CompanyName#", HttpUtility.HtmlEncode(outlet.CompanyName));
                    body = Regex.Replace(body, "#wwwroot#", ConfigurationManager.AppSettings["RootUrl"]);
                    body = Regex.Replace(body, "#ClientServicePhoneNo#", ConfigurationManager.AppSettings["ClientServicePhoneNo"]);
                    body = Regex.Replace(body, "#ClientServiceEmail#", ConfigurationManager.AppSettings["SuggestionClientServiceEmail"]);
                }

                email.Body = body;

                smtp.Host = ConfigurationManager.AppSettings["MailServer"];
                smtp.Timeout = sendTimeout;
                smtp.Send(email);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (smtp != null) smtp.Dispose();
            }
        }

        public static void SendCustomerEmail(OutletDetailsHistory outlet)
        {
            if (!string.IsNullOrEmpty(outlet.Email))
            {
                MailMessage email = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                try
                {
                    string body = null;

                    email.BodyEncoding = System.Text.Encoding.ASCII;
                    email.Subject = "Auto Response: Your update suggestion has been received - " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:tt");
                    email.From = new MailAddress(ConfigurationManager.AppSettings["OmaResearchTeam"]);
                    email.To.Add(new MailAddress(outlet.Email));
                    email.IsBodyHtml = true;

                    string template = HttpContext.Current.Server.MapPath("~/Content/Emails/ContactName.htm");

                    using (StreamReader sr = new StreamReader(template))
                    {
                        body = sr.ReadToEnd();

                        HttpContext ctx = HttpContext.Current;

                        body = Regex.Replace(body, "#fname#", HttpUtility.HtmlEncode(outlet.FirstName).Replace("''", "'"));
                        body = Regex.Replace(body, "#Email#", HttpUtility.HtmlEncode(outlet.Email).Replace("''", "'"));
                        body = Regex.Replace(body, "#ContactName#", HttpUtility.HtmlEncode(outlet.ContactName));
                        body = Regex.Replace(body, "#OutletName#", HttpUtility.HtmlEncode(outlet.OutletName));
                        body = Regex.Replace(body, "#UpdateRequest#", outlet.UpdateRequest.Replace(System.Environment.NewLine, @"<br/>"));
                        body = Regex.Replace(body, "#UserLogoName#", HttpUtility.HtmlEncode(outlet.LogonName));
                        body = Regex.Replace(body, "#CompanyName#", HttpUtility.HtmlEncode(outlet.CompanyName));
                        body = Regex.Replace(body, "#wwwroot#", ConfigurationManager.AppSettings["RootUrl"]);
                        body = Regex.Replace(body, "#ClientServicePhoneNo#", ConfigurationManager.AppSettings["ClientServicePhoneNo"]);
                        body = Regex.Replace(body, "#ClientServiceEmail#", ConfigurationManager.AppSettings["SuggestionClientServiceEmail"]);
                    }

                    email.Body = body;

                    smtp.Host = ConfigurationManager.AppSettings["MailServer"];
                    smtp.Timeout = sendTimeout;
                    smtp.Send(email);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (smtp != null) smtp.Dispose();
                }
            }

        }

        public static void SendShareProfileEmail(string body, string emailAddress, string profileName, bool sharingHtml, bool isOutlet)
        {
            SmtpClient smtp = new SmtpClient
            {
                Host = ConfigurationManager.AppSettings["MailServer"],
                Timeout = sendTimeout
            };

            try
            {
                MailMessage email = new MailMessage
                {
                    BodyEncoding = System.Text.Encoding.ASCII,
                    Subject = string.Format(ConfigurationManager.AppSettings["ShareProfileSubject"], isOutlet ? "outlet" : "contact"),
                    From = new MailAddress(ConfigurationManager.AppSettings["ClientServiceEmail"]),
                    IsBodyHtml = true,
                    Body = sharingHtml ? Regex.Replace(body, @"data-html-\b(\d+)", "width=\"$1\"") : ""
                };

                email.BodyEncoding = Encoding.UTF8;


                if (!sharingHtml)
                {
                    string template = HttpContext.Current.Server.MapPath("~/Content/Emails/ShareProfile.html");

                    using (StreamReader sr = new StreamReader(template))
                    {
                        string html = sr.ReadToEnd().Replace(System.Environment.NewLine, "");
                        html = Regex.Replace(html, "#RootUrl#", ConfigurationManager.AppSettings["RootUrl"]);
                        html = Regex.Replace(html, string.Format("<!--{0}_Start-->(.*?)<!--{0}_End-->", isOutlet ? "Outlet" : "Contact"), "");

                        email.Body = html;
                    }
                }

                email.To.Add(emailAddress);

                if (sharingHtml)
                {
                    smtp.Send(email);
                }
                else
                {
                    body = Regex.Replace(body, @"data-pdf-\b(\d+)", "style=\"width:$1%;\"").Replace(System.Environment.NewLine, "");
                    body = Regex.Replace(body, "<tr id=\"intro\">(.*?)</tr>", "");

                    int startIndex = body.IndexOf("<style>") + 7;
                    var css = body.Substring(startIndex, body.IndexOf("</style>") - startIndex);

                    body = Regex.Replace(body, "<style>(.*?)</style>", "");
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(body);

                    var p = @"(.*?)\s{(.*?)}";
                    IEnumerable<Match> classes = Regex.Matches(css, p).Cast<Match>();

                    foreach (var item in classes)
                    {
                        HtmlNode newNode;
                        IEnumerable<HtmlNode> nodes;

                        string key = item.Groups[1].Value.Replace(" ", "").Replace(":", "");

                        if (key.StartsWith("."))
                        {
                            nodes = doc.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class") &&
                                     x.Attributes["class"].Value.Contains(key.Substring(1))).ToList();
                        }
                        else
                        {
                            var tags = doc.DocumentNode.SelectNodes($"//{key}");
                            nodes = tags == null ? null : tags.ToList();
                        }

                        if (nodes != null)
                            foreach (HtmlNode node in nodes)
                            {
                                newNode = node.Clone();
                                newNode.SetAttributeValue("style", item.Groups[2].Value.Trim().Replace(";            ", "; "));

                                body = body.Replace(node.OuterHtml, newNode.OuterHtml);
                            }
                    }

                    PdfDocument pdf = PdfGenerator.GeneratePdf(body, PageSize.A4, 0);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        pdf.Save(ms, false);

                        email.Attachments.Add(new Attachment(ms, $"{profileName}.pdf"));

                        smtp.Send(email);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (smtp != null) smtp.Dispose();
            }
        }

        public static void SendAdminResearchRequestEmail(Common.Model.User currentUser, string details)
        {
            using (var smtp = new SmtpClient())
            {
                var email = new MailMessage(ConfigurationManager.AppSettings["OmaResearchTeam"],
                    ConfigurationManager.AppSettings["ResearchRequestTeam"].Replace(";", ","));
                string body = null;

                email.BodyEncoding = System.Text.Encoding.ASCII;
                email.Subject = ConfigurationManager.AppSettings["ResearchRequestSubject"] + " - " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:tt");

                if (!string.IsNullOrEmpty(currentUser.EmailAddress))
                {
                    email.ReplyToList.Add(new MailAddress(currentUser.EmailAddress));
                }

                email.IsBodyHtml = true;

                string template = HttpContext.Current.Server.MapPath("~/Content/Emails/ResearchRequestAdmin.htm");

                using (StreamReader sr = new StreamReader(template))
                {
                    body = sr.ReadToEnd();

                    body = Regex.Replace(body, "#UserName#", HttpUtility.HtmlEncode(currentUser.FullName));
                    body = Regex.Replace(body, "#Details#", HttpUtility.HtmlEncode(details));
                    body = Regex.Replace(body, "#CompanyName#", HttpUtility.HtmlEncode(currentUser.CompanyName));
                    body = Regex.Replace(body, "#UserEmail#", HttpUtility.HtmlEncode(currentUser.EmailAddress));                
                    body = Regex.Replace(body, "#wwwroot#", ConfigurationManager.AppSettings["RootUrl"]);
                    body = Regex.Replace(body, "#ClientServicePhoneNo#", ConfigurationManager.AppSettings["ClientServicePhoneNo"]);
                    body = Regex.Replace(body, "#ClientServiceEmail#", ConfigurationManager.AppSettings["ClientServiceEmail"]);
                }

                email.Body = body;

                smtp.Host = ConfigurationManager.AppSettings["MailServer"];
                smtp.Timeout = sendTimeout;
                smtp.Send(email);
            }
        }

        public static void SendClientResearchRequestEmail(Common.Model.User currentUser)
        {
            using (var smtp = new SmtpClient())
            {
                var email = new MailMessage(ConfigurationManager.AppSettings["OmaResearchTeam"],
                    currentUser.EmailAddress);
                string body = null;

                email.BodyEncoding = System.Text.Encoding.ASCII;
                email.Subject = ConfigurationManager.AppSettings["ResearchRequestSubject"] + " - " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:tt");
                email.IsBodyHtml = true;

                string template = HttpContext.Current.Server.MapPath("~/Content/Emails/ResearchRequestClient.htm");

                using (StreamReader sr = new StreamReader(template))
                {
                    body = sr.ReadToEnd();

                    body = Regex.Replace(body, "#wwwroot#", ConfigurationManager.AppSettings["RootUrl"]);
                    body = Regex.Replace(body, "#ClientServicePhoneNo#", ConfigurationManager.AppSettings["ClientServicePhoneNo"]);
                    body = Regex.Replace(body, "#ClientServiceEmail#", ConfigurationManager.AppSettings["ClientServiceEmail"]);
                }

                email.Body = body;

                smtp.Host = ConfigurationManager.AppSettings["MailServer"];
                smtp.Timeout = sendTimeout;
                smtp.Send(email);
            }
        }
    }
}