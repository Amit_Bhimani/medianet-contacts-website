﻿using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.ViewModels
{
    public class MyRecordViewModel
    {
        public List<MyRecordTaskGroups> MyRecordTasksGroup;
        public ICollection<ListBase> BelongsToLists { get; set; }
        public ICollection<TaskBase> BelongsToTasks { get; set; }
        public ICollection<DocumentBase> Documents { get; set; }
    }

    public class MyRecordTaskGroups
    {
        public string Name { get; set; }

        private ICollection<TaskBase> _tasks;

        public ICollection<TaskBase> Tasks
        {
            get
            {
                return _tasks;
            }
            set
            {
                if (!string.IsNullOrEmpty(Name) && value != null && value.Count > 0)
                {
                    _tasks = value.OrderBy(tb => tb.DueDate).ToList();
                }
                else
                {
                    _tasks = value;
                }
            }
        }
    }
}