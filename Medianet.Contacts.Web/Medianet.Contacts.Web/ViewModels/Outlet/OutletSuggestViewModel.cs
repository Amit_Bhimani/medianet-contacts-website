﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Outlet
{
    public class OutletSuggestViewModel
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public bool IsPrivate { get; set; }

        public int RecordType { get; set; }
    }
}