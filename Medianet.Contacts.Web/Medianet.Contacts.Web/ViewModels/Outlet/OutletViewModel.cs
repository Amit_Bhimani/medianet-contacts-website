﻿using System.Collections.Generic;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.ViewModels.Outlet
{
    public class OutletViewModel : ContactOutletBaseViewModel
    {
        public string Name { get; set; }
        public string Circulation { get; set; }
        public int? Audience { get; set; }

        public int? Readership { get; set; }

        public string Bio { get; set; }
        public OutletViewModel ParentOutlet { get; set; }
        public MediaType MediaType { get; set; }
        public string PublishedOn { get; set; }
        public string Deadlines { get; set; }
        public string LogoFileName { get; set; }
        public string CopyPrice { get; set; }
        public OutletFrequency Frequency { get; set; }
        public Demographics OutletDemographics { get; set; }
        public int? MediaInfluencerScore { get; set; }
        public ICollection<Language> Languages { get; set; }
        public ICollection<ContactBase> Contacts { get; set; }
        public bool IsGeneric {get;set;}

        public string YearEstablished { get; set; }

        public string NewsFocusName { get; set; }

        public string CallLetters { get; set; }

        public string TrackingReference { get; set; }

        public string NamePronunciation { get; set; }

        public bool SubscriptionOnlyPublication { get; set; }

        public string AlsoKnownAs { get; set; }

        public string AdditionalWebsite { get; set; }

        public string BroadcastTime { get; set; }

        public string PressReleaseInterests { get; set; }

        public string RegionsCovered { get; set; }

        public string PriorityNotice { get; set; }

        public bool IsActive { get; set; }

        public List<MediaContactsService.MediaOutletPartial> RelatedMediaOutlets { get; set; }

        public List<MediaContactsService.MediaOutletPartial> SyndicatingToMediaOutlets { get; set; }
        
        public List<MediaContactsService.MediaOutletPartial> SyndicatedFromMediaOutlets { get; set; }

        public List<MediaContactsService.MediaOutletPartial> MediaOwnerOutlets { get; set; }
    }
}