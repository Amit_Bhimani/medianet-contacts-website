﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Outlet
{
    public class OutletBaseViewModel
    {
        public string Name { get; set; }
        public RecordType Type { get; set; }
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public bool IsGeneric { get; set; }
    }
}