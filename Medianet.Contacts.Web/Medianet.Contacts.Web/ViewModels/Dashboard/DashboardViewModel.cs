﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Dashboard
{
    public class DashboardViewModel
    {
        public List<MediaMovementsViewModel> MediaMovementsList { get; set; }
    }
}