﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Model;

namespace Medianet.Contacts.Web.ViewModels.Dashboard
{
    public class ContactSummaryViewModel
    {
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public RecordType Type { get; set; }
        public ContactName Name { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string LogoFileName { get; set; }
        public SocialInfo SocialDetails { get; set; }
        public MediaType MediaType { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}