﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.ViewModels.Dashboard
{
    public class MediaMovementsViewModel
    {
        public string ContactId { get; set; }
        public string OutletId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Description { get; set; }
        public string JobTitle { get; set; }
        public string TwitterHandle { get; set; }
        public string LogoFileName { get; set; }
        public string CompanyName { get; set; }
        public DateTime CreatedDate { get; set; }

        public string FullName
        {
            get
            {
                return $"{(FirstName ?? string.Empty)} {(LastName ?? string.Empty)}".Trim();
            }
        }

        public string Initials
        {
            get
            {
                var result = string.Empty;
                result += (string.IsNullOrEmpty(this.FirstName) ? string.Empty : this.FirstName.Substring(0, 1));
                result += (string.IsNullOrEmpty(this.LastName) ? string.Empty : this.LastName.Substring(0, 1));

                return result.Trim().ToUpper();
            }
        }
    }
}