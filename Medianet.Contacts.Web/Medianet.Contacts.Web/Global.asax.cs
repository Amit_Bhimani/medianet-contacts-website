﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.Binders;
using Medianet.Contacts.Web.Common.Caching;
using Medianet.Contacts.Web.Common.Configuration;
using Medianet.Contacts.Web.Common.Logging;
using Medianet.Contacts.Web.Common.Util;
using Medianet.Contacts.Web.Ninject;
using Medianet.Contacts.Web.ViewModels;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Ninject;
using Ninject.Web.Common;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Web
{
    public class MvcApplication : NinjectHttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.aspx/{*pathInfo}");
            routes.IgnoreRoute("WebServices/{myWebServices}.asmx/{*pathInfo}");
            
            routes.MapRoute("Contact", "Contact/{recordType}/{contactId}/{outletId}",
                            new { controller = "contact", action = "Index", recordType = (int)RecordType.MediaContact, outletId = UrlParameter.Optional });

            routes.MapRoute("Outlet", "outlet/{recordType}/{id}",
                new { controller = "outlet", action = "Index", recordType = 1 });

            routes.MapRoute("Search",
                         "Search/{id}",
                         new { controller = "Search", action = "Index", id = UrlParameter.Optional },
                         new { id = @"\d+$" });

            routes.MapRoute("List", "List/{action}/{ListId}/{ListSetId}",
                new { controller = "List", action = "Index", ListId = UrlParameter.Optional, ListSetId = UrlParameter.Optional });

            routes.MapRoute("Logon", "", new { controller = "Account", action = "LogOn" }, new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute("LogOff", "logoff", new { controller = "Account", action = "LogOff" });

            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Search", action = "Index", id = "" });//Default URL
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception unhandledException = Server.GetLastError();
            MPLogger.LogException(unhandledException);
            if (!ExtensionMethods.IsMaxRequestExceededException(unhandledException))
            {
                HttpException httpException = unhandledException as HttpException;

                if (httpException == null)
                {
                    Exception innerException = unhandledException.InnerException;
                    httpException = innerException as HttpException;
                }

                if (httpException != null)
                {
                    int httpCode = httpException.GetHttpCode();
                    switch (httpCode)
                    {
                        case (int)HttpStatusCode.NotFound: //ignore file not found exceptions
                            return;
                        default:
                            MPLogger.LogException(unhandledException);
                            break;
                    }
                }
                else
                    MPLogger.LogException(unhandledException);
            }
        }

        void Session_Start(object sender, EventArgs e)
        {
        }

        void Session_End(object sender, EventArgs e)
        {
            SessionManager.Instance.LogOff();
        }

        protected override IKernel CreateKernel()
        {
            return new StandardKernel(new ServiceProxyModule());
        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Register ModelBinders
            ModelBinders.Binders.DefaultBinder = new TrimModelBinder();
            ModelBinders.Binders.Add(typeof(AdvancedSearchViewModel), new AdvancedSearchViewModelBinder());
            
            //Configure AutoMapper
            AutoMapperConfigurator.Configure();
            //Mapper.AssertConfigurationIsValid();

            Logger.SetLogWriter(new LogWriterFactory().Create(), false);
        }

        protected override void OnApplicationStopped()
        {
            base.OnApplicationStopped();

            // Clear all items being cached.
            Cacher.Clear();
        }
    }
}