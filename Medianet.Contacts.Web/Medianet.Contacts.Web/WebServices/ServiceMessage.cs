﻿using System.Web.Script.Serialization;

namespace Medianet.Contacts.Web.WebServices
{
    /// <summary>
    /// Class for messages returned from web services.
    /// </summary>
    public class ServiceMessage
    {
        #region Properties

        public MessageType Type { get; set; }
        public string Message { get; set; }
        public string HTML { get; set; }
        public string JavaScript { get; set; }

        #endregion

        #region Public Constants

        /// <summary>
        /// Some public constants that can be used. Note that these are not
        /// declared as <c>const</c> because we want to be able to refer to them
        /// without having to create an instance.
        /// </summary>
        public static readonly string UnauthorisedMessage = "You do not have permissions for this. Are you logged in?";
        public static readonly string SessionExpiredMessage = "Your session has expired. Please login again.";
        public static readonly string ServerErrorMessage = "Unknown server error occurred. Please try again.";
        public static readonly string FileNotFound = "Either the file does not exist or you do have permissions to view the file.";

        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of ServiceMessage with the specified parameters.
        /// </summary>
        /// <param name="type">Used to indicate the type of the ServiceMessage.</param>
        /// <param name="message">The message itself.</param>
        /// <param name="html">HTML representation of the message.</param>
        /// <param name="js">Javascript function to call instead.</param>
        public ServiceMessage(MessageType type, string message, string html, string js) {
            this.Type = type;
            this.Message = message;
            this.HTML = html;
            this.JavaScript = js;
        }
        
        public ServiceMessage(MessageType type, string message) : this(type, message, "", "") { }

        /// <summary>
        /// Parameterless constructor to initialise an empty ServiceMessage.
        /// </summary>
        public ServiceMessage() { }

        #endregion

        #region Methods

        /// <summary>
        /// Serialize the given message, type, HTML and JavaScript.
        /// </summary>
        /// <param name="type">The type of the message to serialize.</param>
        /// <param name="message">The message itself.</param>
        /// <param name="html">HTML representation of the message.</param>
        /// <param name="js">Javascript function to call instead.</param>
        /// <returns></returns>
        public static string SerializeToJson(MessageType type, string message, string html, string js) {
            ServiceMessage serviceMessage = new ServiceMessage(type, message, html, js);
            return SerializeToJson(serviceMessage);
        }

        /// <summary>
        /// Serialize the given message and type.
        /// </summary>
        /// <param type="type">The type of the message to serialize.</param>
        /// <param name="message">The message itself.</param>
        /// <returns>The object serialized as a JSON string.</returns>
        public static string SerializeToJson(MessageType type, string message) {
            ServiceMessage serviceMessage = new ServiceMessage(type, message);
            return SerializeToJson(serviceMessage);
        }

        /// <summary>
        /// Serialize the given service message.
        /// </summary>
        /// <param name="message">A ServiceMessage to be serialized.</param>
        /// <returns>The object serialized as a JSON string.</returns>
        public static string SerializeToJson(ServiceMessage message) {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(message);
        }

        #endregion
    }

    /// <summary>
    /// Enum to define the different types of messages.
    /// </summary>
    public enum MessageType
    {
        Error = 1,
        Info = 2,
        JS = 3,
        HTML = 4
    }
}