﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Helpers
{
    public static class BrowserHelper
    {
        public static bool IsCompatible(this HtmlHelper helper)
        {
            string supportedBrowsers = ConfigurationManager.AppSettings["supportedBrowsers"];

            if (string.IsNullOrEmpty(supportedBrowsers))
                return true;

            HttpRequestBase request = helper.ViewContext.HttpContext.Request;

            HttpBrowserCapabilitiesWrapper clientBrowser =  (HttpBrowserCapabilitiesWrapper)request.Browser;

            if (request.Cookies["outdatedBrowser"] != null)
                return true;

            Dictionary<string, int> browsers = supportedBrowsers.Split('|').Select(m => m.Split(',')).ToDictionary(d => d[0], d => int.Parse(d[1]));

            foreach (var item in browsers)
            {
                if (item.Key == clientBrowser.Browser && item.Value > clientBrowser.MajorVersion)
                    return false;
            }

            return true;
        }
    }
}