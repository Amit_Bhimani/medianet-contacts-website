﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Xml.Linq;

namespace Medianet.Contacts.Web.Helpers
{
    public static class ValidationSummaryHelper
    {
        public static MvcHtmlString CustomValidationSummary(this HtmlHelper htmlHelper, bool excludePropertyErrors, string message, object htmlAttributes)
        {
            var htmlString = htmlHelper.ValidationSummary(excludePropertyErrors, message, htmlAttributes);

            if (htmlString != null)
            {
                XElement element = XElement.Parse(htmlString.ToHtmlString());

                var list = element.Elements("ul").Elements("li");

                if (list.Count() == 1 && list.First().Value == string.Empty)
                    return null;
            }

            return htmlString;
        }
    }
}