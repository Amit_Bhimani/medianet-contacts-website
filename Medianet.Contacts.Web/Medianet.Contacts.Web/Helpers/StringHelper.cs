﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Helpers
{
    public static class StringHelper
    {
        public static bool HasValue(this string value)
        {
            return !string.IsNullOrEmpty(value) && value.Trim().ToLower() != "na";
        }

        public static string CleanQuery(string query)
        {
            if (query != null)
                return query.Replace("\t", " ");

            return null;
        }
    }
}