﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web
{
    public class DocumentFileName : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(metadata.GetDisplayName());
            rule.ValidationType = "documentname";
            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            object instance = validationContext.ObjectInstance;
            Type type = instance.GetType();
            int? id = type.GetProperty("DocumentId").GetValue(instance, null) as int?;

            if (id.Value > 0 && (value == null || string.IsNullOrEmpty(value.ToString()))) // editing document  
                return new ValidationResult("The File Name field is required.");

            if (value != null && value.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) > 0)
                return new ValidationResult("Invalid file name");

            return ValidationResult.Success;
        }
    }
}