﻿using Medianet.Contacts.Web.ViewModels.PrivateContact;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Helpers.Validation
{
    public class OutletName : ValidationAttribute, IClientValidatable
    {
        #region Fields & Properties
        
        private const string ErrorMessage = "The outlet is not valid";

        #endregion

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var clientValidationRule = new ModelClientValidationRule()
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "outletvalidator"
            };

            clientValidationRule.ValidationParameters.Add("errormessage", string.Join(",", ErrorMessage));

            yield return clientValidationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var instance = (PrivateContactViewModel)validationContext.ObjectInstance;
            
            if (string.IsNullOrWhiteSpace(instance.OutletName))
                return ValidationResult.Success;

            if (!instance.PrivateOutletId.HasValue && string.IsNullOrWhiteSpace(instance.MediaOutletId) && !instance.PrnOutletId.HasValue)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}