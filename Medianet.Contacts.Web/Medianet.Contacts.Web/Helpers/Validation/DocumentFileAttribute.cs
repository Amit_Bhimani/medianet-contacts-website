﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web
{
    public class DocumentFile : ValidationAttribute, IClientValidatable
    {
        #region Fields & Properties

        private int MaxUploadFileSize
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["MaxUploadFileSize"]) * 1024 * 1024;
            }
        }

        private string MaximumFileSizeErrorMessage
        {
            get
            {
                return $"Max file size is {MaxUploadFileSize} MB";
            }
        }

        private string ValidFileTypeValidatorErrorMessage
        {
            get
            {
                return $"File type is not supported";
            }
        }

        private string[] Extensions
        {
            get
            {
                return ConfigurationManager.AppSettings["DocumentFileExtensions"].ToString().Split(',', '.');
            }
        }

        private readonly string RequiredErrorMessage = "The file is required. ";

        #endregion

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var clientValidationRule = new ModelClientValidationRule()
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "fileuploadvalidator"
            };

            var clientvalidationmethods = new List<string>();
            var parameters = new List<string>();
            var errorMessages = new List<string>();


            clientvalidationmethods.Add("requiredFile");
            parameters.Add("requiredFile");
            errorMessages.Add(RequiredErrorMessage);

            clientvalidationmethods.Add("maximumFileSizeValidator");
            parameters.Add(MaxUploadFileSize.ToString());
            errorMessages.Add(MaximumFileSizeErrorMessage);


            clientvalidationmethods.Add("validFileTypeValidator");
            parameters.Add(String.Join(",", Extensions));
            errorMessages.Add(ValidFileTypeValidatorErrorMessage);
            
            clientValidationRule.ValidationParameters.Add("clientvalidationmethods", string.Join(",", clientvalidationmethods));
            clientValidationRule.ValidationParameters.Add("parameters", string.Join("|", parameters));
            clientValidationRule.ValidationParameters.Add("errormessages", string.Join(",", errorMessages));

            yield return clientValidationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            object instance = validationContext.ObjectInstance;
            Type type = instance.GetType();
            int? id = type.GetProperty("DocumentId").GetValue(instance, null) as int?;

            if (id.HasValue && id.Value > 0) // editing document  
                return ValidationResult.Success;

            var file = value as HttpPostedFileBase;

            if (file == null)
                return new ValidationResult(RequiredErrorMessage);

            if (file.ContentLength > MaxUploadFileSize)
                return new ValidationResult(MaximumFileSizeErrorMessage);
            
            if (!Extensions.Contains(Path.GetExtension(file.FileName).Trim('.')))
                return new ValidationResult(ValidFileTypeValidatorErrorMessage);

            return ValidationResult.Success;
        }
    }
}