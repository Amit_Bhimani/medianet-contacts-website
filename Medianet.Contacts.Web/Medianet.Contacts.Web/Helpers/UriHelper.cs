﻿using System;

namespace Medianet.Contacts.Web.Helpers
{
    public static class UriHelper
    {
        public static string Host(string url)
        {
            var hostValue = string.Empty;
            if (url.HasValue())
            {
                try
                {
                    hostValue = new Uri(url).Host;
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            return hostValue;
        }

        public static string AbsolutePath(string url)
        {
            var absoluteValue = string.Empty;
            if (url.HasValue())
            {
                try
                {
                    absoluteValue = new Uri(url).AbsolutePath;
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            return absoluteValue;

        }
    }
}