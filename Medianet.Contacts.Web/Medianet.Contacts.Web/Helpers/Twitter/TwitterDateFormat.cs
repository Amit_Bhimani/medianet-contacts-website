﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Helpers
{
    public static class TwitterDateFormat
    {
        public static string TwitterDate(this DateTime datetime, DateTime date)
        {
            TimeSpan diff = DateTime.Now - date;

            if (diff.TotalDays < 1)
                return diff.TotalHours < 1 ? diff.TotalMinutes.ToString("N0") +"m" : diff.TotalHours.ToString("N0") + "h";

            return date.ToString("MMM d", CultureInfo.CreateSpecificCulture("en-us"));
        }
    }
}