﻿using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.Common.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Helpers
{
    public static class OutletContactPrivateIconHelper
    {
        public static IHtmlString PrivateIconHelper(this HtmlHelper html, RecordType type)
        {
            return type.IsOmaType() ? html.Raw(@"<i class=""fas fa-lock""></i>") : null;
        }
    }
}