﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Contacts.Web.Helpers
{
    public static class GeneralHelper
    {
        public static string GetIpAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.Headers["X-Forwarded-For"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.UserHostAddress;
        }

        public static string FormatMediaMovementDate(DateTime date)
        {
            TimeSpan span = DateTime.Now - date;
            var result = string.Empty;

            if (span.Days >= 1)
            {
                result = date.ToString("dd MMM yyyy");
            }
            else if (span.Hours > 0)
            {
                result = span.Hours + (span.Hours == 1 ? " hour ago" : " hours ago");
            }
            else if (span.Minutes > 0)
            {
                result = span.Minutes + (span.Minutes == 1 ? " minute ago" : " minutes ago");
            }
            else if (span.Seconds > 0)
            {
                result = span.Seconds + (span.Seconds == 1 ? " second ago" : " seconds ago");
            }


            return result;
        }

        public static bool IsOutOfOffice(DateTime? startDate, DateTime? returnDate)
        {
            // We must have a start date. If not then they aren't out of office
            if (!startDate.HasValue)
                return false;

            DateTime nowDate = DateTime.Now.Date;

            // If the start date is in the future then they aren't on leave yet
            if (startDate.Value > nowDate)
                return false;

            // If we have a start date but no return date then assume they are still out of office
            if (!returnDate.HasValue)
                return true;

            if (returnDate.Value >= nowDate)
                return true;

            return false;
        }
    }
}