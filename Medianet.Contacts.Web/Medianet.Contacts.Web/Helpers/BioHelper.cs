﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Contacts.Web.Helpers
{
    public static class BioHelper
    {
        public static IHtmlString FormatBioHelper(this HtmlHelper html,string bio)
        {
            var match = Regex.Match(bio, @"\{DOB:[0-9]{4,8}\}");
            if (match.Success)
            {
                bio = bio.Replace(match.Value, CalculateAgeFromDOB(match.Value));
            }

            bio = bio.Replace(Environment.NewLine, "<br />");
            
            return html.Raw(HttpUtility.HtmlDecode(bio));
        }
        private static string CalculateAgeFromDOB(string dob) {
            try
            {
                dob = new String(dob.Where(Char.IsDigit).ToArray());
                int year = Convert.ToInt32(dob.Substring(0,4));
                int month = dob.Length >= 6 ? Convert.ToInt32(dob.Substring(4, 2)) : 0;
                int day = dob.Length == 8 ? Convert.ToInt32(dob.Substring(6, 2)) : 0;

                return ToAgeString(year, month, day);
            }
            catch (Exception)
            {
                return dob;
            }
        }
        private static string ToAgeString(this int year, int month, int day = 0)
        {
            DateTime today = DateTime.Today;

            if (day == 0) {
                return (today.Year - year - 1).ToString();
            }

            int months = today.Month - month;
            int years = today.Year - year;

            if (today.Day < day)
            {
                months--;
            }

            if (months < 0)
            {
                years--;
                months += 12;
            }

            return years.ToString();
        }
    }
}