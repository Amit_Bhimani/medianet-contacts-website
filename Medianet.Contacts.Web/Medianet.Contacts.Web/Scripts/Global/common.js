﻿/// <reference path="~/Scripts/Plugins"/>
/// <reference path="~/Scripts/toastr.js"/>
/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js" />
/// <reference path="~/Scripts/jquery.validate-vsdoc.js" />
/// <reference path="~/Scripts/Pages/quick-search.js" />
var MediaPeople = (function () {

    function initConfirm() {
        $(document).on("click", "[data-confirm]", function (event) {
            event.preventDefault();

            showConfirm($(this));
        });
    }

    function showConfirm(obj) {
        var modalContainer = $("[data-confirm_modal]");
        var confirmText = obj.attr("data-confirm-text");
        var description = obj.attr("data-confirm-description");
        var lblDescription = modalContainer.find("[data-confirm-description]");
        var buttonText = obj.attr("data-button-text");
        var titleText = obj.attr("data-confirm");

        if (typeof titleText === "undefined" || titleText === null || titleText === "") {
            // For people calling the confirm popup manually they set this property for the title instead
            modalContainer.find("[data-confirm-title]").html(obj.attr("data-confirm-title"));
        } else {
            modalContainer.find("[data-confirm-title]").html(titleText);
        }

        if (typeof confirmText === "undefined" || confirmText === null || confirmText === "") {
            modalContainer.find("[data-confirm-text]").closest("div").hide();
        } else {
            modalContainer.find("[data-confirm-text]").html("&nbsp;" + confirmText).closest("div").show();
        }

        if (typeof description === "undefined" || description === null || description === "")
            lblDescription.closest("div").hide();
        else {
            lblDescription.html(description).closest("div").show();
        }

        var button = modalContainer.find("[data-confirm-click]");

        button.unbind("click").click(function () {
            eval(obj.attr("data-confirm-click"));
            $("[data-confirm_modal]").modal("hide");
        });

        if (typeof buttonText === "undefined" || buttonText === null || buttonText === "") {
            button.html("Delete"); // Default value
        } else {
            button.html(buttonText);
        }

        modalContainer.modal("show");
    }

    function CheckClientBrowser() {
        if ($("div#outdatedBrowser").length == 0) return;
        MediaPeople.ShowModal($("div#outdatedBrowser")[0].outerHTML);
    }

    function measureText(pText, pFontSize, pFontWeight, pFontFamily, pStyle) {
        var lDiv = document.createElement('div');

        document.body.appendChild(lDiv);

        if (pStyle != null) {
            lDiv.style = pStyle;
        }
        lDiv.style.fontWeight = pFontWeight;
        lDiv.style.fontFamily = pFontFamily;
        lDiv.style.fontSize = "" + pFontSize + "px";
        lDiv.style.position = "absolute";
        lDiv.style.left = -1000;
        lDiv.style.top = -1000;

        lDiv.innerHTML = pText;

        var lResult = {
            width: lDiv.clientWidth,
            height: lDiv.clientHeight
        };

        document.body.removeChild(lDiv);
        lDiv = null;

        return lResult;
    }

    return {
        genericError: $("body").attr("data-generic-error-message"),
        activeClass: "dropdown-active",
        easeOut: "easeOutQuart",
        uiDisabledClass: "ui-state-disabled",
        arrowDownClass: "expandDown",
        arrowUpClass: "expandUp",
        checkedNodeClass: "absTopSelected",
        asideExpandClass: "asideBtnExpand",
        visuallyHiddenClass: "d-none",
        defaultSC: ["PRIMARYNEWSCONTACTONLY_NO", "INCLUDEMETROOUTLETS_NO", "INCLUDEREGIONALOUTLETS_NO", "INCLUDESUBURBANOUTLETS_NO",
            "EXCLUDECOMPLETEDTASKS_NO", "RECORDSTOSHOW_ALL RECORDS", "PREFERSEMAIL_NO", "PREFERSMAIL_NO", "PREFERSFAX_NO",
            "PREFERSPHONE_NO", "PREFERSMOBILE_NO",
            "PREFERSTWITTER_NO", "PREFERSLINKEDIN_NO", "PREFERSFACEBOOK_NO", "PREFERSINSTAGRAM_NO",
            "SHOWONLYWITHTWITTER_NO", "SHOWONLYWITHLINKEDIN_NO", "SHOWONLYWITHFACEBOOK_NO", "SHOWONLYWITHEMAIL_NO",
            "SHOWONLYWITHFAX_NO", "SHOWONLYWITHPOSTALADDRESS_NO", "SHOWONLYWITHINSTAGRAM_NO", "SHOWONLYWITHYOUTUBE_NO",
            "SHOWONLYWITHSNAPCHAT_NO", "SHOWONLYWITHSKYPE_NO", "EXCLUDEOPS_NO", "EXCLUDEFINANCEINST_YES", "EXCLUDEPOLITICIANS_YES", "EXCLUDEOUTOFOFFICE_NO"],
        InDefaultCriteria: function (id, val) {
            return $.inArray($.trim(id).toUpperCase() + "_" + $.trim(val).toUpperCase(), MediaPeople.defaultSC) != -1;
        },
        GetSubjectsTokenInputResultRow: function (item) {
            return item.Id.toString().match(MediaPeople.groupRegex)
                ? '<li class="subjectGroup"><i class="fa fa-folder"></i><p>' + item.Name + '</p></li>'
                : '<li><p>' + item.Name + '</p></li>';
        },
        acceptCompatibility: function () {
            $.ajax({
                url: $("div#outdatedBrowser").attr("data-action"),
                type: "POST"
            });
            MediaPeople.HideModal();
        },
        InitJQueryValidate: function (popupForm, callbackFn) {
            try {
                $.validator.setDefaults({
                    errorClass: "is-invalid"
                });

                var forms = popupForm == null ? $('form') : [$(popupForm)];
                $.each(forms, function (i, item) {
                    item = $(item);
                    item.unbind().data("validator", null);
                    // Check document for changes
                    $.validator.unobtrusive.parse($(item));
                    // Re add validation with changes
                    item.validate(item.data("unobtrusiveValidation").options);
                    item.data("validator").settings.submitHandler = function (form) {
                        if (callbackFn)
                            return callbackFn();
                        else form.submit();
                    };
                });
            }
            catch (e) { }
        },
        commonTokenInputSettings: {
            tokenValue: "Id",
            propertyToSearch: "Name",
            dropdownHeight: "300px",
            keepOpenOnSelect: true,
            deleteFromSourceOnSelect: true,
            searchDelay: 500,
            minChars: 1,
            preventDuplicates: true,
            theme: "mediapeople",
            noResultsText: "No matches found.",
            showSelectedInDropdown: false,
            searchingText: "Fetching matches from the server...",
            hideOnRemove: false
        },
        tokenInputRegex: /^token\-input\-(.*)$/i,
        groupRegex: /^g\d+$/gi,
        /* utility functions starts */
        IsValidDatasource: function (ds) {
            return $.isArray(ds) && ds.length > 0 && ds[0] != null && ds[0].hasOwnProperty('Name') && ds[0].hasOwnProperty('Id');
        },
        GetInputToken: function GetInputToken(id) {
            if (id) { // && $.isArray($("#" + id).tokenInput("get"))) {
                return {
                    wrapper: $("#token-input-" + id + "-token-input-dropdown-mediapeople"),
                    tokensList: $("#token-input-" + id + "-token-input-list-mediapeople"),
                    inputBox: $("#token-input-" + id),
                    height: $("#token-input-" + id + "-token-input-list-mediapeople").height() +
                        parseInt($("#token-input-" + id + "-token-input-list-mediapeople").css('border-top-width')) +
                        parseInt($("#token-input-" + id + "-token-input-list-mediapeople").css('border-bottom-width'))
                };
            }

            return null;
        },
        AddSubjectGroupToTokenInput: function (id) {
            var _data = $("#" + id).data();
            $("#" + id).tokenInput("add", { Id: "g" + _data.groupFilterId, Name: _data.groupFilterName });

            try {
                $("#" + id).tokenInput("hide");
            } catch (e) {
            }
        },
        ClearSubjectFilterById: function (id) {
            $("#" + id).data({ groupFilterId: '', groupFilterName: '' });
            var ddfilter = $("#" + id).parentsUntil('tr').siblings().find(".dropdown.btnFilter");

            return true;
        },
        InitFilterDropdowns: function () {
            /* for keeping track of what's "open" */
            var showingDropdown, showingMenu, showingParent;
            var offsetWidth = 4;
            if ($('html').hasClass('ie7') && $("#qSearchWrapper").length) offsetWidth = 6;
            if ($('html').hasClass('ie7') && $("#AdvancedForm").length) offsetWidth = 2;
            /* hides the current menu */
            var hideMenu = function () {
                if (showingDropdown) {
                    if (showingDropdown.siblings(".dropdown-menu").find(".tokenSelected").length == 0) showingDropdown.removeClass(MediaPeople.activeClass);

                    showingMenu.hide();
                }
            };

            /* recurse through dropdown menus */
            $('.dropdown.btnFilter').each(function () {
                /* track elements: menu, parent */
                var dropdown = $(this);

                var menu = dropdown.parent().find('div.dropdown-menu'), parent = dropdown.parent();

                /* function that shows THIS menu */
                var showMenu = function () {
                    //if ie7 and on the quicksearch page
                    hideMenu();
                    menu.width(menu.closest('tr').children('td.searchInput:first').width() - offsetWidth);
                    showingDropdown = dropdown.addClass('dropdown-active');
                    showingMenu = menu.show();
                    showingParent = parent;
                    try {
                        $("#" + dropdown.data('for')).tokenInput("hide");
                    } catch (e) {
                    }
                };

                dropdown.on({
                    click: function (e) { /* function to show menu when clicked */
                        //                        if (e) { e.stopPropagation(); e.preventDefault(); }
                        showMenu();
                    },
                    focus: function () { showMenu(); } /* function to show menu when someone tabs to the box */
                });

                //when the anchor for the group is clicked -> filter subjects tokenInput by this group (only if it isn't already the selected or being filtered by)
                menu.children('div.dropdown-menu-row').on({
                    click: function () {
                        $("#" + dropdown.data('for')).data({ groupFilterId: $(this).data().groupId, groupFilterName: $(this).data().groupName });
                        //hide the drop down
                        hideMenu();
                        $("[id$=-" + dropdown.data('for') + "]").val(' ').focus().trigger("keydown", { KeyCode: "32" });
                        return false;
                    }
                });
            });

            /* hide when clicked outside */
            $(document.body).on({
                click: function (e) {
                    if (showingParent) {
                        var parentElement = showingParent[0];
                        if (!$.contains(parentElement, e.target) || !parentElement == e.target) {
                            hideMenu();
                        }
                    }
                }
            });
        },
        HideClosestShowingDropdown: function () {
            var tr = $(this).parents('tr:first');

            if (tr.find('div.dropdown-menu').is(':visible')) {
                tr.find('.dropdown.btnFilter').removeClass(MediaPeople.activeClass);
                tr.find('div.dropdown-menu').hide();
            }
        },
        ShowModal: function (html, isLargeModal) {
            if (html != null)
                MediaPeople.SetModalHtml(html);

            if (isLargeModal)
                $("#ModalContainer").addClass("modal-lg");
            else
                $("#ModalContainer").removeClass("modal-lg");

            $('#Modal').modal('show');
        },
        HideModal: function () {
            $('#Modal').modal('hide');
        },
        SetModalHtml: function (html) {
            $('#ModalContainer').html(html);
        },
        InitBootstrapTooltips: function (specificSelector) {
            if (specificSelector == null)
                $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' });
            else
                $(specificSelector + ' [data-toggle="tooltip"]').tooltip({ trigger: 'hover' });
        },
        ShowHideSelectionCriteria: function () {
            MediaPeople.PreUpdateSelectionCriteria();

            if ($("p.prop_value", "#sc").length === 0) {
                $("#emptySC").removeClass(MediaPeople.visuallyHiddenClass);
            } else {
                $("#emptySC").addClass(MediaPeople.visuallyHiddenClass);

                $.each($("p.prop_value", "#sc"), function (index, value) {
                    $(this).closest("div.entity_prop").removeClass(MediaPeople.visuallyHiddenClass);
                    $(this).closest("div.entity").removeClass(MediaPeople.visuallyHiddenClass);
                });
            }
        },
        IsSelectionCriteriaEmpty: function () {
            return $("p.prop_value", "#sc").length === 0;
        },
        PreUpdateSelectionCriteria: function () {
            $("div.entity_prop", "#sc").addClass(MediaPeople.visuallyHiddenClass);
            $("div.entity", "#sc").addClass(MediaPeople.visuallyHiddenClass);
        },
        PostUpdateSelectionCriteria: function (id) {
            MediaPeople.ShowHideSelectionCriteria();

            //if (!$("div." + id + " > span").is(":animated"))
            //    $("div." + id + " > span").effect("highlight", 1200);

        },
        /* JqueryGlobalHandlers */
        RegisterGlobalRedirectOnInvalidSession: function () {
            $.ajaxSetup({
                statusCode: {
                    401: function () {
                        MediaPeople.RedirectToLoginPage();
                    }
                }
            });
        },
        RedirectToLoginPage: function(){
            location.href = $("body").attr("data-login-page");
        },
        InitCheckboxes: function (selector) {
            var checkboxSelector = ":checkbox";

            if (typeof selector !== "undefined")
                checkboxSelector = selector + " " + checkboxSelector;

            // The fancy css checkboxes on this site require a label to be straight
            // after the checkbox, but mvc renders 2 checkboxes with @Html.CheckboxFor
            // so we need to move the 2nd hidden one to after the label.
            $(checkboxSelector).each(function () {
                var hidden_input = $(this).next('input:hidden');

                if (hidden_input.length > 0) {
                    var label = hidden_input.next('label');

                    if (label.length > 0) {
                        //temporarily remove the hidden field
                        hidden_input.remove();
                        //re-add the hidden field after the label
                        label.after(hidden_input);
                    }
                }
            });
        },
        InitFileInput: function () {
            $('.custom-file-input').on('change', function () {
                var fileName = $(this).val().split('\\').pop();
                if (fileName === "")
                    $(this).next('.custom-file-label').removeClass("selected").html("Choose file");
                else
                    $(this).next('.custom-file-label').addClass("selected").html(fileName);
            });
        },
        ShowConfirm: function (obj) {
            showConfirm(obj);
        },
        DisplaySuccess: function (message, title) {
            toastr.success(message, title);
        },
        DisplayError: function (errorMessage, status) {
            if (status == 401) {
                MediaPeople.RedirectToLoginPage();
                return;
            }
            var html = $("#errorModal").removeClass("d-none");
            html.find("#lblErrorMessage").html(errorMessage === "" ? MediaPeople.genericError : errorMessage);
            MediaPeople.ShowModal(html);
        },
        SetCookie: function(cname, cvalue) {
            document.cookie = cname + "=" + cvalue + ";path=/";
        },
        GetCookie: function(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(";");
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },
        FitText: function(el, container, minSize){
            var text = el.text();
            var fSize = parseInt(el.css('font-size'));
            var fWeight = parseInt(el.css('font-weight'));
            var fFamily = el.css('font-family');
            var measured = measureText(text, fSize, fWeight, fFamily);
            var containerSize = container.width() - 10; //Take 10 off just to make sure it fits

            minSize = minSize ? minSize : 11; // Give it a default value if not passed

            if (measured.width > containerSize) {
                while (fSize > minSize) {
                    --fSize;
                    var m = measureText(text, fSize, fWeight, fFamily);

                    if (m.width <= containerSize) {
                        break;
                    }
                }

                el.css('font-size', fSize + 'px');
            }
        },
        Init: function () {
            MediaPeople.RegisterGlobalRedirectOnInvalidSession();
            initConfirm();

            MediaPeople.InitBootstrapTooltips();

            MediaPeople.InitCheckboxes();

            if ($("#sc").length)
                MediaPeople.ShowHideSelectionCriteria();

            CheckClientBrowser();

            $("#lnkBack").click(function (e) {
                window.history.back();
                e.preventDefault();
            });

            MediaPeople.InitJQueryValidate();
        }
    };

})();

$(document).ready(function () {
    MediaPeople.Init();
});
