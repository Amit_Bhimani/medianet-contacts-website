﻿/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Pages/common/grid.js"/>
/// <reference path="~/Scripts/Pages/common/add-to-list.js"/>
/// <reference path="~/Scripts/Plugins/jquery.tokeninput.js" />

MediaPeople.MyAccount = (function () {
    var privateData = $("#divOutlets");

    function initPrivateData() {
        if ($("#divPrivateData").length > 0 && privateData.length > 0)
            $.ajax({
                url: privateData.attr("data-action"),
                type: "GET",
                cache: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 401)
                        MediaPeople.RedirectToLoginPage();
                    else
                        privateData.html(MediaPeople.genericError);
                },
                success: function (data) {
                    privateData.html(data);
                    if ($("#formPrivateData").length == 0)
                        MediaPeople.PrivateOutlet.Init();
                    else
                        MediaPeople.PrivateOutlet.InitGrid();
                    configPrivateDataPagination();
                }
            });
    }

    function configPrivateDataPagination() {
        privateData.find("span.fa-sort,span.fa-sort-down").hide();
        privateData.find("span.sortable").removeClass("sortable");
        $("#result-navigation").hide();
        $("#PageSize").val(6);
        $("#Page").val(1);
    }
    return {
        configPrivateDataPagination: configPrivateDataPagination,
        Init: initPrivateData
    }
})();

$(document).ready(function () {
    MediaPeople.MyAccount.Init();
});
