﻿/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Pages/common/grid.js"/>
/// <reference path="~/Scripts/Plugins/jquery.tokeninput.js" />
/// <reference path="~/Scripts/Pages/common/search-common.js" />

MediaPeople.AdvancedSearch = (function () {
    var sqMinusClass = "ui-icon-squaresmall-minus"
        , sqPlusClass = "ui-icon-squaresmall-plus"
        , allTokenInputs = ["#SelectedPositions", "#SelectedFrequencies", "#SelectedLanguages", "#SelectedCContinents",
                            "#SelectedCCountries", "#SelectedCStates", "#SelectedCStates", "#SelectedCCities", "#SelectedOContinents", "#SelectedOCountries",
                            "#SelectedOStates", "#SelectedOCities", "#SelectedMediaTypes", "#SelectedCSubjects", "#SelectedOSubjects", "#SelectedTaskGroupIds",
                            "#SelectedListIds", "#SelectedTaskCategoryIds" ]
        , itemTmpl = "<li><p  class=\"prop_value\">{0}</p></li>"
        , tokenItemTmpl = "<li class=\"{0}\"><p  class=\"prop_value\">{1}</p></li>";

    var oma_outlet_recordtype = "";

    var ConvertToValidGroups = function (ds) {
        return $.map(ds, function (ele, index) {
            return { Id: "g" + ele.Id, Name: ele.Name };
        });
    }

    var termPrefix = ""; //used by Outlet autosuggest.

    function SaveSectionState(sectionId, collapsed) {
        var cookieVal = $.cookie(sectionId + "Pos");
        $.cookie(sectionId + "Pos", '{"Collapsed": "' + collapsed + '"}', { expires: 365, path: '/' });
    }

    function ConfigureSection(sectionId) {
        var cookieVal = $.cookie(sectionId + "Pos");
        if (cookieVal != null) {
            try {
                var val = $.parseJSON(cookieVal);

                if (val.Collapsed == "true") {
                    // Hide the section.
                    var section = $("#" + sectionId).children(".section2columnsTitle");
                    section.siblings().hide();
                    section.find(".expandArrow").removeClass(MediaPeople.arrowUpClass).addClass(MediaPeople.arrowDownClass);
                }
            }
            catch (ex) { }
        }
    }

    function SaveAllSectionStates(collapsed) {
        $("#aSearchWrapper .section2rows").each(function () {
            SaveSectionState($(this).prop("id"), collapsed);
        });
    }

    return {
        InitContactDetails: function () {
            MediaPeople.AdvancedSearch.InitPositions();
            MediaPeople.AdvancedSearch.InitContactSubjects();
        },
        InitPositions: function () {
            $("#SelectedPositions").tokenInput(MediaPeople.IsValidDatasource(_positions) ? _positions : [], $.extend({}, {
                hintText: "Type in a \"Position\"",
                placeholder: "Position",
                prePopulate: MediaPeople.IsValidDatasource(_selectedPositions) ? _selectedPositions : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedPositions", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedPositions", item);
                }
            }, MediaPeople.commonTokenInputSettings));
        },
        InitContactSubjects: function () {
            $("#SelectedCSubjects").tokenInput(function () {
                var groups = $.map($.grep($("#SelectedCSubjects").val().split(','), function (value, index) {
                    return value.match(MediaPeople.groupRegex);
                }), function (value, index) {
                    return value.slice(1);
                }).join(',');

                var subjects = $.grep($("#SelectedCSubjects").val().split(','), function (value, index) {
                    return !value.match(/^g\d+$/gi);
                }).join(',');

                return "/Data/Subjects?SelectedCSubjects=" + subjects + "&selectedGroups=" + groups + "&group=" + $("#SelectedCSubjects").data().groupFilterId;
            }, $.extend({}, {
                hintText: "Type in a \"Subject\"",
                placeholder: "Subject",
                showHeader: true,
                prePopulate: $.merge($.merge([], MediaPeople.IsValidDatasource(_selectedCSubjects) ? _selectedCSubjects : []), MediaPeople.IsValidDatasource(_selectedCSubjectGroups) ? ConvertToValidGroups(_selectedCSubjectGroups) : []),
                onReady: function () {
                    $("#token-input-SelectedCSubjects").on({
                        focus: function (e) {
                            MediaPeople.HideClosestShowingDropdown.call($(this));
                        }
                    });
                },
                onDelete: function (item) {
                    //if this item is a group -> find this group in the groups dropdown and add class addFilterGroup to the child span.
                    if (item.Id.toString().match(MediaPeople.groupRegex)) {
                        $(this).parent().parent().find("div.dropdown-menu a[data-group-id=" + item.Id.slice(1) + "] span").addClass('addFilterGroup').removeClass('removeFilterGroup');
                        MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedCSubjectGroups", { Id: item.Id.slice(1), Name: item.Name });
                    } else {
                        MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedCSubjects", item);
                    }
                },
                onAdd: function (item) {
                    if (item.Id.toString().match(MediaPeople.groupRegex)) {
                        MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedCSubjectGroups", { Id: item.Id.slice(1), Name: item.Name });
                    } else {
                        MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedCSubjects", item);
                    }
                },
                tokenFormatter: function (item) {
                    return MediaPeople.GetSubjectsTokenInputResultRow(item);
                },
                headerFormatter: function () {
                    return $("#SelectedCSubjects").data().groupFilterId ? "<p><a title='Click to add this subject group to the selection criteria' onclick=MediaPeople.AddSubjectGroupToTokenInput('SelectedCSubjects'); return false;>Filter: " + $("#SelectedCSubjects").data().groupFilterName + "</a></p>" : "";
                },
                onHide: function () {
                    MediaPeople.ClearSubjectFilterById("SelectedCSubjects");
                    return true;
                }
            }, MediaPeople.commonTokenInputSettings)
            );
        },
        InitOutletSubjects: function () {
            $("#SelectedOSubjects").tokenInput(function () {
                var groups = $.map($.grep($("#SelectedOSubjects").val().split(','), function (value, index) {
                    return value.match(MediaPeople.groupRegex);
                }), function (value, index) {
                    return value.slice(1);
                }).join(',');

                var subjects = $.grep($("#SelectedOSubjects").val().split(','), function (value, index) {
                    return !value.match(/^g\d+$/gi);
                }).join(',');

                return "/Data/Subjects?SelectedOSubjects=" + subjects + "&selectedGroups=" + groups + "&group=" + $("#SelectedOSubjects").data().groupFilterId;
            }, $.extend({}, {
                hintText: "Type in a \"Subject\"",
                placeholder: "Subject",
                showHeader: true,
                prePopulate: $.merge($.merge([], MediaPeople.IsValidDatasource(_selectedOSubjects) ? _selectedOSubjects : []), MediaPeople.IsValidDatasource(_selectedOSubjectGroups) ? ConvertToValidGroups(_selectedOSubjectGroups) : []),
                onReady: function () {
                    $("#token-input-SelectedOSubjects").on({
                        focus: function (e) {
                            MediaPeople.HideClosestShowingDropdown.call($(this));
                        }
                    });
                },
                onDelete: function (item) {
                    //if this item is a group -> find this group in the groups dropdown and add class addFilterGroup to the child span.
                    if (item.Id.toString().match(MediaPeople.groupRegex)) {
                        $(this).parent().parent().find("div.dropdown-menu a[data-group-id=" + item.Id.slice(1) + "] span").addClass('addFilterGroup').removeClass('removeFilterGroup');

                        MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedOSubjectGroups", { Id: item.Id.slice(1), Name: item.Name });
                    } else {
                        MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedOSubjects", item);
                    }
                },
                onAdd: function (item) {
                    if (item.Id.toString().match(MediaPeople.groupRegex)) {
                        MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedOSubjectGroups", { Id: item.Id.slice(1), Name: item.Name });
                    } else {
                        MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedOSubjects", item);
                    }
                },
                tokenFormatter: function (item) {
                    return MediaPeople.GetSubjectsTokenInputResultRow(item);
                },
                headerFormatter: function () {
                    return $("#SelectedOSubjects").data().groupFilterId
                                ? "<p><a title='Click to remove filter' href='#' onclick=MediaPeople.ClearSubjectFilterById('SelectedOSubjects');>Filter: "
                                    + $("#SelectedOSubjects").data().groupFilterName + "</a></p>"
                                : "";
                },
                headerFormatter: function () {
                    return $("#SelectedOSubjects").data().groupFilterId ? "<p><a title='Click to add this subject group to the selection criteria' onclick=MediaPeople.AddSubjectGroupToTokenInput('SelectedOSubjects'); return false;>Filter: " + $("#SelectedOSubjects").data().groupFilterName + "</a></p>" : "";
                },
                onHide: function () {
                    MediaPeople.ClearSubjectFilterById('SelectedOSubjects');
                    return true;
                }
            }, MediaPeople.commonTokenInputSettings)
            );
        },
        InitOutletDetails: function () {
            $("#SelectedMediaTypes").tokenInput(MediaPeople.IsValidDatasource(_mediaTypes) ? _mediaTypes : [], $.extend({}, {
                hintText: "Type in a \"Media\"",
                placeholder: "Media type",
                prePopulate: MediaPeople.IsValidDatasource(_selectedMediaTypes) ? _selectedMediaTypes : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedMediaTypes", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedMediaTypes", item);
                }
            }, MediaPeople.commonTokenInputSettings));

            $("#SelectedFrequencies").tokenInput(MediaPeople.IsValidDatasource(_frequencies) ? _frequencies : [], $.extend({}, {
                hintText: "Type in a \"Frequency\"",
                placeholder: "Frequency",
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedFrequencies", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedFrequencies", item);
                },
                prePopulate: MediaPeople.IsValidDatasource(_selectedFrequencies) ? _selectedFrequencies : []
            }, MediaPeople.commonTokenInputSettings));

            $("#SelectedLanguages").tokenInput(MediaPeople.IsValidDatasource(_languages) ? _languages : [], $.extend({}, {
                hintText: "Type in a \"Language\"",
                placeholder: "Language",
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedLanguages", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedLanguages", item);
                },
                prePopulate: MediaPeople.IsValidDatasource(_selectedLanguages) ? _selectedLanguages : []
            }, MediaPeople.commonTokenInputSettings));

            MediaPeople.AdvancedSearch.InitOutletSubjects();
        },
        InitListsSection: function () {
            var scClass = $("#SelectedListIds").data().hasOwnProperty('selectionCriteriaClass') ? $("#SelectedListIds").data().selectionCriteriaClass : "SelectedListIds";

            $("#SelectedListIds").tokenInput(MediaPeople.IsValidDatasource(_lists) ? _lists : [], $.extend({}, {
                hintText: "Type in a \"List\"",
                prePopulate: MediaPeople.IsValidDatasource(_selectedLists) ? _selectedLists : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedListIds", item, scClass);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedListIds", item, scClass);
                }
            }, MediaPeople.commonTokenInputSettings));
        },
        HideValidationErrors: function () {
            $("div.advSearchValidationError").addClass(MediaPeople.visuallyHiddenClass);
        },
        ShowValidationErrors: function () {
            var validationErrorDiv = $(this).parent().siblings("div.advSearchValidationError");
            validationErrorDiv.removeClass(MediaPeople.visuallyHiddenClass);
        },
        InitSelectionCriteria: function () {
            $("input:not([data-is-token-input]):not([id^='token-input-']):not([type=button]), #ContactInfluencerScore", "#AdvancedForm").on({
                change: function () {
                    MediaPeople.AdvancedSearch.HideValidationErrors();

                    var val = $(this).val();
                    var id = $(this).data().hasOwnProperty('selectionCriteriaId') ? $(this).data().selectionCriteriaId : $(this).attr('id');
                    var scClass = $(this).data().hasOwnProperty('selectionCriteriaClass') ? $(this).data().selectionCriteriaClass : id;
                    var emptyOut = false;

                    if ($.trim(val) == "") {
                        $("div." + scClass + " ul.prop_value").html("");
                    } else {
                        if ($(this).is(":checkbox"))
                            val = $(this).is(":checked") ? "Yes" : "No";
                        else if ($(this).is(":radio")) {
                            val = $(this).attr("displayname");
                        } else if ($(this).is("select")) { //this assumes that the valid id are all integers and non-negative.
                            if (val != "-1")
                                val = $(this).find(":selected").text();
                            else
                                emptyOut = true;
                        }

                        if (!emptyOut && !MediaPeople.InDefaultCriteria(id, val))
                            $("div." + scClass + " > ul.prop_value").html(itemTmpl.replace("{0}", val));
                        else
                            $("div." + scClass + " ul.prop_value").html("");
                    }

                    MediaPeople.PostUpdateSelectionCriteria(scClass);
                }
            });
            MediaPeople.ShowHideSelectionCriteria();
        },
        RemoveTokenInputSelectionCriteria: function (id, item, scClass) {
            MediaPeople.AdvancedSearch.HideValidationErrors();
            MediaPeople.PreUpdateSelectionCriteria();
            var scClass = (scClass ? scClass : id);

            $("div." + scClass).find("li." + item.Id).remove();

            MediaPeople.PostUpdateSelectionCriteria(scClass);
        },
        AddTokenInputSelectionCriteria: function (id, item, scClass) {
            MediaPeople.AdvancedSearch.HideValidationErrors();
            MediaPeople.PreUpdateSelectionCriteria();
            var scClass = (scClass ? scClass : id);

            $(tokenItemTmpl.replace("{0}", item.Id).replace("{1}", item.Name)).appendTo($("div." + scClass + " > ul"));

            MediaPeople.PostUpdateSelectionCriteria(scClass);
            try { $("#" + id).tokenInput("redraw"); }
            catch (e) {//isn't a tokenInput; possibly subjectgroups.
            }
        },
        InitContactGeo: function () {
            /* contact continents dropdowns */
            $("#SelectedCContinents").tokenInput(MediaPeople.IsValidDatasource(_continents) ? _continents : [], $.extend({}, {
                hintText: "Type in a \"Continent\"",
                placeholder: "Continent",
                prePopulate: MediaPeople.IsValidDatasource(_selectedCContinents) ? _selectedCContinents : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedCContinents", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedCContinents", item);
                }
            }, MediaPeople.commonTokenInputSettings));

            /* contact countries dropdown */
            $("#SelectedCCountries").tokenInput(function () {
                return "/Data/Countries?continents=" + $("#SelectedCContinents").val();
            }, $.extend({}, {
                hintText: "Type in a \"Country\"",
                placeholder: "Country",
                showHeader: true,
                prePopulate: MediaPeople.IsValidDatasource(_selectedCCountries) ? _selectedCCountries : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedCCountries", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedCCountries", item);
                }
            }, MediaPeople.commonTokenInputSettings)
            );

            /* contact states dropdown */
            $("#SelectedCStates").tokenInput(function () {
                return "/Data/States?countries=" + $("#SelectedCCountries").val();
            }, $.extend({}, {
                hintText: "Type in a \"State\"",
                placeholder: "State",
                showHeader: true,
                prePopulate: MediaPeople.IsValidDatasource(_selectedCStates) ? _selectedCStates : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedCStates", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedCStates", item);
                }
            }, MediaPeople.commonTokenInputSettings)
            );

            /* contact states dropdown */
            $("#SelectedCCities").tokenInput(function () {
                return "/Data/Cities?states=" + $("#SelectedCStates").val() + "&countries=" + $("#SelectedCCountries").val(); //we need the countries to find cities
            }, $.extend({}, {
                hintText: "Type in a \"City\"",
                placeholder: "City",
                showHeader: true,
                prePopulate: MediaPeople.IsValidDatasource(_selectedCCities) ? _selectedCCities : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedCCities", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedCCities", item);
                }
            }, MediaPeople.commonTokenInputSettings)
            );
        },
        InitOutletGeo: function () {
            /* outlet continents dropdown */
            $("#SelectedOContinents").tokenInput(MediaPeople.IsValidDatasource(_continents) ? _continents : [], $.extend({}, {
                hintText: "Type in a \"Continent\"",
                placeholder: "Continent",
                prePopulate: MediaPeople.IsValidDatasource(_selectedOContinents) ? _selectedOContinents : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedOContinents", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedOContinents", item);
                }
            }, MediaPeople.commonTokenInputSettings));

            /* outlet countries dropdown */
            $("#SelectedOCountries").tokenInput(function () {
                return "/Data/Countries?continents=" + $("#SelectedOContinents").val();
            }, $.extend({}, {
                hintText: "Type in a \"Country\"",
                placeholder: "Country",
                prePopulate: MediaPeople.IsValidDatasource(_selectedOCountries) ? _selectedOCountries : [],
                showHeader: true,
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedOCountries", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedOCountries", item);
                }
            }, MediaPeople.commonTokenInputSettings)
            );

            /* outlet states dropdown */
            $("#SelectedOStates").tokenInput(function () {
                return "/Data/States?countries=" + $("#SelectedOCountries").val();
            }, $.extend({}, {
                hintText: "Type in a \"State\"",
                placeholder: "State",
                showHeader: true,
                prePopulate: MediaPeople.IsValidDatasource(_selectedOStates) ? _selectedOStates : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedOStates", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedOStates", item);
                }
            }, MediaPeople.commonTokenInputSettings)
            );

            /* outlet states dropdown */
            $("#SelectedOCities").tokenInput(function () {
                return "/Data/Cities?states=" + $("#SelectedOStates").val() + "&countries=" + $("#SelectedOCountries").val();
            }, $.extend({}, {
                hintText: "Type in a \"City\"",
                placeholder: "City",
                showHeader: true,
                prePopulate: MediaPeople.IsValidDatasource(_selectedOCities) ? _selectedOCities : [],
                onAdd: function (item) {
                    MediaPeople.AdvancedSearch.AddTokenInputSelectionCriteria("SelectedOCities", item);
                },
                onDelete: function (item) {
                    MediaPeople.AdvancedSearch.RemoveTokenInputSelectionCriteria("SelectedOCities", item);
                }
            }, MediaPeople.commonTokenInputSettings)
            );
        },
        ResetForm: function () {
            var radios = [];

            $(':input', "#AdvancedForm")
                     .not(':button, :submit, :reset, [type=hidden]').not("[default]")
                     .val('')
                     .removeAttr('checked')
                     .removeAttr('selected').change();

            $(':input[default]', "#AdvancedForm").each(function (index, value) {
                if ($(this).is(":radio")) {
                    radios.push($(this).attr('name'));
                } else if ($(this).is(":checkbox")) {
                    $(this).prop('checked', $(this).attr('default') === "true").change();
                } else {
                    $(this).val($(this).attr('default')).change();
                }
            });

            //clear all tokenInputs.
            $.each(allTokenInputs, function (index, value) {
                var tInput = $(value);
                if (tInput.length > 0)
                    tInput.tokenInput("clear");
            });

            $.each($.unique(radios), function (index, value) {
                $("[value=" + $("[name=" + value + "]:first").attr('default') + "]").prop('checked', true).change();
            });

            //hide all error messages generated from validate plugin
            $("#AdvancedForm").valid();
        },
        InitForm: function () {
            try {
                if ($("#AdvancedForm").length > 0) {
                    oma_outlet_recordtype = parseInt($("#AdvancedForm").attr("data-oma-outlet-recordtype"), 10);

                    MediaPeople.AdvancedSearch.InitContactGeo();
                    MediaPeople.AdvancedSearch.InitOutletGeo();
                    MediaPeople.AdvancedSearch.InitContactDetails();
                    MediaPeople.AdvancedSearch.InitOutletDetails();
                    MediaPeople.AdvancedSearch.InitSelectionCriteria();

                    /*Init Subjects groups dropdowns*/
                    MediaPeople.InitFilterDropdowns();

                    /* cilcking on the show all should trigger the dropdowns */
                    $(".btnShowAll").on("click", function (index, value) {
                        $(this).parent().siblings(".searchInput").find("input:visible").focus().val(" ").trigger("keydown", { KeyCode: "32" });
                    });

                    /* initalize validation of the form */
                    $("#AdvancedForm").validate({
                        errorClass: "input-validation-error",
                        errorElement: "div",
                        rules: {
                            OutletCirculationMin: {
                                number: true
                            },
                            OutletCirculationMax: {
                                number: true
                            }
                        }
                    });
                    /* clicking on any element with role=submit should submit the closest form */
                    $("[role='submit']").on({
                        click: function() {
                            //if we do not have anything in selection criteria then display validation errors
                            if ($("#AdvancedForm").valid()) {
                                if (MediaPeople.IsSelectionCriteriaEmpty()) {
                                    MediaPeople.AdvancedSearch.ShowValidationErrors.call($(this));
                                } else {
                                    $(this).closest("form").submit();
                                }
                            } else {
                                $("div.input-validation-error:visible").effect("highlight", 1200);
                            }
                        }
                    });

                    /* clicking on any element with role=reset should reset the whole form */
                    $("[role='reset']").on({
                        click: function(event) {
                            if ($('html').hasClass('oldie')) {
                                window.location.replace(location.protocol + '//' + location.host + location.pathname);
                            } else {
                                MediaPeople.AdvancedSearch.ResetForm();
                                event.stopPropagation();
                                return false;
                            }
                        }
                    });

                    //set focus on the full name. This is primarily done because of a css issue if any of the tokeninputs are focused (because of preloading them)
                    // and the panel containing that token input is hidden or clicked to hide it
                    $("#advPeople > h3").focus();
                }
            } catch (err) {
                console.error(err);
            }
        },
        InitAutoComplete: function () {
            try {
                if ($("#OutletName").length > 0) {
                    // Don't navigate away from the field on tab when selecting an item                
                    $("#OutletName").bind("keydown", function(event) {
                            if (event.keyCode === 13) {
                                var activeItem = $(this).typeahead("getActive");
                                if (activeItem != null && activeItem.length > 0)
                                    $("#OutletName").val(activeItem);

                                $("#QuickForm").submit();
                            }
                        })
                        .typeahead({
                            minLength: 2,
                            hint: true,
                            highlight: true,
                            source: function(query, process) {
                                var term = query;
                                termPrefix = "";
                                var andPos = term.lastIndexOf("AND ");
                                var orPos = term.lastIndexOf("OR ");
                                var notPos = term.lastIndexOf("NOT ");

                                if (andPos >= 0 || orPos >= 0 || notPos >= 0) {
                                    var pos = (andPos > orPos ? andPos : orPos);
                                    pos = (pos > notPos ? pos : notPos);
                                    var len = (pos == orPos ? 3 : 4);

                                    termPrefix = term.substring(0, pos + len);
                                    term = term.substring(pos + len);
                                }

                                $.ajax({
                                    url: "/Search/AutoSuggest",
                                    dataType: "json",
                                    data: {
                                        term: term,
                                        context: "Outlet",
                                        country: 2
                                    },
                                    success: function(data) {
                                        //process(data);
                                        var result = [];
                                        $.each(data, function (i, item) {
                                            result.push(item.Name + "#" + item.RecordType);
                                        });

                                        process(result);
                                    }
                                });
                            },
                            highlighter: function (item) {

                                var parts = item.split('#');
                                var recordType = parseInt(parts[1], 10);
                                var data = (recordType >= oma_outlet_recordtype ? $("#prIcon").html() : "") + "  " + parts[0];
                                var regex = new RegExp('(' + this.query + ')', 'gi');
                                return data.replace(regex, "<strong>$1</strong>");
                            },
                            updater: function (item) {
                                var parts = item.split('#');
                                return parts[0];
                            }
                        });
                }
            } catch (err) {
                console.error(err);
            }
        },
        InitGrid: function () {
            if ($(".resultSection").length > 0) {
                var options = {
                    resultsContent: "searchResultSection"
                }

                MediaPeople.Grid.Init(options);
            }
        },
        Init: function () {
            if (history.state) {
                MediaPeople.QuickSearch.LoadFromHistory(history.state);
            }
            MediaPeople.SearchCommon.Init();
            MediaPeople.AdvancedSearch.InitForm();
            MediaPeople.AdvancedSearch.InitGrid(null, false);
            MediaPeople.AdvancedSearch.InitAutoComplete();
        }
    }
})();

$(document).ready(function () {
    MediaPeople.AdvancedSearch.Init();
});

