﻿/// <reference path="~/Scripts/Global"/>
/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js"/>
/// <reference path="~/Scripts/Global/common.js" />

MediaPeople.AllSavedSearches = (function () {

    function DeleteSavedSearch(obj) {
        var searchId = $(obj).data("id");
        var url = $(obj).attr("data-action");
        var spinner = $(obj).parentsUntil('[data-spinnercontainer]').parent()[0];
        var spinnername = "main";
        if (spinner) {
            spinnername = $(spinner).data('spinnercontainer');
        }
        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner(spinnername);
            },
            url: url,
            type: "POST",
            data: { searchId: searchId },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError('Failed to delete search', jqXHR.status);
            },
            success: function () {
                var body = $("tr[data-search-id =" + searchId + "]").parent();
                $("tr[data-search-id =" + searchId + "]").remove();
                if (!$(body).has("tr").length) {
                    $(body).parentsUntil("div[data-group]").parent().remove();
                }
                MediaPeople.DisplaySuccess('Search deleted successfully', '');
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner(spinnername);
            }
        });

    }

    return {
        DeleteSavedSearch: DeleteSavedSearch
    };
})();

$(document).ready(function () {
    window.MediaPeople.Group.Init("[data-search-id]", "MediaPeople.AllSavedSearches.DeleteSavedSearch(obj)");
});

