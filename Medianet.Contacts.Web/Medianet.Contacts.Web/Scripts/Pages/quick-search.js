﻿/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Pages/common/grid.js"/>
/// <reference path="~/Scripts/Plugins/jquery.tokeninput.js" />
/// <reference path="~/Scripts/Pages/common/search-common.js" />

MediaPeople.QuickSearch = (function () {

    var oma_outlet_recordtype = "";
    var selectedSearchText = "";

    function initRecentSearches() {
        var container = $('#divRecentSearches');
        if (container == null || container.length == 0) return;
        $.ajax({
            url: (container.attr("data-action")),
            cache: false,
            success: function (data) {
                container.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    container.html(MediaPeople.genericError);
            }
        });
    }

    function initRecentlyAdded() {
        var container = $('#divRecentlyAdded');
        if (container == null || container.length == 0) return;
        $.ajax({
            url: (container.attr("data-action")),
            cache: false,
            success: function (data) {
                container.html(data);
                MediaPeople.Socialmedia.GetAllTwitterImages(true, "#divRecentlyAdded");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    container.html(MediaPeople.genericError);
            }
        });
    }

    function initRecentlyUpdated() {
        var container = $('#divRecentlyUpdated');
        if (container == null || container.length === 0) return;
        $.ajax({
            url: (container.attr("data-action")),
            cache: false,
            success: function (data) {
                container.html(data);
            },
                    error : function (jqXhr, textStatus, errorThrown) {
                if (jqXhr.status === 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    container.html(MediaPeople.genericError);
                }
        });
    }

    function initRecentLists() {
        var container = $('#divRecentLists');
        if (container == null || container.length == 0) return;
        $.ajax({
            url: (container.attr("data-action")),
            cache: false,
            success: function (data) {
                container.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    container.html(MediaPeople.genericError);
            }
        });
    }

    function initMediaMovements() {
        var container = $('#divMediaMovements');
        if (container == null || container.length == 0) return;
        $.ajax({
            url: (container.attr("data-action")),
            cache: false,
            success: function (data) {
                container.html(data);
                MediaPeople.Socialmedia.GetAllTwitterImages(true, "#divMediaMovements");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    container.html(MediaPeople.genericError);
            }
        });
    }

    function initMediaStatistics() {
        var container = $('#divMediaStatitics');
        if (container == null || container.length == 0) return;
        $.ajax({
            url: (container.attr("data-action")),
            cache: false,
            success: function (data) {
                container.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    container.html(MediaPeople.genericError);
            }
        });
    }

    function enabledDisableLocation() {
        toggleTokenInput("Locations", $("#Country").val() == _australiaCountry);
    }

    function initTokenInputDropdowns() {
        $.getJSON("/Data/Positions", function (data) {
            $("#Positions").tokenInput(data, $.extend({}, {
                hintText: "Type in a \"Position\"",
                placeholder: "Position",
                prePopulate: $.isArray(_positions) ? _positions : [],
                onReady: function () {
                    setTimeout(enableDisablePosition, 500);
                }
            }, MediaPeople.commonTokenInputSettings));
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 401)
                MediaPeople.RedirectToLoginPage();
        });

        $.getJSON("/Data/MediaTypes", function (data) {
            $("#MediaTypes").tokenInput(data, $.extend({}, {
                hintText: "Type in a \"Media type\"",
                placeholder: "Media type",
                prePopulate: $.isArray(_mediaTypes) ? _mediaTypes : []
            }, MediaPeople.commonTokenInputSettings))
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 401)
                MediaPeople.RedirectToLoginPage();
        });

        $.getJSON("/Data/Locations", function (data) {
            var ddWrapper, tokensList;
            $("#Locations").tokenInput(data, $.extend({}, {
                placeholder: "State/Region",
                hintText: null,
                hideOnRemove: false,
                prePopulate: $.isArray(_locations) ? _locations : [],
                onReady: function () {
                    //init variables. We cannot use the utility function GetInputToken(id) yet as the TokenInput is not initialised yet.
                    ddWrapper = $("#token-input-Locations-token-input-dropdown-mediapeople");
                    if (!tokensList) tokensList = $("#token-input-Locations-token-input-list-mediapeople");
                    tokensList.on({
                        click: function (e) {
                            $(this).parents('div.select-container:first').find('.dropdown.btnShowAll').click();
                        }
                    });

                    setTimeout(enabledDisableLocation, 200);

                    $("#token-input-Locations").prop("readonly", "readonly").on(
                        "keypress", function (e) {
                            $(this).val('');
                            return false;
                        }
                    );
                },
                onDelete: function (item) {
                    //if this is a parent. We need to delete all children
                    if (!item.ParentId) {
                        var vals = $("#Locations").tokenInput("get");
                        $($.grep(vals, function (a) { return a.ParentId && a.ParentId == item.Id; })).each(function (index, value) {
                            $("#Locations").tokenInput("remove", value);
                        });
                    }
                },
                onAdd: function (item) {
                    if (!item.ParentId) { // is a parent
                        if (!item.AvoidRecursion) {
                            //add all children to the textbox
                            var children = [];
                            ddWrapper.find("[data-parent-id='" + item.Id + "']").each(function (index, value) {
                                var dt = $(this).data();
                                if (dt)
                                    children.push({ Id: dt.id, Name: dt.name, ParentId: dt.parentId });
                            });

                            //remove all children from dropdown
                            ddWrapper.find("[data-parent-id='" + item.Id + "']").parent().remove();

                            $.each(children, function (index, value) {
                                $("#Locations").tokenInput("add", value);
                            });
                        }

                        //hide all children in textbox
                        var vals = $("#Locations").tokenInput("get");

                        $($.grep(vals, function (a) { return a.ParentId && a.ParentId == item.Id; })).each(function (index, value) {
                            tokensList.find('li:contains("' + value.Id + '")').addClass(MediaPeople.visuallyHiddenClass);
                        });
                    }
                    else { //is a child
                        //Is this the last child?
                        if (ddWrapper.find("[data-parent-id='" + item.ParentId + "']").length == 1) {
                            var prt = ddWrapper.find("[data-id='" + item.ParentId + "']");
                            var parent = prt.data();

                            if (parent) {      //add the parent State to the textbox
                                $("#Locations").tokenInput("add", { Id: parent.id, Name: parent.name, AvoidRecursion: true, ParentId: null });
                                prt.parent().remove();
                                //remove parent dom.
                            }
                        }
                    }

                    $("#Locations").tokenInput("redraw");
                },
                tokenFormatter: function (item) {
                    //if adding a parent then add MediaPeople.visuallyHiddenClass to all the children. This is 
                    // pretty much to handle prePopulate. Otherwise, onAdd should handle all the cases.
                    if (!tokensList) tokensList = $("#token-input-Locations-token-input-list-mediapeople"); // tokenList is initialised onReady and this function is called for prePopulate before onReady.

                    if (item.hasOwnProperty('ParentId') && !item.ParentId) {
                        tokensList.find('li[data-parent-id=' + item.Id + ']').addClass(MediaPeople.visuallyHiddenClass);
                    }

                    return "<li data-parent-id=\"" + (item.hasOwnProperty('ParentId') ? item.ParentId : 'null') + "\"><p>" + item.Id + "</p></li>";
                },
                resultsFormatter: function (item) {
                    return item.ParentId
                    ? "<li class=\"ml-1\"><div class=\"\" data-id=\"" + item.Id + "\" data-parent-id=\"" + item.ParentId + "\" data-name=\"" + item.Name + "\" " + item.ParentId + "\">" + item.Name + "</div></li>"
                    : "<li><div class=\"\" data-id=\"" + item.Id + "\" data-parent-id=\"" + item.ParentId + "\" data-name=\"" + item.Name + "\" " + item.ParentId + ">" + item.Name + "</div></li>";
                }
            }, MediaPeople.commonTokenInputSettings, { preventDuplicates: false, searchDelay: 0 }));
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 401)
                MediaPeople.RedirectToLoginPage();
        });

        $("#Subjects").tokenInput(function () {
            var groups = $.map($.grep($("#Subjects").val().split(','), function (value, index) {
                return value.match(MediaPeople.groupRegex);
            }), function (value, index) {
                return value.slice(1);
            }).join(',');

            var subjects = $.grep($("#Subjects").val().split(','), function (value, index) {
                return !value.match(/^g\d+$/gi);
            }).join(',');

            return "/Data/Subjects?selectedSubjects=" + subjects + "&selectedGroups=" + groups + "&group=" + $("#Subjects").data().groupFilterId;
        }, $.extend({}, {
            hintText: "Type in a \"Subject\"",
            placeholder: "Subject",
            showHeader: true,
            prePopulate: $.merge($.merge([], MediaPeople.IsValidDatasource(_subjects) ? _subjects : []), MediaPeople.IsValidDatasource(_subjectGroups) ? convertToValidGroups(_subjectGroups) : []),
            onReady: function () {
                $("#token-input-Subjects").on({
                    focus: function (e) {
                        MediaPeople.HideClosestShowingDropdown.call($(this));
                    }
                });
            },
            onDelete: function (item) {
                //if this item is a group -> find this group in the groups dropdown and add class addFilterGroup to the child span.
                if (item.Id.toString().match(MediaPeople.groupRegex)) {
                    $(this).parent().parent().find("div.dropdown-menu a[data-group-id=" + item.Id.slice(1) + "] span").addClass('addFilterGroup').removeClass('removeFilterGroup');
                }
            },
            headerFormatter: function () {
                return $("#Subjects").data().groupFilterId ? "<p><a title='Click to add this subject group to the selection criteria' onclick=MediaPeople.AddSubjectGroupToTokenInput('Subjects'); return false;>Filter: " + $("#Subjects").data().groupFilterName + "</a></p>" : "";
            },
            tokenFormatter: function (item) {
                return MediaPeople.GetSubjectsTokenInputResultRow(item);
            },
            onHide: function () {
                MediaPeople.ClearSubjectFilterById('Subjects');
                return true;
            }
        }, MediaPeople.commonTokenInputSettings)
        );
    }

    /// <summary>
    /// Toggles (not for visibility; it is for enabling/disabling) the TokenInput control/input-box.
    /// </summary>
    /// <param name="id">The id of the input-box that the TokenInput was instantiated on.</param>
    /// <param name="enable">Optional parameter. It is a boolean type. If passed in: True will enable and false will disable the TokenInput control. Else: return True 
    /// if TokenInput is enabled else False</param>
    function toggleTokenInput(id, enable) {
        try {
            //var ti = MediaPeople.GetInputToken(id);
            var parentTbl = $("#" + id).closest("div.select-container");

            if (enable != "undefined" && typeof enable === "boolean") {
                enable ? parentTbl.removeClass(MediaPeople.uiDisabledClass) : parentTbl.addClass(MediaPeople.uiDisabledClass);

                $(':input', parentTbl).prop('disabled', !enable);

                $("#" + id).tokenInput("toggleDisabled", !enable);
            }
            else {
                return !parentTbl.hasClass(MediaPeople.uiDisabledClass);
            }
        }
        catch (ex) {
        }
    }

    function enableDisablePosition() {
        toggleTokenInput("Positions", $("#Context").val() != "Outlet");
    }

    function changeSearchContext(obj) {
        var val = obj != null ? $(obj).attr("data-search-context") : $("#Context").val();

        $("#Context").val(val);
        $("button[data-search-context]").removeClass("active");
        $("button[data-search-context=" + val + "]").addClass("active");
        enableDisablePosition();
    }

    function initAutoComplete() {
        if ($("#QuickForm") != null) {
            oma_outlet_recordtype = parseInt($("#QuickForm").attr("data-oma-outlet-recordtype"), 10);
        }

        $('#SearchText')
            .bind("keyup", function (event) {
                if (event.keyCode === 13) {
                    var activeItem = $(this).data("typeahead").$menu.find('.active');
                    if (activeItem == null || activeItem.length === 0)
                        $("#QuickForm").submit();
                }
            })
            .typeahead({
                minLength: 2,
                highlight: true,
                autoSelect: false,
                items: 100,
                source: function (query, process) {
                    return $.ajax({
                        url: "/Search/AutoSuggest",
                        dataType: "json",
                        data: {
                            term: query,
                            context: $("#Context").val(),
                            country: $("#Country").val()
                        },
                        success: function (data) {
                            var list = [];
                            $.each(data, function (i, item) {
                                list.push(item.Name + "#" + item.RecordType + "#" + item.Id);
                            })
                            process(list);
                        }
                    });
                },
                matcher: function (item) {
                    return true;
                },
                highlighter: function (item) {
                    var parts = item.split("#");
                    var recordType = parseInt(parts[1], 10);
                    var data = (recordType >= oma_outlet_recordtype ? $("#prIcon").html() : "") + "  " + parts[0];
                    var regex = new RegExp("(" + this.query + ")", "gi");
                    return data.replace(regex, "<strong>$1</strong>");

                },
                updater: function (item) {
                    return getSelectedItem(item);
                },
                afterSelect: function (item) {
                    if (item !== null)
                        $("#QuickForm").submit();
                }
            });
    }

    function getSelectedItem(item) {
        var parts = item.split('#');

        // Record the Id of the item selected so we can do a search by Id
        $("#SelectedId").val(parts[2]);
        $("#SelectedRecordType").val(parts[1]);
        selectedSearchText = parts[0];

        return parts[0];
    }

    function initRecentSavedSearches() {
        var recentSavedSearchBody = $("#recentSavedSearches");

        if (recentSavedSearchBody.length > 0) {
            $.ajax({
                url: (recentSavedSearchBody.attr("data-action")),
                type: "GET",
                cache: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 401)
                        MediaPeople.RedirectToLoginPage();
                    else
                        recentSavedSearchBody.html(MediaPeople.genericError);
                },
                success: function (data) {
                    recentSavedSearchBody.html(data);
                }
            });
        }
    }

    function convertToValidGroups(ds) {
        return $.map(ds, function (ele, index) {
            return { Id: "g" + ele.Id, Name: ele.Name };
        });
    }

    function showValidationErrors(isValid) {
        var validationErrorDiv = $("#searchValidationError");

        if (!isValid)
            validationErrorDiv.addClass(MediaPeople.visuallyHiddenClass)
        else
            validationErrorDiv.removeClass(MediaPeople.visuallyHiddenClass);
    }

    function isSearchFormValid() {
        var anyNonEmptyText = false;

        $("#QuickForm :text:enabled:visible").each(function (index, value) {
            var id = $(this).attr('id');

            if (id.match(MediaPeople.tokenInputRegex)) {
                var matches = MediaPeople.tokenInputRegex.exec(id);

                if (matches && matches.length >= 2 && $("#" + matches[1]).tokenInput("get").length) {
                    anyNonEmptyText = true;
                }
            } else {
                if ($(value).val() != "")
                    anyNonEmptyText = true;
            }
        });

        return anyNonEmptyText;
    }

    function initSearchForm() {
        initTokenInputDropdowns();
        MediaPeople.InitFilterDropdowns();

        $("#Country").on({
            change: function () {
                enabledDisableLocation();
            }
        });

        // clicking on the show all should trigger the dropdowns
        $(".btnShowAll").on('click', function (index, value) {
            $(this).parent().siblings('.searchInput').find("input:visible").focus().val(' ').trigger("keydown", { KeyCode: "32" });
        });

        // Bind form submission.
        setTimeout(function () {
            $("#QuickForm").unbind("submit").submit(function (event) {
                if (!isSearchFormValid()) {
                    showValidationErrors(true);
                    return false;
                }
                showValidationErrors(false);

                // If the search text has changed from what was selected in
                // the dropdown then its no longer an Id search
                if ($("#SearchText").val() !== selectedSearchText) {
                    $("#SelectedId").val('');
                }
            });
        }, 100);

        $("button[data-search-context]").click(function () {
            changeSearchContext(this);
        });
        changeSearchContext();

        /* clicking on any element with role=reset should reset the whole form */
        $("#btnClearQuickSearch").click(function (event) {
            $(":input", "#QuickForm")
                .not(":button, :submit, :reset, [type=hidden]").not("[default]")
                .val("").change();

            //clear all tokenInputs.
            $.each(["#Subjects", "#Positions", "#MediaTypes", "#Locations"], function (index, value) {
                var tInput = $(value);
                if (tInput.length > 0)
                    tInput.tokenInput("clear");
            });

            $("#Country").val(_australiaCountry).trigger("change");

            //hide all error messages generated from validate plugin
            showValidationErrors(false);

            return false;
        });

        $("select,input:not([data-is-token-input]):not([id^='token-input-']):not([type=button])", "#QuickForm").on({
            change: function () {
                showValidationErrors(false);
            }
        });

        if ($("#SelectedId").val() !== "") {
            selectedSearchText = $("#SearchText").val();
        }
    }

    function initGrid() {
        if ($(".searchResultSection").length > 0 && $("[data-advancedsearch]").length <= 0) {
            var options = {
                resultsContent: "searchResultSection"
            }
        
            MediaPeople.Grid.Init(options, history.state == null && $("#Page").val() === "1");
            //if (history.state && history.state.Page && history.state.Page > 0)
            //    loadFromHistory(history.state);
        }
    }

    function initResearchRequest() {
        $("#btnSubmitRequest").on("click", requestResearch);
    }

    function requestResearch() {
        var requestText = $("#txtReserchRequest");

        $.ajax({
            url: requestText.attr("data-action"),
            type: "POST",
            data: { details: requestText.val() },
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("divResearchRequest");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    MediaPeople.DisplayError("Failed to send research request", jqXHR.status);
            },
            success: function (data) {
                MediaPeople.DisplaySuccess("Research request sent", "");
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("divResearchRequest");
            }
        });
        return false;
    }

    function initPopover() {
        var searchPopupShown = MediaPeople.GetCookie("SearchPopupShown");

        if (searchPopupShown !== "true") {
            var popup = $("#searchHelpPopup");
            // Make it vsible
            popup.removeClass('d-none');

            // If they close it then set a cookie to prevent it being shown for the rest of the session
            popup.on('closed.bs.alert', function () {
                MediaPeople.SetCookie("SearchPopupShown", "true");
            })
        }
    }

    function initPage() {
        initSearchForm();
        initGrid();
        MediaPeople.SearchCommon.Init();
        initAutoComplete();
        initPopover();
        initMediaMovements();
        initRecentSearches();
        initMediaStatistics();
        initRecentlyAdded();
        initRecentSavedSearches();
        initRecentLists();
        initRecentlyUpdated();
        initResearchRequest();
    }
    function loadFromHistory(params) {
        $("#Page").val(params.Page),
        $("#PageSize").val(params.PageSize);
        $("#ResultCount").val(params.ResultCount);
        $("#CurrentPageOnly").val(params.CurrentPageOnly);
        MediaPeople.Grid.Rebind();
    }
    return {
        LoadFromHistory: loadFromHistory,
        Init: initPage
    }
})();

window.onpopstate = function (e) {
    try {
        if (e && e.state)
            MediaPeople.QuickSearch.LoadFromHistory(e.state);
    } catch (e) { }
};

$(document).ready(function () {
    MediaPeople.QuickSearch.Init();
});
