﻿/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Pages/common/grid.js"/>

MediaPeople.PrivateOutlet = (function () {

    var ddcontinent = $("#ContinentId");
    var ddCountry = $("#CountryId");
    var ddState = $("#StateId");
    var ddCity = $("#CityId");
    var form = ddcontinent.length > 0 ? $("#formPrivateData") : null;
    var oma_outlet_recordtype = "";
    var prn_outlet_recordtype = "";

    function continentChange() {
        ddcontinent.change(function () {
            ddCountry.find("option:gt(0)").remove();
            ddState.find("option:gt(0)").remove();
            ddCity.find("option:gt(0)").remove();

            $.ajax({
                url: form.attr("data-get-countries-url") + "?continentId=" + ddcontinent.val(),
                type: "GET",
                success: function (data) {
                    $.each(data, function (i, item) {
                        ddCountry.append($("<option></option>").attr("value", item.Id).text(item.Name));
                    });
                }
            });
        });
    }

    function countryChange() {
        ddCountry.change(function () {
            ddState.find("option:gt(0)").remove();
            ddCity.find("option:gt(0)").remove();

            $.ajax({
                url: form.attr("data-get-states-url") + "?countryId=" + ddCountry.val(),
                type: "GET",
                success: function (data) {
                    $.each(data, function (i, item) {
                        ddState.append($("<option></option>").attr("value", item.Id).text(item.Name));
                    });
                }
            });
        });
    }

    function stateChange() {
        ddState.change(function () {
            ddCity.find("option:gt(0)").remove();

            $.ajax({
                url: form.attr("data-get-cities-url") + "?countryId=" + ddCountry.val() + "&stateId=" + ddState.val(),
                type: "GET",
                success: function (data) {
                    $.each(data, function (i, item) {
                        ddCity.append($("<option></option>").attr("value", item.Id).text(item.Name));
                    });
                }
            });
        });
    }

    function fetchParams() {
        return {
            isSearchPage: false
        };
    }

    function initGrid() {
        if ($(".resultSection").length > 0 && $("#divOutlets").length > 0) {
            var options = {
                singleDeleteDataIdName: "data-id",
                deleteDataMethod: fetchParams,
                rebindDataMethod: fetchParams,
                deleteUrl: $("#divOutlets").attr("data-delete-action"),
                resultsContent: "divOutlets",
                showSpinner: true,
                deleteSuccess: function () {
                    initGrid();
                },
                rebindSuccess: function () {
                    try {
                        if ($("#divPrivateData").length > 0)
                            MediaPeople.MyAccount.configPrivateDataPagination();
                    } catch (e) { }
                }
            };

            MediaPeople.Grid.Init(options, false);
        }
    }

    function initPage() {
        if (form == null) {
            initGrid();
            return;
        }

        if ($("#OutletName").length > 0) {
            oma_outlet_recordtype = +$("#formPrivateData").attr("data-oma-outlet-recordtype");
            prn_outlet_recordtype = +$("#formPrivateData").attr("data-prn-outlet-recordtype");
            initAutoComplete();
        }

        continentChange();
        countryChange();
        stateChange();
        initTokenInputs();
        MediaPeople.InitJQueryValidate(form, save);

        /* cilcking on the show all should trigger the dropdowns */
        $(".btnShowAll").on("click", function (index, value) {
            $(this).parent().siblings(".searchInput").find("input:visible").focus().val(" ").trigger("keydown", { KeyCode: "32" });
        });
    }

    function save() {
        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnSave");
            },
            url: form.attr("action"),
            type: "POST",
            data: form.serialize(),
            success: function (data) {
                MediaPeople.DisplaySuccess('Private data saved succesfully', '');

                setTimeout(function () { location.href = $("#ReferrerUrl").val(); }, 700);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $('#vsPrivateData').removeClass("d-none").html('<span>' + (jqXHR.status == 400 ? errorThrown : MediaPeople.genericError) + '</span>');
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnSave");
            }
        });
    }

    function initTokenInputs() {
        $("#SubjectIdsCsv").tokenInput(function () {
            var groups = $.map($.grep($("#SubjectIdsCsv").val().split(','), function (value, index) {
                return value.match(MediaPeople.groupRegex);
            }), function (value, index) {
                return value.slice(1);
            }).join(',');

            var subjects = $.grep($("#SubjectIdsCsv").val().split(','), function (value, index) {
                return !value.match(/^g\d+$/gi);
            }).join(',');

            return "/Data/Subjects?selectedSubjects=" + subjects + "&selectedGroups=" + groups + "&group=" + $("#SubjectIdsCsv").data().groupFilterId;
        }, $.extend({}, {
            hintText: "Type in a \"Subject\"",
            placeholder: "Subject",
            showHeader: true,
            prePopulate: $.merge($.merge([], MediaPeople.IsValidDatasource(_subjects) ? _subjects : []), MediaPeople.IsValidDatasource(_subjectGroups) ? convertToValidGroups(_subjectGroups) : []),
            onReady: function () {
                $("#token-input-SubjectIdsCsv").on({
                    focus: function (e) {
                        MediaPeople.HideClosestShowingDropdown.call($(this));
                    }
                });
            },
            onDelete: function (item) {
                //if this item is a group -> find this group in the groups dropdown and add class addFilterGroup to the child span.
                if (item.Id.toString().match(MediaPeople.groupRegex)) {
                    $(this).parent().parent().find("div.dropdown-menu a[data-group-id=" + item.Id.slice(1) + "] span").addClass('addFilterGroup').removeClass('removeFilterGroup');
                }
            },
            headerFormatter: function () {
                return $("#SubjectIdsCsv").data().groupFilterId ? "<p><a title='Click to add this subject group to the selection criteria' onclick=MediaPeople.AddSubjectGroupToTokenInput('SubjectIdsCsv'); return false;>Filter: " + $("#SubjectIdsCsv").data().groupFilterName + "</a></p>" : "";
            },
            tokenFormatter: function (item) {
                return MediaPeople.GetSubjectsTokenInputResultRow(item);
            },
            onHide: function () {
                MediaPeople.ClearSubjectFilterById('SubjectIdsCsv');
                return true;
            }
        }, MediaPeople.commonTokenInputSettings)
        );


        /*Init Subjects groups dropdowns*/
        MediaPeople.InitFilterDropdowns();

        if ($("#WorkingLanguageIdsCsv").length > 0)
            $("#WorkingLanguageIdsCsv").tokenInput(MediaPeople.IsValidDatasource(_languages) ? _languages : [], $.extend({}, {
                hintText: "Type in a \"Language\"",
                placeholder: "Language",
                prePopulate: MediaPeople.IsValidDatasource(_selectedLanguages) ? _selectedLanguages : [],
                onDelete: function (item) {
                    $(this).valid();
                },
                onAdd: function (item) {
                    $(this).valid();
                },
            }, MediaPeople.commonTokenInputSettings));

        if ($("#SelectedRoles").length > 0)
            $("#SelectedRoles").tokenInput(MediaPeople.IsValidDatasource(_roles) ? _roles : [], $.extend({}, {
                hintText: "Type in a \"Role\"",
                placeholder: "Role",
                prePopulate: MediaPeople.IsValidDatasource(_selectedRoles) ? _selectedRoles : []
            }, MediaPeople.commonTokenInputSettings));

        //$("#WorkingLanguageIdsCsv").removeAttr("style").addClass("token-input-input");
    }

    function initAutoComplete() {
        var name = '';

        $('#OutletName')
            .keyup(function (v, a) {
                if (name != $("#OutletName").val())
                    $("#PrivateOutletId,#MediaOutletId,#PrnOutletId").val('');
            })
            .typeahead({
                minLength: 2,
                highlight: true,
                autoSelect: false,
                items: 100,
                source: function (query, process) {
                    return $.ajax({
                        url: "/Search/AutoSuggest",
                        dataType: "json",
                        data: {
                            term: query,
                            context: "Outlet",
                            country: 2
                        },
                        success: function (data) {
                            var result = [];
                            $.each(data, function (i, item) {
                                result.push(item.Name + "#" + item.Id + "#" + item.RecordType);
                            });

                            process(result);
                        }
                    });
                },
                matcher: function (item) {
                    return true;
                },
                highlighter: function (item) {

                    var parts = item.split('#');
                    var recordType = parseInt(parts[2], 10);
                    var data = (recordType >= oma_outlet_recordtype ? $("#prIcon").html() : "") + "  " + parts[0];
                    var regex = new RegExp('(' + this.query + ')', 'gi');
                    return data.replace(regex, "<strong>$1</strong>");
                },
                updater: function (item) {
                    var parts = item.split('#');
                    name = parts[0];
                    $(+parts[2] === oma_outlet_recordtype ? "#PrivateOutletId" : +parts[2] === prn_outlet_recordtype ? "#PrnOutletId" : "#MediaOutletId").val(parts[1]);
                    $("#vmOutlet").hide();
                    $("#OutletName").val(parts[0]).valid();
                    return parts[0];
                }
            });
    }

    return {
        InitGrid: initGrid,
        Init: initPage
    };
})();

$(document).ready(function () {
    $.validator.addMethod("outletvalidator", function (value, element, params) {
        if ($("#OutletName").val() != "" && $("#PrivateOutletId").val() == "" && $("#MediaOutletId").val() == "" && $("#PrnOutletId").val() == "") {
            $("#vmOutlet").show();
            return false;
        }
        $("#vmOutlet").hide();
        return true;
    });

    $.validator.unobtrusive.adapters.addBool("outletvalidator");
    //$.validator.addMethod("workinglanguageidscsvvalidator", function (value, element, params) {

    //    var isValid = $(element).tokenInput("get").length > 0;
    //    var ul = $("#token-input-" + element.id).closest("ul");
    //    ul.removeClass("input-validation-error valid");

    //    if (isValid)
    //        ul.addClass("valid");
    //    else
    //        ul.addClass("input-validation-error")

    //    return isValid;
    //});

    //$.validator.unobtrusive.adapters.addBool("workinglanguageidscsvvalidator");

    MediaPeople.PrivateOutlet.Init();
});