﻿/// <reference path="~/Scripts/Global"/>

MediaPeople.Contact = (function () {
    function getPeopleAlsoViewd() {
        var mainContainer = $("[data-main-container]");
        var url = mainContainer.attr("data-viewedlogs-url");
        if (mainContainer.length === 0 || url == null || url === "")
            return;

        $.ajax({
            url: url,
            type: "POST",
            data: {
                contactId: mainContainer.attr("data-contactId"),
                outletId: mainContainer.attr("data-outletId"),
                subjectCodeIds: mainContainer.attr("data-subjects")
            },
            success: function (data) {
                var selector = "#peopleAlsoViewedContainer";
                $(selector).removeClass("d-none").html(data);
                MediaPeople.InitBootstrapTooltips();
                MediaPeople.Socialmedia.GetAllTwitterImages(false, selector);
            }
        });
    }

    function getMediaMovements() {
        var list = $(".contactMediaMovements");
        var container = $(".contactMVContainer");

        $.ajax({
            url: list.attr("data-action"),
            type: "Get",
            success: function(data) {
                if (data.trim() !== "") {
                    list.html(data);
                    container.removeClass("d-none");
                } else {
                    container.addClass("d-none");
                }
            },
            error: function(jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            }
        });
    }

    function initControls() {
        getPeopleAlsoViewd();
        getMediaMovements();

        MediaPeople.Socialmedia.GetTwitterImage(true);

        $("a[data-addlist-url]").unbind("click").click(function () {
            MediaPeople.AddToList.openPopup(this);
            return false;
        });
        $("a[data-suggestion-url]").unbind("click").click(function () {
            MediaPeople.SuggestChange.openPopup(this);
            return false;
        });
        $("a[data-addnote-url]").unbind("click").click(function () {
            MediaPeople.OutletContact.openPopup(this);
            return false;
        });

        $("a[data-share-profile-url]").unbind("click").click(function () {
            MediaPeople.ShareProfile.openPopup(this);
            return false;
        });

        $("a[data-article]").unbind("click").click(function () {
            var url = $(this).data("article-action");

            $.ajax({
                url: url,
                cache: false,
                beforeSend: function () {
                    MediaPeople.Spinner.showSpinner("main");
                },
                success: function (data) {
                    MediaPeople.SetModalHtml(data);

                    MediaPeople.InitBootstrapTooltips();

                    MediaPeople.ShowModal(null, true);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
                },
                complete: function () {
                    MediaPeople.Spinner.hideSpinner("main");
                }
            });
            return false;
        });
    }

    return {
        Init: function () {
            initControls();

            // Resize all email addresses to fit
            $(".fitText").each(function (index) {
                MediaPeople.FitText($(this), $(this).parent(), 11);
            });
        }
    };
})();

$(document).ready(function () {
    MediaPeople.Contact.Init();
});