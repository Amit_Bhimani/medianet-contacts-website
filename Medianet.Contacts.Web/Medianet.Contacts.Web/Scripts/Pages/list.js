﻿/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js" />
/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Pages/common/grid.js"/>
/// <reference path="~/Scripts/Pages/common/export.js" />
MediaPeople.List = (function () {
    // MediaPeople.Grid.Rebind calls this to fetch custom fields to post to the rebind Url
    function fetchParams() {
        return {
            ListId: $("#hdnListId").val(),
            ListSet: $("#hdnListSetId").val()
        };
    }

    function promptToDelete() {
        var obj = $(".deleteRecords");

        MediaPeople.ShowConfirm(obj);
    }

    function rebindCallback(isUpdate) {
        if (isUpdate) {
            rebindListVersions();
            rebindLivingList();
        }
    }

    function rebindListVersions() {
        var params = {
            listId: $("#hdnListId").val(),
            listSetId: $("#hdnListSetId").val()
        };

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("historyContent");
            },
            url: "/List/RebindListVersions",
            type: "POST",
            data: params,
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            },
            success: function (data) {
                var newListVersionsHtml = $(data).siblings("#sectionListVersions");
                if (newListVersionsHtml.length === 0)
                    newListVersionsHtml = $(data);
                $("#sectionListVersions").replaceWith(newListVersionsHtml);

                MediaPeople.List.InitListHistory();
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("historyContent");
            }
        });
    }

    function rebindLivingList() {
        var params = {
            listId: $("#hdnListId").val(),
            listSetId: $("#hdnListSetId").val()
        };

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("livingContent");
            },
            url: "/List/RebindLivingList",
            type: "POST",
            data: params,
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            success: function (data) {
                var newListVersionsHtml = $(data).siblings("#sectionLivingList");
                if (newListVersionsHtml.length === 0)
                    newListVersionsHtml = $(data);
                $("#sectionLivingList").replaceWith(newListVersionsHtml);

                MediaPeople.List.InitLivingLists();
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("livingContent");
            }
        });
    }

    function initChaseList() {
        $(".showChaseList").on("click", function (event) {
            event.preventDefault();

            $.ajax({
                url: "/List/ChaseList",
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    processChaseList();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError("", jqXhr.status);
                }
            });
        });
    }

    function processChaseList() {
        $("#export-button").on("click", function () {
            MediaPeople.HideModal();
            MediaPeople.DisplaySuccess("Chase list report should be downloaded shortly");
            reportExport("iframeChaseListExport", "formChaseListExport", "0");
        });
    }

    function initMailingLabels() {
        $(".showMailingLabels").on("click", function (event) {
            event.preventDefault();

            $.ajax({
                url: "/List/MailingLabels",
                beforeSend: function () {
                    MediaPeople.Spinner.showSpinner("btnMailingList");
                },
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    processMailingLabels();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError("", jqXhr.status);
                },
                complete: function () {
                    MediaPeople.Spinner.hideSpinner("btnMailingList");
                }
            });
        });
    }

    function processMailingLabels() {
        $("#export-button").on("click", function () {
            MediaPeople.HideModal();
            MediaPeople.DisplaySuccess("Mailing label report should be downloaded shortly");
            reportExport("iframeMailingLabels", "formMailingLabels", $("input[name='reportType']:checked").val());
        });
    }

    function reportExport(pFrameId, pFormId, reportType) {
        var iFrame = $("#" + pFrameId);
        var iFrameDoc = iFrame[0].contentDocument || iFrame[0].contentWindow.document;
        var mySelection = MediaPeople.Grid.Selection();
        iFrameDoc.write("<form id='" + pFormId + "' action='/list/TelerikListReport' method='post' >\n" +
            "<input type='hidden' id='listId' name='listId' value='" + $("#hdnListId").val() + "' />\n" +
            "<input type='hidden' id='listSetId' name='listSetId' value='" + $("#hdnListSetId").val() + "' />\n" +
            "<input type='hidden' id='exportType' name='exportType' value='" + $("#fileFormat").val() + "' />\n" +
            "<input type='hidden' id='reportType' name='reportType' value='" + reportType + "' />\n" +
            "<input type='hidden' id='checkboxState' name='checkboxState' value='" + mySelection.CheckboxState + "' />\n" +
            "<input type='hidden' id='checkedItems' name='checkedItems' value='" + mySelection.CheckedItems + "' />\n" +
            "<input type='hidden' id='uncheckedItems' name='uncheckedItems' value='" + mySelection.UncheckedItems + "' />\n" +
            "</form>\n");
        iFrameDoc.close();
        iFrame.contents().find("#" + pFormId).submit();
    }

    function initDuplicateList() {
        $(".showDuplicateList").on("click", function (event) {
            event.preventDefault();

            $.ajax({
                url: "/List/Duplicate?id=" + $("#hdnListId").val(),
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    processDuplicateLists();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError("", jqXhr.status);
                },
            });
        });
    }

    function processDuplicateLists() {
        processGroupState();
        MediaPeople.InitJQueryValidate($("#duplicateForm"), submitDuplicate);
        MediaPeople.InitBootstrapTooltips();
    }

    function submitDuplicate() {
        $("#vsDuplicateList").addClass("d-none");
        var form = $("#duplicateForm");

        $.ajax({
            url: "/List/Duplicate",
            type: "POST",
            data: form.serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnDuplicateList");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401) {
                    MediaPeople.RedirectToLoginPage();
                }
                else if (jqXHR.status === 400) {
                    MediaPeople.SetModalHtml(jqXHR.responseText);
                    processDuplicateLists();
                } else {
                    $("#vsDuplicateList").removeClass("d-none").html("<span>" + MediaPeople.genericError + "</span>");
                }
            },
            success: function (data) {
                var newId = $(data).find("#hdnNewListId").val();
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess("Duplicate list created succesfully. Redirecting...", "");
                setTimeout(function () {
                    window.location = "/List/Index/" + newId;
                }, 3000);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnDuplicateList");
            }
        });
        return false;
    }

    function processGroupState() {
        $("#NewGroup").on("click", function () {
            changeGroupState();
        });

        $("#ExistingGroup").on("click", function () {
            changeGroupState();
        });

        changeGroupState();
    }

    function changeGroupState() {
        var newGroup = $("#NewGroup");
        var groupName = $("#groupName");

        if (newGroup.is(":checked")) {
            groupName.show().attr("required", "required");
            groupName.parent().removeClass("select-container");
            $("#groupId").hide();
            $("span[data-valmsg-for='Group.Name']").show();
        } else {
            groupName.hide().removeAttr("required");
            groupName.parent().addClass("select-container");
            $("#groupId").show();
            $("span[data-valmsg-for='Group.Name']").hide();
        }
    }

    function initMergeLists() {
        $(".showMergeLists").on("click", function (event) {
            event.preventDefault();

            $.ajax({
                url: "/List/Merge?id=" + $("#hdnListId").val(),
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    processMergeLists();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError("", jqXhr.status);
                },
            });
        });
    }

    function processMergeLists() {
        processGroupState();
        MediaPeople.InitJQueryValidate($("#mergeForm"), submitMerge);
        MediaPeople.InitBootstrapTooltips();
    }

    function submitMerge() {
        $("#vsMergeList").addClass("d-none");

        $.ajax({
            url: "/List/Merge",
            type: "POST",
            data: $("#mergeForm").serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnMergeList");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXhr.status == 401){
                    MediaPeople.RedirectToLoginPage();
                }
                else if (jqXHR.status === 400) {
                    MediaPeople.ShowModal(jqXHR.responseText);
                    processMergeLists();
                } else {
                    $("#vsDuplicateList").removeClass("d-none").html("<span>" + MediaPeople.genericError + "</span>");
                }
            },
            success: function (data) {
                var newId = $(data).find("#hdnNewListId").val();
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess("Merged list created succesfully. Redirecting...", "");
                setTimeout(function () {
                    window.location = "/List/Index/" + newId;
                }, 3000);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnMergeList");
            }
        });

        return false;
    }

    function initAddNote() {
        $(".showAddNote").on("click", function (event) {
            event.preventDefault();

            $.ajax({
                url: "/Note/CreateBulk",
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    processAddNote();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError("", jqXhr.status);
                }
            });
        });
    }

    function processAddNote() {
        MediaPeople.InitCheckboxes("#Modal");
        MediaPeople.InitBootstrapTooltips();
        MediaPeople.InitJQueryValidate($("#frmAddNote"), submitAddNote);
    }

    function submitAddNote() {
        var mySelection = MediaPeople.Grid.Selection();
        var form = $("#frmAddNote");

        $("#ListId").val($("#hdnListId").val());
        $("#ListSetId").val($("#hdnListSetId").val());
        $("#CheckboxState").val(mySelection.CheckboxState);
        $("#UncheckedItems").val(mySelection.UncheckedItems);
        $("#CheckedItems").val(mySelection.CheckedItems);

        $("#vsAddNote").addClass("d-none");

        $.ajax({
            url: "/Note/CreateBulk",
            type: "POST",
            data: form.serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnAddNote");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $("#vsAddNote").removeClass("d-none").html("<span>" + (jqXHR.status === 400 ? errorThrown : MediaPeople.genericError) + "</span>");
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess("Note created succesfully", "");
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnAddNote");
            }
        });
        return false;
    }

    function listRejectReplacement(recordId) {
        var params = {
            RecordId: recordId,
            ListId: $("#hdnListId").val(),
            ListSet: $("#hdnListSetId").val()
        };

        //@ make an ajax call that will reject the replacement and will reload Living List Updates section and also Grid below.
        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("historyContent");
            },
            url: "/List/RejectReplacement",
            type: "POST",
            data: params,
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            },
            success: function (data) {
                MediaPeople.DisplaySuccess("Record removed from list");
                // Just refresh the page, this may be ajaxified later so we only refresh grid content and living list updates sections.
                //location.reload();
                MediaPeople.Grid.Rebind(true);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("historyContent");
            }
        });
    }

    function listAcceptReplacement(recordId) {
        var params = {
            RecordId: recordId,
            ListId: $("#hdnListId").val(),
            ListSet: $("#hdnListSetId").val()
        };

        //@ make an ajax call that will reject the replacement and will reload Living List Updates section and also Grid below.
        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("historyContent");
            },
            url: "/List/AcceptReplacement",
            type: "POST",
            data: params,
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            },
            success: function (data) {
                MediaPeople.DisplaySuccess("Record replaced");
                // Just refresh the page, this may be ajaxified later so we only refresh grid content and living list updates sections.
                //location.reload();
                MediaPeople.Grid.Rebind(true);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("historyContent");
            }
        });
    }

    function initChooseAnother() {
        $(".chooseAnother").on("click", function (event) {
            var recordId = $(this).closest("tr").data("record-id");

            event.preventDefault();

            $("#hdnRecordId").val(recordId);

            $.ajax({
                url: "/List/ChooseAnotherContact?id=" + recordId,
                type: "GET",
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    processChooseAnother();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError("", jqXhr.status);
                },
            });
        });
    }

    function processChooseAnother() {
        $("#choose-button").on("click", function () {
            var params = {
                RecordId: $("#hdnRecordId").val(),
                ContactId: $("#ContactId").val().trim()
            };

            $.ajax({
                url: "/List/ChooseAnotherContact",
                type: "POST",
                data: params,
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 401) {
                        MediaPeople.RedirectToLoginPage();
                    }
                    else if (jqXHR.status === 400) {
                        MediaPeople.SetModalHtml(jqXHR.responseText);
                        processChooseAnother();
                    } else {
                        $("#vsDuplicateList").removeClass("d-none").html("<span>" + MediaPeople.genericError + "</span>");
                    }
                },
                success: function () {
                    MediaPeople.HideModal();
                    MediaPeople.DisplaySuccess("Replacement contact chosen", "");
                    MediaPeople.Grid.Rebind(true);
                }
            });

            return false;
        });
    }

    function initEditList() {
        $("button[data-edit-list]").on("click", function (event) {
            event.preventDefault();

            $.ajax({
                url: $(this).attr("data-edit-list-action"),
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    processEdit();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
                }
            });
        });
    }

    function processEdit() {
        MediaPeople.InitCheckboxes("#Modal");
        MediaPeople.InitBootstrapTooltips();
        MediaPeople.InitJQueryValidate($("#frmEdit"), submitEditList);
    }

    function submitEditList() {
        var form = $("#frmEdit");

        $("#vsEdit").addClass("d-none");

        $.ajax({
            url: form.attr("action"),
            type: "POST",
            data: form.serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnSave");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $("#vsEdit").removeClass("d-none").html("<span>" + (jqXHR.status === 400 ? errorThrown : MediaPeople.genericError) + "</span>");
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess("List edited succesfully", "");
                location.href = data.Url;
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnSave");
            }
        });
        return false;
    }

    function acceptAll() {
        var params = {
            listId: $("#hdnListId").val(),
            listSetId: $("#hdnListSetId").val()
        };

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("livingContent");
            },
            url: "/List/AcceptAll",
            type: "POST",
            data: params,
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            },
            success: function (data) {
                MediaPeople.Grid.Rebind(true);
                MediaPeople.DisplaySuccess("Records replaced");
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("livingContent");
            }
        });
    }

    return {
        InitGrid: function () {
            if ($(".resultSection").length > 0) {
                var options = {
                    singleDeleteDataIdName: "record-id",
                    rebindDataMethod: fetchParams,
                    rebindSuccess: rebindCallback,
                    rebindUrl: "/List/Rebind",
                    deleteDataMethod: fetchParams,
                    deleteSuccess: null,
                    deleteUrl: "/List/Delete",
                    deletePromptMethod: promptToDelete
                };

                MediaPeople.Grid.Init(options, false);
            }
        },
        InitPopups: function () {
            initChaseList();
            initMailingLabels();
            initDuplicateList();
            initMergeLists();
            initAddNote();
            initEditList();
            MediaPeople.Export.Init(0);
        },
        InitLivingLists: function () {
            // Configure confirm replacement
            $(".confirmReplacement").attr("data-confirm-description", "This accepts the automatic replacement as a permanent change in the list.<br />" +
                "Selecting this will remove the contact from the 'Living List' window, as they are now a permanent part of the list.");

            $(".confirmReplacement").attr("data-confirm-click", "MediaPeople.List.AcceptReplacement(obj)");

            // Configure confirm removal
            $(".confirmRemoval").attr("data-confirm-description", "This rejects the automatic replacement.<br />" +
                "The automatic replacement should be deleted from the list.<br />" +
                "Selecting this will remove the contact from the 'Living List' window, as the replacement has been rejected.");

            $(".confirmRemoval").attr("data-confirm-click", "MediaPeople.List.RejectReplacement(obj)");

            // Configure choose another
            initChooseAnother();

            // Configure Accept All
            $(".confirmAcceptAll").attr("data-confirm-description", "You are about to Accept all Living list updates for this list. <br />" +
               "All suggested replacements will now be incorporated into your Saved list.");

            $(".confirmAcceptAll").attr("data-confirm-click", "MediaPeople.List.AcceptAll(obj)");
        },
        InitListHistory: function () {
            $(".listsetRestore").attr("data-confirm-click", "MediaPeople.List.ListSetRestore(obj)");
        },
        InitDialogs: function () {
            /*$("span.livingListAction.chooseAnother").click(function () {
                $("#hdnRecordId").val(getListMasterRecordId(this));
                window.MediaPeople.ListPopups.LivingListChooseAnotherContact();
            });*/
        },
        Init: function () {
            MediaPeople.List.InitGrid();
            MediaPeople.List.InitPopups();
            MediaPeople.List.InitLivingLists();
            MediaPeople.List.InitListHistory();
        },
        ListSetRestore: function (button) {
            var listId = button.closest("tr").data("id");
            var listsetId = button.closest("tr").data("listset-id");

            if (!isNaN(listId) && !isNaN(listsetId))
                window.location = "/List/Restore/" + listId + "/" + listsetId;
        },
        PerformMultiDeleting: function () {
            if (MediaPeople.Grid.ItemsAreSelected()) {
                MediaPeople.Grid.PerformMultiDeleting();
            } else {
                MediaPeople.DisplayError("Please select at least one item.");
            }
        },
        AcceptAll: function (button) {
            acceptAll(button);
        },
        AcceptReplacement: function (button) {
            listAcceptReplacement(button.closest("tr").data("record-id"));
        },
        RejectReplacement: function (button) {
            listRejectReplacement(button.closest("tr").data("record-id"));
        }
    };
})();

$(document).ready(function () {
    MediaPeople.List.Init();
});
