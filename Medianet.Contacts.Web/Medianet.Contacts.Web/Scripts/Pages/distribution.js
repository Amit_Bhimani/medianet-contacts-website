﻿/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js" />
/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Plugins/moment.js"/>
/// <reference path="~/Scripts/Plugins/daterangepicker.js" />

MediaPeople.Distribution = (function () {
    function setHoldState() {
        var onHold = $("input[name=OnHold]:checked").val();

        if (onHold === "true") {
            $(".holdCalendar").show();
        } else {
            $(".holdCalendar").hide();
        }
    }
    function SetWireDistVisibility() {
        if ($("#DistributionWire").is(':checked'))
            $(".wireDist").show();
        else
            $(".wireDist").hide();
    }

    return {
        Init: function() {
            MediaPeople.Distribution.InitLists();
            MediaPeople.Distribution.InitHoldDate();
            MediaPeople.Distribution.InitWireDistribution();
            MediaPeople.Distribution.InitReleaseCreated();
            MediaPeople.InitFileInput();

            /* cilcking on the show all should trigger the dropdowns */
            $(".btnShowAll").on('click', function(index, value) {
                $(this).parent().siblings('.searchInput').find("input:visible").focus().val(' ').trigger("keydown", { KeyCode: "32" });
            });
        },
        InitLists: function() {
            $("#SelectedLists").tokenInput(MediaPeople.IsValidDatasource(_lists) ? _lists : [], $.extend({}, {
                hintText: "Type in a \"List\"",
                placeholder: "Distribution lists",
                prePopulate: MediaPeople.IsValidDatasource(_selectedLists) ? _selectedLists : []
            }, MediaPeople.commonTokenInputSettings));
        },
        InitHoldDate: function () {
            var todaysDate = moment().format("DD/MM/YYYY");
            var holdTime = $('#HoldTime');
            var holdValue = holdTime.val();

            if (holdValue === "") {
                holdValue = todaysDate;
            }

            holdTime.daterangepicker({
                "singleDatePicker": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                    "format": "DD/MM/YYYY HH:mm",
                    "separator": " - ",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",
                    "firstDay": 1
                },
                "startDate": holdValue,
                "minDate": todaysDate,
                "maxDate": moment().add(365, 'days').format("DD/MM/YYYY"),
                "opens": "right",
                "applyClass": "btn-primary"
            }, function (start, end, label) {
                console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
            });

            $("input[name=OnHold]").click(function() {
                setHoldState();
            });

            $(".calendarButton").on("click", function() {
                holdTime.click();
            });

            setHoldState();
        },
        InitWireDistribution: function () {
            SetWireDistVisibility();

            $("#DistributionWire").on("click", function() {
                SetWireDistVisibility();
            });
        },
        InitReleaseCreated: function() {
            var releaseId = $("#ReleaseId").val();

            if (typeof releaseId != "undefined" && releaseId !== "" && releaseId !== "0") {
                MediaPeople.DisplaySuccess("Media Release created with Id " + releaseId, "")
            }
        }
    };
})();

$(document).ready(function () {
    MediaPeople.Distribution.Init();
});
