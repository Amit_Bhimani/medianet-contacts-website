﻿/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js" />
/// <reference path="~/Scripts/Global/common.js"/>

MediaPeople.AgreeTerms = (function () {
    return {
        init: function () {
            /*$("#accept-terms").on("click", function () {
                var checkbox = $("#acceptTerms");

                if (checkbox.length > 0 && checkbox.is(":checked")) {
                    $("#termsForm").submit();
                } else {
                    MediaPeople.DisplayError("Please accept the terms before continuing.");
                }
            });*/

            $("#reject-terms").on("click", function () {
                window.location.href = "/Account/LogOff";
                //$("#termsForm").attr("action", "/Account/LogOff").submit();
            });
        }
    };
})();

$(document).ready(function () {
    MediaPeople.AgreeTerms.init();
});
