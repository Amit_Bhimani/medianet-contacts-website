﻿/// <reference path="~/Scripts/Global"/>

MediaPeople.Outlet = (function () {

    function getMediaMovements() {
        var list = $(".outletMediaMovements");
        var container = $(".outletMVContainer");

        $.ajax({
            url: list.attr("data-action"),
            type: "Get",
            success: function (data) {
                if (data.trim() !== "") {
                    list.html(data);
                    container.removeClass("d-none");
                } else {
                    container.addClass("d-none");
                }

                MediaPeople.Socialmedia.GetAllTwitterImages();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            }
        });
    }

    function initControls() {
        getMediaMovements();

        $("a[data-addlist-url]").unbind("click").click(function () {
            MediaPeople.AddToList.openPopup(this);
        });
        $("a[data-suggestion-url]").unbind("click").click(function () {
            MediaPeople.SuggestChange.openPopup(this);
            return false;
        });

        $("a[data-addnote-url]").unbind("click").click(function () {
            MediaPeople.OutletContact.openPopup(this);
            return false;
        });

        $("a[data-share-profile-url]").unbind("click").click(function () {
            MediaPeople.ShareProfile.openPopup(this);
            return false;
        });
    }

    return {
        Init: function () {
            initControls();

            // Resize all email addresses to fit
            $(".fitText").each(function (index) {
                MediaPeople.FitText($(this), $(this).parent(), 11);
            });
        }
    };
})();

$(document).ready(function () {
    MediaPeople.Outlet.Init();
});