﻿/// <reference path="../../lodash.js" />
/// <reference path="../../jquery-3.0.0-vsdoc.js" />

MediaPeople.Socialmedia = (function () {
    function getUserTwitterInfo(container, url) {
        $.ajax({
            url: (url + "?twitterHandle=" + container.attr("data-twitterhandle")),
            type: "GET",
            success: function (data) {
                container.html(data);
                MediaPeople.InitBootstrapTooltips();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    MediaPeople.Spinner.removeInlineSpinner(container);
            }
        });
    }

    function getAllTwitterImages(isLarge, specificSelector) {
        //find all twitter image elements in the selector
        var selectorTwitterImageElms = $((specificSelector != null ? specificSelector + " " : "") + "img[data-twitterhandle]");
        var handleArray = [];

        if (selectorTwitterImageElms != undefined && selectorTwitterImageElms.length > 0) {

            $.each(selectorTwitterImageElms, function () {
                var handle = $(this).attr("data-twitterhandle").replace("@","");
                if ($.inArray(handle, handleArray) === -1) handleArray.push(handle);
            });
        }

        if (handleArray != undefined && handleArray.length > 0) {
            var handles = handleArray.join(",");

            $.ajax({
                url: "/Social/MultiUsersTwitterImages",
                type: "POST",
                data: {twitterHandles: encodeURIComponent(handles)},
                success: function (data) {

                    if (data != undefined && data.length > 0) {

                        $.each(selectorTwitterImageElms, function() {
                            var imageElm = $(this);
                            var imageHandle = imageElm.attr("data-twitterhandle").replace("@", "");

                            var userTwitterDetails = data.find(t => t.Handle.toLowerCase() === imageHandle.toLowerCase());

                            if (userTwitterDetails != undefined && userTwitterDetails.ProfileImage != null && userTwitterDetails.DefaultProfileImage == false)
                                imageElm.prop("src", isLarge ? userTwitterDetails.ProfileImage.replace("_normal", "_400x400") : userTwitterDetails.ProfileImage);
                        });
                    }
                }
            });
        }
    }

    function getTwitterImage(isLarge, specificSelector) {

        var twitterImgs = $((specificSelector != null ? specificSelector + " " : "") + "img[data-twitterhandle]");

        $.each(twitterImgs, function() {
            var container = $(this);
            var imageHandle = container.attr("data-twitterhandle").replace("@", "");

            $.ajax({
                url: ($("[data-twitterimage-url]").attr("data-twitterimage-url") + "?twitterHandle=" + encodeURIComponent(imageHandle)),
                type: "GET",
                currentImage: this,
                success: function(data) {
                    if (data.ProfileImage != null && data.DefaultProfileImage == false)
                        $(this.currentImage).prop("src", isLarge ? data.ProfileImage.replace("_normal", "_400x400") : data.ProfileImage);
                }
            });
        });
    }

    function getYouTube(container) {
        $.ajax({
            url: ($("[data-social]").attr("data-youtube") + "?url=" + container.attr("data-youtube")),
            type: "GET",
            success: function (data) {
                container.html(data);
            }
        });
    }

    return {
        GetTwitterImage: getTwitterImage,
        GetAllTwitterImages: getAllTwitterImages,
        init: function () {
            var twitter = $("li[data-twitterhandle]");
            if (twitter.length > 0)
                getUserTwitterInfo(twitter, $("[data-social]").attr("data-twitter-info"));

            var youtube = $("li[data-youtube]");
            if (youtube.length > 0)
                getYouTube(youtube);

            var tweets = $("[data-tweets]");
            if (tweets.length > 0)
                getUserTwitterInfo(tweets, tweets.attr("data-tweets-url"));
        }
    };
})();

$(document).ready(function () {
    MediaPeople.Socialmedia.init();
})
