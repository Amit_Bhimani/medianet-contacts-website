﻿/// <reference path="../../lodash.js" />
/// <reference path="../../jquery-3.0.0-vsdoc.js" />

MediaPeople.UploadDocuments = (function () {
    var mainContainer = $("div[data-main-container]");
    var grdDocuments = $("#divDocuments");

    function reload() {
        $.ajax({
            url: grdDocuments.attr("data-action"),
            cache: false,
            data: { ContactId: mainContainer.attr("data-contactId"), OutletId: mainContainer.attr("data-outletId"), RecordType: mainContainer.attr("data-recordType") },
            type: "GET",
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("divDocuments");
            },
            success: function (data) {
                $("#tblDocuments").html(data);
                initGrid();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("divDocuments");
            }
        });
    }

    function deleteDocument(obj) {
        $.ajax({
            url: obj.attr("data-action"),
            type: "DELETE",
            cache: false,
            data: { id: obj.attr("data-id") },
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("divDocuments");
            },
            success: function (data) {
                MediaPeople.DisplaySuccess('document deleted succesfully', '');
                reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("divDocuments");
            }
        });
        return false;
    }

    function openPopupHandler(obj, id) {
        obj = $(obj);

        $.ajax({
            url: obj.attr("data-upload-url"),
            data: { DocumentId: id, ContactId: mainContainer.attr("data-contactId"), OutletId: mainContainer.attr("data-outletId"), RecordType: mainContainer.attr("data-recordType") },
            success: function (data) {
                MediaPeople.ShowModal(data);

                MediaPeople.InitBootstrapTooltips();

                MediaPeople.InitJQueryValidate($("#UploadForm"), btnSaveDocument_Click);
                
                MediaPeople.InitFileInput();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            }
        });
    }

    function btnSaveDocument_Click() {
        $.ajax({
            type: "POST",
            url: $("#UploadForm").attr("data-action"),
            data: new FormData($("#UploadForm").get(0)),
            dataType: "json",
            contentType: false,
            processData: false,
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnSaveDocument");
            },
            success: function (data) {
                if (data.Success == true) {
                    MediaPeople.HideModal();
                    MediaPeople.DisplaySuccess('document uploaded succesfully', '');
                    MediaPeople.UploadDocuments.reload();
                }
                else {
                    $('#vsUpload').removeClass("d-none").html('<span>' + (data.status == 400 ? MediaPeople.genericError : data.ErrorMessage) + '</span>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnSaveDocument");
            }
        });
        return false;
    }

    function initGrid() {
        $("#tblDocuments [data-confirm]").attr("data-confirm-click", "MediaPeople.UploadDocuments.deleteDocument(obj)");
    }

    function init() {
        $("#btnUpload").click(function () {
            openPopupHandler(this);
            return false;
        });

        $(grdDocuments).on('click', '[data-document-edit]', function () {
            openPopupHandler(this, parseInt($(this).attr("data-id")));
            return false;
        });

        initGrid();
    }

    return {
        init: init,
        deleteDocument: deleteDocument,
        reload: reload
    };

})();

$(document).ready(function () {
    $.validator.addMethod("documentname", function (value, element, params) {
        return $("#UploadForm #Name").val().length > 0;
    });
    $.validator.unobtrusive.adapters.addBool("documentname");

    $.validator.unobtrusive.adapters.addSingleVal("requiredFile", "size");
    $.validator.unobtrusive.adapters.addSingleVal("maximumFileSizeValidator", "size");
    $.validator.unobtrusive.adapters.addSingleVal("validFileTypeValidator", "filetypes");

    $.validator.addMethod('requiredFile', function (value, element, minSize) {
        return $(element).val().length > 0;
    });

    $.validator.addMethod('maximumFileSizeValidator', function (value, element, maxSize) {
        return convertBytesToMegabytes(element.files[0].size) <= parseFloat(maxSize);
    });

    $.validator.addMethod('validFileTypeValidator', function (value, element, validFileTypes) {
        if (validFileTypes.indexOf(',') > -1) {
            validFileTypes = validFileTypes.split(',');
        } else {
            validFileTypes = [validFileTypes];
        }

        var fileType = value.split('.')[value.split('.').length - 1];

        for (var i = 0; i < validFileTypes.length; i++) {
            if (validFileTypes[i] === fileType) {
                return true;
            }
        }

        return false;
    });

    $.validator.unobtrusive.adapters.add('fileuploadvalidator', ['clientvalidationmethods', 'parameters', 'errormessages'], function (options) {
        options.rules['fileuploadvalidator'] = {
            clientvalidationmethods: options.params['clientvalidationmethods'].split(','),
            parameters: options.params['parameters'].split('|'),
            errormessages: options.params['errormessages'].split(',')
        };
    });

    $.validator.addMethod("fileuploadvalidator", function (value, element, param) {
        //array of jquery validation rule names 
        var validationrules = param["clientvalidationmethods"];

        //array of paramteres required by rules, in this case regex patterns 
        var patterns = param["parameters"];

        //array of error messages for each rule 
        var rulesErrormessages = param["errormessages"];

        var validNameErrorMessage = new Array();
        var index = 0

        for (i = 0; i < patterns.length; i++) {
            var rule = $.validator.methods[validationrules[i].trim()];

            //execute the rule 
            var isValid = rule.call(this, value, element,  patterns[i].trim());

            if (!isValid) {
                $.validator.messages.fileuploadvalidator = rulesErrormessages[i];
                return false;
            }
        }
        return true;
    });

    function convertBytesToMegabytes(bytes) {
        return (bytes / 1024) / 1024;
    }

    MediaPeople.UploadDocuments.init();
})