﻿/// <reference path="~/Scripts/Global/common.js"/>

MediaPeople.OutletContact = (function () {
    var mainContainer = $("div[data-main-container]");
    function GetNotes() {
        var params = {
            outletId: mainContainer.attr("data-outletId"),
            recordType: mainContainer.attr("data-recordType")
        };

        if (mainContainer.attr("data-contactId") !== undefined) {
            params.contactId = mainContainer.attr("data-contactId");
        }

        GetPinnedNotes(params);
        GetUnPinnedNotes(params);
    };

    function GetPinnedNotes(params) {
        $.ajax({
            url: ($("div[data-container='pinnedNotes']").attr("data-action")),
            data: params,
            cache: false,
            type: "GET",
            success: function (data) {
                $("div[data-container='pinnedNotes']").html(data);
                OnSuccess();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            }
        });
    }

    function GetUnPinnedNotes(params) {
        $.ajax({
            url: ($("div[data-container='unPinnedNotes']").attr("data-action")),
            data: params,
            cache: false,
            type: "GET",
            success: function (data) {
                $("div[data-container='unPinnedNotes']").html(data);
                OnSuccess();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            }
        });
    }

    function DeleteNote(obj) {

        var id = $(obj).data("id");
        var url = $(obj).attr("data-action");

        $.ajax({
            url: url,
            data: {
                id: id,
            },
            type: "POST",
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("divAllNotes");
            },
            success: function (data) {
                MediaPeople.DisplaySuccess('Note deleted succesfully', '');
                OnSuccess();
                GetNotes();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("divAllNotes");
            }
        });
    };

    function PinUnpinNote(noteId, pinned, url) {
        $.ajax({
            url: url,
            data: {
                id: noteId,
                pinned: pinned
            },
            type: "POST",
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("divAllNotes");
            },
            success: function (data) {
                OnSuccess();
                GetNotes();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("divAllNotes");
            }
        });
    };

    function NotesListsActions() {
        $('[data-container="sticky-notes-list"],[data-container="notes-list"]').find("[data-pinned]")
            .click(function () {
                var id = $(this).data("id");
                var pinned = $(this).data("pinned");
                var url = $(this).attr("data-action");
                //pin/unpin note
                if (id != null || id != undefined || id != "") {
                    PinUnpinNote(id, pinned, url);
                }
            })

        $('[data-container="sticky-notes-list"],[data-container="notes-list"]').find("[data-edit]")
            .click(function () {
                var id = $(this).data("id");
                var url = $(this).attr("data-action");
                //edit note
                if (id != null || id != undefined || id != "") {
                    openPopupHandler($(this));
                }
                return false;
            })

        $("[data-container='pinnedNotes'],[data-container='unPinnedNotes']").find("[data-confirm]").attr("data-confirm-click", "MediaPeople.OutletContact.DeleteNote(obj)");

        // Pinned Next / Prev paging.
        $("[data-pinned-pagination] .nextPage,[data-pinned-pagination] .prevPage").on("click", function () {
            var pageNo = $(this).data("page");

            if (!isNaN(pageNo)) {
                $("[data-pinned-pagination]").find(".currentPage").text(pageNo);
                Rebind(true);
            }
        });

        // UnPinned Next / Prev paging.
        $("[data-unpinned-pagination] .nextPage,[data-unpinned-pagination] .prevPage").on("click", function () {
            var pageNo = $(this).data("page");

            if (!isNaN(pageNo)) {
                $("[data-unpinned-pagination]").find(".currentPage").text(pageNo);
                Rebind(false);
            }
        });
    }

    function OnSuccess() {
        NotesListsActions();
    }

    function btnModifyNote_Click() {
        $('#vsAddNote').addClass("d-none");
        var form = $('#frmAddNote');
        var message = $("#Id").val() == 0 ? 'Note created succesfully' : 'Note edited succesfully';

        $.ajax({
            url: (form.attr("data-action")),
            type: "POST",
            data: form.serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnAddNote");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXhr.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $('#vsAddNote').removeClass("d-none").html('<span>' + (jqXHR.status == 400 ? errorThrown : MediaPeople.genericError) + '</span>');
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess(message, '');
                GetNotes();
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnAddNote");
            }
        });
        return false;
    }

    function openPopupHandler(obj) {
        var url;
        var data = {};

        data.recordType = mainContainer.attr("data-recordType");

        var edit = $(obj).attr("data-id");
        if (edit !== undefined) {
            data.id = $(edit).val();
            url = $(obj).attr("data-action");
        } else {
            data.outletId = mainContainer.attr("data-outletId");

            if (mainContainer.attr("data-contactId") !== undefined) {
                data.contactId = mainContainer.attr("data-contactId")
            } else data.contactId = "";

            url = $(obj).attr("data-addnote-url")
        };

        $.ajax({
            url: url,
            data: data,
            cache: false,
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("main");
            },
            success: function (data) {
                MediaPeople.SetModalHtml(data);

                MediaPeople.InitBootstrapTooltips();

                MediaPeople.InitJQueryValidate($("#frmAddNote"), btnModifyNote_Click);

                MediaPeople.InitCheckboxes();
                MediaPeople.ShowModal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("main");
            }
        });
    }

    function Rebind(pinned) {

        var paginationContainer = "[data-pinned-pagination]";
        var container = "[data-container='pinnedNotes']";

        if (!pinned) {
            paginationContainer = "[data-unpinned-pagination]"
            container = "[data-container='unPinnedNotes']";
        }

        var params = {
            Page: $(paginationContainer).find(".currentPage").text(),
            PageSize: $(paginationContainer).find("#PageSize").val(),
            ResultCount: $(paginationContainer).find("#ResultCount").val(),
            CurrentPageOnly: $(paginationContainer).find("#CurrentPageOnly").val(),
            outletId: mainContainer.attr("data-outletId"),
            recordType: mainContainer.attr("data-recordType"),
            pinned: pinned
        };
       
        if (mainContainer.attr("data-contactId") !== undefined) {
            params.contactId = mainContainer.attr("data-contactId");
        }

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("divAllNotes");
            },
            url: "/Note/Rebind",
            type: "Get",
            data: params,
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            success: function (data) {

                // Get new pagination state and the new result rows.
                var newPagination = $(data).find(paginationContainer).find("#result-navigation");
                var newRows = $(data).find("table").find("tbody");

                // Update pagination and grid.
                $(paginationContainer).find("#result-navigation").replaceWith(newPagination);

                if (newRows.children().length > 0) {
                    $(container).find("table.resultTableHeader tbody").replaceWith(newRows);
                }

                OnSuccess();
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("divAllNotes");
            }
        });
    }

    return {
        InitNotes: function () {
            GetNotes();
        },
        openPopup: openPopupHandler,
        DeleteNote: DeleteNote
    };
})();

$(document).ready(function () {
    MediaPeople.OutletContact.InitNotes();
});