﻿/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Pages/common/grid.js" />

MediaPeople.AddToList = (function () {
    var container = $("#ModalContainer");

    function checkEnabilityExisitingList() {
        var existingList = container.find("#ExistingList");
        if (container.find("#NewGroup").is(":checked")) {
            existingList.prop("disabled", "disabled");
            container.find("#NewList").prop("checked", true);
            changeListStatus(true);
        }
        else
            existingList.removeAttr("disabled");
    }

    function changeListStatus(isNew, loadData) {
        var listName = container.find("#Name");

        if (isNew) {
            listName.show().attr("required", "required");
            listName.parent().removeClass("select-container");
            container.find("#ListId").hide();
            $("span[data-valmsg-for='Name']").show();
        }
        else {
            if (loadData) {
                bindExistingListDropDown(true);
                return;
            }
            listName.val("").hide().removeAttr("required");
            listName.parent().addClass("select-container");
            container.find("#ListId").show();
            $("span[data-valmsg-for='Name']").hide();
        }
    }

    function changeGroupStatus(isNew) {
        var groupName = container.find("#Group_Name");

        if (isNew) {
            groupName.show().attr("required", "required");
            groupName.parent().removeClass("select-container");
            container.find("#Group_Id").hide();
            $("span[data-valmsg-for='Group.Name']").show();
        }
        else {
            groupName.val("").hide().removeAttr("required");
            groupName.parent().addClass("select-container");
            container.find("#Group_Id").show();
            $("span[data-valmsg-for='Group.Name']").hide();
        }
    }

    function btnAddToList_Click() {
        $('#vsAddToList').addClass("d-none");
        var form = $('#frmAddList');

        $.ajax({
            url: (form.attr("data-action")),
            type: "POST",
            data: form.serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnAddList");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $('#vsAddToList').removeClass("d-none")
                        .html('<span>' + (jqXHR.status == 400 ? errorThrown : MediaPeople.genericError) + '</span>');
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess('List created succesfully', '');
                location.href = data.Url;
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnAddList");
            }
        });
        return false;
    }

    function bindExistingListDropDown() {
        var existingList = container.find("#ExistingList");
        if (!existingList.is(":checked"))
            return;

        $.ajax({
            url: (existingList.attr("data-action") + "?id=" + $("#Group_Id").val()),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("frmAddList");
            },
            success: function (data) {
                $("#GroupListContainer").html(data);
                changeListStatus(false, false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $('#vsAddToList').removeClass("d-none").html('<span>' + (jqXHR.status == 400 ? errorThrown : MediaPeople.genericError) + '</span>');
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("frmAddList");
            }
        });
    }

    function openPopupHandler(obj) {
        obj = $(obj);

        $.ajax({
            url: obj.attr("data-addlist-url"),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("main");
            },
            success: function (data) {
                MediaPeople.SetModalHtml(data);
                MediaPeople.InitBootstrapTooltips();

                $("#Type").val(obj.attr("data-type"));

                if (obj.attr("data-search-page") === "false") {
                    var mainContainer = $("div[data-main-container]");
                    var contactId = mainContainer.attr("data-contactId");
                    $("#CheckedItems").val(mainContainer.attr("data-outletId") + "#" + (contactId ? contactId.trim() : "0") + "#" + mainContainer.attr("data-recordType"));
                }
                else {
                    $("#Expand").val($(".resultTableHeader").attr("data-expand"));
                    var mySelection = window.MediaPeople.Grid.Selection();
                    mySelection = window.MediaPeople.Grid.Selection();
                    $("#CheckedItems").val(mySelection.CheckedItems);
                    $("#hfCheckboxState").val(mySelection.CheckboxState);
                    $("#hfUncheckedItems").val(mySelection.UncheckedItems);
                    $("#hfCurrentPageOnly").val($("#CurrentPageOnly").val());
                }

                $("#NewGroup,#ExistingGroup,#Group_Id,#NewList,#ExistingList").change(function () {
                    if ($(this).prop("id") === "Group_Id")
                        bindExistingListDropDown();
                });

                MediaPeople.InitJQueryValidate($("#frmAddList"), btnAddToList_Click);

                $("#ExistingGroup").click(function () {
                    changeGroupStatus(false);
                    checkEnabilityExisitingList();
                });


                $("#NewGroup").click(function () {
                    changeGroupStatus(true);
                    checkEnabilityExisitingList();
                });

                $("#ExistingList").click(function () {
                    changeListStatus(false, true);
                });

                $("#NewList").click(function () {
                    changeListStatus(true);
                });

                changeGroupStatus(true);
                changeListStatus(true);
                checkEnabilityExisitingList();
                MediaPeople.ShowModal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("main");
            }
        });
    }

    return {
        openPopup: openPopupHandler
    }
})();