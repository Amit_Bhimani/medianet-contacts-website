﻿/// <reference path="~/Scripts/Global"/>
/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js"/>
/// <reference path="~/Scripts/Global/common.js" />
MediaPeople.Group = (function () {
    var settings = {
        deleteObj : "",
        deleteAction: ""
    }
    function OpenRenameGroupPopup(url) {
        $.ajax({
            url: url,
            cache: false,
            success: function (data) {
                MediaPeople.ShowModal(data);

                MediaPeople.InitBootstrapTooltips();
                MediaPeople.InitJQueryValidate($("#frmRenameGroup"), btnRenameGroup_Click);
            }
        });
    }

    function btnRenameGroup_Click(groupId) {
        $('#vsRenameGroup').addClass("d-none");
        var form = $('#frmRenameGroup');
        var groupId = form.find("#GroupId").val();
        var newGroupName = form.find("#NewGroupName").val();

        $.ajax({
            url: $(form).attr('action'),
            type: "POST",
            data: $(form).serialize(),
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $('#vsRenameGroup').removeClass("d-none") .html('<span>' + (jqXHR.status == 400 ? errorThrown : MediaPeople.genericError) + '</span>');
            },
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnRenameGroup");
            },
            success: function () {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess('Group renamed successfully', '');
                RebindAll();
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnRenameGroup");
            }
        });
    }

    function DeleteGroup(obj) {

        var groupId = $(obj).data("id");
        var url = $(obj).attr("data-action");

        $.ajax({
            beforeSend: function () {
                //MediaPeople.Spinner.showSpinner("grp_" + groupId);
            },
            url: url,
            type: "POST",
            data: { groupId: groupId },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError('Failed to delete the group', jqXHR.status);
            },
            success: function (data) {
                $("div[data-group =" + groupId + "]").remove();
                MediaPeople.DisplaySuccess('Group deleted successfully', '');
                RebindAll();
            },
            complete: function () {
                //MediaPeople.Spinner.hideSpinner("grp_" + groupId);
            }
        });

    }

    function BindGroupEvents() {
        // bind the rename group event
        $("[data-rename-group]").on('click', function (e) {
            e.stopPropagation();
            var url = $(this).attr('data-rename-group-action');
            OpenRenameGroupPopup(url);
        });
        // bind the delete group event
        $("[data-delete-group]").attr("data-confirm-click", "MediaPeople.Group.DeleteGroup(obj)");        
    }

    function BindListEvents() {
        $(settings.deleteObj).find("[data-confirm]").attr("data-confirm-click", settings.deleteAction);
        InitEdit();
    }    

    function RebindGroup(groupId, rebindAction, sortCol, sortDir) {

        var params = {
            GroupId: groupId,
            SortDirection: sortDir,
            SortColumn: sortCol
        };

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("grp_"+ groupId);
            },
            url: rebindAction,
            type: "POST",
            data: params,
            success: function (data) {
                var container = $("tbody[data-grouped-id=" + groupId + "]");
                $(container).html(data);
                BindListEvents();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError("Failed to rebind the lists", jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("grp_" + groupId);
            }
        });
    }

    function RebindAll() {

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("main");
            },
            url: window.location.pathname + "/RebindAll",
            type: "Get",
            success: function (data) {
                $(".groupedListsPartial").html(data);
                BindGroupEvents();
                BindListEvents();
                InitSorting();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError("Failed to rebind the lists", jqXhr.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("main");
            }
        });
    }

    function InitSorting() {
        $("thead th span.sortable").on('click', function (e) {
            var header = $(this).parentsUntil("table").parent();
            var sortHandle = $(this).children();
            var sortCol = $(sortHandle).data("sortcol");
            var sortDir = "1";
            var groupId = $(header).data("grouped-id");
            var rebindAction = $(header).data("rebind-action");

            // If we are already sorting by the column just selected then reverse the order.
            // 1 = Ascending, 2 = Descending
            if (sortCol == header.data("sortcol")) {
                sortDir = (header.data("sortdir") == "1" ? "2" : "1");
            }
            // Store the column and sort order.
            $(header).data("sortcol", sortCol);
            $(header).data("sortdir", sortDir);

            // Remove any up/down arrows from all columns and reset back to normal.
            $(this).parentsUntil("thead").find(".fa-sort-up").removeClass("fa-sort-up")
                .addClass("fa-sort");
            $(this).parentsUntil("thead").find(".fa-sort-down").removeClass("fa-sort-down")
                .addClass("fa-sort");

            // Add the up/down arrow on the this column.
            $(sortHandle).removeClass("fa-sort")
                .addClass((sortDir == "1" ? "fa-sort-down" : "fa-sort-up"));

            RebindGroup(groupId, rebindAction, sortCol, sortDir);
        });

    }

    function InitEdit() {
        $("button[data-edit]").on("click", function (event) {
            event.preventDefault();

            $.ajax({
                url: $(this).attr("data-edit-action"),
                success: function (data) {
                    MediaPeople.ShowModal(data);
                    ProcessEdit();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
                }
            });
        });
    }

    function ProcessEdit() {
        MediaPeople.InitCheckboxes("#Modal");
        MediaPeople.InitBootstrapTooltips();
        MediaPeople.InitJQueryValidate($("#frmEdit"), SubmitEdit);
    }

    function SubmitEdit() {
        var form = $("#frmEdit");

        $("#vsEdit").addClass("d-none");

        $.ajax({
            url: form.attr("action"),
            type: "POST",
            data: form.serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnSave");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $("#vsEdit").removeClass("d-none").html("<span>" + (jqXHR.status === 400 ? errorThrown : MediaPeople.genericError) + "</span>");
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess("Edited succesfully", "");
                RebindAll();
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnSave");
            }
        });
        return false;
    }

    return {
        Init: function (deleteObj, deleteAction) {
            settings.deleteObj = deleteObj;
            settings.deleteAction = deleteAction;
            BindGroupEvents();
            BindListEvents();
            InitSorting();
        },
        DeleteGroup: DeleteGroup
    };
})();

