﻿/// <reference path="~/Scripts/Global/common.js"/>

MediaPeople.ShareProfile = (function () {
    var modalObject;

    function save() {
        $('#vsShareProfile').addClass("d-none");

        $.ajax({
            url: (modalObject.attr("data-share-profile-url")),
            type: "POST",
            data: $('#frmShareProfile').serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnShare");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $('#vsShareProfile').removeClass("d-none").html('<span>' + (jqXHR.status == 400 ? errorThrown : MediaPeople.genericError) + '</span>');
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess('The profile successfully shared', '');
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnShare");
            }
        });
        return false;
    }

    function openPopupHandler(obj) {
        modalObject = $(obj);

        var mainContainer = $("div[data-main-container]");
        var contactId = mainContainer.attr("data-contactId");

        $.ajax({
            url: modalObject.attr("data-share-profile-url") + "?outletId=" + mainContainer.attr("data-outletId") + "&recordType=" + mainContainer.attr("data-recordType") + "&contactId=" + (contactId ? contactId.trim() : ""),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("main");
            },
            success: function (data) {
                MediaPeople.SetModalHtml(data);
                MediaPeople.InitJQueryValidate($("#frmShareProfile"), save);
                MediaPeople.ShowModal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("main");
            }
        });
    }

    return {
        openPopup: openPopupHandler
    }
})();