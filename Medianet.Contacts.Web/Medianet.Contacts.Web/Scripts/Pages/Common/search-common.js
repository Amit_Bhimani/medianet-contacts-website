﻿/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Pages/common/grid.js"/>
/// <reference path="~/Scripts/Plugins/jquery.tokeninput.js" />
/// <reference path="~/Scripts/Pages/common/add-to-list.js" />

MediaPeople.SearchCommon = (function () {

    function saveSearch() {
        $("#validationSummary").addClass("d-none");

        $.ajax({
            url: "/Search/Save",
            type: "POST",
            data: $("#frmSaveSearch").serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnSaveSearch");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $("#validationSummary").removeClass("d-none")
                        .html("<span>" + (jqXHR.status == 400 ? jqXHR.statusText : MediaPeople.genericError) + "</span>"); //TODO: Sahar - I'd like to find a better way to add errors to validationSummary
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess("Search saved successfully", "");
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnSaveSearch");
            }
        });
        return false;
    }

    function changeGroupStatus(isNew) {
        var groupName = $("#Group_Name");

        if (isNew) {
            groupName.show().attr("required", "required");
            groupName.parent().removeClass("select-container");
            $("#Group_Id").hide();
            $("span[data-valmsg-for='Group.Name']").show();
        }
        else {
            groupName.val("").hide().removeAttr("required");
            groupName.parent().addClass("select-container");
            $("#Group_Id").show();
            $("span[data-valmsg-for='Group.Name']").hide();
        }
    }

    function openAddToListPopup(obj) {
        if (MediaPeople.Grid != null && !MediaPeople.Grid.ItemsAreSelected()) {
            MediaPeople.DisplayError("Please select at least one item");
            return;
        }
        MediaPeople.AddToList.openPopup(obj);
    }

    function openSaveSearchPopup() {
        $.ajax({
            url: "/Search/Save",
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("main");
            },
            success: function (data) {
                MediaPeople.SetModalHtml(data);
                changeGroupStatus(true);
                $("#ModalContainer").find("#ResultsCount").val($("#result-navigation #ResultCount").val());

                $("#ExistingGroup").click(function () {
                    changeGroupStatus(false);
                });

                $("#NewGroup").click(function () {
                    changeGroupStatus(true);
                });

                MediaPeople.InitJQueryValidate($("#frmSaveSearch"), saveSearch);
                MediaPeople.ShowModal();
                MediaPeople.InitBootstrapTooltips();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("main");
            }
        });
    }

    function initPopups() {
        $("a[data-addlist-url]").unbind("click").click(function (event) {
            event.preventDefault();
            event.stopPropagation();

            openAddToListPopup(this);
        });
        $("a[data-save-search]").unbind("click").click(function (event) {
            event.preventDefault();
            event.stopPropagation();

            openSaveSearchPopup();
            return false;
        });
    }

    return {
        Init: initPopups
    }
})();
