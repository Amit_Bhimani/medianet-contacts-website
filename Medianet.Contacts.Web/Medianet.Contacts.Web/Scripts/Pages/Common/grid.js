﻿/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js" />
/// <reference path="~/Scripts/Plugins"/>
/// <reference path="~/Scripts/Global/common.js"/>
/// <reference path="~/Scripts/Spinner.js" />
/// <reference path="~/Scripts/Pages/common/socialmedia.js" />
MediaPeople.Grid = (function () {
    var settings = {
        resultsContent: "resultsContent", //the id of the content section to show a loading control for
        singleDeleteDataIdName: "record", //the name of the data property containing the unique id
        rebindDataMethod: null, //method to add properties to post data
        rebindSuccess: null, //callback method for success
        rebindUrl: "/Search/Rebind",

        deleteDataMethod: null, //method to add properties to post data
        deleteSuccess: null, //callback method for success
        deleteUrl: "/Search/Delete",
        deletePromptMethod: null, //provide a method to call to prompt to delete. If null then delete happens straight away

        pinDataMethod: null, //method to add properties to post data
        pinSuccess: null, //callback method for success
        pinUrl: "/Search/Pin",

        showSpinner: false
    };

    var checkboxState = {
        All: "1",
        None: "2",
        PartUnchecked: "3",
        PartChecked: "4"
    };

    // Columns
    var header = $(".resultTableHeader");
    var columns = $(".resultTableHeader thead th span.sortable");

    // Grid container.
    var resultSection = $(".resultSection");

    function MakeCSVP(items) {
        var csvp = "";
        for (var i = 0; i < items.length; i++) {
            if (csvp.length > 0) csvp += ",";
            csvp += $(items[i]).data("record");
        }
        return csvp;
    }

    function getParams() {
        var sortCol = header.data("sortcol");
        var sortDir = header.data("sortdir");

        var params = {
            SortDirection: sortDir,
            SortColumn: sortCol,
            Expand: header.data("expand"),
            Page: $("#Page").val(),
            PageSize: $("#PageSize").val(),
            ResultCount: $("#ResultCount").val(),
            CheckboxState: MediaPeople.Grid.CheckboxState(),
            CurrentPageOnly: $("#CurrentPageOnly").val(),
            UncheckedItems: MediaPeople.Grid.UncheckedItems(),
            CheckedItems: MediaPeople.Grid.CheckedItems()
        };

        if ($.isFunction(settings.rebindDataMethod))
            params = $.extend({}, params, settings.rebindDataMethod.call() || {});

        return params;
    }

    function rebind(isUpdate) {
        var params = getParams();

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner(settings.resultsContent);
            },
            url: settings.rebindUrl,
            type: "POST",
            data: params,
            error: function (jqXhr, textStatus, errorThrown) {
                //hideLoading();
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            },
            success: function (data) {
                // Get new pagination state and the new result rows.
                var resultCount = $(data).filter("#result-message");
                var newPagination = $(data).filter("#result-navigation");
                var newRows = $(data).filter("table").children("tbody");

                // Update pagination and grid.
                $("#result-navigation").replaceWith(newPagination);

                if (resultCount.length > 0) {
                    $("#result-message").html(resultCount.html());
                }

                if ($("#ResultCount").val() === "0")
                    $("div[data-search-action-bar]").hide();

                if (newRows.children().length > 0) {
                    $("table.resultTableHeader tbody").replaceWith(newRows);

                    // Reinitalise checkboxes.
                    MediaPeople.Grid.DestroyCheckboxes();
                    MediaPeople.Grid.InitCheckboxes();
                    if (isUpdate) MediaPeople.Grid.UpdateCheckboxes();
                    MediaPeople.Grid.InitPageSize();
                    MediaPeople.Grid.InitPaging();
                    MediaPeople.Grid.InitSinglePinning();
                    MediaPeople.Grid.InitSingleDeleting();
                    MediaPeople.Grid.InitMultiDeleting();
                    resizeText();
                    MediaPeople.Socialmedia.GetAllTwitterImages(false, "table.resultTableHeader");
                    MediaPeople.InitBootstrapTooltips("table.resultTableHeader tbody");
                }
                else {
                    header.remove();
                    $(".resultSection").remove();
                    $("#result-navigation").remove();
                }

                if ($.isFunction(settings.rebindSuccess))
                    settings.rebindSuccess.call(null, isUpdate);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner(settings.resultsContent);
                Popover();
            }
        });
    }

    function resizeText() {
        // Resize the text of all fields with the "fitText" class so it doesn't wrap
        $(".fitText").each(function (index) {
            MediaPeople.FitText($(this), $(this).parent(), 11);
        });
    }

    function Popover() {
        $("[data-toggle='popover']").hover(function () {
            var o = $(this);
            var isContact = false;

            $("[data-toggle='popover']").not(this).popover('hide');

            var params = {
                outletId: o.attr('data-outletId'),
                recordType: o.attr('data-recordType'),
            };

            if (o.attr('data-contactId') != undefined) {
                params.contactId = o.attr('data-contactId');
                isContact = true;
            }

            o.popover({
                placement: 'right',
                html: true,
                trigger: 'manual',
                template: '<div class="popover preview" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                content: function () {
                    var content_id = "content-id-" + $.now();
                    var results = "";

                    $.ajax({
                        type: "POST",
                        url: o.attr("data-action"),
                        data: params,
                        success: function (data) {
                            results = data;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (jqXHR.status == 401)
                                MediaPeople.RedirectToLoginPage();
                            else
                                $('#' + content_id).html("Something went wrong");
                        },
                        complete: function () {
                            $('#' + content_id).html(results);
                            isContact ? MediaPeople.Socialmedia.GetTwitterImage(true, ".popover") : null;
                        }
                    });

                    return '<div id="' + content_id + '">Loading...</div>';
                }
            }).popover('show');

        }, function () {
            $("[data-toggle='popover']").popover('hide');
        });
    }
    function pushStateToHistory() {
        history.pushState(getParams(), "");
    }

    function scrollTopOnPagination() {
        $("html,body").animate({ scrollTop: $(".moveTop").offset().top }, 0);
    }

    return {
        Rebind: function (isUpdate) {
            rebind(isUpdate);
        },
        CheckboxState: function () {
            if ($("#mainCheckbox").length)
                return $("#mainCheckbox").attr("data-checkbox");
            else
                return checkboxState.None;
        },
        UncheckedItems: function () {
            var state = MediaPeople.Grid.CheckboxState();

            if ((state == checkboxState.All) || (state == checkboxState.None))
                return '';
            else {
                var items = $("table.resultTableHeader tbody input[type='checkbox']").filter(":not(:checked)");
                var csvp = MakeCSVP(items);
                return csvp;
            }
        },
        CheckedItems: function () {
            var state = MediaPeople.Grid.CheckboxState();

            if ((state == checkboxState.All) || (state == checkboxState.None))
                return '';
            else {
                var items = $("table.resultTableHeader tbody input[type='checkbox']").filter(":checked");
                var csvp = MakeCSVP(items);
                return csvp;
            }
        },
        Selection: function () {
            return {
                CheckboxState: MediaPeople.Grid.CheckboxState(),
                UncheckedItems: MediaPeople.Grid.UncheckedItems(),
                CheckedItems: MediaPeople.Grid.CheckedItems()
            };
        },
        ItemsAreSelected: function () {
            var state = window.MediaPeople.Grid.CheckboxState();
            return state !== checkboxState.None;
        },
        // Called when a new page is loaded.
        UpdateCheckboxes: function () {
            if ($("#mainCheckbox").length > 0)
                $("#mainCheckbox").tristate('update');
        },
        // Disable the tristate checkbox plugin.
        DestroyCheckboxes: function () {
            try {
                $("#mainCheckbox").tristate('destroy');
            } catch (e) {}
        },
        // Initialise the tristate checkbox behaviour.
        InitCheckboxes: function () {
            if ($("#mainCheckbox").length) {
                $("#mainCheckbox").tristate({
                    children: $("table.resultTableHeader tbody input[type='checkbox']"),
                    //childrenSelector: "table.resultTableHeader tbody input[type='checkbox']",
                    classes: {
                        partial: "tristate"
                    }
                });
            }
        },
        PerformSingleDeleting: function (button, message) {

            var row = button.closest("tr");
            var checkbox = button.closest("tr").find("input[type='checkbox']");
            var csvp = checkbox.length > 0 ? checkbox.data(settings.singleDeleteDataIdName) : button.attr(settings.singleDeleteDataIdName);
            var params = {
                CheckedItems: csvp,
                Type: "DeleteOne"
            };

            if ($.isFunction(settings.deleteDataMethod))
                params = $.extend({}, params, settings.deleteDataMethod.call() || {});

            $.ajax({
                url: settings.deleteUrl,
                data: params,
                type: "POST",
                beforeSend: function () {
                    if (settings.showSpinner == true)
                        MediaPeople.Spinner.showSpinner(settings.resultsContent);
                },
                success: function (data) {
                    rebind(true);

                    if ($.isFunction(settings.deleteSuccess))
                        settings.deleteSuccess.call();

                    if (message !== "") {
                        MediaPeople.DisplaySuccess(message, "");
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
                },
                complete: function () {
                    if (settings.showSpinner == true)
                        MediaPeople.Spinner.hideSpinner(settings.resultsContent);
                }
            });
            return false;
        },
        InitSingleDeleting: function () {

            resultSection.find("[data-confirm]").attr("data-confirm-click", "MediaPeople.Grid.PerformSingleDeleting(obj,'Selected row is deleted')");

            resultSection.find("[data-remove]").unbind("click").click(function () {
                $(this).tooltip('hide')
                MediaPeople.Grid.PerformSingleDeleting($(this), "");
                return false;
            });
        },
        PerformMultiDeleting: function () {
            var params = {
                CheckedItems: MediaPeople.Grid.CheckedItems(),
                UncheckedItems: MediaPeople.Grid.UncheckedItems(),
                CurrentPageOnly: $("#CurrentPageOnly").val(),
                CheckboxState: $("#mainCheckbox").attr("data-checkbox"),
                Type: "DeleteSelected",
                Expand: $(".resultTableHeader").data("expand")
            };

            if ($.isFunction(settings.deleteDataMethod))
                params = $.extend({}, params, settings.deleteDataMethod.call() || {});

            $.ajax({
                beforeSend: function () {
                    $("#mainCheckbox").removeAttr("checked");
                    $("#mainCheckbox").attr("data-checkbox", checkboxState.None);
                    MediaPeople.Spinner.showSpinner("searchResultSection");
                },
                url: settings.deleteUrl,
                data: params,
                type: "POST",
                success: function (data) {
                    rebind(true);

                    if ($.isFunction(settings.deleteSuccess))
                        settings.deleteSuccess.call();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
                },
                complete: function () {
                    MediaPeople.Spinner.hideSpinner("searchResultSection");
                }
            });
        },
        InitMultiDeleting: function () {
            $(".deleteRecords").unbind("click").click(function (event) {
                event.preventDefault();

                if (MediaPeople.Grid.ItemsAreSelected()) {
                    if ($.isFunction(settings.deletePromptMethod))
                        settings.deletePromptMethod.call();
                    else
                        MediaPeople.Grid.PerformMultiDeleting();
                } else {
                    MediaPeople.DisplayError("Please select at least one item.");
                }
                return false;
            });
        },
        InitMultiPinning: function () {
            // Clear all pinned across site.
            $("#clearPinned").click(function () {
                $.ajax({
                    url: settings.pinUrl,
                    type: "POST",
                    data: { Type: "Clear" },
                    success: function (data) {
                        var btn = $("#pinAll").find("i");
                        btn.removeClass("fal").addClass("fas");
                        rebind();
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
                    }
                });
            });

            // Pin all in current search.
            $("#pinAll").click(function () {
                var button = $(this).children();
                var buttonAttr = button.attr("data-prefix");
                var type = buttonAttr === "fas" ? "UnpinAll" : "PinAll";
                var params = {
                    Type: type,
                    Expand: $(".resultTableHeader").data("expand")
                };

                $.ajax({
                    url: settings.pinUrl,
                    data: params,
                    type: "POST",
                    success: function (data) {
                        if (data.AllPinned) {
                            $(".iconPin").attr("data-prefix", "fas");
                            $(".searchRow").addClass("pinned");
                        }
                        else {
                            $(".iconPin").attr("data-prefix", "fal");
                            $(".searchRow").removeClass("pinned");
                        }

                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
                    }
                });
            });
        },
        InitSinglePinning: function () {
            $("table.resultTableHeader tbody .btnPin").each(function () {
                $(this).click(function () {
                    var button = $(this).children();
                    var csvp = button.closest("tr").find("input[type='checkbox']").data("record");
                    var buttonAttr = button.attr("data-prefix");
                    var type = buttonAttr === "fas" || button.closest("tr").hasClass("rtbNotPartOfSearch") ? "UnpinOne" : "PinOne";
                    var currentSearch = button.closest("tr").data("current-search");

                    var params = {
                        Type: type,
                        Items: csvp
                    };

                    $.ajax({
                        url: settings.pinUrl,
                        type: "POST",
                        data: params,
                        success: function (data) {
                            if (type === "PinOne") {
                                button.attr("data-prefix", "fas");
                                button.closest("tr").addClass("pinned");
                            } else {
                                button.attr("data-prefix", "fal");
                                button.closest("tr").removeClass("pinned");
                            }
                            if (type === "UnpinOne") {
                                if (currentSearch.toLowerCase() !== "true" &&
                                    $("table.resultTableHeader tbody tr").length == 1) {
                                    var currentPage = parseInt($("#Page").val());
                                    $("#Page").val(--currentPage);
                                    rebind();
                                } else if (currentSearch.toLowerCase() !== "true") {
                                    button.closest("tr").remove();
                                    var resultCount = parseInt($("#ResultCount").val());
                                    resultCount--;
                                    $("#ResultCount").val(resultCount);
                                    $("#ResultCountDisplay").html(resultCount);
                                } else if (currentSearch.toLowerCase() === "true" && !data.AllPinned) {
                                    $("#pinAll").children().attr("data-prefix", "fal");
                                }
                            } else if (type === "PinOne") {
                                // If we've pinned an item and now all are pinned, disable
                                // pin all.
                                if (currentSearch.toLowerCase() === "true" && data.AllPinned) {
                                    $("#pinAll").children().attr("data-prefix", "fas");
                                }
                            }
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
                        }
                    });
                });
            });
        },
        InitPaging: function () {
            $(".goToPage").keypress(function (e) {
                if (e.which === 13) {
                    var pageNo = $(this).val();

                    if (isNaN(pageNo)) {
                        MediaPeople.DisplayError("Invalid page number");
                        return;
                    }

                    var pageNoInt = parseInt(pageNo);
                    var numberOfPages = parseInt($("#NumberOfPages").val());

                    if (pageNoInt < 1 || pageNoInt > numberOfPages) {
                        MediaPeople.DisplayError("Page number out of range");
                        return;
                    }

                    $("#Page").val(pageNo);
                    rebind();
                    pushStateToHistory();
                    scrollTopOnPagination();

                }
            });

            // Next / Prev paging.
            $(".nextPage, .prevPage").on("click", function () {
                var pageNo = $(this).data("page");
                if (!isNaN(pageNo)) {
                    $("#Page").val(pageNo);
                    rebind();
                    pushStateToHistory();
                    scrollTopOnPagination();

                }
            });
        },
        // Change page size.
        InitPageSize: function () {
            $(".pageSize").on('click', function () {
                var pageSize = $(this).data("page-size");

                if (!isNaN(pageSize)) {
                    $("#PageSize").val(pageSize);
                    $("#Page").val(1);
                    rebind();
                    pushStateToHistory();
                }
            });
        },
        InitSorting: function () {
            columns.unbind("click").on({
                click: function (e) {
                    var sortHandle = $(this).children();
                    var sortCol = sortHandle.data("sortcol");
                    var sortDir = "1";

                    // If we are already sorting by the column just selected then reverse the order.
                    // 1 = Ascending, 2 = Descending
                    if (sortCol == header.data("sortcol")) {
                        sortDir = (header.data("sortdir") == "1" ? "2" : "1");
                    }

                    // Store the column and sort order.
                    header.data("sortcol", sortCol);
                    header.data("sortdir", sortDir);

                    // Remove any up/down arrows from all columns and reset back to normal.
                    columns.children('.fa-sort-up').removeClass("fa-sort-up")
                        .addClass("fa-sort");
                    columns.children('.fa-sort-down').removeClass("fa-sort-down")
                        .addClass("fa-sort");

                    // Add the up/down arrow on the this column.
                    sortHandle.removeClass("fa-sort")
                        .addClass((sortDir == "1" ? "fa-sort-down" : "fa-sort-up"));

                    // Reload the grid.
                    rebind();
                }
            });
        },
        Init: function (options, updateHistory) {
            if (updateHistory != false)
                pushStateToHistory();
            header = $(".resultTableHeader");
            columns = $(".resultTableHeader thead th span.sortable");
            resultSection = $(".resultSection");

            settings = $.extend({}, settings, options || {});

            MediaPeople.Grid.InitPageSize();
            MediaPeople.Grid.InitPaging();
            MediaPeople.Grid.InitSorting();
            MediaPeople.Grid.InitSinglePinning();
            MediaPeople.Grid.InitMultiPinning();
            MediaPeople.Grid.InitSingleDeleting();
            MediaPeople.Grid.InitMultiDeleting();
            MediaPeople.Grid.InitCheckboxes();
            MediaPeople.Grid.InitPopOver();
            resizeText();
            MediaPeople.Socialmedia.GetAllTwitterImages(false, "table.resultTableHeader");
        },
        InitPopOver: function () {
            Popover();
        }
    }
})();
