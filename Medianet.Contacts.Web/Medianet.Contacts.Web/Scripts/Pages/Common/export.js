﻿
MediaPeople.Export = (function () {
    var defaults =
    {
        ExportButton: ".showListExport",
        ListId: "#hdnListId",
        TaskId: "#HiddenTaskId",
        ExportProfileName: "#exportProfileName",
        ExportProfileDropdown: "#exportProfileId",
        ExportType: "#ExportType",
        ExportFields: "#exportFields",
        ExportFieldsSelected: "#exportFieldsSelected",
        ExportField: ".exportField"
    };
    var sort = null;

    function configureExport() {
        MediaPeople.InitJQueryValidate($("#saveExportForm"), onSaveExportClick);
        MediaPeople.InitBootstrapTooltips();

        $("#exportSelectAll").on("click", function () {
            var selectedFields = $(defaults.ExportFieldsSelected);

            $(defaults.ExportFields + " > .exportField:visible").each(function () {
                var field = $(this);

                selectField(field, selectedFields);
            });

            updateExportCount();
        });

        $("#exportUnselectAll").on("click", function () {
            $(defaults.ExportFieldsSelected + " > .exportField").each(function () {
                var currentField = $(this);

                unselectField(currentField);
            });

            updateExportCount();
        });

        $(".exportField > a").on("click", function (event) {
            event.preventDefault();

            var field = $(this).parent();
            var selectedFields = $(defaults.ExportFieldsSelected);

            selectField(field, selectedFields);
            updateExportCount();
        });

        $(".exportField > label").on("dblclick", function (event) {
            event.preventDefault();

            var field = $(this).parent();
            var selectedFields = $(defaults.ExportFieldsSelected);

            selectField(field, selectedFields);
            updateExportCount();
        });

        $("#NewExport").on("click", function () {
            onExportStateChanged();
        });

        $("#ExistingExport").on("click", function () {
            onExportStateChanged();
        });

        $("#deleteProfile").on("click", onDeleteExportClick);
        $("#export-button").on("click", onExportClick);

        onExportStateChanged();

        var list = document.getElementById("exportFieldsSelected");
        sort = Sortable.create(list);

        $("#fieldFilter").on("keyup", function () {
            var filterValue = $("#fieldFilter").val().toLowerCase();

            $(defaults.ExportFields + " > .exportField").each(function () {
                var field = $(this);

                if (!matchesFilter(field, filterValue)) {
                    field.hide();
                } else if (field.attr("selected") !== "selected") {
                    field.show();
                }
            });
        });
    }

    function unselectField(currentField) {
        var hiddenField = $(defaults.ExportFields + " div[data-value='" + currentField.data("value") + "']");

        currentField.remove();

        if (matchesFilter(hiddenField, $("#fieldFilter").val()))
            hiddenField.show();

        hiddenField.removeAttr("selected");
    }

    function selectField(field, selectedFields) {
        var selectedField = field.clone();

        selectedField.prepend("<span class='drag'><i class='fal fa-bars fa-fw'></i></span>");
        selectedField.appendTo(selectedFields);
        selectedField.find("[data-fa-i2svg]").removeClass("fa-plus").addClass("fa-minus");
        selectedField.show(); //The field we copied may have been hidden so make sure this is visible

        field.hide();
        field.attr("selected", "selected");

        selectedField.find("a").on("click", function (event) {
            event.preventDefault();

            var currentField = $(this).parent();

            unselectField(currentField);
            updateExportCount();
        });
    }

    function matchesFilter(field, filterValue) {
        return (field.find("label").html().toLowerCase().indexOf(filterValue.toLowerCase()) >= 0);
    }

    function onExportStateChanged() {
        $("#vsExportList").addClass("d-none");

        var newExport = $("#NewExport");
        var exportName = $(defaults.ExportProfileName);

        if (newExport.is(":checked")) {
            exportName.show().attr("required", "required");
            exportName.parent().removeClass("select-container");
            $(defaults.ExportProfileDropdown).hide();
            $("span[data-valmsg-for='Name']").show();
        } else {
            exportName.hide().removeAttr("required");
            exportName.parent().addClass("select-container");
            $(defaults.ExportProfileDropdown).show();
            $("span[data-valmsg-for='Name']").hide();

            $.ajax({
                url: "/List/GetExportProfiles?exportType=" + $(defaults.ExportType).val(),
                success: function (data) {
                    replaceExportProfiles(data);

                    // Bind the change event to the export profile list.
                    $(defaults.ExportProfileDropdown).change(onExportDropdownChanged);

                    // Trigger the change event now so we load the selected fields
                    onExportDropdownChanged();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MediaPeople.DisplayError("", jqXHR.status);
                }
            });
        }
    }

    function replaceExportProfiles(data) {
        var replace = $(defaults.ExportProfileDropdown);
        var id = $("#NewExportId").val();

        replace.replaceWith(data);

        // If any in the dropdown match the current id then select it
        if (id !== "" && id !== "0") {
            $(defaults.ExportProfileDropdown).find("option").each(function () {
                var option = $(this);

                if (option.val() === id) {
                    option.prop("selected", true);
                }
            });
        }
    }

    function onExportDropdownChanged() {
        $("#vsExportList").addClass("d-none");
        $("#fieldFilter").val("");

        if (!$("#NewExport").is(":checked")) {
            var id = $(defaults.ExportProfileDropdown).val();

            $("#NewExportId").val(id);

            if ($.isNumeric(id)) {
                // Fetch the details of the export profile.
                $.ajax({
                    beforeSend: function () {
                        MediaPeople.Spinner.showSpinner("saveExportForm");
                    },
                    type: "GET",
                    url: "/List/GetExportProfile?id=" + id,
                    dataType: "json",
                    success: function (data) {
                        var allFields = $(defaults.ExportFields);
                        var selectedFields = $(defaults.ExportFieldsSelected);

                        if (data) {
                            if (typeof data.ExportFields !== "undefined" && data.ExportFields !== null) {
                                // Make sure all fields are visible to start with, and nothing is selected
                                allFields.find(".exportField").each(function () {
                                    var item = $(this);
                                    item.show();
                                    item.removeAttr("selected");
                                });

                                selectedFields.empty();

                                if (data.ExportFields !== "") {
                                    var selected = data.ExportFields.split(",");

                                    // Now hide all the selected fields and add a copy to the selected list
                                    for (var i = 0; i < selected.length; i++) {
                                        var field = $(defaults.ExportFields + " div[data-value='" + selected[i] + "']");
                                        selectField(field, selectedFields);
                                    }
                                }

                                updateExportCount();
                            }

                            if (data.IsPrivate)
                                $("#Private").prop("checked", true);
                            else
                                $("#Public").prop("checked", true);
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        if (jqXHR.status == 401)
                            MediaPeople.RedirectToLoginPage();
                        else
                            $("#vsExportList").removeClass("d-none").html("<span>Error fetching saved export settings</span>");
                    },
                    complete: function () {
                        MediaPeople.Spinner.hideSpinner("saveExportForm");
                    }
                });
            }
        }
    }

    function onSaveExportClick() {
        $("#vsExportList").addClass("d-none");

        var params = {
            id: 0,
            name: "",
            isPrivate: $("#Private").is(":checked"),
            exportFields: $.map($(defaults.ExportFieldsSelected + " > div"), function (n, i) { return $(n).data("value"); }),
            exportType: $("input[name=ExportType]").val()
        };

        if ($("#NewExport").is(":checked")) {
            params.name = $.trim($(defaults.ExportProfileName).val());

            // Don't let them save if they haven't specified a name.
            if (params.name === "") {
                $("#vsExportList").removeClass("d-none").html("<span>Please provide a name</span>");
                $(defaults.ExportProfileName).focus();
                return;
            }
        }
        else {
            var id = $(defaults.ExportProfileDropdown).val();

            if ($.isNumeric(id)) {
                params.id = parseInt(id);
                params.name = $(defaults.ExportProfileDropdown).find(":selected").text();
            }
            else {
                $("#vsExportList").removeClass("d-none").html("<span>No export profile is selected</span>");
                return;
            }
        }

        if (params.exportFields.length === 0) {
            $("#vsExportList").removeClass("d-none").html("<span>No columns selected. Please select at least one before saving</span>");
            return;
        }

        $.ajax({
            type: "POST",
            url: "/List/SaveExportProfile",
            data: params,
            dataType: "json",
            traditional: true, // needed for the exportFields array to work
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnSaveProfile");
            },
            success: function (data) {
                if (data) {
                    // Display success message.
                    MediaPeople.DisplaySuccess("Export profile saved");

                    // Store the Id of the item just saved so when we refresh below it will be selected.
                    $("#NewExportId").val(data.id.toString());

                    // Refresh the dropdown.
                    $("#ExistingExport").click();
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnSaveProfile");
            }
        });
    }

    function onDeleteExportClick() {
        $("#vsExportList").addClass("d-none");

        if ($("#NewExport").is(":checked")) {
            $("#vsExportList").removeClass("d-none").html("<span>Please select an existing export setting to delete</span>");
            return;
        }

        var id = $(defaults.ExportProfileDropdown).val();

        if (!$.isNumeric(id)) {
            $("#vsExportList").removeClass("d-none").html("<span>No export setting is selected</span>");
            return;
        }

        var params = {
            id: parseInt(id)
        };

        $.ajax({
            type: "POST",
            url: "/List/DeleteExportProfile",
            data: params,
            dataType: "json",
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnDeleteProfile");
            },
            success: function (data) {
                // Display success message.
                MediaPeople.DisplaySuccess("Export profile deleted");

                $("#NewExportId").val("0");

                // Refresh the dropdown.
                $("#ExistingExport").click();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXhr.responseText, jqXhr.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnDeleteProfile");
            }
        });
    }

    function onExportClick() {
        $("#vsExportList").addClass("d-none");

        var iFrame = $("#exportFrame");
        var iFrameDoc = iFrame[0].contentDocument || iFrame[0].contentWindow.document;
        var mySelection = MediaPeople.Grid.Selection();
        var mySelectedFields = $.map($(defaults.ExportFieldsSelected + " > div"), function (n, i) { return $(n).data("value"); }).join(",");

        if (mySelectedFields.length === 0) {
            $("#vsExportList").removeClass("d-none").html("<span>No columns selected. Please select at least one before exporting</span>");
            return;
        }

        iFrameDoc.write("<form id='exportForm' action='/List/Export' method='post'>" + // target='_blank'>" +
            "<input type='hidden' id='listId' name='listId' value='" + ($(defaults.ListId).val() === undefined ? "0" : $(defaults.ListId).val()) + "' />" +
            "<input type='hidden' id='taskId' name='taskId' value='" + ($(defaults.TaskId).val() === undefined ? "0" : $(defaults.TaskId).val()) + "' />" +
            "<input type='hidden' id='exportFields' name='exportFields' value='" + mySelectedFields + "' />" +
            "<input type='hidden' id='checkboxState' name='checkboxState' value='" + mySelection.CheckboxState + "' />" +
            "<input type='hidden' id='checkedItems' name='checkedItems' value='" + mySelection.CheckedItems + "' />" +
            "<input type='hidden' id='uncheckedItems' name='uncheckedItems' value='" + mySelection.UncheckedItems + "' />" +
            "<input type='hidden' id='exportType' name='exportType' value='" + $("#ExportType").val() + "' />" +
            "</form>");

        iFrameDoc.close();
        iFrame.contents().find("#exportForm").submit();
    }

    function updateExportCount() {
        var count = $(defaults.ExportFieldsSelected + " > .exportField").length;
        $("#exportSelectedCount").html(count);
    }

    return {
        Init: function (exportType) {
            $(defaults.ExportButton).click(function (event) {
                event.preventDefault();

                if (MediaPeople.Grid.ItemsAreSelected()) {
                    $.ajax({
                        url: "/List/Export?exportType=" + exportType,
                        success: function (data) {
                            MediaPeople.ShowModal(data, true);

                            configureExport();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            MediaPeople.DisplayError("", jqXHR.status);
                        }
                    });
                }
                else
                    MediaPeople.DisplayError("Please select at least one record to export");
            });
        }
    };
})();
