﻿/// <reference path="~/Scripts/Global/common.js"/>

MediaPeople.SuggestChange = (function () {
    var modalObject;

    function saveSuggestion() {
        $('#vsSuggestion').addClass("d-none");

        $.ajax({
            url: (modalObject.attr("data-suggestion-url")),
            type: "POST",
            data: $('#frmSuggestion').serialize(),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("btnSuggestion");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401)
                    MediaPeople.RedirectToLoginPage();
                else
                    $('#vsSuggestion').removeClass("d-none")
                        .html('<span>' + (jqXHR.status == 400 ? errorThrown : MediaPeople.genericError) + '</span>');
            },
            success: function (data) {
                MediaPeople.HideModal();
                MediaPeople.DisplaySuccess('Update request successfully sent', '');
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("btnSuggestion");
            }
        });
        return false;
    }

    function openPopupHandler(obj) {
        modalObject = $(obj);

        var mainContainer = $("div[data-main-container]");
        var contactId = mainContainer.attr("data-contactId");

        $.ajax({
            url: modalObject.attr("data-suggestion-url") + "?message=" + encodeURIComponent(modalObject.attr("data-name")) + "&outletId=" + mainContainer.attr("data-outletId") + "&recordType=" + mainContainer.attr("data-recordType") + "&contactId=" + (contactId ? contactId.trim() : ""),
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner("main");
            },
            success: function (data) {
                MediaPeople.SetModalHtml(data);
                MediaPeople.InitJQueryValidate($("#frmSuggestion"), saveSuggestion);
                MediaPeople.ShowModal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError(jqXHR.responseText, jqXHR.status);
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner("main");
            }
        });
    }

    return {
        openPopup: openPopupHandler
    }
})();