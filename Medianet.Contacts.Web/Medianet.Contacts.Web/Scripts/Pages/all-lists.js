﻿/// <reference path="~/Scripts/Global"/>
/// <reference path="~/Scripts/jquery-3.0.0-vsdoc.js"/>
/// <reference path="~/Scripts/Global/common.js" />
MediaPeople.AllLists = (function () {

    function DeleteList(obj) {
        var listId = $(obj).data("id");
        var url = $(obj).attr("data-action");
        var spinner = $(obj).parentsUntil('[data-spinnercontainer]').parent()[0];
        var spinnername = "main";
        if (spinner) {
            spinnername = $(spinner).data('spinnercontainer');
        }

        $.ajax({
            beforeSend: function () {
                MediaPeople.Spinner.showSpinner(spinnername);
            },
            url: url,
            type: "POST",
            data: { listId: listId },
            error: function (jqXHR, textStatus, errorThrown) {
                MediaPeople.DisplayError('Failed to delete list', jqXHR.status);
            },
            success: function () {
                var body = $("tr[data-list-id =" + listId + "]").parent();
                $("tr[data-list-id =" + listId + "]").remove();
                if (!$(body).has("tr").length) {
                    $(body).parentsUntil("div[data-group]").parent().remove();
                }
                MediaPeople.DisplaySuccess('List deleted successfully', '');
            },
            complete: function () {
                MediaPeople.Spinner.hideSpinner(spinnername);
            }

        });

    }

    return {
        DeleteList: DeleteList
    };
})();

$(document).ready(function () {
    window.MediaPeople.Group.Init("[data-list-id]", "MediaPeople.AllLists.DeleteList(obj)");
});

