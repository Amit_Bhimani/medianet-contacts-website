﻿
MediaPeople.MediaMovements = (function () {
    return {
        init: function () {
            MediaPeople.Socialmedia.GetAllTwitterImages(true, "#divMediaMovements");
        }
    };
})();

$(document).ready(function () {
    MediaPeople.MediaMovements.init();
});