﻿// ========================================================
// Original License:
// Copyright 2011, http://webworld-develop.blogspot.com/
// Artistic License 2.0
// http://www.opensource.org/licenses/artistic-license-2.0
// ========================================================

// Copyright 2012, Australian Associated Press Pty Ltd
// Modified By: Christopher Chow
// Modified Date: 20/01/2012
// 
// - Removed disabled checkbox support.
// - Added support to update for when more checkboxes are added dynamically
// - Added namespaces so plugin can be destroyed.

(function ($) {
    // Default defaults.
    var defaults = {
        dataName: "data-checkbox",
        dataValues: {
            all: "1",
            none: "2",
            partAll: "3",
            partNone: "4"
        },
        classes: {
            partial: "tristate"
        },
        children: null
    };

    // Children checkboxes.
    var checkboxes = null;

    // Main checkbox that that is the real checkbox.
    var mainCheckbox = null;

    // Fake checkbox that shows the three states.
    var fakeCheckbox = null;

    // Some aliases for the CSS classes so they're shorter.
    var ch = null;
    var part = null;
    var unch = null;

    // Set the CSS aliases.
    function SetCSSAliases() {
        ch = defaults.classes.checked;
        part = defaults.classes.partial;
        unch = defaults.classes.unchecked;
    }

    // Get the data value from the main checkbox data attribute.
    function MainCheckboxStatus() {
        return mainCheckbox.attr(defaults.dataName);
    }

    // Updates the state of the main checkbox depending on the data
    // attribute value.
    function UpdateTriState() {
        var status = MainCheckboxStatus();

        if (status === defaults.dataValues.all)
            mainCheckbox.prop('checked', true);
        else if (status === defaults.dataValues.none)
            mainCheckbox.prop('checked', false);
        else if (status === defaults.dataValues.partNone ||
            status === defaults.dataValues.partAll) {
            mainCheckbox.prop('checked', true);
            mainCheckbox.addClass(part);
        }
    }

    function BindChildrenCheckboxes() {
        // We only have unselected items on this page.
        if ($("#CurrentPageOnly").val().toLowerCase() === "true") {
            // All items are now checked.
            if (checkboxes.filter(":checked").length === checkboxes.length) {
                mainCheckbox.removeClass(part);
                mainCheckbox.attr(defaults.dataName, defaults.dataValues.all);
            }
            // We've unchecked all items and we only had unselected items on this page.
            else if (checkboxes.filter(":not(:checked)").length === checkboxes.length) {
                mainCheckbox.prop('checked', false);
                mainCheckbox.removeClass(part);
                mainCheckbox.attr(defaults.dataName, defaults.dataValues.none);
            }
            // Set to partially checked.
            else {
                mainCheckbox.prop('checked', true);
                mainCheckbox.addClass(part);

                if (MainCheckboxStatus() === defaults.dataValues.all)
                    mainCheckbox.attr(defaults.dataName, defaults.dataValues.partAll);
                else if (MainCheckboxStatus() === defaults.dataValues.none)
                    mainCheckbox.attr(defaults.dataName, defaults.dataValues.partNone);
            }
        }
        // There are items unselected on other pages.
        else {
            // Set to partially checked.
            mainCheckbox.addClass(part);
            if (MainCheckboxStatus() === defaults.dataValues.all)
                mainCheckbox.attr(defaults.dataName, defaults.dataValues.partAll);
            else if (MainCheckboxStatus() === defaults.dataValues.none)
                mainCheckbox.attr(defaults.dataName, defaults.dataValues.partNone);
        }
    }

    var methods = {
        init: function (options) {
            return this.each(function () {
                // Override defaults and set the CSS aliases.
                defaults = $.extend({}, defaults, options);
                SetCSSAliases();

                mainCheckbox = $(this);

                // Get the child checkboxes.
                checkboxes = defaults.children;

                // Set the state of the checkbox.
                UpdateTriState();

                mainCheckbox.bind('click.tristate', function () {
                    mainCheckbox.removeClass(part);

                    if (mainCheckbox.is(':checked')) {
                        mainCheckbox.attr(defaults.dataName, defaults.dataValues.all);
                        checkboxes.prop("checked", true);
                    }
                    else {
                        mainCheckbox.attr(defaults.dataName, defaults.dataValues.none);
                        checkboxes.prop("checked", false);
                    }
                    $("#CurrentPageOnly").val("true");
                });

                checkboxes.bind('click.tristate', BindChildrenCheckboxes);
            });
        },

        update: BindChildrenCheckboxes,

        destroy: function () {
            return this.each(function () {
                $(this).prev().unbind('.tristate');
                defaults.children.unbind('.tristate');
            });
        }
    };

    $.fn.tristate = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }
    }
})(jQuery);