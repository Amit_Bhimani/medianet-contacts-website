﻿// single class to control all the spinners on a page
/// <reference path="lodash.js" />
/// <reference path="jquery-3.0.0-vsdoc.js" />

MediaPeople.Spinner = (function () {
    var spnDefault = {
        active: false,
        delay: 300, /* delay in ms before showing the spinner */
        name: "", /* Name of the container for the spinner. Cannot be blank. kept in data-spinner-container and also data-spinner-loader (same name) */
        align: "center", /* class to align the spinner . centre, top, bottom */
        timer: null /* timer variable used for clearing the timer. */
    };
    var buttonSpinnerhtml = "<span class='pr-2 btnSpinner'><i class='fal fa-spinner fa-spin'></i></span>";
    var loaderhtml = "<div class='loader'><i class='fal fa-spinner fa-spin fa-2x'></i></div>";
    var isInitialised = false;
    var spinners = []; /* array to hold spinners */

    function loadSpinners() {
        var spins = $("[data-spinnercontainer]");
        spins.each(function (i, spn) {
            spn = $(spn);
            var spinname = spn.data().spinnercontainer;
            if (_.findIndex(spinners, function (s) { return s.name === spinname; }) === -1) {
                var ob = _.clone(spnDefault);
                ob.name = spinname;
                spinners.push(ob);
            }
            if (!$(spn).hasClass("loader-container")) {
                $(spn).addClass("loader-container");
            }

        });

    };

    function init() {
        if (!isInitialised) {
            isInitialised = true;
            spinners = [];
        }
    };

    function initAndLoadSpinners() {
        if (!isInitialised) {
            init();
        }
        loadSpinners(); /* in case we have loaded content via ajax */
    };
    function spinnerTimer(spinners, index) {

        var spin = spinners[index];
        if (spin) {
            if (spin.active) {
                var containerEle = $("[data-spinnercontainer = '" + spin.name + "']");
                if (!$(containerEle).hasClass("loader-container")) {
                    $(containerEle).addClass("loader-container");
                }

                var isButton = containerEle.is('button');
                var loaderEle = $(containerEle).children(isButton ? ".btnSpinner" : ".loader");

                if (loaderEle.length === 0) {
                    containerEle.prepend(isButton ? buttonSpinnerhtml : loaderhtml);
                }

                if (isButton) {
                    containerEle.attr("disabled", "disabled");
                    loaderEle.show();
                }
                else {
                    if (spin.align !== "top") {
                        /* take spinner to top if container height is > window height */
                        var vh = $(containerEle).height();
                        var windowvh = window.innerHeight;
                        if (vh > windowvh) {
                            spin.align = "top";
                        }
                    }
                    loaderEle = $((containerEle).children(".loader")[0]);
                    loaderEle.addClass("show").addClass(spin.align);

                    if(spin.name == "main")
                        loaderEle.addClass("main");
                }
            }
            spin.timer = null;
        }

    };
    function showSpinner(spinnerName) {
        initAndLoadSpinners();
        var index = _.findIndex(spinners, function (s) { return s.name === spinnerName });

        if (index >= 0) {
            var spin = spinners[index];
            spin.active = true;
            if (spin.timer == null) {
                spin.timer = setTimeout(spinnerTimer, spin.delay, spinners, index);
            }
        }
    };

    function hideSpinner(spinnerName) {
        var index = _.findIndex(spinners, function (s) { return s.name === spinnerName });
        if (index >= 0) {
            var spin = spinners[index];
            spin.active = false;
            if (spin.timer != null) {
                clearTimeout(spin.timer);
                spin.timer = null;
            }
            var containerEle = $("[data-spinnercontainer = '" + spin.name + "']");
            if (containerEle != null) {
                var isButton = containerEle.is('button');
                var loaderEle = $((containerEle).children(isButton ? ".btnSpinner" : ".loader")[0]);
                if (isButton) {
                    containerEle.removeAttr("disabled");
                    loaderEle.hide();
                }
                if (loaderEle != null) {
                    loaderEle.removeClass(spin.align).removeClass("show");
                }
            }
        }
    };

    function setDelay(spinnerName, delayMs) {
        initAndLoadSpinners();
        var index = _.findIndex(spinners, function (s) { return s.name === spinnerName });
        if (index >= 0) {
            spinners[index].delay = delayMs;
        }

    };

    function setAlign(spinnerName, alignment) {
        initAndLoadSpinners();
        var index = _.findIndex(spinners, function (s) { return s.name === spinnerName });
        if (index >= 0) {
            spinners[index].align = alignment;
        }
    };
    function removeInlineSpinner(container) {
        if ($(container).find('.fa-spinner').length !== 0) {
            $(container).find('.fa-spinner').parent().html("");
        }
    };

    return {
        init: function () {
            initAndLoadSpinners();
        },
        setDelay: function (spinnerName, delayMs) {
            setDelay(spinnerName, delayMs);
        },
        setAlign: function (spinnerName, alignment) {
            setAlign(spinnerName, alignment);
        },
        showSpinner: function (spinnerName) {
            showSpinner(spinnerName);
        },
        hideSpinner: function (spinnerName) {
            hideSpinner(spinnerName);
        },
        removeInlineSpinner: function(container) {
            removeInlineSpinner(container);
        }
    }
})();


