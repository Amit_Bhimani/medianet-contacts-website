﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Medianet.Contacts.Web.SolrService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://MedianetContacts/services", ConfigurationName="SolrService.ISolrService")]
    public interface ISolrService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://MedianetContacts/services/ISolrService/UpdateContact", ReplyAction="http://MedianetContacts/services/ISolrService/UpdateContactResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Medianet.Contacts.Wcf.DataContracts.DTO.ServiceFault), Action="http://MedianetContacts/services/ISolrService/UpdateContactServiceFaultFault", Name="ServiceFault")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(Medianet.Contacts.Wcf.DataContracts.DTO.SearchResult))]
        bool UpdateContact(Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier record);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://MedianetContacts/services/ISolrService/UpdateContact", ReplyAction="http://MedianetContacts/services/ISolrService/UpdateContactResponse")]
        System.Threading.Tasks.Task<bool> UpdateContactAsync(Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier record);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISolrServiceChannel : Medianet.Contacts.Web.SolrService.ISolrService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SolrServiceClient : System.ServiceModel.ClientBase<Medianet.Contacts.Web.SolrService.ISolrService>, Medianet.Contacts.Web.SolrService.ISolrService {
        
        public SolrServiceClient() {
        }
        
        public SolrServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SolrServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SolrServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SolrServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool UpdateContact(Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier record) {
            return base.Channel.UpdateContact(record);
        }
        
        public System.Threading.Tasks.Task<bool> UpdateContactAsync(Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier record) {
            return base.Channel.UpdateContactAsync(record);
        }
    }
}
