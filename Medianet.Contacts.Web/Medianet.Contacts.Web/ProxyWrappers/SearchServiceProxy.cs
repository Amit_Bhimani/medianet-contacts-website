﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.SearchService;

namespace Medianet.Contacts.Web.ProxyWrappers
{
    public class SearchServiceProxy : ProxyBase<SearchServiceClient, ISearchService>, IDisposable
    {
        public SearchServiceProxy()
            : base() {            
        }

        public SearchServiceProxy(string endpointConfigurationName)
            : base(endpointConfigurationName) {
        }

        public SearchServiceProxy(string endpointConfigurationName, string remoteAddress)
            : base(endpointConfigurationName, remoteAddress) {
        }

        public SearchServiceProxy(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress) {
        }

        #region ISearchService Members
        private int _totalResults;
                
        public int TotalResults() {
            return _totalResults;
        }

        public List<Medianet.Contacts.Wcf.DataContracts.DTO.SearchResult> Search(Medianet.Contacts.Wcf.DataContracts.DTO.SearchOptions so, Medianet.Contacts.Wcf.DataContracts.DTO.SortColumn sortCol, Medianet.Contacts.Wcf.DataContracts.DTO.SortDirection sortDir, bool newSearch, int userid, int? startPos, int? count, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> pinned, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> deleted, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> selected)
        {
            SearchResult[] returnValue = new SearchResult[] {};
            var _pinned = new List<RecordIdentifier>();
            var _deleted = new List<RecordIdentifier>();
            var _selected = new List<RecordIdentifier>();

            try
            {
                if (pinned != null)
                {
                    _pinned = pinned;
                }

                if (deleted != null)
                {
                    _deleted = deleted;
                }

                if (selected != null)
                {
                    _selected = selected;
                }

                returnValue = Proxy.Search(so, sortCol, sortDir, newSearch, userid, startPos, count, _pinned.ToArray(), _deleted.ToArray(), _selected.ToArray(), out _totalResults);
            }
            catch (TimeoutException to) {
                Debug.WriteLine(to.Message);
            }
            catch (CommunicationException comEx) {
                // was this a FaultException? If so, user should see it, if not, user should see it unless we enabled retries for communication exceptions
                FaultException faultEx = comEx as FaultException;
                if (faultEx != null || !this.RetryAfterException) {
                    // user should be informed about this error
                    throw;
                }

                // if it wasn't a FaultException, it could have been a timeout or other communication exception, try again with a new channel and suppress the error
                returnValue = Proxy.Search(so, sortCol, sortDir, newSearch, userid, startPos, count, _pinned.ToArray(), _deleted.ToArray(), _selected.ToArray(), out _totalResults);
            }
            return returnValue.ToList();
        }

        public List<Medianet.Contacts.Wcf.DataContracts.DTO.SearchResult> SearchWithExpandedOutlets(Medianet.Contacts.Wcf.DataContracts.DTO.SearchOptions SearchOptions, Medianet.Contacts.Wcf.DataContracts.DTO.SortColumn sortColumn, Medianet.Contacts.Wcf.DataContracts.DTO.SortDirection sortDirection, int userid, int? startPos, int? count, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> pinned, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> deleted, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> unselected) {

            SearchResult[] returnValue;
            var _pinned = new List<RecordIdentifier>();
            var _deleted = new List<RecordIdentifier>();
            var _unselected = new List<RecordIdentifier>();
            try
            {
                if (pinned != null)
                {
                    _pinned = pinned;
                }

                if (deleted != null)
                {
                    _deleted = deleted;
                }

                if (unselected != null)
                {
                    _unselected = unselected;
                }

                returnValue = Proxy.SearchWithExpandedOutlets(SearchOptions, sortColumn, sortDirection, userid, startPos,
                    count, _pinned.ToArray(), _deleted.ToArray(), _unselected.ToArray(), out _totalResults);

            }
            catch (CommunicationException comEx) {
                // was this a FaultException? If so, user should see it, if not, user should see it unless we enabled retries for communication exceptions
                FaultException faultEx = comEx as FaultException;
                if (faultEx != null || !this.RetryAfterException) {
                    // user should be informed about this error
                    throw;
                }

                // if it wasn't a FaultException, it could have been a timeout or other communication exception, try again with a new channel and suppress the error
                returnValue = Proxy.SearchWithExpandedOutlets(SearchOptions, sortColumn, sortDirection, userid, startPos,
                    count, _pinned.ToArray(), _deleted.ToArray(), _unselected.ToArray(), out _totalResults);
            }

            return returnValue.ToList();
        }

        public List<Medianet.Contacts.Wcf.DataContracts.DTO.SearchResult> FetchRange(List<Medianet.Contacts.Wcf.DataContracts.DTO.SearchResult> results, int startPos, int count, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> pinned, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> deleted, List<Medianet.Contacts.Wcf.DataContracts.DTO.RecordIdentifier> unselected, int userid) {

            SearchResult[] returnValue;
            var _pinned = new List<RecordIdentifier>();
            var _deleted = new List<RecordIdentifier>();
            var _unselected = new List<RecordIdentifier>();

            try
            {
                if (pinned != null)
                {
                    _pinned = pinned;
                }

                if (deleted != null)
                {
                    _deleted = deleted;
                }

                if (unselected != null)
                {
                    _unselected = unselected;
                }

                returnValue = Proxy.FetchRange(results.ToArray(), startPos, count, _pinned.ToArray(), _deleted.ToArray(),
                    _unselected.ToArray(), userid, out _totalResults);
            }
            catch (CommunicationException comEx) {
                // was this a FaultException? If so, user should see it, if not, user should see it unless we enabled retries for communication exceptions
                FaultException faultEx = comEx as FaultException;
                if (faultEx != null || !this.RetryAfterException) {
                    // user should be informed about this error
                    throw;
                }

                // if it wasn't a FaultException, it could have been a timeout or other communication exception, try again with a new channel and suppress the error
                returnValue = Proxy.FetchRange(results.ToArray(), startPos, count, _pinned.ToArray(), _deleted.ToArray(),
                    _unselected.ToArray(), userid, out _totalResults);

            }
            return returnValue.ToList();
        }

        public List<Medianet.Contacts.Wcf.DataContracts.DTO.AutoSuggest> AutoSuggest(SearchContext context, string searchText, bool searchAustralia, bool searchNewZealand, bool searchAllCountries, int maxResults, int userid) {
            AutoSuggest[] returnValue;
            try
            {
                returnValue = Proxy.AutoSuggest(context, searchText, searchAustralia, searchNewZealand,
                    searchAllCountries, maxResults, userid);

            }
            catch (CommunicationException comEx) {
                // was this a FaultException? If so, user should see it, if not, user should see it unless we enabled retries for communication exceptions
                FaultException faultEx = comEx as FaultException;
                if (faultEx != null || !this.RetryAfterException) {
                    // user should be informed about this error
                    throw;
                }

                // if it wasn't a FaultException, it could have been a timeout or other communication exception, try again with a new channel and suppress the error
                returnValue = Proxy.AutoSuggest(context, searchText, searchAustralia, searchNewZealand,
                    searchAllCountries, maxResults, userid);
            }

            return returnValue?.ToList();
        }

        #endregion
    }
}