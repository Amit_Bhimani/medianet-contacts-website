﻿// © 2007 Michele Leroux Bustamante. All rights reserved 
// Book: Learning WCF, O'Reilly
// Book Blog: www.thatindigogirl.com
// Michele's Blog: www.dasblonde.net
// IDesign: www.idesign.net

using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace Medianet.Contacts.Web.ProxyWrappers
{
    public abstract class ProxyBase<TProxy, TChannel> : IDisposable
        where TProxy : ClientBase<TChannel>, new()
        where TChannel : class
    {
        private TProxy _Proxy;
        public TProxy Proxy {
            get {
                if (((_Proxy != null))) {
                    return _Proxy;
                }
                else {
                    throw new ObjectDisposedException("Service Proxy helper");
                }
            }
        }

        private object m_channelLock = new object();
        private ChannelFactory<TChannel> m_innerChannelFactory = null;
        private TChannel m_innerChannel = default(TChannel);

        public bool RetryAfterException { get; set; }
        public bool RecreateOnFault { get; set; }

        /// <summary>
        /// Use this for normal service proxy behaviour instead of advanced behaviour.
        /// With this one you use Proxy instead of InnerChannel.
        /// </summary>
        public ProxyBase() {
            _Proxy = new TProxy();
        }

        public ProxyBase(string endpointConfigurationName)
            : this(endpointConfigurationName, true, true) {
        }

        public ProxyBase(string endpointConfigurationName, bool retryAfterException, bool recreateOnFault) {
            this.RetryAfterException = retryAfterException;
            this.RecreateOnFault = recreateOnFault;

            lock (m_channelLock) {
                m_innerChannelFactory = new ChannelFactory<TChannel>(endpointConfigurationName);

                foreach (var op in m_innerChannelFactory.Endpoint.Contract.Operations) {
                    DataContractSerializerOperationBehavior dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>() as DataContractSerializerOperationBehavior;
                    if (dataContractBehavior != null) {
                        dataContractBehavior.MaxItemsInObjectGraph = 2147483647;
                    }
                }
            }
        }

        public ProxyBase(string endpointConfigurationName, string remoteAddress)
            : this(endpointConfigurationName, remoteAddress, true, true) {

        }

        public ProxyBase(string endpointConfigurationName, string remoteAddress, bool retryAfterException, bool recreateOnFault) {

            this.RetryAfterException = retryAfterException;
            this.RecreateOnFault = recreateOnFault;

            lock (m_channelLock) {
                m_innerChannelFactory = new ChannelFactory<TChannel>(endpointConfigurationName, new EndpointAddress(remoteAddress));
            }
        }

        public ProxyBase(Binding binding, EndpointAddress remoteAddress)
            : this(binding, remoteAddress, true, true) {
        }

        public ProxyBase(Binding binding, EndpointAddress remoteAddress, bool retryAfterException, bool recreateOnFault) {
            this.RetryAfterException = retryAfterException;
            this.RecreateOnFault = recreateOnFault;

            lock (m_channelLock) {
                m_innerChannelFactory = new ChannelFactory<TChannel>(binding, remoteAddress);
            }
        }

        public CommunicationState State {
            get {
                return ((ICommunicationObject)m_innerChannel).State;
            }
        }

        protected TChannel InnerChannel {
            get {
                lock (m_channelLock) {
                    if (!object.Equals(m_innerChannel, default(TChannel))) {
                        if (this.State == CommunicationState.Faulted && RecreateOnFault) {
                            // channel has been faulted, we want to create a new one so clear it
                            m_innerChannel = default(TChannel);
                        }
                    }

                    if (object.Equals(m_innerChannel, default(TChannel))) {
                        // channel is null, create a new one
                        Debug.Assert(m_innerChannelFactory != null);
                        m_innerChannel = m_innerChannelFactory.CreateChannel();

                    }
                }


                return m_innerChannel;
            }

        }

        #region "IDisposable Support"
        // To detect redundant calls
        private bool disposedValue;

        // IDisposable
        protected virtual void Dispose(bool disposing) {
            if (!this.disposedValue) {
                if (disposing) {
                    // TODO: dispose managed state (managed objects).
                    try {
                        if ((_Proxy != null)) {
                            if (_Proxy.State != CommunicationState.Faulted) {
                                _Proxy.Close();
                            }
                            else {
                                _Proxy.Abort();
                            }
                        }
                    }
                    catch (CommunicationException) {
                        if ((_Proxy != null))
                            _Proxy.Abort();
                    }
                    catch (TimeoutException) {
                        if ((_Proxy != null))
                            _Proxy.Abort();
                    }
                    catch (Exception ex) {
                        if ((_Proxy != null))
                            _Proxy.Abort();
                        throw ex;
                    }
                    finally {
                        _Proxy = null;
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                // TODO: set large fields to null.
            }
            this.disposedValue = true;
        }

        public void Dispose() {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
