﻿using System;
using System.Collections.Generic;
using Medianet.Contacts.Web.MediaContactsService;
using System.Linq;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using SortColumn = Medianet.Contacts.Web.MediaContactsService.SortColumn;
using SortDirection = Medianet.Contacts.Web.MediaContactsService.SortDirection;
using System.IO;
using Medianet.Contacts.Web.Helpers;
using Medianet.Contacts.Web.ViewModels.Dashboard;

namespace Medianet.Contacts.Web.ProxyWrappers
{
    public class MediaContactsServiceProxy : ProxyBase<MediaContactsServiceClient, IMediaContactsService>
    {
        public MediaContactsServiceProxy()
            : base()
        {
        }

        public List<MediaContactGroup> GetGroups(int userId, string sessionKey)
        {
            return Proxy.GetMediaContactGroups(userId, sessionKey).ToList();
        }

        public List<MediaContactGroup> GetGroupsByType(MediaContactGroupType type, int userId, string sessionKey)
        {
            return Proxy.GetMediaContactGroupsByType(type, userId, sessionKey).ToList();
        }

        public int AddGroup(MediaContactGroup group, string sessionKey)
        {
            return Proxy.AddMediaContactGroup(group, sessionKey);
        }

        public List<MediaContactList> GetLists(int userId, string sessionKey)
        {
            return Proxy.GetMediaContactLists(userId, 0, sessionKey).ToList();
        }

        public List<MediaContactList> GetRecentLists(int userId, string sessionKey)
        {
            return Proxy.GetMediaContactLists(userId, 6, sessionKey).ToList();
        }

        public MediaContactList GetList(int listId, string sessionKey)
        {
            return Proxy.GetMediaContactList(listId, sessionKey);
        }

        public List<MediaContactListVersion> GetListVersions(int id, string sessionKey)
        {
            return Proxy.GetMediaContactListVersions(id, sessionKey).ToList();
        }

        public void RestoreListSet(int listId, int listSetId, int userId, string sessionKey)
        {
            Proxy.RestoreMediaContactListVersion(listId, listSetId, userId, sessionKey);
        }

        public List<MediaContactLivingListContactReplacement> GetLivingListContactReplacements(string sessionKey, int userId, int listId, int maxRows)
        {
            return Proxy.GetMediaContactLivingListContactReplacement(sessionKey, userId, listId, maxRows).ToList();
        }

        public void LivingListAccept(int recordId, string sessionKey)
        {
            Proxy.LivingListAccept(recordId, sessionKey);
        }

        public void LivingListReject(int recordId, string sessionKey)
        {
            Proxy.LivingListReject(recordId, sessionKey);
        }
        public void LivingListAcceptRejectAll(int listId, string sessionKey)
        {
            Proxy.LivingListAcceptRejectAll(listId, sessionKey);
        }

        public Dictionary<string, string> GetLivingListOutletContactsByRecordId(int recordId, int maxRow, string sessionKey)
        {
            return Proxy.GetLivingListOutletContactsByRecordId(recordId, maxRow, sessionKey);
        }

        public void LivingListReplaceContact(int recordId, string newContactId, string sessionKey)
        {
            Proxy.LivingListReplaceContact(recordId, newContactId, sessionKey);
        }

        public List<MediaContactTask> GetTasks(int userId, string sessionKey)
        {
            return Proxy.GetMediaContactTasks(userId, sessionKey).ToList();
        }

        public List<MediaContactSavedSearch> GetSavedSearches(int userId, string sessionKey)
        {
            return Proxy.GetMediaContactSavedSearches(userId, 0, sessionKey).ToList();
        }

        public List<MediaContactSavedSearch> GetRecentSavedSearches(int userId, string sessionKey)
        {
            return Proxy.GetMediaContactSavedSearches(userId, 6, sessionKey).ToList();
        }

        public MediaContactSavedSearch GetSavedSearch(int userId, string sessionKey)
        {
            return Proxy.GetMediaContactSavedSearch(userId, sessionKey);
        }

        public MediaContactsExport GetExport(int exportId, string sessionKey)
        {
            return Proxy.GetExport(exportId, sessionKey);
        }

        public List<MediaContactsExport> GetExports(string debtorNumber, int? userId, MediaContactsExportType type, string sessionKey)
        {
            return Proxy.GetExports(debtorNumber, userId, type, sessionKey).ToList();
        }

        public int AddExportProfile(MediaContactsExport export, string sessionKey)
        {
            return Proxy.AddExports(export, sessionKey);
        }

        public void UpdateExportProfile(MediaContactsExport export, string sessionKey)
        {
            Proxy.UpdateExports(export, sessionKey);
        }

        public void DeleteExportProfile(int exportId, string sessionKey)
        {
            Proxy.DeleteExports(exportId, sessionKey);
        }

        public MediaContactPaginatedListRecordDetails GetPaginatedRecordsByListsetAndListId(int listId, int listSet,
                                                                                          int pageNumber,
                                                                                          int recordsPerPage,
                                                                                          SortColumn pSortColumn,
                                                                                          SortDirection pSortDirection,
                                                                                          int userId, string sessionKey)
        {
            return Proxy.GetPaginatedRecordsByListsetAndListId(listId, listSet, pageNumber, recordsPerPage, pSortColumn,
                                                        pSortDirection, userId, sessionKey);
        }

        public void DeleteItemsFromList(int listId, List<int> listItemsToDelete, string sessionKey)
        {
            Proxy.DeleteFromList(listId, listItemsToDelete, sessionKey);
        }

        public void AddToList(int listId, int userId, List<string> listItemsToAdd, bool createNewVersion, string sessionKey)
        {
            Proxy.AddToList(listId, userId, listItemsToAdd, createNewVersion, sessionKey);
        }

        public int LogSearch(MediaContactSearchLog entity, string sessionKey)
        {
            return Proxy.MediaContactSearchLog(entity, sessionKey);
        }

        public List<MediaContactSearchLog> GetRecentSearchList(int maxRecords, string sessionKey)
        {
            return Proxy.GetRecentSearchList(maxRecords, sessionKey);
        }

        public MediaContactSearchLog GetRecentSearchById(int searchLogId, string sessionKey)
        {
            return Proxy.GetRecentSearchById(searchLogId, sessionKey);
        }

        public int LogExport(MediaContactExportLog entity, string sessionKey)
        {
            return Proxy.MediaContactExportLog(entity, sessionKey);
        }

        public List<MediaMovement> GetMediaMovements(bool includeHidden, int pageNo, int pageSize, string sessionKey)
        {
            int totalCount;
            int totalPinned;
            return Proxy.GetMediaMovements(includeHidden, pageNo, pageSize, sessionKey, out totalCount, out totalPinned);
        }

        public List<MediaMovement> GetOutletMediaMovements(string outletId, int pageNo, int pageSize, string sessionKey)
        {
            return Proxy.GetOutletMediaMovements(outletId, pageNo, pageSize, sessionKey);
        }
        public List<MediaMovement> GetContactMediaMovements(string contactId, int pageNo, int pageSize, string sessionKey)
        {
            return Proxy.GetContactMediaMovements(contactId, pageNo, pageSize, sessionKey);
        }

        public List<Note> GetOutletNotes(string outletId, string recordType, string sessionKey)
        {
            return Proxy.GetOutletNotes(outletId, recordType, sessionKey).ToList();
        }

        public List<Note> GetContactNotes(string outletId, string contactId, string recordType, string sessionKey)
        {
            return Proxy.GetContactNotes(outletId, contactId, recordType, sessionKey).ToList();
        }

        public void DeleteNote(int id, string sessionKey)
        {
            Proxy.DeleteNote(id, sessionKey);
        }

        public void PinUnpinNote(int id, bool pinned, string sessionKey)
        {
            Proxy.PinUnpinNote(id, pinned, sessionKey);
        }

        public int AddNote(Note note, string sessionKey)
        {
            return Proxy.AddNote(note, sessionKey);
        }

        public void UpdateNote(Note note, string sessionKey)
        {
            Proxy.UpdateNote(note, sessionKey);
        }

        public void AddNoteBulk(NotesBulk note, string sessionKey)
        {
            Proxy.AddNoteBulk(note, sessionKey);
        }

        public Note GetNote(int id, string sessionKey)
        {
            return Proxy.GetNote(id, sessionKey);
        }

        public List<MediaOutletPartial> GetRelatedMediaOutlets(string outletId, string sessionKey)
        {
            return Proxy.GetRelatedMediaOutlets(outletId, sessionKey).ToList();
        }

        public List<MediaOutletPartial> GetSyndicatingToMediaOutlets(string outletId, string sessionKey)
        {
            return Proxy.GetSyndicatingToMediaOutlets(outletId, sessionKey).ToList();
        }

        public List<MediaOutletPartial> GetSyndicatedFromMediaOutlets(string outletId, string sessionKey)
        {
            return Proxy.GetSyndicatedFromMediaOutlets(outletId, sessionKey).ToList();
        }

        public List<MediaOutletPartial> GetMediaOwnerBusinesses(string outletId, string sessionKey)
        {
            return Proxy.GetMediaOwnerBusinesses(outletId, sessionKey).ToList();
        }

        public Stream GetOutletLogo(string logoFileName, string sessionKey)
        {
            return Proxy.GetOutletLogo(false, ref logoFileName, sessionKey);
        }

        public void LogMediaContactView(string contactId, string outletId, int userId, string sessionKey)
        {
            Proxy.LogMediaContactView(new MediaContactViewDto()
            {
                ContactId = contactId,
                OutletId = outletId,
                UserId = userId,
                DateViewed = DateTime.Now,
                IpAddress = GeneralHelper.GetIpAddress()
            }, sessionKey);
        }

        public void LogMediaOutletView(string outletId, int userId, string sessionKey)
        {
            Proxy.LogMediaOutletView(new MediaOutletViewDto()
            {
                OutletId = outletId,
                UserId = userId,
                DateViewed = DateTime.Now,
                IpAddress = GeneralHelper.GetIpAddress()
            }, sessionKey);
        }

        public void LogPrnContactView(string contactId, string outletId, int userId, string sessionKey)
        {
            Proxy.LogPrnContactView(new PrnContactViewDto()
            {
                ContactId = Convert.ToDecimal(contactId),
                OutletId = Convert.ToDecimal(outletId),
                UserId = userId,
                DateViewed = DateTime.Now,
                IpAddress = GeneralHelper.GetIpAddress()
            }, sessionKey);
        }

        public List<MediaContactAlsoViewedDto> GetContactViewSummary(string contactId, string outletId, List<int> subjectCodeIds, List<int> continentIds, int maxCount, string sessionKey)
        {
            return Proxy.GetContactViewSummary(contactId, outletId, subjectCodeIds, continentIds, maxCount, sessionKey).ToList();
        }

        public OmaDocument GetDocumentById(int documentId, string sessionKey)
        {
            return Proxy.GetDocumentById(documentId, sessionKey);
        }

        public List<OmaDocument> GetDocuments(string outletId, string contactId, Wcf.DataContracts.DTO.RecordType recordType, string sessionKey)
        {
            return Proxy.GetDocuments(outletId, contactId, (int)recordType, sessionKey).ToList();
        }

        public void SaveOmaDocument(OmaDocument entity, string sessionKey)
        {
            Proxy.SaveOmaDocument(entity, sessionKey);
        }

        public void DeleteOmaDocument(int documentId, string sessionKey)
        {
            Proxy.DeleteOmaDocument(documentId, sessionKey);
        }

        public OmaDocument GetDocumentFile(int documentId, string sessionKey)
        {
            return Proxy.GetDocumentFile(documentId, sessionKey);
        }

        public List<int> GetOmaContactIds(int outletId, string sessionKey)
        {
            return Proxy.GetOmaContactIds(outletId, sessionKey);
        }

        public List<MediaContactRecentChange> GetMediaContactRecentlyUpdated(int total, string sessionKey)
        {
            return Proxy.GetMediaContactRecentlyUpdated(total, sessionKey);
        }

        public List<MediaContactRecentChange> GetMediaContactRecentlyAdded(int total, string sessionKey)
        {
            return Proxy.GetMediaContactRecentlyAdded(total, sessionKey);
        }
    }
}