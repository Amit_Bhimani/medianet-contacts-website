﻿using Medianet.Contacts.Web.GeneralService;

namespace Medianet.Contacts.Web.ProxyWrappers
{
    public class GeneralServiceProxy : ProxyBase<GeneralServiceClient, IGeneralService>
    {
        public GeneralServiceProxy()
            : base() {            
        }

        public MediaStatistics GetMediaStatistics() {
            return Proxy.GetMediaStatistics();
        }
    }
}