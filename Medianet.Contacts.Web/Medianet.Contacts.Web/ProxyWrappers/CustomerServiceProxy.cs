﻿using System.Collections.Generic;
using System.Linq;
using Medianet.Contacts.Web.CustomerService;

namespace Medianet.Contacts.Web.ProxyWrappers
{
    public class CustomerServiceProxy : ProxyBase<CustomerServiceClient, ICustomerService>
    {
        public CustomerServiceProxy()
            : base() {            
        }

        public DBSession Login(string username, string companyName, string password) {
            return Proxy.Login(username, companyName, password, SystemType.Contacts);
        }

        public DBSession ValidateSession(string sessionKey) {
            return Proxy.ValidateSession(sessionKey);
        }

        public DBSession LoginUsingEmailAddress(string emailAddress, string password) {
            return Proxy.LoginUsingEmailAddress(emailAddress, password, SystemType.Contacts);
        }

        public DBSession LoginUsingOtherSession(string sessionKey) {
            return Proxy.LoginUsingSession(sessionKey, SystemType.Contacts);
        }

        public DBSession LoginReplacingSession(string sessionKey) {
            return Proxy.LoginReplacingSession(sessionKey, SystemType.Contacts);
        }

        public void SendForgotPasswordEmail(string emailAddress)
        {
            Proxy.SendForgotPasswordEmail(emailAddress, SystemType.Contacts);
        }

        public DBSession ChangePassword(string password, string sessionKey) {
            return Proxy.ChangePassword(password, sessionKey);
        }

        public void Logout(string sessionKey) {
            Proxy.Logout(sessionKey);
        }

        public DBSession AcknowledgeTermsReadAndLogin(string emailAddress, string password)
        {
            return Proxy.AcknowledgeTermsReadAndLogin(emailAddress, password, SystemType.Contacts);
        }

        public List<AAPContract> GetCustomerContracts(string debtorNumber)
        {
            return Proxy.GetCustomerContracts(debtorNumber).ToList();

        }

        public SalesForceContact GetSalesForceContact(string emailAddress, string debtorNumber)
        {
            return Proxy.GetUserProfile(emailAddress, debtorNumber);
        }
    }
}