﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Medianet.Contacts.Wcf.DataContracts.DTO;
using Medianet.Contacts.Web.SolrService;

namespace Medianet.Contacts.Web.ProxyWrappers
{
    public class SolrServiceProxy : ProxyBase<SolrServiceClient, ISolrService>, IDisposable
    {
        public SolrServiceProxy()
            : this("") {            
        }

        public SolrServiceProxy(string endpointConfigurationName)
            : base(endpointConfigurationName) {
        }

        public SolrServiceProxy(string endpointConfigurationName, string remoteAddress)
            : base(endpointConfigurationName, remoteAddress) {
        }

        public SolrServiceProxy(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress) {
        }

        #region ISolrService Members
      

        public bool UpdateContact(RecordIdentifier record) {
            bool returnValue = false;

            try {
                returnValue = this.InnerChannel.UpdateContact(record);
            }
            catch (TimeoutException to) {
                Debug.WriteLine(to.Message);
            }
            catch (CommunicationException comEx) {
                // was this a FaultException? If so, user should see it, if not, 
                // user should see it unless we enabled retries for communication exceptions
                FaultException faultEx = comEx as FaultException;
                if (faultEx != null || !this.RetryAfterException) {
                    // user should be informed about this error
                    throw;
                }

                // if it wasn't a FaultException, it could have been a timeout
                // or other communication exception, try again with a new channel and suppress the error
                returnValue = this.InnerChannel.UpdateContact(record);
            }

            return returnValue;
        }


        #endregion
    }
}