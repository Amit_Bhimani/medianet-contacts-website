﻿using System.Web.Mvc;
using Medianet.Contacts.Web.Controllers;

namespace Medianet.Contacts.Web.Filters
{
    /// <summary>
    /// Saves the search session items after the action is executed.
    /// </summary>
    public class SavesSession : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            BaseController controller = filterContext.Controller as BaseController;
            //var session = filterContext.HttpContext.Session;

            if (controller != null)
                controller.SaveSessionItems();

            base.OnActionExecuted(filterContext);
        }
    }
}