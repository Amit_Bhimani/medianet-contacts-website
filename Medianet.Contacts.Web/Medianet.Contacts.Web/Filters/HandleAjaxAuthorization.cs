﻿using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Controllers;
using Medianet.Contacts.Web.Common.Logging;
using System.Net;

namespace Medianet.Contacts.Web.Filters
{
    public class HandleAjaxAuthorization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!SessionManager.Instance.IsUserLoggedIn())
            {
                context.Result = new HttpUnauthorizedResult();
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
            }

            base.OnActionExecuting(context);
        }

    }
}
