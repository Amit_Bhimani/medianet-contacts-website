﻿using System;
using System.Configuration;
using System.Net;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common.Logging;
using System.Text.RegularExpressions;
using Medianet.Contacts.Web.Controllers;
using Medianet.Contacts.Web.Common;

namespace Medianet.Contacts.Web.Filters
{
    public class HandleHtmlError : HandleErrorAttribute
    {
        public string Message { get; set; }

        public override void OnException(ExceptionContext context)
        {
            if (context.ExceptionHandled)
                return;

            // Log the exception.
            MPLogger.LogException(context);

            context.ExceptionHandled = true;

            if (Regex.IsMatch(context.Exception.Message, "SessionKey (.*?) is not valid."))
            {
                SessionManager.Instance.LogOff();
                context.Result = new RedirectResult(AccountController.InvalidSessionURL);
                
                return;
            }

            // Set the message in the error view.
            var result = new ViewResult() { ViewName = "Error" };

            if (string.IsNullOrWhiteSpace(Message))
                Message = $"If this continues, please contact Client Services on {ConfigurationManager.AppSettings["ClientServicePhoneNo"]} or at " + ConfigurationManager.AppSettings["ClientServiceEmail"] + ".";

            result.ViewBag.Message = Message;

            context.Result = result.ViewBag;
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        }
    }
}