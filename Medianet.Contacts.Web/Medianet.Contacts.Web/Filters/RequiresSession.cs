﻿using System.Web.Mvc;
using Medianet.Contacts.Web.Controllers;

namespace Medianet.Contacts.Web.Filters
{
    /// <summary>
    /// Loads the search session items before the action executes.
    /// </summary>
    public class RequiresSession : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            BaseController controller = filterContext.Controller as BaseController;
            //var session = filterContext.HttpContext.Session;

            if (controller != null)
                controller.GetSessionItems();

            base.OnActionExecuting(filterContext);
        }
    }
}