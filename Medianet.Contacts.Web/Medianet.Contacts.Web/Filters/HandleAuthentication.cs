﻿using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Controllers;

namespace Medianet.Contacts.Web.Filters
{
    public class HandleAuthentication : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!SessionManager.Instance.IsUserLoggedIn())
            {
                filterContext.Result = new RedirectResult(AccountController.InvalidSessionURL);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}