﻿using System.Net;
using System.Web.Mvc;
using Medianet.Contacts.Web.Common;
using Medianet.Contacts.Web.Common.Logging;
using System.Text.RegularExpressions;

namespace Medianet.Contacts.Web.Filters
{
    public class HandleAjaxError : HandleErrorAttribute
    {
        //public string Message { get; set; }

        public override void OnException(ExceptionContext context)
        {
            if (context.ExceptionHandled)
                return;

            // Log the exception.
            MPLogger.LogException(context);

            context.ExceptionHandled = true;
            
            // Set the message in the error view.
            if (context.HttpContext.Session[SessionManager.SessionMember] == null || Regex.IsMatch(context.Exception.Message, "SessionKey (.*?) is not valid."))
            {
                SessionManager.Instance.LogOff();
                context.Result = new HttpUnauthorizedResult();
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
            }
            else
            {
                context.Result = new ContentResult();
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
        }
    }
}