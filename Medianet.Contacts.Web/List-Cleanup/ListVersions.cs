﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NLog;

namespace List_Cleanup
{
    public class ListVersions
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();


        public static void CopyListVersionRecords(int inListSetId)
        {
            var LastListSetIdProcessed = 0;
            var sleepTime = GetSleepTime();

            _logger.Info("Start of Copying Records with listsetId >= " + inListSetId.ToString() + "...");
            _logger.Info(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);

            using (var db = new MedianetDataContext())
            {
                db.ObjectTrackingEnabled = false;
                //get all records(just ListId and ListSet columns),group by listId,order by listId asc,and order listsets in each list(group)
                //db.CommandTimeout = db.CommandTimeout*10;
                List<mn_List_Version> listVersions = null;

                var listSetidsInVersionRecords =
                    (from lv in db.mn_List_Versions_Records select lv.ListSetId).Distinct().ToList();
                if (inListSetId > 0)
                {
                    listVersions =
                        (from lv in db.mn_List_Versions.Where(c => c.ListSetId >= inListSetId).AsEnumerable() orderby lv.ListId, lv.Version, lv.ListSetId select lv).ToList();
                }
                else
                {
                    listVersions =
                        (from lv in db.mn_List_Versions.AsEnumerable() orderby lv.ListId, lv.Version, lv.ListSetId select lv).ToList();
                    
                }

                if (listVersions.Any())
                {
                    var mylistId = listVersions[0].ListId;
                    var listsetId = listVersions[0].ListSetId;
                    var versionNo = listVersions[0].Version;
                    LastListSetIdProcessed = listsetId;
                    try
                    {
                        //iterate through lists

                        var i = 0;
                        var listCount = listVersions.Count;
                        while (i < listCount)
                        {
                            mylistId = listVersions[i].ListId;
                            listsetId = listVersions[i].ListSetId;
                            versionNo = listVersions[i].Version;
                            if (listsetId > LastListSetIdProcessed)
                            {
                                LastListSetIdProcessed = listsetId;
                            }

                            if (listSetidsInVersionRecords.Any(c => c == listsetId))
                            {
                                _logger.Info("Found listsetId " + listsetId.ToString() + " in database already.");
                                _logger.Info(Environment.NewLine +
                                             "-----------------------------------------------------------------------------" +
                                             Environment.NewLine);
                                i += 1;
                                continue;
                            }
                            var listSetModifiedRecords = new List<mn_List_Versions_Record>();
                            var thisVersionrecords = (from lm in db.mn_ListMasters
                                                      where
                                                          lm.ListId == listVersions[i].ListId &&
                                                          lm.ListSet == listVersions[i].ListSetId
                                                      select lm).ToList();

                            if (sleepTime > 0) System.Threading.Thread.Sleep(sleepTime);

                            var nextrecordsListSetId = 0;
                            if ((i + 1) < listCount)
                            {
                                if (listVersions[i].ListId == listVersions[i + 1].ListId)
                                {
                                    if (listVersions[i].Version != listVersions[i + 1].Version)
                                    {
                                        nextrecordsListSetId = listVersions[i + 1].ListSetId;
                                    }
                                    else
                                    {
                                        i = i + 1;
                                        continue;  // just in case a versions are the same... ignore this version
                                    }
                                }
                                else
                                {
                                    // Last list id . compare with current version
                                    nextrecordsListSetId = 0;
                                }
                            }
                            else
                            {
                                // this is the last record. compare with current.
                                nextrecordsListSetId = 0;
                            }
                            var NextVersionrecords = (from lm in db.mn_ListMasters
                                                      where
                                                          lm.ListId == listVersions[i].ListId &&
                                                          lm.ListSet == nextrecordsListSetId
                                                      select lm).ToList();

                            if (sleepTime > 0) System.Threading.Thread.Sleep(sleepTime);

                            listSetModifiedRecords = CompareRecords2(thisVersionrecords, NextVersionrecords, listVersions[i].ListSetId);

                            if (listSetModifiedRecords.Count > 0)
                            {
                                using (var dbUpdate = new MedianetDataContext())
                                {
                                    dbUpdate.mn_List_Versions_Records.InsertAllOnSubmit(listSetModifiedRecords);
                                    dbUpdate.SubmitChanges();
                                }

                                if (sleepTime > 0) System.Threading.Thread.Sleep(sleepTime);

                                _logger.Info(string.Format("{0} records as \"Deleted\" and {1} records as \"Added\" " +
                                                           "are inserted into mn_List_Versions_Records table " +
                                                           "for ListSet = {2}",
                                                           listSetModifiedRecords.Count(m => m.ChangeType == char.Parse("D")),
                                                           listSetModifiedRecords.Count(m => m.ChangeType == char.Parse("A")),
                                                           listVersions[i].ListSetId));

                                _logger.Info(Environment.NewLine +
                                             "-----------------------------------------------------------------------------" +
                                             Environment.NewLine);
                            }

                            i = i + 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(string.Format("An error happened.ListId={0},ListSetId={1} version={2}{3}",mylistId, listsetId, versionNo,Environment.NewLine), ex);
                        _logger.Info(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        _logger.Info(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        _logger.Info(String.Format(" We had a Failure : The Last ListSetID Processed {0} {1}", listsetId, Environment.NewLine));
                        throw;
                    }


                }
                _logger.Info("End of Copying Records...");
                _logger.Info("End of Copying Records...");

                _logger.Info(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                _logger.Info(String.Format(" Last ListsetID Processed {0} {1}", LastListSetIdProcessed, Environment.NewLine));
                _logger.Info(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }

        private static List<mn_List_Versions_Record> CompareRecords2(List<mn_ListMaster> thisListRecords,
                                                                    List<mn_ListMaster> nextListRecords,
                                                                    int listSetId)
        {
            var listSetModifiedRecords = new List<mn_List_Versions_Record>();
            foreach (var thisrecord in thisListRecords)
            {
                if (!nextListRecords.Any(lm => lm.MediaContactId == thisrecord.MediaContactId &&
                                              lm.MediaOutletId == thisrecord.MediaOutletId &&
                                              lm.OmaContactId == thisrecord.OmaContactId &&
                                              lm.OmaOutletId == thisrecord.OmaOutletId &&
                                              lm.PrnContactId == thisrecord.PrnContactId &&
                                              lm.PrnOutletId == thisrecord.PrnOutletId))
                {
                    // record has been deleted. at it to listsetModified
                    listSetModifiedRecords.Add(new mn_List_Versions_Record()
                    {
                        RecordId = thisrecord.RecordId,
                        ChangeType = Convert.ToChar("D"),
                        CreatedDate = thisrecord.CreatedDate,
                        CreatedUserId = thisrecord.UserId.GetValueOrDefault(),
                        ListSetId = listSetId,
                        MediaContactId = thisrecord.MediaContactId,
                        MediaOutletId = thisrecord.MediaOutletId,
                        OmaContactId = thisrecord.OmaContactId,
                        OmaOutletId = thisrecord.OmaOutletId,
                        PrnContactId = thisrecord.PrnContactId,
                        PrnOutletId = thisrecord.PrnOutletId,
                    });
                }
            }
            foreach (var nextrecord in nextListRecords)
            {
                if (!thisListRecords.Any(lm => lm.MediaContactId == nextrecord.MediaContactId &&
                                              lm.MediaOutletId == nextrecord.MediaOutletId &&
                                              lm.OmaContactId == nextrecord.OmaContactId &&
                                              lm.OmaOutletId == nextrecord.OmaOutletId &&
                                              lm.PrnContactId == nextrecord.PrnContactId &&
                                              lm.PrnOutletId == nextrecord.PrnOutletId))
                {
                    // record has been deleted. at it to listsetModified
                    listSetModifiedRecords.Add(new mn_List_Versions_Record()
                    {
                        RecordId = nextrecord.RecordId,
                        ChangeType = Convert.ToChar("A"),
                        CreatedDate = nextrecord.CreatedDate,
                        CreatedUserId = nextrecord.UserId.GetValueOrDefault(),
                        ListSetId = listSetId,
                        MediaContactId = nextrecord.MediaContactId,
                        MediaOutletId = nextrecord.MediaOutletId,
                        OmaContactId = nextrecord.OmaContactId,
                        OmaOutletId = nextrecord.OmaOutletId,
                        PrnContactId = nextrecord.PrnContactId,
                        PrnOutletId = nextrecord.PrnOutletId,
                    });
                }
            }



            return listSetModifiedRecords;
        }


        private static int GetSleepTime()
        {
            int value = 0;

            int.TryParse(ConfigurationManager.AppSettings["SleepTime"], out value);

            return value;
        }
    }
}
