﻿using System;

namespace List_Cleanup
{
    class Program
    {
        static void Main(string[] args)
        {
            var listSetId = 0;
            if (args.Length > 0)
            {
                listSetId = Convert.ToInt32(args[0].ToLower());
            }

            ListVersions.CopyListVersionRecords(listSetId);
        }
    }
}
