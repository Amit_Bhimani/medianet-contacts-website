using System;

namespace Medianet.Contacts.Reporting
{
    /// <summary>
    /// Summary description for AveryL7159.
    /// </summary>
    public partial class AveryL7159 : BaseReport
    {
        public AveryL7159()
        {
            InitializeComponent();
        }

        private void AveryL7159_NeedDataSource(object sender, EventArgs e)
        {
            try
            {
                var report = (Telerik.Reporting.Processing.Report)sender;

                int myCheckboxState = 0;
                if (report.Parameters["CheckboxState"].Value != null)
                    Int32.TryParse(report.Parameters["CheckboxState"].Value.ToString(), out myCheckboxState);
                string myCheckedItems = report.Parameters["CheckedItems"].Value == null ? "" : report.Parameters["CheckedItems"].Value.ToString();
                string myUncheckedItems = report.Parameters["UncheckedItems"].Value == null ? "" : report.Parameters["UncheckedItems"].Value.ToString();

                this.DataSource = GetMailingLabelData(Convert.ToInt32(report.Parameters["list"].Value),
                                                      Convert.ToInt32(report.Parameters["UserID"].Value),
                                                      myCheckboxState,
                                                      myCheckedItems,
                                                      myUncheckedItems);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}