namespace Medianet.Contacts.Reporting
{
    partial class AveryDL30
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            this.detail = new Telerik.Reporting.DetailSection();
            this.nameDataTextBox = new Telerik.Reporting.TextBox();
            this.outletNameDataTextBox = new Telerik.Reporting.TextBox();
            this.addressLine1DataTextBox = new Telerik.Reporting.TextBox();
            this.citydetailsDataTextBox = new Telerik.Reporting.TextBox();
            this.addressLine2DataTextBox = new Telerik.Reporting.TextBox();
            this.countryDataTextBox = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnCount = 3;
            this.detail.ColumnSpacing = new Telerik.Reporting.Drawing.Unit(4D, Telerik.Reporting.Drawing.UnitType.Mm);
            this.detail.Height = new Telerik.Reporting.Drawing.Unit(25.408987045288086D, Telerik.Reporting.Drawing.UnitType.Mm);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.nameDataTextBox,
            this.outletNameDataTextBox,
            this.addressLine1DataTextBox,
            this.citydetailsDataTextBox,
            this.addressLine2DataTextBox,
            this.countryDataTextBox});
            this.detail.Name = "detail";
            // 
            // nameDataTextBox
            // 
            this.nameDataTextBox.CanGrow = false;
            this.nameDataTextBox.CanShrink = false;
            this.nameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.nameDataTextBox.Name = "nameDataTextBox";
            this.nameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.7999999523162842D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.nameDataTextBox.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.nameDataTextBox.Value = "=Fields.ContactName";
            // 
            // outletNameDataTextBox
            // 
            this.outletNameDataTextBox.CanGrow = false;
            this.outletNameDataTextBox.CanShrink = false;
            this.outletNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(5.8000001907348633D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.outletNameDataTextBox.Name = "outletNameDataTextBox";
            this.outletNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.7999999523162842D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.outletNameDataTextBox.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.outletNameDataTextBox.Value = "=Fields.Outletname";
            // 
            // addressLine1DataTextBox
            // 
            this.addressLine1DataTextBox.CanGrow = false;
            this.addressLine1DataTextBox.CanShrink = false;
            this.addressLine1DataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(9.6000003814697266D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.addressLine1DataTextBox.Multiline = true;
            this.addressLine1DataTextBox.Name = "addressLine1DataTextBox";
            this.addressLine1DataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.7999999523162842D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.addressLine1DataTextBox.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.addressLine1DataTextBox.Value = "=Fields.Address1";
            // 
            // citydetailsDataTextBox
            // 
            this.citydetailsDataTextBox.CanGrow = false;
            this.citydetailsDataTextBox.CanShrink = false;
            this.citydetailsDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(17.200000762939453D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.citydetailsDataTextBox.Name = "citydetailsDataTextBox";
            this.citydetailsDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.7999999523162842D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.citydetailsDataTextBox.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.citydetailsDataTextBox.Value = "=Fields.Details";
            // 
            // addressLine2DataTextBox
            // 
            this.addressLine2DataTextBox.CanGrow = false;
            this.addressLine2DataTextBox.CanShrink = true;
            this.addressLine2DataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(13.399999618530273D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.addressLine2DataTextBox.Name = "addressLine2DataTextBox";
            this.addressLine2DataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.7999999523162842D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.addressLine2DataTextBox.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.addressLine2DataTextBox.Value = "=Fields.Address2";
            // 
            // countryDataTextBox
            // 
            this.countryDataTextBox.CanGrow = false;
            this.countryDataTextBox.CanShrink = false;
            this.countryDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(21D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.countryDataTextBox.Name = "countryDataTextBox";
            this.countryDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.7999999523162842D, Telerik.Reporting.Drawing.UnitType.Mm));
            this.countryDataTextBox.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.countryDataTextBox.Value = "=Fields.Country";
            // 
            // AveryDL30
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(20.600000381469727D, Telerik.Reporting.Drawing.UnitType.Mm);
            this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(4D, Telerik.Reporting.Drawing.UnitType.Mm);
            this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(5.9900050163269043D, Telerik.Reporting.Drawing.UnitType.Mm);
            this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(20.600000381469727D, Telerik.Reporting.Drawing.UnitType.Mm);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.Name = "list";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "= Parameters.List.Value";
            reportParameter2.Name = "Listset";
            reportParameter2.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter2.Value = "= Parameters.Listset.Value";
            reportParameter3.Name = "UserID";
            reportParameter3.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter3.Value = "= Parameters.UserID.Value";
            reportParameter4.Name = "CheckboxState";
            reportParameter4.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter4.Value = "= Parameters.CheckboxState.Value";
            reportParameter5.Name = "CheckedItems";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.String;
            reportParameter5.Value = "= Parameters.CheckedItems.Value";
            reportParameter6.Name = "UncheckedItems";
            reportParameter6.Type = Telerik.Reporting.ReportParameterType.String;
            reportParameter6.Value = "= Parameters.UncheckedItems.Value";
            reportParameter7.Name = "MaxRowsToDisplay";
            reportParameter7.Type = Telerik.Reporting.ReportParameterType.String;
            reportParameter7.Value = "= Parameters.MaxRowsToDisplay.Value";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = new Telerik.Reporting.Drawing.Unit(64.00799560546875D, Telerik.Reporting.Drawing.UnitType.Mm);
            this.NeedDataSource += new System.EventHandler(this.AveryDL30_NeedDataSource);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox nameDataTextBox;
        private Telerik.Reporting.TextBox outletNameDataTextBox;
        private Telerik.Reporting.TextBox addressLine1DataTextBox;
        private Telerik.Reporting.TextBox citydetailsDataTextBox;
        private Telerik.Reporting.TextBox addressLine2DataTextBox;
        private Telerik.Reporting.TextBox countryDataTextBox;

    }
}