﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Medianet.Contacts.Reporting
{
    /// <summary>
    /// Summary description for DBClass
    /// </summary>
    public class DBClass
    {
        // private static bool isDeveloper = false;
        private WebDataClient.AAP.WebDataClient.MsgProtocol _WebDataClient;
        private string _DatabaseAlias;
        private string _error;

        public DBClass()
        {
            ConnectToDatabase();
        }

        public void CloseDBConnection()
        {
            if (!(_WebDataClient == null))
                if (_WebDataClient.IsConnected) _WebDataClient.Disconnect();
        }
        /// <summary>
        /// This method fires Select Query or Procedure of select query.   
        /// </summary>
        /// <param name="query">This parameter takes the query or procedure name string</param>
        /// <param name="isStoredProcedure">This parameter checks whether it is query or procedure</param>
        /// <returns>@tbl</returns>

        public DataTable SelectQuery(string query, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                if (ds.Tables.Count == 0)
                    throw new Exception("No table returned.");

                return ds.Tables[0];
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This query returns the single record
        /// </summary>
        /// <param name="query">String parameter takes query or stored procedures name.</param>
        /// <param name="isStoredProcedure">Bool parameter checked whether it is procedure or query </param>
        /// <returns>@record</returns>

        public Object SelectScalarQuery(string query, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return null;

                // Return the first column of the first row of the first table.
                return (object)ds.Tables[0].Rows[0][0];
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This query returns the single record
        /// </summary>
        /// <param name="query">String parameter takes query or stored procedures name.</param>
        /// <param name="sqlParams">Parameter for fetching single record</param>
        /// <param name="isStoredProcedure">Bool parameter checked whether it is procedure or query </param>
        /// <returns>@record</returns>

        public Object SelectScalarQuery(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");


                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return null;

                // Return the first column of the first row of the first table.
                return (object)ds.Tables[0].Rows[0][0];
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This is method overriden method of SelectQuery where we can set the Sql parameters for where clause.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@tbl</returns>

        public DataTable SelectQuery(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                if (ds.Tables.Count == 0)
                    throw new Exception("No table returned.");

                return ds.Tables[0];
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet SelectQueryMultiple(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                return ds;
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to insert the data into database.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@result</returns>

        public int Insert(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return 0;

                // Return the first column of the first row of the first table.
                return (int)ds.Tables[0].Rows[0][0];
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to delete the data from database.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@result</returns>

        public int Delete(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return 0;

                // Return the first column of the first row of the first table.
                return (int)ds.Tables[0].Rows[0][0];
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to update the data from database.
        /// </summary>
        /// <param name="query">String parameter takes the query or procedure</param>
        /// <param name="sqlParams">Array of SqlParameters contains list of parameters</param>
        /// <param name="isStoredProcedure">Checks whether it is stored procedure of query</param>
        /// <returns>@result</returns>

        public int Update(string query, OleDbParameter[] sqlParams, bool isStoredProcedure)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet ds = null;

            try
            {
                if (isStoredProcedure)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    throw new Exception("Only stored procedures are supported");

                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParams);

                if (!(_WebDataClient.DatasetRequest(ref ds, ref _error, _DatabaseAlias, cmd)))
                    throw new Exception(_error);

                // If nothing was returned then just return 0;
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return 0;

                // Return the first column of the first row of the first table.
                return (int)ds.Tables[0].Rows[0][0];
            }

            catch (OleDbException sql_ex)
            {
                throw sql_ex;
            }

            catch (NullReferenceException null_ex)
            {
                throw null_ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ConnectToDatabase()
        {
            if (_WebDataClient == null || !(bool)_WebDataClient.IsConnected)
            {
                int maxWaitingTime;
                int remotePort;

                if (_WebDataClient == null)
                {
                    _WebDataClient = new WebDataClient.AAP.WebDataClient.MsgProtocol();
                }

                _WebDataClient.RemoteHost = System.Configuration.ConfigurationManager.AppSettings["WebDataServerHost"].ToString();
                int.TryParse(System.Configuration.ConfigurationManager.AppSettings["WebDataServerPort"], out remotePort);
                _WebDataClient.RemotePort = remotePort;

                if (int.TryParse(System.Configuration.ConfigurationManager.AppSettings["WebDataServerMaxWaitingTime"], out maxWaitingTime))
                {
                    _WebDataClient.MaxWaitingTime = maxWaitingTime;
                }

                _DatabaseAlias = System.Configuration.ConfigurationManager.AppSettings["WebDataServerAlias"];

                _WebDataClient.Connect();
            }
        }
    }
}