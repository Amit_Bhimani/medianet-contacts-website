using System;
using System.Data;
using System.Linq;

namespace Medianet.Contacts.Reporting
{
    /// <summary>
    /// Summary description for ChaseList.
    /// </summary>
    public partial class ChaseList : BaseReport
    {
        public ChaseList()
        {
            InitializeComponent();
        }

        private void ChaseList_NeedDataSource(object sender, EventArgs e)
        {
            try
            {
                var report = (Telerik.Reporting.Processing.Report)sender;

                int myCheckboxState = 0;
                if (report.Parameters["CheckboxState"].Value != null)
                    Int32.TryParse(report.Parameters["CheckboxState"].Value.ToString(), out myCheckboxState);
                string myCheckedItems = report.Parameters["CheckedItems"].Value == null ? "" : report.Parameters["CheckedItems"].Value.ToString();
                string myUncheckedItems = report.Parameters["UncheckedItems"].Value == null ? "" : report.Parameters["UncheckedItems"].Value.ToString();

                this.DataSource = GetMailingLabelData(Convert.ToInt32(report.Parameters["list"].Value),
                                                      Convert.ToInt32(report.Parameters["UserID"].Value),
                                                      myCheckboxState,
                                                      myCheckedItems,
                                                      myUncheckedItems);

                if (report.Parameters["MaxRowsToDisplay"].Value != null)
                {
                    int maxRows;
                    if (int.TryParse(report.Parameters["MaxRowsToDisplay"].Value.ToString(), out maxRows))
                        this.DataSource = LimitToMaxRows((DataTable)this.DataSource, maxRows);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable LimitToMaxRows(DataTable table, int maxRows)
        {
            if (maxRows > 0 && maxRows < table.Rows.Count)
                table = table.AsEnumerable().Take(maxRows).CopyToDataTable();

            return table;
        }
    }
}