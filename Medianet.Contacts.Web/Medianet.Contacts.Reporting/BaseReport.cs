﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using Medianet.Contacts.Wcf.DataContracts.DTO;

namespace Medianet.Contacts.Reporting
{
    public class BaseReport : Telerik.Reporting.Report
    {
        private const int DEFAULT_MAX_ROWS = 2000;

        private int MaxRows
        {
            get
            {
                int maxRows;

                if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings["ListsAndTasksMaxRows"], out maxRows))
                    maxRows = DEFAULT_MAX_ROWS;

                return maxRows;
            }
        }

        public DataTable GetMailingLabelData(int pListId, int pUserId, int pCheckboxState = 0, string pCheckedItems = "", string pUncheckedItems = "")
        {
            DBClass _DBConnection = null;

            try
            {
                _DBConnection = new DBClass();

                OleDbParameter[] parameters = { new OleDbParameter("@listid", pListId),
                                                new OleDbParameter("@USER_ID", pUserId),
                                                new OleDbParameter("@MaxRowcount", MaxRows)
                                              };

                DataTable t = _DBConnection.SelectQuery("e_GetMailingLabels_Listset", parameters, true);
                var myCheckedItems = new List<string>(pCheckedItems.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                var myUncheckedItems = new List<string>(pUncheckedItems.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));

                if (pCheckboxState > 0) 
                {
                    if (pCheckboxState == CheckboxState.All.ToInt())
                    {
                        //@ Do no filtering - all data is required
                    } else if (pCheckboxState == CheckboxState.None.ToInt())
                    {
                        //@ Do nothing, return all data - this should be handled on front end by javascript
                    } else if ((pCheckboxState == CheckboxState.PartNone.ToInt()) || (pCheckboxState == CheckboxState.PartAll.ToInt()))
                    {
                        DataTable t2 = t.Clone();

                        //@ Consider only CheckedItems
                        foreach (DataRow r in t.Rows)
                        {
                            string myId = r["OutletId"].ToString().Trim() + "#" + ((r["ContactId"].ToString().Trim() == "") ? "0" : r["ContactId"].ToString().Trim()) + "#" + ((r["ContactId"].ToString().Trim() == "") ? "1" : "2");

                            //@ Some selected
                            if ((pCheckboxState == CheckboxState.PartNone.ToInt()) && (myCheckedItems.Contains(myId)))
                            {
                                t2.ImportRow(r);
                            }

                            //@ Some unselected
                            if ((pCheckboxState == CheckboxState.PartAll.ToInt()) && (!myUncheckedItems.Contains(myId)))
                            {
                                t2.ImportRow(r);
                            }
                        }

                        t = t2;
                    }
                }

                return t;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (_DBConnection != null) _DBConnection.CloseDBConnection();
            }
        }
    }
}
