﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Medianet.Contacts.Web.Common.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MediaPeople.Test
{
    [TestClass]
    public class AutoMapperTest
    {
        [TestMethod]
        public void Should_Have_Valid_Configuration() {
            AutoMapperConfigurator.Configure();
            Mapper.AssertConfigurationIsValid();
        }
    }
}
