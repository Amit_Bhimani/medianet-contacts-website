﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using Medianet.Contacts.CSharp.ExtensionMethods;
//using FluentAssertions;
//using MediaPeople.Common.Model;
//using MediaPeople.Common.Util;
//using MediaPeople.Controllers;
//using MediaPeople.Services;
//using MediaPeople.ViewModel;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using MvcContrib.TestHelper;
//using MediaPeople.MediaPeopleService;

//using ISearchService = MediaPeople.Services.ISearchService;
//using RecordIdentifier = MediaPeople.MediaPeopleService.RecordIdentifier;

//namespace MediaPeople.Test
//{
//    [TestClass]
//    public class BaseControllerTest : TestBase
//    {
//        private static ISearchService _searchService;
//        private static List<SearchResult> _results;
//        private static List<RecordIdentifier> _records;

//        private BaseController _baseController;

//        private static int UserId = 28923;
//        private static int RandomTake = 5;

//        [ClassInitialize]
//        public static void BaseControllerTestClassInitialise(TestContext textContext) {
//            // Load fixtures.
//            using (StreamReader sr = new StreamReader("data.txt")) {
//                string line;
//                _records = new List<RecordIdentifier>();
//                while ((line = sr.ReadLine()) != null) {
//                    if (!line.IsNullOrEmpty() && !line.StartsWith("//")) {
//                        string[] split = line.Split('#');
//                        var r = new RecordIdentifier(split[0], split[1], split[2]);
//                        _records.Add(r);
//                    }
//                }
//            }
//            _searchService = SearchService.CreateSearchService();
//            //_results = _searchService.FetchRecordIdentifiers(_records, SortColumn.Default, SortDirection.ASC, UserId);
//        }

//        [ClassCleanup]
//        public static void BaseControllerTestClassCleanup() {
//            if (_searchService != null) _searchService.Dispose();
//        }

//        [TestInitialize]
//        public void BaseControllerTestInitialise() {
//            if (_searchService == null)
//                _searchService = SearchService.CreateSearchService();

//            _baseController = new BaseController();
//            var builder = new TestControllerBuilder();
//            builder.InitializeController(_baseController);
//        }

//        [TestCleanup]
//        public void BaseControllerTestCleanup() {
//            if (_baseController != null) _baseController.Dispose();
//        }

//        /// <summary>
//        /// Given a search with some X unselected items
//        /// And a model with new unchecked items
//        /// Then the number of records to add should be the difference between the total results
//        /// and the total of the unselected items.
//        /// </summary>
//        [TestMethod]
//        public void Should_Return_Correct_Records_To_Add_When_PartAll_And_New_Unselected_Items() {
//            // Set all to checked.
//            _results.ForEach(r => r.Selected = true);

//            // Randomly choose a number of items to uncheck.
//            _baseController.Session[BaseController.SessionSearchUnselectedItems] = 
//                _records.OrderBy(x => new Random().Next()).Take(RandomTake).ToList();

//            // Create model.
//            var model = new GridViewModelBase() {
//                CheckboxState = CheckboxState.PartAll,
//                CheckedItems = "",
//                UncheckedItems = "6947941#23771471#4,1781262#3082402#4", // Randomly chosen from data.txt
//                CurrentPageOnly = false
//            };

//            // Get results to add.
//            var result = _baseController.GetSelectedRecords(model, _results);
//            result.Count.Should().Be(_results.Count - (RandomTake + 2));
//        }

//        /// <summary>
//        /// Given a search with some X unselected items
//        /// And a model with new unchecked items
//        /// And a model with new checked items
//        /// Then the number of records to add should be the difference between the total results
//        /// and the total of the unselected items.
//        /// </summary>
//        [TestMethod]
//        public void Should_Return_Correct_Records_To_Add_When_PartAll_And_New_Selected_Items() {
//            var random = new Random().Next(_records.Count);

//            var _checkedList = _records.OrderBy(x => new Random().Next())
//                                .Take(random)
//                                .ToList();
//            var _checked = string.Join(",", _checkedList.Select(r => r.ToClientString()));

//            // Randomly choose a number of items to uncheck.
//            var unselected = _records.OrderBy(x => new Random().Next()).Take(_records.Count - RandomTake).ToList();
//            _baseController.Session[BaseController.SessionSearchUnselectedItems] = unselected;

//            // Create model.
//            var model = new GridViewModelBase() {
//                CheckboxState = CheckboxState.PartAll,
//                CheckedItems = _checked,
//                UncheckedItems = "",
//                CurrentPageOnly = false
//            };

//            // Get results to add.
//            var result = _baseController.GetSelectedRecords(model, _results);
//            result.Count.Should().Be(_records.Count - (unselected.Except(_checkedList).Count()));
//        }

//        [TestMethod]
//        public void Should_Return_All_Current_Search_Records_When_All() {
//            var random = new Random().Next(10);

//            // Randomly choose a number of items to uncheck.
//            _baseController.Session[BaseController.SessionSearchUnselectedItems] =
//                _records.OrderBy(x => new Random().Next()).Take(RandomTake).ToList();

//            var model = new GridViewModelBase() {
//                CheckboxState = CheckboxState.All,
//                CheckedItems = string.Join(",", _records.OrderBy(x => new Random().Next())
//                            .Take(random)
//                            .Select(r => r.ToClientString())
//                            .ToArray()),
//                UncheckedItems = "6947941#23771471#4,1781262#3082402#4", // Randomly chosen from data.txt
//                CurrentPageOnly = true
//            };

//            var result = _baseController.GetSelectedRecords(model, _results);
//            _baseController.GetSessionItems();

//            result.Count.Should().Be(_results.Count);
//            _baseController.UnselectedItems.Should().BeEmpty();
//        }

//        [TestMethod]
//        public void Should_Return_Correct_Records_To_Add_When_PartNone_And_New_Selected_Items() {
//            var random = new Random().Next(_records.Count);
//            var _checkedList = _records.OrderBy(x => new Random().Next())
//                                .Take(random)
//                                .ToList();
//            var _checked = string.Join(",", _checkedList.Select(r => r.ToClientString()));

//            // Randomly choose a number of items to uncheck.
//            _baseController.Session[BaseController.SessionSearchUnselectedItems] =
//                _records.OrderBy(x => new Random().Next()).Take(_records.Count - RandomTake).ToList();

//            var model = new GridViewModelBase() {
//                CheckboxState = CheckboxState.PartNone,
//                CheckedItems = _checked,
//                UncheckedItems = "",
//                CurrentPageOnly = false
//            };

//            var result = _baseController.GetSelectedRecords(model, _results);
//            _baseController.GetSessionItems();

//            result.Count.Should().Be(RandomTake + result.Intersect(_checkedList).Count());
//        }
//    }
//}
