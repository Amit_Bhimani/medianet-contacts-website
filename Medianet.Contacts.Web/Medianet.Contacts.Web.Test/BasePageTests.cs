﻿using System;
using Medianet.Contacts.Web.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MediaPeople.Test
{
    [TestClass]
    public class BasePageTests
    {
        [TestMethod]
        public void FormatTextAsHtml_Test1() {
            var result = BasePage.FormatTextAsHtml("This is a test of www.aap.com.au converting to a URL.");
            Assert.AreEqual("This is a test of <a href=\"http://www.aap.com.au\" target=\"_blank\">www.aap.com.au</a> converting to a URL.", result);
        }
        [TestMethod]
        public void FormatTextAsHtml_Test2() {
            var result = BasePage.FormatTextAsHtml("This is a test of the email david@aap.com.au.");
            Assert.AreEqual("This is a test of the email <a href=\"mailto:david@aap.com.au\">david@aap.com.au</a>.", result);
        }
        [TestMethod]
        public void FormatTextAsHtml_Test3() {
            var result = BasePage.FormatTextAsHtml("This is a test of https://www.aap.com.au converting to a URL.");
            Assert.AreEqual("This is a test of <a href=\"https://www.aap.com.au\" target=\"_blank\">https://www.aap.com.au</a> converting to a URL.", result);
        }
        [TestMethod]
        public void FormatTextAsHtml_Test4() {
            var result = BasePage.FormatTextAsHtml("This is a test of the URL www.aap.com.au.");
            Assert.AreEqual("This is a test of the URL <a href=\"http://www.aap.com.au\" target=\"_blank\">www.aap.com.au</a>.", result);
        }
    }
}
